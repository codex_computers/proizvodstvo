object frmRE: TfrmRE
  Left = 128
  Top = 212
  BorderIcons = [biSystemMenu, biMaximize]
  Caption = #1056#1072#1073#1086#1090#1085#1072' '#1077#1076#1080#1085#1080#1094#1072' ('#1087#1088#1086#1080#1079#1074#1086#1076#1089#1090#1074#1077#1085' '#1088#1077#1089#1091#1088#1089')'
  ClientHeight = 664
  ClientWidth = 715
  Color = clBtnFace
  Constraints.MinHeight = 500
  Constraints.MinWidth = 610
  Font.Charset = RUSSIAN_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  ShowHint = True
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object dxRibbon1: TdxRibbon
    Left = 0
    Top = 0
    Width = 715
    Height = 126
    BarManager = dxBarManager1
    ColorSchemeName = 'Blue'
    Contexts = <>
    TabOrder = 0
    TabStop = False
    object dxRibbon1Tab1: TdxRibbonTab
      Active = True
      Caption = #1052#1077#1085#1080
      Groups = <
        item
          ToolbarName = 'dxBarManager1Bar1'
        end
        item
          ToolbarName = 'dxBarManager1Bar2'
        end
        item
          ToolbarName = 'dxBarManager1Bar3'
        end>
      Index = 0
    end
    object dxRibbon1Tab2: TdxRibbonTab
      Caption = #1055#1086#1076#1077#1089#1091#1074#1072#1114#1072
      Groups = <
        item
          ToolbarName = 'dxBarManager1Bar4'
        end
        item
          ToolbarName = 'dxBarManager1Bar5'
        end>
      Index = 1
    end
  end
  object StatusBar1: TdxRibbonStatusBar
    Left = 0
    Top = 641
    Width = 715
    Height = 23
    Panels = <
      item
        PanelStyleClassName = 'TdxStatusBarTextPanelStyle'
        Text = 
          'F5 - '#1053#1086#1074', F6 - '#1040#1078#1091#1088#1080#1088#1072#1112', F7 - '#1054#1089#1074#1077#1078#1080', F8 - '#1041#1088#1080#1096#1080',F9 - '#1047#1072#1087#1080#1096#1080', F1' +
          '0 - '#1055#1077#1095#1072#1090#1080', Esc - '#1054#1090#1082#1072#1078#1080' / '#1048#1079#1083#1077#1079
      end>
    Ribbon = dxRibbon1
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clDefault
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
  end
  object lPanel: TPanel
    Left = 0
    Top = 126
    Width = 715
    Height = 242
    Align = alClient
    BevelInner = bvLowered
    BevelOuter = bvSpace
    TabOrder = 2
    object dxTree: TdxDBTreeView
      Left = 2
      Top = 2
      Width = 711
      Height = 238
      ShowNodeHint = True
      HotTrack = True
      RowSelect = True
      DataSource = dm.dsRE
      DisplayField = 'ID;NAZIV'
      KeyField = 'ID'
      ListField = 'NAZIV'
      ParentField = 'KOREN'
      RootValue = Null
      SeparatedSt = ' -> '
      RaiseOnError = True
      ReadOnly = True
      DragMode = dmAutomatic
      Indent = 19
      OnChanging = dxTreeChanging
      Align = alClient
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentColor = False
      ParentCtl3D = False
      Ctl3D = True
      Options = [trDBConfirmDelete, trCanDBNavigate, trSmartRecordCopy, trCheckHasChildren]
      SelectedIndex = -1
      TabOrder = 0
      OnDblClick = dxTreeDblClick
      OnKeyPress = dxTreeKeyPress
      ParentFont = False
    end
  end
  object pcRabotniEdinici: TcxPageControl
    Left = 0
    Top = 368
    Width = 715
    Height = 273
    Align = alBottom
    TabOrder = 7
    Properties.ActivePage = tsRE
    OnChange = pcRabotniEdiniciChange
    ClientRectBottom = 273
    ClientRectRight = 715
    ClientRectTop = 24
    object tsRE: TcxTabSheet
      Caption = 'tsRE'
      ImageIndex = 0
      object dPanel: TPanel
        Left = 0
        Top = 0
        Width = 715
        Height = 249
        Align = alClient
        Color = 15790320
        Enabled = False
        ParentBackground = False
        TabOrder = 0
        DesignSize = (
          715
          249)
        object Label1: TLabel
          Left = 22
          Top = 41
          Width = 80
          Height = 13
          Alignment = taRightJustify
          AutoSize = False
          Caption = #1064#1080#1092#1088#1072' :'
          Font.Charset = RUSSIAN_CHARSET
          Font.Color = clRed
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label15: TLabel
          Left = 22
          Top = 68
          Width = 80
          Height = 13
          Alignment = taRightJustify
          AutoSize = False
          Caption = #1053#1072#1079#1080#1074' :'
          Font.Charset = RUSSIAN_CHARSET
          Font.Color = clRed
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label5: TLabel
          Left = 22
          Top = 95
          Width = 80
          Height = 13
          Alignment = taRightJustify
          AutoSize = False
          Caption = #1050#1086#1088#1077#1085' :'
          Font.Charset = RUSSIAN_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label2: TLabel
          Left = 7
          Top = 122
          Width = 95
          Height = 13
          Alignment = taRightJustify
          AutoSize = False
          Caption = #1056#1072#1082#1086#1074#1086#1076#1080#1090#1077#1083' :'
          Font.Charset = RUSSIAN_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label3: TLabel
          Left = 202
          Top = 41
          Width = 80
          Height = 16
          Alignment = taRightJustify
          AutoSize = False
          Caption = #1055#1086#1090#1077#1082#1083#1086' :'
          Font.Charset = RUSSIAN_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
          Visible = False
        end
        object Label4: TLabel
          Left = 205
          Top = 95
          Width = 80
          Height = 13
          Alignment = taRightJustify
          AutoSize = False
          Caption = #1057#1087#1080#1089#1086#1082' :'
          Font.Charset = RUSSIAN_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
          Visible = False
        end
        object Label14: TLabel
          Left = 36
          Top = 151
          Width = 66
          Height = 13
          Alignment = taRightJustify
          AutoSize = False
          Caption = #1055#1072#1088#1090#1085#1077#1088' :'
          Font.Charset = RUSSIAN_CHARSET
          Font.Color = clNavy
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Sifra: TcxDBTextEdit
          Tag = 1
          Left = 108
          Top = 38
          BeepOnEnter = False
          DataBinding.DataField = 'ID'
          DataBinding.DataSource = dm.dsRE
          ParentFont = False
          Properties.BeepOnError = True
          Properties.CharCase = ecUpperCase
          TabOrder = 0
          OnEnter = cxDBTextEditAllEnter
          OnExit = cxDBTextEditAllExit
          OnKeyDown = EnterKakoTab
          Width = 100
        end
        object Naziv: TcxDBTextEdit
          Tag = 1
          Left = 108
          Top = 65
          BeepOnEnter = False
          DataBinding.DataField = 'NAZIV'
          DataBinding.DataSource = dm.dsRE
          ParentFont = False
          Properties.BeepOnError = True
          TabOrder = 1
          OnEnter = cxDBTextEditAllEnter
          OnExit = cxDBTextEditAllExit
          OnKeyDown = EnterKakoTab
          Width = 490
        end
        object Koren: TcxDBTextEdit
          Left = 108
          Top = 92
          BeepOnEnter = False
          DataBinding.DataField = 'KOREN'
          DataBinding.DataSource = dm.dsRE
          ParentFont = False
          Properties.BeepOnError = True
          Properties.CharCase = ecUpperCase
          TabOrder = 2
          OnEnter = cxDBTextEditAllEnter
          OnExit = cxDBTextEditAllExit
          OnKeyDown = EnterKakoTab
          Width = 100
        end
        object Rakovoditel: TcxDBTextEdit
          Left = 108
          Top = 119
          BeepOnEnter = False
          DataBinding.DataField = 'RAKOVODITEL'
          DataBinding.DataSource = dm.dsRE
          ParentFont = False
          Properties.BeepOnError = True
          TabOrder = 3
          OnEnter = cxDBTextEditAllEnter
          OnExit = cxDBTextEditAllExit
          OnKeyDown = EnterKakoTab
          Width = 490
        end
        object Poteklo: TcxDBTextEdit
          Left = 288
          Top = 38
          BeepOnEnter = False
          DataBinding.DataField = 'POTEKLO'
          DataBinding.DataSource = dm.dsRE
          ParentFont = False
          Properties.BeepOnError = True
          TabOrder = 4
          Visible = False
          OnEnter = cxDBTextEditAllEnter
          OnExit = cxDBTextEditAllExit
          OnKeyDown = EnterKakoTab
          Width = 310
        end
        object Spisok: TcxDBTextEdit
          Left = 288
          Top = 92
          BeepOnEnter = False
          DataBinding.DataField = 'SPISOK'
          DataBinding.DataSource = dm.dsRE
          ParentFont = False
          Properties.BeepOnError = True
          TabOrder = 5
          Visible = False
          OnEnter = cxDBTextEditAllEnter
          OnExit = cxDBTextEditAllExit
          OnKeyDown = EnterKakoTab
          Width = 310
        end
        object TipPartner: TcxDBTextEdit
          Left = 108
          Top = 148
          BeepOnEnter = False
          DataBinding.DataField = 'TIP_PARTNER'
          DataBinding.DataSource = dm.dsRE
          ParentFont = False
          Properties.CharCase = ecUpperCase
          TabOrder = 8
          OnEnter = cxDBTextEditAllEnter
          OnExit = cxDBTextEditAllExit
          OnKeyDown = EnterKakoTab
          Width = 53
        end
        object Partner: TcxDBTextEdit
          Left = 167
          Top = 148
          BeepOnEnter = False
          DataBinding.DataField = 'PARTNER'
          DataBinding.DataSource = dm.dsRE
          ParentFont = False
          Properties.CharCase = ecUpperCase
          Properties.OnEditValueChanged = PartnerPropertiesEditValueChanged
          TabOrder = 9
          OnEnter = cxDBTextEditAllEnter
          OnExit = cxDBTextEditAllExit
          OnKeyDown = EnterKakoTab
          Width = 50
        end
        object ZapisiButton: TcxButton
          Left = 868
          Top = 185
          Width = 75
          Height = 25
          Action = aZapisi
          Anchors = [akRight, akBottom]
          Colors.Pressed = clGradientActiveCaption
          TabOrder = 6
        end
        object OtkaziButton: TcxButton
          Left = 949
          Top = 185
          Width = 75
          Height = 25
          Action = aOtkazi
          Anchors = [akRight, akBottom]
          Colors.Pressed = clGradientActiveCaption
          TabOrder = 7
        end
        object cBoxExtPartner: TcxExtLookupComboBox
          Left = 223
          Top = 148
          Properties.DropDownSizeable = True
          Properties.View = cxGridViewRepository1DBTableView1
          Properties.KeyFieldNames = 'TIP_PARTNER;ID'
          Properties.ListFieldItem = cxGridViewRepository1DBTableView1NAZIV
          Properties.OnEditValueChanged = cBoxExtPartnerPropertiesEditValueChanged
          TabOrder = 10
          OnKeyDown = EnterKakoTab
          Width = 375
        end
        object cxButton3: TcxButton
          Left = 513
          Top = 210
          Width = 75
          Height = 25
          Action = aZapisi
          Anchors = [akRight, akBottom]
          Colors.Pressed = clGradientActiveCaption
          TabOrder = 11
        end
        object cxButton4: TcxButton
          Left = 592
          Top = 210
          Width = 75
          Height = 25
          Action = aOtkazi
          Anchors = [akRight, akBottom]
          Colors.Pressed = clGradientActiveCaption
          TabOrder = 12
        end
      end
    end
    object tsPR: TcxTabSheet
      Caption = #1050#1072#1088#1072#1082#1090#1077#1088#1080#1089#1090#1080#1082#1080
      ImageIndex = 1
      object Panel1: TPanel
        Left = 0
        Top = 6
        Width = 715
        Height = 243
        Align = alBottom
        Color = 15790320
        Enabled = False
        ParentBackground = False
        TabOrder = 0
        DesignSize = (
          715
          243)
        object Label6: TLabel
          Left = 22
          Top = 15
          Width = 80
          Height = 13
          Alignment = taRightJustify
          AutoSize = False
          Caption = #1064#1080#1092#1088#1072' :'
          Font.Charset = RUSSIAN_CHARSET
          Font.Color = clRed
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
          Visible = False
        end
        object cxDBTextEdit1: TcxDBTextEdit
          Tag = 1
          Left = 108
          Top = 12
          BeepOnEnter = False
          DataBinding.DataField = 'ID'
          DataBinding.DataSource = dm.dsRE
          ParentFont = False
          Properties.BeepOnError = True
          Properties.CharCase = ecUpperCase
          TabOrder = 0
          Visible = False
          OnEnter = cxDBTextEditAllEnter
          OnExit = cxDBTextEditAllExit
          OnKeyDown = EnterKakoTab
          Width = 100
        end
        object cxButton1: TcxButton
          Left = 535
          Top = 212
          Width = 75
          Height = 25
          Action = aZapisi
          Anchors = [akRight, akBottom]
          Colors.Pressed = clGradientActiveCaption
          TabOrder = 3
        end
        object cxButton2: TcxButton
          Left = 626
          Top = 212
          Width = 75
          Height = 25
          Action = aOtkazi
          Anchors = [akRight, akBottom]
          Colors.Pressed = clGradientActiveCaption
          TabOrder = 4
        end
        object gbRE: TcxGroupBox
          Left = 1
          Top = 1
          Align = alTop
          Caption = '  '#1047#1072' '#1087#1088#1086#1080#1079#1074#1086#1076#1089#1090#1074#1077#1085' '#1088#1077#1089#1091#1088#1089'  '
          TabOrder = 1
          DesignSize = (
            713
            80)
          Height = 80
          Width = 713
          object Label7: TLabel
            Left = 6
            Top = 21
            Width = 58
            Height = 13
            Alignment = taRightJustify
            AutoSize = False
            Caption = #1053#1072#1079#1080#1074' :'
            Font.Charset = RUSSIAN_CHARSET
            Font.Color = clRed
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object Label8: TLabel
            Left = 6
            Top = 45
            Width = 58
            Height = 13
            Alignment = taRightJustify
            AutoSize = False
            Caption = #1054#1087#1080#1089' :'
            Font.Charset = RUSSIAN_CHARSET
            Font.Color = clNavy
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object Label9: TLabel
            Left = 429
            Top = 45
            Width = 100
            Height = 13
            Alignment = taRightJustify
            Anchors = [akRight, akBottom]
            AutoSize = False
            Caption = #1056#1072#1073#1086#1090#1085#1086' '#1074#1088#1077#1084#1077' :'
            Font.Charset = RUSSIAN_CHARSET
            Font.Color = clNavy
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object Label10: TLabel
            Left = 605
            Top = 44
            Width = 95
            Height = 19
            Anchors = [akRight, akBottom]
            AutoSize = False
            Caption = #1095#1072#1089#1086#1074#1080'/'#1085#1077#1076#1077#1083#1085#1086
            Font.Charset = RUSSIAN_CHARSET
            Font.Color = clNavy
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            Transparent = False
            WordWrap = True
          end
          object txtRE: TcxDBTextEdit
            Tag = 1
            Left = 67
            Top = 18
            BeepOnEnter = False
            DataBinding.DataField = 'ID_RE'
            DataBinding.DataSource = dm.dsProizvodstvenResurs
            Enabled = False
            ParentFont = False
            Properties.BeepOnError = True
            StyleDisabled.BorderStyle = ebsUltraFlat
            StyleDisabled.Color = clWindow
            StyleDisabled.TextColor = clWindowText
            TabOrder = 0
            OnEnter = cxDBTextEditAllEnter
            OnExit = cxDBTextEditAllExit
            OnKeyDown = EnterKakoTab
            Width = 54
          end
          object cbRE: TcxDBLookupComboBox
            Left = 123
            Top = 18
            Anchors = [akLeft, akTop, akRight, akBottom]
            DataBinding.DataField = 'ID_RE'
            DataBinding.DataSource = dm.dsProizvodstvenResurs
            Enabled = False
            Properties.ClearKey = 46
            Properties.DropDownSizeable = True
            Properties.ImmediatePost = True
            Properties.KeyFieldNames = 'ID'
            Properties.ListColumns = <
              item
                FieldName = 'ID'
              end
              item
                FieldName = 'naziv'
              end>
            Properties.ListFieldIndex = 1
            Properties.ListSource = dmMat.dsRE
            StyleDisabled.BorderStyle = ebsUltraFlat
            StyleDisabled.Color = clWindow
            StyleDisabled.TextColor = clWindowText
            TabOrder = 1
            OnEnter = cxDBTextEditAllEnter
            OnExit = cxDBTextEditAllExit
            OnKeyDown = EnterKakoTab
            Width = 292
          end
          object cbAktiven: TcxDBCheckBox
            Left = 535
            Top = 17
            Anchors = [akRight, akBottom]
            Caption = #1040#1082#1090#1080#1074#1077#1085
            DataBinding.DataField = 'R'
            DataBinding.DataSource = dm.dsRE
            ParentBackground = False
            ParentColor = False
            ParentFont = False
            Properties.Alignment = taLeftJustify
            Properties.DisplayChecked = 'true'
            Properties.DisplayUnchecked = 'false'
            Properties.ImmediatePost = True
            Properties.ValueChecked = 1
            Properties.ValueUnchecked = 0
            TabOrder = 2
            Transparent = True
            OnKeyDown = EnterKakoTab
            Width = 111
          end
          object txtOpis: TcxDBTextEdit
            Left = 67
            Top = 42
            Anchors = [akLeft, akTop, akRight, akBottom]
            BeepOnEnter = False
            DataBinding.DataField = 'OPIS'
            DataBinding.DataSource = dm.dsProizvodstvenResurs
            ParentFont = False
            Properties.BeepOnError = True
            TabOrder = 3
            OnEnter = cxDBTextEditAllEnter
            OnExit = cxDBTextEditAllExit
            OnKeyDown = EnterKakoTab
            Width = 348
          end
          object txtRabotnoVreme: TcxDBTextEdit
            Left = 535
            Top = 42
            Anchors = [akRight, akBottom]
            BeepOnEnter = False
            DataBinding.DataField = 'NEDELNO_RABOTNO_VREME'
            DataBinding.DataSource = dm.dsProizvodstvenResurs
            ParentFont = False
            Properties.BeepOnError = True
            TabOrder = 4
            OnEnter = cxDBTextEditAllEnter
            OnExit = cxDBTextEditAllExit
            OnKeyDown = EnterKakoTab
            Width = 68
          end
        end
        object cxGroupBox2: TcxGroupBox
          Left = 1
          Top = 81
          Align = alTop
          Caption = '  '#1050#1072#1087#1072#1094#1080#1090#1077#1090' '#1080' '#1094#1077#1085#1080'  '
          TabOrder = 2
          DesignSize = (
            713
            123)
          Height = 123
          Width = 713
          object Label11: TLabel
            Left = 21
            Top = 29
            Width = 173
            Height = 13
            Alignment = taRightJustify
            AutoSize = False
            Caption = #1042#1088#1077#1084#1077#1085#1089#1082#1072' '#1084#1077#1088#1085#1072' '#1077#1076#1080#1085#1080#1094#1072
            Font.Charset = RUSSIAN_CHARSET
            Font.Color = clRed
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object Label12: TLabel
            Left = 21
            Top = 51
            Width = 173
            Height = 13
            Alignment = taRightJustify
            AutoSize = False
            Caption = #1042#1088#1077#1084#1077' '#1079#1072' '#1087#1086#1076#1075#1086#1090#1086#1074#1082#1072
            Font.Charset = RUSSIAN_CHARSET
            Font.Color = clNavy
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object Label13: TLabel
            Left = 21
            Top = 73
            Width = 173
            Height = 13
            Alignment = taRightJustify
            AutoSize = False
            Caption = #1042#1088#1077#1084#1077' '#1079#1072' '#1082#1088#1072#1112
            Font.Charset = RUSSIAN_CHARSET
            Font.Color = clNavy
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object Label16: TLabel
            Left = 21
            Top = 95
            Width = 173
            Height = 13
            Alignment = taRightJustify
            AutoSize = False
            Caption = #1042#1088#1077#1084#1077' '#1085#1072' '#1077#1076#1077#1085' '#1094#1080#1082#1083#1091#1089
            Font.Charset = RUSSIAN_CHARSET
            Font.Color = clNavy
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object Label18: TLabel
            Left = 437
            Top = 53
            Width = 91
            Height = 13
            Alignment = taRightJustify
            Anchors = [akRight, akBottom]
            Caption = #1062#1077#1085#1072' '#1085#1072' '#1094#1080#1082#1083#1091#1089
            Font.Charset = RUSSIAN_CHARSET
            Font.Color = clNavy
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object Label19: TLabel
            Left = 440
            Top = 75
            Width = 88
            Height = 13
            Alignment = taRightJustify
            Anchors = [akRight, akBottom]
            Caption = #1060#1072#1082#1090#1086#1088' '#1094#1080#1082#1083#1091#1089
            Font.Charset = RUSSIAN_CHARSET
            Font.Color = clNavy
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object Label20: TLabel
            Left = 404
            Top = 97
            Width = 124
            Height = 13
            Alignment = taRightJustify
            Anchors = [akRight, akBottom]
            Caption = #1050#1072#1087#1072#1094#1080#1090#1077#1090' '#1085#1072' '#1094#1080#1082#1083#1091#1089
            Font.Charset = RUSSIAN_CHARSET
            Font.Color = clNavy
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object Label17: TLabel
            Left = 495
            Top = 31
            Width = 33
            Height = 13
            Alignment = taRightJustify
            Anchors = [akRight, akBottom]
            Caption = #1062#1077#1085#1072' '
            Font.Charset = RUSSIAN_CHARSET
            Font.Color = clNavy
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object txtVremenskaME: TcxDBTextEdit
            Tag = 1
            Left = 200
            Top = 26
            BeepOnEnter = False
            DataBinding.DataField = 'MERNA_EDINICA'
            DataBinding.DataSource = dm.dsProizvodstvenResurs
            ParentFont = False
            Properties.BeepOnError = True
            TabOrder = 0
            OnEnter = cxDBTextEditAllEnter
            OnExit = cxDBTextEditAllExit
            OnKeyDown = EnterKakoTab
            Width = 49
          end
          object txtVremePocetok: TcxDBTextEdit
            Left = 200
            Top = 48
            BeepOnEnter = False
            DataBinding.DataField = 'VREME_ZA_POCETOK'
            DataBinding.DataSource = dm.dsProizvodstvenResurs
            ParentFont = False
            Properties.BeepOnError = True
            TabOrder = 2
            OnEnter = cxDBTextEditAllEnter
            OnExit = cxDBTextEditAllExit
            OnKeyDown = EnterKakoTab
            Width = 49
          end
          object txtVremeKraj: TcxDBTextEdit
            Left = 200
            Top = 70
            BeepOnEnter = False
            DataBinding.DataField = 'VREME_PO_ZAVRSUVANJE'
            DataBinding.DataSource = dm.dsProizvodstvenResurs
            ParentFont = False
            Properties.BeepOnError = True
            TabOrder = 3
            OnEnter = cxDBTextEditAllEnter
            OnExit = cxDBTextEditAllExit
            OnKeyDown = EnterKakoTab
            Width = 49
          end
          object lblMernaEdinicaK: TcxDBLabel
            Left = 248
            Top = 73
            DataBinding.DataField = 'MERNA_EDINICA'
            DataBinding.DataSource = dm.dsProizvodstvenResurs
            Transparent = True
            Height = 21
            Width = 64
          end
          object txtVremeCiklus: TcxDBTextEdit
            Left = 200
            Top = 92
            BeepOnEnter = False
            DataBinding.DataField = 'VREME_CIKLUS'
            DataBinding.DataSource = dm.dsProizvodstvenResurs
            ParentFont = False
            Properties.BeepOnError = True
            TabOrder = 5
            OnEnter = cxDBTextEditAllEnter
            OnExit = cxDBTextEditAllExit
            OnKeyDown = EnterKakoTab
            Width = 49
          end
          object lblMernaEdinica2: TcxDBLabel
            Left = 248
            Top = 95
            DataBinding.DataField = 'MERNA_EDINICA'
            DataBinding.DataSource = dm.dsProizvodstvenResurs
            Transparent = True
            Height = 21
            Width = 64
          end
          object txtCena: TcxDBTextEdit
            Left = 534
            Top = 26
            Anchors = [akRight, akBottom]
            BeepOnEnter = False
            DataBinding.DataField = 'CENA_CAS'
            DataBinding.DataSource = dm.dsProizvodstvenResurs
            ParentFont = False
            Properties.BeepOnError = True
            TabOrder = 7
            OnEnter = cxDBTextEditAllEnter
            OnExit = cxDBTextEditAllExit
            OnKeyDown = EnterKakoTab
            Width = 49
          end
          object txtCenaCiklus: TcxDBTextEdit
            Left = 534
            Top = 48
            Anchors = [akRight, akBottom]
            BeepOnEnter = False
            DataBinding.DataField = 'CENA_CIKLUS'
            DataBinding.DataSource = dm.dsProizvodstvenResurs
            ParentFont = False
            Properties.BeepOnError = True
            TabOrder = 8
            OnEnter = cxDBTextEditAllEnter
            OnExit = cxDBTextEditAllExit
            OnKeyDown = EnterKakoTab
            Width = 49
          end
          object txtFaktorCiklus: TcxDBTextEdit
            Left = 534
            Top = 70
            Anchors = [akRight, akBottom]
            BeepOnEnter = False
            DataBinding.DataField = 'FAKTOR_CIKLUS'
            DataBinding.DataSource = dm.dsProizvodstvenResurs
            ParentFont = False
            Properties.BeepOnError = True
            TabOrder = 9
            OnEnter = cxDBTextEditAllEnter
            OnExit = cxDBTextEditAllExit
            OnKeyDown = EnterKakoTab
            Width = 49
          end
          object txtKapacitetCiklus: TcxDBTextEdit
            Left = 534
            Top = 92
            Anchors = [akRight, akBottom]
            BeepOnEnter = False
            DataBinding.DataField = 'KAPACITET_CIKLUS'
            DataBinding.DataSource = dm.dsProizvodstvenResurs
            ParentFont = False
            Properties.BeepOnError = True
            TabOrder = 10
            OnEnter = cxDBTextEditAllEnter
            OnExit = cxDBTextEditAllExit
            OnKeyDown = EnterKakoTab
            Width = 49
          end
          object cbVremenskaME: TcxDBLookupComboBox
            Left = 248
            Top = 26
            Hint = #1050#1083#1080#1082#1085#1077#1090#1077' '#1076#1074#1072' '#1087#1072#1090#1080' '#1079#1072' '#1076#1072' '#1074#1085#1077#1089#1077#1090#1077' '#1085#1086#1074#1072' '#1084#1077#1088#1085#1072' '#1077#1076#1080#1085#1080#1094#1072'!'
            Anchors = [akLeft, akTop, akRight, akBottom]
            DataBinding.DataField = 'MERNA_EDINICA'
            DataBinding.DataSource = dm.dsProizvodstvenResurs
            Properties.ClearKey = 46
            Properties.DropDownSizeable = True
            Properties.ImmediatePost = True
            Properties.KeyFieldNames = 'ID'
            Properties.ListColumns = <
              item
                FieldName = 'ID'
              end
              item
                FieldName = 'naziv'
              end>
            Properties.ListFieldIndex = 1
            Properties.ListSource = dmMat.dsMerka
            TabOrder = 1
            OnEnter = cxDBTextEditAllEnter
            OnExit = cxDBTextEditAllExit
            OnKeyDown = EnterKakoTab
            Width = 167
          end
          object cxLabel2: TcxLabel
            Left = 585
            Top = 49
            Anchors = [akRight, akBottom]
            Caption = #1076#1077#1085'.'
          end
          object cxLabel1: TcxLabel
            Left = 585
            Top = 27
            Anchors = [akRight, akBottom]
            Caption = #1076#1077#1085'.'
          end
          object lblMernaEdinica: TcxDBLabel
            Left = 248
            Top = 49
            DataBinding.DataField = 'MERNA_EDINICA'
            DataBinding.DataSource = dm.dsProizvodstvenResurs
            Transparent = True
            Height = 19
            Width = 64
          end
        end
      end
    end
  end
  object cxGridPopupMenu1: TcxGridPopupMenu
    PopupMenus = <>
    Left = 264
    Top = 176
  end
  object PopupMenu1: TPopupMenu
    Left = 368
    Top = 168
    object N1: TMenuItem
      Caption = #1057#1085#1080#1084#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      ShortCut = 49235
      OnClick = SaveToIniFileExecute
    end
    object Excel1: TMenuItem
      Caption = #1057#1085#1080#1084#1080' '#1075#1086' '#1074#1086' Excel'
      ShortCut = 49221
      OnClick = ExportToExcelExecute
    end
  end
  object dxBarManager1: TdxBarManager
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Segoe UI'
    Font.Style = []
    Categories.Strings = (
      'Default')
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    ImageOptions.Images = dmRes.cxSmallImages
    ImageOptions.LargeImages = dmRes.cxLargeImages
    PopupMenuLinks = <>
    UseSystemFont = True
    Left = 472
    Top = 168
    DockControlHeights = (
      0
      0
      0
      0)
    object dxBarManager1Bar1: TdxBar
      Caption = 'Custom 1'
      CaptionButtons = <>
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 715
      FloatTop = 157
      FloatClientWidth = 51
      FloatClientHeight = 120
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton1'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton2'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton3'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar2: TdxBar
      Caption = #1040#1082#1094#1080#1080
      CaptionButtons = <>
      DockedLeft = 177
      DockedTop = 0
      FloatLeft = 789
      FloatTop = 120
      FloatClientWidth = 51
      FloatClientHeight = 120
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarSubItem1'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar3: TdxBar
      Caption = #1048#1079#1083#1077#1079
      CaptionButtons = <>
      DockedLeft = 257
      DockedTop = 0
      FloatLeft = 718
      FloatTop = 62
      FloatClientWidth = 51
      FloatClientHeight = 80
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton7'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton8'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar4: TdxBar
      Caption = #1055#1077#1095#1072#1090#1077#1114#1077
      CaptionButtons = <>
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 676
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton12'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton15'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton9'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton13'
        end>
      OneOnRow = True
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar5: TdxBar
      Caption = 'Layout'
      CaptionButtons = <>
      DockedDockingStyle = dsTop
      DockedLeft = 336
      DockedTop = 0
      FloatLeft = 759
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton17'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton10'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton16'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = False
      WholeRow = False
    end
    object dxBarLargeButton2: TdxBarLargeButton
      Action = aAzuriraj
      Category = 0
    end
    object dxBarLargeButton3: TdxBarLargeButton
      Action = aBrisi
      Category = 0
    end
    object dxBarLargeButton4: TdxBarLargeButton
      Caption = #1057#1085#1080#1084#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      Category = 0
      Visible = ivAlways
      LargeImageIndex = 9
      ShortCut = 24659
      OnClick = aSnimiIzgledExecute
    end
    object dxBarLargeButton5: TdxBarLargeButton
      Caption = #1047#1072#1095#1091#1074#1072#1112' '#1074#1086' Excel'
      Category = 0
      Visible = ivAlways
      LargeImageIndex = 9
      ShortCut = 24645
      OnClick = aZacuvajExcelExecute
    end
    object dxBarLargeButton6: TdxBarLargeButton
      Action = aPecati
      Caption = #1055#1077#1095#1072#1090#1080' '#1090#1072#1073#1077#1083#1072
      Category = 0
    end
    object dxBarLargeButton7: TdxBarLargeButton
      Action = aHelp
      Category = 0
    end
    object dxBarLargeButton8: TdxBarLargeButton
      Action = aIzlez
      Category = 0
    end
    object dxBarLargeButton1: TdxBarLargeButton
      Action = aNov
      Category = 0
    end
    object dxBarLargeButton9: TdxBarLargeButton
      Action = aSnimiPecatenje
      Category = 0
    end
    object dxBarLargeButton11: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object ribbonFilterReset: TdxBarLargeButton
      Caption = #1056#1077#1089#1077#1090#1080#1088#1072#1112' '#1092#1080#1083#1090#1077#1088
      Category = 0
      Hint = #1056#1077#1089#1077#1090#1080#1088#1072#1112' '#1092#1080#1083#1090#1077#1088
      Visible = ivAlways
      OnClick = ribbonFilterResetClick
    end
    object dxBarLargeButton12: TdxBarLargeButton
      Action = aPodesuvanjePecatenje
      Category = 0
    end
    object dxBarLargeButton13: TdxBarLargeButton
      Action = aBrisiPodesuvanjePecatenje
      Category = 0
    end
    object dxBarLargeButton14: TdxBarLargeButton
      Caption = #1048#1079#1073#1088#1080#1096#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      Category = 0
      Visible = ivAlways
      LargeImageIndex = 40
    end
    object dxBarLargeButton15: TdxBarLargeButton
      Action = aPageSetup
      Category = 0
    end
    object dxBarLargeButton10: TdxBarLargeButton
      Action = aSnimiLayout
      Category = 0
    end
    object dxBarLargeButton16: TdxBarLargeButton
      Action = aBrisiLayout
      Caption = #1041#1088#1080#1096#1080' '#1087#1086#1076#1077#1089#1091#1074#1072#1114#1072
      Category = 0
    end
    object dxBarLargeButton17: TdxBarLargeButton
      Action = aDizajnLayout
      Category = 0
    end
    object dxBarSubItem1: TdxBarSubItem
      Caption = #1055#1077#1095#1072#1090#1077#1114#1077
      Category = 0
      Visible = ivAlways
      ImageIndex = 30
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton6'
        end>
    end
  end
  object ActionList1: TActionList
    Images = dmRes.cxSmallImages
    Left = 152
    Top = 184
    object aNov: TAction
      Caption = #1053#1086#1074#1072
      ImageIndex = 10
      ShortCut = 116
      OnExecute = aNovExecute
    end
    object aAzuriraj: TAction
      Caption = #1040#1078#1091#1088#1080#1088#1072#1112
      ImageIndex = 12
      ShortCut = 117
      OnExecute = aAzurirajExecute
    end
    object aBrisi: TAction
      Caption = #1041#1088#1080#1096#1080
      ImageIndex = 11
      ShortCut = 119
      OnExecute = aBrisiExecute
    end
    object aPecati: TAction
      Caption = #1055#1077#1095#1072#1090#1080
      ImageIndex = 30
      OnExecute = aPecatiExecute
    end
    object aHelp: TAction
      Caption = #1055#1086#1084#1086#1096
      ImageIndex = 24
    end
    object aIzlez: TAction
      Caption = #1048#1079#1083#1077#1079
      ImageIndex = 25
      OnExecute = aIzlezExecute
    end
    object aZapisi: TAction
      Caption = #1047#1072#1087#1080#1096#1080
      ImageIndex = 7
      ShortCut = 120
      OnExecute = aZapisiExecute
    end
    object aOtkazi: TAction
      Caption = #1054#1090#1082#1072#1078#1080
      ImageIndex = 21
      ShortCut = 27
      OnExecute = aOtkaziExecute
    end
    object aRefresh: TAction
      Caption = 'aRefresh'
      ImageIndex = 18
      ShortCut = 118
      OnExecute = aRefreshExecute
    end
    object aSnimiPecatenje: TAction
      Caption = #1057#1085#1080#1084#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      ImageIndex = 39
      OnExecute = aSnimiPecatenjeExecute
    end
    object aPodesuvanjePecatenje: TAction
      Caption = #1044#1080#1079#1072#1112#1085' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090
      ImageIndex = 38
      OnExecute = aPodesuvanjePecatenjeExecute
    end
    object aBrisiPodesuvanjePecatenje: TAction
      Caption = #1041#1088#1080#1096#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      ImageIndex = 37
      OnExecute = aBrisiPodesuvanjePecatenjeExecute
    end
    object aPageSetup: TAction
      Caption = #1057#1077#1090#1080#1088#1072#1114#1077' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072
      ImageIndex = 38
      OnExecute = aPageSetupExecute
    end
    object aSnimiLayout: TAction
      Caption = #1047#1072#1095#1091#1074#1072#1112' '#1087#1086#1076#1077#1089#1091#1074#1072#1114#1072
      ImageIndex = 41
      OnExecute = aSnimiLayoutExecute
    end
    object aBrisiLayout: TAction
      Caption = #1041#1088#1080#1096#1080' Layout'
      ImageIndex = 40
      OnExecute = aBrisiLayoutExecute
    end
    object aDizajnLayout: TAction
      Caption = #1044#1080#1079#1072#1112#1085#1080#1088#1072#1112
      ImageIndex = 41
      OnExecute = aDizajnLayoutExecute
    end
  end
  object dxComponentPrinter1: TdxComponentPrinter
    CurrentLink = dxComponentPrinter1Link1
    Version = 0
    Left = 56
    Top = 184
    object dxComponentPrinter1Link1: TdxDBTreeViewReportLink
      Active = True
      Component = dxTree
      PrinterPage.DMPaper = 9
      PrinterPage.Footer = 6350
      PrinterPage.GrayShading = True
      PrinterPage.Header = 6350
      PrinterPage.Margins.Bottom = 12700
      PrinterPage.Margins.Left = 12700
      PrinterPage.Margins.Right = 12700
      PrinterPage.Margins.Top = 12700
      PrinterPage.Orientation = poLandscape
      PrinterPage.PageSize.X = 210000
      PrinterPage.PageSize.Y = 297000
      PrinterPage._dxMeasurementUnits_ = 0
      PrinterPage._dxLastMU_ = 2
      ReportDocument.CreationDate = 41109.887060162040000000
      AutoNodesExpand = True
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clBlack
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      BuiltInReportLink = True
    end
  end
  object cxGridViewRepository1: TcxGridViewRepository
    Left = 392
    Top = 280
    object cxGridViewRepository1DBTableView1: TcxGridDBTableView
      DataController.DataSource = dmMat.dsPartner
      DataController.KeyFieldNames = 'TIP_PARTNER;ID'
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <>
      DataController.Summary.SummaryGroups = <>
      object cxGridViewRepository1DBTableView1TIP_PARTNER: TcxGridDBColumn
        DataBinding.FieldName = 'TIP_PARTNER'
        Visible = False
      end
      object cxGridViewRepository1DBTableView1ID: TcxGridDBColumn
        DataBinding.FieldName = 'ID'
        Visible = False
      end
      object cxGridViewRepository1DBTableView1NAZIV: TcxGridDBColumn
        DataBinding.FieldName = 'NAZIV'
        Width = 200
      end
      object cxGridViewRepository1DBTableView1MESTO_NAZIV: TcxGridDBColumn
        DataBinding.FieldName = 'MESTO_NAZIV'
        Width = 200
      end
    end
  end
end
