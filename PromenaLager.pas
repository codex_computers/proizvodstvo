unit PromenaLager;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ExtCtrls, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxContainer, cxEdit, dxSkinsCore,
  dxSkinBlack, dxSkinBlue, dxSkinBlueprint, dxSkinCaramel, dxSkinCoffee,
  dxSkinDarkRoom, dxSkinDarkSide, dxSkinDevExpressDarkStyle,
  dxSkinDevExpressStyle, dxSkinFoggy, dxSkinGlassOceans, dxSkinHighContrast,
  dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky,
  dxSkinMcSkin, dxSkinMetropolis, dxSkinMetropolisDark, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinOffice2013DarkGray,
  dxSkinOffice2013LightGray, dxSkinOffice2013White, dxSkinOffice2016Colorful,
  dxSkinOffice2016Dark, dxSkinPumpkin, dxSkinSeven, dxSkinSevenClassic,
  dxSkinSharp, dxSkinSharpPlus, dxSkinSilver, dxSkinSpringTime, dxSkinStardust,
  dxSkinSummer2008, dxSkinTheAsphaltWorld, dxSkinsDefaultPainters,
  dxSkinValentine, dxSkinVisualStudio2013Blue, dxSkinVisualStudio2013Dark,
  dxSkinVisualStudio2013Light, dxSkinVS2010, dxSkinWhiteprint,
  dxSkinXmas2008Blue, cxTextEdit, cxLabel, Vcl.Menus, System.Actions,
  Vcl.ActnList, Vcl.StdCtrls, cxButtons;

type
  TfrmPromenaLager = class(TForm)
    Panel1: TPanel;
    lblNaziv: TcxLabel;
    txtVlez: TcxTextEdit;
    txtIzlez: TcxTextEdit;
    cxLabel1: TcxLabel;
    cxLabel2: TcxLabel;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    ActionList1: TActionList;
    aZapisi: TAction;
    aOtkazi: TAction;
    procedure aZapisiExecute(Sender: TObject);
    procedure aOtkaziExecute(Sender: TObject);
  private
    { Private declarations }
  public
  id_kt_lager : integer;
    { Public declarations }
  end;

var
  frmPromenaLager: TfrmPromenaLager;

implementation

uses
  dmUnit, dmMaticni, dmResources;

{$R *.dfm}

procedure TfrmPromenaLager.aOtkaziExecute(Sender: TObject);
begin
    Close;
end;

procedure TfrmPromenaLager.aZapisiExecute(Sender: TObject);
begin

     dm.tblKTLager.Close;
     dm.tblKTLager.ParamByName('id').AsInteger := id_kt_lager;
     dm.tblKTLager.Open;

     dm.tblKTLager.Edit;
     dm.tblKTLagerKOL_VLEZ.AsString := txtVlez.Text;
     dm.tblKTLagerKOL_IZLEZ.AsString := txtIzlez.Text;
     dm.tblKTLager.Post;

     Close;
end;

end.
