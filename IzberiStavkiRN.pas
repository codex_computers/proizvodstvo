unit IzberiStavkiRN;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData, cxEdit,
  DB, cxDBData, cxGridCustomPopupMenu, cxGridPopupMenu,
  ComCtrls, cxContainer, cxTextEdit, cxDBEdit, StdCtrls, cxGridLevel,
  cxClasses, cxControls, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGrid, ExtCtrls, Menus, ActnList,
  cxGridExportLink, cxExport, cxLookAndFeelPainters, cxDataStorage,
  cxButtons, dxStatusBar, dxRibbonStatusBar, dxRibbon, dxBar, dxBarExtItems,
  dxPSGlbl, dxPSUtl, dxPSEngn, dxPrnPg, dxBkgnd, dxWrap, dxPrnDev,
  dxPSCompsProvider, dxPSFillPatterns, dxPSEdgePatterns, dxPSCore, dxPScxCommon,
  cxPC, cxMemo, cxLocalization, cxLookAndFeels,
  dxPSPDFExportCore, dxPSPDFExport, cxDrawTextUtils, dxPSPrVwStd, dxPSPrVwAdv,
  dxPSPrVwRibbon, dxPScxEditorProducers, dxPScxExtEditorProducers,
  dxPScxPageControlProducer, dxPgsDlg, cxCheckBox, cxBarEditItem, dxSkinsCore,
  dxSkinBlack, dxSkinBlue, dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom,
  dxSkinDarkSide, dxSkinFoggy, dxSkinGlassOceans, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven,
  dxSkinSharp, dxSkinSilver, dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008,
  dxSkinsDefaultPainters, dxSkinValentine, dxSkinXmas2008Blue,
  dxSkinsdxRibbonPainter, dxSkinsdxBarPainter, dxSkinscxPCPainter,
  dxBarSkinnedCustForm, cxDropDownEdit, cxCalendar, cxDBLookupComboBox,
  cxMaskEdit, cxLookupEdit, cxDBLookupEdit, cxDBExtLookupComboBox, cxGroupBox,
  cxRadioGroup, dxRibbonSkins, dxPScxGridLnk, dxPScxGridLayoutViewLnk,
  dxScreenTip, dxSkinBlueprint, dxSkinDevExpressDarkStyle,
  dxSkinDevExpressStyle, dxSkinHighContrast, dxSkinSevenClassic,
  dxSkinSharpPlus, dxSkinTheAsphaltWorld, dxSkinVS2010, dxSkinWhiteprint, dmResources,
  cxSplitter, FIBQuery, pFIBQuery, dxSkinOffice2013White, cxNavigator,
  System.Actions, dxSkinMetropolis, dxSkinMetropolisDark,
  dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray, dxRibbonCustomizationForm;

type
//  niza = Array[1..5] of Variant;

  TfrmIzberiStavkiRN = class(TForm)
    cxGridPopupMenu1: TcxGridPopupMenu;
    PopupMenu1: TPopupMenu;
    N1: TMenuItem;
    Excel1: TMenuItem;
    dxRibbon1Tab1: TdxRibbonTab;
    dxRibbon1: TdxRibbon;
    StatusBar1: TdxRibbonStatusBar;
    dxBarManager1: TdxBarManager;
    dxBarManager1Bar1: TdxBar;
    dxBarManager1Bar2: TdxBar;
    dxBarManager1Bar3: TdxBar;
    dxBarLargeButton2: TdxBarLargeButton;
    dxBarLargeButton3: TdxBarLargeButton;
    dxBarLargeButton4: TdxBarLargeButton;
    dxBarLargeButton5: TdxBarLargeButton;
    dxBarLargeButton6: TdxBarLargeButton;
    dxBarLargeButton7: TdxBarLargeButton;
    dxBarLargeButton8: TdxBarLargeButton;
    dxBarLargeButton1: TdxBarLargeButton;
    ActionList1: TActionList;
    aNov: TAction;
    aAzuriraj: TAction;
    aBrisi: TAction;
    aSnimiIzgled: TAction;
    aZacuvajExcel: TAction;
    aPecatiTabela: TAction;
    aHelp: TAction;
    aIzlez: TAction;
    aZapisi: TAction;
    aOtkazi: TAction;
    aRefresh: TAction;
    dxRibbon1Tab2: TdxRibbonTab;
    dxBarManager1Bar4: TdxBar;
    dxBarLargeButton9: TdxBarLargeButton;
    aSnimiPecatenje: TAction;
    dxComponentPrinter1: TdxComponentPrinter;
    dxComponentPrinter1Link1: TdxGridReportLink;
    dxBarLargeButton11: TdxBarLargeButton;
    ribbonFilterReset: TdxBarLargeButton;
    aPodesuvanjePecatenje: TAction;
    dxBarLargeButton12: TdxBarLargeButton;
    aBrisiPodesuvanjePecatenje: TAction;
    dxBarLargeButton13: TdxBarLargeButton;
    dxBarLargeButton14: TdxBarLargeButton;
    aPageSetup: TAction;
    dxBarLargeButton15: TdxBarLargeButton;
    dxBarLargeButton16: TdxBarLargeButton;
    dxBarLargeButton17: TdxBarLargeButton;
    cxBarEditItem1: TcxBarEditItem;
    dxBarSubItem1: TdxBarSubItem;
    aFormConfig: TAction;
    dxBarScreenTipRepository1: TdxBarScreenTipRepository;
    tipSnimiIzgled: TdxBarScreenTip;
    tipZacuvajExcel: TdxBarScreenTip;
    tipDizajnReport: TdxBarScreenTip;
    tipSetiranjeStrana: TdxBarScreenTip;
    tipSnimiKonf: TdxBarScreenTip;
    tipBrisiKonf: TdxBarScreenTip;
    dxBarManager1BarIzgledGrid: TdxBar;
    dxBarLBtnSnimiIzgled: TdxBarLargeButton;
    dxBarLBtnBrisiIzgled: TdxBarLargeButton;
    aBrisiIzgled: TAction;
    aSpustiSoberi: TAction;
    dxBarLargeButton10: TdxBarLargeButton;
    dPanel: TPanel;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    cxSplitter2: TcxSplitter;
    aKreirajRN: TAction;
    cxGrid1DBTableView1ID_RABOTEN_NALOG: TcxGridDBColumn;
    cxGrid1DBTableView1BROJ: TcxGridDBColumn;
    cxGrid1DBTableView1TP_PORACKA: TcxGridDBColumn;
    cxGrid1DBTableView1P_PORACKA: TcxGridDBColumn;
    cxGrid1DBTableView1NAZIV_PARTNER_PORACKA: TcxGridDBColumn;
    cxGrid1DBTableView1ID_RN_STAVKA: TcxGridDBColumn;
    cxGrid1DBTableView1VID_ARTIKAL: TcxGridDBColumn;
    cxGrid1DBTableView1ARTIKAL: TcxGridDBColumn;
    cxGrid1DBTableView1NAZIV_ARTIKAL: TcxGridDBColumn;
    cxGrid1DBTableView1KOLICINA: TcxGridDBColumn;
    cxGrid1DBTableView1MERKA: TcxGridDBColumn;
    cxGrid1DBTableView1ZABELESKA: TcxGridDBColumn;
    cxGrid1DBTableView1Izberi: TcxGridDBColumn;
    aSteStavki: TAction;
    cbSite: TcxBarEditItem;
    qRNStavka: TpFIBUpdateObject;
    cxGrid1DBTableView1LOT: TcxGridDBColumn;
    cxGrid1DBTableView1ID_PORACKA_STAVKA: TcxGridDBColumn;
    cxGrid1DBTableView1KOLICINA_OSTANATA: TcxGridDBColumn;
    qRNStavkaMain: TpFIBUpdateObject;
    pFIBUpdateObject1: TpFIBUpdateObject;
    cxGrid1DBTableView1SIFRA_PORACKA: TcxGridDBColumn;
    cxGrid1DBTableView1REF_NO: TcxGridDBColumn;
    cxGrid1DBTableView1SIFRA_PORACKA_STAVKA: TcxGridDBColumn;
    cxGrid1DBTableView1BROJ_PORACKA_STAVKA: TcxGridDBColumn;
    cxGrid1DBTableView1TAPACIR_SEDISTE: TcxGridDBColumn;
    cxGrid1DBTableView1TAPACIR_NAZAD: TcxGridDBColumn;
    cxGrid1DBTableView1SUNGER: TcxGridDBColumn;
    cxGrid1DBTableView1BOJA_NOGARKI: TcxGridDBColumn;
    cxGrid1DBTableView1LABELS: TcxGridDBColumn;
    cxGrid1DBTableView1GLIDERS: TcxGridDBColumn;
    cxGrid1DBTableView1KOLICINA_GOTOVA: TcxGridDBColumn;
    qPorPartner: TpFIBQuery;

    procedure cxDBTextEditAllEnter(Sender: TObject);
    procedure cxDBTextEditAllExit(Sender: TObject);
    procedure EnterKakoTab(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    //procedure PrvPosledenTab(panel:TPanel;var posledna,prva:TWinControl);
    procedure SaveToIniFileExecute(Sender: TObject);
    procedure cxGrid1DBTableView1KeyPress(Sender: TObject; var Key: Char);
    procedure aZapisiExecute(Sender: TObject);
    procedure aOtkaziExecute(Sender: TObject);
    procedure aNovExecute(Sender: TObject);
    procedure aAzurirajExecute(Sender: TObject);
    procedure aBrisiExecute(Sender: TObject);
    procedure aZacuvajExcelExecute(Sender: TObject);
    procedure aRefreshExecute(Sender: TObject);
    procedure aSnimiIzgledExecute(Sender: TObject);
    procedure aIzlezExecute(Sender: TObject);
    procedure aPecatiTabelaExecute(Sender: TObject);
    procedure aSnimiPecatenjeExecute(Sender: TObject);
    procedure zacuvajPrintVoIni(ime:AnsiString);
    procedure procitajPrintOdIni(ime:AnsiString);
    procedure aPodesuvanjePecatenjeExecute(Sender: TObject);
    procedure aBrisiPodesuvanjePecatenjeExecute(Sender: TObject);
    procedure aPageSetupExecute(Sender: TObject);
    procedure cxGrid1DBTableView1FilterCustomization(
      Sender: TcxCustomGridTableView; var ADone: Boolean);
    procedure aFormConfigExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure PartnerPropertiesChange(Sender: TObject);
    procedure aBrisiIzgledExecute(Sender: TObject);
    procedure aSpustiSoberiExecute(Sender: TObject);
    procedure aKreirajRNExecute(Sender: TObject);
    procedure aSteStavkiExecute(Sender: TObject);
    procedure site(kako:Boolean);
    procedure cbSitePropertiesChange(Sender: TObject);
  private
    { Private declarations }

    _sifra_kluc :array[1..10] of string;  // �������� ���������� �� ������ �� ������� �� ������������� ���

  protected
    list : TList;
    inserting, sobrano : boolean;
    prva, posledna :TWinControl;
    procedure prefrli;

  public
    { Public declarations }
    constructor Create(Owner : TComponent; insert : boolean = true);reintroduce; overload;

    procedure SetSifra(br: integer; s : string);
    function GetSifra(br: integer) : string;
    var godina : string;
    //������� �� ���������� ������������ _sifra_kluc
    property sifra_kluc[br :integer]: string read GetSifra write SetSifra;
  end;

var
  frmIzberiStavkiRN: TfrmIzberiStavkiRN;
  rData : TRepositoryData;

implementation

uses DaNe, dmKonekcija, Utils, FormConfig, NurkoRepository, dmMaticni, dmSystem,
  dmUnit;

{$R *.dfm}
//------------------------------------------------------------------------------

constructor TfrmIzberiStavkiRN.Create(Owner : TComponent; insert : boolean = true);
begin
inherited Create(Owner);
//	�������� ���� ������� � �������� �� ����������� ���������
//	insert = true -> ����� ���� ����� ���������� �� ������� �� Insert Mode
//  insert = false -> ����� ���� ����� ������� �� ����� �� ������. �� ������� ����� ����� �� �� �������� F5
  inserting := insert;
end;

//	����� �� �����������. �� ��������� ������� �� ������, �� �������� ������� �� ���� ��������,
//	�� ������� ������� �� ������ ������ � �� ������� �� Insert Mode
procedure TfrmIzberiStavkiRN.aNovExecute(Sender: TObject);
begin
//  if(cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) then
//  begin
//    dPanel.Enabled:=True;
//    lPanel.Enabled:=False;
//    prva.SetFocus;
//    cxGrid1DBTableView1.DataController.DataSet.Insert;
//  end
//  else ShowMessage('������ �������� �� ��� �������� �� �� ��������� �������/r/n � ����� ������� �� ������� ��� �����!');
end;

//	����� �� ���������. �� ��������� ������� �� ������, �� �������� ������� �� ���� ��������,
//	�� ������� ������� �� ������ ������ � �� ������� �� Edit Mode
procedure TfrmIzberiStavkiRN.aAzurirajExecute(Sender: TObject);
begin
//  if(cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) then
//  begin
//    dPanel.Enabled:=True;
//    lPanel.Enabled:=False;
//    prva.SetFocus;
//    cxGrid1DBTableView1.DataController.DataSet.Edit;
//  end
//  else ShowMessage('������ �������� �� ��� �������� �� �� ��������� �������/r/n � ����� ������� �� ������� ��� �����!');
end;

//	����� �� ������ �� ������������� �����
procedure TfrmIzberiStavkiRN.aBrisiExecute(Sender: TObject);
begin
//  if ((cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) and
//     (cxGrid1DBTableView1.DataController.RecordCount <> 0)) then
//    cxGrid1DBTableView1.DataController.DataSet.Delete();
end;

procedure TfrmIzberiStavkiRN.aBrisiIzgledExecute(Sender: TObject);
begin
  brisiGridVoBaza(Name,cxGrid1DBTableView1);
  //brisiPivotVoBaza(Name,cxDBPivotGrid1); �� pivot ��� ���
  BrisiFormaIzgled(self);
end;

//	����� �� ���������� �� ����������
procedure TfrmIzberiStavkiRN.aRefreshExecute(Sender: TObject);
begin
  cxGrid1DBTableView1.DataController.Filter.Root.Clear; //���������� �� ��������
  cxGrid1DBTableView1.DataController.DataSet.Refresh;
end;

//	����� �� ����� �� ������� �� ������� �� ������� �����
procedure TfrmIzberiStavkiRN.aIzlezExecute(Sender: TObject);
begin
  Close;
end;

procedure TfrmIzberiStavkiRN.aKreirajRNExecute(Sender: TObject);
var i : Integer;
begin
   // �� �� ������ ���� ����� �� ��
try
frmDaNe := TfrmDaNe.Create(self, '�������', '���� �������� ������ �� ��?', 1);
  if (frmDaNe.ShowModal = mrYes) then
     begin
          with cxGrid1DBTableView1.DataController do
              for I := 0 to RecordCount - 1 do
              begin
                if (DisplayTexts[i,cxGrid1DBTableView1Izberi.Index]= '�����') then
                begin
                    begin

                           //�������� ���� �� ���������� �� ������ ��, �� ����� �� ����������� � ������������� ��������

                           if Values[i,cxGrid1DBTableView1KOLICINA_OSTANATA.Index] > 0 then
                              begin
                              if (Values[i,cxGrid1DBTableView1KOLICINA_GOTOVA.Index]) > 0 then
                                begin
                                 //�� ����������� ������ �� ������ �����, �� �� ������ ��������� �� �������� �� ����������� ��������
                                     qRNStavkaMain.Close;
                                     qRNStavkaMain.ParamByName('id').Value := (Values[i,cxGrid1DBTableView1ID_RN_STAVKA.Index]);
                                     qRNStavkaMain.ParamByName('kolicina').Value := (Values[i,cxGrid1DBTableView1KOLICINA_GOTOVA.Index]);
                                     qRNStavkaMain.ExecQuery;

                                      //� �� �� ������� ���� ������, �� ��� �� ���� ������������� ����������, ���� ����������
                                     dm.tblRNStavka.Insert;
                                     dm.tblRNStavkaID_RABOTEN_NALOG.Value := (Values[i,cxGrid1DBTableView1ID_RABOTEN_NALOG.Index]);
                                     dm.tblRNStavkaVID_ARTIKAL.AsInteger := (Values[i,cxGrid1DBTableView1VID_ARTIKAL.Index]);
                                     dm.tblRNStavkaARTIKAL.AsInteger := (Values[i,cxGrid1DBTableView1ARTIKAL.Index]);
                                     dm.tblRNStavkaKOLICINA.Value := (Values[i,cxGrid1DBTableView1KOLICINA_OSTANATA.Index]);
                                     dm.tblRNStavkaMERKA.Value := (DisplayTexts[i,cxGrid1DBTableView1MERKA.Index]);
                                     dm.tblRNStavkaLOT.Value := (DisplayTexts[i,cxGrid1DBTableView1LOT.Index]);
                                     dm.tblRNStavkaSTATUS.Value := 3;
                                     dm.tblRNStavkaID_PORACKA_STAVKA.Value := (Values[i,cxGrid1DBTableView1ID_PORACKA_STAVKA.Index]);
                                     dm.tblRNStavkaZABELESKA.Value := (DisplayTexts[i,cxGrid1DBTableView1ZABELESKA.Index]);
                                     dm.tblRNStavka.Post;
                                end
                                else
                                begin
                                    qRNStavka.Close;
                                    qRNStavka.ParamByName('id').Value := (Values[i,cxGrid1DBTableView1ID_RN_STAVKA.Index]);
                                    qRNStavka.ParamByName('status').Value := 3;
                                    qRNStavka.ExecQuery;
                                end;

                                 //���� ������ �� ��������� ��, �� ������������� �������� (�������� �� ������)
                                     dm.tblRNStavka.Insert;
                                     dm.tblRNStavkaID_RABOTEN_NALOG.Value := dm.tblRabotenNalogID.value;//(Values[i,cxGrid1DBTableView1ID_RABOTEN_NALOG.Index]);
                                     dm.tblRNStavkaVID_ARTIKAL.AsInteger := (Values[i,cxGrid1DBTableView1VID_ARTIKAL.Index]);
                                     dm.tblRNStavkaARTIKAL.AsInteger := (Values[i,cxGrid1DBTableView1ARTIKAL.Index]);
                                     dm.tblRNStavkaKOLICINA.Value := (Values[i,cxGrid1DBTableView1KOLICINA_OSTANATA.Index]);
                                     dm.tblRNStavkaMERKA.Value := (DisplayTexts[i,cxGrid1DBTableView1MERKA.Index]);
                                     dm.tblRNStavkaID_RN_OLD.Value := (Values[i,cxGrid1DBTableView1ID_RABOTEN_NALOG.Index]);
                                     dm.tblRNStavkaLOT.Value := (DisplayTexts[i,cxGrid1DBTableView1LOT.Index]);
                                     dm.tblRNStavkaSTATUS.Value := 1;
                                     dm.tblRNStavkaID_PORACKA_STAVKA.Value := (Values[i,cxGrid1DBTableView1ID_PORACKA_STAVKA.Index]);
                                     dm.tblRNStavkaZABELESKA.Value := (DisplayTexts[i,cxGrid1DBTableView1ZABELESKA.Index]);
                                     dm.tblRNStavka.Post;

                                 //
//
                              end;
                    end;

                end;
              end;
              ShowMessage('�������� �� �������� �� ��!');
              dm.tblIzberiStavkaRN.CloseOpen(true);
     end
     else
       abort;

//   dm.pPorackaRN.Close;
//   dm.pPorackaRN.ParamByName('id_firma').AsString := dmKon.firma;
//   dm.pPorackaRN.ParamByName('godina').AsString := godina;
//   dm.pPorackaRN.ParamByName('id_poracka').
//   dm.pPorackaRN.ParamByName('id_rn').asinteger := dm.tblRabotenNalogID.Value;
//   dm.pPorackaRN.ExecProc;


   //dm.tblRabotenNalog.AutoUpdateOptions.WhenGetGenID(2);

   //����� �� ��������� �� �� ������ ��, �� �� �� ������ ��������� �������
   finally

end;
end;

//	����� �� ������� �� �������� �� ������ �� ���� (Utils.pas)
procedure TfrmIzberiStavkiRN.aSnimiIzgledExecute(Sender: TObject);
begin
  zacuvajGridVoBaza(Name,cxGrid1DBTableView1);
  ZacuvajFormaIzgled(self);
end;

//	����� �� ������� �� ������ �� Excel ������ (Utils.pas)
procedure TfrmIzberiStavkiRN.aZacuvajExcelExecute(Sender: TObject);
begin
  zacuvajVoExcel(cxGrid1, Caption);
end;

//  ��������� �� ������ �� ������� �� ���������� �� ����� �� Enter
procedure TfrmIzberiStavkiRN.EnterKakoTab(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  kom : TWinControl;
begin
    kom := Sender as TWinControl;
    case Key of
        VK_DOWN:
        begin
          PostMessage(Handle,WM_NextDlgCtl,0,0);
          key:=0;
        end;
        VK_UP:
        begin
          PostMessage(Handle,WM_NextDlgCtl,1,0);
          key:=0;
        end;
        VK_RETURN:
        begin
          PostMessage(Handle,WM_NextDlgCtl,0,0);
        end;
        VK_INSERT:
        begin
          //  �� ����� ��������
//          if (kom = cxExtLookupComboBox1)  then
//          begin
//            frmNurkoRepository := TfrmNurkoRepository.Create(nil,Name,rData);
//            frmNurkoRepository.kontrola_naziv := kom.Name;
//            frmNurkoRepository.ShowModal;
//
//            if (frmNurkoRepository.ModalResult = mrOk) then
//              cxExtLookupComboBox1.EditValue := frmNurkoRepository.cxGrid1Level1.GridView.DataController.GetRecordId(frmNurkoRepository.cxGrid1Level1.GridView.DataController.FocusedRecordIndex);
//
//            frmNurkoRepository.Free;
//       	 end;
	end;
    end;
end;

//	������� �� ��� �� ������ ��� �������� �� �����
procedure TfrmIzberiStavkiRN.cxDBTextEditAllEnter(Sender: TObject);
begin
    TEdit(Sender).Color:=clSkyBlue;
end;

//	������� �� ��� �� ������ ��� ����� �� �����
procedure TfrmIzberiStavkiRN.cxDBTextEditAllExit(Sender: TObject);
begin
    TEdit(Sender).Color:=clWhite;
end;

procedure TfrmIzberiStavkiRN.cxGrid1DBTableView1FilterCustomization(
  Sender: TcxCustomGridTableView; var ADone: Boolean);
begin
end;

//  ��������� �� ������ �� ��������� �� ���������� ���� �� ��������
//  ��������� ��: br - ����� ��� �� ������ (�� 0 �� 9) �� ��������� �� ����� �� ����� �� ���������� ����
//  s - ��������� �� ������ �� ������ (���������� �� string �� ����� �� integer �� IntToStr ���������)
procedure TfrmIzberiStavkiRN.SetSifra(br: integer; s : string);
begin
  _sifra_kluc[br] := s;
end;

//  ������� �� ������ �� ��������� �� ���������� ���� �� ��������
//  ��������� ��: br - ����� ��� �� ������ (�� 0 �� 9) �� ��������� �� ����� �� ����� �� ���������� ����
function TfrmIzberiStavkiRN.GetSifra(br: integer) : string;
begin
  Result := _sifra_kluc[br];
end;

procedure TfrmIzberiStavkiRN.PartnerPropertiesChange(Sender: TObject);
begin

end;

//  �� ������� �� ������� ��� ����������� �� Master-�� �� �������� �� ��������� �� ������ ����� SetSifra
procedure TfrmIzberiStavkiRN.prefrli;
begin
end;

procedure TfrmIzberiStavkiRN.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    //	��������� ��� ��������� �� �������
//    if (cxGrid1DBTableView1.DataController.DataSource.State = dsEdit) or (cxGrid1DBTableView1.DataController.DataSource.State = dsInsert) then
//    begin
//        frmDaNe := TfrmDaNe.Create(self, '���������� ��������', '���������� �� �� ��������. ���� ������ �� �� �������?', 1);
//        if (frmDaNe.ShowModal <> mrYes) then
//        begin
//            cxGrid1DBTableView1.DataController.DataSet.Cancel;
//            Action := caFree;
//        end
//        else
//          if (Validacija(dPanel) = false) then
//          begin
//            cxGrid1DBTableView1.DataController.DataSet.Post;
            Action := caFree;
//          end
//          else
//   Action := caNone;
//    end;
end;
procedure TfrmIzberiStavkiRN.FormCreate(Sender: TObject);
begin
  ProcitajFormaIzgled(self);
  if (dmRes.use_skin = true) then dxRibbon1.ColorSchemeName := dmRes.skin_name;

  rData := TRepositoryData.Create();  // kreirame instance od klasata TRepositoryData sto se naogja vo dmRes
end;

//------------------------------------------------------------------------------

procedure TfrmIzberiStavkiRN.FormShow(Sender: TObject);
begin
  //  ������� �� ������������ ������������/���������� �� ���������� �� ���� �����
    SpremiForma(self);
  //	�� ��������� �� ������� ������ ��� ���������� � ���� � ��� �������� �� �������
//    PrvPosledenTab(dPanel,posledna,prva);
    dxBarManager1Bar1.Caption := Caption;
    dxComponentPrinter1Link1.ReportTitle.Text := Caption;

  //	������� �� ������������ �� ������ �� ������
    procitajGridOdBaza(Name,cxGrid1DBTableView1,false,false);
  //	������� �� ������������ �� ��������� �� ������
    procitajPrintOdBaza(Name,cxGrid1DBTableView1.Name, dxComponentPrinter1Link1);
    sobrano := true;

    qPorPartner.Close;
    qPorPartner.ParamByName('id_rn').Value := dm.tblRabotenNalogID.Value;
    qPorPartner.ExecQuery;

    dm.tblIzberiStavkaRN.Close;
    dm.tblIzberiStavkaRN.ParamByName('id_kt').Value := MGP;
    dm.tblIzberiStavkaRN.ParamByName('id_rn').Value := dm.tblRabotenNalogID.Value;
//    dm.tblIzberiStavkaRN.ParamByName('p').Value := dm.tblRabotenNalogP_PORACKA.Value;
  //  dm.tblIzberiStavkaRN.ParamByName('id').asstring := '%';
    dm.tblIzberiStavkaRN.Open;

   // dm.tblIzberiPStavki.Open;
    //dm.tblIzberiPElement.Open;

//  �������� �� ����� �� ������ �� cxExtLookupComboBox ��� cxDBExtLookupComboBox ��������
    //cxExtLookupComboBox1.RepositoryItem := dmRes.InitRepository(23, 'cxExtLookupComboBox1', Name, rData);
  	//procitajGridOdBaza(Name + '_' + IntToStr(rData.nurkoRepository[rData.br_repository]), rData.gridRepository[rData.br_repository] ,false,false);
end;
//------------------------------------------------------------------------------

procedure TfrmIzberiStavkiRN.SaveToIniFileExecute(Sender: TObject);
begin
    cxGrid1DBTableView1.StoreToIniFile(Name,True,[]);
end;
//------------------------------------------------------------------------------

procedure TfrmIzberiStavkiRN.cxGrid1DBTableView1KeyPress(Sender: TObject;
  var Key: Char);
begin
  if(Ord(Key) <> VK_RETURN) then cxSortiraj(cxGrid1DBTableView1);
end;

//  ����� �� �����
procedure TfrmIzberiStavkiRN.aZapisiExecute(Sender: TObject);
var
  st: TDataSetState;
begin
//  ZapisiButton.SetFocus;

//  st := cxGrid1DBTableView1.DataController.DataSet.State;
//  if st in [dsEdit,dsInsert] then
//  begin
//    if (Validacija(dPanel) = false) then
//    begin
//      if ((st = dsInsert) and inserting) then
//      begin
//        cxGrid1DBTableView1.DataController.DataSet.Post;
//        aNov.Execute;
//      end;
//
//      if ((st = dsInsert) and (not inserting)) then
//      begin
//        cxGrid1DBTableView1.DataController.DataSet.Post;
//        dPanel.Enabled:=false;
//        lPanel.Enabled:=true;
//        cxGrid1.SetFocus;
//      end;
//
//      if (st = dsEdit) then
//      begin
//        cxGrid1DBTableView1.DataController.DataSet.Post;
//        dPanel.Enabled:=false;
//        lPanel.Enabled:=true;
//        cxGrid1.SetFocus;
//      end;
//    end;
//  end;
end;

procedure TfrmIzberiStavkiRN.cbSitePropertiesChange(Sender: TObject);
begin
     aSteStavki.Execute;
end;

//	����� �� ���������� �� �������
procedure TfrmIzberiStavkiRN.aOtkaziExecute(Sender: TObject);
begin
  if (cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) then
  begin
      ModalResult := mrCancel;
      Close();
  end
//  else
//  begin
//      cxGrid1DBTableView1.DataController.DataSet.Cancel;
//      RestoreControls(dPanel);
//      dPanel.Enabled := false;
//      lPanel.Enabled := true;
//      cxGrid1.SetFocus;
//  end;
end;

//----------------------------------------------------------------------------------
// ��������� �� ��������, ���������� �� ��������� � �������� �� Print ��������
//----------------------------------------------------------------------------------

//	����� �� �������� �� �������� �� �������
procedure TfrmIzberiStavkiRN.aPageSetupExecute(Sender: TObject);
begin
  dxComponentPrinter1Link1.PageSetup;
end;

//	����� �� ������� �� ������
procedure TfrmIzberiStavkiRN.aPecatiTabelaExecute(Sender: TObject);
begin
  dxComponentPrinter1Link1.ReportTitle.Text := Caption;

  dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Clear;
  dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Add(dmkon.firma_naziv);
  dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Add(DateTimeToStr(Now));

  dxComponentPrinter1Link1.PrinterPage.PageHeader.LeftTitle.Clear;
  //dxComponentPrinter1Link1.PrinterPage.PageHeader.LeftTitle.Add('������� : ' + TipSifra.Text + '-' + IdSifra.Text + ' ' + Partner.Text);

  dxComponentPrinter1.Preview(true, dxComponentPrinter1Link1);
end;

//	����� �� ���������� �� ��������� �� �������
procedure TfrmIzberiStavkiRN.aPodesuvanjePecatenjeExecute(Sender: TObject);
begin
  dxComponentPrinter1Link1.DesignReport();
end;

procedure TfrmIzberiStavkiRN.aSnimiPecatenjeExecute(Sender: TObject);
begin
  zacuvajPrintVoBaza(Name,cxGrid1DBTableView1.Name,dxComponentPrinter1Link1);
end;

//	����� �� �������� ��� �������� �� �������� �� ������ ���� ��� ��������� �� ����� ������
procedure TfrmIzberiStavkiRN.aSpustiSoberiExecute(Sender: TObject);
begin
  if (sobrano = true) then
  begin
    cxGrid1DBTableView1.ViewData.Expand(false);
    sobrano := false;
  end
  else
  begin
    cxGrid1DBTableView1.ViewData.Collapse(false);
    sobrano := true;
  end;
end;

procedure TfrmIzberiStavkiRN.aSteStavkiExecute(Sender: TObject);
begin
  if cbSite.EditValue then
  begin
    site(true);
    //cxGrid1DBTableView1IzberiPropertiesChange(Sender);
  end
  else
  begin
    site(false);
    //cxTxtVkZadolzuvanje.Text := '';
  end;
end;

//	����� �� ������ �� ����������� �������� �� ������� (Utils.pas)
procedure TfrmIzberiStavkiRN.aBrisiPodesuvanjePecatenjeExecute(Sender: TObject);
begin
  brisiPrintOdBaza(Name,cxGrid1DBTableView1.Name, dxComponentPrinter1Link1);
end;

//  ����� �� ����� �� ����� �� ������������� �� �������� � ��������������� ��
//  �������(������ ���������� ��������)
procedure TfrmIzberiStavkiRN.aFormConfigExecute(Sender: TObject);
begin
  frmFormConfig := TfrmFormConfig.Create(Application);
  frmFormConfig.formPtr := Addr(Self);
  frmFormConfig.ShowModal;
  frmFormConfig.Free;
end;

//----------------------------------------------------------------------------------
// ���� -> ��������� �� ��������, ���������� �� ��������� � �������� �� Print ��������
//----------------------------------------------------------------------------------

// ��������� �� ���������� �� ��������� � �������� �� Print �������� �� .ini ��������
procedure TfrmIzberiStavkiRN.zacuvajPrintVoIni(ime:AnsiString);
var
  AStream: TFileStream;
begin
  AStream := TFileStream.Create(dmKon.RabotenDir + ime + 'Print.ini', fmCreate);
  try
    AStream.WriteComponent(dxComponentPrinter1Link1);
  finally
    AStream.Free;
  end;

end;

// ��������� �� ������ �� ��������� � �������� �� Print �������� �������� �� .ini ��������
procedure TfrmIzberiStavkiRN.procitajPrintOdIni(ime:AnsiString);
var
  AStream: TFileStream;
begin
  AStream := TFileStream.Create(dmKon.RabotenDir + ime + 'Print.ini', fmOpenRead);
  try
    dxComponentPrinter1Link1.RestoreDefaults;
    AStream.ReadComponent(dxComponentPrinter1Link1);
  finally
    AStream.Free;
  end;
end;

procedure TfrmIzberiStavkiRN.site(kako:Boolean);   //procedura so koa gi stikliram - otstikliram site
var    suma, I:INTEGER;
begin
        with cxGrid1DBTableView1.DataController do
        for I := 0 to FilteredRecordCount - 1 do
        begin
          Values[FilteredRecordIndex[i] ,  cxGrid1DBTableView1Izberi.Index]:=  kako;
        end;
end;

end.
