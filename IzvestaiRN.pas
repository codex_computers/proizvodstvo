unit IzvestaiRN;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData, cxEdit,
  DB, cxDBData, cxGridCustomPopupMenu, cxGridPopupMenu,
  ComCtrls, cxContainer, cxTextEdit, cxDBEdit, StdCtrls, cxGridLevel,
  cxClasses, cxControls, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGrid, ExtCtrls, Menus, ActnList,
  cxGridExportLink, cxExport, cxLookAndFeelPainters, cxDataStorage,
  cxButtons, dxStatusBar, dxRibbonStatusBar, dxRibbon, dxBar, dxBarExtItems,
  dxPSGlbl, dxPSUtl, dxPSEngn, dxPrnPg, dxBkgnd, dxWrap, dxPrnDev,
  dxPSCompsProvider, dxPSFillPatterns, dxPSEdgePatterns, dxPSCore, dxPScxCommon,
  cxPC, cxMemo, cxLocalization, cxLookAndFeels,
  dxPSPDFExportCore, dxPSPDFExport, cxDrawTextUtils, dxPSPrVwStd, dxPSPrVwAdv,
  dxPSPrVwRibbon, dxPScxEditorProducers, dxPScxExtEditorProducers,
  dxPScxPageControlProducer, dxPgsDlg, cxCheckBox, cxBarEditItem, dxSkinsCore,
  dxSkinBlack, dxSkinBlue, dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom,
  dxSkinDarkSide, dxSkinFoggy, dxSkinGlassOceans, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven,
  dxSkinSharp, dxSkinSilver, dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008,
  dxSkinsDefaultPainters, dxSkinValentine, dxSkinXmas2008Blue,
  dxSkinsdxRibbonPainter, dxSkinsdxBarPainter, dxSkinscxPCPainter,
  dxBarSkinnedCustForm, cxDropDownEdit, cxCalendar, cxDBLookupComboBox,
  cxMaskEdit, cxLookupEdit, cxDBLookupEdit, cxDBExtLookupComboBox, cxGroupBox,
  cxRadioGroup, dxRibbonSkins, dxPScxGridLnk, dxPScxGridLayoutViewLnk,
  dxScreenTip, dxSkinBlueprint, dxSkinDevExpressDarkStyle,
  dxSkinDevExpressStyle, dxSkinHighContrast, dxSkinSevenClassic,
  dxSkinSharpPlus, dxSkinTheAsphaltWorld, dxSkinVS2010, dxSkinWhiteprint,
  dxSkinsdxNavBarPainter, cxPCdxBarPopupMenu, dxNavBar, dxNavBarCollns,
  dxNavBarBase, cxLabel, dxSkinsdxStatusBarPainter, cxColorComboBox,
  cxDBColorComboBox, cxDBLabel, cxButtonEdit, Vcl.ExtDlgs, cxImage,
  Vcl.Samples.Gauges, cxScheduler, cxSchedulerStorage,
  cxSchedulerCustomControls, cxSchedulerCustomResourceView, cxSchedulerDayView,
  cxSchedulerDateNavigator, cxSchedulerHolidays, cxSchedulerTimeGridView,
  cxSchedulerUtils, cxSchedulerWeekView, cxSchedulerYearView,
  cxSchedulerGanttView, dxSkinscxSchedulerPainter, cxSchedulercxGridConnection,
  cxSchedulerDBStorage, cxImageComboBox, Vcl.ImgList, dxPScxSchedulerLnk,
  cxPropertiesStore, dxLayoutContainer, cxGridLayoutView, cxGridDBLayoutView,
  cxGridCustomLayoutView, cxGridCardView, cxGridDBCardView,
  cxGridBandedTableView, cxGridDBBandedTableView, FIBDataSet, pFIBDataSet, DateUtils,
  dxBarExtDBItems, cxNavigator, cxDBNavigator, FIBQuery, pFIBQuery, cxBlobEdit,
  dxSkinOffice2013White, cxSchedulerTreeListBrowser, System.Actions,
  dxSkinMetropolis, dxSkinMetropolisDark, dxSkinOffice2013DarkGray,
  dxSkinOffice2013LightGray, dxSkinOffice2016Colorful, dxSkinOffice2016Dark,
  dxSkinVisualStudio2013Blue, dxSkinVisualStudio2013Dark,
  dxSkinVisualStudio2013Light, dxRibbonCustomizationForm, dxBarBuiltInMenu,
  cxSchedulerAgendaView, cxSchedulerRecurrence,
  cxSchedulerRibbonStyleEventEditor;

type
//  niza = Array[1..5] of Variant;

  TfrmIzvestaiRN = class(TForm)
    cxGridPopupMenu1: TcxGridPopupMenu;
    PopupMenu1: TPopupMenu;
    dxRibbon1Tab1: TdxRibbonTab;
    dxRibbon1: TdxRibbon;
    StatusBar1: TdxRibbonStatusBar;
    dxBarManager1: TdxBarManager;
    bmRabotenNalog: TdxBar;
    dxBarManager1Bar2: TdxBar;
    dxBarManager1Bar3: TdxBar;
    dxBarLargeButton2: TdxBarLargeButton;
    dxBarLargeButton3: TdxBarLargeButton;
    dxBarLargeButton4: TdxBarLargeButton;
    dxBarLargeButton5: TdxBarLargeButton;
    dxBarLargeButton6: TdxBarLargeButton;
    dxBarLargeButton7: TdxBarLargeButton;
    dxBarLargeButton8: TdxBarLargeButton;
    dxBarLargeButton1: TdxBarLargeButton;
    ActionList1: TActionList;
    aSnimiIzgled: TAction;
    aZacuvajExcel: TAction;
    aPecatiTabela: TAction;
    aHelp: TAction;
    aIzlez: TAction;
    aZapisi: TAction;
    aOtkazi: TAction;
    aRefresh: TAction;
    dxRibbon1Tab2: TdxRibbonTab;
    dxBarManager1Bar4: TdxBar;
    dxBarLargeButton9: TdxBarLargeButton;
    aSnimiPecatenje: TAction;
    dxComponentPrinter1: TdxComponentPrinter;
    dxComponentPrinter1Link1: TdxGridReportLink;
    dxBarLargeButton11: TdxBarLargeButton;
    ribbonFilterReset: TdxBarLargeButton;
    aPodesuvanjePecatenje: TAction;
    dxBarLargeButton12: TdxBarLargeButton;
    aBrisiPodesuvanjePecatenje: TAction;
    dxBarLargeButton13: TdxBarLargeButton;
    dxBarLargeButton14: TdxBarLargeButton;
    aPageSetup: TAction;
    dxBarLargeButton15: TdxBarLargeButton;
    dxBarLargeButton16: TdxBarLargeButton;
    dxBarLargeButton17: TdxBarLargeButton;
    cxBarEditItem1: TcxBarEditItem;
    dxBarSubItem1: TdxBarSubItem;
    aFormConfig: TAction;
    dxBarScreenTipRepository1: TdxBarScreenTipRepository;
    tipSnimiIzgled: TdxBarScreenTip;
    tipZacuvajExcel: TdxBarScreenTip;
    tipDizajnReport: TdxBarScreenTip;
    tipSetiranjeStrana: TdxBarScreenTip;
    tipSnimiKonf: TdxBarScreenTip;
    tipBrisiKonf: TdxBarScreenTip;
    dxBarManager1BarIzgledGrid: TdxBar;
    dxBarLBtnSnimiIzgled: TdxBarLargeButton;
    dxBarLBtnBrisiIzgled: TdxBarLargeButton;
    aBrisiIzgled: TAction;
    cxPageControl1: TcxPageControl;
    tsListaRN: TcxTabSheet;
    tsRN: TcxTabSheet;
    tsStavki: TcxTabSheet;
    lPanel: TPanel;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    dPanel: TPanel;
    cxGrid1DBTableView1ID: TcxGridDBColumn;
    cxGrid1DBTableView1BROJ: TcxGridDBColumn;
    cxGrid1DBTableView1ID_PORACKA: TcxGridDBColumn;
    cxGrid1DBTableView1ID_RE: TcxGridDBColumn;
    cxGrid1DBTableView1DATUM_PLANIRAN_POCETOK: TcxGridDBColumn;
    cxGrid1DBTableView1DATUM_START: TcxGridDBColumn;
    cxGrid1DBTableView1SOURCE_DOKUMENT: TcxGridDBColumn;
    cxGrid1DBTableView1STATUS: TcxGridDBColumn;
    cxGrid1DBTableView1NAZIV_STATUS: TcxGridDBColumn;
    cxGrid1DBTableView1ID_RE_SUROVINI: TcxGridDBColumn;
    cxGrid1DBTableView1NAZIV_MAGACIN_SUROVINI: TcxGridDBColumn;
    cxGrid1DBTableView1ID_RE_GOTOV_PROIZVOD: TcxGridDBColumn;
    cxGrid1DBTableView1NAZIV_MAGACIN_GOTOVI_PROIZVODI: TcxGridDBColumn;
    cxGrid1DBTableView1PRIORITET: TcxGridDBColumn;
    cxGrid1DBTableView1DATUM_PLANIRAN_KRAJ: TcxGridDBColumn;
    cxGrid1DBTableView1DATUM_KRAJ: TcxGridDBColumn;
    cxGrid1DBTableView1BR_CASOVI: TcxGridDBColumn;
    cxGrid1DBTableView1BR_CIKLUSI: TcxGridDBColumn;
    dPanelO: TPanel;
    tsNormativ: TcxTabSheet;
    gbRabotenNalog: TcxGroupBox;
    cxLabel15: TcxLabel;
    txtBrCiklusi: TcxDBTextEdit;
    lPanelO: TPanel;
    aListaRN: TAction;
    cxGroupBox3: TcxGroupBox;
    cxGrid2: TcxGrid;
    cxGrid2DBTableView1: TcxGridDBTableView;
    cxGrid2Level1: TcxGridLevel;
    ���: TcxLabel;
    txtBroj: TcxDBTextEdit;
    cxLabel2: TcxLabel;
    txtRE: TcxDBTextEdit;
    cxLabel1: TcxLabel;
    txtPoracka: TcxDBTextEdit;
    cxLabel12: TcxLabel;
    cbRE: TcxDBLookupComboBox;
    cxGrid3: TcxGrid;
    cxGrid3DBTableView1: TcxGridDBTableView;
    cxGrid3DBTableView1ID: TcxGridDBColumn;
    cxGrid3DBTableView1BROJ: TcxGridDBColumn;
    cxGrid3DBTableView1ID_RABOTEN_NALOG: TcxGridDBColumn;
    cxGrid3DBTableView1VID_ARTIKAL: TcxGridDBColumn;
    cxGrid3DBTableView1ARTIKAL: TcxGridDBColumn;
    cxGrid3DBTableView1SLIKA: TcxGridDBColumn;
    cxGrid3DBTableView1OD_DATUM_PLANIRAN: TcxGridDBColumn;
    cxGrid3DBTableView1DO_DATUM_PLANIRAN: TcxGridDBColumn;
    cxGrid3DBTableView1OD_DATUM_REALIZACIJA: TcxGridDBColumn;
    cxGrid3DBTableView1DO_DATUM_REALIZACIJA: TcxGridDBColumn;
    cxGrid3DBTableView1KOLICINA: TcxGridDBColumn;
    cxGrid3DBTableView1MERKA: TcxGridDBColumn;
    cxGrid3DBTableView1ID_PORACKA: TcxGridDBColumn;
    cxGrid3DBTableView1DATUM_OD_ISPORAKA: TcxGridDBColumn;
    cxGrid3DBTableView1DATUM_DO_ISPORAKA: TcxGridDBColumn;
    cxGrid3DBTableView1ZABELESKA: TcxGridDBColumn;
    cxGrid3DBTableView1KRAEN_DATUM_PREVOZ: TcxGridDBColumn;
    cxGrid3DBTableView1PRIORITET: TcxGridDBColumn;
    cxGrid3DBTableView1BROJ_CIKLUSI: TcxGridDBColumn;
    cxGrid3DBTableView1BROJ_CASOVI: TcxGridDBColumn;
    cxGrid3DBTableView1CUSTOM1: TcxGridDBColumn;
    cxGrid3DBTableView1CUSTOM2: TcxGridDBColumn;
    cxGrid3DBTableView1CUSTOM3: TcxGridDBColumn;
    cxGrid3Level1: TcxGridLevel;
    cxGrid3DBTableView1NAZIV_ARTIKAL: TcxGridDBColumn;
    cxGrid3DBTableView1NAZIV_MERKA: TcxGridDBColumn;
    cxGrid1DBTableView1GODINA: TcxGridDBColumn;
    cxGrid2DBTableView1VID_ARTIKAL_N: TcxGridDBColumn;
    cxGrid2DBTableView1ARTIKAL_N: TcxGridDBColumn;
    cxGrid2DBTableView1NAZIV_ARTIKAL: TcxGridDBColumn;
    cxGrid2DBTableView1KOLICINA: TcxGridDBColumn;
    cxGrid2DBTableView1MERKA: TcxGridDBColumn;
    cxGrid2DBTableView1AKTIVEN: TcxGridDBColumn;
    cxGroupBox8: TcxGroupBox;
    cxLabel16: TcxLabel;
    cxLabel17: TcxLabel;
    txtRealDatumPoc: TcxDBDateEdit;
    txtRealDatumKraj: TcxDBDateEdit;
    OpenPictureDialog1: TOpenPictureDialog;
    bmStavki: TdxBar;
    dxBarLargeButton10: TdxBarLargeButton;
    dxBarLargeButton18: TdxBarLargeButton;
    dxBarLargeButton19: TdxBarLargeButton;
    aPogledRN: TAction;
    Panel1: TPanel;
    cxDBImageComboBox1: TcxDBImageComboBox;
    tsOperacii: TcxTabSheet;
    cxGroupBox2: TcxGroupBox;
    cxGrid5: TcxGrid;
    cxGridDBTableView2: TcxGridDBTableView;
    cxGridLevel2: TcxGridLevel;
    cxGridDBTableView2OPERACIJA_N: TcxGridDBColumn;
    cxGridDBTableView2NAZIV_OPERACIJA: TcxGridDBColumn;
    cxGridDBTableView2POTREBNO_VREME: TcxGridDBColumn;
    cxGridDBTableView2MERKA: TcxGridDBColumn;
    cxGridDBTableView2AKTIVEN: TcxGridDBColumn;
    cxGridDBTableView2SEKVENCA: TcxGridDBColumn;
    gbRNStavki: TcxGroupBox;
    lblCustom3: TLabel;
    cxLabel8: TcxLabel;
    txtVidArtikal: TcxDBTextEdit;
    txtArtikal: TcxDBTextEdit;
    cxLabel9: TcxLabel;
    txtKolicinaArtikal: TcxDBTextEdit;
    txtCustom3: TcxDBTextEdit;
    Button1: TButton;
    cxLabel22: TcxLabel;
    txtPorackaS: TcxDBTextEdit;
    cbPorackaS: TcxDBLookupComboBox;
    txtKraenDatum: TcxDBDateEdit;
    cxLabel23: TcxLabel;
    txtBrCiklusiS: TcxDBTextEdit;
    cxLabel24: TcxLabel;
    cxLabel25: TcxLabel;
    txtBrCasoviS: TcxDBTextEdit;
    cxLabel28: TcxLabel;
    txtStatusS: TcxDBTextEdit;
    cbStatusS: TcxDBLookupComboBox;
    aSiteRN: TAction;
    aOtvoreniRN: TAction;
    dxBarLargeButton20: TdxBarLargeButton;
    dxBarLargeButton21: TdxBarLargeButton;
    cxGrid3Level2: TcxGridLevel;
    cxGrid3Level3: TcxGridLevel;
    cxGrid3DBTableView2: TcxGridDBTableView;
    cxGrid3DBTableView3: TcxGridDBTableView;
    cxGrid3DBTableView3OPERACIJA_N: TcxGridDBColumn;
    cxGrid3DBTableView3NAZIV_OPERACIJA: TcxGridDBColumn;
    cxGrid3DBTableView3POTREBNO_VREME: TcxGridDBColumn;
    cxGrid3DBTableView3MERKA: TcxGridDBColumn;
    cxGrid3DBTableView3SEKVENCA: TcxGridDBColumn;
    cxGrid3DBTableView2VID_ARTIKAL_N: TcxGridDBColumn;
    cxGrid3DBTableView2ARTIKAL_N: TcxGridDBColumn;
    cxGrid3DBTableView2NAZIV_ARTIKAL: TcxGridDBColumn;
    cxGrid3DBTableView2KOLICINA: TcxGridDBColumn;
    cxGrid3DBTableView2MERKA: TcxGridDBColumn;
    cxTabSheet1: TcxTabSheet;
    Panel3: TPanel;
    cxSchedulerOtsustva: TcxScheduler;
    cxGrid6: TcxGrid;
    cxGridDBTableView3: TcxGridDBTableView;
    cxGrid2DBTableView1BOJA: TcxGridDBColumn;
    cxGrid2DBTableView1NAZIV: TcxGridDBColumn;
    cxGrid2DBTableView1DENOVI: TcxGridDBColumn;
    cxGrid2DBTableView1PlatenoNaziv: TcxGridDBColumn;
    cxGrid2DBTableView1DogovorNaziv: TcxGridDBColumn;
    cxGridLevel3: TcxGridLevel;
    cxGrid1DBTableView1Column1: TcxGridDBColumn;
    cxGrid1DBTableView1STATUS_STAVKI: TcxGridDBColumn;
    cxGrid1DBTableView1NAZIV_STATUS_STAVKI: TcxGridDBColumn;
    aSmeniStatus: TAction;
    gbSurovini: TcxGroupBox;
    cxGrid7: TcxGrid;
    cxGridDBTableView4: TcxGridDBTableView;
    cxGridLevel4: TcxGridLevel;
    cxGridDBTableView4ID: TcxGridDBColumn;
    cxGridDBTableView4VID_SUROVINA: TcxGridDBColumn;
    cxGridDBTableView4NAZIV_SUROVINA: TcxGridDBColumn;
    cxGridDBTableView4SUROVINA: TcxGridDBColumn;
    cxGridDBTableView4KOLICINA: TcxGridDBColumn;
    cxGridDBTableView4NAZIV_MERKA: TcxGridDBColumn;
    cxGridDBTableView4PRODUKCISKI_LOT: TcxGridDBColumn;
    dxBarManager1Bar6: TdxBar;
    dxBarDateCombo1: TdxBarDateCombo;
    cbGodina: TcxBarEditItem;
    txtIzvorenDok: TcxDBTextEdit;
    cxLabel37: TcxLabel;
    txtTipOdobril: TcxDBTextEdit;
    txtOdobril: TcxDBTextEdit;
    cbOdobril: TcxLookupComboBox;
    cxGrid1DBTableView1NAZIV_RE: TcxGridDBColumn;
    aRNMasinsko: TAction;
    cxBarEditItem2: TcxBarEditItem;
    cbNovRE: TdxBarLookupCombo;
    cxBarEditItem3: TcxBarEditItem;
    cxGrid1DBTableView1TP_PORACKA: TcxGridDBColumn;
    cxGrid1DBTableView1P_PORACKA: TcxGridDBColumn;
    cxGrid1DBTableView1NAZIV_PARTNER_PORACKA: TcxGridDBColumn;
    cxGrid3DBTableView1TIP_ODOBRIL: TcxGridDBColumn;
    cxGrid3DBTableView1ODOBRIL: TcxGridDBColumn;
    cxGrid3DBTableView1NAZIV_ODOBRIL: TcxGridDBColumn;
    cxDBNavigator1: TcxDBNavigator;
    aZatvoriRN: TAction;
    aZatvoriRNButton: TAction;
    dxBarLargeButton22: TdxBarLargeButton;
    cxGridPopupMenu2: TcxGridPopupMenu;
    cxGridPopupMenu3: TcxGridPopupMenu;
    cxGridPopupMenu4: TcxGridPopupMenu;
    dxBarLargeButton23: TdxBarLargeButton;
    aIzberiPoracka: TAction;
    cxLabel39: TcxLabel;
    txtPakuvanje: TcxDBTextEdit;
    Label2: TLabel;
    cxLabel41: TcxLabel;
    lblGodina: TcxDBLabel;
    lblMerka: TcxDBLabel;
    aPecatiRN: TAction;
    dxBarLargeButton24: TdxBarLargeButton;
    dxBarButton1: TdxBarButton;
    cxLabel42: TcxLabel;
    txtVidAmbalaza: TcxDBTextEdit;
    txtAmbalaza: TcxDBTextEdit;
    cxGrid3DBTableView1SERISKI_BROJ: TcxGridDBColumn;
    cxGrid3DBTableView1VID_AMBALAZA: TcxGridDBColumn;
    cxGrid3DBTableView1AMBALAZA: TcxGridDBColumn;
    cxGrid3DBTableView1ID_PAKUVANJE: TcxGridDBColumn;
    aZatvoriStavka: TAction;
    dxBarButton2: TdxBarButton;
    dxBarButton3: TdxBarButton;
    dxBarButton4: TdxBarButton;
    dxBarButton5: TdxBarButton;
    dxBarButton6: TdxBarButton;
    dxBarButton7: TdxBarButton;
    dxBarButton8: TdxBarButton;
    dxBarButton9: TdxBarButton;
    dxBarButton10: TdxBarButton;
    dxBarButton11: TdxBarButton;
    dxBarButton12: TdxBarButton;
    dxBarSubItem2: TdxBarSubItem;
    dxBarButton13: TdxBarButton;
    dxBarButton14: TdxBarButton;
    dxBarButton15: TdxBarButton;
    dxBarButton16: TdxBarButton;
    dxBarLargeButton25: TdxBarLargeButton;
    dxBarSubItem3: TdxBarSubItem;
    dxBarButton17: TdxBarButton;
    cxGrid3DBTableView1STATUS: TcxGridDBColumn;
    cxGrid3DBTableView1VREDNOST: TcxGridDBColumn;
    cxGrid3DBTableView1BOJA: TcxGridDBColumn;
    cxGrid3DBTableView1NAZIV_STATUS: TcxGridDBColumn;
    cxGrid3DBTableView1NAZIV_AMBALAZA: TcxGridDBColumn;
    bbRNPoracka: TdxBarButton;
    cxGridDBTableView4ID_RE: TcxGridDBColumn;
    cxGridDBTableView4NAZIV_MAGACIN: TcxGridDBColumn;
    cxGridDBTableView4ID_RN_STAVKA: TcxGridDBColumn;
    cxGridDBTableView4MERNA_EDINICA: TcxGridDBColumn;
    dxBarSubItem4: TdxBarSubItem;
    dxBarSubItem5: TdxBarSubItem;
    dxBarButton19: TdxBarButton;
    dxBarButton20: TdxBarButton;
    dxBarButton21: TdxBarButton;
    cxGridPopupMenu5: TcxGridPopupMenu;
    aIzberiStavkiRN: TAction;
    bbIzberiStavki: TdxBarButton;
    cbNazivArtikal: TcxExtLookupComboBox;
    cbAmbalaza: TcxExtLookupComboBox;
    cxLabel45: TcxLabel;
    txtStatus: TcxDBTextEdit;
    cbStatus: TcxDBLookupComboBox;
    cxGrid1DBTableView1BOJA: TcxGridDBColumn;
    cxDBColorComboBox1: TcxDBColorComboBox;
    dxBarSubItem6: TdxBarSubItem;
    dxBarButton23: TdxBarButton;
    dxBarButton24: TdxBarButton;
    aPogledZavisniRN: TAction;
    cbPakuvanje: TcxDBLookupComboBox;
    cxLabel43: TcxLabel;
    txtSerija: TcxDBTextEdit;
    cxGrid1DBTableView2: TcxGridDBTableView;
    cxGrid1DBTableView2BROJ: TcxGridDBColumn;
    cxGrid1DBTableView2GODINA: TcxGridDBColumn;
    cxGrid1DBTableView2DATUM_PLANIRAN_POCETOK: TcxGridDBColumn;
    cxGrid1Level2: TcxGridLevel;
    cxGrid1DBTableView3: TcxGridDBTableView;
    cxGrid1DBTableView3BROJ: TcxGridDBColumn;
    cxGrid1DBTableView3GODINA: TcxGridDBColumn;
    cxGrid1DBTableView3DATUM_PLANIRAN_POCETOK: TcxGridDBColumn;
    cxGrid1DBTableView3NAZIV_STATUS: TcxGridDBColumn;
    cxLabel40: TcxLabel;
    dxBarLargeButton26: TdxBarLargeButton;
    aDizajnerRN: TAction;
    cxGroupBox7: TcxGroupBox;
    cxLabel6: TcxLabel;
    txtPlanDatumKraj: TcxDBDateEdit;
    cxLabel3: TcxLabel;
    txtPlanDatumPoc: TcxDBDateEdit;
    PanelInformacii: TPanel;
    cxPropertiesStore1: TcxPropertiesStore;
    cxLabel36: TcxLabel;
    txtTipPartner: TcxDBTextEdit;
    txtPartner: TcxDBTextEdit;
    cbNazivPartner: TcxLookupComboBox;
    cxLabel5: TcxLabel;
    cbIzvorenDokument: TcxDBLookupComboBox;
    cxLabel10: TcxLabel;
    txtMagacinS: TcxDBTextEdit;
    cbMagacinS: TcxDBLookupComboBox;
    cxLabel13: TcxLabel;
    txtMagacinGP: TcxDBTextEdit;
    cbMagacinGP: TcxDBLookupComboBox;
    cxLabel38: TcxLabel;
    txtPrioritet: TcxDBTextEdit;
    cbPrioritet: TcxDBLookupComboBox;
    cbInformacii: TcxCheckBox;
    cxGroupBox4: TcxGroupBox;
    Label3: TLabel;
    txtZabeleskaS: TcxDBMemo;
    dxBarButton25: TdxBarButton;
    aZavrsiRN: TAction;
    cxGrid1DBTableView1BROJ_SD: TcxGridDBColumn;
    cxGrid1DBTableView3BOJA: TcxGridDBColumn;
    btStavki: TcxButton;
    qArtikalMerka: TpFIBQuery;
    dxBarButton18: TdxBarButton;
    aLagerArtikli: TAction;
    cxLabel11: TcxLabel;
    txtKolicinaEdMerka: TcxDBTextEdit;
    lblMerkaK: TcxDBLabel;
    cxLabel26: TcxLabel;
    txtArtKolicina: TcxDBTextEdit;
    cxGrid3DBTableView1KOLICINA_PAKUVANJE: TcxGridDBColumn;
    cxGrid3DBTableView1LOT: TcxGridDBColumn;
    cxGrid3DBTableView1KOLICINA_BROJ: TcxGridDBColumn;
    cxGrid3DBTableView1MERKA_KOLICINA: TcxGridDBColumn;
    cxDBLabel1: TcxDBLabel;
    txtArtBrPak: TcxDBTextEdit;
    cxLabel27: TcxLabel;
    qMaxBroj: TpFIBQuery;
    tblRE: TpFIBDataSet;
    dsRE: TDataSource;
    tblREID: TFIBIntegerField;
    tblRENAZIV: TFIBStringField;
    aDodadiPRN: TAction;
    aLinija: TAction;
    cxGrid3DBTableView1KOLICINA_REALNA: TcxGridDBColumn;
    cxGrid3DBTableView1ID_RN_OLD: TcxGridDBColumn;
    cxGrid3DBTableView1ROK_UPOTREBA: TcxGridDBColumn;
    cxGrid3DBTableView1ART_KOLICINA: TcxGridDBColumn;
    cxGrid3DBTableView1ART_BROJ_PAK: TcxGridDBColumn;
    cxGrid3DBTableView1ID_PORACKA_STAVKA: TcxGridDBColumn;
    cxGrid3DBTableView1LINIJA: TcxGridDBColumn;
    pImaLinija: TpFIBQuery;
    cxLabel46: TcxLabel;
    txtKolicinaPak: TcxDBTextEdit;
    aZatvoriLinija: TAction;
    N1: TMenuItem;
    N2: TMenuItem;
    dxBarButton22: TdxBarButton;
    aPogledPorackaRN: TAction;
    aBrLinija: TAction;
    cxGrid3DBTableView1STATUS_LINIJA: TcxGridDBColumn;

    procedure cxDBTextEditAllEnter(Sender: TObject);
    procedure cxDBTextEditAllExit(Sender: TObject);
    procedure EnterKakoTab(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    //procedure PrvPosledenTab(panel:TPanel;var posledna,prva:TWinControl);
    procedure SaveToIniFileExecute(Sender: TObject);
    procedure cxGrid1DBTableView1KeyPress(Sender: TObject; var Key: Char);
    procedure aOtkaziExecute(Sender: TObject);
    procedure aAzurirajExecute(Sender: TObject);
    procedure aZacuvajExcelExecute(Sender: TObject);
    procedure aRefreshExecute(Sender: TObject);
    procedure aSnimiIzgledExecute(Sender: TObject);
    procedure aIzlezExecute(Sender: TObject);
    procedure aPecatiTabelaExecute(Sender: TObject);
    procedure aSnimiPecatenjeExecute(Sender: TObject);
    procedure zacuvajPrintVoIni(ime:AnsiString);
    procedure procitajPrintOdIni(ime:AnsiString);
    procedure aPodesuvanjePecatenjeExecute(Sender: TObject);
    procedure aBrisiPodesuvanjePecatenjeExecute(Sender: TObject);
    procedure aPageSetupExecute(Sender: TObject);
    procedure aFormConfigExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure PartnerPropertiesChange(Sender: TObject);
    procedure aBrisiIzgledExecute(Sender: TObject);
    procedure SetirajLukap(sender:TObject; tabela:TDataSet; tip:TcxDBTextEdit; sifra:TcxDBTextEdit; lukap:TcxExtLookupComboBox);
    procedure ActionList1Execute(Action: TBasicAction; var Handled: Boolean);
    procedure aListaRNExecute(Sender: TObject);
    procedure CustomFields();
    procedure XPColorMap1ColorChange(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure StatusBoi;
    procedure aNovvExecute(Sender: TObject);
    procedure cxGrid3DBTableView1FocusedRecordChanged(
      Sender: TcxCustomGridTableView; APrevFocusedRecord,
      AFocusedRecord: TcxCustomGridRecord;
      ANewItemRecordFocusingChanged: Boolean);
    procedure cxPageControl1Change(Sender: TObject);
    procedure aPogledRNExecute(Sender: TObject);
    procedure cxGrid1DBTableView1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure cxGrid1DBTableView1DblClick(Sender: TObject);
    procedure aSiteRNExecute(Sender: TObject);
    procedure aOtvoreniRNExecute(Sender: TObject);
    procedure cxGrid3LayoutChanged(Sender: TcxCustomGrid;
      AGridView: TcxCustomGridView);
    procedure cxGrid3FocusedViewChanged(Sender: TcxCustomGrid; APrevFocusedView,
      AFocusedView: TcxCustomGridView);
    procedure aSmeniStatusExecute(Sender: TObject);
    procedure cbRekoltaPropertiesChange(Sender: TObject);
    procedure dxRibbon1TabChanging(Sender: TdxCustomRibbon;
      ANewTab: TdxRibbonTab; var Allow: Boolean);
    procedure cbGodinaChange(Sender: TObject);
    procedure cbNovREChange(Sender: TObject);
    procedure aZatvoriRNExecute(Sender: TObject);
    procedure gbZatvoriRNClick(Sender: TObject);
    procedure aIzberiPorackaExecute(Sender: TObject);
    procedure cxGrid1DBTableView1TcxGridDBDataControllerTcxDataSummaryFooterSummaryItems0GetText(
      Sender: TcxDataSummaryItem; const AValue: Variant; AIsFooter: Boolean;
      var AText: string);
    procedure aPecatiRNExecute(Sender: TObject);
    procedure aZatvoriStavkaExecute(Sender: TObject);
    procedure cxGridDBTableView4FocusedRecordChanged(
      Sender: TcxCustomGridTableView; APrevFocusedRecord,
      AFocusedRecord: TcxCustomGridRecord;
      ANewItemRecordFocusingChanged: Boolean);
    procedure aIzberiStavkiRNExecute(Sender: TObject);
    procedure aPogledZavisniRNExecute(Sender: TObject);
    procedure btStavkiClick(Sender: TObject);
    procedure aDizajnerRNExecute(Sender: TObject);
    procedure cbInformaciiClick(Sender: TObject);
    procedure aZavrsiRNExecute(Sender: TObject);
    procedure cxGrid1DBTableView1FocusedRecordChanged(
      Sender: TcxCustomGridTableView; APrevFocusedRecord,
      AFocusedRecord: TcxCustomGridRecord;
      ANewItemRecordFocusingChanged: Boolean);
    procedure cxGrid1DBTableView1DataControllerDetailCollapsed(
      ADataController: TcxCustomDataController; ARecordIndex: Integer);
    procedure cxGrid1DBTableView1DataControllerDetailExpanded(
      ADataController: TcxCustomDataController; ARecordIndex: Integer);
    procedure cxGrid1DBTableView1DataControllerDetailExpanding(
      ADataController: TcxCustomDataController; ARecordIndex: Integer;
      var AAllow: Boolean);
    procedure cxGrid1DBTableView1StylesGetContentStyle(
      Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord;
      AItem: TcxCustomGridTableItem; var AStyle: TcxStyle);
    procedure ArtMerka;
    procedure aLagerArtikliExecute(Sender: TObject);
    procedure aDodadiPRNExecute(Sender: TObject);
    procedure aLinijaExecute(Sender: TObject);
    procedure cxGrid3DBTableView1StylesGetContentStyle(
      Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord;
      AItem: TcxCustomGridTableItem; var AStyle: TcxStyle);
    procedure aZatvoriLinijaExecute(Sender: TObject);
    procedure aPogledPorackaRNExecute(Sender: TObject);
    procedure aUrediTextOpisExecute(Sender: TObject);
    procedure aBrLinijaExecute(Sender: TObject);
  private
    { Private declarations }

    _sifra_kluc :array[1..10] of string;
    procedure OtvoreniRN;
    procedure OtvoriNormativ;
    procedure OtvoriOperacija;  // �������� ���������� �� ������ �� ������� �� ������������� ���

  protected
    list : TList;
    inserting : boolean;
    prva, posledna :TWinControl;
    custom1,custom2,custom3 : AnsiString;
    merka, art_kolicina, art_br_pak, barkod : string;
    procedure prefrli;

  public
    { Public declarations }
    constructor Create(Owner : TComponent; insert : boolean = true);reintroduce; overload;

    procedure SetSifra(br: integer; s : string);
    function GetSifra(br: integer) : string;

    //������� �� ���������� ������������ _sifra_kluc
    property sifra_kluc[br :integer]: string read GetSifra write SetSifra;
    var re: ansistring;
    kol_amb:Real;
    merka_art, merka_kol:string;
    br_pak : integer;
    source_dok : Int64;
  published

  end;

var
  frmIzvestaiRN: TfrmIzvestaiRN;

implementation

uses DaNe, dmKonekcija, Utils, dmResources, FormConfig, dmMaticni, dmSystem,
   Login, dmUnit, CustomFields, IzberiPoracki, IzberiStavkiRN, NurkoRepository,
  ZavrsiRN, LagerArtikli, PogledPorackiRN, Notepad, BrLinija,
  ManufacturingOrder;

{$R *.dfm}
//------------------------------------------------------------------------------

constructor TfrmIzvestaiRN.Create(Owner : TComponent; insert : boolean = true);
begin
inherited Create(Owner);
//	�������� ���� ������� � �������� �� ����������� ���������
//	insert = true -> ����� ���� ����� ���������� �� ������� �� Insert Mode
//  insert = false -> ����� ���� ����� ������� �� ����� �� ������. �� ������� ����� ����� �� �� �������� F5
  inserting := insert;
end;
//	����� �� �����������. �� ��������� ������� �� ������, �� �������� ������� �� ���� ��������,
//	�� ������� ������� �� ������ ������ � �� ������� �� Insert Mode
procedure TfrmIzvestaiRN.aNovvExecute(Sender: TObject);
begin

end;

//	����� �� ���������. �� ��������� ������� �� ������, �� �������� ������� �� ���� ��������,
//	�� ������� ������� �� ������ ������ � �� ������� �� Edit Mode
procedure TfrmIzvestaiRN.aAzurirajExecute(Sender: TObject);
begin

end;

//	����� �� ������ �� ������������� �����
procedure TfrmIzvestaiRN.aBrisiIzgledExecute(Sender: TObject);
begin
  brisiGridVoBaza(Name,cxGrid1DBTableView1);
  BrisiFormaIzgled(self);
end;

//	����� �� ���������� �� ����������
procedure TfrmIzvestaiRN.aRefreshExecute(Sender: TObject);
begin
  cxGrid1DBTableView1.DataController.Filter.Root.Clear; //���������� �� ��������
  cxGrid1DBTableView1.DataController.DataSet.Refresh;
end;

//	����� �� ����� �� ������� �� ������� �� ������� �����
procedure TfrmIzvestaiRN.aIzberiPorackaExecute(Sender: TObject);
begin
   frmIzberiPoracki := TfrmIzberiPoracki.Create(Application);
   frmIzberiPoracki.godina := cbGodina.EditValue;
   frmIzberiPoracki.ShowModal;
   frmIzberiPoracki.Free;

   OtvoreniRN;
end;

procedure TfrmIzvestaiRN.aIzberiStavkiRNExecute(Sender: TObject);
begin
    frmIzberiStavkiRN := TfrmIzberiStavkiRN.Create(Application);
    frmIzberiStavkiRN.ShowModal;
    frmIzberiStavkiRN.Free;
end;

procedure TfrmIzvestaiRN.aIzlezExecute(Sender: TObject);
begin
  Close;
end;

procedure TfrmIzvestaiRN.aLagerArtikliExecute(Sender: TObject);
begin
    frmLagerArtikli := TfrmLagerArtikli.Create(Application);
    frmLagerArtikli.ShowModal;
    frmLagerArtikli.Free;
end;

procedure TfrmIzvestaiRN.aLinijaExecute(Sender: TObject);
begin
  if (dm.tblRabotenNalog.State = dsBrowse) and (dm.tblRNStavka.State = dsBrowse)
     and (dm.tblRabotenNalogSTATUS.Value = 1)
     and (dm.tblRNStavkaSTATUS.Value = 1) then
    begin
  // �� �� �������� ���ȣ�
  //�������� ���� ��� ��� ���� ����� �������

  if (dm.tblRNStavkaLINIJA.Value = 0)then
  begin
    // begin
     frmDaNe := TfrmDaNe.Create(self, '����������', '���� �� �� �������� �������?', 1);
       if (frmDaNe.ShowModal = mrYes) then
              begin
//                dm.tblRNStavka.Edit;
//                dm.tblRNStavkaLINIJA.Value := 1;
//                dm.tblRNStavka.Post;


                  frmBrLinija :=TfrmBrLinija.Create(self);
                  frmBrLinija.ShowModal;


                  if frmBrLinija.cbNaziv.Text <> '' then
                  begin
                     //������� ���� ��� ������� �����
                     pImaLinija.Close;
                     pImaLinija.ParamByName('linija').AsInteger := dm.tblLinijaID.Value;
                     pImaLinija.ExecQuery;

                     if pImaLinija.FldByName['linija'].AsString = '' then
                       begin
                           dm.tblRNStavka.Edit;
                           dm.tblRNStavkaLINIJA.Value := dm.tblLinijaID.Value;
                           dm.tblRNStavkaSTATUS_LINIJA.AsInteger := 1;
                           dm.tblRNStavka.Post
                       end
                   else
                       begin
                         ShowMessage('��� ������� ����� '+frmBrLinija.cbNaziv.Text);
                         Abort;
                       end
                  end
                  else
                  dm.tblRNStavka.Cancel;
              end
              else
              begin
                Abort;
              end;
              frmBrLinija.Free;
            //end
  end
 else
 if dm.tblRNStavkaLINIJA.Value <> 0 then
 begin
  //������� ���� ��� ������� �����
     pImaLinija.Close;
     pImaLinija.ParamByName('linija').AsInteger := dm.tblLinijaID.Value;
     pImaLinija.ExecQuery;
 if pImaLinija.FldByName['linija'].AsString <> '' then
   begin
    frmDaNe := TfrmDaNe.Create(self, '������������', '���� �� �� ���������� �������?', 1);
     if (frmDaNe.ShowModal = mrYes) then
        begin
             dm.tblRNStavka.Edit;
             dm.tblRNStavkaLINIJA.AsInteger := 0;
             dm.tblRNStavkaSTATUS_LINIJA.AsInteger := 0;
             dm.tblRNStavka.Post
        end;
   end
 end;


    end;
end;

procedure TfrmIzvestaiRN.aListaRNExecute(Sender: TObject);
begin
    if dm.tblRabotenNalog.State <> dsBrowse then
       begin
          dm.tblRabotenNalog.Cancel;
          dPanel.Enabled := false;
       end;
          tsListaRN.TabVisible := true;
          cxPageControl1.ActivePage := tsListaRN;
end;

//	����� �� ������� �� �������� �� ������ �� ���� (Utils.pas)
procedure TfrmIzvestaiRN.aSiteRNExecute(Sender: TObject);
begin
  dm.tblRabotenNalog.Close;
  dm.tblRabotenNalog.ParamByName('status').AsString := '%';
  dm.tblRabotenNalog.ParamByName('godina').AsString := cbGodina.EditValue;
  dm.tblRabotenNalog.ParamByName('site').AsString := '1';
  dm.tblRabotenNalog.ParamByName('re').AsInteger := dmkon.UserRE;
  dm.tblRabotenNalog.Open;

   dm.tblRNStavka.Open;
   dm.pRNNormativ.Open;
   dm.tblRNSurovini.Open;
   dm.tblArtikalRN.Close;
   dm.tblArtikalRN.Open;

   cxGrid1Level1.Caption := '���� ������� ������'
end;

procedure TfrmIzvestaiRN.aSmeniStatusExecute(Sender: TObject);
begin
  if dm.tblRNStavka.State = dsBrowse then
  begin
    dm.tblRNStavka.Edit;
    dm.tblRNStavkaSTATUS.Value:=dm.tblRabotenNalogSTATUS_STAVKI.Value+1;
    dm.tblRNStavka.Post;
//    dm.tblRabotenNalog.Refresh;  //CloseOpen(False);
  //  dm.tblRNStavka.Open;
  //  dm.tblRNNormativ.Open;
  end;

end;

procedure TfrmIzvestaiRN.aSnimiIzgledExecute(Sender: TObject);
begin
  zacuvajGridVoBaza(Name,cxGrid1DBTableView1);
  ZacuvajFormaIzgled(self);
end;

//	����� �� ������� �� ������ �� Excel ������ (Utils.pas)
procedure TfrmIzvestaiRN.aZacuvajExcelExecute(Sender: TObject);
begin
  zacuvajVoExcel(cxGrid1, Caption);
end;

//  ��������� �� ������ �� ������� �� ���������� �� ����� �� Enter
procedure TfrmIzvestaiRN.EnterKakoTab(Sender: TObject; var Key: Word;
  Shift: TShiftState);

begin

end;

//	������� �� ��� �� ������ ��� �������� �� �����
procedure TfrmIzvestaiRN.btStavkiClick(Sender: TObject);
begin
   cxPageControl1.ActivePage := tsStavki;
end;

procedure TfrmIzvestaiRN.cbInformaciiClick(Sender: TObject);
begin
     if cbInformacii.Checked then
     begin
        PanelInformacii.Enabled := true;
        txtTipPartner.SetFocus;
     end
     else
     begin
        PanelInformacii.Enabled := False;
     end;

end;

procedure TfrmIzvestaiRN.cxDBTextEditAllEnter(Sender: TObject);
begin
    TEdit(Sender).Color:=clSkyBlue;
    if (TcxdbTextEdit(Sender) = txtPakuvanje)
      or (TcxDBLookupComboBox(Sender) = cbPakuvanje)
          then
    begin
      dm.tblIzvedenaEM.Close;
      dm.tblIzvedenaEM.ParamByName('artikal').AsString := txtArtikal.Text;
      dm.tblIzvedenaEM.ParamByName('vid_artikal').AsString := txtVidArtikal.Text;
      dm.tblIzvedenaEM.Open;
    end;
    ArtMerka;

    if (TcxdbTextEdit(Sender) = txtSerija)
     // or (TcxDBLookupComboBox(Sender) = cbPakuvanje)
          then
          begin

          end;

//    if ((Sender as TWinControl)= txtKolicinaEdMerka) then
//        begin
//           if dm.tblArtikal.Locate('ARTVID;ID',VarArrayOf([txtVidArtikal.Text,txtArtikal.Text]),[]) then
//          begin
//              merka_kol := dm.tblArtikalMERKA_KOLICINA.Value;
//              kol_amb := dm.tblArtikalKOLICINA.Value;
//              merka_art := dm.tblArtikalMERKA.Value;
//              art_kolicina := dm.tblArtikalKOLICINA.Value;
//              art_br_pak := dm.tblArtikalBRPAK.AsString;
//              if dm.tblIzvedenaEMKOEFICIENT.IsNull then
//                 br_pak := dm.tblArtikalBRPAK.Value
//              else
//                 br_pak := dm.tblIzvedenaEMKOEFICIENT.Value;
//          end
//          else
//              begin
//               kol_amb := 0;
//               br_pak := 0;
//              end;
//
////          if not dm.tblIzvedenaEMKOEFICIENT.IsNull then
////             dm.tblRNZavrsiStavkaKOLICINA.Value := StrToFloat(txtKolicinaArtikal.Text)*dm.tblIzvedenaEMKOEFICIENT.Value*kol_amb
////          else dm.tblRNZavrsiStavkaKOLICINA.Value := 0;
//
//         //if kol_amb <> '' then
//            // dm.tblRNStavkaKOLICINA.Value := StrToInt(txtKolicinaArtikal.Text) * kol_amb;
//
//             dm.tblRNStavkaKOLICINA_PAKUVANJE.AsString :=floattostr(strtofloat(txtKolicinaArtikal.Text)/(dm.tblArtikalKOLICINA.AsFloat));
//             dm.tblRNStavkaKOLICINA_BROJ.Value := dm.tblRNStavkaKOLICINA_PAKUVANJE.AsInteger * br_pak;
//             lblMerkaK.Caption := merka_kol;
//             lblMerka.Caption := merka_art;
//             dm.tblRNStavkaART_KOLICINA.AsString := art_kolicina;
//             dm.tblRNStavkaART_BROJ_PAK.AsString := art_br_pak;
//
//          //else dm.tblRNZavrsiStavkaKOLICINA.Value := 0;
//
//        end;


end;

//	������� �� ��� �� ������ ��� ����� �� �����
procedure TfrmIzvestaiRN.cxDBTextEditAllExit(Sender: TObject);
begin
    TEdit(Sender).Color:=clWhite;

   if (dm.tblRNStavka.State) in [dsInsert, dsEdit] then
    begin
    if ((Sender as TWinControl)= cbNazivArtikal) then
        begin
            if (cbNazivArtikal.Text <>'') then
            begin
              txtVidArtikal.Text := cbNazivArtikal.EditValue[0];
	            txtArtikal.Text := cbNazivArtikal.EditValue[1];
 	        //    lblmerka.Caption := cbNazivArtikal.EditValue[2];
              //dm.tblNormativO.Close;
              //dm.tblNormativO.ParamByName('mas_id').va
              dm.tblIzvedenaEM.Close;
              dm.tblIzvedenaEM.ParamByName('artikal').AsString := txtArtikal.Text;
              dm.tblIzvedenaEM.ParamByName('vid_artikal').AsString := txtVidArtikal.Text;
              dm.tblIzvedenaEM.Open;
            end
            else
            begin
              dm.tblRNStavkaVID_ARTIKAL.Clear;
              dm.tblRNStavkaARTIKAL.Clear;
              lblMerka.Clear;
            end;

         end;
         if ((Sender as TWinControl)= txtArtikal) or ((Sender as TWinControl)= txtVidArtikal) then
         begin
             SetirajLukap(sender,dm.tblArtikalRN,txtVidArtikal,txtArtikal, cbNazivArtikal);
             dm.tblIzvedenaEM.Close;
             dm.tblIzvedenaEM.ParamByName('artikal').AsString := txtArtikal.Text;
             dm.tblIzvedenaEM.ParamByName('vid_artikal').AsString := txtVidArtikal.Text;
             dm.tblIzvedenaEM.Open;
         end;


         if ((Sender as TWinControl)= txtKolicinaArtikal) then
          begin
              begin
           if dm.tblArtikal.Locate('ARTVID;ID',VarArrayOf([txtVidArtikal.Text,txtArtikal.Text]),[]) then
          begin
              merka_kol := dm.tblArtikalMERKA_KOLICINA.Value;
             // kol_amb := dm.tblArtikalKOLICINA.Value;
              merka_art := dm.tblArtikalMERKA.Value;
              art_kolicina := dm.tblArtikalKOLICINA.AsString;
              art_br_pak := dm.tblArtikalBRPAK.AsString;
              if dm.tblIzvedenaEMKOEFICIENT.IsNull then
                 br_pak := dm.tblArtikalBRPAK.Value
              else
                 br_pak := dm.tblIzvedenaEMKOEFICIENT.Value;
          end
          else
              begin
               kol_amb := 0;
               br_pak := 0;
              end;
          end;
          if not dm.tblArtikalKOLICINA.IsNull then
             dm.tblRNStavkaKOLICINA_BROJ.AsFloat := strtoint(txtKolicinaArtikal.Text)/ strtofloat(art_kolicina);
          end;



        if ((Sender as TWinControl)= cbPakuvanje) then
        begin
//           if dm.tblArtikal.Locate('ARTVID;ID',VarArrayOf([txtVidArtikal.Text,txtArtikal.Text]),[]) then
//          begin
//              merka_kol := dm.tblArtikalMERKA_KOLICINA.Value;
//             // kol_amb := dm.tblArtikalKOLICINA.Value;
//              merka_art := dm.tblArtikalMERKA.Value;
//              art_kolicina := dm.tblArtikalKOLICINA.AsString;
//              art_br_pak := dm.tblArtikalBRPAK.AsString;
              if dm.tblIzvedenaEMKOEFICIENT.IsNull then
                 br_pak := dm.tblArtikalBRPAK.Value
              else
                 br_pak := dm.tblIzvedenaEMKOEFICIENT.Value;
//          end
//          else
//              begin
//               kol_amb := 0;
//               br_pak := 0;
//              end;

//          if not dm.tblIzvedenaEMKOEFICIENT.IsNull then
//             dm.tblRNZavrsiStavkaKOLICINA.Value := StrToFloat(txtKolicinaArtikal.Text)*dm.tblIzvedenaEMKOEFICIENT.Value*kol_amb
//          else dm.tblRNZavrsiStavkaKOLICINA.Value := 0;

         //if kol_amb <> '' then
            // dm.tblRNStavkaKOLICINA.Value := StrToInt(txtKolicinaArtikal.Text) * kol_amb;
//          if not dm.tblArtikalKOLICINA.IsNull then
//             dm.tblRNStavkaKOLICINA_BROJ.AsFloat := strtoint(txtKolicinaArtikal.Text)* strtofloat(art_kolicina);

            if br_pak <> 0 then
               dm.tblRNStavkaKOLICINA_PAKUVANJE.Value :=  dm.tblRNStavkaKOLICINA_BROJ.AsFloat / br_pak;

            // dm.tblRNStavkaKOLICINA_BROJ.Value := dm.tblRNStavkaKOLICINA_PAKUVANJE.AsInteger * br_pak;
            // dm.tblRNStavkaKOLICINA_PAKUVANJE.AsString :=floattostr(strtofloat(txtKolicinaArtikal.Text)/(dm.tblArtikalKOLICINA.AsFloat));
            // dm.tblRNStavkaKOLICINA_BROJ.Value := dm.tblRNStavkaKOLICINA_PAKUVANJE.AsInteger * br_pak;
             lblMerkaK.Caption := merka_kol;
             lblMerka.Caption := merka_art;
             dm.tblRNStavkaMERKA.AsString := merka_art;
             dm.tblRNStavkaART_KOLICINA.AsString := art_kolicina;
             dm.tblRNStavkaART_BROJ_PAK.AsString := art_br_pak;

          //else dm.tblRNZavrsiStavkaKOLICINA.Value := 0;

        end;


    end;



    if (dm.tblRNStavka.State) in [dsInsert, dsEdit] then
    begin
   if ((Sender as TWinControl)= cbAmbalaza) then
        begin
            if (cbAmbalaza.Text <>'') then
            begin
              txtVidAmbalaza.Text := cbAmbalaza.EditValue[0];
              txtAmbalaza.Text := cbAmbalaza.EditValue[1];
              //dm.tblNormativO.Close;
              //dm.tblNormativO.ParamByName('mas_id').va
            end
            else
            begin
              dm.tblRNStavkaVID_AMBALAZA.Clear;
              dm.tblRNStavkaAMBALAZA.Clear;
            end;
         end;
         if ((Sender as TWinControl)= txtAmbalaza) or ((Sender as TWinControl)= txtVidAmbalaza) then
         begin
             SetirajLukap(sender,dm.tblAmbalaza,txtVidAmbalaza,txtAmbalaza, cbAmbalaza);
         end;


         if ((Sender as TWinControl)= txtOdobril) or ((Sender as TWinControl)= txtTipOdobril) then
         begin
              dm.SetirajLukap(sender,dmMat.tblPartner,txtTipOdobril,txtOdobril, cbOdobril);
         end
         else
         if ((Sender as TWinControl)= cbOdobril) then
         begin
            if (cbOdobril.Text <>'') then
            begin
              txtTipOdobril.Text := dmMat.tblPartnerTIP_PARTNER.AsString;
              txtOdobril.Text := dmMat.tblPartnerID.AsString;
            end
            else
            begin
              dm.tblRNStavkaTIP_ODOBRIL.Clear;
              dm.tblRNStavkaODOBRIL.Clear;
            end;
         end;
         if ((Sender as TWinControl)= txtOdobril) or ((Sender as TWinControl)= txtTipOdobril) then
         begin
              dm.SetirajLukap(sender,dmMat.tblPartner,txtTipOdobril,txtOdobril, cbOdobril);
         end;
    end;



   if (dm.tblRabotenNalog.State) in [dsInsert, dsEdit] then
    begin
    if ((Sender as TWinControl)= cbNazivPartner) then
         begin
            if (cbNazivPartner.Text <>'') then
            begin
              txtTipPartner.Text := dmMat.tblPartnerTIP_PARTNER.AsString;
              txtPartner.Text := dmMat.tblPartnerID.AsString;
            end
            else
            begin
              dm.tblRabotenNalogTP_PORACKA.Clear;
              dm.tblRabotenNalogP_PORACKA.Clear;
            end;
         //end;

     end;


//    if (dm.tblRabotenNalog.State) = dsBrowse then
//    begin
//    if ((Sender as TdxBarLookupCombo)= cbNovRE) then
//         begin
//            if (cbNovRE.Text <> '') then
//            begin
//             // txtTipPartner.Text := dmMat.tblPartnerTIP_PARTNER.AsString;
//             // txtPartner.Text := dmMat.tblPartnerID.AsString;
//             aNovv.Execute;
//            end
//
//           else Abort;
//           // begin
//           //   dm.tblRabotenNalogTP_PORACKA.Clear;
//           //   dm.tblRabotenNalogP_PORACKA.Clear;
//           // end;
//         end;

         if ((Sender as TWinControl)= txtPartner) or ((Sender as TWinControl)= txtTipPartner) then
         begin
              dm.SetirajLukap(sender,dmMat.tblPartner,txtTipPartner,txtPartner, cbNazivPartner);
         end
         else
         if ((Sender as TWinControl)= cbNazivPartner) then
         begin
            if (cbNazivPartner.Text <>'') then
            begin
              txtTipPartner.Text := dmMat.tblPartnerTIP_PARTNER.AsString;
              txtPartner.Text := dmMat.tblPartnerID.AsString;
            end
            else
            begin
              dm.tblRabotenNalogTP_PORACKA.Clear;
              dm.tblRabotenNalogP_PORACKA.Clear;
            end;
         end;

         if ((Sender as TWinControl)= txtPartner) or ((Sender as TWinControl)= txtTipPartner) then
         begin
              dm.SetirajLukap(sender,dmMat.tblPartner,txtTipPartner,txtPartner, cbNazivPartner);
         end;
       end;




end;

procedure TfrmIzvestaiRN.cxGrid1DBTableView1DataControllerDetailCollapsed(
  ADataController: TcxCustomDataController; ARecordIndex: Integer);
begin
 ADataController.ClearDetailLinkObject(ARecordIndex, 0);
 //dm.tblRabotenNalog.Open;
end;

procedure TfrmIzvestaiRN.cxGrid1DBTableView1DataControllerDetailExpanded(
  ADataController: TcxCustomDataController; ARecordIndex: Integer);
begin
  ADataController.FocusedRecordIndex := ARecordIndex;
end;

procedure TfrmIzvestaiRN.cxGrid1DBTableView1DataControllerDetailExpanding(
  ADataController: TcxCustomDataController; ARecordIndex: Integer;
  var AAllow: Boolean);
begin
  ADataController.CollapseDetails;
end;

procedure TfrmIzvestaiRN.cxGrid1DBTableView1DblClick(Sender: TObject);
begin
   aPogledRN.Execute;
end;

procedure TfrmIzvestaiRN.cxGrid1DBTableView1FocusedRecordChanged(
  Sender: TcxCustomGridTableView; APrevFocusedRecord,
  AFocusedRecord: TcxCustomGridRecord; ANewItemRecordFocusingChanged: Boolean);
begin
     frmIzvestaiRN.Caption := '������� ����� ��. '+dm.tblRabotenNalogBROJ.AsString+'/'+dm.tblRabotenNalogGODINA.AsString;

end;

//  ��������� �� ������ �� ��������� �� ���������� ���� �� ��������
//  ��������� ��: br - ����� ��� �� ������ (�� 0 �� 9) �� ��������� �� ����� �� ����� �� ���������� ����
//  s - ��������� �� ������ �� ������ (���������� �� string �� ����� �� integer �� IntToStr ���������)
procedure TfrmIzvestaiRN.SetSifra(br: integer; s : string);
begin
  _sifra_kluc[br] := s;
end;

procedure TfrmIzvestaiRN.XPColorMap1ColorChange(Sender: TObject);
begin

end;

//  ������� �� ������ �� ��������� �� ���������� ���� �� ��������
//  ��������� ��: br - ����� ��� �� ������ (�� 0 �� 9) �� ��������� �� ����� �� ����� �� ���������� ����
function TfrmIzvestaiRN.GetSifra(br: integer) : string;
begin
  Result := _sifra_kluc[br];
end;

procedure TfrmIzvestaiRN.OtvoriOperacija;
begin
    dm.pRNOperacija.Close;
    dm.pRNOperacija.ParamByName('ID_RN').AsString := dm.tblRNStavkaID_RABOTEN_NALOG.AsString;
    dm.pRNOperacija.ParamByName('ID').AsString := dm.tblRNStavkaID.AsString;
    dm.pRNOperacija.Open;
end;

procedure TfrmIzvestaiRN.OtvoriNormativ;
begin
  if not dm.tblRNStavkaid.IsNull  then
 begin
  dm.pRNNormativ.Close;
  dm.pRNNormativ.ParamByName('ID_RN').AsString := dm.tblRNStavkaID_RABOTEN_NALOG.AsString;
  dm.pRNNormativ.ParamByName('ID').AsString := dm.tblRNStavkaID.AsString;
  dm.pRNNormativ.Open;
 end;
end;

procedure TfrmIzvestaiRN.OtvoreniRN;
begin
  dm.tblRabotenNalog.Close;
  dm.tblRabotenNalog.ParamByName('status').AsString := '1';
  dm.tblRabotenNalog.ParamByName('godina').AsString := cbGodina.EditValue;
  dm.tblRabotenNalog.ParamByName('site').AsString := '2';
  dm.tblRabotenNalog.ParamByName('re').AsInteger := dmkon.UserRE;
  dm.tblRabotenNalog.Open;

  dm.tblRNSD.Close;
  dm.tblRNSD.Open;

  dm.tblRNStavka.Open;
  dm.pRNNormativ.Open;
  dm.tblRNSurovini.Open;
  dm.tblArtikalRN.Close;
  dm.tblArtikalRN.Open;

  dm.tblIzberiRN.Close;
  dm.tblIzberiRN.ParamByName('status').AsString := '1';
  dm.tblIzberiRN.ParamByName('godina').AsString := cbGodina.EditValue;
  dm.tblIzberiRN.ParamByName('id_re').AsString := '%';
  dm.tblIzberiRN.Open;

  cxGrid1Level1.Caption := '�������� ������� ������'
end;

procedure TfrmIzvestaiRN.PartnerPropertiesChange(Sender: TObject);
begin

end;

//  �� ������� �� ������� ��� ����������� �� Master-�� �� �������� �� ��������� �� ������ ����� SetSifra
procedure TfrmIzvestaiRN.prefrli;
begin
end;

procedure TfrmIzvestaiRN.FormClose(Sender: TObject; var Action: TCloseAction);
begin
   dmRes.FreeRepository(rData);

end;
procedure TfrmIzvestaiRN.FormCreate(Sender: TObject);
begin
  ProcitajFormaIzgled(self);
  if (dmRes.use_skin = true) then dxRibbon1.ColorSchemeName := dmRes.skin_name;
  dm.tblAmbalaza.CloseOpen(true);
  rData := TRepositoryData.Create();
end;

//------------------------------------------------------------------------------

procedure TfrmIzvestaiRN.FormShow(Sender: TObject);
begin
  //  ������� �� ������������ ������������/���������� �� ���������� �� ���� �����
    SpremiForma(self);
  //	�� ��������� �� ������� ������ ��� ���������� � ���� � ��� �������� �� �������
   PrvPosledenTab(dPanel,posledna,prva);
    bmRabotenNalog.Caption := Caption;
    dxComponentPrinter1Link1.ReportTitle.Text := Caption;
    cbgodina.EditValue:=godina;
    tblRE.ParamByName('u').AsString := dmKon.user;
    tblRE.Open;
    OtvoreniRN;
    dm.tblZavisniRN.Open;
    dm.tblRNStavka.Open;
    dm.pRNNormativ.Open;
    dm.tblRNSurovini.Open;
    dm.tblArtikalRN.Close;
    dm.tblArtikalRN.Open;
 //   dm.tblAmbalaza.CloseOpen(true);
  //	������� �� ������������ �� ������ �� ������
    procitajGridOdBaza(Name,cxGrid1DBTableView1,false,false);
  //	������� �� ������������ �� ��������� �� ������
    procitajPrintOdBaza(Name,cxGrid1DBTableView1.Name, dxComponentPrinter1Link1);
     CustomFields();
    cxPageControl1.ActivePageIndex := 0;
    cxGrid1.SetFocus;
    cxGrid1DBTableView1.Controller.GoToFirst(True);

    dm.tblIzvedenaEM.Close;
    dm.tblIzvedenaEM.ParamByName('vid_artikal').AsString := '%';
    dm.tblIzvedenaEM.ParamByName('artikal').AsString := '%';
    dm.tblIzvedenaEM.Open;

    dm.tblMatStatus.Close;
    dm.tblMatStatus.ParamByName('TABELA').AsString := 'MAN_RABOTEN_NALOG';
    dm.tblMatStatus.Open;

    dmMat.tblNurko.Open;

  	cbNazivArtikal.RepositoryItem := dmRes.InitRepository(-101, 'cbNazivArtikal', Name, rData);
   // cbSurovina.RepositoryItem := dmRes.InitRepository(-102, 'cbSurovina', Name, rData);
    cbAmbalaza.RepositoryItem := dmRes.InitRepository(-103, 'cbAmbalaza', Name, rData);
    cbNazivArtikal.RepositoryItem := dmRes.InitRepositoryRefresh(-101, 'cbNazivArtikal', Name, rData);
    cbAmbalaza.RepositoryItem := dmRes.InitRepositoryRefresh(-103, 'cbAmbalaza', Name, rData);
end;
//------------------------------------------------------------------------------

procedure TfrmIzvestaiRN.SaveToIniFileExecute(Sender: TObject);
begin
    cxGrid1DBTableView1.StoreToIniFile(Name,True,[]);
end;
//------------------------------------------------------------------------------

procedure TfrmIzvestaiRN.cxGrid1DBTableView1KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
 if key = VK_RETURN then
     aPogledRN.Execute;
end;

procedure TfrmIzvestaiRN.cxGrid1DBTableView1KeyPress(Sender: TObject;
  var Key: Char);
begin
  if(Ord(Key) <> VK_RETURN) then cxSortiraj(cxGrid1DBTableView1);
end;

procedure TfrmIzvestaiRN.cxGrid1DBTableView1StylesGetContentStyle(
  Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord;
  AItem: TcxCustomGridTableItem; var AStyle: TcxStyle);
var  AStatus:Integer;
begin
  AStatus:=-1;
  if (ARecord<>nil) and (AItem<>nil) then
    if not VarIsNull(TcxCustomGridRecord(ARecord).Values[cxGrid1DBTableView1SOURCE_DOKUMENT.Index])
     then
     begin
       AStatus:=VarAsType(TcxCustomGridRecord(ARecord).Values[cxGrid1DBTableView1SOURCE_DOKUMENT.Index], varInteger);   AStatus:=0;
     end;
     //if (VarAsType(TcxCustomGridRecord(ARecord).Values[cxGrid1DBTableView1STATUS.Index], varInteger) = 0)
     //and (VarAsType(TcxCustomGridRecord(ARecord).Values[cxGrid1DBTableView1Precekoren.Index], varInteger) = 1)
     //then
     //    AStatus:=3;
     //if (VarAsType(TcxCustomGridRecord(ARecord).Values[cxGrid1DBTableView1SOURCE_DOKUMENT.Index], varInteger) <> 0)
    // and (VarIsNull(TcxCustomGridRecord(ARecord).Values[cxGrid1DBTableView1DBColumn2.Index]))
    // then

    case AStatus of
     -1:AStyle:=dm.cxStyle2;
   //  1:AStyle:=dmRN1.cxStyleZatvoreniN;
  //  2:AStyle:=dmRN1.cxStyleStorniraniN;
   //  3:AStyle:=dmRN1.cxStylePrecekoreniN;
  //   4:AStyle:=dmRN1.cxNekontrolirani;
    end;

end;

procedure TfrmIzvestaiRN.cxGrid1DBTableView1TcxGridDBDataControllerTcxDataSummaryFooterSummaryItems0GetText(
  Sender: TcxDataSummaryItem; const AValue: Variant; AIsFooter: Boolean;
  var AText: string);
begin
  if dm.tblRabotenNalog.RecordCount > 0 then
     AText := '������ '+VarToStr(AValue)+' ��';
end;

procedure TfrmIzvestaiRN.cxGrid3DBTableView1FocusedRecordChanged(
  Sender: TcxCustomGridTableView; APrevFocusedRecord,
  AFocusedRecord: TcxCustomGridRecord; ANewItemRecordFocusingChanged: Boolean);
begin
   SetirajLukap(sender,dm.tblArtikalRN,txtVidArtikal,txtArtikal, cbNazivArtikal);
   SetirajLukap(sender,dm.tblAmbalaza,txtVidAmbalaza,txtAmbalaza, cbAmbalaza);
   dm.SetirajLukap(sender,dmMat.tblPartner,txtTipOdobril,txtOdobril, cbOdobril);
   ArtMerka;
end;

procedure TfrmIzvestaiRN.cxGrid3DBTableView1StylesGetContentStyle(
  Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord;
  AItem: TcxCustomGridTableItem; var AStyle: TcxStyle);
var  AStatus:Integer;
begin
  AStatus:=-1;
  if (ARecord<>nil) and (AItem<>nil) then
    if not VarIsNull(TcxCustomGridRecord(ARecord).Values[cxGrid3DBTableView1LINIJA.Index])
     then
     begin
       AStatus:=VarAsType(TcxCustomGridRecord(ARecord).Values[cxGrid3DBTableView1LINIJA.Index], varInteger);   AStatus:=0;
     end;
     if (VarAsType(TcxCustomGridRecord(ARecord).Values[cxGrid3DBTableView1LINIJA.Index], varInteger) = 1)
     //and (VarAsType(TcxCustomGridRecord(ARecord).Values[cxGrid1DBTableView1Precekoren.Index], varInteger) = 1)
     then
         AStatus:=1;
     //if (VarAsType(TcxCustomGridRecord(ARecord).Values[cxGrid1DBTableView1SOURCE_DOKUMENT.Index], varInteger) <> 0)
    // and (VarIsNull(TcxCustomGridRecord(ARecord).Values[cxGrid1DBTableView1DBColumn2.Index]))
    // then

    case AStatus of
     1:AStyle:=dm.cxStyle2;
   //  1:AStyle:=dmRN1.cxStyleZatvoreniN;

end;
end;

procedure TfrmIzvestaiRN.cxGrid3FocusedViewChanged(
  Sender: TcxCustomGrid; APrevFocusedView, AFocusedView: TcxCustomGridView);
begin
   if AFocusedView = cxGrid3DBTableView2 then
    begin
      OtvoriNormativ;
    end
    else
    if AFocusedView = cxGrid3DBTableView3 then
    begin
      OtvoriOperacija;
    end;
end;

procedure TfrmIzvestaiRN.cxGrid3LayoutChanged(Sender: TcxCustomGrid;
  AGridView: TcxCustomGridView);
begin
//    if cxGrid3Level2.Active then
//    begin
//      OtvoriNormativ;
//    end;
end;

procedure TfrmIzvestaiRN.cxGridDBTableView4FocusedRecordChanged(
  Sender: TcxCustomGridTableView; APrevFocusedRecord,
  AFocusedRecord: TcxCustomGridRecord; ANewItemRecordFocusingChanged: Boolean);
begin
   if dm.tblRNSurovini.State = dsBrowse then
   begin
  //   SetirajLukap(sender,dm.tblArtikalRN,txtVidSurovina,txtSurovina, cbSurovina);
   end;
end;

procedure TfrmIzvestaiRN.cxPageControl1Change(Sender: TObject);
begin
//  if ((dm.tblRNStavka.State in [dsInsert,dsEdit])
//  and (cxPageControl1.ActivePageIndex in [0,1,3])) or
//  ((dm.tblRabotenNalog.State in [dsInsert,dsEdit])
//  and (cxPageControl1.ActivePageIndex in [0,2,3])) then
//     aOtkazi.Execute;
   if cxPageControl1.ActivePageIndex = 3 then
   begin
   if dm.tblRNStavka.RecordCount>0 then
    begin
     dm.pRNNormativ.Close;
     dm.pRNNormativ.ParamByName('ID_RN').AsString := dm.tblRNStavkaID_RABOTEN_NALOG.AsString;
     dm.pRNNormativ.ParamByName('ID').AsString := '%';
     dm.pRNNormativ.Open;
    end
    else
    begin
     dm.pRNNormativ.Close;
    end;
//       SetirajLukap(sender,dm.tblArtikalRN,txtVidSurovina,txtSurovina, cbSurovina);
   end

   else
   if cxPageControl1.ActivePageIndex = 4 then
   begin
   if dm.tblRNStavka.RecordCount>0 then
    begin
     dm.pRNOperacija.Close;
     dm.pRNOperacija.ParamByName('ID_RN').AsString := dm.tblRNStavkaID_RABOTEN_NALOG.AsString;
     dm.pRNOperacija.ParamByName('ID').AsString := '%';   //dm.tblRNStavkaID.AsString;
     dm.pRNOperacija.Open;
    end
    else
      dm.pRNOperacija.Close;
    end

   else
   if cxPageControl1.ActivePageIndex = 0 then
   begin
       aOtvoreniRN.Enabled := true;
       aSiteRN.Enabled := true;
   end

   else
   if cxPageControl1.ActivePageIndex = 2 then
   begin
       gbRNStavki.Caption := '  ������ �� �� ��� '+dm.tblRabotenNalogBROJ.AsString+'/'+dm.tblRabotenNalogGODINA.AsString+'  ';
       dm.tblMatStatus.Close;
       dm.tblMatStatus.ParamByName('TABELA').AsString := 'MAN_RABOTEN_NALOG_STAVKA';
       dm.tblMatStatus.Open;
       dm.SetirajLukap(sender,dmMat.tblPartner,txtTipOdobril,txtOdobril, cbOdobril);
       ArtMerka;
   end

   else
   if cxPageControl1.ActivePageIndex = 1 then
   begin
      dm.tblMatStatus.Close;
      dm.tblMatStatus.ParamByName('TABELA').AsString := 'MAN_RABOTEN_NALOG';
      dm.tblMatStatus.Open;
      //cxDBButtonEdit1.Properties.Buttons[0].Caption := dm.tblRabotenNalogNAZIV_STATUS_STAVKI.AsString;
     // cxDBButtonEdit2.Properties.Buttons[0].Caption := dm.tblRabotenNalogNAZIV_STATUS_STAVKI_SLEDEN.AsString;
      //cxDBButtonEdit1.Style.TextStyle[cxDBButtonEdit1.Properties.Buttons[0].Caption].fsBold;
      dm.SetirajLukap(sender,dmMat.tblPartner,txtTipPartner,txtPartner, cbNazivPartner);
      gbRabotenNalog.Caption := '  '+dm.tblRabotenNalogNAZIV_STATUS.AsString+' ������� �����';
   end
   else
   if cxPageControl1.ActivePageIndex <> 0 then
   begin
       aOtvoreniRN.Enabled := False;
       aSiteRN.Enabled := False;
   end

end;

procedure TfrmIzvestaiRN.dxRibbon1TabChanging(Sender: TdxCustomRibbon;
  ANewTab: TdxRibbonTab; var Allow: Boolean);
begin
 if Self.Active then

   if ANewTab = dxRibbon1Tab2 then
   begin
     cbGodina.Visible:=ivNever;
   end
   else
   begin
     cbGodina.Visible:=ivAlways;
   end;
end;

procedure TfrmIzvestaiRN.gbZatvoriRNClick(Sender: TObject);
begin

end;

//  ����� �� �����
procedure TfrmIzvestaiRN.aZatvoriLinijaExecute(Sender: TObject);
begin
     if (dm.tblRabotenNalog.State = dsBrowse) and (dm.tblRNStavka.State = dsBrowse)
     and (dm.tblRabotenNalogSTATUS.Value = 1)
     and (dm.tblRNStavkaSTATUS.Value = 1) then
  begin
  // �� �� �������� ���ȣ�
  //�������� ���� ��� ��� ���� ����� �������

 if (dm.tblRNStavkaLINIJA.Value <> 0)then
  begin
     pImaLinija.Close;
     pImaLinija.ParamByName('linija').Value := dm.tblRNStavkaLINIJA.Value;
     pImaLinija.ExecQuery;
     if not (pImaLinija.FldByName['linija'].IsNull) then
     begin
     frmDaNe := TfrmDaNe.Create(self, '����������', '���� �� �� ������� �������?', 1);
       if (frmDaNe.ShowModal = mrYes) then
              begin
                dm.tblRNStavka.Edit;
                //dm.tblRNStavkaLINIJA.Value := 0;
                dm.tblRNStavkaSTATUS_LINIJA.Value := 2;
                dm.tblRNStavka.Post;
              end
              else
              begin
                Abort;
              end;
    end
              else
              begin
                ShowMessage('������� �� � �������!');
                Abort;
              end

  end
  end;
end;

procedure TfrmIzvestaiRN.aZatvoriRNExecute(Sender: TObject);
begin
  if (dm.tblRabotenNalog.State in [dsBrowse,dsInactive])
   and (dm.tblRNStavka.State in [dsBrowse, dsInactive])
   and (dm.tblRNSurovini.State in [dsBrowse, dsInactive]) then
   begin
     cxPageControl1.ActivePageIndex := 1;
     dm.SetirajLukap(sender,dmMat.tblPartner,txtTipPartner,txtPartner, cbNazivPartner);
     dPanel.Enabled := true;
     dm.tblRabotenNalog.Edit;
     dm.tblRabotenNalogSTATUS.Value := 2;
   end;
end;

procedure TfrmIzvestaiRN.aZatvoriStavkaExecute(Sender: TObject);
begin
 if (dm.tblRabotenNalog.State in [dsBrowse,dsInactive])
   and (dm.tblRNStavka.State in [dsBrowse, dsInactive])
   and (dm.tblRNSurovini.State in [dsBrowse, dsInactive]) then
   begin
     lPanelO.Enabled := true;
     gbRNStavki.Enabled := false;
     cxPageControl1.ActivePageIndex := 2;
     //dm.SetirajLukap(sender,dmMat.tblPartner,txtTipPartner,txtPartner, cbNazivPartner);
     dm.tblRNStavka.Edit;
     dm.tblRNStavkaSTATUS.Value := 2;
   end;
end;

procedure TfrmIzvestaiRN.aZavrsiRNExecute(Sender: TObject);
begin
   frmZavrsiRN := TfrmZavrsiRN.Create(Application);
   frmZavrsiRN.ShowModal;
   frmZavrsiRN.Free;

end;

procedure TfrmIzvestaiRN.Button1Click(Sender: TObject);
begin
  OpenPictureDialog1.Filter := 'JPEG Image File |*.jpg;*.jpeg';
  OpenPictureDialog1.Execute();
  if OpenPictureDialog1.FileName <> '' then
   //  dm.tblRNStavkaSLIKA.LoadFromFile(OpenPictureDialog1.FileName);
end;

procedure TfrmIzvestaiRN.cbGodinaChange(Sender: TObject);
begin
   OtvoreniRN;
end;

procedure TfrmIzvestaiRN.cbNovREChange(Sender: TObject);
begin
//  if cbNovRE.Text <> '' then
//  begin
//    frmDaNe := TfrmDaNe.Create(self, '�������', '���� �� ��������� �� �� '+cbNovRE.Text+ '?', 1);
//        if (frmDaNe.ShowModal <> mrYes) then
//            Abort
//        else
//        begin
//          re := cbNovRE.KeyValue;
//          dm.tblIzberiRN.Refresh;
//          aNovv.Execute;
//        end;
//  end;

end;

procedure TfrmIzvestaiRN.cbRekoltaPropertiesChange(Sender: TObject);
begin
   aOtvoreniRN.Execute;
end;

//	����� �� ���������� �� �������
procedure TfrmIzvestaiRN.aOtkaziExecute(Sender: TObject);
begin
if cxPageControl1.ActivePageIndex in [0,1] then
  begin
    if (cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) then
        begin
            ModalResult := mrCancel;
            Close();
        end
        else
        begin
            cxGrid1DBTableView1.DataController.DataSet.Cancel;
            gbRabotenNalog.Caption := '  ������� �����  ';
//            cbInformacii.Checked := false;
            RestoreControls(dPanel);
            dPanel.Enabled := false;
            //lPanel.Enabled := true;
            Panel1.Enabled:=true;
            if cxPageControl1.ActivePage = tsRN then ActiveControl := btStavki;
            //cxPageControl1.ActivePageIndex := 0;
          //  cxGrid4.SetFocus;
        end;
  end
  else
  //begin
   if cxPageControl1.ActivePageIndex = 2 then
     begin
        if (dm.tblRNStavka.State = dsBrowse) then
        begin
            ModalResult := mrCancel;
            Close();
        end
        else
        begin
            dm.tblRNStavka.Cancel;
            RestoreControls(lPanelo);
            lPanelO.Enabled := false;
            dPanelO.Enabled := true;
          //  cxPageControl1.ActivePageIndex := 0;
            dm.tblIzvedenaEM.Close;
            dm.tblIzvedenaEM.ParamByName('vid_artikal').AsString := '%';
            dm.tblIzvedenaEM.ParamByName('artikal').AsString := '%';
            dm.tblIzvedenaEM.Open;
            cxGrid3.SetFocus;
        end;
     end
     else
       //begin
   if cxPageControl1.ActivePageIndex = 3 then
    if dm.tblRNSurovini.State = dsBrowse then
        begin
            ModalResult := mrCancel;
            Close();
        end
           else
        begin
            dm.tblRNSurovini.Cancel;
      //      RestoreControls(PanelSurovini);
            //PanelO.Enabled := false;
            gbSurovini.Enabled := true;
          //  cxPageControl1.ActivePageIndex := 0;
            cxGrid7.SetFocus;
        end;

 //end;
end;

procedure TfrmIzvestaiRN.aOtvoreniRNExecute(Sender: TObject);
begin
    OtvoreniRN;
end;

//----------------------------------------------------------------------------------
// ��������� �� ��������, ���������� �� ��������� � �������� �� Print ��������
//----------------------------------------------------------------------------------

//	����� �� �������� �� �������� �� �������
procedure TfrmIzvestaiRN.aPageSetupExecute(Sender: TObject);
begin
  dxComponentPrinter1Link1.PageSetup;
end;

//	����� �� ������� �� ������
procedure TfrmIzvestaiRN.aPecatiRNExecute(Sender: TObject);
begin
if not (dm.tblRabotenNalog.State in [dsEdit,dsInsert]) and (dm.tblRNStavka.State = dsBrowse) then
 begin
  dmRes.Spremi('MAN', 1);
  dmKon.tblSqlReport.ParamByName('id').AsInteger :=dm.tblRabotenNalogID.Value;;
  dmKon.tblSqlReport.Open;
  dmRes.frxReport1.ShowReport();
 end;
end;

procedure TfrmIzvestaiRN.aPecatiTabelaExecute(Sender: TObject);
begin
  dxComponentPrinter1Link1.ReportTitle.Text := Caption;

  dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Clear;
  dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Add(dmkon.firma_naziv);
  dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Add(DateTimeToStr(Now));

  dxComponentPrinter1Link1.PrinterPage.PageHeader.LeftTitle.Clear;
  //dxComponentPrinter1Link1.PrinterPage.PageHeader.LeftTitle.Add('������� : ' + TipSifra.Text + '-' + IdSifra.Text + ' ' + Partner.Text);

  dxComponentPrinter1.Preview(true, dxComponentPrinter1Link1);
end;

//	����� �� ���������� �� ��������� �� �������
procedure TfrmIzvestaiRN.aPodesuvanjePecatenjeExecute(Sender: TObject);
begin
  dxComponentPrinter1Link1.DesignReport();
end;

procedure TfrmIzvestaiRN.aPogledPorackaRNExecute(Sender: TObject);
begin
  frmPogledPorackiRN := TfrmPogledPorackiRN.Create(Application);
  frmPogledPorackiRN.cxGrid1Level1.Caption := '������� �� ������� ����� ��� '+dm.tblRabotenNalogBROJ.AsString+'/'+dm.tblRabotenNalogGODINA.AsString+'  ';
  frmPogledPorackiRN.ShowModal;
  frmPogledPorackiRN.Free;

  OtvoreniRN;
end;

procedure TfrmIzvestaiRN.aPogledRNExecute(Sender: TObject);
begin
  cxPageControl1.ActivePageIndex := 1;
  //cxGrid4.SetFocus;
end;

procedure TfrmIzvestaiRN.aPogledZavisniRNExecute(Sender: TObject);
begin
  dm.tblRabotenNalog.Close;
  dm.tblRabotenNalog.ParamByName('status').AsString := '1';
  dm.tblRabotenNalog.ParamByName('godina').AsString := cbGodina.EditValue;
  dm.tblRabotenNalog.ParamByName('site').AsString := '0';
  dm.tblRabotenNalog.ParamByName('re').AsInteger := dmkon.UserRE;
  dm.tblRabotenNalog.Open;

  dm.tblRNStavka.Open;
  dm.pRNNormativ.Open;
  dm.tblRNSurovini.Open;
  dm.tblArtikalRN.Close;
  dm.tblArtikalRN.Open;

  dm.tblIzberiRN.Close;
  dm.tblIzberiRN.ParamByName('status').AsString := '1';
  dm.tblIzberiRN.ParamByName('godina').AsString := cbGodina.EditValue;
  dm.tblIzberiRN.ParamByName('id_re').AsString := '%';
  dm.tblIzberiRN.Open;

  cxGrid1Level1.Caption := '������ �� ������� �� �� �����'
end;

procedure TfrmIzvestaiRN.aSnimiPecatenjeExecute(Sender: TObject);
begin
  zacuvajPrintVoBaza(Name,cxGrid1DBTableView1.Name,dxComponentPrinter1Link1);
end;

procedure TfrmIzvestaiRN.aUrediTextOpisExecute(Sender: TObject);
begin
  frmNotepad :=TfrmNotepad.Create(Application);
  frmnotepad.LoadText(dm.tblRNStavkaZABELESKA.Value);
  frmNotepad.ShowModal;
  if frmNotepad.ModalResult=mrOk then
  begin
	   dm.tblRNStavkaZABELESKA.Value := frmNotepad.ReturnText;
  end;
  frmNotepad.Free;
end;

//	����� �� ������ �� ����������� �������� �� ������� (Utils.pas)
procedure TfrmIzvestaiRN.aBrisiPodesuvanjePecatenjeExecute(Sender: TObject);
begin
  brisiPrintOdBaza(Name,cxGrid1DBTableView1.Name, dxComponentPrinter1Link1);
end;

procedure TfrmIzvestaiRN.aBrLinijaExecute(Sender: TObject);
begin
     frmBrLinija :=TfrmBrLinija.Create(self);
     frmBrLinija.ShowModal;
     frmBrLinija.Free;
end;

procedure TfrmIzvestaiRN.ActionList1Execute(Action: TBasicAction;
  var Handled: Boolean);
begin
end;

//  ����� �� ����� �� ����� �� ������������� �� �������� � ��������������� ��
//  �������(������ ���������� ��������)
procedure TfrmIzvestaiRN.aDizajnerRNExecute(Sender: TObject);
begin
if not (dm.tblRabotenNalog.State in [dsEdit,dsInsert]) and (dm.tblRNStavka.State = dsBrowse) then
 begin
  dmRes.Spremi('MAN', 1);
  dmKon.tblSqlReport.ParamByName('id').AsInteger :=dm.tblRabotenNalogID.Value;;
  dmKon.tblSqlReport.Open;
  dmRes.frxReport1.DesignReport();
 end;
end;

procedure TfrmIzvestaiRN.aDodadiPRNExecute(Sender: TObject);
begin
   frmIzberiPoracki := TfrmIzberiPoracki.Create(Application);
   frmIzberiPoracki.godina := cbGodina.EditValue;
   frmIzberiPoracki.ShowModal;
   frmIzberiPoracki.Free;

   OtvoreniRN;

end;

procedure TfrmIzvestaiRN.aFormConfigExecute(Sender: TObject);
begin
  frmFormConfig := TfrmFormConfig.Create(Application);
  frmFormConfig.formPtr := Addr(Self);
  frmFormConfig.ShowModal;
  frmFormConfig.Free;
end;

//----------------------------------------------------------------------------------
// ���� -> ��������� �� ��������, ���������� �� ��������� � �������� �� Print ��������
//----------------------------------------------------------------------------------

// ��������� �� ���������� �� ��������� � �������� �� Print �������� �� .ini ��������
procedure TfrmIzvestaiRN.zacuvajPrintVoIni(ime:AnsiString);
var
  AStream: TFileStream;
begin
  AStream := TFileStream.Create(dmKon.RabotenDir + ime + 'Print.ini', fmCreate);
  try
    AStream.WriteComponent(dxComponentPrinter1Link1);
  finally
    AStream.Free;
  end;

end;

// ��������� �� ������ �� ��������� � �������� �� Print �������� �������� �� .ini ��������
procedure TfrmIzvestaiRN.procitajPrintOdIni(ime:AnsiString);
var
  AStream: TFileStream;
begin
  AStream := TFileStream.Create(dmKon.RabotenDir + ime + 'Print.ini', fmOpenRead);
  try
    dxComponentPrinter1Link1.RestoreDefaults;
    AStream.ReadComponent(dxComponentPrinter1Link1);
  finally
    AStream.Free;
  end;
end;

procedure TfrmIzvestaiRN.SetirajLukap(sender:TObject; tabela:TDataSet; tip:TcxDBTextEdit; sifra:TcxDBTextEdit; lukap:TcxExtLookupComboBox);
begin
       if (tip.Text <>'') and (sifra.Text<>'')  then
       begin
         lukap.EditValue := VarArrayOf([StrToInt(tip.text), StrToInt(sifra.Text)]);
       end
       else
         lukap.Clear;
end;

procedure TfrmIzvestaiRN.CustomFields();
begin
  custom1 := getCustomField('MAN_RABOTEN_NALOG_STAVKA','CUSTOM1');
  custom2 := getCustomField('MAN_RABOTEN_NALOG_STAVKA','CUSTOM2');
  custom3 := getCustomField('MAN_RABOTEN_NALOG_STAVKA','CUSTOM3');

  if custom1 <> '' then
  begin
    cxGrid3DBTableView1CUSTOM1.Visible := true;
    cxGrid3DBTableView1CUSTOM1.VisibleForCustomization := true;
    cxGrid3DBTableView1CUSTOM1.Caption := custom1;

    cxGrid3DBTableView1CUSTOM1.Visible := false;
  end;
  if custom2 <> '' then
  begin
    cxGrid3DBTableView1CUSTOM2.Visible := true;
    cxGrid3DBTableView1CUSTOM2.VisibleForCustomization := true;
    cxGrid3DBTableView1CUSTOM2.Caption := custom2;

    cxGrid3DBTableView1CUSTOM2.Visible := false;
  end;
  if custom3 <> '' then
  begin
    cxGrid3DBTableView1CUSTOM3.Visible := true;
    cxGrid3DBTableView1CUSTOM3.VisibleForCustomization := true;
    cxGrid3DBTableView1CUSTOM3.Caption := custom3;

    lblCustom3.Visible := true;
    lblCustom3.Caption := custom3 + ' :';
    txtCustom3.Visible := true;

    cxGrid3DBTableView1CUSTOM3.Visible := false;
  end;
end;

procedure TfrmIzvestaiRN.StatusBoi;
begin
 // if dm.tblRabotenNalogSTATUS.Value then

end;

procedure TfrmIzvestaiRN.ArtMerka;
begin
if (dm.tblRNStavka.RecordCount > 0) and (txtVidArtikal.Text <> '') then
  begin
  qArtikalMerka.Close;
  qArtikalMerka.ParamByName('vid_artikal').AsString := txtVidArtikal.Text;
  qArtikalMerka.ParamByName('artikal').AsString := txtArtikal.Text;;
  qArtikalMerka.ExecQuery;
  merka := qArtikalMerka.FldByName['merka'].AsString;
  barkod := qArtikalMerka.FldByName['barkod'].AsString;
  lblMerka.Caption := merka;
  end;
end;

end.
