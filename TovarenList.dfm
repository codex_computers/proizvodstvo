object frmTovarenList: TfrmTovarenList
  Left = 128
  Top = 212
  ActiveControl = cxGrid1
  BorderIcons = [biSystemMenu, biMaximize]
  Caption = #1058#1086#1074#1072#1088#1077#1085' '#1083#1080#1089#1090
  ClientHeight = 662
  ClientWidth = 954
  Color = clBtnFace
  Constraints.MinHeight = 500
  Constraints.MinWidth = 610
  Font.Charset = RUSSIAN_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  ShowHint = True
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label4: TLabel
    Left = 31
    Top = 21
    Width = 46
    Height = 13
    Caption = #1064#1080#1092#1088#1072' :'
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clRed
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object lPanel: TPanel
    Left = 0
    Top = 126
    Width = 954
    Height = 266
    Align = alClient
    BevelInner = bvLowered
    BevelOuter = bvSpace
    TabOrder = 0
    object cxGrid1: TcxGrid
      Left = 2
      Top = 2
      Width = 950
      Height = 262
      Align = alClient
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      object cxGrid1DBTableView1: TcxGridDBTableView
        OnDblClick = cxGrid1DBTableView1DblClick
        OnKeyDown = cxGrid1DBTableView1KeyDown
        OnKeyPress = cxGrid1DBTableView1KeyPress
        OnFilterCustomization = cxGrid1DBTableView1FilterCustomization
        DataController.DataSource = dm.dsTovarenList
        DataController.Filter.Options = [fcoCaseInsensitive]
        DataController.Options = [dcoAnsiSort, dcoAssignGroupingValues, dcoAssignMasterDetailKeys, dcoSaveExpanding]
        DataController.Summary.DefaultGroupSummaryItems = <
          item
            Kind = skCount
            Position = spFooter
            Column = cxGrid1DBTableView1BROJ
          end>
        DataController.Summary.FooterSummaryItems = <
          item
            Kind = skCount
            Column = cxGrid1DBTableView1BROJ
          end>
        DataController.Summary.SummaryGroups = <>
        Filtering.ColumnFilteredItemsList = True
        FilterRow.InfoText = #1050#1083#1080#1082#1085#1080' '#1086#1074#1076#1077' '#1079#1072' '#1076#1072' '#1076#1077#1092#1080#1085#1080#1088#1072#1096' '#1092#1080#1083#1090#1077#1088
        FilterRow.Visible = True
        FilterRow.ApplyChanges = fracImmediately
        OptionsBehavior.CellHints = True
        OptionsBehavior.IncSearch = True
        OptionsCustomize.ColumnsQuickCustomization = True
        OptionsData.Deleting = False
        OptionsData.Editing = False
        OptionsData.Inserting = False
        OptionsSelection.HideFocusRectOnExit = False
        OptionsView.NoDataToDisplayInfoText = '<'#1053#1077#1084#1072' '#1090#1086#1074#1072#1088#1085#1080' '#1083#1080#1089#1090#1086#1074#1080'>'
        OptionsView.Footer = True
        OptionsView.GroupFooters = gfVisibleWhenExpanded
        object cxGrid1DBTableView1ID: TcxGridDBColumn
          DataBinding.FieldName = 'ID'
          Visible = False
        end
        object cxGrid1DBTableView1RE: TcxGridDBColumn
          DataBinding.FieldName = 'RE'
          Visible = False
        end
        object cxGrid1DBTableView1BROJ: TcxGridDBColumn
          DataBinding.FieldName = 'BROJ'
        end
        object cxGrid1DBTableView1GODINA: TcxGridDBColumn
          DataBinding.FieldName = 'GODINA'
          Visible = False
        end
        object cxGrid1DBTableView1DATUM: TcxGridDBColumn
          DataBinding.FieldName = 'DATUM'
        end
        object cxGrid1DBTableView1TP: TcxGridDBColumn
          DataBinding.FieldName = 'TP'
        end
        object cxGrid1DBTableView1P: TcxGridDBColumn
          DataBinding.FieldName = 'P'
        end
        object cxGrid1DBTableView1PARTNER_NAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'PARTNER_NAZIV'
          Width = 300
        end
        object cxGrid1DBTableView1MESTO_NAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'MESTO_NAZIV'
          Width = 150
        end
        object cxGrid1DBTableView1PREVOZNIK: TcxGridDBColumn
          DataBinding.FieldName = 'PREVOZNIK'
          Width = 150
        end
        object cxGrid1DBTableView1PREVOZNIK_ADRESA: TcxGridDBColumn
          DataBinding.FieldName = 'PREVOZNIK_ADRESA'
          Width = 150
        end
        object cxGrid1DBTableView1PREVOZNIK_MESTO: TcxGridDBColumn
          DataBinding.FieldName = 'PREVOZNIK_MESTO'
          Width = 150
        end
        object cxGrid1DBTableView1PREVOZNIK_REGISTRACIJA: TcxGridDBColumn
          DataBinding.FieldName = 'PREVOZNIK_REGISTRACIJA'
          Width = 134
        end
        object cxGrid1DBTableView1VOZILO: TcxGridDBColumn
          DataBinding.FieldName = 'VOZILO'
        end
        object cxGrid1DBTableView1VOZ_MODEL: TcxGridDBColumn
          DataBinding.FieldName = 'VOZ_MODEL'
          Width = 200
        end
        object cxGrid1DBTableView1VOZ_REG_BR: TcxGridDBColumn
          DataBinding.FieldName = 'VOZ_REG_BR'
          Visible = False
          Width = 100
        end
        object cxGrid1DBTableView1UTOVAR_MESTO: TcxGridDBColumn
          DataBinding.FieldName = 'UTOVAR_MESTO'
          Width = 150
        end
        object cxGrid1DBTableView1UTOVAR_DATA: TcxGridDBColumn
          DataBinding.FieldName = 'UTOVAR_DATA'
          Width = 82
        end
        object cxGrid1DBTableView1ISTOVAR_MESTO: TcxGridDBColumn
          DataBinding.FieldName = 'ISTOVAR_MESTO'
          Width = 150
        end
        object cxGrid1DBTableView1ISTOVAR_DATA: TcxGridDBColumn
          DataBinding.FieldName = 'ISTOVAR_DATA'
          Width = 86
        end
        object cxGrid1DBTableView1ZABELESKA: TcxGridDBColumn
          DataBinding.FieldName = 'ZABELESKA'
          Width = 250
        end
        object cxGrid1DBTableView1INSTRUKCII: TcxGridDBColumn
          DataBinding.FieldName = 'INSTRUKCII'
          Width = 250
        end
        object cxGrid1DBTableView1STATUS_ID: TcxGridDBColumn
          DataBinding.FieldName = 'STATUS_ID'
          Visible = False
          Width = 72
        end
        object cxGrid1DBTableView1STATUS: TcxGridDBColumn
          DataBinding.FieldName = 'STATUS'
        end
        object cxGrid1DBTableView1STATUS_NAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'STATUS_NAZIV'
          Width = 150
        end
        object cxGrid1DBTableView1STATUS_BOJA: TcxGridDBColumn
          DataBinding.FieldName = 'STATUS_BOJA'
          Visible = False
          Width = 73
        end
        object cxGrid1DBTableView1CUSTOM1: TcxGridDBColumn
          DataBinding.FieldName = 'CUSTOM1'
          Visible = False
          Width = 150
        end
        object cxGrid1DBTableView1CUSTOM2: TcxGridDBColumn
          DataBinding.FieldName = 'CUSTOM2'
          Visible = False
          Width = 150
        end
        object cxGrid1DBTableView1CUSTOM3: TcxGridDBColumn
          DataBinding.FieldName = 'CUSTOM3'
          Visible = False
          Width = 150
        end
        object cxGrid1DBTableView1TS_INS: TcxGridDBColumn
          DataBinding.FieldName = 'TS_INS'
          Visible = False
        end
        object cxGrid1DBTableView1TS_UPD: TcxGridDBColumn
          DataBinding.FieldName = 'TS_UPD'
          Visible = False
        end
        object cxGrid1DBTableView1USR_INS: TcxGridDBColumn
          DataBinding.FieldName = 'USR_INS'
          Visible = False
          Width = 150
        end
        object cxGrid1DBTableView1USR_UPD: TcxGridDBColumn
          DataBinding.FieldName = 'USR_UPD'
          Visible = False
          Width = 150
        end
      end
      object cxGrid1Level1: TcxGridLevel
        GridView = cxGrid1DBTableView1
      end
    end
  end
  object dPanel: TPanel
    Left = 0
    Top = 392
    Width = 954
    Height = 247
    Align = alBottom
    BevelInner = bvLowered
    BevelOuter = bvSpace
    Color = 15790320
    Enabled = False
    ParentBackground = False
    PopupMenu = PopupMenu1
    TabOrder = 1
    DesignSize = (
      954
      247)
    object Label1: TLabel
      Left = 17
      Top = 11
      Width = 31
      Height = 13
      Caption = #1041#1088#1086#1112' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label6: TLabel
      Left = 83
      Top = 11
      Width = 45
      Height = 13
      Alignment = taRightJustify
      Caption = #1044#1072#1090#1091#1084' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lblPartnerK: TLabel
      Left = 189
      Top = 11
      Width = 104
      Height = 13
      Alignment = taRightJustify
      Caption = #1055#1088#1080#1084#1072#1095'/'#1050#1091#1087#1091#1074#1072#1095' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clBlue
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label12: TLabel
      Left = 17
      Top = 143
      Width = 71
      Height = 13
      Caption = #1047#1072#1073#1077#1083#1077#1096#1082#1072' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clBlue
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label13: TLabel
      Left = 363
      Top = 143
      Width = 75
      Height = 13
      Caption = #1048#1085#1089#1090#1088#1091#1082#1094#1080#1080' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clBlue
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Broj: TcxDBTextEdit
      Tag = 1
      Left = 17
      Top = 26
      BeepOnEnter = False
      DataBinding.DataField = 'BROJ'
      DataBinding.DataSource = dm.dsTovarenList
      ParentFont = False
      Properties.BeepOnError = True
      Style.Shadow = False
      TabOrder = 0
      OnKeyDown = EnterKakoTab
      Width = 60
    end
    object OtkaziButton: TcxButton
      Left = 863
      Top = 207
      Width = 75
      Height = 25
      Action = aOtkazi
      Anchors = [akRight, akBottom]
      Colors.Pressed = clGradientActiveCaption
      TabOrder = 11
    end
    object ZapisiButton: TcxButton
      Left = 782
      Top = 207
      Width = 75
      Height = 25
      Action = aZapisi
      Anchors = [akRight, akBottom]
      Colors.Pressed = clGradientActiveCaption
      TabOrder = 10
    end
    object cxGroupBoxPrevoznik: TcxGroupBox
      Left = 17
      Top = 53
      Caption = #1055#1056#1045#1042#1054#1047#1053#1048#1050
      TabOrder = 5
      Height = 84
      Width = 688
      object Label2: TLabel
        Left = 291
        Top = 25
        Width = 40
        Height = 13
        Caption = #1053#1072#1079#1080#1074' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clBlue
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label3: TLabel
        Left = 238
        Top = 52
        Width = 93
        Height = 13
        Caption = #1040#1076#1088#1077#1089#1072'/'#1052#1077#1089#1090#1086' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clBlue
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label11: TLabel
        Left = 14
        Top = 52
        Width = 84
        Height = 13
        Caption = #1056#1077#1075#1080#1089#1090#1088#1072#1094#1080#1112#1072' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clBlue
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label5: TLabel
        Left = 52
        Top = 25
        Width = 46
        Height = 13
        Caption = #1042#1086#1079#1080#1083#1086' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clBlue
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Prevoznik: TcxDBTextEdit
        Left = 337
        Top = 22
        BeepOnEnter = False
        DataBinding.DataField = 'PREVOZNIK'
        DataBinding.DataSource = dm.dsTovarenList
        ParentFont = False
        Properties.BeepOnError = True
        Style.Shadow = False
        TabOrder = 1
        OnKeyDown = EnterKakoTab
        Width = 332
      end
      object PrevoznikAdresa: TcxDBTextEdit
        Left = 337
        Top = 49
        BeepOnEnter = False
        DataBinding.DataField = 'PREVOZNIK_ADRESA'
        DataBinding.DataSource = dm.dsTovarenList
        ParentFont = False
        Properties.BeepOnError = True
        Style.Shadow = False
        TabOrder = 3
        OnKeyDown = EnterKakoTab
        Width = 228
      end
      object PrevoznikRegistracija: TcxDBTextEdit
        Left = 104
        Top = 49
        BeepOnEnter = False
        DataBinding.DataField = 'PREVOZNIK_REGISTRACIJA'
        DataBinding.DataSource = dm.dsTovarenList
        ParentFont = False
        Properties.BeepOnError = True
        Style.Shadow = False
        TabOrder = 2
        OnKeyDown = EnterKakoTab
        Width = 128
      end
      object PrevoznikMesto: TcxDBTextEdit
        Left = 571
        Top = 49
        BeepOnEnter = False
        DataBinding.DataField = 'PREVOZNIK_MESTO'
        DataBinding.DataSource = dm.dsTovarenList
        ParentFont = False
        Properties.BeepOnError = True
        Style.Shadow = False
        TabOrder = 4
        OnKeyDown = EnterKakoTab
        Width = 98
      end
      object lcbVozilo: TcxDBLookupComboBox
        Left = 104
        Top = 22
        BeepOnEnter = False
        DataBinding.DataField = 'VOZILO'
        DataBinding.DataSource = dm.dsTovarenList
        Properties.DropDownAutoSize = True
        Properties.DropDownSizeable = True
        Properties.ImmediatePost = True
        Properties.KeyFieldNames = 'ID'
        Properties.ListColumns = <
          item
            FieldName = 'ID'
          end
          item
            FieldName = 'MODEL'
          end
          item
            FieldName = 'REG_BROJ'
          end>
        Properties.ListFieldIndex = 1
        Properties.ListOptions.AnsiSort = True
        Properties.ListOptions.SyncMode = True
        Properties.ListSource = dmMat.dsVozilo
        TabOrder = 0
        OnExit = lcbVoziloExit
        OnKeyDown = EnterKakoTab
        Width = 128
      end
    end
    object Datum: TcxDBDateEdit
      Tag = 1
      Left = 83
      Top = 26
      BeepOnEnter = False
      DataBinding.DataField = 'DATUM'
      DataBinding.DataSource = dm.dsTovarenList
      TabOrder = 1
      OnKeyDown = EnterKakoTab
      Width = 100
    end
    object TipPartner: TcxDBTextEdit
      Left = 189
      Top = 26
      BeepOnEnter = False
      DataBinding.DataField = 'TP'
      DataBinding.DataSource = dm.dsTovarenList
      TabOrder = 2
      OnKeyDown = EnterKakoTab
      Width = 60
    end
    object Partner: TcxDBTextEdit
      Left = 255
      Top = 26
      BeepOnEnter = False
      DataBinding.DataField = 'P'
      DataBinding.DataSource = dm.dsTovarenList
      Properties.OnChange = PartnerPropertiesChange
      TabOrder = 3
      OnKeyDown = EnterKakoTab
      Width = 60
    end
    object cxExtPartner: TcxExtLookupComboBox
      Left = 321
      Top = 26
      BeepOnEnter = False
      TabOrder = 4
      OnExit = cxExtPartnerExit
      OnKeyDown = EnterKakoTab
      Width = 384
    end
    object cxGroupBoxUtovar: TcxGroupBox
      Left = 717
      Top = 11
      Caption = #1059#1058#1054#1042#1040#1056
      TabOrder = 6
      Height = 83
      Width = 221
      object Label7: TLabel
        Left = 12
        Top = 24
        Width = 43
        Height = 13
        Caption = #1052#1077#1089#1090#1086' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clBlue
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label8: TLabel
        Left = 10
        Top = 51
        Width = 45
        Height = 13
        Alignment = taRightJustify
        Caption = #1044#1072#1090#1091#1084' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clBlue
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object UtovarMesto: TcxDBTextEdit
        Left = 61
        Top = 21
        BeepOnEnter = False
        DataBinding.DataField = 'UTOVAR_MESTO'
        DataBinding.DataSource = dm.dsTovarenList
        ParentFont = False
        Properties.BeepOnError = True
        Style.Shadow = False
        TabOrder = 0
        OnKeyDown = EnterKakoTab
        Width = 150
      end
      object UtovarDatum: TcxDBDateEdit
        Left = 61
        Top = 48
        BeepOnEnter = False
        DataBinding.DataField = 'UTOVAR_DATA'
        DataBinding.DataSource = dm.dsTovarenList
        TabOrder = 1
        OnKeyDown = EnterKakoTab
        Width = 100
      end
    end
    object cxGroupBoxIstovar: TcxGroupBox
      Left = 717
      Top = 100
      Caption = #1048#1057#1058#1054#1042#1040#1056
      TabOrder = 7
      Height = 83
      Width = 221
      object Label9: TLabel
        Left = 12
        Top = 24
        Width = 43
        Height = 13
        Caption = #1052#1077#1089#1090#1086' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clBlue
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object Label10: TLabel
        Left = 10
        Top = 51
        Width = 45
        Height = 13
        Alignment = taRightJustify
        Caption = #1044#1072#1090#1091#1084' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clBlue
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object IstovarMesto: TcxDBTextEdit
        Left = 61
        Top = 21
        BeepOnEnter = False
        DataBinding.DataField = 'ISTOVAR_MESTO'
        DataBinding.DataSource = dm.dsTovarenList
        ParentFont = False
        Properties.BeepOnError = True
        Style.Shadow = False
        TabOrder = 0
        OnKeyDown = EnterKakoTab
        Width = 150
      end
      object IstovarDatum: TcxDBDateEdit
        Left = 61
        Top = 48
        BeepOnEnter = False
        DataBinding.DataField = 'ISTOVAR_DATA'
        DataBinding.DataSource = dm.dsTovarenList
        TabOrder = 1
        OnKeyDown = EnterKakoTab
        Width = 100
      end
    end
    object Zabeleska: TcxDBMemo
      Left = 17
      Top = 158
      DataBinding.DataField = 'ZABELESKA'
      DataBinding.DataSource = dm.dsTovarenList
      Properties.ScrollBars = ssVertical
      Properties.WantReturns = False
      TabOrder = 8
      OnKeyDown = EnterKakoTab
      Height = 67
      Width = 340
    end
    object Instrukcii: TcxDBMemo
      Left = 363
      Top = 158
      DataBinding.DataField = 'INSTRUKCII'
      DataBinding.DataSource = dm.dsTovarenList
      Properties.ScrollBars = ssVertical
      Properties.WantReturns = False
      TabOrder = 9
      OnKeyDown = EnterKakoTab
      Height = 67
      Width = 342
    end
  end
  object dxRibbon1: TdxRibbon
    Left = 0
    Top = 0
    Width = 954
    Height = 126
    BarManager = dxBarManager1
    ColorSchemeName = 'Blue'
    Contexts = <>
    TabOrder = 2
    TabStop = False
    object dxRibbon1Tab1: TdxRibbonTab
      Active = True
      Caption = #1052#1077#1085#1080
      Groups = <
        item
          Caption = #1058#1086#1074#1072#1088#1077#1085' '#1083#1080#1089#1090
          ToolbarName = 'dxBarManager1Bar1'
        end
        item
          ToolbarName = 'dxBarManager1Bar2'
        end
        item
          ToolbarName = 'dxBarManager1Bar3'
        end>
      Index = 0
    end
    object dxRibbon1Tab2: TdxRibbonTab
      Caption = #1055#1086#1076#1077#1089#1091#1074#1072#1114#1072
      Groups = <
        item
          ToolbarName = 'dxBarManager1Bar4'
        end
        item
          ToolbarName = 'dxBarManager1BarTabela'
        end>
      Index = 1
    end
  end
  object StatusBar1: TdxRibbonStatusBar
    Left = 0
    Top = 639
    Width = 954
    Height = 23
    Panels = <
      item
        PanelStyleClassName = 'TdxStatusBarTextPanelStyle'
        Text = 
          ' F5 - '#1053#1086#1074', F6 - '#1040#1078#1091#1088#1080#1088#1072#1112', F7 - '#1054#1089#1074#1077#1078#1080', F8 - '#1041#1088#1080#1096#1080', F9 - '#1047#1072#1087#1080#1096#1080', ' +
          'Esc - '#1054#1090#1082#1072#1078#1080' / '#1048#1079#1083#1077#1079
      end>
    Ribbon = dxRibbon1
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clDefault
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
  end
  object cxGridPopupMenu1: TcxGridPopupMenu
    Grid = cxGrid1
    PopupMenus = <>
    Left = 256
    Top = 216
  end
  object PopupMenu1: TPopupMenu
    Left = 376
    Top = 208
    object N1: TMenuItem
      Caption = #1057#1085#1080#1084#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      ShortCut = 49235
      OnClick = SaveToIniFileExecute
    end
    object Excel1: TMenuItem
      Caption = #1057#1085#1080#1084#1080' '#1075#1086' '#1074#1086' Excel'
      ShortCut = 49221
    end
  end
  object dxBarManager1: TdxBarManager
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Segoe UI'
    Font.Style = []
    Categories.Strings = (
      'Default')
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    ImageOptions.Images = dmRes.cxSmallImages
    ImageOptions.LargeImages = dmRes.cxLargeImages
    PopupMenuLinks = <>
    UseSystemFont = True
    Left = 456
    Top = 208
    DockControlHeights = (
      0
      0
      0
      0)
    object dxBarManager1Bar1: TdxBar
      Caption = 'Custom 1'
      CaptionButtons = <>
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 715
      FloatTop = 157
      FloatClientWidth = 51
      FloatClientHeight = 120
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton1'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton2'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton10'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarLargeButton3'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar2: TdxBar
      Caption = #1040#1082#1094#1080#1080
      CaptionButtons = <>
      DockedLeft = 240
      DockedTop = 0
      FloatLeft = 789
      FloatTop = 120
      FloatClientWidth = 51
      FloatClientHeight = 120
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButtonSoberi'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton5'
        end
        item
          Visible = True
          ItemName = 'dxBarSubItem1'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar3: TdxBar
      Caption = #1048#1079#1083#1077#1079
      CaptionButtons = <>
      DockedLeft = 485
      DockedTop = 0
      FloatLeft = 718
      FloatTop = 62
      FloatClientWidth = 51
      FloatClientHeight = 80
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton7'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton8'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar4: TdxBar
      Caption = #1055#1077#1095#1072#1090#1077#1114#1077
      CaptionButtons = <>
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 676
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton12'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton15'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton9'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarLargeButton13'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1BarTabela: TdxBar
      Caption = #1058#1072#1073#1077#1083#1072
      CaptionButtons = <>
      DockedLeft = 363
      DockedTop = 0
      FloatLeft = 767
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLBtnSnimiIzgled'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarLBtnBrisiIzgled'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarLargeButton2: TdxBarLargeButton
      Action = aAzuriraj
      Category = 0
    end
    object dxBarLargeButton3: TdxBarLargeButton
      Action = aBrisi
      Category = 0
    end
    object dxBarLargeButton4: TdxBarLargeButton
      Action = aSnimiIzgled
      Category = 0
      ScreenTip = tipSnimiIzgled
    end
    object dxBarLargeButton5: TdxBarLargeButton
      Action = aZacuvajExcel
      Category = 0
      ScreenTip = tipZacuvajExcel
    end
    object dxBarLargeButton6: TdxBarLargeButton
      Action = aPecatiTabela
      Caption = #1055#1077#1095#1072#1090#1080' '#1090#1072#1073#1077#1083#1072
      Category = 0
    end
    object dxBarLargeButton7: TdxBarLargeButton
      Action = aHelp
      Category = 0
    end
    object dxBarLargeButton8: TdxBarLargeButton
      Action = aIzlez
      Category = 0
    end
    object dxBarLargeButton1: TdxBarLargeButton
      Action = aNov
      Category = 0
    end
    object dxBarLargeButton9: TdxBarLargeButton
      Action = aSnimiPecatenje
      Category = 0
      ScreenTip = tipSnimiKonf
    end
    object ribbonFilterReset: TdxBarLargeButton
      Caption = #1056#1077#1089#1077#1090#1080#1088#1072#1112' '#1092#1080#1083#1090#1077#1088
      Category = 0
      Hint = #1056#1077#1089#1077#1090#1080#1088#1072#1112' '#1092#1080#1083#1090#1077#1088
      Visible = ivAlways
    end
    object dxBarLargeButton12: TdxBarLargeButton
      Action = aPodesuvanjePecatenje
      Category = 0
      ScreenTip = tipDizajnReport
    end
    object dxBarLargeButton13: TdxBarLargeButton
      Action = aBrisiPodesuvanjePecatenje
      Category = 0
      ScreenTip = tipBrisiKonf
    end
    object dxBarLargeButton14: TdxBarLargeButton
      Caption = #1048#1079#1073#1088#1080#1096#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      Category = 0
      Visible = ivAlways
      LargeImageIndex = 40
    end
    object dxBarLargeButton15: TdxBarLargeButton
      Action = aPageSetup
      Category = 0
      ScreenTip = tipSetiranjeStrana
    end
    object dxBarSubItem1: TdxBarSubItem
      Caption = #1055#1077#1095#1072#1090#1077#1114#1077
      Category = 0
      Visible = ivAlways
      ImageIndex = 30
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton11'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarLargeButton6'
        end>
    end
    object cxBarEditItem1: TcxBarEditItem
      Caption = 'Skin :'
      Category = 0
      Hint = 'Skin :'
      Visible = ivAlways
      Width = 160
      PropertiesClassName = 'TcxComboBoxProperties'
      Properties.OnChange = cxBarEditItem1PropertiesChange
    end
    object dxBarLBtnSnimiIzgled: TdxBarLargeButton
      Action = aSnimiIzgled
      Category = 0
    end
    object dxBarLBtnBrisiIzgled: TdxBarLargeButton
      Action = aBrisiIzgled
      Category = 0
    end
    object dxBarLargeButtonSoberi: TdxBarLargeButton
      Action = aSpustiSoberi
      Category = 0
    end
    object dxBarLargeButton10: TdxBarLargeButton
      Action = aEditStavka
      Category = 0
    end
    object dxBarLargeButton11: TdxBarLargeButton
      Action = aPecatiTL
      Category = 0
    end
  end
  object ActionList1: TActionList
    Images = dmRes.cxSmallImages
    Left = 152
    Top = 240
    object aNov: TAction
      Caption = #1053#1086#1074
      ImageIndex = 10
      ShortCut = 116
      OnExecute = aNovExecute
    end
    object aAzuriraj: TAction
      Caption = #1040#1078#1091#1088#1080#1088#1072#1112
      ImageIndex = 12
      ShortCut = 117
      OnExecute = aAzurirajExecute
    end
    object aBrisi: TAction
      Caption = #1041#1088#1080#1096#1080
      ImageIndex = 11
      ShortCut = 119
      OnExecute = aBrisiExecute
    end
    object aSnimiIzgled: TAction
      Caption = #1057#1085#1080#1084#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      ImageIndex = 9
      ShortCut = 24659
      OnExecute = aSnimiIzgledExecute
    end
    object aZacuvajExcel: TAction
      Caption = #1047#1072#1095#1091#1074#1072#1112' '#1074#1086' Excel'
      ImageIndex = 9
      ShortCut = 24645
      OnExecute = aZacuvajExcelExecute
    end
    object aPecatiTabela: TAction
      Caption = #1055#1077#1095#1072#1090#1080
      ImageIndex = 30
      OnExecute = aPecatiTabelaExecute
    end
    object aHelp: TAction
      Caption = #1055#1086#1084#1086#1096
      ImageIndex = 24
    end
    object aIzlez: TAction
      Caption = #1048#1079#1083#1077#1079
      ImageIndex = 25
      OnExecute = aIzlezExecute
    end
    object aZapisi: TAction
      Caption = #1047#1072#1087#1080#1096#1080
      ImageIndex = 7
      ShortCut = 120
      OnExecute = aZapisiExecute
    end
    object aOtkazi: TAction
      Caption = #1054#1090#1082#1072#1078#1080
      ImageIndex = 21
      ShortCut = 27
      OnExecute = aOtkaziExecute
    end
    object aRefresh: TAction
      Caption = 'aRefresh'
      ImageIndex = 18
      ShortCut = 118
      OnExecute = aRefreshExecute
    end
    object aSnimiPecatenje: TAction
      Caption = #1057#1085#1080#1084#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      ImageIndex = 39
      OnExecute = aSnimiPecatenjeExecute
    end
    object aPodesuvanjePecatenje: TAction
      Caption = #1044#1080#1079#1072#1112#1085' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090
      ImageIndex = 38
      OnExecute = aPodesuvanjePecatenjeExecute
    end
    object aBrisiPodesuvanjePecatenje: TAction
      Caption = #1041#1088#1080#1096#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      ImageIndex = 37
      OnExecute = aBrisiPodesuvanjePecatenjeExecute
    end
    object aPageSetup: TAction
      Caption = #1057#1077#1090#1080#1088#1072#1114#1077' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072
      ImageIndex = 38
      OnExecute = aPageSetupExecute
    end
    object aFormConfig: TAction
      Caption = 'aFormConfig'
      ShortCut = 24699
      OnExecute = aFormConfigExecute
    end
    object aBrisiIzgled: TAction
      Caption = #1041#1088#1080#1096#1080' '#1075#1086' '#1079#1072#1095#1091#1074#1072#1085#1080#1086#1090' '#1080#1079#1075#1083#1077#1076
      ImageIndex = 56
      ShortCut = 24644
      OnExecute = aBrisiIzgledExecute
    end
    object aSpustiSoberi: TAction
      Caption = #1057#1087#1091#1096#1090#1080'/'#1057#1086#1073#1077#1088#1080' '#1058#1072#1073#1077#1083#1072
      ImageIndex = 65
      OnExecute = aSpustiSoberiExecute
    end
    object aEditStavka: TAction
      Caption = #1054#1090#1074#1086#1088#1080' '#1089#1090#1072#1074#1082#1080
      ImageIndex = 89
      OnExecute = aEditStavkaExecute
    end
    object aResetNurkovci: TAction
      Caption = 'aResetNurkovci'
      OnExecute = aResetNurkovciExecute
    end
    object aPecatiTL: TAction
      Caption = #1055#1077#1095#1072#1090#1080' '#1090#1086#1074#1072#1088#1077#1085' '#1083#1080#1089#1090
      ImageIndex = 30
      ShortCut = 121
      OnExecute = aPecatiTLExecute
    end
    object aDizajnTL: TAction
      Caption = 'aDizajnTL'
      ShortCut = 24697
      OnExecute = aDizajnTLExecute
    end
  end
  object dxComponentPrinter1: TdxComponentPrinter
    CurrentLink = dxComponentPrinter1Link1
    Version = 0
    Left = 64
    Top = 248
    object dxComponentPrinter1Link1: TdxGridReportLink
      Active = True
      Component = cxGrid1
      PrinterPage.DMPaper = 9
      PrinterPage.Footer = 6350
      PrinterPage.GrayShading = True
      PrinterPage.Header = 6350
      PrinterPage.Margins.Bottom = 12700
      PrinterPage.Margins.Left = 12700
      PrinterPage.Margins.Right = 12700
      PrinterPage.Margins.Top = 12700
      PrinterPage.Orientation = poLandscape
      PrinterPage.PageFooter.RightTitle.Strings = (
        #1057#1090#1088'.[Page #] '#1086#1076' [Total Pages]')
      PrinterPage.PageHeader.RightTitle.Strings = (
        '')
      PrinterPage.PageSize.X = 210000
      PrinterPage.PageSize.Y = 297000
      PrinterPage._dxMeasurementUnits_ = 0
      PrinterPage._dxLastMU_ = 2
      ReportDocument.CreationDate = 41431.370866192130000000
      OptionsFormatting.UseNativeStyles = True
      OptionsOnEveryPage.FilterBar = False
      OptionsView.Caption = False
      OptionsView.FilterBar = False
      StyleRepository = dmRes.cxStyleRepositoryPrinting
      Styles.StyleSheet = dmRes.dxGridReportLinkStyleSheet
      BuiltInReportLink = True
    end
  end
  object dxBarScreenTipRepository1: TdxBarScreenTipRepository
    Left = 624
    Top = 64
    object tipSnimiIzgled: TdxBarScreenTip
      Header.Text = #1057#1085#1080#1084#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      Description.Text = #1047#1072#1095#1091#1074#1072#1112' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090' '#1085#1072' '#1092#1086#1088#1084#1072#1090#1072' '#1080' '#1085#1072' '#1090#1072#1073#1077#1083#1072#1090#1072
    end
    object tipZacuvajExcel: TdxBarScreenTip
      Header.Text = #1047#1072#1095#1091#1074#1072#1112' '#1074#1086' Excel'
      Description.Text = #1057#1085#1080#1084#1080' '#1075#1080' '#1087#1086#1076#1072#1090#1086#1094#1080#1090#1077' '#1086#1076' '#1090#1072#1073#1077#1083#1072#1090#1072' '#1074#1086' Excel '#1092#1086#1088#1084#1072#1090
    end
    object tipDizajnReport: TdxBarScreenTip
      Header.Text = #1044#1080#1079#1072#1112#1085' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090
      Description.Text = #1055#1086#1076#1077#1089#1091#1074#1072#1114#1072' '#1085#1072' '#1080#1079#1075#1083#1077#1076#1086#1090' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090' '#1085#1072' '#1090#1072#1073#1077#1083#1072#1090#1072
    end
    object tipSetiranjeStrana: TdxBarScreenTip
      Header.Text = #1057#1077#1090#1080#1088#1072#1114#1077' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072
      Description.Text = #1055#1086#1076#1077#1089#1091#1074#1072#1114#1077' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072' '#1085#1072' '#1082#1086#1112#1072' '#1116#1077' '#1089#1077' '#1087#1077#1095#1072#1090#1080' '#1090#1072#1073#1077#1083#1072#1090#1072
    end
    object tipSnimiKonf: TdxBarScreenTip
      Header.Text = #1057#1085#1080#1084#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      Description.Text = #1047#1072#1095#1091#1074#1072#1112' '#1075#1080' '#1087#1086#1076#1077#1089#1091#1074#1072#1114#1072#1090#1072' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090' '#1080' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072' '#1079#1072' '#1087#1077#1095#1072#1090#1077#1114#1077
    end
    object tipBrisiKonf: TdxBarScreenTip
      Header.Text = #1041#1088#1080#1096#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      Description.Text = #1048#1079#1073#1088#1080#1096#1080' '#1075#1080' '#1087#1086#1076#1077#1089#1091#1074#1072#1114#1072#1090#1072' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090' '#1080' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072' '#1079#1072' '#1087#1077#1095#1072#1090#1077#1114#1077
    end
  end
  object qryZemiBroj: TpFIBQuery
    Transaction = dmKon.qTransaction
    Database = dmKon.fibBaza
    SQL.Strings = (
      'select coalesce(max(TL.BROJ), 0) as MAKS'
      'from MTR_TL_GLAVA TL'
      'where TL.GODINA = :GOD'
      '      and TL.RE = :RE   ')
    Left = 584
    Top = 264
    qoAutoCommit = True
    qoStartTransaction = True
  end
end
