object frmUpotrebeniSerii: TfrmUpotrebeniSerii
  Left = 128
  Top = 212
  BorderIcons = [biSystemMenu, biMaximize]
  Caption = #1059#1087#1086#1090#1088#1077#1073#1077#1085#1080' '#1089#1077#1088#1080#1080' '#1074#1086' '#1077#1074#1080#1076#1077#1085#1094#1080#1112#1072#1090#1072' '#1079#1072' :'
  ClientHeight = 546
  ClientWidth = 769
  Color = clBtnFace
  Constraints.MinHeight = 500
  Constraints.MinWidth = 610
  Font.Charset = RUSSIAN_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  ShowHint = True
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object dxRibbon1: TdxRibbon
    Left = 0
    Top = 0
    Width = 769
    Height = 126
    BarManager = dxBarManager1
    ColorSchemeName = 'Blue'
    Contexts = <>
    TabOrder = 0
    TabStop = False
    ExplicitWidth = 774
    object dxRibbon1Tab1: TdxRibbonTab
      Active = True
      Caption = #1052#1077#1085#1080
      Groups = <
        item
          ToolbarName = 'dxBarManager1Bar2'
        end
        item
          ToolbarName = 'dxBarManager1Bar3'
        end>
      Index = 0
    end
    object dxRibbon1Tab2: TdxRibbonTab
      Caption = #1055#1086#1076#1077#1089#1091#1074#1072#1114#1072
      Groups = <
        item
          ToolbarName = 'dxBarManager1Bar4'
        end
        item
          ToolbarName = 'dxBarManager1BarIzgledGrid'
        end>
      Index = 1
    end
  end
  object StatusBar1: TdxRibbonStatusBar
    Left = 0
    Top = 523
    Width = 769
    Height = 23
    Panels = <
      item
        PanelStyleClassName = 'TdxStatusBarTextPanelStyle'
        Text = ' F7 - '#1054#1089#1074#1077#1078#1080', Esc - '#1054#1090#1082#1072#1078#1080' / '#1048#1079#1083#1077#1079
      end>
    Ribbon = dxRibbon1
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clDefault
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ExplicitTop = 659
    ExplicitWidth = 774
  end
  object Panel1: TPanel
    Left = 0
    Top = 126
    Width = 769
    Height = 397
    Align = alClient
    Color = 15790320
    ParentBackground = False
    TabOrder = 6
    ExplicitLeft = 208
    ExplicitTop = 208
    ExplicitWidth = 185
    ExplicitHeight = 41
    object cxGrid1: TcxGrid
      Left = 1
      Top = 1
      Width = 767
      Height = 395
      Align = alClient
      TabOrder = 0
      ExplicitTop = 36
      ExplicitWidth = 952
      ExplicitHeight = 408
      object cxGrid1DBTableView1: TcxGridDBTableView
        DataController.DataSource = dsUpotrebeni
        DataController.Options = [dcoAnsiSort, dcoAssignGroupingValues, dcoAssignMasterDetailKeys, dcoSaveExpanding, dcoImmediatePost]
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        Filtering.ColumnFilteredItemsList = True
        FilterRow.InfoText = #1050#1083#1080#1082#1085#1080' '#1090#1091#1082#1072' '#1079#1072' '#1076#1072' '#1076#1077#1092#1080#1085#1080#1088#1072#1096' '#1092#1080#1083#1090#1077#1088
        FilterRow.Visible = True
        FilterRow.ApplyChanges = fracImmediately
        Images = dmRes.cxImageGrid
        OptionsBehavior.IncSearch = True
        OptionsCustomize.ColumnsQuickCustomization = True
        OptionsData.Deleting = False
        OptionsData.Inserting = False
        OptionsView.NoDataToDisplayInfoText = '<'#1053#1077#1084#1072' '#1089#1077#1088#1080#1112#1072' '#1079#1072' '#1086#1076#1073#1088#1072#1085#1080#1086#1090' '#1072#1088#1090#1080#1082#1072#1083'>'
        OptionsView.Footer = True
        OptionsView.GroupByBox = False
        object cxGrid1DBTableView1ID: TcxGridDBColumn
          DataBinding.FieldName = 'ID'
        end
        object cxGrid1DBTableView1IN_S_ID: TcxGridDBColumn
          DataBinding.FieldName = 'IN_S_ID'
          Visible = False
        end
        object cxGrid1DBTableView1PRIEM_KOLICINA: TcxGridDBColumn
          DataBinding.FieldName = 'PRIEM_KOLICINA'
          Visible = False
          Width = 97
        end
        object cxGrid1DBTableView1KOLICINA: TcxGridDBColumn
          DataBinding.FieldName = 'KOLICINA'
          Width = 103
        end
        object cxGrid1DBTableView1PRIEM_LOT_NO: TcxGridDBColumn
          DataBinding.FieldName = 'PRIEM_LOT_NO'
        end
        object cxGrid1DBTableView1PRIEM_BR: TcxGridDBColumn
          DataBinding.FieldName = 'PRIEM_BR'
        end
        object cxGrid1DBTableView1PRIEM_GOD: TcxGridDBColumn
          DataBinding.FieldName = 'PRIEM_GOD'
        end
        object cxGrid1DBTableView1PRIEM_TP: TcxGridDBColumn
          DataBinding.FieldName = 'PRIEM_TP'
          Visible = False
        end
        object cxGrid1DBTableView1PRIEM_P: TcxGridDBColumn
          DataBinding.FieldName = 'PRIEM_P'
          Visible = False
        end
        object cxGrid1DBTableView1PRIEM_PARTNER: TcxGridDBColumn
          DataBinding.FieldName = 'PRIEM_PARTNER'
          Width = 250
        end
        object cxGrid1DBTableView1PRIEM_PARTNER_MESTO: TcxGridDBColumn
          DataBinding.FieldName = 'PRIEM_PARTNER_MESTO'
          Visible = False
          Width = 150
        end
        object cxGrid1DBTableView1OUT_S_ID: TcxGridDBColumn
          DataBinding.FieldName = 'OUT_S_ID'
          Visible = False
        end
        object cxGrid1DBTableView1TS_INS: TcxGridDBColumn
          DataBinding.FieldName = 'TS_INS'
          Visible = False
          Width = 100
        end
        object cxGrid1DBTableView1TS_UPD: TcxGridDBColumn
          DataBinding.FieldName = 'TS_UPD'
          Visible = False
          Width = 100
        end
        object cxGrid1DBTableView1USR_INS: TcxGridDBColumn
          DataBinding.FieldName = 'USR_INS'
          Visible = False
          Width = 100
        end
        object cxGrid1DBTableView1USR_UPD: TcxGridDBColumn
          DataBinding.FieldName = 'USR_UPD'
          Visible = False
          Width = 100
        end
      end
      object cxGrid1Level1: TcxGridLevel
        GridView = cxGrid1DBTableView1
      end
    end
  end
  object cxGridPopupMenu1: TcxGridPopupMenu
    Grid = cxGrid1
    PopupMenus = <>
    Left = 304
    Top = 328
  end
  object PopupMenu1: TPopupMenu
    Left = 480
    Top = 288
    object N1: TMenuItem
      Caption = #1057#1085#1080#1084#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      ShortCut = 49235
      OnClick = SaveToIniFileExecute
    end
    object Excel1: TMenuItem
      Caption = #1057#1085#1080#1084#1080' '#1075#1086' '#1074#1086' Excel'
      ShortCut = 49221
    end
  end
  object dxBarManager1: TdxBarManager
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Segoe UI'
    Font.Style = []
    Categories.Strings = (
      'Default')
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    ImageOptions.Images = dmRes.cxSmallImages
    ImageOptions.LargeImages = dmRes.cxLargeImages
    PopupMenuLinks = <>
    UseSystemFont = True
    Left = 560
    Top = 264
    DockControlHeights = (
      0
      0
      0
      0)
    object dxBarManager1Bar2: TdxBar
      Caption = #1040#1082#1094#1080#1080
      CaptionButtons = <>
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 789
      FloatTop = 120
      FloatClientWidth = 51
      FloatClientHeight = 120
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton10'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton5'
        end
        item
          Visible = True
          ItemName = 'dxBarSubItem1'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar3: TdxBar
      Caption = #1048#1079#1083#1077#1079
      CaptionButtons = <>
      DockedLeft = 245
      DockedTop = 0
      FloatLeft = 718
      FloatTop = 62
      FloatClientWidth = 51
      FloatClientHeight = 80
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton7'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton8'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar4: TdxBar
      Caption = #1055#1077#1095#1072#1090#1077#1114#1077
      CaptionButtons = <>
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 676
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton12'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton15'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton9'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarLargeButton13'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1BarIzgledGrid: TdxBar
      Caption = #1058#1072#1073#1077#1083#1072
      CaptionButtons = <>
      DockedLeft = 363
      DockedTop = 0
      FloatLeft = 808
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLBtnSnimiIzgled'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarLBtnBrisiIzgled'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarLargeButton2: TdxBarLargeButton
      Action = aAzuriraj
      Category = 0
    end
    object dxBarLargeButton3: TdxBarLargeButton
      Action = aBrisi
      Category = 0
    end
    object dxBarLargeButton4: TdxBarLargeButton
      Action = aSnimiIzgled
      Category = 0
      ScreenTip = tipSnimiIzgled
    end
    object dxBarLargeButton5: TdxBarLargeButton
      Action = aZacuvajExcel
      Category = 0
      ScreenTip = tipZacuvajExcel
    end
    object dxBarLargeButton6: TdxBarLargeButton
      Action = aPecatiTabela
      Caption = #1055#1077#1095#1072#1090#1080' '#1090#1072#1073#1077#1083#1072
      Category = 0
    end
    object dxBarLargeButton7: TdxBarLargeButton
      Action = aHelp
      Category = 0
    end
    object dxBarLargeButton8: TdxBarLargeButton
      Action = aIzlez
      Category = 0
    end
    object dxBarLargeButton1: TdxBarLargeButton
      Action = aNov
      Category = 0
    end
    object dxBarLargeButton9: TdxBarLargeButton
      Action = aSnimiPecatenje
      Category = 0
      ScreenTip = tipSnimiKonf
    end
    object dxBarLargeButton11: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object ribbonFilterReset: TdxBarLargeButton
      Caption = #1056#1077#1089#1077#1090#1080#1088#1072#1112' '#1092#1080#1083#1090#1077#1088
      Category = 0
      Hint = #1056#1077#1089#1077#1090#1080#1088#1072#1112' '#1092#1080#1083#1090#1077#1088
      Visible = ivAlways
    end
    object dxBarLargeButton12: TdxBarLargeButton
      Action = aPodesuvanjePecatenje
      Category = 0
      ScreenTip = tipDizajnReport
    end
    object dxBarLargeButton13: TdxBarLargeButton
      Action = aBrisiPodesuvanjePecatenje
      Category = 0
      ScreenTip = tipBrisiKonf
    end
    object dxBarLargeButton14: TdxBarLargeButton
      Caption = #1048#1079#1073#1088#1080#1096#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      Category = 0
      Visible = ivAlways
      LargeImageIndex = 40
    end
    object dxBarLargeButton15: TdxBarLargeButton
      Action = aPageSetup
      Category = 0
      ScreenTip = tipSetiranjeStrana
    end
    object dxBarLargeButton16: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object dxBarLargeButton17: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object cxBarEditItem1: TcxBarEditItem
      Caption = 'New Item'
      Category = 0
      Hint = 'New Item'
      Visible = ivAlways
      PropertiesClassName = 'TcxCheckBoxProperties'
    end
    object dxBarSubItem1: TdxBarSubItem
      Caption = #1055#1077#1095#1072#1090#1077#1114#1077
      Category = 0
      Visible = ivAlways
      ImageIndex = 30
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton6'
        end>
    end
    object dxBarLBtnSnimiIzgled: TdxBarLargeButton
      Action = aSnimiIzgled
      Category = 0
    end
    object dxBarLBtnBrisiIzgled: TdxBarLargeButton
      Action = aBrisiIzgled
      Category = 0
    end
    object dxBarLargeButton10: TdxBarLargeButton
      Action = aSpustiSoberi
      Category = 0
    end
  end
  object ActionList1: TActionList
    Images = dmRes.cxSmallImages
    Left = 88
    Top = 312
    object aNov: TAction
      Caption = #1053#1086#1074
      ImageIndex = 10
      ShortCut = 116
      OnExecute = aNovExecute
    end
    object aAzuriraj: TAction
      Caption = #1040#1078#1091#1088#1080#1088#1072#1112
      ImageIndex = 12
      ShortCut = 117
      OnExecute = aAzurirajExecute
    end
    object aBrisi: TAction
      Caption = #1041#1088#1080#1096#1080
      ImageIndex = 11
      ShortCut = 119
      OnExecute = aBrisiExecute
    end
    object aSnimiIzgled: TAction
      Caption = #1057#1085#1080#1084#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      ImageIndex = 9
      ShortCut = 24659
      OnExecute = aSnimiIzgledExecute
    end
    object aZacuvajExcel: TAction
      Caption = #1047#1072#1095#1091#1074#1072#1112' '#1074#1086' Excel'
      ImageIndex = 9
      ShortCut = 24645
      OnExecute = aZacuvajExcelExecute
    end
    object aPecatiTabela: TAction
      Caption = #1055#1077#1095#1072#1090#1080
      ImageIndex = 30
      OnExecute = aPecatiTabelaExecute
    end
    object aHelp: TAction
      Caption = #1055#1086#1084#1086#1096
      ImageIndex = 24
    end
    object aIzlez: TAction
      Caption = #1048#1079#1083#1077#1079
      ImageIndex = 25
      OnExecute = aIzlezExecute
    end
    object aZapisi: TAction
      Caption = #1047#1072#1087#1080#1096#1080
      ImageIndex = 7
      ShortCut = 120
      OnExecute = aZapisiExecute
    end
    object aOtkazi: TAction
      Caption = #1054#1090#1082#1072#1078#1080
      ImageIndex = 21
      ShortCut = 27
      OnExecute = aOtkaziExecute
    end
    object aRefresh: TAction
      Caption = 'aRefresh'
      ImageIndex = 18
      ShortCut = 118
      OnExecute = aRefreshExecute
    end
    object aSnimiPecatenje: TAction
      Caption = #1057#1085#1080#1084#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      ImageIndex = 39
      OnExecute = aSnimiPecatenjeExecute
    end
    object aPodesuvanjePecatenje: TAction
      Caption = #1044#1080#1079#1072#1112#1085' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090
      ImageIndex = 38
      OnExecute = aPodesuvanjePecatenjeExecute
    end
    object aBrisiPodesuvanjePecatenje: TAction
      Caption = #1041#1088#1080#1096#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      ImageIndex = 37
      OnExecute = aBrisiPodesuvanjePecatenjeExecute
    end
    object aPageSetup: TAction
      Caption = #1057#1077#1090#1080#1088#1072#1114#1077' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072
      ImageIndex = 38
      OnExecute = aPageSetupExecute
    end
    object aFormConfig: TAction
      Caption = 'aFormConfig'
      ShortCut = 24699
      OnExecute = aFormConfigExecute
    end
    object aBrisiIzgled: TAction
      Caption = #1041#1088#1080#1096#1080' '#1075#1086' '#1079#1072#1095#1091#1074#1072#1085#1080#1086#1090' '#1080#1079#1075#1083#1077#1076
      ImageIndex = 56
      OnExecute = aBrisiIzgledExecute
    end
    object aSpustiSoberi: TAction
      Caption = #1057#1087#1091#1096#1090#1080'/'#1057#1086#1073#1077#1088#1080' '#1058#1072#1073#1077#1083#1072
      ImageIndex = 65
      OnExecute = aSpustiSoberiExecute
    end
  end
  object dxComponentPrinter1: TdxComponentPrinter
    CurrentLink = dxComponentPrinter1Link1
    Version = 0
    Left = 184
    Top = 344
    object dxComponentPrinter1Link1: TdxGridReportLink
      Component = cxGrid1
      PrinterPage.DMPaper = 9
      PrinterPage.Footer = 6350
      PrinterPage.GrayShading = True
      PrinterPage.Header = 6350
      PrinterPage.Margins.Bottom = 12700
      PrinterPage.Margins.Left = 12700
      PrinterPage.Margins.Right = 12700
      PrinterPage.Margins.Top = 12700
      PrinterPage.Orientation = poLandscape
      PrinterPage.PageFooter.RightTitle.Strings = (
        #1057#1090#1088'.[Page #] '#1086#1076' [Total Pages]')
      PrinterPage.PageSize.X = 210000
      PrinterPage.PageSize.Y = 297000
      PrinterPage._dxMeasurementUnits_ = 0
      PrinterPage._dxLastMU_ = 2
      ReportDocument.CreationDate = 40336.425359629630000000
      OptionsFormatting.UseNativeStyles = True
      OptionsOnEveryPage.Caption = False
      OptionsOnEveryPage.FilterBar = False
      OptionsView.Caption = False
      OptionsView.FilterBar = False
      StyleRepository = dmRes.cxStyleRepositoryPrinting
      Styles.StyleSheet = dmRes.dxGridReportLinkStyleSheet
      BuiltInReportLink = True
    end
  end
  object dxBarScreenTipRepository1: TdxBarScreenTipRepository
    Left = 440
    Top = 192
    object tipSnimiIzgled: TdxBarScreenTip
      Header.Text = #1057#1085#1080#1084#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      Description.Text = #1047#1072#1095#1091#1074#1072#1112' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090' '#1085#1072' '#1092#1086#1088#1084#1072#1090#1072' '#1080' '#1085#1072' '#1090#1072#1073#1077#1083#1072#1090#1072
    end
    object tipZacuvajExcel: TdxBarScreenTip
      Header.Text = #1047#1072#1095#1091#1074#1072#1112' '#1074#1086' Excel'
      Description.Text = #1057#1085#1080#1084#1080' '#1075#1080' '#1087#1086#1076#1072#1090#1086#1094#1080#1090#1077' '#1086#1076' '#1090#1072#1073#1077#1083#1072#1090#1072' '#1074#1086' Excel '#1092#1086#1088#1084#1072#1090
    end
    object tipDizajnReport: TdxBarScreenTip
      Header.Text = #1044#1080#1079#1072#1112#1085' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090
      Description.Text = #1055#1086#1076#1077#1089#1091#1074#1072#1114#1072' '#1085#1072' '#1080#1079#1075#1083#1077#1076#1086#1090' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090' '#1085#1072' '#1090#1072#1073#1077#1083#1072#1090#1072
    end
    object tipSetiranjeStrana: TdxBarScreenTip
      Header.Text = #1057#1077#1090#1080#1088#1072#1114#1077' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072
      Description.Text = #1055#1086#1076#1077#1089#1091#1074#1072#1114#1077' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072' '#1085#1072' '#1082#1086#1112#1072' '#1116#1077' '#1089#1077' '#1087#1077#1095#1072#1090#1080' '#1090#1072#1073#1077#1083#1072#1090#1072
    end
    object tipSnimiKonf: TdxBarScreenTip
      Header.Text = #1057#1085#1080#1084#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      Description.Text = #1047#1072#1095#1091#1074#1072#1112' '#1075#1080' '#1087#1086#1076#1077#1089#1091#1074#1072#1114#1072#1090#1072' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090' '#1080' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072' '#1079#1072' '#1087#1077#1095#1072#1090#1077#1114#1077
    end
    object tipBrisiKonf: TdxBarScreenTip
      Header.Text = #1041#1088#1080#1096#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      Description.Text = #1048#1079#1073#1088#1080#1096#1080' '#1075#1080' '#1087#1086#1076#1077#1089#1091#1074#1072#1114#1072#1090#1072' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090' '#1080' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072' '#1079#1072' '#1087#1077#1095#1072#1090#1077#1114#1077
    end
  end
  object dsUpotrebeni: TDataSource
    DataSet = tblUpotrebeni
    Left = 120
    Top = 184
  end
  object tblUpotrebeni: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE MTR_INOUT_KOLICINA'
      'SET '
      '    ID = :ID,'
      '    IN_S_ID = :IN_S_ID,'
      '    OUT_S_ID = :OUT_S_ID,'
      '    KOLICINA = :KOLICINA,'
      '    TS_INS = :TS_INS,'
      '    TS_UPD = :TS_UPD,'
      '    USR_INS = :USR_INS,'
      '    USR_UPD = :USR_UPD'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    MTR_INOUT_KOLICINA'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO MTR_INOUT_KOLICINA('
      '    ID,'
      '    IN_S_ID,'
      '    OUT_S_ID,'
      '    KOLICINA,'
      '    TS_INS,'
      '    TS_UPD,'
      '    USR_INS,'
      '    USR_UPD'
      ')'
      'VALUES('
      '    :ID,'
      '    :IN_S_ID,'
      '    :OUT_S_ID,'
      '    :KOLICINA,'
      '    :TS_INS,'
      '    :TS_UPD,'
      '    :USR_INS,'
      '    :USR_UPD'
      ')')
    RefreshSQL.Strings = (
      'select IK.ID,'
      '       IK.IN_S_ID,'
      '       S.KOLICINA as PRIEM_KOLICINA,'
      '       S.LOT_NO as PRIEM_LOT_NO,'
      '       I.BROJ as PRIEM_BR,'
      '       I.GODINA as PRIEM_GOD,'
      '       I.TP as PRIEM_TP,'
      '       I.P as PRIEM_P,'
      '       P.NAZIV as PRIEM_PARTNER,'
      '       M.NAZIV as PRIEM_PARTNER_MESTO,'
      '       IK.OUT_S_ID,'
      '       IK.KOLICINA,'
      '       IK.TS_INS,'
      '       IK.TS_UPD,'
      '       IK.USR_INS,'
      '       IK.USR_UPD'
      'from MTR_INOUT_KOLICINA IK'
      'join MTR_IN_S S on S.ID = IK.IN_S_ID'
      'join MTR_IN_S S2 on S2.ID = S.REF_ID'
      'join MTR_IN I on I.ID = S2.MTR_IN_ID'
      'left join MAT_PARTNER P on P.TIP_PARTNER = I.TP and P.ID = I.P'
      'left join MAT_MESTO M on M.ID = P.MESTO'
      'where(  IK.OUT_S_ID = :ID'
      '     ) and (     IK.ID = :OLD_ID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      'select IK.ID,'
      '       IK.IN_S_ID,'
      '       S.KOLICINA as PRIEM_KOLICINA,'
      '       S.LOT_NO as PRIEM_LOT_NO,'
      '       I.BROJ as PRIEM_BR,'
      '       I.GODINA as PRIEM_GOD,'
      '       I.TP as PRIEM_TP,'
      '       I.P as PRIEM_P,'
      '       P.NAZIV as PRIEM_PARTNER,'
      '       M.NAZIV as PRIEM_PARTNER_MESTO,'
      '       IK.OUT_S_ID,'
      '       IK.KOLICINA,'
      '       IK.TS_INS,'
      '       IK.TS_UPD,'
      '       IK.USR_INS,'
      '       IK.USR_UPD'
      'from MTR_INOUT_KOLICINA IK'
      'join MTR_IN_S S on S.ID = IK.IN_S_ID'
      'join MTR_IN_S S2 on S2.ID = S.REF_ID'
      'join MTR_IN I on I.ID = S2.MTR_IN_ID'
      'left join MAT_PARTNER P on P.TIP_PARTNER = I.TP and P.ID = I.P'
      'left join MAT_MESTO M on M.ID = P.MESTO'
      'where IK.OUT_S_ID = :ID')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    Left = 200
    Top = 184
    poSQLINT64ToBCD = True
    object tblUpotrebeniID: TFIBBCDField
      DisplayLabel = #1064#1080#1092'.'
      FieldName = 'ID'
      Size = 0
      RoundByScale = True
    end
    object tblUpotrebeniIN_S_ID: TFIBBCDField
      FieldName = 'IN_S_ID'
      Size = 0
      RoundByScale = True
    end
    object tblUpotrebeniPRIEM_KOLICINA: TFIBFloatField
      DisplayLabel = #1055#1088#1080#1077#1084' '#1082#1086#1083#1080#1095#1080#1085#1072
      FieldName = 'PRIEM_KOLICINA'
    end
    object tblUpotrebeniPRIEM_LOT_NO: TFIBStringField
      DisplayLabel = #1055#1088#1080#1077#1084' LOT_NO'
      FieldName = 'PRIEM_LOT_NO'
      Size = 30
      EmptyStrToNull = True
    end
    object tblUpotrebeniPRIEM_BR: TFIBIntegerField
      DisplayLabel = #1055#1088#1080#1077#1084' '#1073#1088'.'
      FieldName = 'PRIEM_BR'
    end
    object tblUpotrebeniPRIEM_GOD: TFIBIntegerField
      DisplayLabel = #1055#1088#1080#1077#1084' '#1075#1086#1076'.'
      FieldName = 'PRIEM_GOD'
    end
    object tblUpotrebeniPRIEM_TP: TFIBIntegerField
      DisplayLabel = #1055#1088#1080#1077#1084' '#1058#1055
      FieldName = 'PRIEM_TP'
    end
    object tblUpotrebeniPRIEM_P: TFIBIntegerField
      DisplayLabel = #1055#1088#1080#1077#1084' '#1055
      FieldName = 'PRIEM_P'
    end
    object tblUpotrebeniPRIEM_PARTNER: TFIBStringField
      DisplayLabel = #1055#1088#1080#1077#1084' '#1087#1072#1088#1090#1085#1077#1088
      FieldName = 'PRIEM_PARTNER'
      Size = 100
      EmptyStrToNull = True
    end
    object tblUpotrebeniPRIEM_PARTNER_MESTO: TFIBStringField
      DisplayLabel = #1055#1088#1080#1077#1084' '#1087#1072#1088#1090#1085#1077#1088' '#1084#1077#1089#1090#1086
      FieldName = 'PRIEM_PARTNER_MESTO'
      Size = 50
      EmptyStrToNull = True
    end
    object tblUpotrebeniOUT_S_ID: TFIBBCDField
      FieldName = 'OUT_S_ID'
      Size = 0
      RoundByScale = True
    end
    object tblUpotrebeniKOLICINA: TFIBFloatField
      DisplayLabel = #1059#1087#1086#1090#1088#1077#1073#1077#1085#1072' '#1082#1086#1083'.'
      FieldName = 'KOLICINA'
    end
    object tblUpotrebeniTS_INS: TFIBDateTimeField
      FieldName = 'TS_INS'
    end
    object tblUpotrebeniTS_UPD: TFIBDateTimeField
      FieldName = 'TS_UPD'
    end
    object tblUpotrebeniUSR_INS: TFIBStringField
      FieldName = 'USR_INS'
      Size = 50
      EmptyStrToNull = True
    end
    object tblUpotrebeniUSR_UPD: TFIBStringField
      FieldName = 'USR_UPD'
      Size = 50
      EmptyStrToNull = True
    end
  end
end
