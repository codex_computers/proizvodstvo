inherited frmPartnerEtiketa: TfrmPartnerEtiketa
  Caption = #1045#1090#1080#1082#1077#1090#1072' '#1087#1086' '#1055#1072#1088#1090#1085#1077#1088
  ClientWidth = 711
  ExplicitWidth = 727
  ExplicitHeight = 592
  PixelsPerInch = 96
  TextHeight = 13
  inherited lPanel: TPanel
    Width = 711
    ExplicitWidth = 711
    inherited cxGrid1: TcxGrid
      Width = 707
      ExplicitWidth = 707
      inherited cxGrid1DBTableView1: TcxGridDBTableView
        OnFocusedRecordChanged = cxGrid1DBTableView1FocusedRecordChanged
        DataController.DataSource = dm.dsPartnerEtiketa
        OptionsView.NoDataToDisplayInfoText = '<'#1053#1077#1084#1072' '#1087#1086#1076#1072#1090#1086#1094#1080'>'
        object cxGrid1DBTableView1ID: TcxGridDBColumn
          Caption = #1064#1080#1092#1088#1072
          DataBinding.FieldName = 'ID'
          Visible = False
        end
        object cxGrid1DBTableView1TIP_PARNER: TcxGridDBColumn
          Caption = #1058#1080#1087' '#1085#1072' '#1087#1072#1088#1090#1085#1077#1088
          DataBinding.FieldName = 'TIP_PARNER'
        end
        object cxGrid1DBTableView1PARTNER: TcxGridDBColumn
          Caption = #1064#1080#1092#1088#1072' '#1085#1072' '#1055#1072#1088#1090#1085#1077#1088
          DataBinding.FieldName = 'PARTNER'
          Width = 42
        end
        object cxGrid1DBTableView1NAZIV_PARTNER: TcxGridDBColumn
          Caption = #1055#1072#1088#1090#1085#1077#1088
          DataBinding.FieldName = 'NAZIV_PARTNER'
          Width = 268
        end
        object cxGrid1DBTableView1ID_SYS_REPORT: TcxGridDBColumn
          Caption = #1064#1080#1092#1088#1072' '#1085#1072' '#1080#1079#1074#1077#1096#1090#1072#1112
          DataBinding.FieldName = 'ID_SYS_REPORT'
        end
        object cxGrid1DBTableView1NASLOV: TcxGridDBColumn
          Caption = #1053#1072#1089#1083#1086#1074' '#1085#1072' '#1080#1079#1074#1077#1096#1090#1072#1112
          DataBinding.FieldName = 'NASLOV'
          Width = 239
        end
      end
    end
  end
  inherited dPanel: TPanel
    Width = 711
    ParentFont = False
    ExplicitWidth = 711
    inherited Label1: TLabel
      Left = 543
      Top = 29
      Visible = False
      ExplicitLeft = 543
      ExplicitTop = 29
    end
    inherited Sifra: TcxDBTextEdit
      Tag = 0
      Left = 595
      Top = 26
      TabOrder = 9
      Visible = False
      ExplicitLeft = 595
      ExplicitTop = 26
    end
    inherited OtkaziButton: TcxButton
      Left = 620
      TabOrder = 6
      ExplicitLeft = 620
    end
    inherited ZapisiButton: TcxButton
      Left = 539
      TabOrder = 5
      ExplicitLeft = 539
    end
    object cxLabel36: TcxLabel
      Left = 15
      Top = 37
      AutoSize = False
      Caption = #1047#1072' '#1055#1072#1088#1090#1085#1077#1088' '
      ParentFont = False
      Style.TextColor = clRed
      Properties.Alignment.Horz = taRightJustify
      Properties.WordWrap = True
      Transparent = True
      Height = 18
      Width = 63
      AnchorX = 78
    end
    object txtTipPartner: TcxDBTextEdit
      Tag = 1
      Left = 84
      Top = 36
      Hint = #1058#1080#1087' '#1085#1072' '#1055#1072#1088#1090#1085#1077#1088
      DataBinding.DataField = 'TIP_PARNER'
      DataBinding.DataSource = dm.dsPartnerEtiketa
      ParentFont = False
      TabOrder = 0
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 63
    end
    object txtPartner: TcxDBTextEdit
      Tag = 1
      Left = 146
      Top = 36
      Hint = #1064#1080#1092#1088#1072' '#1085#1072' '#1055#1072#1088#1090#1085#1077#1088
      DataBinding.DataField = 'PARTNER'
      DataBinding.DataSource = dm.dsPartnerEtiketa
      ParentFont = False
      TabOrder = 1
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 58
    end
    object cbNazivPartner: TcxLookupComboBox
      Left = 203
      Top = 36
      Hint = #1053#1072#1079#1080#1074' '#1085#1072' '#1055#1072#1088#1090#1085#1077#1088
      Properties.ClearKey = 46
      Properties.DropDownAutoSize = True
      Properties.DropDownSizeable = True
      Properties.KeyFieldNames = 'tip_partner;id'
      Properties.ListColumns = <
        item
          Width = 40
          FieldName = 'TIP_PARTNER'
        end
        item
          Width = 60
          FieldName = 'id'
        end
        item
          Width = 170
          FieldName = 'naziv'
        end>
      Properties.ListFieldIndex = 2
      Properties.ListOptions.SyncMode = True
      Properties.ListSource = dmMat.dsPartner
      TabOrder = 2
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 311
    end
    object cxLabel38: TcxLabel
      Left = 18
      Top = 63
      AutoSize = False
      Caption = #1048#1079#1074#1077#1096#1090#1072#1112
      ParentFont = False
      Style.TextColor = clRed
      Properties.Alignment.Horz = taRightJustify
      Transparent = True
      Height = 17
      Width = 60
      AnchorX = 78
    end
    object txtizvestaj: TcxDBTextEdit
      Tag = 1
      Left = 84
      Top = 62
      DataBinding.DataField = 'ID_SYS_REPORT'
      DataBinding.DataSource = dm.dsPartnerEtiketa
      TabOrder = 3
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 63
    end
    object cbIzvestaj: TcxDBLookupComboBox
      Left = 146
      Top = 62
      DataBinding.DataField = 'ID_SYS_REPORT'
      DataBinding.DataSource = dm.dsPartnerEtiketa
      Properties.DropDownAutoSize = True
      Properties.DropDownSizeable = True
      Properties.KeyFieldNames = 'ID'
      Properties.ListColumns = <
        item
          FieldName = 'ID'
        end
        item
          FieldName = 'naslov'
        end>
      Properties.ListFieldIndex = 1
      Properties.ListSource = dm.dsSysReport
      TabOrder = 4
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 368
    end
  end
  inherited dxRibbon1: TdxRibbon
    Width = 711
    ExplicitWidth = 711
    inherited dxRibbon1Tab1: TdxRibbonTab
      Index = 0
    end
    inherited dxRibbon1Tab2: TdxRibbonTab
      Index = 1
    end
  end
  inherited StatusBar1: TdxRibbonStatusBar
    Width = 711
    ExplicitTop = 32000
    ExplicitWidth = 711
  end
  inherited cxGridPopupMenu1: TcxGridPopupMenu
    Left = 527
    Top = 16
  end
  inherited PopupMenu1: TPopupMenu
    Left = 667
    Top = 16
  end
  inherited dxBarManager1: TdxBarManager
    Left = 562
    Top = 16
    DockControlHeights = (
      0
      0
      0
      0)
    inherited dxBarManager1Bar1: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 69
      FloatClientHeight = 167
    end
    inherited dxBarManager1Bar2: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 149
      FloatClientHeight = 130
    end
    inherited dxBarManager1Bar3: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 59
      FloatClientHeight = 108
    end
    inherited dxBarManager1Bar4: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 141
      FloatClientHeight = 221
    end
    inherited dxBarManager1BarTabela: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 174
      FloatClientHeight = 113
    end
  end
  inherited ActionList1: TActionList
    Left = 492
    Top = 16
  end
  inherited dxComponentPrinter1: TdxComponentPrinter
    Left = 632
    Top = 16
    inherited dxComponentPrinter1Link1: TdxGridReportLink
      ReportDocument.CreationDate = 41743.331505057870000000
      Styles.BandHeader = nil
      Styles.Caption = nil
      Styles.CardCaptionRow = nil
      Styles.CardRowCaption = nil
      Styles.Content = nil
      Styles.ContentEven = nil
      Styles.ContentOdd = nil
      Styles.FilterBar = nil
      Styles.Footer = nil
      Styles.Group = nil
      Styles.Header = nil
      Styles.Preview = nil
      Styles.Selection = nil
      BuiltInReportLink = True
    end
  end
  inherited dxBarScreenTipRepository1: TdxBarScreenTipRepository
    Left = 597
    Top = 16
  end
end
