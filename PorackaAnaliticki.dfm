object frmPorackaAnaliticki: TfrmPorackaAnaliticki
  Left = 128
  Top = 212
  BorderIcons = [biSystemMenu, biMaximize]
  ClientHeight = 689
  ClientWidth = 781
  Color = clBtnFace
  Constraints.MinHeight = 500
  Constraints.MinWidth = 610
  Font.Charset = RUSSIAN_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  ShowHint = True
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object dxRibbon1: TdxRibbon
    Left = 0
    Top = 0
    Width = 781
    Height = 126
    BarManager = dxBarManager1
    ColorSchemeName = 'Blue'
    Contexts = <>
    TabOrder = 0
    TabStop = False
    object dxRibbon1Tab1: TdxRibbonTab
      Active = True
      Caption = #1052#1077#1085#1080
      Groups = <
        item
          ToolbarName = 'dxBarManager1Bar1'
        end
        item
          ToolbarName = 'dxBarManager1Bar2'
        end
        item
          ToolbarName = 'dxBarManager1Bar3'
        end>
      Index = 0
    end
    object dxRibbon1Tab2: TdxRibbonTab
      Caption = #1055#1086#1076#1077#1089#1091#1074#1072#1114#1072
      Groups = <
        item
          ToolbarName = 'dxBarManager1Bar4'
        end
        item
          ToolbarName = 'dxBarManager1BarIzgledGrid'
        end>
      Index = 1
    end
  end
  object StatusBar1: TdxRibbonStatusBar
    Left = 0
    Top = 666
    Width = 781
    Height = 23
    Panels = <
      item
        PanelStyleClassName = 'TdxStatusBarTextPanelStyle'
        Text = 
          ' F5 - '#1053#1086#1074', F6 - '#1040#1078#1091#1088#1080#1088#1072#1112', F7 - '#1054#1089#1074#1077#1078#1080', F8 - '#1041#1088#1080#1096#1080', F9 - '#1047#1072#1087#1080#1096#1080', ' +
          'Esc - '#1054#1090#1082#1072#1078#1080' / '#1048#1079#1083#1077#1079
      end>
    Ribbon = dxRibbon1
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clDefault
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
  end
  object Panel1: TPanel
    Left = 0
    Top = 126
    Width = 781
    Height = 281
    Align = alClient
    TabOrder = 3
    object cxGroupBox1: TcxGroupBox
      Left = 1
      Top = 1
      Align = alTop
      Caption = ' '#1047#1072' '#1055#1088#1086#1080#1079#1074#1086#1076'  '
      TabOrder = 0
      Height = 53
      Width = 779
      object lblProizvod: TcxLabel
        Left = 56
        Top = 16
        AutoSize = False
        Caption = 'lblProizvod'
        Transparent = True
        Height = 24
        Width = 673
      end
    end
    object cxGroupBox2: TcxGroupBox
      Left = 16
      Top = 47
      Caption = ' '#1055#1088#1086#1080#1079#1074#1086#1076#1086#1090' '#1074#1086' '#1055#1054#1056#1040#1063#1050#1040' '
      TabOrder = 1
      Height = 224
      Width = 362
      object cxDBVerticalGrid1: TcxDBVerticalGrid
        Left = 2
        Top = 18
        Width = 358
        Height = 204
        Align = alClient
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        OptionsView.RowHeaderWidth = 118
        OptionsBehavior.AllowChangeRecord = False
        OptionsBehavior.IncSearch = True
        OptionsData.Editing = False
        OptionsData.Appending = False
        OptionsData.Deleting = False
        OptionsData.Inserting = False
        OptionsData.MultiThreadedFiltering = bTrue
        Navigator.Buttons.CustomButtons = <>
        ParentFont = False
        TabOrder = 0
        DataController.DataSource = dm.dsDokument
        Version = 1
        object cxDBVerticalGrid1DBEditorRow1: TcxDBEditorRow
          Properties.Caption = #1056#1077#1092'.'#1073#1088':'
          Properties.DataBinding.FieldName = 'REF_NO'
          ID = 0
          ParentID = -1
          Index = 0
          Version = 1
        end
        object cxDBVerticalGrid1DBEditorRow2: TcxDBEditorRow
          Properties.Caption = #1058#1072#1087#1072#1094#1080#1088' '#1089#1077#1076#1080#1096#1090#1077':'
          Properties.DataBinding.FieldName = 'TAPACIR_SEDISTE'
          ID = 1
          ParentID = -1
          Index = 1
          Version = 1
        end
        object cxDBVerticalGrid1DBEditorRow3: TcxDBEditorRow
          Properties.Caption = #1058#1072#1087#1072#1094#1080#1088' '#1085#1072#1079#1072#1076':'
          Properties.DataBinding.FieldName = 'TAPACIR_NAZAD'
          ID = 2
          ParentID = -1
          Index = 2
          Version = 1
        end
        object cxDBVerticalGrid1DBEditorRow4: TcxDBEditorRow
          Properties.Caption = #1041#1086#1112#1072' '#1085#1072' '#1085#1086#1075#1072#1088#1082#1080':'
          Properties.DataBinding.FieldName = 'BOJA_NOGARKI'
          ID = 3
          ParentID = -1
          Index = 3
          Version = 1
        end
        object cxDBVerticalGrid1DBEditorRow5: TcxDBEditorRow
          Properties.Caption = 'GLIDERS:'
          Properties.DataBinding.FieldName = 'GLIDERS'
          ID = 4
          ParentID = -1
          Index = 4
          Version = 1
        end
        object cxDBVerticalGrid1DBEditorRow6: TcxDBEditorRow
          Properties.Caption = #1057#1091#1085#1107#1077#1088':'
          Properties.DataBinding.FieldName = 'SUNGER'
          ID = 5
          ParentID = -1
          Index = 5
          Version = 1
        end
        object cxDBVerticalGrid1DBEditorRow7: TcxDBEditorRow
          Properties.Caption = 'LABELS:'
          Properties.DataBinding.FieldName = 'LABELS'
          ID = 6
          ParentID = -1
          Index = 6
          Version = 1
        end
      end
    end
    object cxGroupBox3: TcxGroupBox
      Left = 402
      Top = 55
      Caption = ' '#1055#1088#1086#1080#1079#1074#1086#1076#1086#1090' '#1074#1086' '#1056#1040#1041#1054#1058#1045#1053' '#1053#1040#1051#1054#1043' '
      TabOrder = 2
      Height = 106
      Width = 354
      object cxDBVerticalGrid2: TcxDBVerticalGrid
        Left = 2
        Top = 18
        Width = 350
        Height = 86
        Align = alClient
        OptionsData.Editing = False
        OptionsData.Appending = False
        OptionsData.Deleting = False
        OptionsData.Inserting = False
        OptionsData.MultiThreadedFiltering = bTrue
        Navigator.Buttons.CustomButtons = <>
        TabOrder = 0
        DataController.DataSource = dm.dsDokument
        Version = 1
        object cxDBVerticalGrid2DBEditorRow1: TcxDBEditorRow
          Properties.Caption = #1041#1088'.'#1056#1053':'
          Properties.DataBinding.FieldName = 'BROJ_RN'
          ID = 0
          ParentID = -1
          Index = 0
          Version = 1
        end
        object cxDBVerticalGrid2DBEditorRow2: TcxDBEditorRow
          Properties.Caption = #1050#1086#1083#1080#1095#1080#1085#1072':'
          Properties.DataBinding.FieldName = 'KOL_RN'
          ID = 1
          ParentID = -1
          Index = 1
          Version = 1
        end
        object cxDBVerticalGrid2DBEditorRow3: TcxDBEditorRow
          Properties.Caption = #1051#1054#1058':'
          Properties.DataBinding.FieldName = 'LOT'
          ID = 2
          ParentID = -1
          Index = 2
          Version = 1
        end
      end
    end
    object cxGroupBox4: TcxGroupBox
      Left = 400
      Top = 167
      Caption = #1055#1088#1086#1080#1079#1074#1086#1076#1086#1090' '#1074#1086' '#1055#1051#1040#1053' '#1079#1072' '#1050#1072#1084#1080#1086#1085' '
      TabOrder = 3
      Height = 106
      Width = 354
      object cxDBVerticalGrid3: TcxDBVerticalGrid
        Left = 2
        Top = 18
        Width = 350
        Height = 86
        Align = alClient
        OptionsData.Editing = False
        OptionsData.Appending = False
        OptionsData.Deleting = False
        OptionsData.Inserting = False
        OptionsData.MultiThreadedFiltering = bTrue
        Navigator.Buttons.CustomButtons = <>
        TabOrder = 0
        DataController.DataSource = dm.dsDokument
        Version = 1
        object cxDBVerticalGrid3DBEditorRow2: TcxDBEditorRow
          Properties.Caption = #1064#1080#1092#1088#1072' '#1082#1072#1084#1080#1086#1085':'
          Properties.DataBinding.FieldName = 'ID_TRAILER'
          ID = 0
          ParentID = -1
          Index = 0
          Version = 1
        end
        object cxDBVerticalGrid3DBEditorRow1: TcxDBEditorRow
          Options.TabStop = False
          Properties.Caption = #1053#1072#1079#1080#1074':'
          Properties.DataBinding.FieldName = 'NAZIV_TRAILER'
          ID = 1
          ParentID = -1
          Index = 1
          Version = 1
        end
        object cxDBVerticalGrid3DBEditorRow3: TcxDBEditorRow
          Properties.Caption = #1050#1086#1083#1080#1095#1080#1085#1072':'
          Properties.DataBinding.FieldName = 'KOL_TRAILER'
          ID = 2
          ParentID = -1
          Index = 2
          Version = 1
        end
        object cxDBVerticalGrid3DBEditorRow4: TcxDBEditorRow
          Properties.Caption = #1044#1072#1090#1091#1084':'
          Properties.DataBinding.FieldName = 'DATUM_TRANSPORT'
          ID = 3
          ParentID = -1
          Index = 3
          Version = 1
        end
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 407
    Width = 781
    Height = 259
    Align = alBottom
    TabOrder = 2
    object cxGroupBox5: TcxGroupBox
      Left = 16
      Top = 15
      Caption = ' '#1055#1088#1086#1080#1079#1074#1086#1076#1086#1090' '#1074#1086' '#1052#1072#1096#1080#1085#1089#1082#1086' '
      TabOrder = 0
      OnClick = cxGroupBox5Click
      Height = 106
      Width = 354
      object cxDBVerticalGrid4: TcxDBVerticalGrid
        Left = 2
        Top = 18
        Width = 350
        Height = 86
        Align = alClient
        OptionsData.Editing = False
        OptionsData.Appending = False
        OptionsData.Deleting = False
        OptionsData.Inserting = False
        OptionsData.MultiThreadedFiltering = bTrue
        Navigator.Buttons.CustomButtons = <>
        TabOrder = 0
        DataController.DataSource = dm.dsProizvodstvo
        Version = 1
        object cxDBEditorRow1: TcxDBEditorRow
          Properties.Caption = #1055#1088#1086#1080#1079#1074#1077#1076#1077#1085#1072': '
          Properties.DataBinding.FieldName = 'MAS_KOLICINA_PROIZVEDENA'
          ID = 0
          ParentID = -1
          Index = 0
          Version = 1
        end
        object cxDBEditorRow2: TcxDBEditorRow
          Properties.Caption = #1055#1086#1084#1080#1085#1072#1090#1072':'
          Properties.DataBinding.FieldName = 'MAS_KOLICINA_POMINATA'
          ID = 1
          ParentID = -1
          Index = 1
          Version = 1
        end
        object cxDBEditorRow3: TcxDBEditorRow
          Properties.Caption = #1051#1072#1075#1077#1088':'
          Properties.DataBinding.FieldName = 'MAS_LAGER'
          ID = 2
          ParentID = -1
          Index = 2
          Version = 1
        end
      end
    end
    object cxGroupBox6: TcxGroupBox
      Left = 398
      Top = 15
      Caption = ' '#1055#1088#1086#1080#1079#1074#1086#1076#1086#1090' '#1074#1086' '#1060#1040#1056#1041#1040#1056#1040
      TabOrder = 1
      Height = 106
      Width = 354
      object cxDBVerticalGrid5: TcxDBVerticalGrid
        Left = 2
        Top = 18
        Width = 350
        Height = 86
        Align = alClient
        OptionsData.Editing = False
        OptionsData.Appending = False
        OptionsData.Deleting = False
        OptionsData.Inserting = False
        OptionsData.MultiThreadedFiltering = bTrue
        Navigator.Buttons.CustomButtons = <>
        TabOrder = 0
        DataController.DataSource = dm.dsProizvodstvo
        Version = 1
        object cxDBEditorRow4: TcxDBEditorRow
          Properties.Caption = #1055#1088#1086#1080#1079#1074#1077#1076#1077#1085#1072':'
          Properties.DataBinding.FieldName = 'FAR_KOLICINA_PROIZVEDENA'
          ID = 0
          ParentID = -1
          Index = 0
          Version = 1
        end
        object cxDBEditorRow5: TcxDBEditorRow
          Properties.Caption = #1055#1086#1084#1080#1085#1072#1090#1072':'
          Properties.DataBinding.FieldName = 'FAR_KOLICINA_POMINATA'
          ID = 1
          ParentID = -1
          Index = 1
          Version = 1
        end
        object cxDBEditorRow6: TcxDBEditorRow
          Properties.Caption = #1051#1072#1075#1077#1088':'
          Properties.DataBinding.FieldName = 'FAR_LAGER'
          ID = 2
          ParentID = -1
          Index = 2
          Version = 1
        end
      end
    end
    object cxGroupBox7: TcxGroupBox
      Left = 16
      Top = 127
      Caption = ' '#1055#1088#1086#1080#1079#1074#1086#1076#1086#1090' '#1074#1086' '#1058#1040#1055#1040#1062#1045#1056#1057#1050#1054' '
      TabOrder = 2
      Height = 106
      Width = 354
      object cxDBVerticalGrid6: TcxDBVerticalGrid
        Left = 2
        Top = 18
        Width = 350
        Height = 86
        Align = alClient
        OptionsData.Editing = False
        OptionsData.Appending = False
        OptionsData.Deleting = False
        OptionsData.Inserting = False
        OptionsData.MultiThreadedFiltering = bTrue
        Navigator.Buttons.CustomButtons = <>
        TabOrder = 0
        DataController.DataSource = dm.dsProizvodstvo
        Version = 1
        object cxDBEditorRow7: TcxDBEditorRow
          Properties.Caption = #1055#1088#1086#1080#1079#1074#1077#1076#1077#1085#1072':'
          Properties.DataBinding.FieldName = 'TAP_KOLICINA_PROIZVEDENA'
          ID = 0
          ParentID = -1
          Index = 0
          Version = 1
        end
        object cxDBEditorRow8: TcxDBEditorRow
          Properties.Caption = #1055#1086#1084#1080#1085#1072#1090#1072':'
          Properties.DataBinding.FieldName = 'TAP_KOLICINA_POMINATA'
          ID = 1
          ParentID = -1
          Index = 1
          Version = 1
        end
        object cxDBEditorRow9: TcxDBEditorRow
          Properties.Caption = #1051#1072#1075#1077#1088':'
          Properties.DataBinding.FieldName = 'TAP_LAGER'
          ID = 2
          ParentID = -1
          Index = 2
          Version = 1
        end
      end
    end
    object cxGroupBox8: TcxGroupBox
      Left = 398
      Top = 127
      Caption = ' '#1055#1088#1086#1080#1079#1074#1086#1076#1086#1090' '#1074#1086' '#1052#1043#1055
      TabOrder = 3
      Height = 106
      Width = 354
      object cxDBVerticalGrid7: TcxDBVerticalGrid
        Left = 2
        Top = 18
        Width = 350
        Height = 86
        Align = alClient
        OptionsData.Editing = False
        OptionsData.Appending = False
        OptionsData.Deleting = False
        OptionsData.Inserting = False
        OptionsData.MultiThreadedFiltering = bTrue
        Navigator.Buttons.CustomButtons = <>
        TabOrder = 0
        DataController.DataSource = dm.dsProizvodstvo
        Version = 1
        object cxDBEditorRow10: TcxDBEditorRow
          Properties.Caption = #1055#1088#1086#1080#1079#1074#1077#1076#1077#1085#1072':'
          Properties.DataBinding.FieldName = 'MGP_KOLICINA_PROIZVEDENA'
          ID = 0
          ParentID = -1
          Index = 0
          Version = 1
        end
        object cxDBEditorRow11: TcxDBEditorRow
          Properties.Caption = #1055#1086#1084#1080#1085#1072#1090#1072':'
          Properties.DataBinding.FieldName = 'MAS_KOLICINA_POMINATA'
          ID = 1
          ParentID = -1
          Index = 1
          Version = 1
        end
        object cxDBEditorRow12: TcxDBEditorRow
          Properties.Caption = #1051#1072#1075#1077#1088':'
          Properties.DataBinding.FieldName = 'MGP_LAGER'
          ID = 2
          ParentID = -1
          Index = 2
          Version = 1
        end
      end
    end
  end
  object cxGridPopupMenu1: TcxGridPopupMenu
    PopupMenus = <>
    Left = 48
    Top = 16
  end
  object PopupMenu1: TPopupMenu
    Left = 80
    Top = 48
    object N1: TMenuItem
      Caption = #1057#1085#1080#1084#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      ShortCut = 49235
      OnClick = SaveToIniFileExecute
    end
    object Excel1: TMenuItem
      Caption = #1057#1085#1080#1084#1080' '#1075#1086' '#1074#1086' Excel'
      ShortCut = 49221
    end
  end
  object dxBarManager1: TdxBarManager
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Segoe UI'
    Font.Style = []
    Categories.Strings = (
      'Default')
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    ImageOptions.Images = dmRes.cxSmallImages
    ImageOptions.LargeImages = dmRes.cxLargeImages
    PopupMenuLinks = <>
    UseSystemFont = True
    Left = 112
    Top = 16
    PixelsPerInch = 96
    object dxBarManager1Bar1: TdxBar
      Caption = 'Custom 1'
      CaptionButtons = <>
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 715
      FloatTop = 157
      FloatClientWidth = 51
      FloatClientHeight = 120
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton1'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton2'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarLargeButton3'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar2: TdxBar
      Caption = #1040#1082#1094#1080#1080
      CaptionButtons = <>
      DockedLeft = 183
      DockedTop = 0
      FloatLeft = 789
      FloatTop = 120
      FloatClientWidth = 51
      FloatClientHeight = 120
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton10'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton5'
        end
        item
          Visible = True
          ItemName = 'dxBarSubItem1'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar3: TdxBar
      Caption = #1048#1079#1083#1077#1079
      CaptionButtons = <>
      DockedLeft = 428
      DockedTop = 0
      FloatLeft = 718
      FloatTop = 62
      FloatClientWidth = 51
      FloatClientHeight = 80
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton7'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton8'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar4: TdxBar
      Caption = #1055#1077#1095#1072#1090#1077#1114#1077
      CaptionButtons = <>
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 676
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton12'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton15'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton9'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarLargeButton13'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1BarIzgledGrid: TdxBar
      Caption = #1058#1072#1073#1077#1083#1072
      CaptionButtons = <>
      DockedLeft = 363
      DockedTop = 0
      FloatLeft = 808
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLBtnSnimiIzgled'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarLBtnBrisiIzgled'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarLargeButton2: TdxBarLargeButton
      Action = aAzuriraj
      Category = 0
    end
    object dxBarLargeButton3: TdxBarLargeButton
      Action = aBrisi
      Category = 0
    end
    object dxBarLargeButton4: TdxBarLargeButton
      Action = aSnimiIzgled
      Category = 0
      ScreenTip = tipSnimiIzgled
    end
    object dxBarLargeButton5: TdxBarLargeButton
      Action = aZacuvajExcel
      Category = 0
      ScreenTip = tipZacuvajExcel
    end
    object dxBarLargeButton6: TdxBarLargeButton
      Action = aPecatiTabela
      Caption = #1055#1077#1095#1072#1090#1080' '#1090#1072#1073#1077#1083#1072
      Category = 0
    end
    object dxBarLargeButton7: TdxBarLargeButton
      Action = aHelp
      Category = 0
    end
    object dxBarLargeButton8: TdxBarLargeButton
      Action = aIzlez
      Category = 0
    end
    object dxBarLargeButton1: TdxBarLargeButton
      Action = aNov
      Category = 0
    end
    object dxBarLargeButton9: TdxBarLargeButton
      Action = aSnimiPecatenje
      Category = 0
      ScreenTip = tipSnimiKonf
    end
    object dxBarLargeButton11: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object ribbonFilterReset: TdxBarLargeButton
      Caption = #1056#1077#1089#1077#1090#1080#1088#1072#1112' '#1092#1080#1083#1090#1077#1088
      Category = 0
      Hint = #1056#1077#1089#1077#1090#1080#1088#1072#1112' '#1092#1080#1083#1090#1077#1088
      Visible = ivAlways
    end
    object dxBarLargeButton12: TdxBarLargeButton
      Action = aPodesuvanjePecatenje
      Category = 0
      ScreenTip = tipDizajnReport
    end
    object dxBarLargeButton13: TdxBarLargeButton
      Action = aBrisiPodesuvanjePecatenje
      Category = 0
      ScreenTip = tipBrisiKonf
    end
    object dxBarLargeButton14: TdxBarLargeButton
      Caption = #1048#1079#1073#1088#1080#1096#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      Category = 0
      Visible = ivAlways
      LargeImageIndex = 40
    end
    object dxBarLargeButton15: TdxBarLargeButton
      Action = aPageSetup
      Category = 0
      ScreenTip = tipSetiranjeStrana
    end
    object dxBarLargeButton16: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object dxBarLargeButton17: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object cxBarEditItem1: TcxBarEditItem
      Caption = 'New Item'
      Category = 0
      Hint = 'New Item'
      Visible = ivAlways
      PropertiesClassName = 'TcxCheckBoxProperties'
    end
    object dxBarSubItem1: TdxBarSubItem
      Caption = #1055#1077#1095#1072#1090#1077#1114#1077
      Category = 0
      Visible = ivAlways
      ImageIndex = 30
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton6'
        end>
    end
    object dxBarLBtnSnimiIzgled: TdxBarLargeButton
      Action = aSnimiIzgled
      Category = 0
    end
    object dxBarLBtnBrisiIzgled: TdxBarLargeButton
      Action = aBrisiIzgled
      Category = 0
    end
    object dxBarLargeButton10: TdxBarLargeButton
      Action = aSpustiSoberi
      Category = 0
    end
  end
  object ActionList1: TActionList
    Images = dmRes.cxSmallImages
    Left = 16
    Top = 16
    object aNov: TAction
      Caption = #1053#1086#1074
      ImageIndex = 10
      ShortCut = 116
      OnExecute = aNovExecute
    end
    object aAzuriraj: TAction
      Caption = #1040#1078#1091#1088#1080#1088#1072#1112
      ImageIndex = 12
      ShortCut = 117
      OnExecute = aAzurirajExecute
    end
    object aBrisi: TAction
      Caption = #1041#1088#1080#1096#1080
      ImageIndex = 11
      ShortCut = 119
      OnExecute = aBrisiExecute
    end
    object aSnimiIzgled: TAction
      Caption = #1057#1085#1080#1084#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      ImageIndex = 9
      ShortCut = 24659
      OnExecute = aSnimiIzgledExecute
    end
    object aZacuvajExcel: TAction
      Caption = #1047#1072#1095#1091#1074#1072#1112' '#1074#1086' Excel'
      ImageIndex = 9
      ShortCut = 24645
      OnExecute = aZacuvajExcelExecute
    end
    object aPecatiTabela: TAction
      Caption = #1055#1077#1095#1072#1090#1080
      ImageIndex = 30
      OnExecute = aPecatiTabelaExecute
    end
    object aHelp: TAction
      Caption = #1055#1086#1084#1086#1096
      ImageIndex = 24
    end
    object aIzlez: TAction
      Caption = #1048#1079#1083#1077#1079
      ImageIndex = 25
      OnExecute = aIzlezExecute
    end
    object aZapisi: TAction
      Caption = #1047#1072#1087#1080#1096#1080
      ImageIndex = 7
      ShortCut = 120
      OnExecute = aZapisiExecute
    end
    object aOtkazi: TAction
      Caption = #1054#1090#1082#1072#1078#1080
      ImageIndex = 21
      ShortCut = 27
      OnExecute = aOtkaziExecute
    end
    object aRefresh: TAction
      Caption = 'aRefresh'
      ImageIndex = 18
      ShortCut = 118
      OnExecute = aRefreshExecute
    end
    object aSnimiPecatenje: TAction
      Caption = #1057#1085#1080#1084#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      ImageIndex = 39
      OnExecute = aSnimiPecatenjeExecute
    end
    object aPodesuvanjePecatenje: TAction
      Caption = #1044#1080#1079#1072#1112#1085' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090
      ImageIndex = 38
      OnExecute = aPodesuvanjePecatenjeExecute
    end
    object aBrisiPodesuvanjePecatenje: TAction
      Caption = #1041#1088#1080#1096#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      ImageIndex = 37
      OnExecute = aBrisiPodesuvanjePecatenjeExecute
    end
    object aPageSetup: TAction
      Caption = #1057#1077#1090#1080#1088#1072#1114#1077' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072
      ImageIndex = 38
      OnExecute = aPageSetupExecute
    end
    object aFormConfig: TAction
      Caption = 'aFormConfig'
      ShortCut = 24699
      OnExecute = aFormConfigExecute
    end
    object aBrisiIzgled: TAction
      Caption = #1041#1088#1080#1096#1080' '#1075#1086' '#1079#1072#1095#1091#1074#1072#1085#1080#1086#1090' '#1080#1079#1075#1083#1077#1076
      ImageIndex = 56
      OnExecute = aBrisiIzgledExecute
    end
    object aSpustiSoberi: TAction
      Caption = #1057#1087#1091#1096#1090#1080'/'#1057#1086#1073#1077#1088#1080' '#1058#1072#1073#1077#1083#1072
      ImageIndex = 65
      OnExecute = aSpustiSoberiExecute
    end
  end
  object dxComponentPrinter1: TdxComponentPrinter
    CurrentLink = dxComponentPrinter1Link1
    Version = 0
    Left = 16
    Top = 48
    object dxComponentPrinter1Link1: TdxGridReportLink
      PrinterPage.DMPaper = 9
      PrinterPage.Footer = 6350
      PrinterPage.GrayShading = True
      PrinterPage.Header = 6350
      PrinterPage.Margins.Bottom = 12700
      PrinterPage.Margins.Left = 12700
      PrinterPage.Margins.Right = 12700
      PrinterPage.Margins.Top = 12700
      PrinterPage.Orientation = poLandscape
      PrinterPage.PageFooter.RightTitle.Strings = (
        #1057#1090#1088'.[Page #] '#1086#1076' [Total Pages]')
      PrinterPage.PageSize.X = 210000
      PrinterPage.PageSize.Y = 297000
      PrinterPage._dxMeasurementUnits_ = 0
      PrinterPage._dxLastMU_ = 2
      ReportDocument.CreationDate = 40336.425359629630000000
      OptionsFormatting.UseNativeStyles = True
      OptionsOnEveryPage.Caption = False
      OptionsOnEveryPage.FilterBar = False
      OptionsView.Caption = False
      OptionsView.FilterBar = False
      StyleRepository = dmRes.cxStyleRepositoryPrinting
      Styles.StyleSheet = dmRes.dxGridReportLinkStyleSheet
      BuiltInReportLink = True
    end
  end
  object dxBarScreenTipRepository1: TdxBarScreenTipRepository
    Left = 144
    Top = 16
    PixelsPerInch = 96
    object tipSnimiIzgled: TdxBarScreenTip
      Header.Text = #1057#1085#1080#1084#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      Description.Text = #1047#1072#1095#1091#1074#1072#1112' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090' '#1085#1072' '#1092#1086#1088#1084#1072#1090#1072' '#1080' '#1085#1072' '#1090#1072#1073#1077#1083#1072#1090#1072
    end
    object tipZacuvajExcel: TdxBarScreenTip
      Header.Text = #1047#1072#1095#1091#1074#1072#1112' '#1074#1086' Excel'
      Description.Text = #1057#1085#1080#1084#1080' '#1075#1080' '#1087#1086#1076#1072#1090#1086#1094#1080#1090#1077' '#1086#1076' '#1090#1072#1073#1077#1083#1072#1090#1072' '#1074#1086' Excel '#1092#1086#1088#1084#1072#1090
    end
    object tipDizajnReport: TdxBarScreenTip
      Header.Text = #1044#1080#1079#1072#1112#1085' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090
      Description.Text = #1055#1086#1076#1077#1089#1091#1074#1072#1114#1072' '#1085#1072' '#1080#1079#1075#1083#1077#1076#1086#1090' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090' '#1085#1072' '#1090#1072#1073#1077#1083#1072#1090#1072
    end
    object tipSetiranjeStrana: TdxBarScreenTip
      Header.Text = #1057#1077#1090#1080#1088#1072#1114#1077' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072
      Description.Text = #1055#1086#1076#1077#1089#1091#1074#1072#1114#1077' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072' '#1085#1072' '#1082#1086#1112#1072' '#1116#1077' '#1089#1077' '#1087#1077#1095#1072#1090#1080' '#1090#1072#1073#1077#1083#1072#1090#1072
    end
    object tipSnimiKonf: TdxBarScreenTip
      Header.Text = #1057#1085#1080#1084#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      Description.Text = #1047#1072#1095#1091#1074#1072#1112' '#1075#1080' '#1087#1086#1076#1077#1089#1091#1074#1072#1114#1072#1090#1072' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090' '#1080' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072' '#1079#1072' '#1087#1077#1095#1072#1090#1077#1114#1077
    end
    object tipBrisiKonf: TdxBarScreenTip
      Header.Text = #1041#1088#1080#1096#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      Description.Text = #1048#1079#1073#1088#1080#1096#1080' '#1075#1080' '#1087#1086#1076#1077#1089#1091#1074#1072#1114#1072#1090#1072' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090' '#1080' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072' '#1079#1072' '#1087#1077#1095#1072#1090#1077#1114#1077
    end
  end
  object tblDokument: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE MAN_PORACKA_STAVKA'
      'SET '
      '    CENA = :CENA,'
      '    VALUTA = :VALUTA,'
      '    KURS = :KURS'
      'WHERE '
      '    ID = :OLD_ID'
      '    ')
    SelectSQL.Strings = (
      'select'
      '    p.ref_no,'
      '    p.tip_partner,'
      '    p.partner,'
      '    mp.naziv naziv_parner,'
      ''
      '    ps.vid_artikal,'
      '    ps.artikal,'
      '    ma.naziv naziv_artikal,'
      '    ps.kolicina kol_poracka,'
      '    ps.tapacir_sediste,'
      '    ps.tapacir_nazad,'
      '    ps.sunger,'
      '    ps.boja_nogarki,'
      '    ps.labels,'
      '    ps.gliders,'
      ''
      '    rn.broj broj_rn,'
      '    rns.kolicina kol_rn,'
      '    rns.lot,'
      ''
      '    pst.id,'
      '    '#39'K'#1072#1084#1080#1086#1085' : '#39'||t.id||'#39' / '#39'||t.naziv opis ,'
      '    pst.id_poracka_stavka,'
      '    pst.id_trailer,'
      '    pst.kolicina kol_trailer,'
      '    t.naziv naziv_trailer,'
      '    t.datum datum_transport'
      ''
      'from man_por_s_trailer pst'
      'inner join man_trailer t on t.id = pst.id_trailer'
      
        'inner join man_poracka_stavka ps on ps.id = pst.id_poracka_stavk' +
        'a and ps.status <> 3 '
      
        'inner join man_poracka p on p.id = ps.id_poracka and p.status <>' +
        ' 3'
      
        'inner join mat_partner mp on mp.tip_partner = p.tip_partner and ' +
        'mp.id = p.partner'
      
        'inner join mtr_artikal ma on ma.artvid = ps.vid_artikal and ma.i' +
        'd = ps.artikal'
      
        'inner join man_raboten_nalog_stavka rns on rns.id_poracka_stavka' +
        ' = ps.id and rns.status <> 3'
      
        'inner join man_raboten_nalog rn on rn.id = rns.id_raboten_nalog ' +
        'and rn.status <> 3'
      '--where rns.lot = :lot'
      'where ps.ID = :id_poracka_stavka')
    AutoUpdateOptions.KeyFields = 'ID'
    AutoUpdateOptions.AutoParamsToFields = True
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 48
    Top = 48
    poAskRecordCount = True
    object tblDokumentREF_NO: TFIBStringField
      FieldName = 'REF_NO'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblDokumentTIP_PARTNER: TFIBIntegerField
      FieldName = 'TIP_PARTNER'
    end
    object tblDokumentPARTNER: TFIBIntegerField
      FieldName = 'PARTNER'
    end
    object tblDokumentNAZIV_PARNER: TFIBStringField
      FieldName = 'NAZIV_PARNER'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblDokumentVID_ARTIKAL: TFIBIntegerField
      FieldName = 'VID_ARTIKAL'
    end
    object tblDokumentARTIKAL: TFIBIntegerField
      FieldName = 'ARTIKAL'
    end
    object tblDokumentNAZIV_ARTIKAL: TFIBStringField
      FieldName = 'NAZIV_ARTIKAL'
      Size = 500
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblDokumentKOL_PORACKA: TFIBFloatField
      FieldName = 'KOL_PORACKA'
    end
    object tblDokumentTAPACIR_SEDISTE: TFIBStringField
      FieldName = 'TAPACIR_SEDISTE'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblDokumentTAPACIR_NAZAD: TFIBStringField
      FieldName = 'TAPACIR_NAZAD'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblDokumentSUNGER: TFIBStringField
      FieldName = 'SUNGER'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblDokumentBOJA_NOGARKI: TFIBStringField
      FieldName = 'BOJA_NOGARKI'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblDokumentLABELS: TFIBStringField
      FieldName = 'LABELS'
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblDokumentGLIDERS: TFIBStringField
      FieldName = 'GLIDERS'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblDokumentBROJ_RN: TFIBStringField
      FieldName = 'BROJ_RN'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblDokumentKOL_RN: TFIBFloatField
      FieldName = 'KOL_RN'
    end
    object tblDokumentLOT: TFIBStringField
      FieldName = 'LOT'
      Size = 30
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblDokumentID: TFIBIntegerField
      FieldName = 'ID'
    end
    object tblDokumentOPIS: TFIBStringField
      FieldName = 'OPIS'
      Size = 123
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblDokumentID_PORACKA_STAVKA: TFIBBCDField
      FieldName = 'ID_PORACKA_STAVKA'
      Size = 0
    end
    object tblDokumentID_TRAILER: TFIBIntegerField
      FieldName = 'ID_TRAILER'
    end
    object tblDokumentKOL_TRAILER: TFIBIntegerField
      FieldName = 'KOL_TRAILER'
    end
    object tblDokumentNAZIV_TRAILER: TFIBStringField
      FieldName = 'NAZIV_TRAILER'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblDokumentDATUM_TRANSPORT: TFIBDateField
      FieldName = 'DATUM_TRANSPORT'
    end
  end
  object dsDokument: TDataSource
    DataSet = tblDokument
    Left = 80
    Top = 16
  end
  object dsProizvodstvo: TDataSource
    DataSet = tblProizvodstvo
    Left = 176
    Top = 16
  end
  object tblProizvodstvo: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE MAN_PORACKA_STAVKA'
      'SET '
      '    CENA = :CENA,'
      '    VALUTA = :VALUTA,'
      '    KURS = :KURS'
      'WHERE '
      '    ID = :OLD_ID'
      '    ')
    SelectSQL.Strings = (
      
        'select A.NAZIV NAZIV_ARTIKAL, RNS.ID_RABOTEN_NALOG, A.NAZIV2, PS' +
        '.CUSTOMER_NAME, RNS.VID_ARTIKAL, RNS.ARTIKAL, P.REF_NO,'
      
        '       PS.TAPACIR_SEDISTE, PS.TAPACIR_NAZAD, PS.SUNGER, PS.BOJA_' +
        'NOGARKI, PS.LABELS, PS.GLIDERS, PS.ZABELESKA,'
      ''
      '       RNS.KOLICINA MAS_KOLICINA_PROIZVEDENA,'
      ''
      '       coalesce((select sum(KTI1.ZAVRSENO)'
      '                 from MAN_KT_IZLEZ KTI1'
      '                 where (KTI1.ID_KT = 1 and'
      
        '                       KTI1.ID_RN_STAVKA = RNS.ID)), 0) MAS_KOLI' +
        'CINA_POMINATA,'
      
        '                       RNS.KOLICINA - coalesce((select sum(KTI1.' +
        'ZAVRSENO)'
      
        '                                                          from M' +
        'AN_KT_IZLEZ KTI1'
      
        '                                                          where ' +
        '(KTI1.ID_KT = 1 and'
      
        '                                                          KTI1.I' +
        'D_RN_STAVKA = RNS.ID)), 0) MAS_LAGER,'
      ''
      '       coalesce((select sum(KTV.PRIMENI)'
      '                 from MAN_KT_VLEZ KTV'
      
        '                 inner join MAN_KT_IZLEZ KTI on KTI.ID = KTV.ID_' +
        'EKT'
      '                 where (KTI.ID_RN_STAVKA = RNS.ID and'
      
        '                       KTV.ID_KT = 2)), 0) FAR_KOLICINA_PROIZVED' +
        'ENA,'
      '                       coalesce((select sum(KTI1.ZAVRSENO)'
      '                                   from MAN_KT_IZLEZ KTI1'
      '                                    where (KTI1.ID_KT = 2 and'
      
        '                                    KTI1.ID_RN_STAVKA = RNS.ID))' +
        ', 0) FAR_KOLICINA_POMINATA,'
      ''
      '       coalesce((select sum(KTV.PRIMENI)'
      '                 from MAN_KT_VLEZ KTV'
      
        '                 inner join MAN_KT_IZLEZ KTI on KTI.ID = KTV.ID_' +
        'EKT'
      '                 where (KTI.ID_RN_STAVKA = RNS.ID and'
      
        '                       KTV.ID_KT = 2)), 0) - coalesce((select su' +
        'm(KTI1.ZAVRSENO)'
      
        '                                                       from MAN_' +
        'KT_IZLEZ KTI1'
      
        '                                                       where (KT' +
        'I1.ID_KT = 2 and'
      
        '                                                             KTI' +
        '1.ID_RN_STAVKA = RNS.ID)), 0) FAR_LAGER,'
      ''
      '       coalesce((select sum(KTV.PRIMENI)'
      '                 from MAN_KT_VLEZ KTV'
      
        '                 inner join MAN_KT_IZLEZ KTI on KTI.ID = KTV.ID_' +
        'EKT'
      '                 where (KTI.ID_RN_STAVKA = RNS.ID and'
      
        '                       KTV.ID_KT = 10)), 0) TAP_KOLICINA_PROIZVE' +
        'DENA,'
      '       coalesce((select sum(KTI1.ZAVRSENO)'
      '                 from MAN_KT_IZLEZ KTI1'
      '                    where (KTI1.ID_KT = 10 and'
      
        '                    KTI1.ID_RN_STAVKA = RNS.ID)), 0) TAP_KOLICIN' +
        'A_POMINATA,'
      '       coalesce((select sum(KTV.PRIMENI)'
      '                 from MAN_KT_VLEZ KTV'
      
        '                 inner join MAN_KT_IZLEZ KTI on KTI.ID = KTV.ID_' +
        'EKT'
      '                 where (KTI.ID_RN_STAVKA = RNS.ID and'
      
        '                 KTV.ID_KT = 10)), 0) - coalesce((select sum(KTI' +
        '1.ZAVRSENO)'
      
        '                                                     from MAN_KT' +
        '_IZLEZ KTI1'
      
        '                                                      where (KTI' +
        '1.ID_KT = 10 and'
      
        '                                                      KTI1.ID_RN' +
        '_STAVKA = RNS.ID)), 0) TAP_LAGER,'
      ''
      '       coalesce((select sum(KTV.PRIMENI)'
      '                 from MAN_KT_VLEZ KTV'
      
        '                 inner join MAN_KT_IZLEZ KTI on KTI.ID = KTV.ID_' +
        'EKT'
      '                 where (KTI.ID_RN_STAVKA = RNS.ID and'
      
        '                       KTV.ID_KT = 19)), 0) MGP_KOLICINA_PROIZVE' +
        'DENA,'
      ''
      '       coalesce((select sum(OSL.KOLICINA)'
      '                 from MTR_OUT_S_LOT OSL'
      
        '                 where OSL.LOT_NO = RNS.LOT), 0) MGP_KOLICINA_PO' +
        'MINATA,'
      ''
      '       coalesce((select sum(KTV.PRIMENI)'
      '                 from MAN_KT_VLEZ KTV'
      
        '                 inner join MAN_KT_IZLEZ KTI on KTI.ID = KTV.ID_' +
        'EKT'
      '                 where (KTI.ID_RN_STAVKA = RNS.ID and'
      
        '                       KTV.ID_KT = 19)), 0) - coalesce((select s' +
        'um(KTI1.ZAVRSENO)'
      
        '                                                        from MAN' +
        '_KT_IZLEZ KTI1'
      
        '                                                        where (K' +
        'TI1.ID_KT = 19 and'
      
        '                                                              KT' +
        'I1.ID_RN_STAVKA = RNS.ID)), 0) -  coalesce((select sum(OSL.KOLIC' +
        'INA)'
      
        '                                                                ' +
        '                                     from MTR_OUT_S_LOT OSL'
      
        '                                                                ' +
        '                                     where OSL.LOT_NO = RNS.LOT)' +
        ', 0) MGP_LAGER'
      '                    '
      ''
      'from MAN_RABOTEN_NALOG_STAVKA RNS'
      
        'inner join MAN_PORACKA_STAVKA PS on PS.ID = RNS.ID_PORACKA_STAVK' +
        'A and'
      '      PS.STATUS <> 3 and'
      '      RNS.STATUS <> 3'
      'inner join MAN_PORACKA P on P.ID = PS.ID_PORACKA and'
      '      PS.STATUS <> 3'
      'inner join MTR_ARTIKAL A on A.ARTVID = RNS.VID_ARTIKAL and'
      '      A.ID = RNS.ARTIKAL'
      'where ps.ID = :id_poracka_stavka  ')
    AutoUpdateOptions.KeyFields = 'ID'
    AutoUpdateOptions.AutoParamsToFields = True
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 144
    Top = 48
    poAskRecordCount = True
    object tblProizvodstvoNAZIV_ARTIKAL: TFIBStringField
      FieldName = 'NAZIV_ARTIKAL'
      Size = 500
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblProizvodstvoID_RABOTEN_NALOG: TFIBBCDField
      FieldName = 'ID_RABOTEN_NALOG'
      Size = 0
    end
    object tblProizvodstvoNAZIV2: TFIBStringField
      FieldName = 'NAZIV2'
      Size = 500
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblProizvodstvoCUSTOMER_NAME: TFIBStringField
      FieldName = 'CUSTOMER_NAME'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblProizvodstvoVID_ARTIKAL: TFIBIntegerField
      FieldName = 'VID_ARTIKAL'
    end
    object tblProizvodstvoARTIKAL: TFIBIntegerField
      FieldName = 'ARTIKAL'
    end
    object tblProizvodstvoREF_NO: TFIBStringField
      FieldName = 'REF_NO'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblProizvodstvoTAPACIR_SEDISTE: TFIBStringField
      FieldName = 'TAPACIR_SEDISTE'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblProizvodstvoTAPACIR_NAZAD: TFIBStringField
      FieldName = 'TAPACIR_NAZAD'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblProizvodstvoSUNGER: TFIBStringField
      FieldName = 'SUNGER'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblProizvodstvoBOJA_NOGARKI: TFIBStringField
      FieldName = 'BOJA_NOGARKI'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblProizvodstvoLABELS: TFIBStringField
      FieldName = 'LABELS'
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblProizvodstvoGLIDERS: TFIBStringField
      FieldName = 'GLIDERS'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblProizvodstvoZABELESKA: TFIBStringField
      FieldName = 'ZABELESKA'
      Size = 500
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblProizvodstvoMAS_KOLICINA_PROIZVEDENA: TFIBFloatField
      FieldName = 'MAS_KOLICINA_PROIZVEDENA'
    end
    object tblProizvodstvoMAS_KOLICINA_POMINATA: TFIBFloatField
      FieldName = 'MAS_KOLICINA_POMINATA'
    end
    object tblProizvodstvoMAS_LAGER: TFIBFloatField
      FieldName = 'MAS_LAGER'
    end
    object tblProizvodstvoFAR_KOLICINA_PROIZVEDENA: TFIBBCDField
      FieldName = 'FAR_KOLICINA_PROIZVEDENA'
      Size = 0
    end
    object tblProizvodstvoFAR_KOLICINA_POMINATA: TFIBFloatField
      FieldName = 'FAR_KOLICINA_POMINATA'
    end
    object tblProizvodstvoFAR_LAGER: TFIBFloatField
      FieldName = 'FAR_LAGER'
    end
    object tblProizvodstvoTAP_KOLICINA_PROIZVEDENA: TFIBBCDField
      FieldName = 'TAP_KOLICINA_PROIZVEDENA'
      Size = 0
    end
    object tblProizvodstvoTAP_KOLICINA_POMINATA: TFIBFloatField
      FieldName = 'TAP_KOLICINA_POMINATA'
    end
    object tblProizvodstvoTAP_LAGER: TFIBFloatField
      FieldName = 'TAP_LAGER'
    end
    object tblProizvodstvoMGP_KOLICINA_PROIZVEDENA: TFIBBCDField
      FieldName = 'MGP_KOLICINA_PROIZVEDENA'
      Size = 0
    end
    object tblProizvodstvoMGP_KOLICINA_POMINATA: TFIBFloatField
      FieldName = 'MGP_KOLICINA_POMINATA'
    end
    object tblProizvodstvoMGP_LAGER: TFIBFloatField
      FieldName = 'MGP_LAGER'
    end
  end
end
