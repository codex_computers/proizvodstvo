object frmIzberiStavkiRN: TfrmIzberiStavkiRN
  Left = 128
  Top = 212
  BorderIcons = [biSystemMenu, biMaximize]
  Caption = #1048#1079#1073#1077#1088#1080' '#1057#1090#1072#1074#1082#1080' '#1086#1076' '#1056#1053
  ClientHeight = 700
  ClientWidth = 1000
  Color = clBtnFace
  Constraints.MinHeight = 500
  Constraints.MinWidth = 610
  Font.Charset = RUSSIAN_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  ShowHint = True
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object dxRibbon1: TdxRibbon
    Left = 0
    Top = 0
    Width = 1000
    Height = 126
    BarManager = dxBarManager1
    ColorSchemeName = 'Blue'
    Contexts = <>
    TabOrder = 0
    TabStop = False
    object dxRibbon1Tab1: TdxRibbonTab
      Active = True
      Caption = #1052#1077#1085#1080
      Groups = <
        item
          ToolbarName = 'dxBarManager1Bar1'
        end
        item
          ToolbarName = 'dxBarManager1Bar2'
        end
        item
          ToolbarName = 'dxBarManager1Bar3'
        end>
      Index = 0
    end
    object dxRibbon1Tab2: TdxRibbonTab
      Caption = #1055#1086#1076#1077#1089#1091#1074#1072#1114#1072
      Groups = <
        item
          ToolbarName = 'dxBarManager1Bar4'
        end
        item
          ToolbarName = 'dxBarManager1BarIzgledGrid'
        end>
      Index = 1
    end
  end
  object StatusBar1: TdxRibbonStatusBar
    Left = 0
    Top = 677
    Width = 1000
    Height = 23
    Panels = <
      item
        PanelStyleClassName = 'TdxStatusBarTextPanelStyle'
        Text = 'F5 - '#1042#1085#1077#1089#1080' '#1089#1090#1072#1074#1082#1080' '#1074#1086' '#1056#1053',  Esc - '#1054#1090#1082#1072#1078#1080' / '#1048#1079#1083#1077#1079
      end>
    Ribbon = dxRibbon1
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clDefault
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
  end
  object dPanel: TPanel
    Left = 0
    Top = 126
    Width = 1000
    Height = 543
    Align = alClient
    TabOrder = 5
    object cxGrid1: TcxGrid
      Left = 1
      Top = 1
      Width = 998
      Height = 541
      Align = alClient
      TabOrder = 0
      RootLevelOptions.DetailTabsPosition = dtpTop
      ExplicitLeft = 2
      ExplicitTop = 4
      object cxGrid1DBTableView1: TcxGridDBTableView
        Navigator.Buttons.CustomButtons = <>
        DataController.DataSource = dm.dsIzberiStavkaRN
        DataController.Options = [dcoAnsiSort, dcoAssignGroupingValues, dcoAssignMasterDetailKeys, dcoSaveExpanding]
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        FilterRow.InfoText = #1050#1083#1080#1082#1085#1077#1090#1077' '#1090#1091#1082#1072' '#1079#1072' '#1076#1072' '#1076#1077#1092#1080#1085#1080#1088#1072#1090#1077' '#1092#1080#1083#1090#1077#1088
        FilterRow.Visible = True
        OptionsBehavior.IncSearch = True
        OptionsCustomize.ColumnsQuickCustomization = True
        OptionsData.Deleting = False
        OptionsData.Inserting = False
        OptionsSelection.MultiSelect = True
        OptionsView.NoDataToDisplayInfoText = '<'#1053#1077#1084#1072' '#1087#1086#1076#1072#1090#1086#1094#1080'>'
        OptionsView.Footer = True
        object cxGrid1DBTableView1Izberi: TcxGridDBColumn
          Caption = #1048#1079#1073#1077#1088#1080
          DataBinding.ValueType = 'Boolean'
          PropertiesClassName = 'TcxCheckBoxProperties'
          Properties.ImmediatePost = True
          Properties.NullStyle = nssUnchecked
          Width = 57
        end
        object cxGrid1DBTableView1ID_RABOTEN_NALOG: TcxGridDBColumn
          Caption = #1064#1080#1092#1088#1072' '#1085#1072' '#1056#1053
          DataBinding.FieldName = 'ID_RABOTEN_NALOG'
          Visible = False
          Options.Editing = False
          Width = 45
        end
        object cxGrid1DBTableView1BROJ: TcxGridDBColumn
          Caption = #1041#1088'. '#1085#1072' '#1056#1053
          DataBinding.FieldName = 'BROJ'
          Visible = False
          GroupIndex = 0
          Options.Editing = False
          Width = 166
        end
        object cxGrid1DBTableView1TP_PORACKA: TcxGridDBColumn
          Caption = #1058#1080#1087' '#1087#1072#1088#1090#1085#1077#1088
          DataBinding.FieldName = 'TP_PORACKA'
          Visible = False
          Options.Editing = False
          Width = 26
        end
        object cxGrid1DBTableView1P_PORACKA: TcxGridDBColumn
          Caption = #1064#1080#1092#1088#1072' '#1085#1072' '#1087#1072#1088#1090#1085#1077#1088
          DataBinding.FieldName = 'P_PORACKA'
          Visible = False
          Options.Editing = False
          Width = 43
        end
        object cxGrid1DBTableView1NAZIV_PARTNER_PORACKA: TcxGridDBColumn
          Caption = #1055#1072#1088#1090#1085#1077#1088
          DataBinding.FieldName = 'NAZIV_PARTNER_PORACKA'
          Visible = False
          Options.Editing = False
          Width = 149
        end
        object cxGrid1DBTableView1ID_RN_STAVKA: TcxGridDBColumn
          Caption = #1064#1080#1092#1088#1072' '#1085#1072' '#1089#1090#1072#1074#1082#1072' '#1085#1072' '#1056#1053
          DataBinding.FieldName = 'ID_RN_STAVKA'
          Visible = False
          Options.Editing = False
          Width = 140
        end
        object cxGrid1DBTableView1VID_ARTIKAL: TcxGridDBColumn
          Caption = #1042#1080#1076' '#1085#1072' '#1072#1088#1090#1080#1082#1072#1083
          DataBinding.FieldName = 'VID_ARTIKAL'
          Options.Editing = False
          Width = 42
        end
        object cxGrid1DBTableView1ARTIKAL: TcxGridDBColumn
          Caption = #1064#1080#1092#1088#1072' '#1085#1072' '#1072#1088#1090#1080#1082#1072#1083
          DataBinding.FieldName = 'ARTIKAL'
          Options.Editing = False
        end
        object cxGrid1DBTableView1NAZIV_ARTIKAL: TcxGridDBColumn
          Caption = #1040#1088#1090#1080#1082#1072#1083
          DataBinding.FieldName = 'NAZIV_ARTIKAL'
          Options.Editing = False
          Width = 235
        end
        object cxGrid1DBTableView1KOLICINA: TcxGridDBColumn
          Caption = #1042#1082#1091#1087#1085#1072' '#1050#1086#1083#1080#1095#1080#1085#1072
          DataBinding.FieldName = 'KOLICINA'
          Visible = False
          Options.Editing = False
        end
        object cxGrid1DBTableView1KOLICINA_OSTANATA: TcxGridDBColumn
          Caption = #1050#1086#1083#1080#1095#1080#1085#1072
          DataBinding.FieldName = 'KOLICINA_OSTANATA'
          Width = 92
        end
        object cxGrid1DBTableView1MERKA: TcxGridDBColumn
          Caption = #1052#1077#1088#1082#1072
          DataBinding.FieldName = 'MERKA'
          Options.Editing = False
          Width = 67
        end
        object cxGrid1DBTableView1ZABELESKA: TcxGridDBColumn
          Caption = #1047#1072#1073#1077#1083#1077#1096#1082#1072
          DataBinding.FieldName = 'ZABELESKA'
          Options.Editing = False
          Width = 76
        end
        object cxGrid1DBTableView1LOT: TcxGridDBColumn
          DataBinding.FieldName = 'LOT'
          Visible = False
        end
        object cxGrid1DBTableView1ID_PORACKA_STAVKA: TcxGridDBColumn
          DataBinding.FieldName = 'ID_PORACKA_STAVKA'
          Visible = False
        end
        object cxGrid1DBTableView1SIFRA_PORACKA: TcxGridDBColumn
          AlternateCaption = #1064#1080#1092#1088#1072' '#1085#1072' '#1087#1086#1088#1072#1095#1082#1072
          Caption = #1064#1080#1092#1088#1072' '#1085#1072' '#1087#1086#1088#1072#1095#1082#1072
          DataBinding.FieldName = 'SIFRA_PORACKA'
        end
        object cxGrid1DBTableView1REF_NO: TcxGridDBColumn
          Caption = #1056#1077#1092'. '#1073#1088'. '#1087#1086#1088#1072#1095#1082#1072
          DataBinding.FieldName = 'REF_NO'
          Width = 102
        end
        object cxGrid1DBTableView1SIFRA_PORACKA_STAVKA: TcxGridDBColumn
          Caption = #1064#1080#1092#1088#1072' '#1085#1072' '#1089#1090'. '#1085#1072' '#1087#1086#1088#1072#1095#1082#1072
          DataBinding.FieldName = 'SIFRA_PORACKA_STAVKA'
        end
        object cxGrid1DBTableView1BROJ_PORACKA_STAVKA: TcxGridDBColumn
          Caption = #1041#1088'. '#1085#1072' '#1089#1090'. '#1085#1072' '#1087#1086#1088#1072#1095#1082#1072
          DataBinding.FieldName = 'BROJ_PORACKA_STAVKA'
          Width = 50
        end
        object cxGrid1DBTableView1TAPACIR_SEDISTE: TcxGridDBColumn
          Caption = #1058#1072#1087#1072#1094#1080#1088' '#1089#1077#1076#1080#1096#1090#1077
          DataBinding.FieldName = 'TAPACIR_SEDISTE'
          Width = 100
        end
        object cxGrid1DBTableView1TAPACIR_NAZAD: TcxGridDBColumn
          Caption = #1058#1072#1087#1072#1094#1080#1088' '#1085#1072#1079#1072#1076
          DataBinding.FieldName = 'TAPACIR_NAZAD'
          Width = 100
        end
        object cxGrid1DBTableView1SUNGER: TcxGridDBColumn
          Caption = #1057#1091#1085#1107#1077#1088
          DataBinding.FieldName = 'SUNGER'
          Width = 100
        end
        object cxGrid1DBTableView1BOJA_NOGARKI: TcxGridDBColumn
          Caption = #1041#1086#1112#1072' '#1085#1072' '#1085#1086#1075#1072#1088#1082#1080
          DataBinding.FieldName = 'BOJA_NOGARKI'
          Width = 100
        end
        object cxGrid1DBTableView1LABELS: TcxGridDBColumn
          DataBinding.FieldName = 'LABELS'
          Width = 100
        end
        object cxGrid1DBTableView1GLIDERS: TcxGridDBColumn
          DataBinding.FieldName = 'GLIDERS'
          Width = 100
        end
        object cxGrid1DBTableView1KOLICINA_GOTOVA: TcxGridDBColumn
          Caption = #1050#1086#1083#1080#1095#1080#1085#1072' '#1075#1086#1090#1086#1074#1072
          DataBinding.FieldName = 'KOLICINA_GOTOVA'
        end
      end
      object cxGrid1Level1: TcxGridLevel
        Caption = #1053#1077#1079#1072#1074#1088#1096#1077#1085#1080' '#1089#1090#1072#1074#1082#1080
        GridView = cxGrid1DBTableView1
      end
    end
  end
  object cxSplitter2: TcxSplitter
    Left = 0
    Top = 669
    Width = 1000
    Height = 8
    AlignSplitter = salBottom
    Control = StatusBar1
  end
  object cxGridPopupMenu1: TcxGridPopupMenu
    Grid = cxGrid1
    PopupMenus = <>
    Left = 368
    Top = 472
  end
  object PopupMenu1: TPopupMenu
    Left = 480
    Top = 472
    object N1: TMenuItem
      Caption = #1057#1085#1080#1084#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      ShortCut = 49235
      OnClick = SaveToIniFileExecute
    end
    object Excel1: TMenuItem
      Caption = #1057#1085#1080#1084#1080' '#1075#1086' '#1074#1086' Excel'
      ShortCut = 49221
    end
  end
  object dxBarManager1: TdxBarManager
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Segoe UI'
    Font.Style = []
    Categories.Strings = (
      'Default')
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    ImageOptions.Images = dmRes.cxSmallImages
    ImageOptions.LargeImages = dmRes.cxLargeImages
    PopupMenuLinks = <>
    UseSystemFont = True
    Left = 560
    Top = 472
    DockControlHeights = (
      0
      0
      0
      0)
    object dxBarManager1Bar1: TdxBar
      Caption = 'Custom 1'
      CaptionButtons = <>
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 715
      FloatTop = 157
      FloatClientWidth = 51
      FloatClientHeight = 120
      ItemLinks = <
        item
          ViewLayout = ivlGlyphControlCaption
          Visible = True
          ItemName = 'cbSite'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarLargeButton1'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar2: TdxBar
      Caption = #1040#1082#1094#1080#1080
      CaptionButtons = <>
      DockedLeft = 213
      DockedTop = 0
      FloatLeft = 789
      FloatTop = 120
      FloatClientWidth = 51
      FloatClientHeight = 120
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton10'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton5'
        end
        item
          Visible = True
          ItemName = 'dxBarSubItem1'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar3: TdxBar
      Caption = #1048#1079#1083#1077#1079
      CaptionButtons = <>
      DockedLeft = 458
      DockedTop = 0
      FloatLeft = 718
      FloatTop = 62
      FloatClientWidth = 51
      FloatClientHeight = 80
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton7'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton8'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar4: TdxBar
      Caption = #1055#1077#1095#1072#1090#1077#1114#1077
      CaptionButtons = <>
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 676
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton12'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton15'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton9'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarLargeButton13'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1BarIzgledGrid: TdxBar
      Caption = #1058#1072#1073#1077#1083#1072
      CaptionButtons = <>
      DockedLeft = 363
      DockedTop = 0
      FloatLeft = 808
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLBtnSnimiIzgled'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarLBtnBrisiIzgled'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarLargeButton2: TdxBarLargeButton
      Action = aAzuriraj
      Category = 0
    end
    object dxBarLargeButton3: TdxBarLargeButton
      Action = aBrisi
      Category = 0
    end
    object dxBarLargeButton4: TdxBarLargeButton
      Action = aSnimiIzgled
      Category = 0
      ScreenTip = tipSnimiIzgled
    end
    object dxBarLargeButton5: TdxBarLargeButton
      Action = aZacuvajExcel
      Category = 0
      ScreenTip = tipZacuvajExcel
    end
    object dxBarLargeButton6: TdxBarLargeButton
      Action = aPecatiTabela
      Caption = #1055#1077#1095#1072#1090#1080' '#1090#1072#1073#1077#1083#1072
      Category = 0
    end
    object dxBarLargeButton7: TdxBarLargeButton
      Action = aHelp
      Category = 0
    end
    object dxBarLargeButton8: TdxBarLargeButton
      Action = aIzlez
      Category = 0
    end
    object dxBarLargeButton1: TdxBarLargeButton
      Action = aKreirajRN
      Caption = #1044#1086#1076#1072#1074#1072#1114#1077' '#1085#1072' '#1089#1090#1072#1074#1082#1080' '#1074#1086' '#1056#1053
      Category = 0
    end
    object dxBarLargeButton9: TdxBarLargeButton
      Action = aSnimiPecatenje
      Category = 0
      ScreenTip = tipSnimiKonf
    end
    object dxBarLargeButton11: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object ribbonFilterReset: TdxBarLargeButton
      Caption = #1056#1077#1089#1077#1090#1080#1088#1072#1112' '#1092#1080#1083#1090#1077#1088
      Category = 0
      Hint = #1056#1077#1089#1077#1090#1080#1088#1072#1112' '#1092#1080#1083#1090#1077#1088
      Visible = ivAlways
    end
    object dxBarLargeButton12: TdxBarLargeButton
      Action = aPodesuvanjePecatenje
      Category = 0
      ScreenTip = tipDizajnReport
    end
    object dxBarLargeButton13: TdxBarLargeButton
      Action = aBrisiPodesuvanjePecatenje
      Category = 0
      ScreenTip = tipBrisiKonf
    end
    object dxBarLargeButton14: TdxBarLargeButton
      Caption = #1048#1079#1073#1088#1080#1096#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      Category = 0
      Visible = ivAlways
      LargeImageIndex = 40
    end
    object dxBarLargeButton15: TdxBarLargeButton
      Action = aPageSetup
      Category = 0
      ScreenTip = tipSetiranjeStrana
    end
    object dxBarLargeButton16: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object dxBarLargeButton17: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object cxBarEditItem1: TcxBarEditItem
      Caption = 'New Item'
      Category = 0
      Hint = 'New Item'
      Visible = ivAlways
      PropertiesClassName = 'TcxCheckBoxProperties'
    end
    object dxBarSubItem1: TdxBarSubItem
      Caption = #1055#1077#1095#1072#1090#1077#1114#1077
      Category = 0
      Visible = ivAlways
      ImageIndex = 30
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton6'
        end>
    end
    object dxBarLBtnSnimiIzgled: TdxBarLargeButton
      Action = aSnimiIzgled
      Category = 0
    end
    object dxBarLBtnBrisiIzgled: TdxBarLargeButton
      Action = aBrisiIzgled
      Category = 0
    end
    object dxBarLargeButton10: TdxBarLargeButton
      Action = aSpustiSoberi
      Category = 0
    end
    object cbSite: TcxBarEditItem
      Action = aSteStavki
      Category = 0
      ShowCaption = True
      Width = 0
      PropertiesClassName = 'TcxCheckBoxProperties'
      Properties.ImmediatePost = True
      Properties.NullStyle = nssUnchecked
      Properties.OnChange = cbSitePropertiesChange
    end
  end
  object ActionList1: TActionList
    Images = dmRes.cxSmallImages
    Left = 184
    Top = 320
    object aNov: TAction
      Caption = #1053#1086#1074
      ImageIndex = 10
      OnExecute = aNovExecute
    end
    object aAzuriraj: TAction
      Caption = #1040#1078#1091#1088#1080#1088#1072#1112
      ImageIndex = 12
      ShortCut = 117
      OnExecute = aAzurirajExecute
    end
    object aBrisi: TAction
      Caption = #1041#1088#1080#1096#1080
      ImageIndex = 11
      ShortCut = 119
      OnExecute = aBrisiExecute
    end
    object aSnimiIzgled: TAction
      Caption = #1057#1085#1080#1084#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      ImageIndex = 9
      ShortCut = 24659
      OnExecute = aSnimiIzgledExecute
    end
    object aZacuvajExcel: TAction
      Caption = #1047#1072#1095#1091#1074#1072#1112' '#1074#1086' Excel'
      ImageIndex = 9
      ShortCut = 24645
      OnExecute = aZacuvajExcelExecute
    end
    object aPecatiTabela: TAction
      Caption = #1055#1077#1095#1072#1090#1080
      ImageIndex = 30
      OnExecute = aPecatiTabelaExecute
    end
    object aHelp: TAction
      Caption = #1055#1086#1084#1086#1096
      ImageIndex = 24
    end
    object aIzlez: TAction
      Caption = #1048#1079#1083#1077#1079
      ImageIndex = 25
      OnExecute = aIzlezExecute
    end
    object aZapisi: TAction
      Caption = #1047#1072#1087#1080#1096#1080
      ImageIndex = 7
      ShortCut = 120
      OnExecute = aZapisiExecute
    end
    object aOtkazi: TAction
      Caption = #1054#1090#1082#1072#1078#1080
      ImageIndex = 21
      ShortCut = 27
      OnExecute = aOtkaziExecute
    end
    object aRefresh: TAction
      Caption = 'aRefresh'
      ImageIndex = 18
      ShortCut = 118
      OnExecute = aRefreshExecute
    end
    object aSnimiPecatenje: TAction
      Caption = #1057#1085#1080#1084#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      ImageIndex = 39
      OnExecute = aSnimiPecatenjeExecute
    end
    object aPodesuvanjePecatenje: TAction
      Caption = #1044#1080#1079#1072#1112#1085' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090
      ImageIndex = 38
      OnExecute = aPodesuvanjePecatenjeExecute
    end
    object aBrisiPodesuvanjePecatenje: TAction
      Caption = #1041#1088#1080#1096#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      ImageIndex = 37
      OnExecute = aBrisiPodesuvanjePecatenjeExecute
    end
    object aPageSetup: TAction
      Caption = #1057#1077#1090#1080#1088#1072#1114#1077' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072
      ImageIndex = 38
      OnExecute = aPageSetupExecute
    end
    object aFormConfig: TAction
      Caption = 'aFormConfig'
      ShortCut = 24699
      OnExecute = aFormConfigExecute
    end
    object aBrisiIzgled: TAction
      Caption = #1041#1088#1080#1096#1080' '#1075#1086' '#1079#1072#1095#1091#1074#1072#1085#1080#1086#1090' '#1080#1079#1075#1083#1077#1076
      ImageIndex = 56
      OnExecute = aBrisiIzgledExecute
    end
    object aSpustiSoberi: TAction
      Caption = #1057#1087#1091#1096#1090#1080'/'#1057#1086#1073#1077#1088#1080' '#1058#1072#1073#1077#1083#1072
      ImageIndex = 65
      OnExecute = aSpustiSoberiExecute
    end
    object aKreirajRN: TAction
      Caption = #1050#1088#1077#1080#1088#1072#1112' '#1056#1072#1073#1086#1090#1077#1085' '#1085#1072#1083#1086#1075
      ImageIndex = 10
      ShortCut = 116
      OnExecute = aKreirajRNExecute
    end
    object aSteStavki: TAction
      Caption = #1054#1079#1085#1072#1095#1080' '#1075#1086' '#1089#1080#1090#1077
      OnExecute = aSteStavkiExecute
    end
  end
  object dxComponentPrinter1: TdxComponentPrinter
    CurrentLink = dxComponentPrinter1Link1
    Version = 0
    Left = 280
    Top = 320
    object dxComponentPrinter1Link1: TdxGridReportLink
      Component = cxGrid1
      PrinterPage.DMPaper = 9
      PrinterPage.Footer = 6350
      PrinterPage.GrayShading = True
      PrinterPage.Header = 6350
      PrinterPage.Margins.Bottom = 12700
      PrinterPage.Margins.Left = 12700
      PrinterPage.Margins.Right = 12700
      PrinterPage.Margins.Top = 12700
      PrinterPage.Orientation = poLandscape
      PrinterPage.PageFooter.RightTitle.Strings = (
        #1057#1090#1088'.[Page #] '#1086#1076' [Total Pages]')
      PrinterPage.PageSize.X = 210000
      PrinterPage.PageSize.Y = 297000
      PrinterPage._dxMeasurementUnits_ = 0
      PrinterPage._dxLastMU_ = 2
      ReportDocument.CreationDate = 40336.425359629630000000
      OptionsFormatting.UseNativeStyles = True
      OptionsOnEveryPage.Caption = False
      OptionsOnEveryPage.FilterBar = False
      OptionsView.Caption = False
      OptionsView.FilterBar = False
      StyleRepository = dmRes.cxStyleRepositoryPrinting
      Styles.StyleSheet = dmRes.dxGridReportLinkStyleSheet
      BuiltInReportLink = True
    end
  end
  object dxBarScreenTipRepository1: TdxBarScreenTipRepository
    Left = 344
    Top = 416
    object tipSnimiIzgled: TdxBarScreenTip
      Header.Text = #1057#1085#1080#1084#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      Description.Text = #1047#1072#1095#1091#1074#1072#1112' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090' '#1085#1072' '#1092#1086#1088#1084#1072#1090#1072' '#1080' '#1085#1072' '#1090#1072#1073#1077#1083#1072#1090#1072
    end
    object tipZacuvajExcel: TdxBarScreenTip
      Header.Text = #1047#1072#1095#1091#1074#1072#1112' '#1074#1086' Excel'
      Description.Text = #1057#1085#1080#1084#1080' '#1075#1080' '#1087#1086#1076#1072#1090#1086#1094#1080#1090#1077' '#1086#1076' '#1090#1072#1073#1077#1083#1072#1090#1072' '#1074#1086' Excel '#1092#1086#1088#1084#1072#1090
    end
    object tipDizajnReport: TdxBarScreenTip
      Header.Text = #1044#1080#1079#1072#1112#1085' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090
      Description.Text = #1055#1086#1076#1077#1089#1091#1074#1072#1114#1072' '#1085#1072' '#1080#1079#1075#1083#1077#1076#1086#1090' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090' '#1085#1072' '#1090#1072#1073#1077#1083#1072#1090#1072
    end
    object tipSetiranjeStrana: TdxBarScreenTip
      Header.Text = #1057#1077#1090#1080#1088#1072#1114#1077' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072
      Description.Text = #1055#1086#1076#1077#1089#1091#1074#1072#1114#1077' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072' '#1085#1072' '#1082#1086#1112#1072' '#1116#1077' '#1089#1077' '#1087#1077#1095#1072#1090#1080' '#1090#1072#1073#1077#1083#1072#1090#1072
    end
    object tipSnimiKonf: TdxBarScreenTip
      Header.Text = #1057#1085#1080#1084#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      Description.Text = #1047#1072#1095#1091#1074#1072#1112' '#1075#1080' '#1087#1086#1076#1077#1089#1091#1074#1072#1114#1072#1090#1072' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090' '#1080' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072' '#1079#1072' '#1087#1077#1095#1072#1090#1077#1114#1077
    end
    object tipBrisiKonf: TdxBarScreenTip
      Header.Text = #1041#1088#1080#1096#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      Description.Text = #1048#1079#1073#1088#1080#1096#1080' '#1075#1080' '#1087#1086#1076#1077#1089#1091#1074#1072#1114#1072#1090#1072' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090' '#1080' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072' '#1079#1072' '#1087#1077#1095#1072#1090#1077#1114#1077
    end
  end
  object qRNStavka: TpFIBUpdateObject
    Transaction = dmKon.qTransaction
    Database = dmKon.fibBaza
    SQL.Strings = (
      'UPDATE MAN_RABOTEN_NALOG_STAVKA SET '
      '    STATUS=:STATUS'
      'WHERE'
      '    ID=:ID')
    DataSet = dm.tblRNStavka
    OrderInList = 1
    Left = 576
    Top = 296
    qoAutoCommit = True
    qoStartTransaction = True
  end
  object qRNStavkaMain: TpFIBUpdateObject
    Transaction = dmKon.qTransaction
    Database = dmKon.fibBaza
    SQL.Strings = (
      'UPDATE MAN_RABOTEN_NALOG_STAVKA SET '
      '    KOLICINA=:KOLICINA'
      'WHERE'
      '    ID=:ID')
    DataSet = dm.tblRNStavka
    OrderInList = 2
    Left = 656
    Top = 296
    qoAutoCommit = True
    qoStartTransaction = True
  end
  object pFIBUpdateObject1: TpFIBUpdateObject
    Transaction = dmKon.qTransaction
    Database = dmKon.fibBaza
    SQL.Strings = (
      'UPDATE MAN_RABOTEN_NALOG_STAVKA SET '
      '    KOLICINA=:KOLICINA'
      'WHERE'
      '    ID=:ID')
    DataSet = dm.tblRNStavka
    OrderInList = 0
    Left = 728
    Top = 296
    qoAutoCommit = True
    qoStartTransaction = True
  end
  object qPorPartner: TpFIBQuery
    Transaction = dmKon.qTransaction
    Database = dmKon.fibBaza
    SQL.Strings = (
      'select distinct P.TIP_PARTNER || P.PARTNER TPP'
      'from MAN_PORACKA P'
      'where P.ID_RN = :ID_RN   ')
    Left = 156
    Top = 87
    qoStartTransaction = True
  end
end
