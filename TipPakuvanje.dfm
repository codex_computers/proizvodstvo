inherited frmTipPakuvanje: TfrmTipPakuvanje
  Caption = #1058#1080#1087' '#1085#1072' '#1087#1072#1082#1091#1074#1072#1114#1077
  ClientHeight = 564
  ClientWidth = 594
  ExplicitWidth = 610
  ExplicitHeight = 603
  PixelsPerInch = 96
  TextHeight = 13
  inherited lPanel: TPanel
    Width = 594
    Height = 261
    TabOrder = 1
    ExplicitWidth = 594
    ExplicitHeight = 261
    inherited cxGrid1: TcxGrid
      Width = 590
      Height = 257
      ExplicitWidth = 590
      ExplicitHeight = 257
      inherited cxGrid1DBTableView1: TcxGridDBTableView
        DataController.DataSource = dm.dsTipPakuvanje
        object cxGrid1DBTableView1ID: TcxGridDBColumn
          Caption = #1064#1080#1092#1088#1072
          DataBinding.FieldName = 'ID'
        end
        object cxGrid1DBTableView1NAZIV: TcxGridDBColumn
          Caption = #1053#1072#1079#1080#1074
          DataBinding.FieldName = 'NAZIV'
        end
      end
    end
  end
  inherited dPanel: TPanel
    Top = 387
    Width = 594
    TabOrder = 5
    ExplicitTop = 387
    ExplicitWidth = 594
    inherited Label1: TLabel
      Left = 543
      Top = 29
      Visible = False
      ExplicitLeft = 543
      ExplicitTop = 29
    end
    object lbl1: TLabel [1]
      Left = 31
      Top = 47
      Width = 40
      Height = 13
      Caption = #1053#1072#1079#1080#1074' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    inherited Sifra: TcxDBTextEdit
      Tag = 0
      Left = 595
      Top = 26
      DataBinding.DataField = 'ID'
      DataBinding.DataSource = dm.dsTipPakuvanje
      TabOrder = 3
      Visible = False
      ExplicitLeft = 595
      ExplicitTop = 26
    end
    inherited OtkaziButton: TcxButton
      Left = 503
      ExplicitLeft = 503
    end
    inherited ZapisiButton: TcxButton
      Left = 422
      ExplicitLeft = 422
    end
    object txtNaziv: TcxDBTextEdit
      Tag = 1
      Left = 83
      Top = 44
      BeepOnEnter = False
      DataBinding.DataField = 'NAZIV'
      DataBinding.DataSource = dm.dsTipPakuvanje
      ParentFont = False
      Properties.BeepOnError = True
      Style.Shadow = False
      TabOrder = 0
      OnKeyDown = EnterKakoTab
      Width = 302
    end
  end
  inherited dxRibbon1: TdxRibbon
    Width = 594
    TabOrder = 0
    ExplicitWidth = 594
    inherited dxRibbon1Tab1: TdxRibbonTab
      Index = 0
    end
    inherited dxRibbon1Tab2: TdxRibbonTab
      Index = 1
    end
  end
  inherited StatusBar1: TdxRibbonStatusBar
    Top = 541
    Width = 594
    ExplicitTop = 541
    ExplicitWidth = 594
  end
  inherited cxGridPopupMenu1: TcxGridPopupMenu
    Left = 48
    Top = 16
  end
  inherited PopupMenu1: TPopupMenu
    Left = 16
    Top = 48
  end
  inherited dxBarManager1: TdxBarManager
    Left = 80
    Top = 16
    DockControlHeights = (
      0
      0
      0
      0)
    inherited dxBarManager1Bar1: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 69
      FloatClientHeight = 167
    end
    inherited dxBarManager1Bar2: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 149
      FloatClientHeight = 130
    end
    inherited dxBarManager1Bar3: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 59
      FloatClientHeight = 108
    end
    inherited dxBarManager1Bar4: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 141
      FloatClientHeight = 221
    end
    inherited dxBarManager1BarTabela: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 174
      FloatClientHeight = 113
    end
  end
  inherited ActionList1: TActionList
    Left = 16
    Top = 16
  end
  inherited dxComponentPrinter1: TdxComponentPrinter
    Left = 144
    Top = 16
    inherited dxComponentPrinter1Link1: TdxGridReportLink
      ReportDocument.CreationDate = 41668.385385439810000000
      AssignedFormatValues = [fvDate, fvTime, fvPageNumber]
      Styles.BandHeader = nil
      Styles.Caption = nil
      Styles.CardCaptionRow = nil
      Styles.CardRowCaption = nil
      Styles.Content = nil
      Styles.ContentEven = nil
      Styles.ContentOdd = nil
      Styles.FilterBar = nil
      Styles.Footer = nil
      Styles.Group = nil
      Styles.Header = nil
      Styles.Preview = nil
      Styles.Selection = nil
      BuiltInReportLink = True
    end
  end
  inherited dxBarScreenTipRepository1: TdxBarScreenTipRepository
    Left = 112
    Top = 16
  end
end
