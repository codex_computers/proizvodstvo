unit OutLots;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData, cxEdit,
  DB, cxDBData, cxGridCustomPopupMenu, cxGridPopupMenu,
  ComCtrls, cxContainer, cxTextEdit, cxDBEdit, StdCtrls, cxGridLevel,
  cxClasses, cxControls, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGrid, ExtCtrls, Menus, ActnList,
  cxGridExportLink, cxExport, cxLookAndFeelPainters, cxDataStorage,
  cxButtons, dxStatusBar, dxRibbonStatusBar, dxRibbon, dxBar, dxBarExtItems,
  dxPSGlbl, dxPSUtl, dxPSEngn, dxPrnPg, dxBkgnd, dxWrap, dxPrnDev,
  dxPSCompsProvider, dxPSFillPatterns, dxPSEdgePatterns, dxPSCore, dxPScxCommon,
  cxPC, cxMemo, cxLocalization, cxLookAndFeels,
  dxPSPDFExportCore, dxPSPDFExport, cxDrawTextUtils, dxPSPrVwStd, dxPSPrVwAdv,
  dxPSPrVwRibbon, dxPScxEditorProducers, dxPScxExtEditorProducers,
  dxPScxPageControlProducer, dxPgsDlg, cxCheckBox, cxBarEditItem, dxSkinsCore,
  dxSkinOffice2007Blue, dxSkinscxPCPainter, dxSkinsdxRibbonPainter,
  dxSkinsdxBarPainter, dxBarSkinnedCustForm, dxSkinBlack, dxSkinBlue,
  dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy,
  dxSkinGlassOceans, dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky,
  dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins, dxSkinOffice2007Black,
  dxSkinOffice2007Green, dxSkinOffice2007Pink, dxSkinOffice2007Silver,
  dxSkinPumpkin, dxSkinSeven, dxSkinSharp, dxSkinSilver, dxSkinSpringTime,
  dxSkinStardust, dxSkinSummer2008, dxSkinsDefaultPainters, dxSkinValentine,
  dxSkinXmas2008Blue, cxDropDownEdit, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxRibbonSkins, dxPScxGridLnk,
  dxPScxGridLayoutViewLnk, dxScreenTip, dxSkinBlueprint,
  dxSkinDevExpressDarkStyle, dxSkinDevExpressStyle, dxSkinHighContrast,
  dxSkinSevenClassic, dxSkinSharpPlus, dxSkinTheAsphaltWorld, dxSkinVS2010,
  dxSkinWhiteprint, dxPScxPivotGridLnk, dmResources, FIBDataSet, pFIBDataSet,
  cxMaskEdit, cxCalendar, IBCustomDataSet;

type
//  niza = Array[1..5] of Variant;

  TfrmOutLots = class(TForm)
    dPanel: TPanel;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    lPanel: TPanel;
    Label1: TLabel;
    Sifra: TcxDBTextEdit;
    cxGridPopupMenu1: TcxGridPopupMenu;
    PopupMenu1: TPopupMenu;
    N1: TMenuItem;
    Excel1: TMenuItem;
    ZapisiButton: TcxButton;
    OtkaziButton: TcxButton;
    dxRibbon1Tab1: TdxRibbonTab;
    dxRibbon1: TdxRibbon;
    StatusBar1: TdxRibbonStatusBar;
    dxBarManager1: TdxBarManager;
    dxBarManager1Bar1: TdxBar;
    dxBarManager1Bar2: TdxBar;
    dxBarManager1Bar3: TdxBar;
    dxBarLargeButton2: TdxBarLargeButton;
    dxBarLargeButton3: TdxBarLargeButton;
    dxBarLargeButton4: TdxBarLargeButton;
    dxBarLargeButton5: TdxBarLargeButton;
    dxBarLargeButton6: TdxBarLargeButton;
    dxBarLargeButton7: TdxBarLargeButton;
    dxBarLargeButton8: TdxBarLargeButton;
    dxBarLargeButton1: TdxBarLargeButton;
    ActionList1: TActionList;
    aNov: TAction;
    aAzuriraj: TAction;
    aBrisi: TAction;
    aSnimiIzgled: TAction;
    aZacuvajExcel: TAction;
    aPecatiTabela: TAction;
    aHelp: TAction;
    aIzlez: TAction;
    aZapisi: TAction;
    aOtkazi: TAction;
    aRefresh: TAction;
    dxRibbon1Tab2: TdxRibbonTab;
    dxBarManager1Bar4: TdxBar;
    dxBarLargeButton9: TdxBarLargeButton;
    aSnimiPecatenje: TAction;
    dxComponentPrinter1: TdxComponentPrinter;
    dxComponentPrinter1Link1: TdxGridReportLink;
    ribbonFilterReset: TdxBarLargeButton;
    aPodesuvanjePecatenje: TAction;
    dxBarLargeButton12: TdxBarLargeButton;
    aBrisiPodesuvanjePecatenje: TAction;
    dxBarLargeButton13: TdxBarLargeButton;
    dxBarLargeButton14: TdxBarLargeButton;
    aPageSetup: TAction;
    dxBarLargeButton15: TdxBarLargeButton;
    dxBarSubItem1: TdxBarSubItem;
    aFormConfig: TAction;
    dxBarScreenTipRepository1: TdxBarScreenTipRepository;
    tipSnimiIzgled: TdxBarScreenTip;
    tipZacuvajExcel: TdxBarScreenTip;
    tipDizajnReport: TdxBarScreenTip;
    tipSetiranjeStrana: TdxBarScreenTip;
    tipSnimiKonf: TdxBarScreenTip;
    tipBrisiKonf: TdxBarScreenTip;
    cxBarEditItem1: TcxBarEditItem;
    dxBarManager1BarTabela: TdxBar;
    dxBarLBtnSnimiIzgled: TdxBarLargeButton;
    dxBarLBtnBrisiIzgled: TdxBarLargeButton;
    aBrisiIzgled: TAction;
    dxBarLargeButtonSoberi: TdxBarLargeButton;
    aSpustiSoberi: TAction;
    dsUpotrebeni: TDataSource;
    tblUpotrebeni: TpFIBDataSet;
    tblUpotrebeniID: TFIBBCDField;
    tblUpotrebeniLOT_NO: TFIBStringField;
    tblUpotrebeniKOLICINA: TFIBFloatField;
    tblUpotrebeniVAZI_DO: TFIBDateField;
    tblUpotrebeniCUSTOM1: TFIBStringField;
    tblUpotrebeniCUSTOM2: TFIBStringField;
    tblUpotrebeniCUSTOM3: TFIBStringField;
    tblUpotrebeniTS_INS: TFIBDateTimeField;
    tblUpotrebeniTS_UPD: TFIBDateTimeField;
    tblUpotrebeniUSR_INS: TFIBStringField;
    tblUpotrebeniUSR_UPD: TFIBStringField;
    cxGrid1DBTableView1ID: TcxGridDBColumn;
    cxGrid1DBTableView1LOT_NO: TcxGridDBColumn;
    cxGrid1DBTableView1KOLICINA: TcxGridDBColumn;
    cxGrid1DBTableView1VAZI_DO: TcxGridDBColumn;
    cxGrid1DBTableView1CUSTOM1: TcxGridDBColumn;
    cxGrid1DBTableView1CUSTOM2: TcxGridDBColumn;
    cxGrid1DBTableView1CUSTOM3: TcxGridDBColumn;
    cxGrid1DBTableView1TS_INS: TcxGridDBColumn;
    cxGrid1DBTableView1TS_UPD: TcxGridDBColumn;
    cxGrid1DBTableView1USR_INS: TcxGridDBColumn;
    cxGrid1DBTableView1USR_UPD: TcxGridDBColumn;
    Label2: TLabel;
    Kolicina: TcxDBTextEdit;
    Label3: TLabel;
    DatumK: TcxDBDateEdit;
    tblUpotrebeniOUT_S_ID: TFIBBCDField;
    cxGrid1DBTableView1OUT_S_ID: TcxGridDBColumn;
    dxBarLargeButton10: TdxBarLargeButton;
    aOdberi: TAction;
    tblOutStavka: TpFIBDataSet;
    tblOutStavkaID: TFIBBCDField;
    tblOutStavkaMTR_OUT_ID: TFIBBCDField;
    tblOutStavkaART_VID: TFIBIntegerField;
    tblOutStavkaART_SIF: TFIBIntegerField;
    tblOutStavkaART_NAZIV: TFIBStringField;
    tblOutStavkaKOLICINA: TFIBFloatField;
    tblOutStavkaPAKETI: TFIBIntegerField;
    tblOutStavkaIZV_EM: TFIBIntegerField;
    tblOutStavkaIZVEDENA_EM: TFIBStringField;
    tblOutStavkaBR_PAKET: TFIBBCDField;
    tblOutStavkaCENA_SO_DDV: TFIBFloatField;
    tblOutStavkaCENA_BEZ_DDV: TFIBFloatField;
    tblOutStavkaDANOK_PR: TFIBBCDField;
    tblOutStavkaRABAT_PR: TFIBBCDField;
    tblOutStavkaKNIG_CENA: TFIBFloatField;
    tblOutStavkaKNIG_IZNOS: TFIBBCDField;
    tblOutStavkaIZNOS_BEZ_DDV: TFIBBCDField;
    tblOutStavkaIZNOS_SO_DDV: TFIBBCDField;
    tblOutStavkaRABAT_IZNOS: TFIBBCDField;
    tblOutStavkaDANOK_IZNOS: TFIBBCDField;
    tblOutStavkaLOT_NO: TFIBStringField;
    tblOutStavkaVAZI_DO: TFIBDateField;
    tblOutStavkaTS_INS: TFIBDateTimeField;
    tblOutStavkaTS_UPD: TFIBDateTimeField;
    tblOutStavkaUSR_INS: TFIBStringField;
    tblOutStavkaUSR_UPD: TFIBStringField;
    tblOutStavkaCUSTOM1: TFIBStringField;
    tblOutStavkaCUSTOM2: TFIBStringField;
    tblOutStavkaCUSTOM3: TFIBStringField;
    tblOutStavkaREF_ID: TFIBBCDField;
    tblOutStavkaOPIS: TFIBStringField;
    tblOutStavkaCALC_DANOK_IZNOS: TIBBCDField;
    tblOutStavkaART_MERKA: TFIBStringField;
    tblOutStavkaCENA_BEZ_RABAT_SO_DDV: TFIBFloatField;
    tblOutStavkaCENA_BEZ_RABAT_BEZ_DDV: TFIBFloatField;
    tblOutStavkaLOT_ID: TFIBBCDField;
    tblOutStavkaSLEDLIVOST_KOLICINA: TFIBSmallIntField;
    dsOutStavka: TDataSource;

    procedure cxDBTextEditAllEnter(Sender: TObject);
    procedure cxDBTextEditAllExit(Sender: TObject);
    procedure EnterKakoTab(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    //procedure PrvPosledenTab(panel:TPanel;var posledna,prva:TWinControl);
    procedure SaveToIniFileExecute(Sender: TObject);
    procedure cxGrid1DBTableView1KeyPress(Sender: TObject; var Key: Char);
    procedure aZapisiExecute(Sender: TObject);
    procedure aOtkaziExecute(Sender: TObject);
    procedure aNovExecute(Sender: TObject);
    procedure aAzurirajExecute(Sender: TObject);
    procedure aBrisiExecute(Sender: TObject);
    procedure aZacuvajExcelExecute(Sender: TObject);
    procedure aRefreshExecute(Sender: TObject);
    procedure aSnimiIzgledExecute(Sender: TObject);
    procedure aIzlezExecute(Sender: TObject);
    procedure aPecatiTabelaExecute(Sender: TObject);
    procedure aSnimiPecatenjeExecute(Sender: TObject);
    procedure zacuvajPrintVoIni(ime:AnsiString);
    procedure procitajPrintOdIni(ime:AnsiString);
    procedure aPodesuvanjePecatenjeExecute(Sender: TObject);
    procedure aBrisiPodesuvanjePecatenjeExecute(Sender: TObject);
    procedure aPageSetupExecute(Sender: TObject);
    procedure cxGrid1DBTableView1FilterCustomization(
      Sender: TcxCustomGridTableView; var ADone: Boolean);
    procedure aFormConfigExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure cxBarEditItem1PropertiesChange(Sender: TObject);
    procedure aBrisiIzgledExecute(Sender: TObject);
    procedure aSpustiSoberiExecute(Sender: TObject);
    procedure tblUpotrebeniBeforeOpen(DataSet: TDataSet);
    procedure tblUpotrebeniAfterInsert(DataSet: TDataSet);
    procedure aOdberiExecute(Sender: TObject);
    procedure TabelaBeforeDelete(DataSet: TDataSet);
    procedure SifraEnter(Sender: TObject);
    procedure SifraExit(Sender: TObject);
  private
    { Private declarations }

    _sifra_kluc :array[1..10] of string;  // �������� ���������� �� ������ �� ������� �� ������������� ���

  protected
    list : TList;
    inserting, sobrano : boolean;
    prva, posledna :TWinControl;
    procedure prefrli;

  public
    { Public declarations }
    evidencija_id : Int64;
    evidencija_artvid, evidencija_artsif, evidencija_sledvlivost : integer;
    evidencija_kolicina : double;

    constructor Create(Owner : TComponent; insert : boolean = true);reintroduce; overload;

    procedure SetSifra(br: integer; s : string);
    function GetSifra(br: integer) : string;

    //������� �� ���������� ������������ _sifra_kluc
    property sifra_kluc[br :integer]: string read GetSifra write SetSifra;
  end;

var
  frmOutLots: TfrmOutLots;

implementation

uses DaNe, dmKonekcija, Utils, FormConfig, dmUnit, EvidencijaSerijaKolicini,
  dmMaticni;

{$R *.dfm}
//------------------------------------------------------------------------------

constructor TfrmOutLots.Create(Owner : TComponent; insert : boolean = true);
begin
inherited Create(Owner);
//	�������� ���� ������� � �������� �� ����������� ���������
//	insert = true -> ����� ���� ����� ���������� �� ������� �� Insert Mode
//  insert = false -> ����� ���� ����� ������� �� ����� �� ������. �� ������� ����� ����� �� �� �������� F5
  inserting := insert;
  evidencija_id := 0;
end;

//	����� �� �����������. �� ��������� ������� �� ������, �� �������� ������� �� ���� ��������,
//	�� ������� ������� �� ������ ������ � �� ������� �� Insert Mode
procedure TfrmOutLots.aNovExecute(Sender: TObject);
begin
  if(cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) then
  begin
    dPanel.Enabled:=True;
    lPanel.Enabled:=False;
    prva.SetFocus;
    cxGrid1DBTableView1.DataController.DataSet.Insert;
  end
  else ShowMessage('������ �������� �� ��� �������� �� �� ��������� �������,' + sLineBreak + '� ����� ������� �� ������� ��� �����!');
end;

//	����� �� ���������. �� ��������� ������� �� ������, �� �������� ������� �� ���� ��������,
//	�� ������� ������� �� ������ ������ � �� ������� �� Edit Mode
procedure TfrmOutLots.aAzurirajExecute(Sender: TObject);
begin
  if(cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) then
  begin
    dPanel.Enabled:=True;
    lPanel.Enabled:=False;
    prva.SetFocus;
    cxGrid1DBTableView1.DataController.DataSet.Edit;
  end
  else ShowMessage('������ �������� �� ��� �������� �� �� ��������� �������,' + sLineBreak + '� ����� ������� �� ���������!');
end;

//	����� �� ������ �� ������������� �����
procedure TfrmOutLots.aBrisiExecute(Sender: TObject);
begin
  if ((cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) and
     (cxGrid1DBTableView1.DataController.RecordCount <> 0)) then
    cxGrid1DBTableView1.DataController.DataSet.Delete();
end;

procedure TfrmOutLots.aBrisiIzgledExecute(Sender: TObject);
begin
  brisiGridVoBaza(Name,cxGrid1DBTableView1);
//  brisiPivotVoBaza(Name,cxDBPivotGrid1); �� pivot ��� ���
  BrisiFormaIzgled(self);
end;

//	����� �� ���������� �� ����������
procedure TfrmOutLots.aRefreshExecute(Sender: TObject);
begin
  cxGrid1DBTableView1.DataController.Filter.Root.Clear; //���������� �� ��������
  cxGrid1DBTableView1.DataController.DataSet.Refresh;
end;

//	����� �� ����� �� ������� �� ������� �� ������� �����
procedure TfrmOutLots.aIzlezExecute(Sender: TObject);
begin
  Close;
end;

//	����� �� ������� �� �������� �� ������ �� ���� (Utils.pas)
procedure TfrmOutLots.aSnimiIzgledExecute(Sender: TObject);
begin
  zacuvajGridVoBaza(Name,cxGrid1DBTableView1);
  ZacuvajFormaIzgled(self);
end;

//	����� �� ������� �� ������ �� Excel ������ (Utils.pas)
procedure TfrmOutLots.aZacuvajExcelExecute(Sender: TObject);
begin
  zacuvajVoExcel(cxGrid1, Caption);
end;

//  ��������� �� ������ �� ������� �� ���������� �� ����� �� Enter
procedure TfrmOutLots.EnterKakoTab(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
    case Key of
        VK_DOWN:
        begin
          PostMessage(Handle,WM_NextDlgCtl,0,0);
          key:=0;
        end;
        VK_UP:
        begin
          PostMessage(Handle,WM_NextDlgCtl,1,0);
          key:=0;
        end;
        VK_RETURN:
        begin
          PostMessage(Handle,WM_NextDlgCtl,0,0);
        end;
    end;
end;

//	������� �� ��� �� ������ ��� �������� �� �����
procedure TfrmOutLots.cxBarEditItem1PropertiesChange(Sender: TObject);
begin
  dmRes.SkinPromeni(Sender);
  dxRibbon1.ColorSchemeName := dmRes.skin_name;
end;

procedure TfrmOutLots.cxDBTextEditAllEnter(Sender: TObject);
begin
    TEdit(Sender).Color:=clSkyBlue;
end;

//	������� �� ��� �� ������ ��� ����� �� �����
procedure TfrmOutLots.cxDBTextEditAllExit(Sender: TObject);
begin
    TEdit(Sender).Color:=clWhite;
end;

procedure TfrmOutLots.cxGrid1DBTableView1FilterCustomization(
  Sender: TcxCustomGridTableView; var ADone: Boolean);
begin
end;

//  ��������� �� ������ �� ��������� �� ���������� ���� �� ��������
//  ��������� ��: br - ����� ��� �� ������ (�� 0 �� 9) �� ��������� �� ����� �� ����� �� ���������� ����
//  s - ��������� �� ������ �� ������ (���������� �� string �� ����� �� integer �� IntToStr ���������)
procedure TfrmOutLots.SetSifra(br: integer; s : string);
begin
  _sifra_kluc[br] := s;
end;

procedure TfrmOutLots.SifraEnter(Sender: TObject);
begin
  ActivateKeyboardLayout($04090409, KLF_REORDER);
end;

procedure TfrmOutLots.SifraExit(Sender: TObject);
begin
  ActivateKeyboardLayout($042F042F, KLF_REORDER);
end;

procedure TfrmOutLots.tblUpotrebeniAfterInsert(DataSet: TDataSet);
begin
  if (evidencija_id = 0) then
    tblUpotrebeniOut_S_ID.Value := tblOutStavkaID.Value
  else
    tblUpotrebeniOut_S_ID.Value := evidencija_id;
end;

procedure TfrmOutLots.tblUpotrebeniBeforeOpen(DataSet: TDataSet);
begin
  if (evidencija_id = 0) then
    tblUpotrebeni.ParamByName('IN_ID').Value := tblOutStavkaID.Value
  else
    tblUpotrebeni.ParamByName('IN_ID').Value := evidencija_id;
end;

//  ������� �� ������ �� ��������� �� ���������� ���� �� ��������
//  ��������� ��: br - ����� ��� �� ������ (�� 0 �� 9) �� ��������� �� ����� �� ����� �� ���������� ����
function TfrmOutLots.GetSifra(br: integer) : string;
begin
  Result := _sifra_kluc[br];
end;

//  �� ������� �� ������� ��� ����������� �� Master-�� �� �������� �� ��������� �� ������ ����� SetSifra
procedure TfrmOutLots.prefrli;
begin
end;

procedure TfrmOutLots.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    //	��������� ��� ��������� �� �������
    if (cxGrid1DBTableView1.DataController.DataSource.State = dsEdit) or (cxGrid1DBTableView1.DataController.DataSource.State = dsInsert) then
    begin
        frmDaNe := TfrmDaNe.Create(self, '���������� ��������', '���������� �� �� ��������. ���� ������ �� �� �������?', 1);
        if (frmDaNe.ShowModal <> mrYes) then
        begin
            cxGrid1DBTableView1.DataController.DataSet.Cancel;
            Action := caFree;
        end
        else
          if (Validacija(dPanel) = false) then
          begin
            cxGrid1DBTableView1.DataController.DataSet.Post;
            Action := caFree;
          end
          else Action := caNone;
    end;
end;

procedure TfrmOutLots.FormCreate(Sender: TObject);
begin
  ProcitajFormaIzgled(self);
  if (dmRes.use_skin = true) then dxRibbon1.ColorSchemeName := dmRes.skin_name;
end;

//------------------------------------------------------------------------------

procedure TfrmOutLots.FormShow(Sender: TObject);
begin
// ������� �� ������������ ������������/���������� �� ���������� �� ���� �����
    SpremiForma(self);
//	�� ��������� �� ������� ������ ��� ���������� � ���� � ��� �������� �� �������
    PrvPosledenTab(dPanel,posledna,prva);
//    dxBarManager1Bar1.Caption := Caption;
    dxComponentPrinter1Link1.ReportTitle.Text := Caption;

//	������� �� ������������ �� ������ �� ������
    procitajGridOdBaza(Name,cxGrid1DBTableView1,false,false);
//	������� �� ������������ �� ��������� �� ������
    procitajPrintOdBaza(Name,cxGrid1DBTableView1.Name, dxComponentPrinter1Link1);

    sobrano := true;

    tblUpotrebeni.Close;
    tblUpotrebeni.Open;

    if ((evidencija_id = 0) and (tblOutStavkaSLEDLIVOST_KOLICINA.Value = 1))  then
      aOdberi.Enabled := false
    else
    if ((evidencija_id <> 0) and (evidencija_sledvlivost = 1))  then
      aOdberi.Enabled := false;

end;
//------------------------------------------------------------------------------

procedure TfrmOutLots.SaveToIniFileExecute(Sender: TObject);
begin
    cxGrid1DBTableView1.StoreToIniFile(Name,True,[]);
end;
//------------------------------------------------------------------------------

procedure TfrmOutLots.cxGrid1DBTableView1KeyPress(Sender: TObject;
  var Key: Char);
begin
  if(Ord(Key) <> VK_RETURN) then cxSortiraj(cxGrid1DBTableView1);
end;


//  ����� �� �����
procedure TfrmOutLots.aZapisiExecute(Sender: TObject);
var
  st: TDataSetState;
begin
  if(cxGrid1DBTableView1.DataController.DataSource.State <> dsBrowse) then
  begin
    ZapisiButton.SetFocus;

    st := cxGrid1DBTableView1.DataController.DataSet.State;
    if st in [dsEdit,dsInsert] then
    begin
      if (Validacija(dPanel) = false) then
      begin
        if ((st = dsInsert) and inserting) then
        begin
          cxGrid1DBTableView1.DataController.DataSet.Post;
          aNov.Execute;
        end;

        if ((st = dsInsert) and (not inserting)) then
        begin
          cxGrid1DBTableView1.DataController.DataSet.Post;
          dPanel.Enabled:=false;
          lPanel.Enabled:=true;
          cxGrid1.SetFocus;
        end;

        if (st = dsEdit) then
        begin
          cxGrid1DBTableView1.DataController.DataSet.Post;
          dPanel.Enabled:=false;
          lPanel.Enabled:=true;
          cxGrid1.SetFocus;
        end;
      end;
    end;
  end;
end;

procedure TfrmOutLots.TabelaBeforeDelete(DataSet: TDataSet);
begin
    frmDaNe := TfrmDaNe.Create(self, '������ �� �����', '���� ��������� ������ �� �� ��������� �������?', 1);
    if (frmDaNe.ShowModal <> mrYes) then
        Abort;
end;

//	����� �� ���������� �� �������
procedure TfrmOutLots.aOdberiExecute(Sender: TObject);
var
  prodolzi : boolean;
  br_na_serii : integer;
begin
  frmEvidencijaSerijaKolicini := TfrmEvidencijaSerijaKolicini.Create(self, false);
  if (evidencija_id = 0) then
    br_na_serii := frmEvidencijaSerijaKolicini.initParams(dmMat.tblReMagacinID.Value, tblOutStavkaART_VID.Value, tblOutStavkaART_SIF.Value, tblOutStavkaKOLICINA.Value)
  else
    br_na_serii := frmEvidencijaSerijaKolicini.initParams(dmMat.tblReMagacinID.Value, evidencija_artvid, evidencija_artsif, evidencija_kolicina);
  if (br_na_serii = 0) then
  begin
    prodolzi := false;
    ShowMessage('������! �� ��������� ������� ���� ������ �� ���������!');
    frmEvidencijaSerijaKolicini.Free;
  end;

  if (br_na_serii = 1) then
  begin
    frmEvidencijaSerijaKolicini.aZapisi.Execute;
    prodolzi := frmEvidencijaSerijaKolicini.uspesno;
    if (prodolzi = false) then
    begin
      frmEvidencijaSerijaKolicini.Free;
    end;
  end;

  if (br_na_serii > 1) then
  begin
    frmEvidencijaSerijaKolicini.ShowModal;
    prodolzi := frmEvidencijaSerijaKolicini.uspesno;
    if (prodolzi = false) then
    begin
      frmEvidencijaSerijaKolicini.Free;
    end;
  end;

  if (prodolzi = true) then
  begin
    if (evidencija_id = 0) then
      frmEvidencijaSerijaKolicini.GenerirajInOut(tblOutStavkaID.Value)
    else
      frmEvidencijaSerijaKolicini.GenerirajInOut(evidencija_id);

    //tblOutStavka.FullRefresh;
    frmEvidencijaSerijaKolicini.Free;
  end;

  tblUpotrebeni.FullRefresh;

end;

procedure TfrmOutLots.aOtkaziExecute(Sender: TObject);
begin
  if (cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) then
  begin
      ModalResult := mrCancel;
      Close();
  end
  else
  begin
      cxGrid1DBTableView1.DataController.DataSet.Cancel;
      RestoreControls(dPanel);
      dPanel.Enabled := false;
      lPanel.Enabled := true;
      cxGrid1.SetFocus;
  end;
end;

//----------------------------------------------------------------------------------
// ��������� �� ��������, ���������� �� ��������� � �������� �� Print ��������
//----------------------------------------------------------------------------------

//	����� �� �������� �� �������� �� �������
procedure TfrmOutLots.aPageSetupExecute(Sender: TObject);
begin
  dxComponentPrinter1Link1.PageSetup;
end;

//	����� �� ������� �� ������
procedure TfrmOutLots.aPecatiTabelaExecute(Sender: TObject);
begin
  dxComponentPrinter1Link1.ReportTitle.Text := Caption;

  dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Clear;
  dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Add(dmkon.firma_naziv);
  dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Add(DateTimeToStr(Now));

  dxComponentPrinter1Link1.PrinterPage.PageHeader.LeftTitle.Clear;
  //dxComponentPrinter1Link1.PrinterPage.PageHeader.LeftTitle.Add('������� : ' + TipSifra.Text + '-' + IdSifra.Text + ' ' + Partner.Text);

  dxComponentPrinter1.Preview(true, dxComponentPrinter1Link1);
end;

//	����� �� ���������� �� ��������� �� �������
procedure TfrmOutLots.aPodesuvanjePecatenjeExecute(Sender: TObject);
begin
  dxComponentPrinter1Link1.DesignReport();
end;

procedure TfrmOutLots.aSnimiPecatenjeExecute(Sender: TObject);
begin
  zacuvajPrintVoBaza(Name,cxGrid1DBTableView1.Name,dxComponentPrinter1Link1);
end;

//	����� �� �������� ��� �������� �� �������� �� ������ ���� ��� ��������� �� ����� ������
procedure TfrmOutLots.aSpustiSoberiExecute(Sender: TObject);
begin
  if (sobrano = true) then
  begin
    cxGrid1DBTableView1.ViewData.Expand(false);
    sobrano := false;
  end
  else
  begin
    cxGrid1DBTableView1.ViewData.Collapse(false);
    sobrano := true;
  end;
end;

//	����� �� ������ �� ����������� �������� �� ������� (Utils.pas)
procedure TfrmOutLots.aBrisiPodesuvanjePecatenjeExecute(Sender: TObject);
begin
  brisiPrintOdBaza(Name,cxGrid1DBTableView1.Name, dxComponentPrinter1Link1);
end;

//  ����� �� ����� �� ����� �� ������������� �� �������� � ��������������� ��
//  �������(������ ���������� ��������)
procedure TfrmOutLots.aFormConfigExecute(Sender: TObject);
begin
  frmFormConfig := TfrmFormConfig.Create(Application);
  frmFormConfig.formPtr := Addr(Self);
  frmFormConfig.ShowModal;
  frmFormConfig.Free;
end;

//----------------------------------------------------------------------------------
// ���� -> ��������� �� ��������, ���������� �� ��������� � �������� �� Print ��������
//----------------------------------------------------------------------------------

// ��������� �� ���������� �� ��������� � �������� �� Print �������� �� .ini ��������
procedure TfrmOutLots.zacuvajPrintVoIni(ime:AnsiString);
var
  AStream: TFileStream;
begin
  AStream := TFileStream.Create(dmKon.RabotenDir + ime + 'Print.ini', fmCreate);
  try
    AStream.WriteComponent(dxComponentPrinter1Link1);
  finally
    AStream.Free;
  end;

end;

// ��������� �� ������ �� ��������� � �������� �� Print �������� �������� �� .ini ��������
procedure TfrmOutLots.procitajPrintOdIni(ime:AnsiString);
var
  AStream: TFileStream;
begin
  AStream := TFileStream.Create(dmKon.RabotenDir + ime + 'Print.ini', fmOpenRead);
  try
    dxComponentPrinter1Link1.RestoreDefaults;
    AStream.ReadComponent(dxComponentPrinter1Link1);
  finally
    AStream.Free;
  end;
end;

end.
