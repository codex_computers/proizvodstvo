unit UpDownStream;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData, cxEdit,
  DB, cxDBData, cxGridCustomPopupMenu, cxGridPopupMenu,
  ComCtrls, cxContainer, cxTextEdit, cxDBEdit, StdCtrls, cxGridLevel,
  cxClasses, cxControls, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGrid, ExtCtrls, Menus, ActnList,
  cxGridExportLink, cxExport, cxLookAndFeelPainters, cxDataStorage,
  cxButtons, dxStatusBar, dxRibbonStatusBar, dxRibbon, dxBar, dxBarExtItems,
  dxPSGlbl, dxPSUtl, dxPSEngn, dxPrnPg, dxBkgnd, dxWrap, dxPrnDev,
  dxPSCompsProvider, dxPSFillPatterns, dxPSEdgePatterns, dxPSCore, dxPScxCommon,
  cxPC, cxMemo, cxLocalization, cxLookAndFeels,
  dxPSPDFExportCore, dxPSPDFExport, cxDrawTextUtils, dxPSPrVwStd, dxPSPrVwAdv,
  dxPSPrVwRibbon, dxPScxEditorProducers, dxPScxExtEditorProducers,
  dxPScxPageControlProducer, dxPgsDlg, cxCheckBox, cxBarEditItem, dxSkinsCore,
  dxSkinBlack, dxSkinBlue, dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom,
  dxSkinDarkSide, dxSkinFoggy, dxSkinGlassOceans, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven,
  dxSkinSharp, dxSkinSilver, dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008,
  dxSkinsDefaultPainters, dxSkinValentine, dxSkinXmas2008Blue,
  dxSkinsdxRibbonPainter, dxSkinsdxBarPainter, dxSkinscxPCPainter,
  dxBarSkinnedCustForm, cxDropDownEdit, cxCalendar, cxDBLookupComboBox,
  cxMaskEdit, cxLookupEdit, cxDBLookupEdit, cxDBExtLookupComboBox, cxGroupBox,
  cxRadioGroup, dxRibbonSkins, dxPScxGridLnk, dxPScxGridLayoutViewLnk,
  dxScreenTip, dxSkinBlueprint, dxSkinDevExpressDarkStyle,
  dxSkinDevExpressStyle, dxSkinHighContrast, dxSkinSevenClassic,
  dxSkinSharpPlus, dxSkinTheAsphaltWorld, dxSkinVS2010, dxSkinWhiteprint, dmResources,
  dxSkinsdxStatusBarPainter, cxPCdxBarPopupMenu, cxPropertiesStore, FIBDataSet,
  pFIBDataSet, cxLabel;

type
//  niza = Array[1..5] of Variant;

  TfrmUpDownStream = class(TForm)
    cxGridPopupMenu1: TcxGridPopupMenu;
    PopupMenu1: TPopupMenu;
    N1: TMenuItem;
    Excel1: TMenuItem;
    dxRibbon1Tab1: TdxRibbonTab;
    dxRibbon1: TdxRibbon;
    StatusBar1: TdxRibbonStatusBar;
    dxBarManager1: TdxBarManager;
    dxBarManager1Bar2: TdxBar;
    dxBarManager1Bar3: TdxBar;
    dxBarLargeButton2: TdxBarLargeButton;
    dxBarLargeButton3: TdxBarLargeButton;
    dxBarLargeButton4: TdxBarLargeButton;
    dxBarLargeButton5: TdxBarLargeButton;
    dxBarLargeButton6: TdxBarLargeButton;
    dxBarLargeButton7: TdxBarLargeButton;
    dxBarLargeButton8: TdxBarLargeButton;
    dxBarLargeButton1: TdxBarLargeButton;
    ActionList1: TActionList;
    aNov: TAction;
    aAzuriraj: TAction;
    aBrisi: TAction;
    aSnimiIzgled: TAction;
    aZacuvajExcel: TAction;
    aPecatiTabela: TAction;
    aHelp: TAction;
    aIzlez: TAction;
    aZapisi: TAction;
    aOtkazi: TAction;
    aRefresh: TAction;
    dxRibbon1Tab2: TdxRibbonTab;
    dxBarManager1Bar4: TdxBar;
    dxBarLargeButton9: TdxBarLargeButton;
    aSnimiPecatenje: TAction;
    dxComponentPrinter1: TdxComponentPrinter;
    dxComponentPrinter1Link1: TdxGridReportLink;
    dxBarLargeButton11: TdxBarLargeButton;
    ribbonFilterReset: TdxBarLargeButton;
    aPodesuvanjePecatenje: TAction;
    dxBarLargeButton12: TdxBarLargeButton;
    aBrisiPodesuvanjePecatenje: TAction;
    dxBarLargeButton13: TdxBarLargeButton;
    dxBarLargeButton14: TdxBarLargeButton;
    aPageSetup: TAction;
    dxBarLargeButton15: TdxBarLargeButton;
    dxBarLargeButton16: TdxBarLargeButton;
    dxBarLargeButton17: TdxBarLargeButton;
    cxBarEditItem1: TcxBarEditItem;
    dxBarSubItem1: TdxBarSubItem;
    aFormConfig: TAction;
    dxBarScreenTipRepository1: TdxBarScreenTipRepository;
    tipSnimiIzgled: TdxBarScreenTip;
    tipZacuvajExcel: TdxBarScreenTip;
    tipDizajnReport: TdxBarScreenTip;
    tipSetiranjeStrana: TdxBarScreenTip;
    tipSnimiKonf: TdxBarScreenTip;
    tipBrisiKonf: TdxBarScreenTip;
    dxBarManager1BarIzgledGrid: TdxBar;
    dxBarLBtnSnimiIzgled: TdxBarLargeButton;
    dxBarLBtnBrisiIzgled: TdxBarLargeButton;
    aBrisiIzgled: TAction;
    aSpustiSoberi: TAction;
    dxBarLargeButton10: TdxBarLargeButton;
    cxPropertiesStore1: TcxPropertiesStore;
    Panel1: TPanel;
    tblLotProizvod: TpFIBDataSet;
    dsLotProizvod: TDataSource;
    tblLotProizvodVID_PROIZVOD: TFIBIntegerField;
    tblLotProizvodPROIZVOD: TFIBIntegerField;
    tblLotProizvodNAZIV_PROIZVOD: TFIBStringField;
    tblLotProizvodLOT_PROIZVOD: TFIBStringField;
    aUp: TAction;
    btUp: TdxBarLargeButton;
    aUpDizajner: TAction;
    cxPageControl1: TcxPageControl;
    tsProizvod: TcxTabSheet;
    Panel3: TPanel;
    ts1Panel: TPanel;
    cxLabel1: TcxLabel;
    cbUp: TcxLookupComboBox;
    cxLabel8: TcxLabel;
    txtVidA: TcxTextEdit;
    txtA: TcxTextEdit;
    cbA: TcxTextEdit;
    cxButton1: TcxButton;
    tsSurovina: TcxTabSheet;
    Panel5: TPanel;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1DBTableView1VID_PROIZVOD: TcxGridDBColumn;
    cxGrid1DBTableView1PROIZVOD: TcxGridDBColumn;
    cxGrid1DBTableView1MERKA_KOLICINA: TcxGridDBColumn;
    cxGrid1DBTableView1DATUM_RN: TcxGridDBColumn;
    cxGrid1DBTableView1LOT_PROIZVOD: TcxGridDBColumn;
    cxGrid1DBTableView1NAZIV_PROIZVOD: TcxGridDBColumn;
    cxGrid1DBTableView1ART_VID: TcxGridDBColumn;
    cxGrid1DBTableView1ART_SIF: TcxGridDBColumn;
    cxGrid1DBTableView1NAZIV_SUROVINA: TcxGridDBColumn;
    cxGrid1DBTableView1PAKETI: TcxGridDBColumn;
    cxGrid1DBTableView1IZV_EM: TcxGridDBColumn;
    cxGrid1DBTableView1BR_PAKET: TcxGridDBColumn;
    cxGrid1DBTableView1LOT_NO: TcxGridDBColumn;
    cxGrid1DBTableView1MERKA: TcxGridDBColumn;
    cxGrid1Level1: TcxGridLevel;
    Panel2: TPanel;
    cxLabel2: TcxLabel;
    cbDown: TcxLookupComboBox;
    cxLabel3: TcxLabel;
    txtVS: TcxTextEdit;
    txtS: TcxTextEdit;
    cbSurovina: TcxTextEdit;
    cxButton2: TcxButton;
    cxGrid2: TcxGrid;
    cxGridDBTableView1: TcxGridDBTableView;
    cxGridLevel1: TcxGridLevel;
    tblLotSurovina: TpFIBDataSet;
    dsLotSurovina: TDataSource;
    tblLotSurovinaVID_SUROVINA: TFIBIntegerField;
    tblLotSurovinaSUROVINA: TFIBIntegerField;
    tblLotSurovinaNAZIV_SUROVINA: TFIBStringField;
    tblLotSurovinaLOT_SUROVINA: TFIBStringField;
    cxGridDBTableView1VID_PROIZVOD: TcxGridDBColumn;
    cxGridDBTableView1PROIZVOD: TcxGridDBColumn;
    cxGridDBTableView1NAZIV_PROIZVOD: TcxGridDBColumn;
    cxGridDBTableView1MERKA_KOLICINA: TcxGridDBColumn;
    cxGridDBTableView1DATUM_RN: TcxGridDBColumn;
    cxGridDBTableView1LOT_PROIZVOD: TcxGridDBColumn;
    cxGridDBTableView1ART_VID: TcxGridDBColumn;
    cxGridDBTableView1ART_SIF: TcxGridDBColumn;
    cxGridDBTableView1NAZIV_SUROVINA: TcxGridDBColumn;
    cxGridDBTableView1MERKA: TcxGridDBColumn;
    cxGridDBTableView1LOT_NO: TcxGridDBColumn;
    btDown: TdxBarLargeButton;
    dxComponentPrinter2: TdxComponentPrinter;
    dxComponentPrinter2Link1: TdxGridReportLink;
    cxGrid1DBTableView1KOLICINA_PROIZVOD: TcxGridDBColumn;
    cxGrid1DBTableView1KOLICINA_SUROVINA: TcxGridDBColumn;
    cxGridDBTableView1KOLICINA_PROIZVOD: TcxGridDBColumn;
    cxGridDBTableView1KOLICINA_SUROVINA: TcxGridDBColumn;

    procedure cxDBTextEditAllEnter(Sender: TObject);
    procedure cxDBTextEditAllExit(Sender: TObject);
    procedure EnterKakoTab(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    //procedure PrvPosledenTab(panel:TPanel;var posledna,prva:TWinControl);
    procedure SaveToIniFileExecute(Sender: TObject);
    procedure cxGrid1DBTableView1KeyPress(Sender: TObject; var Key: Char);
    procedure aZapisiExecute(Sender: TObject);
    procedure aOtkaziExecute(Sender: TObject);
    procedure aNovExecute(Sender: TObject);
    procedure aAzurirajExecute(Sender: TObject);
    procedure aBrisiExecute(Sender: TObject);
    procedure aZacuvajExcelExecute(Sender: TObject);
    procedure aRefreshExecute(Sender: TObject);
    procedure aSnimiIzgledExecute(Sender: TObject);
    procedure aIzlezExecute(Sender: TObject);
    procedure aPecatiTabelaExecute(Sender: TObject);
    procedure aSnimiPecatenjeExecute(Sender: TObject);
    procedure zacuvajPrintVoIni(ime:AnsiString);
    procedure procitajPrintOdIni(ime:AnsiString);
    procedure aPodesuvanjePecatenjeExecute(Sender: TObject);
    procedure aBrisiPodesuvanjePecatenjeExecute(Sender: TObject);
    procedure aPageSetupExecute(Sender: TObject);
    procedure cxGrid1DBTableView1FilterCustomization(
      Sender: TcxCustomGridTableView; var ADone: Boolean);
    procedure aFormConfigExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure PartnerPropertiesChange(Sender: TObject);
    procedure aBrisiIzgledExecute(Sender: TObject);
    procedure aSpustiSoberiExecute(Sender: TObject);
    procedure cxButton2Click(Sender: TObject);
    procedure aUpExecute(Sender: TObject);
    procedure aUpDizajnerExecute(Sender: TObject);
    procedure cxButton1Click(Sender: TObject);
    procedure aUpDownExecute(Sender: TObject);
    procedure cxGrid1DBTableView1TcxGridDBDataControllerTcxDataSummaryFooterSummaryItems0GetText(
      Sender: TcxDataSummaryItem; const AValue: Variant; AIsFooter: Boolean;
      var AText: string);
    procedure cxGrid1DBTableView1TcxGridDBDataControllerTcxDataSummarySummaryGroups0SummaryItems0GetText(
      Sender: TcxDataSummaryItem; const AValue: Variant; AIsFooter: Boolean;
      var AText: string);
    procedure cxGridDBTableView1TcxGridDBDataControllerTcxDataSummarySummaryGroups0SummaryItems0GetText(
      Sender: TcxDataSummaryItem; const AValue: Variant; AIsFooter: Boolean;
      var AText: string);
    procedure cxGridDBTableView1TcxGridDBDataControllerTcxDataSummaryFooterSummaryItems0GetText(
      Sender: TcxDataSummaryItem; const AValue: Variant; AIsFooter: Boolean;
      var AText: string);
  private
    { Private declarations }

    _sifra_kluc :array[1..10] of string;  // �������� ���������� �� ������ �� ������� �� ������������� ���

  protected
    list : TList;
    inserting, sobrano : boolean;
    prva, posledna :TWinControl;
    procedure prefrli;

  public
    { Public declarations }
    constructor Create(Owner : TComponent; insert : boolean = true);reintroduce; overload;

    procedure SetSifra(br: integer; s : string);
    function GetSifra(br: integer) : string;

    //������� �� ���������� ������������ _sifra_kluc
    property sifra_kluc[br :integer]: string read GetSifra write SetSifra;
  end;

var
  frmUpDownStream: TfrmUpDownStream;
  rData : TRepositoryData;

implementation

uses DaNe, dmKonekcija, Utils, FormConfig, NurkoRepository, dmMaticni, dmSystem,
  dmUnit;

{$R *.dfm}
//------------------------------------------------------------------------------

constructor TfrmUpDownStream.Create(Owner : TComponent; insert : boolean = true);
begin
inherited Create(Owner);
//	�������� ���� ������� � �������� �� ����������� ���������
//	insert = true -> ����� ���� ����� ���������� �� ������� �� Insert Mode
//  insert = false -> ����� ���� ����� ������� �� ����� �� ������. �� ������� ����� ����� �� �� �������� F5
  inserting := insert;
end;

//	����� �� �����������. �� ��������� ������� �� ������, �� �������� ������� �� ���� ��������,
//	�� ������� ������� �� ������ ������ � �� ������� �� Insert Mode
procedure TfrmUpDownStream.aNovExecute(Sender: TObject);
begin
//  if(cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) then
//  begin
//    dPanel.Enabled:=True;
//    lPanel.Enabled:=False;
//    prva.SetFocus;
//    cxGrid1DBTableView1.DataController.DataSet.Insert;
//  end
//  else ShowMessage('������ �������� �� ��� �������� �� �� ��������� �������,' + sLineBreak + '� ����� ������� �� ������� ��� �����!');
end;

//	����� �� ���������. �� ��������� ������� �� ������, �� �������� ������� �� ���� ��������,
//	�� ������� ������� �� ������ ������ � �� ������� �� Edit Mode
procedure TfrmUpDownStream.aAzurirajExecute(Sender: TObject);
begin
//  if(cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) then
//  begin
//    dPanel.Enabled:=True;
//    lPanel.Enabled:=False;
//    prva.SetFocus;
//    cxGrid1DBTableView1.DataController.DataSet.Edit;
//  end
//  else ShowMessage('������ �������� �� ��� �������� �� �� ��������� �������,' + sLineBreak + '� ����� ������� �� ������� ��� �����!');
end;

//	����� �� ������ �� ������������� �����
procedure TfrmUpDownStream.aBrisiExecute(Sender: TObject);
begin
//  if ((cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) and
//     (cxGrid1DBTableView1.DataController.RecordCount <> 0)) then
//    cxGrid1DBTableView1.DataController.DataSet.Delete();
end;

procedure TfrmUpDownStream.aBrisiIzgledExecute(Sender: TObject);
begin
  brisiGridVoBaza(Name,cxGrid1DBTableView1);
//  brisiPivotVoBaza(Name,cxDBPivotGrid1); �� pivot ��� ���
  BrisiFormaIzgled(self);
end;

//	����� �� ���������� �� ����������
procedure TfrmUpDownStream.aRefreshExecute(Sender: TObject);
begin
  cxGrid1DBTableView1.DataController.Filter.Root.Clear; //���������� �� ��������
  cxGrid1DBTableView1.DataController.DataSet.Refresh;
end;

//	����� �� ����� �� ������� �� ������� �� ������� �����
procedure TfrmUpDownStream.aIzlezExecute(Sender: TObject);
begin
  Close;
end;

//	����� �� ������� �� �������� �� ������ �� ���� (Utils.pas)
procedure TfrmUpDownStream.aSnimiIzgledExecute(Sender: TObject);
begin
  zacuvajGridVoBaza(Name,cxGrid1DBTableView1);
  ZacuvajFormaIzgled(self);
end;

//	����� �� ������� �� ������ �� Excel ������ (Utils.pas)
procedure TfrmUpDownStream.aZacuvajExcelExecute(Sender: TObject);
begin
  zacuvajVoExcel(cxGrid1, Caption);
end;

//  ��������� �� ������ �� ������� �� ���������� �� ����� �� Enter
procedure TfrmUpDownStream.EnterKakoTab(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  kom : TWinControl;
begin
    kom := Sender as TWinControl;
    case Key of
        VK_DOWN:
        begin
          PostMessage(Handle,WM_NextDlgCtl,0,0);
          key:=0;
        end;
        VK_UP:
        begin
          PostMessage(Handle,WM_NextDlgCtl,1,0);
          key:=0;
        end;
        VK_RETURN:
        begin
          PostMessage(Handle,WM_NextDlgCtl,0,0);
        end;
        VK_INSERT:
        begin
          //  �� ����� ��������
//          if (kom = cxExtLookupComboBox1)  then
//          begin
//            frmNurkoRepository := TfrmNurkoRepository.Create(nil,Name,rData);
//            frmNurkoRepository.kontrola_naziv := kom.Name;
//            frmNurkoRepository.ShowModal;
//
//            if (frmNurkoRepository.ModalResult = mrOk) then
//              cxExtLookupComboBox1.EditValue := frmNurkoRepository.cxGrid1Level1.GridView.DataController.GetRecordId(frmNurkoRepository.cxGrid1Level1.GridView.DataController.FocusedRecordIndex);
//
//            frmNurkoRepository.Free;
//       	 end;
	end;
    end;
end;

//	������� �� ��� �� ������ ��� �������� �� �����
procedure TfrmUpDownStream.cxButton1Click(Sender: TObject);
begin
  dm.tblUpStream.Close;
  dm.tblUpStream.ParamByName('lot').AsString := cbUp.Text;
  dm.tblUpStream.Open;
end;

procedure TfrmUpDownStream.cxButton2Click(Sender: TObject);
begin
  dm.tblDownStream.Close;
  dm.tblDownStream.ParamByName('lot').AsString := cbDown.Text;
  dm.tblDownStream.Open;
end;

procedure TfrmUpDownStream.cxDBTextEditAllEnter(Sender: TObject);
begin
    TEdit(Sender).Color:=clSkyBlue;
end;

//	������� �� ��� �� ������ ��� ����� �� �����
procedure TfrmUpDownStream.cxDBTextEditAllExit(Sender: TObject);
begin
    TEdit(Sender).Color:=clWhite;
  if ((Sender as TcxLookupComboBox)= cbup) then
    begin
      txtVidA.Text := tblLotProizvodVID_PROIZVOD.AsString;
      txtA.Text := tblLotProizvodPROIZVOD.AsString;
      cbA.Text := tblLotProizvodNAZIV_PROIZVOD.AsString;
    end
    else
    begin
      txtVS.Text := tblLotSurovinaVID_SUROVINA.AsString;
      txtS.Text := tblLotSurovinaSUROVINA.AsString;
      cbSurovina.Text := tblLotSurovinaNAZIV_SUROVINA.AsString;
    end;
end;

procedure TfrmUpDownStream.cxGrid1DBTableView1FilterCustomization(
  Sender: TcxCustomGridTableView; var ADone: Boolean);
begin
end;

//  ��������� �� ������ �� ��������� �� ���������� ���� �� ��������
//  ��������� ��: br - ����� ��� �� ������ (�� 0 �� 9) �� ��������� �� ����� �� ����� �� ���������� ����
//  s - ��������� �� ������ �� ������ (���������� �� string �� ����� �� integer �� IntToStr ���������)
procedure TfrmUpDownStream.SetSifra(br: integer; s : string);
begin
  _sifra_kluc[br] := s;
end;

//  ������� �� ������ �� ��������� �� ���������� ���� �� ��������
//  ��������� ��: br - ����� ��� �� ������ (�� 0 �� 9) �� ��������� �� ����� �� ����� �� ���������� ����
function TfrmUpDownStream.GetSifra(br: integer) : string;
begin
  Result := _sifra_kluc[br];
end;

procedure TfrmUpDownStream.PartnerPropertiesChange(Sender: TObject);
begin

end;

//  �� ������� �� ������� ��� ����������� �� Master-�� �� �������� �� ��������� �� ������ ����� SetSifra
procedure TfrmUpDownStream.prefrli;
begin
end;

procedure TfrmUpDownStream.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    //	��������� ��� ��������� �� �������
//    if (cxGrid1DBTableView1.DataController.DataSource.State = dsEdit) or (cxGrid1DBTableView1.DataController.DataSource.State = dsInsert) then
//    begin
//        frmDaNe := TfrmDaNe.Create(self, '���������� ��������', '���������� �� �� ��������. ���� ������ �� �� �������?', 1);
//        if (frmDaNe.ShowModal <> mrYes) then
//        begin
//            cxGrid1DBTableView1.DataController.DataSet.Cancel;
//            Action := caFree;
//        end
//        else
//          if (Validacija(dPanel) = false) then
//          begin
//            cxGrid1DBTableView1.DataController.DataSet.Post;
//            Action := caFree;
//          end
//          else Action := caNone;
//    end;

  dmRes.FreeRepository(rData);
end;
procedure TfrmUpDownStream.FormCreate(Sender: TObject);
begin
  ProcitajFormaIzgled(self);
  if (dmRes.use_skin = true) then dxRibbon1.ColorSchemeName := dmRes.skin_name;

  rData := TRepositoryData.Create();  // kreirame instance od klasata TRepositoryData sto se naogja vo dmRes
end;

//------------------------------------------------------------------------------

procedure TfrmUpDownStream.FormShow(Sender: TObject);
begin
  //  ������� �� ������������ ������������/���������� �� ���������� �� ���� �����
    SpremiForma(self);
  //	�� ��������� �� ������� ������ ��� ���������� � ���� � ��� �������� �� �������
//    PrvPosledenTab(dPanel,posledna,prva);
    //dxBarManager1Bar1.Caption := Caption;
    dxComponentPrinter1Link1.ReportTitle.Text := Caption;

  //	������� �� ������������ �� ������ �� ������
    procitajGridOdBaza(Name,cxGrid1DBTableView1,false,false);
  //	������� �� ������������ �� ��������� �� ������
    procitajPrintOdBaza(Name,cxGrid1DBTableView1.Name, dxComponentPrinter1Link1);
    sobrano := true;

    //������ ����������
    tblLotProizvod.CloseOpen(true);
    tblLotSurovina.CloseOpen(true);

    cxPageControl1.ActivePage := tsProizvod;
  //  dm.tblUpStream.Close;
   // dm.tblUpStream.ParamByName('lot').Value :=

//  �������� �� ����� �� ������ �� cxExtLookupComboBox ��� cxDBExtLookupComboBox ��������
    //cxExtLookupComboBox1.RepositoryItem := dmRes.InitRepository(23, 'cxExtLookupComboBox1', Name, rData);
  	//procitajGridOdBaza(Name + '_' + IntToStr(rData.nurkoRepository[rData.br_repository]), rData.gridRepository[rData.br_repository] ,false,false);
end;
//------------------------------------------------------------------------------

procedure TfrmUpDownStream.SaveToIniFileExecute(Sender: TObject);
begin
    cxGrid1DBTableView1.StoreToIniFile(Name,True,[]);
end;
//------------------------------------------------------------------------------

procedure TfrmUpDownStream.cxGrid1DBTableView1KeyPress(Sender: TObject;
  var Key: Char);
begin
  if(Ord(Key) <> VK_RETURN) then cxSortiraj(cxGrid1DBTableView1);
end;

procedure TfrmUpDownStream.cxGrid1DBTableView1TcxGridDBDataControllerTcxDataSummaryFooterSummaryItems0GetText(
  Sender: TcxDataSummaryItem; const AValue: Variant; AIsFooter: Boolean;
  var AText: string);
begin
  AText := '������ �������� '+ VarToStr(AValue)+' '+dm.tblUpStreamMERKA_KOLICINA.AsString;
end;

procedure TfrmUpDownStream.cxGrid1DBTableView1TcxGridDBDataControllerTcxDataSummarySummaryGroups0SummaryItems0GetText(
  Sender: TcxDataSummaryItem; const AValue: Variant; AIsFooter: Boolean;
  var AText: string);
begin
     AText := '������ �������� '+ VarToStr(AValue)+' '+dm.tblDownStreamMERKA_KOLICINA.AsString;
end;

procedure TfrmUpDownStream.cxGridDBTableView1TcxGridDBDataControllerTcxDataSummaryFooterSummaryItems0GetText(
  Sender: TcxDataSummaryItem; const AValue: Variant; AIsFooter: Boolean;
  var AText: string);
begin
 AText := '������ �������� '+ VarToStr(AValue)+' '+dm.tblDownStreamMERKA.AsString;
end;

procedure TfrmUpDownStream.cxGridDBTableView1TcxGridDBDataControllerTcxDataSummarySummaryGroups0SummaryItems0GetText(
  Sender: TcxDataSummaryItem; const AValue: Variant; AIsFooter: Boolean;
  var AText: string);
begin
    AText := '������ �������� '+ VarToStr(AValue)+' '+dm.tblDownStreamMERKA.AsString;
end;

//  ����� �� �����
procedure TfrmUpDownStream.aZapisiExecute(Sender: TObject);
var
  st: TDataSetState;
begin
//  ZapisiButton.SetFocus;

//  st := cxGrid1DBTableView1.DataController.DataSet.State;
//  if st in [dsEdit,dsInsert] then
//  begin
//    if (Validacija(dPanel) = false) then
//    begin
//      if ((st = dsInsert) and inserting) then
//      begin
//        cxGrid1DBTableView1.DataController.DataSet.Post;
//        aNov.Execute;
//      end;
//
//      if ((st = dsInsert) and (not inserting)) then
//      begin
//        cxGrid1DBTableView1.DataController.DataSet.Post;
//        dPanel.Enabled:=false;
//        lPanel.Enabled:=true;
//        cxGrid1.SetFocus;
//      end;
//
//      if (st = dsEdit) then
//      begin
//        cxGrid1DBTableView1.DataController.DataSet.Post;
//        dPanel.Enabled:=false;
//        lPanel.Enabled:=true;
//        cxGrid1.SetFocus;
//      end;
//    end;
//  end;
end;

//	����� �� ���������� �� �������
procedure TfrmUpDownStream.aOtkaziExecute(Sender: TObject);
begin
//  if (cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) then
//  begin
//      ModalResult := mrCancel;
//      Close();
//  end
//  else
//  begin
//      cxGrid1DBTableView1.DataController.DataSet.Cancel;
//      RestoreControls(dPanel);
//      dPanel.Enabled := false;
//      lPanel.Enabled := true;
//      cxGrid1.SetFocus;
//  end;
end;

//----------------------------------------------------------------------------------
// ��������� �� ��������, ���������� �� ��������� � �������� �� Print ��������
//----------------------------------------------------------------------------------

//	����� �� �������� �� �������� �� �������
procedure TfrmUpDownStream.aPageSetupExecute(Sender: TObject);
begin
  dxComponentPrinter1Link1.PageSetup;
end;

//	����� �� ������� �� ������
procedure TfrmUpDownStream.aPecatiTabelaExecute(Sender: TObject);
begin
if cxPageControl1.ActivePage = tsProizvod then
  begin
    dxComponentPrinter1Link1.ReportTitle.Text := Caption;

    dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Clear;
    dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Add(dmkon.firma_naziv);
    dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Add(DateTimeToStr(Now));

    dxComponentPrinter1Link1.PrinterPage.PageHeader.LeftTitle.Clear;
  //  dxComponentPrinter1Link1.PrinterPage.PageHeader.LeftTitle.Add('������� : ' + TipSifra.Text + '-' + IdSifra.Text + ' ' + Partner.Text);

    dxComponentPrinter1.Preview(true, dxComponentPrinter1Link1);
  end
  else
  begin
    dxComponentPrinter2Link1.ReportTitle.Text := Caption;

    dxComponentPrinter2Link1.PrinterPage.PageHeader.RightTitle.Clear;
    dxComponentPrinter2Link1.PrinterPage.PageHeader.RightTitle.Add(dmkon.firma_naziv);
    dxComponentPrinter2Link1.PrinterPage.PageHeader.RightTitle.Add(DateTimeToStr(Now));

    dxComponentPrinter2Link1.PrinterPage.PageHeader.LeftTitle.Clear;
    //dxComponentPrinter1Link1.PrinterPage.PageHeader.LeftTitle.Add('������� : ' + TipSifra.Text + '-' + IdSifra.Text + ' ' + Partner.Text);

    dxComponentPrinter2.Preview(true, dxComponentPrinter2Link1);
  end;
end;

//	����� �� ���������� �� ��������� �� �������
procedure TfrmUpDownStream.aPodesuvanjePecatenjeExecute(Sender: TObject);
begin
  dxComponentPrinter1Link1.DesignReport();
end;

procedure TfrmUpDownStream.aSnimiPecatenjeExecute(Sender: TObject);
begin
  zacuvajPrintVoBaza(Name,cxGrid1DBTableView1.Name,dxComponentPrinter1Link1);
end;

//	����� �� �������� ��� �������� �� �������� �� ������ ���� ��� ��������� �� ����� ������
procedure TfrmUpDownStream.aSpustiSoberiExecute(Sender: TObject);
begin
  if (sobrano = true) then
  begin
    cxGrid1DBTableView1.ViewData.Expand(false);
    sobrano := false;
  end
  else
  begin
    cxGrid1DBTableView1.ViewData.Collapse(false);
    sobrano := true;
  end;
end;

procedure TfrmUpDownStream.aUpDizajnerExecute(Sender: TObject);
begin
if cxPageControl1.ActivePage = tsProizvod then
begin
 if not (dm.tblUpStream.IsEmpty) then
 begin
  dmRes.Spremi('MAN', 4);
  dmKon.tblSqlReport.ParamByName('lot').AsString :=dm.tblUpStreamLOT_PROIZVOD.AsString;
  //dmKon.tblSqlReport.ParamByName('id').AsString := '%';
  dmKon.tblSqlReport.Open;

  dmRes.frxReport1.Variables.Variables['proizvod']:=quotedstr(dm.tblUpStreamNAZIV_PROIZVOD.AsString);
  dmRes.frxReport1.Variables.Variables['lot_rns']:=QuotedStr(dm.tblUpStreamLOT_PROIZVOD.AsString);
  dmRes.frxReport1.DesignReport();
 end
end
else
//begin
if cxPageControl1.ActivePage = tsSurovina then
 begin
  if not (dm.tblDownStream.IsEmpty) then
  begin
   dmRes.Spremi('MAN', 6);
   dmKon.tblSqlReport.ParamByName('lot').AsString :=dm.tblDownStreamLOT_NO.AsString;
   //dmKon.tblSqlReport.ParamByName('id').AsString := '%';
   dmKon.tblSqlReport.Open;

   dmRes.frxReport1.Variables.Variables['proizvod']:=quotedstr(dm.tblDownStreamNAZIV_SUROVINA.AsString);
   dmRes.frxReport1.Variables.Variables['lot_rns']:=QuotedStr(dm.tblDownStreamLOT_NO.AsString);
   dmRes.frxReport1.DesignReport();
  end;
//end;
end;
end;

procedure TfrmUpDownStream.aUpDownExecute(Sender: TObject);
begin
if True then



if not (dm.tblUpStream.IsEmpty) then
 begin
  dmRes.Spremi('MAN', 6);
  dmKon.tblSqlReport.ParamByName('lot').AsString :=dm.tblDownStreamLOT_NO.AsString;
  //dmKon.tblSqlReport.ParamByName('id').AsString := '%';
  dmKon.tblSqlReport.Open;

  dmRes.frxReport1.Variables.Variables['proizvod']:=quotedstr(dm.tblUpStreamNAZIV_PROIZVOD.AsString);
  dmRes.frxReport1.Variables.Variables['lot_rns']:=QuotedStr(dm.tblUpStreamLOT_PROIZVOD.AsString);
  dmRes.frxReport1.ShowReport();
 end;
end;

procedure TfrmUpDownStream.aUpExecute(Sender: TObject);
begin
if cxPageControl1.ActivePage = tsProizvod then
begin
 if not (dm.tblUpStream.IsEmpty) then
 begin
  dmRes.Spremi('MAN', 4);
  dmKon.tblSqlReport.ParamByName('lot').AsString :=dm.tblUpStreamLOT_PROIZVOD.AsString;
  //dmKon.tblSqlReport.ParamByName('id').AsString := '%';
  dmKon.tblSqlReport.Open;

  dmRes.frxReport1.Variables.Variables['proizvod']:=quotedstr(dm.tblUpStreamNAZIV_PROIZVOD.AsString);
  dmRes.frxReport1.Variables.Variables['lot_rns']:=QuotedStr(dm.tblUpStreamLOT_PROIZVOD.AsString);
  dmRes.frxReport1.ShowReport();
 end
end
else
//begin
if cxPageControl1.ActivePage = tsSurovina then
 begin
  if not (dm.tblDownStream.IsEmpty) then
  begin
   dmRes.Spremi('MAN', 6);
   dmKon.tblSqlReport.ParamByName('lot').AsString :=dm.tblDownStreamLOT_NO.AsString;
   //dmKon.tblSqlReport.ParamByName('id').AsString := '%';
   dmKon.tblSqlReport.Open;

   dmRes.frxReport1.Variables.Variables['proizvod']:=quotedstr(dm.tblDownStreamNAZIV_SUROVINA.AsString);
   dmRes.frxReport1.Variables.Variables['lot_rns']:=QuotedStr(dm.tblDownStreamLOT_NO.AsString);
   dmRes.frxReport1.ShowReport();
  end;
//end;
end;

end;

//	����� �� ������ �� ����������� �������� �� ������� (Utils.pas)
procedure TfrmUpDownStream.aBrisiPodesuvanjePecatenjeExecute(Sender: TObject);
begin
//  brisiPrintOdBaza(Name,cxGrid1DBTableView1.Name, dxComponentPrinter1Link1);
end;

//  ����� �� ����� �� ����� �� ������������� �� �������� � ��������������� ��
//  �������(������ ���������� ��������)
procedure TfrmUpDownStream.aFormConfigExecute(Sender: TObject);
begin
  frmFormConfig := TfrmFormConfig.Create(Application);
  frmFormConfig.formPtr := Addr(Self);
  frmFormConfig.ShowModal;
  frmFormConfig.Free;
end;

//----------------------------------------------------------------------------------
// ���� -> ��������� �� ��������, ���������� �� ��������� � �������� �� Print ��������
//----------------------------------------------------------------------------------

// ��������� �� ���������� �� ��������� � �������� �� Print �������� �� .ini ��������
procedure TfrmUpDownStream.zacuvajPrintVoIni(ime:AnsiString);
var
  AStream: TFileStream;
begin
  AStream := TFileStream.Create(dmKon.RabotenDir + ime + 'Print.ini', fmCreate);
  try
    AStream.WriteComponent(dxComponentPrinter1Link1);
  finally
    AStream.Free;
  end;

end;

// ��������� �� ������ �� ��������� � �������� �� Print �������� �������� �� .ini ��������
procedure TfrmUpDownStream.procitajPrintOdIni(ime:AnsiString);
var
  AStream: TFileStream;
begin
  AStream := TFileStream.Create(dmKon.RabotenDir + ime + 'Print.ini', fmOpenRead);
  try
    dxComponentPrinter1Link1.RestoreDefaults;
    AStream.ReadComponent(dxComponentPrinter1Link1);
  finally
    AStream.Free;
  end;
end;


end.
