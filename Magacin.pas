unit Magacin;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ComCtrls, dxtree, dxdbtree,
  Vcl.ExtCtrls, Vcl.ActnList, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxContainer, cxEdit, dxSkinsCore, dxSkinBlack,
  dxSkinBlue, dxSkinBlueprint, dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom,
  dxSkinDarkSide, dxSkinDevExpressDarkStyle, dxSkinDevExpressStyle, dxSkinFoggy,
  dxSkinGlassOceans, dxSkinHighContrast, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven,
  dxSkinSevenClassic, dxSkinSharp, dxSkinSharpPlus, dxSkinSilver,
  dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008, dxSkinTheAsphaltWorld,
  dxSkinsDefaultPainters, dxSkinValentine, dxSkinVS2010, dxSkinWhiteprint,
  dxSkinXmas2008Blue, cxTextEdit, cxMaskEdit, cxDropDownEdit, cxLookupEdit,
  cxDBLookupEdit, cxDBLookupComboBox, Vcl.Menus, Vcl.StdCtrls, cxButtons,
  cxLabel, cxDBLabel;

type
  TfrmMagacin = class(TForm)
    Panel1: TPanel;
    dxTree: TdxDBTreeView;
    ActionList1: TActionList;
    aOtkazi: TAction;
    lPanel: TPanel;
    lcbMagacin: TcxLookupComboBox;
    lblMagacin: TcxDBLabel;
    btnOdberi: TcxButton;
    aOdberi: TAction;
    cxEditRepository1: TcxEditRepository;
    procedure aOtkaziExecute(Sender: TObject);
    procedure dxTreeDblClick(Sender: TObject);
    procedure dxTreeKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure FormShow(Sender: TObject);
    procedure EnterKakoTab(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure aOdberiExecute(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
    re_id : integer;

  public
    { Public declarations }
    re_selected : boolean;
  end;

var
  frmMagacin: TfrmMagacin;

implementation

{$R *.dfm}

uses dmUnit, dmResources, dmMaticni;

procedure TfrmMagacin.aOdberiExecute(Sender: TObject);
begin
  re_selected := true;

  Close;
end;

procedure TfrmMagacin.aOtkaziExecute(Sender: TObject);
begin
  re_selected := false;

  Close;
end;

procedure TfrmMagacin.dxTreeDblClick(Sender: TObject);
begin
  re_selected := true;
  Close;
end;

procedure TfrmMagacin.dxTreeKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
  if(Ord(Key) = VK_RETURN) then Close;
end;

procedure TfrmMagacin.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  if (re_selected = false) then
  begin
    if (re_id <> dmMat.tblReMagacinID.Value) then dmMat.tblReMagacin.Locate('ID', re_id, []);
  end;
end;

procedure TfrmMagacin.FormShow(Sender: TObject);
begin
  re_selected := false;

  lcbMagacin.SetFocus;

  re_id := dmMat.tblReMagacinID.Value;
end;

//  ��������� �� ������ �� ������� �� ���������� �� ����� �� Enter
procedure TfrmMagacin.EnterKakoTab(Sender: TObject; var Key: Word; Shift: TShiftState);
begin
    case Key of
        VK_DOWN:
        begin
          PostMessage(Handle,WM_NextDlgCtl,0,0);
          key:=0;
        end;
        VK_UP:
        begin
          PostMessage(Handle,WM_NextDlgCtl,1,0);
          key:=0;
        end;
        VK_RETURN:
        begin
          PostMessage(Handle,WM_NextDlgCtl,0,0);
        end;
    end;
end;

end.
