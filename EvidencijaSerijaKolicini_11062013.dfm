object frmEvidencijaSerijaKolicini: TfrmEvidencijaSerijaKolicini
  Left = 128
  Top = 212
  BorderIcons = [biSystemMenu, biMaximize]
  Caption = #1045#1074#1080#1076#1077#1085#1094#1080#1112#1072' '#1085#1072' '#1082#1086#1083#1080#1095#1080#1085#1072' '#1087#1086' '#1089#1077#1088#1080#1112#1072
  ClientHeight = 594
  ClientWidth = 954
  Color = 15790320
  Constraints.MinHeight = 500
  Constraints.MinWidth = 610
  Font.Charset = RUSSIAN_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  ShowHint = True
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object dxRibbon1: TdxRibbon
    Left = 0
    Top = 0
    Width = 954
    Height = 126
    BarManager = dxBarManager1
    ColorSchemeName = 'Blue'
    Contexts = <>
    TabOrder = 0
    TabStop = False
    object dxRibbon1Tab1: TdxRibbonTab
      Active = True
      Caption = #1052#1077#1085#1080
      Groups = <
        item
          Caption = #1057#1077#1083#1077#1082#1094#1080#1112#1072
          ToolbarName = 'dxBarManager1Bar1'
        end
        item
          ToolbarName = 'dxBarManager1Bar2'
        end
        item
          ToolbarName = 'dxBarManager1Bar3'
        end>
      Index = 0
    end
    object dxRibbon1Tab2: TdxRibbonTab
      Caption = #1055#1086#1076#1077#1089#1091#1074#1072#1114#1072
      Groups = <
        item
          ToolbarName = 'dxBarManager1Bar4'
        end
        item
          ToolbarName = 'dxBarManager1BarIzgledGrid'
        end>
      Index = 1
    end
  end
  object StatusBar1: TdxRibbonStatusBar
    Left = 0
    Top = 571
    Width = 954
    Height = 23
    Panels = <
      item
        PanelStyleClassName = 'TdxStatusBarTextPanelStyle'
        Text = ' F7 - '#1054#1089#1074#1077#1078#1080', F9 - '#1047#1072#1087#1080#1096#1080', Esc - '#1054#1090#1082#1072#1078#1080' / '#1048#1079#1083#1077#1079
      end>
    Ribbon = dxRibbon1
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clDefault
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
  end
  object Panel1: TPanel
    Left = 0
    Top = 126
    Width = 954
    Height = 445
    Align = alClient
    Color = 15790320
    ParentBackground = False
    TabOrder = 6
    object cxGrid1: TcxGrid
      Left = 1
      Top = 36
      Width = 952
      Height = 356
      Align = alClient
      TabOrder = 0
      object cxGrid1DBTableView1: TcxGridDBTableView
        OnEditKeyDown = cxGrid1DBTableView1EditKeyDown
        OnFocusedRecordChanged = cxGrid1DBTableView1FocusedRecordChanged
        DataController.DataSource = dsArtikalSerija
        DataController.Options = [dcoAnsiSort, dcoAssignGroupingValues, dcoAssignMasterDetailKeys, dcoSaveExpanding, dcoImmediatePost]
        DataController.Summary.DefaultGroupSummaryItems = <
          item
            Format = '###,##0.00'
            Kind = skSum
            Position = spFooter
            Column = UPOTREBENO
          end>
        DataController.Summary.FooterSummaryItems = <
          item
            Format = '###,##0.00'
            Kind = skSum
            Column = UPOTREBENO
          end>
        DataController.Summary.SummaryGroups = <>
        Filtering.ColumnFilteredItemsList = True
        FilterRow.InfoText = #1050#1083#1080#1082#1085#1080' '#1090#1091#1082#1072' '#1079#1072' '#1076#1072' '#1076#1077#1092#1080#1085#1080#1088#1072#1096' '#1092#1080#1083#1090#1077#1088
        FilterRow.Visible = True
        FilterRow.ApplyChanges = fracImmediately
        Images = dmRes.cxImageGrid
        OptionsBehavior.GoToNextCellOnEnter = True
        OptionsBehavior.IncSearch = True
        OptionsBehavior.FocusCellOnCycle = True
        OptionsCustomize.ColumnsQuickCustomization = True
        OptionsData.Deleting = False
        OptionsData.Inserting = False
        OptionsSelection.HideFocusRectOnExit = False
        OptionsView.NoDataToDisplayInfoText = '<'#1053#1077#1084#1072' '#1072#1088#1090#1080#1082#1083#1080' '#1074#1086' '#1087#1086#1087#1080#1089#1085#1072#1090#1072' '#1083#1080#1089#1090#1072'>'
        OptionsView.Footer = True
        OptionsView.GroupByBox = False
        OptionsView.HeaderAutoHeight = True
        object cxGrid1DBTableView1O_IN_S_ID: TcxGridDBColumn
          DataBinding.FieldName = 'O_IN_S_ID'
          Visible = False
          Options.Editing = False
          Options.Focusing = False
          Width = 82
        end
        object cxGrid1DBTableView1O_REF_ID: TcxGridDBColumn
          DataBinding.FieldName = 'O_REF_ID'
          Visible = False
          Options.Editing = False
          Options.Focusing = False
          Width = 86
        end
        object cxGrid1DBTableView1O_VLEZ_KOLICINA: TcxGridDBColumn
          DataBinding.FieldName = 'O_VLEZ_KOLICINA'
          Visible = False
          Options.Editing = False
          Options.Focusing = False
        end
        object cxGrid1DBTableView1O_IZLEZ_KOLICINA: TcxGridDBColumn
          DataBinding.FieldName = 'O_IZLEZ_KOLICINA'
          Visible = False
          Options.Editing = False
          Options.Focusing = False
        end
        object cxGrid1DBTableView1O_KNIG_CENA: TcxGridDBColumn
          DataBinding.FieldName = 'O_KNIG_CENA'
          Visible = False
          Options.Editing = False
          Options.Focusing = False
        end
        object cxGrid1DBTableView1O_OSTATOK: TcxGridDBColumn
          DataBinding.FieldName = 'O_OSTATOK'
          Options.Editing = False
          Options.Focusing = False
          Width = 75
        end
        object UPOTREBENO: TcxGridDBColumn
          Caption = #1059#1087#1086#1090#1088#1077#1073#1077#1085#1086
          DataBinding.ValueType = 'Float'
          HeaderImageIndex = 17
          Styles.Content = dmRes.MoneyGreen
          Width = 100
        end
        object cxGrid1DBTableView1LOT_NO: TcxGridDBColumn
          DataBinding.FieldName = 'LOT_NO'
          Options.Editing = False
          Options.Focusing = False
          Width = 150
        end
        object cxGrid1DBTableView1ISP_GOD: TcxGridDBColumn
          DataBinding.FieldName = 'ISP_GOD'
          Options.Editing = False
          Options.Focusing = False
          Width = 84
        end
        object cxGrid1DBTableView1ISP_BR: TcxGridDBColumn
          DataBinding.FieldName = 'ISP_BR'
          Options.Editing = False
          Options.Focusing = False
          Width = 77
        end
        object cxGrid1DBTableView1TP: TcxGridDBColumn
          DataBinding.FieldName = 'TP'
          Visible = False
          Options.Editing = False
          Options.Focusing = False
        end
        object cxGrid1DBTableView1P: TcxGridDBColumn
          DataBinding.FieldName = 'P'
          Visible = False
          Options.Editing = False
          Options.Focusing = False
        end
        object cxGrid1DBTableView1ISP_PARTNER: TcxGridDBColumn
          DataBinding.FieldName = 'ISP_PARTNER'
          Options.Editing = False
          Options.Focusing = False
          Width = 300
        end
        object cxGrid1DBTableView1ISP_PARTNER_MESTO: TcxGridDBColumn
          DataBinding.FieldName = 'ISP_PARTNER_MESTO'
          Options.Editing = False
          Options.Focusing = False
          Width = 100
        end
      end
      object cxGrid1Level1: TcxGridLevel
        GridView = cxGrid1DBTableView1
      end
    end
    object Panel2: TPanel
      Left = 1
      Top = 1
      Width = 952
      Height = 35
      Align = alTop
      Color = 15790320
      ParentBackground = False
      TabOrder = 1
      DesignSize = (
        952
        35)
      object lblSelekcijaSuma: TLabel
        Left = 681
        Top = 8
        Width = 152
        Height = 13
        Alignment = taRightJustify
        Anchors = [akTop, akRight]
        Caption = #1057#1045#1051#1045#1050#1058#1048#1056#1040#1053#1040' '#1050#1054#1051#1048#1063#1048#1053#1040' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        ExplicitLeft = 611
      end
      object lblStorn: TLabel
        Left = 12
        Top = 8
        Width = 3
        Height = 13
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Visible = False
      end
      object Label1: TLabel
        Left = 12
        Top = 9
        Width = 125
        Height = 13
        Caption = #1042#1053#1045#1057#1045#1053#1040' '#1050#1054#1051#1048#1063#1048#1053#1040' : '
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
      end
      object SumaSelekcija: TcxCurrencyEdit
        Left = 839
        Top = 5
        Anchors = [akTop, akRight]
        Properties.AssignedValues.DisplayFormat = True
        Properties.ReadOnly = True
        Properties.UseDisplayFormatWhenEditing = True
        TabOrder = 0
        Width = 100
      end
      object VnesenaKolicina: TcxCurrencyEdit
        Left = 143
        Top = 5
        Properties.AssignedValues.DisplayFormat = True
        Properties.AssignedValues.EditFormat = True
        Properties.ReadOnly = True
        Properties.UseDisplayFormatWhenEditing = True
        TabOrder = 1
        Width = 100
      end
    end
    object Panel3: TPanel
      Left = 1
      Top = 392
      Width = 952
      Height = 52
      Align = alBottom
      Color = 15790320
      ParentBackground = False
      TabOrder = 2
      object Label3: TLabel
        Left = 12
        Top = 8
        Width = 3
        Height = 13
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = [fsBold]
        ParentFont = False
        Visible = False
      end
      object ZapisiButton: TcxButton
        Left = 783
        Top = 14
        Width = 75
        Height = 25
        Action = aZapisi
        Colors.Pressed = clGradientActiveCaption
        TabOrder = 0
      end
      object OtkaziButton: TcxButton
        Left = 864
        Top = 14
        Width = 75
        Height = 25
        Action = aOtkazi
        Colors.Pressed = clGradientActiveCaption
        TabOrder = 1
      end
    end
  end
  object cxGridPopupMenu1: TcxGridPopupMenu
    Grid = cxGrid1
    PopupMenus = <>
    Left = 328
    Top = 376
  end
  object PopupMenu1: TPopupMenu
    Left = 456
    Top = 432
    object N1: TMenuItem
      Caption = #1057#1085#1080#1084#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      ShortCut = 49235
      OnClick = SaveToIniFileExecute
    end
    object Excel1: TMenuItem
      Caption = #1057#1085#1080#1084#1080' '#1075#1086' '#1074#1086' Excel'
      ShortCut = 49221
    end
  end
  object dxBarManager1: TdxBarManager
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Segoe UI'
    Font.Style = []
    Categories.Strings = (
      'Default')
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    ImageOptions.Images = dmRes.cxSmallImages
    ImageOptions.LargeImages = dmRes.cxLargeImages
    PopupMenuLinks = <>
    UseSystemFont = True
    Left = 608
    Top = 376
    DockControlHeights = (
      0
      0
      0
      0)
    object dxBarManager1Bar1: TdxBar
      Caption = 'Custom 1'
      CaptionButtons = <>
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 715
      FloatTop = 157
      FloatClientWidth = 51
      FloatClientHeight = 120
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton18'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarLargeButton19'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar2: TdxBar
      Caption = #1040#1082#1094#1080#1080
      CaptionButtons = <>
      DockedLeft = 136
      DockedTop = 0
      FloatLeft = 789
      FloatTop = 120
      FloatClientWidth = 51
      FloatClientHeight = 120
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton5'
        end
        item
          Visible = True
          ItemName = 'dxBarSubItem1'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar3: TdxBar
      Caption = #1048#1079#1083#1077#1079
      CaptionButtons = <>
      DockedLeft = 274
      DockedTop = 0
      FloatLeft = 718
      FloatTop = 62
      FloatClientWidth = 51
      FloatClientHeight = 80
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton7'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton8'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar4: TdxBar
      Caption = #1055#1077#1095#1072#1090#1077#1114#1077
      CaptionButtons = <>
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 676
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton12'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton15'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton9'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarLargeButton13'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1BarIzgledGrid: TdxBar
      Caption = #1058#1072#1073#1077#1083#1072
      CaptionButtons = <>
      DockedLeft = 363
      DockedTop = 0
      FloatLeft = 808
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLBtnSnimiIzgled'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarLBtnBrisiIzgled'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarLargeButton2: TdxBarLargeButton
      Action = aAzuriraj
      Category = 0
    end
    object dxBarLargeButton3: TdxBarLargeButton
      Action = aBrisi
      Category = 0
    end
    object dxBarLargeButton4: TdxBarLargeButton
      Action = aSnimiIzgled
      Category = 0
      ScreenTip = tipSnimiIzgled
    end
    object dxBarLargeButton5: TdxBarLargeButton
      Action = aZacuvajExcel
      Category = 0
      ScreenTip = tipZacuvajExcel
    end
    object dxBarLargeButton6: TdxBarLargeButton
      Action = aPecatiTabela
      Caption = #1055#1077#1095#1072#1090#1080' '#1090#1072#1073#1077#1083#1072
      Category = 0
    end
    object dxBarLargeButton7: TdxBarLargeButton
      Action = aHelp
      Category = 0
    end
    object dxBarLargeButton8: TdxBarLargeButton
      Action = aIzlez
      Category = 0
    end
    object dxBarLargeButton1: TdxBarLargeButton
      Action = aNov
      Category = 0
    end
    object dxBarLargeButton9: TdxBarLargeButton
      Action = aSnimiPecatenje
      Category = 0
      ScreenTip = tipSnimiKonf
    end
    object dxBarLargeButton11: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object ribbonFilterReset: TdxBarLargeButton
      Caption = #1056#1077#1089#1077#1090#1080#1088#1072#1112' '#1092#1080#1083#1090#1077#1088
      Category = 0
      Hint = #1056#1077#1089#1077#1090#1080#1088#1072#1112' '#1092#1080#1083#1090#1077#1088
      Visible = ivAlways
    end
    object dxBarLargeButton12: TdxBarLargeButton
      Action = aPodesuvanjePecatenje
      Category = 0
      ScreenTip = tipDizajnReport
    end
    object dxBarLargeButton13: TdxBarLargeButton
      Action = aBrisiPodesuvanjePecatenje
      Category = 0
      ScreenTip = tipBrisiKonf
    end
    object dxBarLargeButton14: TdxBarLargeButton
      Caption = #1048#1079#1073#1088#1080#1096#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      Category = 0
      Visible = ivAlways
      LargeImageIndex = 40
    end
    object dxBarLargeButton15: TdxBarLargeButton
      Action = aPageSetup
      Category = 0
      ScreenTip = tipSetiranjeStrana
    end
    object dxBarLargeButton16: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object dxBarLargeButton17: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object cxBarEditItem1: TcxBarEditItem
      Caption = 'New Item'
      Category = 0
      Hint = 'New Item'
      Visible = ivAlways
      PropertiesClassName = 'TcxCheckBoxProperties'
    end
    object dxBarSubItem1: TdxBarSubItem
      Caption = #1055#1077#1095#1072#1090#1077#1114#1077
      Category = 0
      Visible = ivAlways
      ImageIndex = 30
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton6'
        end>
    end
    object dxBarLBtnSnimiIzgled: TdxBarLargeButton
      Action = aSnimiIzgled
      Category = 0
    end
    object dxBarLBtnBrisiIzgled: TdxBarLargeButton
      Action = aBrisiIzgled
      Category = 0
    end
    object dxBarLargeButton10: TdxBarLargeButton
      Action = aSpustiSoberi
      Category = 0
    end
    object dxBarLargeButton18: TdxBarLargeButton
      Action = aZapisi
      Category = 0
    end
    object dxBarLargeButton19: TdxBarLargeButton
      Action = aOtkazi
      Category = 0
    end
  end
  object ActionList1: TActionList
    Images = dmRes.cxSmallImages
    Left = 200
    Top = 440
    object aNov: TAction
      Caption = #1053#1086#1074
      ImageIndex = 10
      ShortCut = 116
      OnExecute = aNovExecute
    end
    object aAzuriraj: TAction
      Caption = #1040#1078#1091#1088#1080#1088#1072#1112
      ImageIndex = 12
      ShortCut = 117
      OnExecute = aAzurirajExecute
    end
    object aBrisi: TAction
      Caption = #1041#1088#1080#1096#1080
      ImageIndex = 11
      ShortCut = 119
      OnExecute = aBrisiExecute
    end
    object aSnimiIzgled: TAction
      Caption = #1057#1085#1080#1084#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      ImageIndex = 9
      ShortCut = 24659
      OnExecute = aSnimiIzgledExecute
    end
    object aZacuvajExcel: TAction
      Caption = #1047#1072#1095#1091#1074#1072#1112' '#1074#1086' Excel'
      ImageIndex = 9
      ShortCut = 24645
      OnExecute = aZacuvajExcelExecute
    end
    object aPecatiTabela: TAction
      Caption = #1055#1077#1095#1072#1090#1080
      ImageIndex = 30
      OnExecute = aPecatiTabelaExecute
    end
    object aHelp: TAction
      Caption = #1055#1086#1084#1086#1096
      ImageIndex = 24
    end
    object aIzlez: TAction
      Caption = #1048#1079#1083#1077#1079
      ImageIndex = 25
      OnExecute = aIzlezExecute
    end
    object aZapisi: TAction
      Caption = #1047#1072#1087#1080#1096#1080
      ImageIndex = 7
      ShortCut = 120
      OnExecute = aZapisiExecute
    end
    object aOtkazi: TAction
      Caption = #1054#1090#1082#1072#1078#1080
      ImageIndex = 21
      ShortCut = 27
      OnExecute = aOtkaziExecute
    end
    object aRefresh: TAction
      Caption = 'aRefresh'
      ImageIndex = 18
      ShortCut = 118
      OnExecute = aRefreshExecute
    end
    object aSnimiPecatenje: TAction
      Caption = #1057#1085#1080#1084#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      ImageIndex = 39
      OnExecute = aSnimiPecatenjeExecute
    end
    object aPodesuvanjePecatenje: TAction
      Caption = #1044#1080#1079#1072#1112#1085' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090
      ImageIndex = 38
      OnExecute = aPodesuvanjePecatenjeExecute
    end
    object aBrisiPodesuvanjePecatenje: TAction
      Caption = #1041#1088#1080#1096#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      ImageIndex = 37
      OnExecute = aBrisiPodesuvanjePecatenjeExecute
    end
    object aPageSetup: TAction
      Caption = #1057#1077#1090#1080#1088#1072#1114#1077' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072
      ImageIndex = 38
      OnExecute = aPageSetupExecute
    end
    object aFormConfig: TAction
      Caption = 'aFormConfig'
      ShortCut = 24699
      OnExecute = aFormConfigExecute
    end
    object aBrisiIzgled: TAction
      Caption = #1041#1088#1080#1096#1080' '#1075#1086' '#1079#1072#1095#1091#1074#1072#1085#1080#1086#1090' '#1080#1079#1075#1083#1077#1076
      ImageIndex = 56
      OnExecute = aBrisiIzgledExecute
    end
    object aSpustiSoberi: TAction
      Caption = #1057#1087#1091#1096#1090#1080'/'#1057#1086#1073#1077#1088#1080' '#1058#1072#1073#1077#1083#1072
      ImageIndex = 65
      OnExecute = aSpustiSoberiExecute
    end
    object aSoberiSelektirani: TAction
      Caption = 'aSoberiSelektirani'
      OnExecute = aSoberiSelektiraniExecute
    end
  end
  object dxComponentPrinter1: TdxComponentPrinter
    CurrentLink = dxComponentPrinter1Link1
    Version = 0
    Left = 128
    Top = 400
    object dxComponentPrinter1Link1: TdxGridReportLink
      Component = cxGrid1
      PrinterPage.DMPaper = 9
      PrinterPage.Footer = 6350
      PrinterPage.GrayShading = True
      PrinterPage.Header = 6350
      PrinterPage.Margins.Bottom = 12700
      PrinterPage.Margins.Left = 12700
      PrinterPage.Margins.Right = 12700
      PrinterPage.Margins.Top = 12700
      PrinterPage.Orientation = poLandscape
      PrinterPage.PageFooter.RightTitle.Strings = (
        #1057#1090#1088'.[Page #] '#1086#1076' [Total Pages]')
      PrinterPage.PageSize.X = 210000
      PrinterPage.PageSize.Y = 297000
      PrinterPage._dxMeasurementUnits_ = 0
      PrinterPage._dxLastMU_ = 2
      ReportDocument.CreationDate = 40336.425359629630000000
      OptionsFormatting.UseNativeStyles = True
      OptionsOnEveryPage.Caption = False
      OptionsOnEveryPage.FilterBar = False
      OptionsView.Caption = False
      OptionsView.FilterBar = False
      StyleRepository = dmRes.cxStyleRepositoryPrinting
      Styles.StyleSheet = dmRes.dxGridReportLinkStyleSheet
      BuiltInReportLink = True
    end
  end
  object dxBarScreenTipRepository1: TdxBarScreenTipRepository
    Left = 448
    Top = 296
    object tipSnimiIzgled: TdxBarScreenTip
      Header.Text = #1057#1085#1080#1084#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      Description.Text = #1047#1072#1095#1091#1074#1072#1112' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090' '#1085#1072' '#1092#1086#1088#1084#1072#1090#1072' '#1080' '#1085#1072' '#1090#1072#1073#1077#1083#1072#1090#1072
    end
    object tipZacuvajExcel: TdxBarScreenTip
      Header.Text = #1047#1072#1095#1091#1074#1072#1112' '#1074#1086' Excel'
      Description.Text = #1057#1085#1080#1084#1080' '#1075#1080' '#1087#1086#1076#1072#1090#1086#1094#1080#1090#1077' '#1086#1076' '#1090#1072#1073#1077#1083#1072#1090#1072' '#1074#1086' Excel '#1092#1086#1088#1084#1072#1090
    end
    object tipDizajnReport: TdxBarScreenTip
      Header.Text = #1044#1080#1079#1072#1112#1085' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090
      Description.Text = #1055#1086#1076#1077#1089#1091#1074#1072#1114#1072' '#1085#1072' '#1080#1079#1075#1083#1077#1076#1086#1090' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090' '#1085#1072' '#1090#1072#1073#1077#1083#1072#1090#1072
    end
    object tipSetiranjeStrana: TdxBarScreenTip
      Header.Text = #1057#1077#1090#1080#1088#1072#1114#1077' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072
      Description.Text = #1055#1086#1076#1077#1089#1091#1074#1072#1114#1077' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072' '#1085#1072' '#1082#1086#1112#1072' '#1116#1077' '#1089#1077' '#1087#1077#1095#1072#1090#1080' '#1090#1072#1073#1077#1083#1072#1090#1072
    end
    object tipSnimiKonf: TdxBarScreenTip
      Header.Text = #1057#1085#1080#1084#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      Description.Text = #1047#1072#1095#1091#1074#1072#1112' '#1075#1080' '#1087#1086#1076#1077#1089#1091#1074#1072#1114#1072#1090#1072' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090' '#1080' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072' '#1079#1072' '#1087#1077#1095#1072#1090#1077#1114#1077
    end
    object tipBrisiKonf: TdxBarScreenTip
      Header.Text = #1041#1088#1080#1096#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      Description.Text = #1048#1079#1073#1088#1080#1096#1080' '#1075#1080' '#1087#1086#1076#1077#1089#1091#1074#1072#1114#1072#1090#1072' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090' '#1080' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072' '#1079#1072' '#1087#1077#1095#1072#1090#1077#1114#1077
    end
  end
  object tblArtikalSerija: TpFIBDataSet
    RefreshSQL.Strings = (
      'select SO.O_IN_S_ID,'
      '       SO.O_REF_ID,'
      '       S.LOT_NO,'
      '       I.GODINA as ISP_GOD,'
      '       I.BROJ as ISP_BR,'
      '       I.TP,'
      '       I.P,'
      '       P.NAZIV as ISP_PARTNER,'
      '       M.NAZIV as ISP_PARTNER_MESTO,'
      '       SO.O_VLEZ_KOLICINA,'
      '       SO.O_IZLEZ_KOLICINA,'
      '       SO.O_KNIG_CENA,'
      '       SO.O_OSTATOK,'
      '       0.0 as UPOTREBENO'
      'from PROC_MTR_INOUT_SERIJA_OSTATOK(:RE, :ART_VID, :ART_SIF) SO'
      'join MTR_IN_S S on S.ID = SO.O_REF_ID'
      'join MTR_IN I on I.ID = S.MTR_IN_ID'
      'left join MAT_PARTNER P on P.TIP_PARTNER = I.TP and P.ID = I.P'
      'left join MAT_MESTO M on M.ID = P.MESTO'
      'where SO.O_OSTATOK > 0'
      '      and I.AKTIVEN = 1   ')
    SelectSQL.Strings = (
      'select SO.O_IN_S_ID,'
      '       SO.O_REF_ID,'
      '       S.LOT_NO,'
      '       I.GODINA as ISP_GOD,'
      '       I.BROJ as ISP_BR,'
      '       I.TP,'
      '       I.P,'
      '       P.NAZIV as ISP_PARTNER,'
      '       M.NAZIV as ISP_PARTNER_MESTO,'
      '       SO.O_VLEZ_KOLICINA,'
      '       SO.O_IZLEZ_KOLICINA,'
      '       SO.O_KNIG_CENA,'
      '       SO.O_OSTATOK,'
      '       0.0 as UPOTREBENO'
      'from PROC_MTR_INOUT_SERIJA_OSTATOK(:RE, :ART_VID, :ART_SIF) SO'
      'join MTR_IN_S S on S.ID = SO.O_REF_ID'
      'join MTR_IN I on I.ID = S.MTR_IN_ID'
      'left join MAT_PARTNER P on P.TIP_PARTNER = I.TP and P.ID = I.P'
      'left join MAT_MESTO M on M.ID = P.MESTO'
      'where SO.O_OSTATOK > 0'
      '      and I.AKTIVEN = 1   ')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    Left = 256
    Top = 248
    poSQLINT64ToBCD = True
    object tblArtikalSerijaO_IN_S_ID: TFIBBCDField
      DisplayLabel = #1057#1090#1072#1074#1082#1072' '#1096#1080#1092'.'
      FieldName = 'O_IN_S_ID'
      Size = 0
      RoundByScale = True
    end
    object tblArtikalSerijaO_REF_ID: TFIBBCDField
      DisplayLabel = #1057#1090#1072#1074#1082#1072' REF_ID'
      FieldName = 'O_REF_ID'
      Size = 0
      RoundByScale = True
    end
    object tblArtikalSerijaO_VLEZ_KOLICINA: TFIBFloatField
      DisplayLabel = #1042#1083#1077#1079
      FieldName = 'O_VLEZ_KOLICINA'
      DisplayFormat = '###,###0.000'
      EditFormat = '0.000'
    end
    object tblArtikalSerijaO_IZLEZ_KOLICINA: TFIBFloatField
      DisplayLabel = #1048#1079#1083#1077#1079
      FieldName = 'O_IZLEZ_KOLICINA'
      DisplayFormat = '###,###0.000'
      EditFormat = '0.000'
    end
    object tblArtikalSerijaO_KNIG_CENA: TFIBFloatField
      DisplayLabel = #1050#1085#1080#1075'. '#1094#1077#1085#1072
      FieldName = 'O_KNIG_CENA'
      DisplayFormat = '###,##0.00'
      EditFormat = '0.00'
    end
    object tblArtikalSerijaO_OSTATOK: TFIBFloatField
      DisplayLabel = #1054#1089#1090#1072#1090#1086#1082
      FieldName = 'O_OSTATOK'
      DisplayFormat = '###,###0.000'
      EditFormat = '0.000'
    end
    object tblArtikalSerijaLOT_NO: TFIBStringField
      DisplayLabel = 'LOT '#1073#1088'.'
      FieldName = 'LOT_NO'
      Size = 30
      EmptyStrToNull = True
    end
    object tblArtikalSerijaISP_GOD: TFIBIntegerField
      DisplayLabel = #1055#1088#1080#1077#1084'. '#1075#1086#1076'.'
      FieldName = 'ISP_GOD'
    end
    object tblArtikalSerijaISP_BR: TFIBIntegerField
      DisplayLabel = #1055#1088#1080#1077#1084'. '#1073#1088#1086#1112
      FieldName = 'ISP_BR'
    end
    object tblArtikalSerijaTP: TFIBIntegerField
      DisplayLabel = #1058#1055
      FieldName = 'TP'
    end
    object tblArtikalSerijaP: TFIBIntegerField
      DisplayLabel = #1055
      FieldName = 'P'
    end
    object tblArtikalSerijaISP_PARTNER: TFIBStringField
      DisplayLabel = #1055#1072#1088#1090#1085#1077#1088
      FieldName = 'ISP_PARTNER'
      Size = 255
      EmptyStrToNull = True
    end
    object tblArtikalSerijaISP_PARTNER_MESTO: TFIBStringField
      DisplayLabel = #1052#1077#1089#1090#1086
      FieldName = 'ISP_PARTNER_MESTO'
      Size = 50
      EmptyStrToNull = True
    end
    object tblArtikalSerijaUPOTREBENO: TFIBBCDField
      DisplayLabel = #1059#1087#1086#1090#1088#1077#1073#1077#1085#1086
      FieldName = 'UPOTREBENO'
      Size = 1
      RoundByScale = True
    end
  end
  object dsArtikalSerija: TDataSource
    DataSet = tblArtikalSerija
    Left = 176
    Top = 248
  end
  object spGenerirajInOutSerija: TpFIBStoredProc
    Transaction = dmKon.tProc
    Database = dmKon.fibBaza
    SQL.Strings = (
      
        'EXECUTE PROCEDURE PROC_MTR_INOUT_GENERIRAJ_SERIJA (:IN_OUT_S_ID,' +
        ' :IN_PRIEMI_ID, :IN_PRIEMI_KOLICINA, :IN_PRIEMI_BR)')
    StoredProcName = 'PROC_MTR_INOUT_GENERIRAJ_SERIJA'
    Left = 576
    Top = 296
    qoAutoCommit = True
    qoStartTransaction = True
  end
end
