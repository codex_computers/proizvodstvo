object frmIzberiSD: TfrmIzberiSD
  Left = 0
  Top = 0
  Caption = #1057#1086#1089#1090#1072#1074#1085#1080' '#1076#1077#1083#1086#1074#1080' '#1080' '#1045#1090#1080#1082#1077#1090#1080
  ClientHeight = 646
  ClientWidth = 738
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 16
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 738
    Height = 348
    Align = alClient
    TabOrder = 0
    object cxGrid1: TcxGrid
      Left = 1
      Top = 1
      Width = 736
      Height = 346
      Align = alClient
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      object cxGrid1DBTableView1: TcxGridDBTableView
        Navigator.Buttons.CustomButtons = <>
        DataController.DataSource = dsIzberiSD
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        OptionsData.Deleting = False
        OptionsData.Editing = False
        OptionsData.Inserting = False
        OptionsSelection.MultiSelect = True
        OptionsSelection.CheckBoxVisibility = [cbvDataRow, cbvColumnHeader]
        OptionsSelection.HideSelection = True
        OptionsView.ColumnAutoWidth = True
        object cxGrid1DBTableView1ARTVID: TcxGridDBColumn
          Caption = #1042#1080#1076' '#1085#1072' '#1072#1088#1090#1080#1082#1072#1083
          DataBinding.FieldName = 'ARTVID'
        end
        object cxGrid1DBTableView1ID: TcxGridDBColumn
          Caption = #1064#1080#1092#1088#1072
          DataBinding.FieldName = 'ID'
        end
        object cxGrid1DBTableView1NAZIV: TcxGridDBColumn
          Caption = #1053#1072#1079#1080#1074
          DataBinding.FieldName = 'NAZIV'
          HeaderAlignmentHorz = taCenter
          Width = 228
        end
      end
      object cxGrid1DBBandedTableView1: TcxGridDBBandedTableView
        Navigator.Buttons.CustomButtons = <>
        DataController.DataSource = dsIzberiSD
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        OptionsData.Deleting = False
        OptionsData.Editing = False
        OptionsData.Inserting = False
        OptionsSelection.MultiSelect = True
        OptionsSelection.CellMultiSelect = True
        OptionsSelection.CheckBoxVisibility = [cbvDataRow]
        OptionsSelection.HideSelection = True
        OptionsSelection.InvertSelect = False
        OptionsView.CellAutoHeight = True
        Bands = <
          item
            Caption = #1057#1054#1057#1058#1040#1042#1053#1048' '#1044#1045#1051#1054#1042#1048
          end>
        object cxGrid1DBBandedTableView1ARTVID: TcxGridDBBandedColumn
          Caption = #1042#1080#1076' '#1085#1072' '#1072#1088#1090#1080#1082#1072#1083
          DataBinding.FieldName = 'ARTVID'
          Width = 89
          Position.BandIndex = 0
          Position.ColIndex = 0
          Position.RowIndex = 0
        end
        object cxGrid1DBBandedTableView1ID: TcxGridDBBandedColumn
          Caption = #1064#1080#1092#1088#1072
          DataBinding.FieldName = 'ID'
          Position.BandIndex = 0
          Position.ColIndex = 1
          Position.RowIndex = 0
        end
        object cxGrid1DBBandedTableView1NAZIV: TcxGridDBBandedColumn
          Caption = #1053#1072#1079#1080#1074
          DataBinding.FieldName = 'NAZIV'
          Width = 558
          Position.BandIndex = 0
          Position.ColIndex = 2
          Position.RowIndex = 0
        end
      end
      object cxGrid1Level1: TcxGridLevel
        GridView = cxGrid1DBBandedTableView1
      end
    end
  end
  object Panel3: TPanel
    Left = 0
    Top = 394
    Width = 738
    Height = 252
    Align = alBottom
    TabOrder = 1
    object StatusBar1: TStatusBar
      Left = 1
      Top = 232
      Width = 736
      Height = 19
      Panels = <
        item
          Text = 'F5 - '#1043#1077#1085#1077#1088#1080#1088#1072#1112' '#1045#1090#1080#1082#1077#1090#1080',  F8 / DoubleClick - '#1041#1088#1080#1096#1080' '#1089#1086#1089#1090#1072#1074#1077#1085' '#1076#1077#1083
          Width = 50
        end>
    end
    object cxGrid2: TcxGrid
      Left = 1
      Top = 1
      Width = 736
      Height = 231
      Align = alClient
      TabOrder = 1
      object cxGrid2DBTableView1: TcxGridDBTableView
        Navigator.Buttons.CustomButtons = <>
        DataController.DataSource = dsSosDelLot
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        object cxGrid2DBTableView1ID: TcxGridDBColumn
          Caption = #1064#1080#1092#1088#1072
          DataBinding.FieldName = 'ID'
          Visible = False
        end
        object cxGrid2DBTableView1VID_SOS_DEL: TcxGridDBColumn
          Caption = #1042#1080#1076' '#1089#1086#1089'.'#1076#1077#1083
          DataBinding.FieldName = 'VID_SOS_DEL'
          Width = 72
        end
        object cxGrid2DBTableView1SOS_DEL: TcxGridDBColumn
          Caption = #1064#1080#1092#1088#1072' '#1089#1086#1089'.'#1076#1077#1083
          DataBinding.FieldName = 'SOS_DEL'
          Width = 90
        end
        object cxGrid2DBTableView1NAZIV: TcxGridDBColumn
          Caption = #1053#1072#1079#1080#1074
          DataBinding.FieldName = 'NAZIV'
          Width = 231
        end
        object cxGrid2DBTableView1LOT: TcxGridDBColumn
          Caption = #1051#1086#1090
          DataBinding.FieldName = 'LOT'
          Width = 92
        end
        object cxGrid2DBTableView1BARKOD: TcxGridDBColumn
          Caption = #1041#1072#1088#1082#1086#1076
          DataBinding.FieldName = 'BARKOD'
          Width = 263
        end
      end
      object cxGrid2DBBandedTableView1: TcxGridDBBandedTableView
        OnDblClick = cxGrid2DBBandedTableView1DblClick
        Navigator.Buttons.CustomButtons = <>
        DataController.DataSource = dsSosDelLot
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        OptionsCustomize.ColumnsQuickCustomization = True
        OptionsData.Deleting = False
        OptionsData.Editing = False
        OptionsData.Inserting = False
        OptionsView.ColumnAutoWidth = True
        OptionsView.GroupByBox = False
        Bands = <
          item
            Caption = #1057#1086#1089#1090#1072#1074#1085#1080' '#1076#1077#1083#1086#1074#1080' '#1080' '#1080#1079#1075#1077#1085#1077#1088#1080#1088#1072#1085#1080' '#1077#1090#1080#1082#1077#1090#1080
          end>
        object cxGrid2DBBandedTableView1ID: TcxGridDBBandedColumn
          DataBinding.FieldName = 'ID'
          Visible = False
          Position.BandIndex = 0
          Position.ColIndex = 0
          Position.RowIndex = 0
        end
        object cxGrid2DBBandedTableView1VID_SOS_DEL: TcxGridDBBandedColumn
          DataBinding.FieldName = 'VID_SOS_DEL'
          Visible = False
          Position.BandIndex = 0
          Position.ColIndex = 1
          Position.RowIndex = 0
        end
        object cxGrid2DBBandedTableView1SOS_DEL: TcxGridDBBandedColumn
          Caption = #1064#1080#1092#1088#1072' '#1089#1086#1089'. '#1076#1077#1083
          DataBinding.FieldName = 'SOS_DEL'
          Position.BandIndex = 0
          Position.ColIndex = 2
          Position.RowIndex = 0
        end
        object cxGrid2DBBandedTableView1NAZIV: TcxGridDBBandedColumn
          Caption = #1053#1072#1079#1080#1074
          DataBinding.FieldName = 'NAZIV'
          Width = 200
          Position.BandIndex = 0
          Position.ColIndex = 3
          Position.RowIndex = 0
        end
        object cxGrid2DBBandedTableView1LOT: TcxGridDBBandedColumn
          Caption = #1051#1086#1090
          DataBinding.FieldName = 'LOT'
          Width = 150
          Position.BandIndex = 0
          Position.ColIndex = 4
          Position.RowIndex = 0
        end
        object cxGrid2DBBandedTableView1BARKOD: TcxGridDBBandedColumn
          Caption = #1041#1072#1088#1082#1086#1076
          DataBinding.FieldName = 'BARKOD'
          Width = 300
          Position.BandIndex = 0
          Position.ColIndex = 5
          Position.RowIndex = 0
        end
      end
      object cxGrid2Level1: TcxGridLevel
        GridView = cxGrid2DBBandedTableView1
      end
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 348
    Width = 738
    Height = 46
    Align = alBottom
    TabOrder = 2
    DesignSize = (
      738
      46)
    object cxButton1: TcxButton
      Left = 424
      Top = 6
      Width = 107
      Height = 33
      Action = aZapisi
      Anchors = [akTop, akRight]
      TabOrder = 0
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object cxButton2: TcxButton
      Left = 585
      Top = 6
      Width = 138
      Height = 33
      Action = aOtkazi
      Anchors = [akTop, akRight]
      Caption = #1054#1090#1082#1072#1078#1080'/'#1048#1079#1083#1077#1079
      TabOrder = 1
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
  end
  object tblIzberiSD: TpFIBDataSet
    RefreshSQL.Strings = (
      'select ARTVID, ID, NAZIV'
      'from MTR_ARTIKAL'
      'where MTR_ARTIKAL.ARTVID = (select cast(S.V2 as integer)'
      '                            from MAN_SETUP S'
      '                            where S.OPIS = '#39'sostaven_del'#39' and'
      '                                  S.V1 = '#39'vid'#39') and'
      '      MTR_ARTIKAL.ID not in (select SS.SOS_DEL'
      '                             from MAN_SOS_DEL SS'
      '                             where (SS.LOT like :LOT))')
    SelectSQL.Strings = (
      'select ARTVID, ID, NAZIV'
      'from MTR_ARTIKAL'
      'where MTR_ARTIKAL.ARTVID = (select cast(S.V2 as integer)'
      '                            from MAN_SETUP S'
      '                            where S.OPIS = '#39'sostaven_del'#39' and'
      '                                  S.V1 = '#39'vid'#39') and'
      '      MTR_ARTIKAL.ID not in (select SS.SOS_DEL'
      '                             from MAN_SOS_DEL SS'
      '                             where (SS.LOT like :LOT))')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    Left = 80
    Top = 16
    object tblIzberiSDARTVID: TFIBIntegerField
      FieldName = 'ARTVID'
    end
    object tblIzberiSDID: TFIBIntegerField
      FieldName = 'ID'
    end
    object tblIzberiSDNAZIV: TFIBStringField
      FieldName = 'NAZIV'
      Size = 500
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object dsIzberiSD: TDataSource
    DataSet = tblIzberiSD
    Left = 48
    Top = 16
  end
  object ActionList1: TActionList
    Images = dmRes.cxSmallImages
    Left = 16
    Top = 16
    object aZapisi: TAction
      Caption = #1047#1072#1087#1080#1096#1080
      ImageIndex = 6
      ShortCut = 120
      OnExecute = aZapisiExecute
    end
    object aOtkazi: TAction
      Caption = 'aOtkazi'
      ImageIndex = 20
      OnExecute = aOtkaziExecute
    end
    object aGenerirajEtiketi: TAction
      Caption = 'aGenerirajEtiketi'
      OnExecute = aGenerirajEtiketiExecute
    end
    object aGenerirajEtiketiStavki: TAction
      Caption = 'aGenerirajEtiketiStavki'
      OnExecute = aGenerirajEtiketiStavkiExecute
    end
    object aPresmetajSD: TAction
      Caption = 'aPresmetajSD'
    end
    object aBrisiSD: TAction
      Caption = 'aBrisiSD'
      ShortCut = 119
      OnExecute = aBrisiSDExecute
    end
    object aGE: TAction
      Caption = 'aGE'
      ShortCut = 116
      OnExecute = aGEExecute
    end
  end
  object tblSosDelLot: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE MAN_SOS_DEL'
      'SET '
      '    VID_SOS_DEL = :VID_SOS_DEL,'
      '    SOS_DEL = :SOS_DEL,'
      '    LOT = :LOT'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    MAN_SOS_DEL'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO MAN_SOS_DEL('
      '    ID,'
      '    VID_SOS_DEL,'
      '    SOS_DEL,'
      '    LOT'
      ')'
      'VALUES('
      '    :ID,'
      '    :VID_SOS_DEL,'
      '    :SOS_DEL,'
      '    :LOT'
      ')')
    RefreshSQL.Strings = (
      'select S.ID, S.VID_SOS_DEL, S.SOS_DEL, A.NAZIV, S.LOT, ES.BARKOD'
      'from MAN_SOS_DEL S'
      'inner join MTR_ARTIKAL A on A.ARTVID = S.VID_SOS_DEL and'
      '      A.ID = S.SOS_DEL'
      'left join MAN_ETIKETA_STAVKA ES on ES.LOT = S.LOT and'
      '      ES.ID_SOS_DEL = S.SOS_DEL'
      'where(  S.LOT = :LOT'
      '     ) and (     S.ID = :OLD_ID'
      '     )'
      '    '
      '  ')
    SelectSQL.Strings = (
      'select S.ID, S.VID_SOS_DEL, S.SOS_DEL, A.NAZIV, S.LOT, ES.BARKOD'
      'from MAN_SOS_DEL S'
      'inner join MTR_ARTIKAL A on A.ARTVID = S.VID_SOS_DEL and'
      '      A.ID = S.SOS_DEL'
      'left join MAN_ETIKETA_STAVKA ES on ES.LOT = S.LOT and'
      '      ES.ID_SOS_DEL = S.SOS_DEL'
      'where S.LOT = :LOT'
      '  ')
    BeforeDelete = tblSosDelLotBeforeDelete
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    Left = 424
    Top = 104
    oVisibleRecno = True
    object tblSosDelLotID: TFIBIntegerField
      FieldName = 'ID'
    end
    object tblSosDelLotVID_SOS_DEL: TFIBIntegerField
      FieldName = 'VID_SOS_DEL'
    end
    object tblSosDelLotSOS_DEL: TFIBIntegerField
      FieldName = 'SOS_DEL'
    end
    object tblSosDelLotNAZIV: TFIBStringField
      FieldName = 'NAZIV'
      Size = 500
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblSosDelLotLOT: TFIBStringField
      FieldName = 'LOT'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblSosDelLotBARKOD: TFIBStringField
      FieldName = 'BARKOD'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object dsSosDelLot: TDataSource
    DataSet = tblSosDelLot
    Left = 496
    Top = 120
  end
end
