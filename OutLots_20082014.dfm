object frmOutLots: TfrmOutLots
  Left = 128
  Top = 212
  ActiveControl = cxGrid1
  BorderIcons = [biSystemMenu, biMaximize]
  Caption = 'LOT '#1073#1088#1086#1077#1074#1080' '#1080' '#1082#1086#1083#1080#1095#1080#1085#1080' '#1079#1072' '#1072#1088#1090#1080#1082#1083#1086#1090
  ClientHeight = 553
  ClientWidth = 614
  Color = clBtnFace
  Constraints.MinHeight = 500
  Constraints.MinWidth = 610
  Font.Charset = RUSSIAN_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  ShowHint = True
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object lPanel: TPanel
    Left = 0
    Top = 126
    Width = 614
    Height = 298
    Align = alClient
    BevelInner = bvLowered
    BevelOuter = bvSpace
    TabOrder = 0
    object cxGrid1: TcxGrid
      Left = 2
      Top = 2
      Width = 610
      Height = 294
      Align = alClient
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      object cxGrid1DBTableView1: TcxGridDBTableView
        OnKeyPress = cxGrid1DBTableView1KeyPress
        OnFilterCustomization = cxGrid1DBTableView1FilterCustomization
        DataController.DataSource = dsUpotrebeni
        DataController.Filter.Options = [fcoCaseInsensitive]
        DataController.Options = [dcoAnsiSort, dcoAssignGroupingValues, dcoAssignMasterDetailKeys, dcoSaveExpanding]
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <
          item
            Kind = skCount
            Column = cxGrid1DBTableView1ID
          end>
        DataController.Summary.SummaryGroups = <>
        Filtering.ColumnFilteredItemsList = True
        FilterRow.InfoText = #1050#1083#1080#1082#1085#1080' '#1086#1074#1076#1077' '#1079#1072' '#1076#1072' '#1076#1077#1092#1080#1085#1080#1088#1072#1096' '#1092#1080#1083#1090#1077#1088
        FilterRow.Visible = True
        FilterRow.ApplyChanges = fracImmediately
        OptionsBehavior.CellHints = True
        OptionsBehavior.IncSearch = True
        OptionsCustomize.ColumnsQuickCustomization = True
        OptionsData.Deleting = False
        OptionsData.Editing = False
        OptionsData.Inserting = False
        OptionsSelection.HideFocusRectOnExit = False
        OptionsView.Footer = True
        OptionsView.GroupFooters = gfVisibleWhenExpanded
        object cxGrid1DBTableView1ID: TcxGridDBColumn
          DataBinding.FieldName = 'ID'
        end
        object cxGrid1DBTableView1OUT_S_ID: TcxGridDBColumn
          DataBinding.FieldName = 'OUT_S_ID'
          Visible = False
          Width = 75
        end
        object cxGrid1DBTableView1LOT_NO: TcxGridDBColumn
          DataBinding.FieldName = 'LOT_NO'
          Width = 230
        end
        object cxGrid1DBTableView1KOLICINA: TcxGridDBColumn
          DataBinding.FieldName = 'KOLICINA'
          Width = 114
        end
        object cxGrid1DBTableView1VAZI_DO: TcxGridDBColumn
          DataBinding.FieldName = 'VAZI_DO'
          Width = 124
        end
        object cxGrid1DBTableView1CUSTOM1: TcxGridDBColumn
          DataBinding.FieldName = 'CUSTOM1'
          Visible = False
        end
        object cxGrid1DBTableView1CUSTOM2: TcxGridDBColumn
          DataBinding.FieldName = 'CUSTOM2'
          Visible = False
        end
        object cxGrid1DBTableView1CUSTOM3: TcxGridDBColumn
          DataBinding.FieldName = 'CUSTOM3'
          Visible = False
        end
        object cxGrid1DBTableView1TS_INS: TcxGridDBColumn
          DataBinding.FieldName = 'TS_INS'
          Visible = False
        end
        object cxGrid1DBTableView1TS_UPD: TcxGridDBColumn
          DataBinding.FieldName = 'TS_UPD'
          Visible = False
        end
        object cxGrid1DBTableView1USR_INS: TcxGridDBColumn
          DataBinding.FieldName = 'USR_INS'
          Visible = False
        end
        object cxGrid1DBTableView1USR_UPD: TcxGridDBColumn
          DataBinding.FieldName = 'USR_UPD'
          Visible = False
        end
      end
      object cxGrid1Level1: TcxGridLevel
        GridView = cxGrid1DBTableView1
      end
    end
  end
  object dPanel: TPanel
    Left = 0
    Top = 424
    Width = 614
    Height = 106
    Align = alBottom
    BevelInner = bvLowered
    BevelOuter = bvSpace
    Color = 15790320
    Enabled = False
    ParentBackground = False
    PopupMenu = PopupMenu1
    TabOrder = 1
    DesignSize = (
      614
      106)
    object Label1: TLabel
      Left = 22
      Top = 21
      Width = 55
      Height = 13
      Caption = 'LOT '#1073#1088#1086#1112' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label2: TLabel
      Left = 17
      Top = 48
      Width = 60
      Height = 13
      Caption = #1050#1086#1083#1080#1095#1080#1085#1072' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label3: TLabel
      Left = 21
      Top = 75
      Width = 56
      Height = 13
      Caption = #1042#1072#1078#1080' '#1076#1086' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clBlue
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Sifra: TcxDBTextEdit
      Tag = 1
      Left = 83
      Top = 18
      BeepOnEnter = False
      DataBinding.DataField = 'LOT_NO'
      DataBinding.DataSource = dsUpotrebeni
      ParentFont = False
      Properties.BeepOnError = True
      Style.Shadow = False
      TabOrder = 0
      OnEnter = SifraEnter
      OnExit = SifraExit
      OnKeyDown = EnterKakoTab
      Width = 318
    end
    object OtkaziButton: TcxButton
      Left = 523
      Top = 66
      Width = 75
      Height = 25
      Action = aOtkazi
      Anchors = [akRight, akBottom]
      Colors.Pressed = clGradientActiveCaption
      TabOrder = 4
    end
    object ZapisiButton: TcxButton
      Left = 442
      Top = 66
      Width = 75
      Height = 25
      Action = aZapisi
      Anchors = [akRight, akBottom]
      Colors.Pressed = clGradientActiveCaption
      TabOrder = 3
    end
    object Kolicina: TcxDBTextEdit
      Tag = 1
      Left = 83
      Top = 45
      BeepOnEnter = False
      DataBinding.DataField = 'KOLICINA'
      DataBinding.DataSource = dsUpotrebeni
      ParentFont = False
      Properties.BeepOnError = True
      Style.Shadow = False
      TabOrder = 1
      OnKeyDown = EnterKakoTab
      Width = 121
    end
    object DatumK: TcxDBDateEdit
      Left = 83
      Top = 72
      DataBinding.DataField = 'VAZI_DO'
      DataBinding.DataSource = dsUpotrebeni
      TabOrder = 2
      OnKeyDown = EnterKakoTab
      Width = 121
    end
  end
  object dxRibbon1: TdxRibbon
    Left = 0
    Top = 0
    Width = 614
    Height = 126
    BarManager = dxBarManager1
    ColorSchemeName = 'Blue'
    Contexts = <>
    TabOrder = 2
    TabStop = False
    object dxRibbon1Tab1: TdxRibbonTab
      Active = True
      Caption = #1052#1077#1085#1080
      Groups = <
        item
          Caption = 'LOT '#1073#1088#1086#1112
          ToolbarName = 'dxBarManager1Bar1'
        end
        item
          ToolbarName = 'dxBarManager1Bar2'
        end
        item
          ToolbarName = 'dxBarManager1Bar3'
        end>
      Index = 0
    end
    object dxRibbon1Tab2: TdxRibbonTab
      Caption = #1055#1086#1076#1077#1089#1091#1074#1072#1114#1072
      Groups = <
        item
          ToolbarName = 'dxBarManager1Bar4'
        end
        item
          ToolbarName = 'dxBarManager1BarTabela'
        end>
      Index = 1
    end
  end
  object StatusBar1: TdxRibbonStatusBar
    Left = 0
    Top = 530
    Width = 614
    Height = 23
    Panels = <
      item
        PanelStyleClassName = 'TdxStatusBarTextPanelStyle'
        Text = 
          ' F5 - '#1053#1086#1074', F6 - '#1040#1078#1091#1088#1080#1088#1072#1112', F7 - '#1054#1089#1074#1077#1078#1080', F8 - '#1041#1088#1080#1096#1080', F9 - '#1047#1072#1087#1080#1096#1080', ' +
          'Esc - '#1054#1090#1082#1072#1078#1080' / '#1048#1079#1083#1077#1079
      end>
    Ribbon = dxRibbon1
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clDefault
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
  end
  object cxGridPopupMenu1: TcxGridPopupMenu
    Grid = cxGrid1
    PopupMenus = <>
    Left = 248
    Top = 176
  end
  object PopupMenu1: TPopupMenu
    Left = 360
    Top = 192
    object N1: TMenuItem
      Caption = #1057#1085#1080#1084#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      ShortCut = 49235
      OnClick = SaveToIniFileExecute
    end
    object Excel1: TMenuItem
      Caption = #1057#1085#1080#1084#1080' '#1075#1086' '#1074#1086' Excel'
      ShortCut = 49221
    end
  end
  object dxBarManager1: TdxBarManager
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Segoe UI'
    Font.Style = []
    Categories.Strings = (
      'Default')
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    ImageOptions.Images = dmRes.cxSmallImages
    ImageOptions.LargeImages = dmRes.cxLargeImages
    PopupMenuLinks = <>
    UseSystemFont = True
    Left = 432
    Top = 208
    DockControlHeights = (
      0
      0
      0
      0)
    object dxBarManager1Bar1: TdxBar
      Caption = 'Custom 1'
      CaptionButtons = <>
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 715
      FloatTop = 157
      FloatClientWidth = 51
      FloatClientHeight = 120
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton10'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarLargeButton1'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton2'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarLargeButton3'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar2: TdxBar
      Caption = #1040#1082#1094#1080#1080
      CaptionButtons = <>
      DockedLeft = 249
      DockedTop = 0
      FloatLeft = 789
      FloatTop = 120
      FloatClientWidth = 51
      FloatClientHeight = 120
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton5'
        end
        item
          Visible = True
          ItemName = 'dxBarSubItem1'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar3: TdxBar
      Caption = #1048#1079#1083#1077#1079
      CaptionButtons = <>
      DockedLeft = 387
      DockedTop = 0
      FloatLeft = 718
      FloatTop = 62
      FloatClientWidth = 51
      FloatClientHeight = 80
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton7'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton8'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar4: TdxBar
      Caption = #1055#1077#1095#1072#1090#1077#1114#1077
      CaptionButtons = <>
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 676
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton12'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton15'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton9'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarLargeButton13'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1BarTabela: TdxBar
      Caption = #1058#1072#1073#1077#1083#1072
      CaptionButtons = <>
      DockedLeft = 363
      DockedTop = 0
      FloatLeft = 767
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLBtnSnimiIzgled'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarLBtnBrisiIzgled'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarLargeButton2: TdxBarLargeButton
      Action = aAzuriraj
      Category = 0
    end
    object dxBarLargeButton3: TdxBarLargeButton
      Action = aBrisi
      Category = 0
    end
    object dxBarLargeButton4: TdxBarLargeButton
      Action = aSnimiIzgled
      Category = 0
      ScreenTip = tipSnimiIzgled
    end
    object dxBarLargeButton5: TdxBarLargeButton
      Action = aZacuvajExcel
      Category = 0
      ScreenTip = tipZacuvajExcel
    end
    object dxBarLargeButton6: TdxBarLargeButton
      Action = aPecatiTabela
      Caption = #1055#1077#1095#1072#1090#1080' '#1090#1072#1073#1077#1083#1072
      Category = 0
    end
    object dxBarLargeButton7: TdxBarLargeButton
      Action = aHelp
      Category = 0
    end
    object dxBarLargeButton8: TdxBarLargeButton
      Action = aIzlez
      Category = 0
    end
    object dxBarLargeButton1: TdxBarLargeButton
      Action = aNov
      Category = 0
    end
    object dxBarLargeButton9: TdxBarLargeButton
      Action = aSnimiPecatenje
      Category = 0
      ScreenTip = tipSnimiKonf
    end
    object ribbonFilterReset: TdxBarLargeButton
      Caption = #1056#1077#1089#1077#1090#1080#1088#1072#1112' '#1092#1080#1083#1090#1077#1088
      Category = 0
      Hint = #1056#1077#1089#1077#1090#1080#1088#1072#1112' '#1092#1080#1083#1090#1077#1088
      Visible = ivAlways
    end
    object dxBarLargeButton12: TdxBarLargeButton
      Action = aPodesuvanjePecatenje
      Category = 0
      ScreenTip = tipDizajnReport
    end
    object dxBarLargeButton13: TdxBarLargeButton
      Action = aBrisiPodesuvanjePecatenje
      Category = 0
      ScreenTip = tipBrisiKonf
    end
    object dxBarLargeButton14: TdxBarLargeButton
      Caption = #1048#1079#1073#1088#1080#1096#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      Category = 0
      Visible = ivAlways
      LargeImageIndex = 40
    end
    object dxBarLargeButton15: TdxBarLargeButton
      Action = aPageSetup
      Category = 0
      ScreenTip = tipSetiranjeStrana
    end
    object dxBarSubItem1: TdxBarSubItem
      Caption = #1055#1077#1095#1072#1090#1077#1114#1077
      Category = 0
      Visible = ivAlways
      ImageIndex = 30
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton6'
        end>
    end
    object cxBarEditItem1: TcxBarEditItem
      Caption = 'Skin :'
      Category = 0
      Hint = 'Skin :'
      Visible = ivAlways
      Width = 160
      PropertiesClassName = 'TcxComboBoxProperties'
      Properties.OnChange = cxBarEditItem1PropertiesChange
    end
    object dxBarLBtnSnimiIzgled: TdxBarLargeButton
      Action = aSnimiIzgled
      Category = 0
    end
    object dxBarLBtnBrisiIzgled: TdxBarLargeButton
      Action = aBrisiIzgled
      Category = 0
    end
    object dxBarLargeButtonSoberi: TdxBarLargeButton
      Action = aSpustiSoberi
      Category = 0
    end
    object dxBarLargeButton10: TdxBarLargeButton
      Action = aOdberi
      Category = 0
    end
  end
  object ActionList1: TActionList
    Images = dmRes.cxSmallImages
    Left = 144
    Top = 184
    object aNov: TAction
      Caption = #1053#1086#1074
      ImageIndex = 10
      ShortCut = 116
      OnExecute = aNovExecute
    end
    object aAzuriraj: TAction
      Caption = #1040#1078#1091#1088#1080#1088#1072#1112
      ImageIndex = 12
      ShortCut = 117
      OnExecute = aAzurirajExecute
    end
    object aBrisi: TAction
      Caption = #1041#1088#1080#1096#1080
      ImageIndex = 11
      ShortCut = 119
      OnExecute = aBrisiExecute
    end
    object aSnimiIzgled: TAction
      Caption = #1057#1085#1080#1084#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      ImageIndex = 9
      ShortCut = 24659
      OnExecute = aSnimiIzgledExecute
    end
    object aZacuvajExcel: TAction
      Caption = #1047#1072#1095#1091#1074#1072#1112' '#1074#1086' Excel'
      ImageIndex = 9
      ShortCut = 24645
      OnExecute = aZacuvajExcelExecute
    end
    object aPecatiTabela: TAction
      Caption = #1055#1077#1095#1072#1090#1080
      ImageIndex = 30
      OnExecute = aPecatiTabelaExecute
    end
    object aHelp: TAction
      Caption = #1055#1086#1084#1086#1096
      ImageIndex = 24
    end
    object aIzlez: TAction
      Caption = #1048#1079#1083#1077#1079
      ImageIndex = 25
      OnExecute = aIzlezExecute
    end
    object aZapisi: TAction
      Caption = #1047#1072#1087#1080#1096#1080
      ImageIndex = 7
      ShortCut = 120
      OnExecute = aZapisiExecute
    end
    object aOtkazi: TAction
      Caption = #1054#1090#1082#1072#1078#1080
      ImageIndex = 21
      ShortCut = 27
      OnExecute = aOtkaziExecute
    end
    object aRefresh: TAction
      Caption = 'aRefresh'
      ImageIndex = 18
      ShortCut = 118
      OnExecute = aRefreshExecute
    end
    object aSnimiPecatenje: TAction
      Caption = #1057#1085#1080#1084#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      ImageIndex = 39
      OnExecute = aSnimiPecatenjeExecute
    end
    object aPodesuvanjePecatenje: TAction
      Caption = #1044#1080#1079#1072#1112#1085' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090
      ImageIndex = 38
      OnExecute = aPodesuvanjePecatenjeExecute
    end
    object aBrisiPodesuvanjePecatenje: TAction
      Caption = #1041#1088#1080#1096#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      ImageIndex = 37
      OnExecute = aBrisiPodesuvanjePecatenjeExecute
    end
    object aPageSetup: TAction
      Caption = #1057#1077#1090#1080#1088#1072#1114#1077' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072
      ImageIndex = 38
      OnExecute = aPageSetupExecute
    end
    object aFormConfig: TAction
      Caption = 'aFormConfig'
      ShortCut = 24699
      OnExecute = aFormConfigExecute
    end
    object aBrisiIzgled: TAction
      Caption = #1041#1088#1080#1096#1080' '#1075#1086' '#1079#1072#1095#1091#1074#1072#1085#1080#1086#1090' '#1080#1079#1075#1083#1077#1076
      ImageIndex = 56
      ShortCut = 24644
      OnExecute = aBrisiIzgledExecute
    end
    object aSpustiSoberi: TAction
      Caption = #1057#1087#1091#1096#1090#1080'/'#1057#1086#1073#1077#1088#1080' '#1058#1072#1073#1077#1083#1072
      ImageIndex = 65
      OnExecute = aSpustiSoberiExecute
    end
    object aOdberi: TAction
      Caption = #1054#1076#1073#1077#1088#1080' '#1086#1076' '#1074#1083#1077#1079
      ImageIndex = 16
      OnExecute = aOdberiExecute
    end
  end
  object dxComponentPrinter1: TdxComponentPrinter
    CurrentLink = dxComponentPrinter1Link1
    Version = 0
    Left = 64
    Top = 248
    object dxComponentPrinter1Link1: TdxGridReportLink
      Active = True
      Component = cxGrid1
      PrinterPage.DMPaper = 9
      PrinterPage.Footer = 6350
      PrinterPage.GrayShading = True
      PrinterPage.Header = 6350
      PrinterPage.Margins.Bottom = 12700
      PrinterPage.Margins.Left = 12700
      PrinterPage.Margins.Right = 12700
      PrinterPage.Margins.Top = 12700
      PrinterPage.Orientation = poLandscape
      PrinterPage.PageFooter.RightTitle.Strings = (
        #1057#1090#1088'.[Page #] '#1086#1076' [Total Pages]')
      PrinterPage.PageHeader.RightTitle.Strings = (
        '')
      PrinterPage.PageSize.X = 210000
      PrinterPage.PageSize.Y = 297000
      PrinterPage._dxMeasurementUnits_ = 0
      PrinterPage._dxLastMU_ = 2
      ReportDocument.CreationDate = 41435.907315289350000000
      OptionsFormatting.UseNativeStyles = True
      OptionsOnEveryPage.FilterBar = False
      OptionsView.Caption = False
      OptionsView.FilterBar = False
      StyleRepository = dmRes.cxStyleRepositoryPrinting
      Styles.StyleSheet = dmRes.dxGridReportLinkStyleSheet
      BuiltInReportLink = True
    end
  end
  object dxBarScreenTipRepository1: TdxBarScreenTipRepository
    Left = 544
    Top = 184
    object tipSnimiIzgled: TdxBarScreenTip
      Header.Text = #1057#1085#1080#1084#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      Description.Text = #1047#1072#1095#1091#1074#1072#1112' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090' '#1085#1072' '#1092#1086#1088#1084#1072#1090#1072' '#1080' '#1085#1072' '#1090#1072#1073#1077#1083#1072#1090#1072
    end
    object tipZacuvajExcel: TdxBarScreenTip
      Header.Text = #1047#1072#1095#1091#1074#1072#1112' '#1074#1086' Excel'
      Description.Text = #1057#1085#1080#1084#1080' '#1075#1080' '#1087#1086#1076#1072#1090#1086#1094#1080#1090#1077' '#1086#1076' '#1090#1072#1073#1077#1083#1072#1090#1072' '#1074#1086' Excel '#1092#1086#1088#1084#1072#1090
    end
    object tipDizajnReport: TdxBarScreenTip
      Header.Text = #1044#1080#1079#1072#1112#1085' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090
      Description.Text = #1055#1086#1076#1077#1089#1091#1074#1072#1114#1072' '#1085#1072' '#1080#1079#1075#1083#1077#1076#1086#1090' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090' '#1085#1072' '#1090#1072#1073#1077#1083#1072#1090#1072
    end
    object tipSetiranjeStrana: TdxBarScreenTip
      Header.Text = #1057#1077#1090#1080#1088#1072#1114#1077' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072
      Description.Text = #1055#1086#1076#1077#1089#1091#1074#1072#1114#1077' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072' '#1085#1072' '#1082#1086#1112#1072' '#1116#1077' '#1089#1077' '#1087#1077#1095#1072#1090#1080' '#1090#1072#1073#1077#1083#1072#1090#1072
    end
    object tipSnimiKonf: TdxBarScreenTip
      Header.Text = #1057#1085#1080#1084#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      Description.Text = #1047#1072#1095#1091#1074#1072#1112' '#1075#1080' '#1087#1086#1076#1077#1089#1091#1074#1072#1114#1072#1090#1072' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090' '#1080' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072' '#1079#1072' '#1087#1077#1095#1072#1090#1077#1114#1077
    end
    object tipBrisiKonf: TdxBarScreenTip
      Header.Text = #1041#1088#1080#1096#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      Description.Text = #1048#1079#1073#1088#1080#1096#1080' '#1075#1080' '#1087#1086#1076#1077#1089#1091#1074#1072#1114#1072#1090#1072' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090' '#1080' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072' '#1079#1072' '#1087#1077#1095#1072#1090#1077#1114#1077
    end
  end
  object dsUpotrebeni: TDataSource
    DataSet = tblUpotrebeni
    Left = 176
    Top = 272
  end
  object tblUpotrebeni: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE MTR_OUT_S_LOT'
      'SET '
      '    ID = :ID,'
      '    OUT_S_ID = :OUT_S_ID,'
      '    LOT_NO = :LOT_NO,'
      '    KOLICINA = :KOLICINA,'
      '    VAZI_DO = :VAZI_DO,'
      '    CUSTOM1 = :CUSTOM1,'
      '    CUSTOM2 = :CUSTOM2,'
      '    CUSTOM3 = :CUSTOM3,'
      '    TS_INS = :TS_INS,'
      '    TS_UPD = :TS_UPD,'
      '    USR_INS = :USR_INS,'
      '    USR_UPD = :USR_UPD'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    MTR_OUT_S_LOT'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO MTR_OUT_S_LOT('
      '    ID,'
      '    OUT_S_ID,'
      '    LOT_NO,'
      '    KOLICINA,'
      '    VAZI_DO,'
      '    CUSTOM1,'
      '    CUSTOM2,'
      '    CUSTOM3,'
      '    TS_INS,'
      '    TS_UPD,'
      '    USR_INS,'
      '    USR_UPD'
      ')'
      'VALUES('
      '    :ID,'
      '    :OUT_S_ID,'
      '    :LOT_NO,'
      '    :KOLICINA,'
      '    :VAZI_DO,'
      '    :CUSTOM1,'
      '    :CUSTOM2,'
      '    :CUSTOM3,'
      '    :TS_INS,'
      '    :TS_UPD,'
      '    :USR_INS,'
      '    :USR_UPD'
      ')')
    RefreshSQL.Strings = (
      'select SL.ID,'
      '       SL.OUT_S_ID,'
      '       SL.LOT_NO,'
      '       SL.KOLICINA,'
      '       SL.VAZI_DO,'
      '       SL.CUSTOM1,'
      '       SL.CUSTOM2,'
      '       SL.CUSTOM3,'
      '       SL.TS_INS,'
      '       SL.TS_UPD,'
      '       SL.USR_INS,'
      '       SL.USR_UPD'
      'from MTR_OUT_S_LOT SL'
      'where(  SL.OUT_S_ID = :IN_ID'
      '     ) and (     SL.ID = :OLD_ID'
      '     )'
      '      ')
    SelectSQL.Strings = (
      'select SL.ID,'
      '       SL.OUT_S_ID,'
      '       SL.LOT_NO,'
      '       SL.KOLICINA,'
      '       SL.VAZI_DO,'
      '       SL.CUSTOM1,'
      '       SL.CUSTOM2,'
      '       SL.CUSTOM3,'
      '       SL.TS_INS,'
      '       SL.TS_UPD,'
      '       SL.USR_INS,'
      '       SL.USR_UPD'
      'from MTR_OUT_S_LOT SL'
      'where SL.OUT_S_ID = :IN_ID  ')
    AfterInsert = tblUpotrebeniAfterInsert
    BeforeDelete = TabelaBeforeDelete
    BeforeOpen = tblUpotrebeniBeforeOpen
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 256
    Top = 272
    poSQLINT64ToBCD = True
    object tblUpotrebeniID: TFIBBCDField
      DisplayLabel = #1064#1080#1092'.'
      FieldName = 'ID'
      Size = 0
      RoundByScale = True
    end
    object tblUpotrebeniOUT_S_ID: TFIBBCDField
      DisplayLabel = #1057#1090#1072#1074#1082#1072' '#1096#1080#1092'.'
      FieldName = 'OUT_S_ID'
      Size = 0
      RoundByScale = True
    end
    object tblUpotrebeniLOT_NO: TFIBStringField
      DisplayLabel = 'LOT '#1073#1088'.'
      FieldName = 'LOT_NO'
      Size = 30
      EmptyStrToNull = True
    end
    object tblUpotrebeniKOLICINA: TFIBFloatField
      DisplayLabel = #1050#1086#1083#1080#1095#1080#1085#1072
      FieldName = 'KOLICINA'
      DisplayFormat = '###,###0.000'
      EditFormat = '0.000'
    end
    object tblUpotrebeniVAZI_DO: TFIBDateField
      DisplayLabel = #1042#1072#1078#1080' '#1076#1086
      FieldName = 'VAZI_DO'
    end
    object tblUpotrebeniCUSTOM1: TFIBStringField
      FieldName = 'CUSTOM1'
      Size = 100
      EmptyStrToNull = True
    end
    object tblUpotrebeniCUSTOM2: TFIBStringField
      FieldName = 'CUSTOM2'
      Size = 100
      EmptyStrToNull = True
    end
    object tblUpotrebeniCUSTOM3: TFIBStringField
      FieldName = 'CUSTOM3'
      Size = 100
      EmptyStrToNull = True
    end
    object tblUpotrebeniTS_INS: TFIBDateTimeField
      FieldName = 'TS_INS'
    end
    object tblUpotrebeniTS_UPD: TFIBDateTimeField
      FieldName = 'TS_UPD'
    end
    object tblUpotrebeniUSR_INS: TFIBStringField
      FieldName = 'USR_INS'
      Size = 50
      EmptyStrToNull = True
    end
    object tblUpotrebeniUSR_UPD: TFIBStringField
      FieldName = 'USR_UPD'
      Size = 50
      EmptyStrToNull = True
    end
  end
  object tblOutStavka: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE MTR_OUT_S'
      'SET '
      '    ID = :ID,'
      '    MTR_OUT_ID = :MTR_OUT_ID,'
      '    ART_VID = :ART_VID,'
      '    ART_SIF = :ART_SIF,'
      '    KOLICINA = :KOLICINA,'
      '    SLEDLIVOST_KOLICINA = :SLEDLIVOST_KOLICINA,'
      '    PAKETI = :PAKETI,'
      '    IZV_EM = :IZV_EM,'
      '    BR_PAKET = :BR_PAKET,'
      '    CENA_SO_DDV = :CENA_SO_DDV,'
      '    CENA_BEZ_DDV = :CENA_BEZ_DDV,'
      '    DANOK_PR = :DANOK_PR,'
      '    RABAT_PR = :RABAT_PR,'
      '    DANOK_IZNOS = :DANOK_IZNOS,'
      '    RABAT_IZNOS = :RABAT_IZNOS,'
      '    IZNOS_SO_DDV = :IZNOS_SO_DDV,'
      '    IZNOS_BEZ_DDV = :IZNOS_BEZ_DDV,'
      '    KNIG_CENA = :KNIG_CENA,'
      '    KNIG_IZNOS = :KNIG_IZNOS,'
      '    CENA_BEZ_RABAT_SO_DDV = :CENA_BEZ_RABAT_SO_DDV,'
      '    CENA_BEZ_RABAT_BEZ_DDV = :CENA_BEZ_RABAT_BEZ_DDV,'
      '    VAZI_DO = :VAZI_DO,'
      '    LOT_ID = :LOT_ID,'
      '    LOT_NO = :LOT_NO,'
      '    REF_ID = :REF_ID,'
      '    OPIS = :OPIS,'
      '    CUSTOM1 = :CUSTOM1,'
      '    CUSTOM2 = :CUSTOM2,'
      '    CUSTOM3 = :CUSTOM3,'
      '    TS_INS = :TS_INS,'
      '    TS_UPD = :TS_UPD,'
      '    USR_INS = :USR_INS,'
      '    USR_UPD = :USR_UPD'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    MTR_OUT_S'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO MTR_OUT_S('
      '    ID,'
      '    MTR_OUT_ID,'
      '    ART_VID,'
      '    ART_SIF,'
      '    KOLICINA,'
      '    SLEDLIVOST_KOLICINA,'
      '    PAKETI,'
      '    IZV_EM,'
      '    BR_PAKET,'
      '    CENA_SO_DDV,'
      '    CENA_BEZ_DDV,'
      '    DANOK_PR,'
      '    RABAT_PR,'
      '    DANOK_IZNOS,'
      '    RABAT_IZNOS,'
      '    IZNOS_SO_DDV,'
      '    IZNOS_BEZ_DDV,'
      '    KNIG_CENA,'
      '    KNIG_IZNOS,'
      '    CENA_BEZ_RABAT_SO_DDV,'
      '    CENA_BEZ_RABAT_BEZ_DDV,'
      '    VAZI_DO,'
      '    LOT_ID,'
      '    LOT_NO,'
      '    REF_ID,'
      '    OPIS,'
      '    CUSTOM1,'
      '    CUSTOM2,'
      '    CUSTOM3,'
      '    TS_INS,'
      '    TS_UPD,'
      '    USR_INS,'
      '    USR_UPD'
      ')'
      'VALUES('
      '    :ID,'
      '    :MTR_OUT_ID,'
      '    :ART_VID,'
      '    :ART_SIF,'
      '    :KOLICINA,'
      '    :SLEDLIVOST_KOLICINA,'
      '    :PAKETI,'
      '    :IZV_EM,'
      '    :BR_PAKET,'
      '    :CENA_SO_DDV,'
      '    :CENA_BEZ_DDV,'
      '    :DANOK_PR,'
      '    :RABAT_PR,'
      '    :DANOK_IZNOS,'
      '    :RABAT_IZNOS,'
      '    :IZNOS_SO_DDV,'
      '    :IZNOS_BEZ_DDV,'
      '    :KNIG_CENA,'
      '    :KNIG_IZNOS,'
      '    :CENA_BEZ_RABAT_SO_DDV,'
      '    :CENA_BEZ_RABAT_BEZ_DDV,'
      '    :VAZI_DO,'
      '    :LOT_ID,'
      '    :LOT_NO,'
      '    :REF_ID,'
      '    :OPIS,'
      '    :CUSTOM1,'
      '    :CUSTOM2,'
      '    :CUSTOM3,'
      '    :TS_INS,'
      '    :TS_UPD,'
      '    :USR_INS,'
      '    :USR_UPD'
      ')')
    RefreshSQL.Strings = (
      'select S.ID,'
      '       S.MTR_OUT_ID,'
      '       S.ART_VID,'
      '       S.ART_SIF,'
      '       A.NAZIV as ART_NAZIV,'
      '       S.KOLICINA,'
      '       S.SLEDLIVOST_KOLICINA,'
      '       A.MERKA as ART_MERKA,'
      '       S.PAKETI,'
      '       S.IZV_EM,'
      '       IE.IZVEDENA_EM,'
      '       S.BR_PAKET,'
      '       S.CENA_SO_DDV,'
      '       S.CENA_BEZ_DDV,'
      '       S.DANOK_PR,'
      '       S.RABAT_PR,'
      '       S.DANOK_IZNOS,'
      '       S.RABAT_IZNOS,'
      '       S.IZNOS_SO_DDV,'
      '       S.IZNOS_BEZ_DDV,'
      '       S.KNIG_CENA,'
      '       S.KNIG_IZNOS,'
      '       S.CENA_BEZ_RABAT_SO_DDV,'
      '       S.CENA_BEZ_RABAT_BEZ_DDV,'
      '       S.VAZI_DO,'
      '       S.LOT_ID,'
      '       S.LOT_NO,'
      '       S.REF_ID,'
      '       S.OPIS,'
      '       S.CUSTOM1,'
      '       S.CUSTOM2,'
      '       S.CUSTOM3,'
      '       S.TS_INS,'
      '       S.TS_UPD,'
      '       S.USR_INS,'
      '       S.USR_UPD'
      'from MTR_OUT_S S'
      'join MTR_ARTIKAL A on A.ARTVID = S.ART_VID and A.ID = S.ART_SIF'
      'left join MTR_IZVEDENA_EM IE on IE.ID = S.IZV_EM'
      'where(  S.MTR_OUT_ID = :MAS_ID'
      '     ) and (     S.ID = :OLD_ID'
      '     )'
      '       ')
    SelectSQL.Strings = (
      'select S.ID,'
      '       S.MTR_OUT_ID,'
      '       S.ART_VID,'
      '       S.ART_SIF,'
      '       A.NAZIV as ART_NAZIV,'
      '       S.KOLICINA,'
      '       S.SLEDLIVOST_KOLICINA,'
      '       A.MERKA as ART_MERKA,'
      '       S.PAKETI,'
      '       S.IZV_EM,'
      '       IE.IZVEDENA_EM,'
      '       S.BR_PAKET,'
      '       S.CENA_SO_DDV,'
      '       S.CENA_BEZ_DDV,'
      '       S.DANOK_PR,'
      '       S.RABAT_PR,'
      '       S.DANOK_IZNOS,'
      '       S.RABAT_IZNOS,'
      '       S.IZNOS_SO_DDV,'
      '       S.IZNOS_BEZ_DDV,'
      '       S.KNIG_CENA,'
      '       S.KNIG_IZNOS,'
      '       S.CENA_BEZ_RABAT_SO_DDV,'
      '       S.CENA_BEZ_RABAT_BEZ_DDV,'
      '       S.VAZI_DO,'
      '       S.LOT_ID,'
      '       S.LOT_NO,'
      '       S.REF_ID,'
      '       S.OPIS,'
      '       S.CUSTOM1,'
      '       S.CUSTOM2,'
      '       S.CUSTOM3,'
      '       S.TS_INS,'
      '       S.TS_UPD,'
      '       S.USR_INS,'
      '       S.USR_UPD'
      'from MTR_OUT_S S'
      'join MTR_ARTIKAL A on A.ARTVID = S.ART_VID and A.ID = S.ART_SIF'
      'left join MTR_IZVEDENA_EM IE on IE.ID = S.IZV_EM'
      'where S.MTR_OUT_ID = :MAS_ID   ')
    AutoUpdateOptions.UpdateTableName = 'MTR_OUT_S'
    AutoUpdateOptions.KeyFields = 'ID'
    AutoUpdateOptions.GeneratorName = 'GEN_MTR_OUT_S_ID'
    AutoUpdateOptions.WhenGetGenID = wgBeforePost
    BeforeDelete = TabelaBeforeDelete
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 472
    Top = 80
    poSQLINT64ToBCD = True
    object tblOutStavkaID: TFIBBCDField
      DisplayLabel = #1064#1080#1092'.'
      FieldName = 'ID'
      Size = 0
      RoundByScale = True
    end
    object tblOutStavkaMTR_OUT_ID: TFIBBCDField
      DisplayLabel = #1048#1089#1087'. '#1096#1080#1092'.'
      FieldName = 'MTR_OUT_ID'
      Size = 0
      RoundByScale = True
    end
    object tblOutStavkaART_VID: TFIBIntegerField
      DisplayLabel = #1040#1088#1090'. '#1074#1080#1076
      FieldName = 'ART_VID'
    end
    object tblOutStavkaART_SIF: TFIBIntegerField
      DisplayLabel = #1040#1088#1090'. '#1096#1080#1092'.'
      FieldName = 'ART_SIF'
    end
    object tblOutStavkaART_NAZIV: TFIBStringField
      DisplayLabel = #1040#1088#1090'. '#1085#1072#1079#1080#1074
      FieldName = 'ART_NAZIV'
      Size = 500
      EmptyStrToNull = True
    end
    object tblOutStavkaKOLICINA: TFIBFloatField
      DisplayLabel = #1050#1086#1083#1080#1095#1080#1085#1072
      FieldName = 'KOLICINA'
      DisplayFormat = '###,###0.000'
      EditFormat = '0.000'
    end
    object tblOutStavkaPAKETI: TFIBIntegerField
      DisplayLabel = #1055#1072#1082#1077#1090#1080
      FieldName = 'PAKETI'
    end
    object tblOutStavkaIZV_EM: TFIBIntegerField
      DisplayLabel = #1048'.'#1077#1076'.'#1084#1077#1088#1082#1072' '#1096#1080#1092'.'
      FieldName = 'IZV_EM'
    end
    object tblOutStavkaIZVEDENA_EM: TFIBStringField
      DisplayLabel = #1048#1079#1074#1077#1076#1077#1085#1072' '#1077#1076'. '#1084#1077#1088#1082#1072
      FieldName = 'IZVEDENA_EM'
      EmptyStrToNull = True
    end
    object tblOutStavkaBR_PAKET: TFIBBCDField
      DisplayLabel = #1041#1088'. '#1074#1086' '#1087#1072#1082#1077#1090
      FieldName = 'BR_PAKET'
      Size = 4
      RoundByScale = True
    end
    object tblOutStavkaCENA_SO_DDV: TFIBFloatField
      DisplayLabel = #1062#1077#1085#1072' '#1089#1086' '#1044#1044#1042
      FieldName = 'CENA_SO_DDV'
      DisplayFormat = '###,##0.00'
      EditFormat = '0.00'
    end
    object tblOutStavkaCENA_BEZ_DDV: TFIBFloatField
      DisplayLabel = #1062#1077#1085#1072' '#1073#1077#1079' '#1044#1044#1042
      FieldName = 'CENA_BEZ_DDV'
      DisplayFormat = '###,##0.00'
      EditFormat = '0.00'
    end
    object tblOutStavkaDANOK_PR: TFIBBCDField
      DisplayLabel = #1055#1088#1086#1094'. '#1085#1072' '#1076#1072#1085#1086#1082
      FieldName = 'DANOK_PR'
      Size = 2
      RoundByScale = True
    end
    object tblOutStavkaRABAT_PR: TFIBBCDField
      DisplayLabel = #1055#1088#1086#1094'. '#1084#1072' '#1088#1072#1073#1072#1090
      FieldName = 'RABAT_PR'
      Size = 2
      RoundByScale = True
    end
    object tblOutStavkaKNIG_CENA: TFIBFloatField
      DisplayLabel = #1050#1085#1080#1075'. '#1094#1077#1085#1072
      FieldName = 'KNIG_CENA'
      DisplayFormat = '###,##0.00'
      EditFormat = '0.00'
    end
    object tblOutStavkaKNIG_IZNOS: TFIBBCDField
      DisplayLabel = #1050#1085#1080#1075'. '#1080#1079#1085#1086#1089
      FieldName = 'KNIG_IZNOS'
      Size = 2
      RoundByScale = True
    end
    object tblOutStavkaIZNOS_BEZ_DDV: TFIBBCDField
      DisplayLabel = #1048#1079#1085#1086#1089' '#1073#1077#1079' '#1044#1044#1042
      FieldName = 'IZNOS_BEZ_DDV'
      Size = 2
      RoundByScale = True
    end
    object tblOutStavkaIZNOS_SO_DDV: TFIBBCDField
      DisplayLabel = #1048#1079#1085#1086#1089' '#1089#1086' '#1044#1044#1042
      FieldName = 'IZNOS_SO_DDV'
      Size = 2
      RoundByScale = True
    end
    object tblOutStavkaRABAT_IZNOS: TFIBBCDField
      DisplayLabel = #1048#1079#1085#1086#1089' '#1085#1072' '#1088#1072#1073#1072#1090
      FieldName = 'RABAT_IZNOS'
      Size = 2
      RoundByScale = True
    end
    object tblOutStavkaDANOK_IZNOS: TFIBBCDField
      DisplayLabel = #1048#1079#1085#1086#1089' '#1085#1072' '#1044#1044#1042
      FieldName = 'DANOK_IZNOS'
      Size = 2
      RoundByScale = True
    end
    object tblOutStavkaLOT_NO: TFIBStringField
      DisplayLabel = 'LOT '#1073#1088'.'
      FieldName = 'LOT_NO'
      Size = 30
      EmptyStrToNull = True
    end
    object tblOutStavkaVAZI_DO: TFIBDateField
      DisplayLabel = #1042#1072#1078#1080' '#1076#1086
      FieldName = 'VAZI_DO'
    end
    object tblOutStavkaTS_INS: TFIBDateTimeField
      FieldName = 'TS_INS'
    end
    object tblOutStavkaTS_UPD: TFIBDateTimeField
      FieldName = 'TS_UPD'
    end
    object tblOutStavkaUSR_INS: TFIBStringField
      FieldName = 'USR_INS'
      Size = 50
      EmptyStrToNull = True
    end
    object tblOutStavkaUSR_UPD: TFIBStringField
      FieldName = 'USR_UPD'
      Size = 50
      EmptyStrToNull = True
    end
    object tblOutStavkaCUSTOM1: TFIBStringField
      FieldName = 'CUSTOM1'
      Size = 100
      EmptyStrToNull = True
    end
    object tblOutStavkaCUSTOM2: TFIBStringField
      FieldName = 'CUSTOM2'
      Size = 100
      EmptyStrToNull = True
    end
    object tblOutStavkaCUSTOM3: TFIBStringField
      FieldName = 'CUSTOM3'
      Size = 100
      EmptyStrToNull = True
    end
    object tblOutStavkaREF_ID: TFIBBCDField
      FieldName = 'REF_ID'
      Size = 0
      RoundByScale = True
    end
    object tblOutStavkaOPIS: TFIBStringField
      DisplayLabel = #1054#1087#1080#1089
      FieldName = 'OPIS'
      Size = 200
      EmptyStrToNull = True
    end
    object tblOutStavkaCALC_DANOK_IZNOS: TIBBCDField
      FieldKind = fkCalculated
      FieldName = 'CALC_DANOK_IZNOS'
      Size = 2
      Calculated = True
    end
    object tblOutStavkaART_MERKA: TFIBStringField
      DisplayLabel = #1040#1088#1090'. '#1084#1077#1088#1082#1072
      FieldName = 'ART_MERKA'
      Size = 5
      EmptyStrToNull = True
    end
    object tblOutStavkaCENA_BEZ_RABAT_SO_DDV: TFIBFloatField
      DisplayLabel = #1062#1077#1085#1072' '#1073#1077#1079' '#1088#1072#1073#1072#1090' '#1089#1086' '#1044#1044#1042
      FieldName = 'CENA_BEZ_RABAT_SO_DDV'
      DisplayFormat = '###,##0.00'
      EditFormat = '0.00'
    end
    object tblOutStavkaCENA_BEZ_RABAT_BEZ_DDV: TFIBFloatField
      DisplayLabel = #1062#1077#1085#1072' '#1073#1077#1079' '#1088#1072#1073#1072#1090' '#1073#1077#1079' '#1044#1044#1042
      FieldName = 'CENA_BEZ_RABAT_BEZ_DDV'
      DisplayFormat = '###,##0.00'
      EditFormat = '0.00'
    end
    object tblOutStavkaLOT_ID: TFIBBCDField
      DisplayLabel = 'LOT '#1096#1080#1092'.'
      FieldName = 'LOT_ID'
      Size = 0
      RoundByScale = True
    end
    object tblOutStavkaSLEDLIVOST_KOLICINA: TFIBSmallIntField
      DisplayLabel = #1057#1083#1077#1076#1083#1080#1074#1086#1089#1090
      FieldName = 'SLEDLIVOST_KOLICINA'
    end
  end
  object dsOutStavka: TDataSource
    DataSet = tblOutStavka
    Left = 408
    Top = 80
  end
end
