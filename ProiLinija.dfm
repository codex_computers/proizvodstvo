inherited frmProLinija: TfrmProLinija
  Caption = #1055#1088#1086#1080#1079#1074#1086#1076#1089#1090#1074#1077#1085#1072' '#1051#1080#1085#1080#1112#1072
  ExplicitWidth = 749
  ExplicitHeight = 591
  PixelsPerInch = 96
  TextHeight = 13
  inherited lPanel: TPanel
    inherited cxGrid1: TcxGrid
      ExplicitLeft = 4
      ExplicitTop = -2
      inherited cxGrid1DBTableView1: TcxGridDBTableView
        DataController.DataSource = dm.dsLinija
        object cxGrid1DBTableView1ID: TcxGridDBColumn
          DataBinding.FieldName = 'ID'
          Width = 81
        end
        object cxGrid1DBTableView1NAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'NAZIV'
        end
      end
    end
  end
  inherited dPanel: TPanel
    ExplicitLeft = 8
    ExplicitTop = 370
    inherited Label1: TLabel
      Visible = False
    end
    object Label2: TLabel [1]
      Left = 34
      Top = 54
      Width = 40
      Height = 13
      Caption = #1053#1072#1079#1080#1074' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    inherited Sifra: TcxDBTextEdit
      Tag = 0
      Left = 86
      TabOrder = 3
      Visible = False
      ExplicitLeft = 86
    end
    object cbNaziv: TcxDBTextEdit
      Tag = 1
      Left = 86
      Top = 51
      BeepOnEnter = False
      DataBinding.DataField = 'NAZIV'
      DataBinding.DataSource = dm.dsLinija
      ParentFont = False
      Properties.BeepOnError = True
      Style.Shadow = False
      TabOrder = 0
      OnKeyDown = EnterKakoTab
      Width = 446
    end
  end
  inherited dxRibbon1: TdxRibbon
    ExplicitLeft = -8
    ExplicitTop = 8
    inherited dxRibbon1Tab1: TdxRibbonTab
      Index = 0
    end
    inherited dxRibbon1Tab2: TdxRibbonTab
      Index = 1
    end
  end
  inherited dxBarManager1: TdxBarManager
    DockControlHeights = (
      0
      0
      0
      0)
    inherited dxBarManager1Bar1: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 69
      FloatClientHeight = 172
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton1'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarLargeButton10'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarLargeButton3'
        end>
    end
    inherited dxBarManager1Bar2: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      DockedLeft = 191
      FloatClientWidth = 149
      FloatClientHeight = 130
    end
    inherited dxBarManager1Bar3: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      DockedLeft = 436
      FloatClientWidth = 59
      FloatClientHeight = 108
    end
    inherited dxBarManager1Bar4: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 141
      FloatClientHeight = 221
    end
    inherited dxBarManager1BarTabela: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 174
      FloatClientHeight = 113
    end
    object dxBarLargeButton10: TdxBarLargeButton
      Action = aAzuriraj
      Category = 0
    end
  end
  inherited dxComponentPrinter1: TdxComponentPrinter
    inherited dxComponentPrinter1Link1: TdxGridReportLink
      ReportDocument.CreationDate = 41368.443607719910000000
      AssignedFormatValues = [fvDate, fvTime, fvPageNumber]
      Styles.BandHeader = nil
      Styles.Caption = nil
      Styles.CardCaptionRow = nil
      Styles.CardRowCaption = nil
      Styles.Content = nil
      Styles.ContentEven = nil
      Styles.ContentOdd = nil
      Styles.FilterBar = nil
      Styles.Footer = nil
      Styles.Group = nil
      Styles.Header = nil
      Styles.Preview = nil
      Styles.Selection = nil
      BuiltInReportLink = True
    end
  end
end
