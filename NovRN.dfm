object frmNovRN: TfrmNovRN
  Left = 0
  Top = 0
  Caption = #1042#1085#1077#1089#1080' '#1053#1086#1074' '#1056#1072#1073#1086#1090#1077#1085' '#1053#1072#1083#1086#1075
  ClientHeight = 237
  ClientWidth = 523
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  ShowHint = True
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 523
    Height = 237
    Align = alClient
    BevelInner = bvRaised
    BevelOuter = bvLowered
    TabOrder = 0
    object stat1: TStatusBar
      Left = 2
      Top = 216
      Width = 519
      Height = 19
      Panels = <>
    end
    object cxlblBroj: TcxLabel
      Left = 34
      Top = 49
      AutoSize = False
      Caption = #1053#1072#1079#1080#1074
      ParentFont = False
      Style.Font.Charset = RUSSIAN_CHARSET
      Style.Font.Color = clWindowText
      Style.Font.Height = -13
      Style.Font.Name = 'Tahoma'
      Style.Font.Style = []
      Style.TextColor = clRed
      Style.IsFontAssigned = True
      Properties.Alignment.Horz = taRightJustify
      Transparent = True
      Height = 21
      Width = 100
      AnchorX = 134
    end
    object txtNaziv: TcxDBTextEdit
      Tag = 1
      Left = 140
      Top = 48
      Hint = #1053#1072#1079#1080#1074' '#1085#1072' '#1056#1072#1073#1086#1090#1077#1085' '#1053#1072#1083#1086#1075
      DataBinding.DataField = 'BROJ'
      DataBinding.DataSource = dm.dsRabotenNalog
      ParentFont = False
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clWindowText
      Style.Font.Height = -13
      Style.Font.Name = 'Tahoma'
      Style.Font.Style = []
      Style.IsFontAssigned = True
      TabOrder = 0
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 174
    end
    object txtPlanDatumPoc: TcxDBDateEdit
      Tag = 1
      Left = 141
      Top = 83
      Hint = #1044#1072#1090#1091#1084' '#1085#1072' '#1056#1072#1073#1086#1090#1077#1085' '#1053#1072#1083#1086#1075
      DataBinding.DataField = 'DATUM_PLANIRAN_POCETOK'
      DataBinding.DataSource = dm.dsRabotenNalog
      ParentFont = False
      Properties.Kind = ckDateTime
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clWindowText
      Style.Font.Height = -13
      Style.Font.Name = 'Tahoma'
      Style.Font.Style = []
      Style.IsFontAssigned = True
      TabOrder = 1
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 173
    end
    object cxlbl1: TcxLabel
      Left = 84
      Top = 84
      AutoSize = False
      Caption = #1044#1072#1090#1091#1084
      ParentFont = False
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clWindowText
      Style.Font.Height = -13
      Style.Font.Name = 'Tahoma'
      Style.Font.Style = []
      Style.TextColor = clRed
      Style.IsFontAssigned = True
      Properties.Alignment.Horz = taRightJustify
      Properties.WordWrap = True
      Transparent = True
      Height = 21
      Width = 50
      AnchorX = 134
    end
    object btnZapisi: TcxButton
      Left = 304
      Top = 168
      Width = 75
      Height = 25
      Action = aZapisi
      ModalResult = 1
      TabOrder = 2
    end
    object btnOtkazi: TcxButton
      Left = 394
      Top = 168
      Width = 75
      Height = 25
      Action = aOtkazi
      ModalResult = 2
      TabOrder = 3
    end
    object cxlbl2: TcxLabel
      Left = 320
      Top = 48
      AutoSize = False
      Caption = '/'
      Transparent = True
      Height = 17
      Width = 14
    end
    object txtBroj: TcxTextEdit
      Left = 332
      Top = 46
      ParentFont = False
      Properties.ReadOnly = True
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clWindowText
      Style.Font.Height = -13
      Style.Font.Name = 'Tahoma'
      Style.Font.Style = []
      Style.IsFontAssigned = True
      TabOrder = 8
      Width = 88
    end
  end
  object actnlst1: TActionList
    Images = dmRes.cxSmallImages
    Left = 464
    Top = 32
    object aZapisi: TAction
      Caption = #1047#1072#1087#1080#1096#1080
      ImageIndex = 6
      ShortCut = 120
      OnExecute = aZapisiExecute
    end
    object aOtkazi: TAction
      Caption = #1054#1090#1082#1072#1078#1080
      ImageIndex = 21
      ShortCut = 27
      OnExecute = aOtkaziExecute
    end
  end
  object qMaxBroj: TpFIBQuery
    Transaction = dmKon.qTransaction
    Database = dmKon.fibBaza
    SQL.Strings = (
      'select coalesce(count(r.id),0)+1  broj'
      'from man_raboten_nalog r'
      'where r.godina = :godina'
      'and r.id_re = :id_re')
    Left = 463
    Top = 70
    qoAutoCommit = True
    qoStartTransaction = True
  end
end
