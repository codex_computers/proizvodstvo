object frmObrabotkaRN: TfrmObrabotkaRN
  Left = 128
  Top = 212
  BorderIcons = [biSystemMenu, biMaximize]
  Caption = #1054#1073#1088#1072#1073#1086#1090#1082#1072' '#1085#1072' '#1056#1072#1073#1086#1090#1077#1085' '#1085#1072#1083#1086#1075
  ClientHeight = 742
  ClientWidth = 1211
  Color = clBtnFace
  Constraints.MinHeight = 500
  Constraints.MinWidth = 610
  Font.Charset = RUSSIAN_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  ShowHint = True
  WindowState = wsMaximized
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object dxRibbon1: TdxRibbon
    Left = 0
    Top = 0
    Width = 1211
    Height = 126
    BarManager = dxBarManager1
    ColorSchemeName = 'Blue'
    Contexts = <>
    TabOrder = 0
    TabStop = False
    object dxRibbon1Tab1: TdxRibbonTab
      Active = True
      Caption = #1052#1077#1085#1080
      Groups = <
        item
          ToolbarName = 'dxBarManager1Bar5'
        end
        item
          ToolbarName = 'dxBarManager1Bar2'
        end
        item
          ToolbarName = 'dxBarManager1Bar3'
        end>
      Index = 0
    end
    object dxRibbon1Tab2: TdxRibbonTab
      Caption = #1055#1086#1076#1077#1089#1091#1074#1072#1114#1072
      Groups = <
        item
          ToolbarName = 'dxBarManager1Bar4'
        end
        item
          ToolbarName = 'dxBarManager1BarIzgledGrid'
        end>
      Index = 1
    end
  end
  object StatusBar1: TdxRibbonStatusBar
    Left = 0
    Top = 719
    Width = 1211
    Height = 23
    Panels = <
      item
        PanelStyleClassName = 'TdxStatusBarTextPanelStyle'
        Text = 
          'F5 - '#1053#1086#1074', F6 - '#1040#1078#1091#1088#1080#1088#1072#1112', F7 - '#1054#1089#1074#1077#1078#1080', F8 - '#1041#1088#1080#1096#1080', F9 - '#1047#1072#1087#1080#1096#1080', E' +
          'sc - '#1054#1090#1082#1072#1078#1080' / '#1048#1079#1083#1077#1079
      end>
    Ribbon = dxRibbon1
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clDefault
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
  end
  object cxPageControl1: TcxPageControl
    Left = 0
    Top = 126
    Width = 1211
    Height = 593
    Align = alClient
    TabOrder = 5
    Properties.ActivePage = tsListaRN
    Properties.NavigatorPosition = npLeftBottom
    Properties.Rotate = True
    Properties.Style = 6
    Properties.TabPosition = tpLeft
    LookAndFeel.NativeStyle = False
    OnChange = cxPageControl1Change
    ClientRectBottom = 593
    ClientRectLeft = 143
    ClientRectRight = 1211
    ClientRectTop = 0
    object tsListaRN: TcxTabSheet
      Caption = #1051#1080#1089#1090#1072' '#1085#1072' '#1056#1072#1073#1086#1090#1085#1080' '#1085#1072#1083#1086#1079#1080
      ImageIndex = 0
      ParentShowHint = False
      ShowHint = True
      object lPanel: TPanel
        Left = 0
        Top = 0
        Width = 1068
        Height = 536
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object cxGrid1: TcxGrid
          Left = 0
          Top = 0
          Width = 1068
          Height = 536
          Align = alClient
          TabOrder = 0
          object cxGrid1DBTableView1: TcxGridDBTableView
            OnDblClick = cxGrid1DBTableView1DblClick
            OnKeyDown = cxGrid1DBTableView1KeyDown
            OnFocusedRecordChanged = cxGrid3DBTableView1FocusedRecordChanged
            DataController.DataSource = dm.dsRabotenNalog
            DataController.Options = [dcoAnsiSort, dcoAssignGroupingValues, dcoAssignMasterDetailKeys, dcoSaveExpanding, dcoGroupsAlwaysExpanded]
            DataController.Summary.DefaultGroupSummaryItems = <>
            DataController.Summary.FooterSummaryItems = <>
            DataController.Summary.SummaryGroups = <>
            DataController.OnDetailCollapsed = cxGrid1DBTableView1DataControllerDetailCollapsed
            DataController.OnDetailExpanding = cxGrid1DBTableView1DataControllerDetailExpanding
            DataController.OnDetailExpanded = cxGrid1DBTableView1DataControllerDetailExpanded
            FilterRow.InfoText = #1050#1083#1080#1082#1085#1077#1090#1077' '#1090#1091#1082#1072' '#1079#1072' '#1076#1072' '#1076#1077#1092#1080#1085#1080#1088#1072#1090#1077' '#1092#1080#1083#1090#1077#1088
            FilterRow.Visible = True
            OptionsBehavior.CellHints = True
            OptionsBehavior.ImmediateEditor = False
            OptionsBehavior.IncSearch = True
            OptionsCustomize.ColumnsQuickCustomization = True
            OptionsData.Deleting = False
            OptionsData.Editing = False
            OptionsData.Inserting = False
            OptionsView.NoDataToDisplayInfoText = '<'#1053#1077#1084#1072' '#1088#1072#1073#1086#1090#1085#1080' '#1085#1072#1083#1086#1079#1080'>'
            OptionsView.Footer = True
            OptionsView.Indicator = True
            object cxGrid1DBTableView1NAZIV_STATUS: TcxGridDBColumn
              Caption = #1053#1072#1079#1080#1074' '#1085#1072' '#1089#1090#1072#1090#1091#1089
              DataBinding.FieldName = 'STATUS'
              PropertiesClassName = 'TcxImageComboBoxProperties'
              Properties.Images = dmRes.cxSmallImages
              Properties.Items = <
                item
                  Description = #1054#1058#1042#1054#1056#1045#1053
                  ImageIndex = 47
                  Value = 1
                end
                item
                  Description = #1047#1040#1058#1042#1054#1056#1045#1053
                  ImageIndex = 36
                  Value = 2
                end>
              Width = 97
            end
            object cxGrid1DBTableView1NAZIV_STATUS_STAVKI: TcxGridDBColumn
              Caption = #1053#1072#1079#1080#1074' '#1089#1090#1072#1090#1091#1089' '#1089#1090#1072#1074#1082#1080
              DataBinding.FieldName = 'NAZIV_STATUS_STAVKI'
              Width = 159
            end
            object cxGrid1DBTableView1ID: TcxGridDBColumn
              Caption = #1064#1080#1092#1088#1072
              DataBinding.FieldName = 'ID'
              Visible = False
            end
            object cxGrid1DBTableView1BROJ: TcxGridDBColumn
              DataBinding.FieldName = 'BROJ'
              Width = 91
            end
            object cxGrid1DBTableView1ID_PORACKA: TcxGridDBColumn
              Caption = #1064#1080#1092#1088#1072' '#1085#1072' '#1087#1086#1088#1072#1095#1082#1072
              DataBinding.FieldName = 'ID_PORACKA'
            end
            object cxGrid1DBTableView1ID_RE: TcxGridDBColumn
              Caption = #1064#1080#1092#1088#1072' '#1085#1072' '#1057#1077#1082#1090#1086#1088'/'#1054#1076#1077#1083#1077#1085#1080#1077
              DataBinding.FieldName = 'ID_RE'
            end
            object cxGrid1DBTableView1DATUM_PLANIRAN_POCETOK: TcxGridDBColumn
              Caption = #1044#1072#1090#1091#1084' '#1085#1072' '#1087#1083#1072#1085#1080#1088#1072#1085' '#1087#1086#1095#1077#1090#1086#1082
              DataBinding.FieldName = 'DATUM_PLANIRAN_POCETOK'
              SortIndex = 0
              SortOrder = soAscending
            end
            object cxGrid1DBTableView1DATUM_START: TcxGridDBColumn
              Caption = #1055#1086#1095#1077#1090#1077#1085' '#1076#1072#1090#1091#1084
              DataBinding.FieldName = 'DATUM_START'
              Width = 101
            end
            object cxGrid1DBTableView1SOURCE_DOKUMENT: TcxGridDBColumn
              Caption = #1048#1079#1074#1086#1088#1077#1085' '#1076#1086#1082#1091#1084#1077#1085#1090
              DataBinding.FieldName = 'SOURCE_DOKUMENT'
              Visible = False
            end
            object cxGrid1DBTableView1STATUS: TcxGridDBColumn
              Caption = #1064#1080#1092#1088#1072' '#1085#1072' '#1089#1090#1072#1090#1091#1089
              DataBinding.FieldName = 'STATUS'
            end
            object cxGrid1DBTableView1Column1: TcxGridDBColumn
              Caption = #1057#1090#1072#1090#1091#1089
              DataBinding.ValueType = 'String'
              PropertiesClassName = 'TcxButtonEditProperties'
              Properties.Buttons = <
                item
                  Default = True
                  Kind = bkEllipsis
                end>
            end
            object cxGrid1DBTableView1ID_RE_SUROVINI: TcxGridDBColumn
              Caption = #1064#1080#1092#1088#1072' '#1085#1072' '#1084#1072#1075#1072#1094#1080#1085' '#1079#1072' '#1089#1091#1088#1086#1074#1080#1085#1080
              DataBinding.FieldName = 'ID_RE_SUROVINI'
            end
            object cxGrid1DBTableView1NAZIV_MAGACIN_SUROVINI: TcxGridDBColumn
              Caption = #1052#1072#1075#1072#1094#1080#1085' '#1079#1072' '#1089#1091#1088#1086#1074#1080#1085#1080
              DataBinding.FieldName = 'NAZIV_MAGACIN_SUROVINI'
              Width = 142
            end
            object cxGrid1DBTableView1ID_RE_GOTOV_PROIZVOD: TcxGridDBColumn
              Caption = #1064#1080#1092#1088#1072' '#1085#1072' '#1084#1072#1075#1072#1094#1080#1085' '#1079#1072' '#1075#1086#1090#1086#1074#1080' '#1087#1088#1086#1080#1079#1074#1086#1076#1080
              DataBinding.FieldName = 'ID_RE_GOTOV_PROIZVOD'
            end
            object cxGrid1DBTableView1NAZIV_MAGACIN_GOTOVI_PROIZVODI: TcxGridDBColumn
              Caption = #1053#1072#1079#1080#1074' '#1085#1072' '#1084#1072#1075#1072#1094#1080#1085' '#1079#1072' '#1075#1086#1090#1086#1074#1080' '#1087#1088#1086#1080#1079#1074#1086#1076#1080
              DataBinding.FieldName = 'NAZIV_MAGACIN_GOTOVI_PROIZVODI'
              Width = 272
            end
            object cxGrid1DBTableView1PRIORITET: TcxGridDBColumn
              Caption = #1055#1088#1080#1086#1088#1080#1090#1077#1090
              DataBinding.FieldName = 'PRIORITET'
            end
            object cxGrid1DBTableView1DATUM_PLANIRAN_KRAJ: TcxGridDBColumn
              Caption = #1055#1083#1072#1085#1080#1088#1072#1085' '#1076#1072#1090#1091#1084' '#1079#1072' '#1082#1088#1072#1112
              DataBinding.FieldName = 'DATUM_PLANIRAN_KRAJ'
            end
            object cxGrid1DBTableView1DATUM_KRAJ: TcxGridDBColumn
              Caption = #1050#1088#1072#1077#1085' '#1076#1072#1090#1091#1084
              DataBinding.FieldName = 'DATUM_KRAJ'
              Width = 98
            end
            object cxGrid1DBTableView1BR_CASOVI: TcxGridDBColumn
              Caption = #1042#1082#1091#1087#1077#1085' '#1073#1088#1086#1112' '#1085#1072' '#1095#1072#1089#1086#1074#1080
              DataBinding.FieldName = 'BR_CASOVI'
              Visible = False
            end
            object cxGrid1DBTableView1BR_CIKLUSI: TcxGridDBColumn
              Caption = #1042#1082#1091#1087#1077#1085' '#1073#1088#1086#1112' '#1085#1072' '#1094#1080#1082#1083#1091#1089#1080
              DataBinding.FieldName = 'BR_CIKLUSI'
              Visible = False
            end
            object cxGrid1DBTableView1GODINA: TcxGridDBColumn
              Caption = #1043#1086#1076#1080#1085#1072
              DataBinding.FieldName = 'GODINA'
            end
            object cxGrid1DBTableView1STATUS_STAVKI: TcxGridDBColumn
              Caption = #1057#1090#1072#1090#1091#1089' '#1089#1090#1072#1074#1082#1080
              DataBinding.FieldName = 'STATUS_STAVKI'
            end
          end
          object cxGrid1DBTableView2: TcxGridDBTableView
            DataController.DataSource = dm.dsRNStavka
            DataController.DetailKeyFieldNames = 'ID_RABOTEN_NALOG'
            DataController.KeyFieldNames = 'ID'
            DataController.MasterKeyFieldNames = 'ID'
            DataController.Options = [dcoAssignGroupingValues, dcoAssignMasterDetailKeys, dcoSaveExpanding, dcoGroupsAlwaysExpanded]
            DataController.Summary.DefaultGroupSummaryItems = <>
            DataController.Summary.FooterSummaryItems = <>
            DataController.Summary.SummaryGroups = <>
            OptionsBehavior.IncSearch = True
            OptionsCustomize.ColumnsQuickCustomization = True
            OptionsData.Deleting = False
            OptionsData.Editing = False
            OptionsData.Inserting = False
            OptionsView.ColumnAutoWidth = True
            OptionsView.GroupByBox = False
            object cxGrid1DBTableView2NAZIV_STATUS: TcxGridDBColumn
              Caption = #1053#1072#1079#1080#1074' '#1089#1090#1072#1090#1091#1089
              DataBinding.FieldName = 'NAZIV_STATUS'
              Width = 126
            end
            object cxGrid1DBTableView2BOJA: TcxGridDBColumn
              Caption = #1057#1090#1072#1090#1091#1089
              DataBinding.FieldName = 'BOJA'
              PropertiesClassName = 'TcxColorComboBoxProperties'
              Properties.CustomColors = <
                item
                  Color = clBlack
                end>
            end
            object cxGrid1DBTableView2VID_ARTIKAL: TcxGridDBColumn
              Caption = #1042#1080#1076' '#1085#1072' '#1072#1088#1090#1080#1082#1072#1083
              DataBinding.FieldName = 'VID_ARTIKAL'
            end
            object cxGrid1DBTableView2ARTIKAL: TcxGridDBColumn
              Caption = #1064#1080#1092#1088#1072' '#1085#1072' '#1072#1088#1090#1080#1082#1072#1083
              DataBinding.FieldName = 'ARTIKAL'
            end
            object cxGrid1DBTableView2NAZIV_ARTIKAL: TcxGridDBColumn
              Caption = #1040#1088#1090#1080#1082#1072#1083
              DataBinding.FieldName = 'NAZIV_ARTIKAL'
              Width = 104
            end
            object cxGrid1DBTableView2KOLICINA: TcxGridDBColumn
              Caption = #1050#1086#1083#1080#1095#1080#1085#1072
              DataBinding.FieldName = 'KOLICINA'
            end
            object cxGrid1DBTableView2MERKA: TcxGridDBColumn
              Caption = #1052#1077#1088#1082#1072
              DataBinding.FieldName = 'MERKA'
            end
            object cxGrid1DBTableView2CUSTOM1: TcxGridDBColumn
              Caption = #1058#1072#1087#1072#1094#1080#1088
              DataBinding.FieldName = 'CUSTOM1'
              Width = 118
            end
            object cxGrid1DBTableView2CUSTOM2: TcxGridDBColumn
              Caption = #1053#1086#1075#1072#1088#1082#1080
              DataBinding.FieldName = 'CUSTOM2'
              Width = 112
            end
            object cxGrid1DBTableView2CUSTOM3: TcxGridDBColumn
              Caption = #1054#1089#1090#1072#1085#1072#1090#1086
              DataBinding.FieldName = 'CUSTOM3'
              Width = 104
            end
            object cxGrid1DBTableView2ID: TcxGridDBColumn
              Caption = #1064#1080#1092#1088#1072
              DataBinding.FieldName = 'ID'
              Visible = False
            end
            object cxGrid1DBTableView2BROJ: TcxGridDBColumn
              Caption = #1041#1088#1086#1112
              DataBinding.FieldName = 'BROJ'
            end
            object cxGrid1DBTableView2ID_PORACKA: TcxGridDBColumn
              Caption = #1064#1080#1092#1088#1072' '#1085#1072' '#1087#1086#1088#1072#1095#1082#1072
              DataBinding.FieldName = 'ID_PORACKA'
              Visible = False
            end
            object cxGrid1DBTableView2BROJ_CASOVI: TcxGridDBColumn
              Caption = #1041#1088#1086#1112' '#1085#1072' '#1095#1072#1089#1086#1074#1080
              DataBinding.FieldName = 'BROJ_CASOVI'
              Visible = False
            end
            object cxGrid1DBTableView2OD_DATUM_PLANIRAN: TcxGridDBColumn
              Caption = #1054#1076' '#1076#1072#1090#1091#1084' '#1087#1083#1072#1085#1080#1088#1072#1085
              DataBinding.FieldName = 'OD_DATUM_PLANIRAN'
            end
            object cxGrid1DBTableView2DO_DATUM_PLANIRAN: TcxGridDBColumn
              Caption = #1044#1086' '#1076#1072#1090#1091#1084' '#1087#1083#1072#1085#1080#1088#1072#1085
              DataBinding.FieldName = 'DO_DATUM_PLANIRAN'
            end
            object cxGrid1DBTableView2OD_DATUM_REALIZACIJA: TcxGridDBColumn
              Caption = #1044#1086' '#1076#1072#1090#1091#1084' '#1088#1077#1072#1083#1080#1079#1072#1094#1080#1112#1072
              DataBinding.FieldName = 'OD_DATUM_REALIZACIJA'
            end
            object cxGrid1DBTableView2DO_DATUM_REALIZACIJA: TcxGridDBColumn
              Caption = #1054#1076' '#1076#1072#1090#1091#1084' '#1088#1077#1072#1083#1080#1079#1072#1094#1080#1112#1072
              DataBinding.FieldName = 'DO_DATUM_REALIZACIJA'
            end
            object cxGrid1DBTableView2DATUM_OD_ISPORAKA: TcxGridDBColumn
              Caption = #1054#1076' '#1076#1072#1090#1091#1084' '#1080#1089#1087#1086#1088#1072#1082#1072
              DataBinding.FieldName = 'DATUM_OD_ISPORAKA'
              Visible = False
            end
            object cxGrid1DBTableView2DATUM_DO_ISPORAKA: TcxGridDBColumn
              Caption = #1044#1086' '#1076#1072#1090#1091#1084' '#1080#1089#1087#1086#1088#1072#1082#1072
              DataBinding.FieldName = 'DATUM_DO_ISPORAKA'
              Visible = False
            end
            object cxGrid1DBTableView2KRAEN_DATUM_PREVOZ: TcxGridDBColumn
              Caption = #1050#1088#1072#1077#1085' '#1076#1072#1090#1091#1084' '#1087#1088#1077#1074#1086#1079
              DataBinding.FieldName = 'KRAEN_DATUM_PREVOZ'
              Visible = False
            end
            object cxGrid1DBTableView2ZABELESKA: TcxGridDBColumn
              Caption = #1047#1072#1073#1077#1083#1077#1096#1082#1072
              DataBinding.FieldName = 'ZABELESKA'
              Width = 155
            end
            object cxGrid1DBTableView2VREDNOST: TcxGridDBColumn
              Caption = #1064#1080#1092#1088#1072' '#1085#1072' '#1089#1090#1072#1090#1091#1089
              DataBinding.FieldName = 'VREDNOST'
              Visible = False
            end
            object cxGrid1DBTableView2STATUS: TcxGridDBColumn
              Caption = #1057#1090#1072#1090#1091#1089
              DataBinding.FieldName = 'STATUS'
              Visible = False
            end
            object cxGrid1DBTableView2NAZIV_MERKA: TcxGridDBColumn
              Caption = #1052#1077#1088#1082#1072
              DataBinding.FieldName = 'NAZIV_MERKA'
              Visible = False
              Width = 135
            end
          end
          object cxGrid1Level1: TcxGridLevel
            GridView = cxGrid1DBTableView1
            object cxGrid1Level2: TcxGridLevel
              GridView = cxGrid1DBTableView2
            end
          end
        end
      end
      object cxGroupBox4: TcxGroupBox
        Left = 0
        Top = 536
        Align = alBottom
        Caption = #1052#1086#1084#1077#1085#1090#1072#1083#1077#1085' '#1089#1090#1072#1090#1091#1089' '#1085#1072' '#1056#1072#1073#1086#1090#1085#1080#1086#1090' '#1085#1072#1083#1086#1075
        TabOrder = 1
        Height = 57
        Width = 1068
        object cxDBLabel1: TcxDBLabel
          Left = 96
          Top = 23
          DataBinding.DataField = 'NAZIV_STATUS_STAVKI'
          DataBinding.DataSource = dm.dsRabotenNalog
          Properties.Alignment.Horz = taCenter
          Properties.Alignment.Vert = taVCenter
          Style.BorderStyle = ebsUltraFlat
          Style.HotTrack = False
          Style.LookAndFeel.NativeStyle = False
          Style.TextColor = clNavy
          Style.TextStyle = [fsBold]
          StyleDisabled.LookAndFeel.NativeStyle = False
          StyleFocused.LookAndFeel.NativeStyle = False
          StyleHot.LookAndFeel.NativeStyle = False
          Height = 21
          Width = 412
          AnchorX = 302
          AnchorY = 34
        end
        object cxLabel31: TcxLabel
          Left = 527
          Top = 24
          Caption = #1055#1088#1086#1084#1077#1085#1080' '#1074#1086
          Style.TextColor = clNavy
          Properties.Alignment.Horz = taRightJustify
          Properties.WordWrap = True
          Transparent = True
          Visible = False
          Width = 62
          AnchorX = 589
        end
        object cxDBButtonEdit2: TcxDBButtonEdit
          Left = 595
          Top = 20
          Hint = #1050#1083#1080#1082#1085#1077#1090#1077', '#1079#1072' '#1076#1072' '#1087#1088#1086#1084#1077#1085#1080#1090#1077' '#1089#1090#1072#1090#1091#1089' '#1085#1072' '#1056#1072#1073#1086#1090#1077#1085' '#1085#1072#1083#1086#1075
          Touch.ParentTabletOptions = False
          Touch.TabletOptions = [toPressAndHold]
          DataBinding.DataField = 'NAZIV_STATUS_STAVKI'
          DataBinding.DataSource = dm.dsRabotenNalog
          ParentFont = False
          Properties.Buttons = <
            item
              Caption = 'Status'
              Default = True
              Kind = bkText
            end>
          Properties.ViewStyle = vsButtonsOnly
          Style.BorderStyle = ebsUltraFlat
          Style.Font.Charset = RUSSIAN_CHARSET
          Style.Font.Color = clWindowText
          Style.Font.Height = -11
          Style.Font.Name = 'Tahoma'
          Style.Font.Style = []
          Style.LookAndFeel.Kind = lfUltraFlat
          Style.LookAndFeel.NativeStyle = False
          Style.Shadow = False
          Style.TextStyle = []
          Style.ButtonStyle = btsDefault
          Style.ButtonTransparency = ebtNone
          Style.IsFontAssigned = True
          StyleDisabled.LookAndFeel.Kind = lfUltraFlat
          StyleDisabled.LookAndFeel.NativeStyle = False
          StyleFocused.LookAndFeel.Kind = lfUltraFlat
          StyleFocused.LookAndFeel.NativeStyle = False
          StyleHot.LookAndFeel.Kind = lfUltraFlat
          StyleHot.LookAndFeel.NativeStyle = False
          TabOrder = 2
          Visible = False
          OnClick = aSmeniStatusExecute
          Width = 238
        end
      end
    end
    object tsOperacii: TcxTabSheet
      Caption = #1054#1087#1077#1088#1072#1094#1080#1080
      ImageIndex = 4
      TabVisible = False
      ExplicitLeft = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object cxGroupBox2: TcxGroupBox
        Left = 0
        Top = 0
        Align = alClient
        Caption = '  '#1054#1087#1077#1088#1072#1094#1080#1080' '#1079#1072' '#1085#1086#1088#1084#1072#1090#1080#1074#1086#1090'  '
        TabOrder = 0
        Height = 593
        Width = 1068
        object cxGrid5: TcxGrid
          Left = 2
          Top = 18
          Width = 1064
          Height = 573
          Align = alClient
          TabOrder = 0
          ExplicitWidth = 885
          ExplicitHeight = 581
          object cxGridDBTableView2: TcxGridDBTableView
            DataController.DataSource = dm.dspRNOperacija
            DataController.Summary.DefaultGroupSummaryItems = <>
            DataController.Summary.FooterSummaryItems = <>
            DataController.Summary.SummaryGroups = <>
            FilterRow.InfoText = #1050#1083#1080#1082#1085#1077#1090#1077' '#1090#1091#1082#1072' '#1079#1072' '#1076#1072' '#1076#1077#1092#1080#1085#1080#1088#1072#1090#1077' '#1092#1080#1083#1090#1077#1088
            FilterRow.Visible = True
            OptionsCustomize.ColumnsQuickCustomization = True
            OptionsData.Deleting = False
            OptionsData.Editing = False
            OptionsData.Inserting = False
            OptionsView.Indicator = True
            object cxGridDBTableView2OPERACIJA_N: TcxGridDBColumn
              Caption = #1064#1080#1092#1088#1072' '#1085#1072' '#1086#1087#1077#1088#1072#1094#1080#1112#1072
              DataBinding.FieldName = 'OPERACIJA_N'
              Width = 40
            end
            object cxGridDBTableView2NAZIV_OPERACIJA: TcxGridDBColumn
              Caption = #1054#1087#1077#1088#1072#1094#1080#1112#1072
              DataBinding.FieldName = 'NAZIV_OPERACIJA'
              Width = 283
            end
            object cxGridDBTableView2POTREBNO_VREME: TcxGridDBColumn
              Caption = #1055#1086#1090#1088#1077#1073#1085#1086' '#1074#1088#1077#1084#1077
              DataBinding.FieldName = 'POTREBNO_VREME'
            end
            object cxGridDBTableView2MERKA: TcxGridDBColumn
              Caption = #1052#1077#1088#1082#1072
              DataBinding.FieldName = 'MERKA'
            end
            object cxGridDBTableView2AKTIVEN: TcxGridDBColumn
              Caption = #1040#1082#1090#1080#1074#1077#1085
              DataBinding.FieldName = 'AKTIVEN'
            end
            object cxGridDBTableView2SEKVENCA: TcxGridDBColumn
              Caption = #1057#1077#1082#1074#1077#1085#1094#1072
              DataBinding.FieldName = 'SEKVENCA'
            end
          end
          object cxGridLevel2: TcxGridLevel
            GridView = cxGridDBTableView2
          end
        end
      end
    end
    object tsNormativ: TcxTabSheet
      Caption = #1052#1072#1090#1077#1088#1080#1112#1072#1083#1080' '#1080' '#1086#1087#1077#1088#1072#1094#1080#1080
      ImageIndex = 3
      object cxGroupBox3: TcxGroupBox
        Left = 0
        Top = 0
        Align = alTop
        Caption = '  '#1057#1091#1088#1086#1074#1080#1085#1080' '#1080' '#1086#1087#1077#1088#1094#1080#1080' '#1087#1086' '#1085#1086#1088#1084#1072#1090#1080#1074'  '
        ParentFont = False
        Style.Font.Charset = RUSSIAN_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -11
        Style.Font.Name = 'Tahoma'
        Style.Font.Style = []
        Style.LookAndFeel.NativeStyle = False
        Style.TextStyle = [fsBold]
        Style.IsFontAssigned = True
        StyleDisabled.LookAndFeel.NativeStyle = False
        StyleFocused.LookAndFeel.NativeStyle = False
        StyleHot.LookAndFeel.NativeStyle = False
        TabOrder = 0
        Height = 217
        Width = 1068
        object cxGrid2: TcxGrid
          Left = 2
          Top = 18
          Width = 1064
          Height = 197
          Align = alClient
          TabOrder = 0
          RootLevelOptions.DetailTabsPosition = dtpTop
          object cxGrid2DBTableView1: TcxGridDBTableView
            DataController.DataSource = dm.dspRNNormativ
            DataController.Summary.DefaultGroupSummaryItems = <>
            DataController.Summary.FooterSummaryItems = <>
            DataController.Summary.SummaryGroups = <>
            FilterRow.InfoText = #1050#1083#1080#1082#1085#1077#1090#1077' '#1090#1091#1082#1072' '#1079#1072' '#1076#1072' '#1076#1077#1092#1080#1085#1080#1088#1072#1090#1077' '#1092#1080#1083#1090#1077#1088
            FilterRow.Visible = True
            OptionsCustomize.ColumnsQuickCustomization = True
            OptionsData.Deleting = False
            OptionsData.Editing = False
            OptionsData.Inserting = False
            OptionsView.NoDataToDisplayInfoText = '<'#1053#1077#1084#1072' '#1084#1072#1090#1077#1088#1080#1112#1072#1083#1080'>'
            OptionsView.Indicator = True
            object cxGrid2DBTableView1VID_ARTIKAL_N: TcxGridDBColumn
              Caption = #1042#1080#1076' '#1085#1072' '#1089#1091#1088#1086#1074#1080#1085#1072
              DataBinding.FieldName = 'VID_ARTIKAL_N'
              Width = 89
            end
            object cxGrid2DBTableView1ARTIKAL_N: TcxGridDBColumn
              Caption = #1064#1080#1092#1088#1072' '#1085#1072' '#1089#1091#1088#1086#1074#1080#1085#1072
              DataBinding.FieldName = 'ARTIKAL_N'
              Width = 106
            end
            object cxGrid2DBTableView1NAZIV_ARTIKAL: TcxGridDBColumn
              Caption = #1057#1091#1088#1086#1074#1080#1085#1072
              DataBinding.FieldName = 'NAZIV_ARTIKAL'
              Width = 311
            end
            object cxGrid2DBTableView1KOLICINA: TcxGridDBColumn
              Caption = #1050#1086#1083#1080#1095#1080#1085#1072
              DataBinding.FieldName = 'KOLICINA'
              Width = 82
            end
            object cxGrid2DBTableView1MERKA: TcxGridDBColumn
              Caption = #1052#1077#1088#1082#1072
              DataBinding.FieldName = 'MERKA'
              Width = 60
            end
            object cxGrid2DBTableView1AKTIVEN: TcxGridDBColumn
              Caption = #1040#1082#1090#1080#1074#1077#1085
              DataBinding.FieldName = 'AKTIVEN'
              Visible = False
            end
          end
          object cxGrid2DBTableView2: TcxGridDBTableView
            DataController.DataSource = dm.dspRNOperacija
            DataController.Summary.DefaultGroupSummaryItems = <>
            DataController.Summary.FooterSummaryItems = <>
            DataController.Summary.SummaryGroups = <>
            FilterRow.InfoText = #1050#1083#1080#1082#1085#1077#1090#1077' '#1090#1091#1082#1072' '#1079#1072' '#1076#1072' '#1076#1077#1092#1080#1085#1080#1088#1072#1090#1077' '#1092#1080#1083#1090#1077#1088
            FilterRow.Visible = True
            OptionsBehavior.IncSearch = True
            OptionsCustomize.ColumnsQuickCustomization = True
            OptionsData.Deleting = False
            OptionsData.Editing = False
            OptionsData.Inserting = False
            OptionsView.NoDataToDisplayInfoText = '<'#1053#1077#1084#1072' '#1086#1087#1077#1088#1072#1094#1080#1112#1072'>'
            OptionsView.Indicator = True
            object cxGrid2DBTableView2OPERACIJA_N: TcxGridDBColumn
              Caption = #1064#1080#1092#1088#1072' '#1085#1072' '#1086#1087#1077#1088#1072#1094#1080#1112#1072
              DataBinding.FieldName = 'OPERACIJA_N'
            end
            object cxGrid2DBTableView2NAZIV_OPERACIJA: TcxGridDBColumn
              Caption = #1054#1087#1077#1088#1072#1094#1080#1112#1072
              DataBinding.FieldName = 'NAZIV_OPERACIJA'
              Width = 314
            end
            object cxGrid2DBTableView2POTREBNO_VREME: TcxGridDBColumn
              Caption = #1055#1086#1090#1088#1077#1073#1085#1086' '#1074#1088#1077#1084#1077
              DataBinding.FieldName = 'POTREBNO_VREME'
            end
            object cxGrid2DBTableView2MERKA: TcxGridDBColumn
              Caption = #1052#1077#1088#1082#1072
              DataBinding.FieldName = 'MERKA'
              Width = 134
            end
            object cxGrid2DBTableView2SEKVENCA: TcxGridDBColumn
              Caption = #1057#1077#1082#1074#1077#1085#1094#1072
              DataBinding.FieldName = 'SEKVENCA'
              Visible = False
            end
          end
          object cxGrid2Level1: TcxGridLevel
            Caption = #1052#1072#1090#1077#1088#1080#1112#1072#1083#1080
            GridView = cxGrid2DBTableView1
          end
          object cxGrid2Level2: TcxGridLevel
            Caption = #1054#1087#1077#1088#1072#1094#1080#1080
            GridView = cxGrid2DBTableView2
          end
        end
      end
      object cxGroupBox9: TcxGroupBox
        Left = 0
        Top = 348
        Align = alClient
        Caption = #1057#1091#1088#1086#1074#1080#1085#1080' '#1086#1076' '#1052#1072#1075#1072#1094#1080#1085' '#1079#1072' '#1088#1077#1087#1088#1086#1084#1072#1090#1077#1088#1080#1112#1072#1083#1080'  '
        TabOrder = 1
        Visible = False
        Height = 245
        Width = 1068
        object cxGrid7: TcxGrid
          Left = 3
          Top = 25
          Width = 885
          Height = 221
          TabOrder = 0
          object cxGridDBTableView4: TcxGridDBTableView
            OnFocusedRecordChanged = cxGridDBTableView4FocusedRecordChanged
            DataController.DataSource = dm.dsRNSurovini
            DataController.Summary.DefaultGroupSummaryItems = <>
            DataController.Summary.FooterSummaryItems = <>
            DataController.Summary.SummaryGroups = <>
            FilterRow.InfoText = #1050#1083#1080#1082#1085#1077#1090#1077' '#1090#1091#1082#1072' '#1079#1072' '#1076#1072' '#1076#1077#1092#1080#1085#1080#1088#1072#1090#1077' '#1092#1080#1083#1090#1077#1088
            FilterRow.Visible = True
            OptionsCustomize.ColumnsQuickCustomization = True
            OptionsData.Deleting = False
            OptionsData.Editing = False
            OptionsData.Inserting = False
            OptionsView.NoDataToDisplayInfoText = '<'#1053#1077#1084#1072' '#1089#1091#1088#1086#1074#1080#1085#1080'>'
            OptionsView.Indicator = True
            object cxGridDBTableView4ID: TcxGridDBColumn
              Caption = #1064#1080#1092#1088#1072
              DataBinding.FieldName = 'ID'
              Visible = False
            end
            object cxGridDBTableView4ID_RABOTEN_NALOG: TcxGridDBColumn
              Caption = #1064#1080#1092#1088#1072' '#1085#1072' '#1056#1053
              DataBinding.FieldName = 'ID_RABOTEN_NALOG'
              Visible = False
            end
            object cxGridDBTableView4VID_SUROVINA: TcxGridDBColumn
              Caption = #1042#1080#1076' '#1085#1072' '#1089#1091#1088#1086#1074#1080#1085#1072
              DataBinding.FieldName = 'VID_SUROVINA'
              Width = 26
            end
            object cxGridDBTableView4SUROVINA: TcxGridDBColumn
              Caption = #1064#1080#1092#1088#1072' '#1085#1072' '#1089#1091#1088#1086#1074#1080#1085#1072
              DataBinding.FieldName = 'SUROVINA'
              Width = 75
            end
            object cxGridDBTableView4NAZIV_SUROVINA: TcxGridDBColumn
              Caption = #1057#1091#1088#1086#1074#1080#1085#1072
              DataBinding.FieldName = 'NAZIV_SUROVINA'
              Width = 246
            end
            object cxGridDBTableView4KOLICINA: TcxGridDBColumn
              Caption = #1050#1086#1083#1080#1095#1080#1085#1072
              DataBinding.FieldName = 'KOLICINA'
            end
            object cxGridDBTableView4MERKA: TcxGridDBColumn
              Caption = #1064#1080#1092#1088#1072' '#1085#1072' '#1084#1077#1088#1082#1072
              DataBinding.FieldName = 'MERKA'
              Width = 73
            end
            object cxGridDBTableView4NAZIV_MERKA: TcxGridDBColumn
              Caption = #1052#1077#1088#1082#1072
              DataBinding.FieldName = 'NAZIV_MERKA'
              Width = 154
            end
            object cxGridDBTableView4PRODUKCISKI_LOT: TcxGridDBColumn
              Caption = #1055#1088#1086#1076#1091#1082#1094#1080#1089#1082#1080' '#1083#1086#1090
              DataBinding.FieldName = 'PRODUKCISKI_LOT'
            end
          end
          object cxGridLevel4: TcxGridLevel
            GridView = cxGridDBTableView4
          end
        end
      end
      object PanelSurovini: TPanel
        Left = 0
        Top = 270
        Width = 1068
        Height = 78
        Align = alTop
        TabOrder = 2
        Visible = False
        DesignSize = (
          1068
          78)
        object cxLabel32: TcxLabel
          Left = 67
          Top = 39
          Caption = #1057#1091#1088#1086#1074#1080#1085#1072
          Style.TextColor = clRed
          Properties.Alignment.Horz = taRightJustify
          Properties.WordWrap = True
          Transparent = True
          Width = 53
          AnchorX = 120
        end
        object txtVidSurovina: TcxDBTextEdit
          Tag = 1
          Left = 129
          Top = 39
          DataBinding.DataField = 'VID_SUROVINA'
          DataBinding.DataSource = dm.dsRNSurovini
          ParentFont = False
          TabOrder = 3
          OnEnter = cxDBTextEditAllEnter
          OnExit = cxDBTextEditAllExit
          OnKeyDown = EnterKakoTab
          Width = 64
        end
        object txtSurovina: TcxDBTextEdit
          Tag = 1
          Left = 192
          Top = 39
          DataBinding.DataField = 'SUROVINA'
          DataBinding.DataSource = dm.dsRNSurovini
          ParentFont = False
          TabOrder = 4
          OnEnter = cxDBTextEditAllEnter
          OnExit = cxDBTextEditAllExit
          OnKeyDown = EnterKakoTab
          Width = 63
        end
        object cbSurovina: TcxLookupComboBox
          Left = 255
          Top = 39
          Properties.DropDownSizeable = True
          Properties.KeyFieldNames = 'ARTVID;id'
          Properties.ListColumns = <
            item
              FieldName = 'ARTVID'
            end
            item
              FieldName = 'id'
            end
            item
              FieldName = 'naziv'
            end>
          Properties.ListFieldIndex = 2
          Properties.ListOptions.SyncMode = True
          Properties.ListSource = dm.dsArtikalRN
          TabOrder = 5
          OnEnter = cxDBTextEditAllEnter
          OnExit = cxDBTextEditAllExit
          OnKeyDown = EnterKakoTab
          Width = 303
        end
        object cxLabel33: TcxLabel
          Left = 602
          Top = 39
          Caption = #1050#1086#1083#1080#1095#1080#1085#1072
          Style.TextColor = clRed
          Properties.Alignment.Horz = taRightJustify
          Transparent = True
          AnchorX = 655
        end
        object txtKolicina: TcxDBTextEdit
          Tag = 1
          Left = 661
          Top = 39
          DataBinding.DataField = 'KOLICINA'
          DataBinding.DataSource = dm.dsRNSurovini
          ParentFont = False
          Style.BorderStyle = ebsUltraFlat
          Style.Font.Charset = RUSSIAN_CHARSET
          Style.Font.Color = clWindowText
          Style.Font.Height = -11
          Style.Font.Name = 'Tahoma'
          Style.Font.Style = []
          Style.LookAndFeel.NativeStyle = False
          Style.LookAndFeel.SkinName = ''
          Style.TextStyle = []
          Style.TransparentBorder = False
          Style.IsFontAssigned = True
          StyleDisabled.LookAndFeel.NativeStyle = False
          StyleDisabled.LookAndFeel.SkinName = ''
          StyleFocused.LookAndFeel.NativeStyle = False
          StyleFocused.LookAndFeel.SkinName = ''
          StyleHot.LookAndFeel.NativeStyle = False
          StyleHot.LookAndFeel.SkinName = ''
          TabOrder = 10
          OnEnter = cxDBTextEditAllEnter
          OnExit = cxDBTextEditAllExit
          OnKeyDown = EnterKakoTab
          Width = 89
        end
        object cxLabel34: TcxLabel
          Left = 556
          Top = 17
          Caption = #1052#1077#1088#1082#1072
          Style.TextColor = clRed
          Properties.Alignment.Horz = taRightJustify
          Transparent = True
          AnchorX = 592
        end
        object txtMerka: TcxDBTextEdit
          Tag = 1
          Left = 598
          Top = 17
          DataBinding.DataField = 'MERKA'
          DataBinding.DataSource = dm.dsRNSurovini
          ParentFont = False
          Style.BorderStyle = ebsUltraFlat
          Style.Font.Charset = RUSSIAN_CHARSET
          Style.Font.Color = clWindowText
          Style.Font.Height = -11
          Style.Font.Name = 'Tahoma'
          Style.Font.Style = []
          Style.LookAndFeel.NativeStyle = False
          Style.LookAndFeel.SkinName = ''
          Style.TextStyle = []
          Style.TransparentBorder = False
          Style.IsFontAssigned = True
          StyleDisabled.LookAndFeel.NativeStyle = False
          StyleDisabled.LookAndFeel.SkinName = ''
          StyleDisabled.TextColor = clWindowText
          StyleFocused.LookAndFeel.NativeStyle = False
          StyleFocused.LookAndFeel.SkinName = ''
          StyleHot.LookAndFeel.NativeStyle = False
          StyleHot.LookAndFeel.SkinName = ''
          TabOrder = 7
          OnEnter = cxDBTextEditAllEnter
          OnExit = cxDBTextEditAllExit
          OnKeyDown = EnterKakoTab
          Width = 61
        end
        object cbMerka: TcxDBLookupComboBox
          Left = 660
          Top = 16
          Hint = #1050#1083#1080#1082#1085#1077#1090#1077' '#1076#1074#1072' '#1087#1072#1090#1080' '#1079#1072' '#1074#1085#1077#1089' '#1085#1072' '#1085#1086#1074#1072' '#1077#1076#1080#1085#1080#1094#1072' '#1084#1077#1088#1082#1072
          RepositoryItem = dm.erLookupComboBox
          AutoSize = False
          DataBinding.DataField = 'MERKA'
          DataBinding.DataSource = dm.dsRNSurovini
          Properties.KeyFieldNames = 'ID'
          Properties.ListColumns = <
            item
              FieldName = 'NAZIV'
            end>
          Properties.ListSource = dmMat.dsMerka
          TabOrder = 8
          OnEnter = cxDBTextEditAllEnter
          OnExit = cxDBTextEditAllExit
          OnKeyDown = EnterKakoTab
          Height = 21
          Width = 139
        end
        object cxLabel35: TcxLabel
          Left = 9
          Top = 20
          Caption = #1052#1072#1075#1072#1094#1080#1085' '#1079#1072' '#1089#1091#1088#1086#1074#1080#1085#1080
          Style.TextColor = clNavy
          Properties.Alignment.Horz = taRightJustify
          Properties.WordWrap = True
          Transparent = True
          Width = 111
          AnchorX = 120
        end
        object txtMagacin: TcxDBTextEdit
          Left = 130
          Top = 16
          DataBinding.DataField = 'ID_RE'
          DataBinding.DataSource = dm.dsRNSurovini
          TabOrder = 0
          OnEnter = cxDBTextEditAllEnter
          OnExit = cxDBTextEditAllExit
          OnKeyDown = EnterKakoTab
          Width = 63
        end
        object cbMagacin: TcxDBLookupComboBox
          Left = 192
          Top = 16
          DataBinding.DataField = 'ID_RE'
          DataBinding.DataSource = dm.dsRNSurovini
          Properties.DropDownSizeable = True
          Properties.KeyFieldNames = 'ID'
          Properties.ListColumns = <
            item
              FieldName = 'ID'
            end
            item
              FieldName = 'naziv'
            end>
          Properties.ListFieldIndex = 1
          Properties.ListSource = dm.dsRE
          TabOrder = 1
          OnEnter = cxDBTextEditAllEnter
          OnExit = cxDBTextEditAllExit
          OnKeyDown = EnterKakoTab
          Width = 240
        end
        object cxButton2: TcxButton
          Left = 878
          Top = 45
          Width = 75
          Height = 25
          Action = aZapisi
          Anchors = [akRight, akBottom]
          TabOrder = 13
        end
        object cxButton4: TcxButton
          Left = 959
          Top = 45
          Width = 75
          Height = 25
          Action = aOtkazi
          Anchors = [akRight, akBottom]
          TabOrder = 14
        end
        object lblMernaEdinica: TcxDBLabel
          Left = 753
          Top = 39
          DataBinding.DataField = 'MERKA'
          DataBinding.DataSource = dm.dsRNSurovini
          Transparent = True
          Height = 19
          Width = 64
        end
        object cxLabel2: TcxLabel
          Left = 823
          Top = 13
          Caption = #1055#1088#1086#1076#1091#1082#1094#1080#1089#1082#1080' '#1083#1086#1090
          Style.TextColor = clNavy
          Properties.Alignment.Horz = taRightJustify
          Properties.WordWrap = True
          Transparent = True
          Width = 74
          AnchorX = 897
        end
        object txtProdukciskiLot: TcxDBTextEdit
          Left = 901
          Top = 18
          DataBinding.DataField = 'PRODUKCISKI_LOT'
          DataBinding.DataSource = dm.dsRNSurovini
          ParentFont = False
          Style.BorderStyle = ebsUltraFlat
          Style.Font.Charset = RUSSIAN_CHARSET
          Style.Font.Color = clWindowText
          Style.Font.Height = -11
          Style.Font.Name = 'Tahoma'
          Style.Font.Style = []
          Style.LookAndFeel.NativeStyle = False
          Style.LookAndFeel.SkinName = ''
          Style.TextStyle = []
          Style.TransparentBorder = False
          Style.IsFontAssigned = True
          StyleDisabled.LookAndFeel.NativeStyle = False
          StyleDisabled.LookAndFeel.SkinName = ''
          StyleFocused.LookAndFeel.NativeStyle = False
          StyleFocused.LookAndFeel.SkinName = ''
          StyleHot.LookAndFeel.NativeStyle = False
          StyleHot.LookAndFeel.SkinName = ''
          TabOrder = 9
          OnEnter = cxDBTextEditAllEnter
          OnExit = cxDBTextEditAllExit
          OnKeyDown = EnterKakoTab
          Width = 157
        end
      end
      object cxGroupBox7: TcxGroupBox
        Left = 0
        Top = 217
        Align = alTop
        Caption = #1052#1086#1084#1077#1085#1090#1072#1083#1077#1085' '#1089#1090#1072#1090#1091#1089' '#1085#1072' '#1056#1072#1073#1086#1090#1085#1080#1086#1090' '#1085#1072#1083#1086#1075
        Style.Edges = [bBottom]
        Style.TextStyle = [fsBold]
        TabOrder = 3
        Transparent = True
        Height = 53
        Width = 1068
        object cxDBLabel2: TcxDBLabel
          Left = 96
          Top = 20
          DataBinding.DataField = 'NAZIV_STATUS_STAVKI'
          DataBinding.DataSource = dm.dsRabotenNalog
          Properties.Alignment.Horz = taCenter
          Properties.Alignment.Vert = taVCenter
          Style.BorderStyle = ebsUltraFlat
          Style.HotTrack = False
          Style.LookAndFeel.NativeStyle = False
          Style.TextColor = clNavy
          Style.TextStyle = [fsBold]
          StyleDisabled.LookAndFeel.NativeStyle = False
          StyleFocused.LookAndFeel.NativeStyle = False
          StyleHot.LookAndFeel.NativeStyle = False
          Height = 21
          Width = 329
          AnchorX = 261
          AnchorY = 31
        end
        object cxLabel1: TcxLabel
          Left = 527
          Top = 24
          Caption = #1055#1088#1086#1084#1077#1085#1080' '#1074#1086
          Style.TextColor = clNavy
          Properties.Alignment.Horz = taRightJustify
          Properties.WordWrap = True
          Transparent = True
          Width = 62
          AnchorX = 589
        end
        object cxDBButtonEdit1: TcxDBButtonEdit
          Left = 595
          Top = 20
          Hint = #1050#1083#1080#1082#1085#1077#1090#1077', '#1079#1072' '#1076#1072' '#1087#1088#1086#1084#1077#1085#1080#1090#1077' '#1089#1090#1072#1090#1091#1089' '#1085#1072' '#1056#1072#1073#1086#1090#1077#1085' '#1085#1072#1083#1086#1075
          Touch.ParentTabletOptions = False
          Touch.TabletOptions = [toPressAndHold]
          DataBinding.DataField = 'NAZIV_STATUS_STAVKI'
          DataBinding.DataSource = dm.dsRabotenNalog
          ParentFont = False
          Properties.Buttons = <
            item
              Caption = 'Status'
              Default = True
              Kind = bkText
            end>
          Properties.ViewStyle = vsButtonsOnly
          Style.BorderStyle = ebsUltraFlat
          Style.Font.Charset = RUSSIAN_CHARSET
          Style.Font.Color = clWindowText
          Style.Font.Height = -11
          Style.Font.Name = 'Tahoma'
          Style.Font.Style = []
          Style.LookAndFeel.Kind = lfUltraFlat
          Style.LookAndFeel.NativeStyle = False
          Style.Shadow = False
          Style.TextStyle = []
          Style.ButtonStyle = btsDefault
          Style.ButtonTransparency = ebtNone
          Style.IsFontAssigned = True
          StyleDisabled.LookAndFeel.Kind = lfUltraFlat
          StyleDisabled.LookAndFeel.NativeStyle = False
          StyleFocused.LookAndFeel.Kind = lfUltraFlat
          StyleFocused.LookAndFeel.NativeStyle = False
          StyleHot.LookAndFeel.Kind = lfUltraFlat
          StyleHot.LookAndFeel.NativeStyle = False
          TabOrder = 2
          OnClick = aSmeniStatusExecute
          Width = 238
        end
      end
    end
  end
  object cxGridPopupMenu1: TcxGridPopupMenu
    Grid = cxGrid1
    PopupMenus = <>
    Left = 624
    Top = 32
  end
  object PopupMenu1: TPopupMenu
    Left = 696
    Top = 32
    object N1: TMenuItem
      Caption = #1057#1085#1080#1084#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      ShortCut = 49235
      OnClick = SaveToIniFileExecute
    end
    object Excel1: TMenuItem
      Caption = #1057#1085#1080#1084#1080' '#1075#1086' '#1074#1086' Excel'
      ShortCut = 49221
    end
  end
  object dxBarManager1: TdxBarManager
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Segoe UI'
    Font.Style = []
    Categories.Strings = (
      'Default')
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    ImageOptions.Images = dmRes.cxSmallImages
    ImageOptions.LargeImages = dmRes.cxLargeImages
    PopupMenuLinks = <>
    UseSystemFont = True
    Left = 568
    Top = 40
    DockControlHeights = (
      0
      0
      0
      0)
    object dxBarManager1Bar2: TdxBar
      Caption = #1040#1082#1094#1080#1080
      CaptionButtons = <>
      DockedLeft = 177
      DockedTop = 0
      FloatLeft = 789
      FloatTop = 120
      FloatClientWidth = 51
      FloatClientHeight = 120
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton5'
        end
        item
          Visible = True
          ItemName = 'dxBarSubItem1'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar3: TdxBar
      Caption = #1048#1079#1083#1077#1079
      CaptionButtons = <>
      DockedLeft = 315
      DockedTop = 0
      FloatLeft = 718
      FloatTop = 62
      FloatClientWidth = 51
      FloatClientHeight = 80
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton7'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton8'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar4: TdxBar
      Caption = #1055#1077#1095#1072#1090#1077#1114#1077
      CaptionButtons = <>
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 676
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton12'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton15'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton9'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarLargeButton13'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1BarIzgledGrid: TdxBar
      Caption = #1058#1072#1073#1077#1083#1072
      CaptionButtons = <>
      DockedLeft = 363
      DockedTop = 0
      FloatLeft = 808
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLBtnSnimiIzgled'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarLBtnBrisiIzgled'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar5: TdxBar
      Caption = '  '#1057#1091#1088#1086#1074#1080#1085#1072' '
      CaptionButtons = <>
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 1034
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton10'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton18'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton19'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarLargeButton2: TdxBarLargeButton
      Action = aAzurirajj
      Category = 0
    end
    object dxBarLargeButton3: TdxBarLargeButton
      Action = aBrisii
      Category = 0
    end
    object dxBarLargeButton4: TdxBarLargeButton
      Action = aSnimiIzgled
      Category = 0
      ScreenTip = tipSnimiIzgled
    end
    object dxBarLargeButton5: TdxBarLargeButton
      Action = aZacuvajExcel
      Category = 0
      ScreenTip = tipZacuvajExcel
    end
    object dxBarLargeButton6: TdxBarLargeButton
      Action = aPecatiTabela
      Caption = #1055#1077#1095#1072#1090#1080' '#1090#1072#1073#1077#1083#1072
      Category = 0
    end
    object dxBarLargeButton7: TdxBarLargeButton
      Action = aHelp
      Category = 0
    end
    object dxBarLargeButton8: TdxBarLargeButton
      Action = aIzlez
      Category = 0
    end
    object dxBarLargeButton1: TdxBarLargeButton
      Action = aNovv
      Category = 0
    end
    object dxBarLargeButton9: TdxBarLargeButton
      Action = aSnimiPecatenje
      Category = 0
      ScreenTip = tipSnimiKonf
    end
    object dxBarLargeButton11: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object ribbonFilterReset: TdxBarLargeButton
      Caption = #1056#1077#1089#1077#1090#1080#1088#1072#1112' '#1092#1080#1083#1090#1077#1088
      Category = 0
      Hint = #1056#1077#1089#1077#1090#1080#1088#1072#1112' '#1092#1080#1083#1090#1077#1088
      Visible = ivAlways
    end
    object dxBarLargeButton12: TdxBarLargeButton
      Action = aPodesuvanjePecatenje
      Category = 0
      ScreenTip = tipDizajnReport
    end
    object dxBarLargeButton13: TdxBarLargeButton
      Action = aBrisiPodesuvanjePecatenje
      Category = 0
      ScreenTip = tipBrisiKonf
    end
    object dxBarLargeButton14: TdxBarLargeButton
      Caption = #1048#1079#1073#1088#1080#1096#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      Category = 0
      Visible = ivAlways
      LargeImageIndex = 40
    end
    object dxBarLargeButton15: TdxBarLargeButton
      Action = aPageSetup
      Category = 0
      ScreenTip = tipSetiranjeStrana
    end
    object dxBarLargeButton16: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object dxBarLargeButton17: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object cxBarEditItem1: TcxBarEditItem
      Caption = 'New Item'
      Category = 0
      Hint = 'New Item'
      Visible = ivAlways
      PropertiesClassName = 'TcxCheckBoxProperties'
    end
    object dxBarSubItem1: TdxBarSubItem
      Caption = #1055#1077#1095#1072#1090#1077#1114#1077
      Category = 0
      Visible = ivAlways
      ImageIndex = 30
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton6'
        end>
    end
    object dxBarLBtnSnimiIzgled: TdxBarLargeButton
      Action = aSnimiIzgled
      Category = 0
    end
    object dxBarLBtnBrisiIzgled: TdxBarLargeButton
      Action = aBrisiIzgled
      Category = 0
    end
    object dxBarLargeButton10: TdxBarLargeButton
      Action = aNova
      Category = 0
    end
    object dxBarLargeButton18: TdxBarLargeButton
      Action = aAzurirajS
      Category = 0
    end
    object dxBarLargeButton19: TdxBarLargeButton
      Action = aBrisiS
      Category = 0
    end
    object dxBarLargeButton20: TdxBarLargeButton
      Action = aOtvoreniRN
      Category = 0
      SyncImageIndex = False
      ImageIndex = -1
    end
    object dxBarLargeButton21: TdxBarLargeButton
      Action = aSiteRN
      Category = 0
    end
  end
  object ActionList1: TActionList
    Images = dmRes.cxSmallImages
    Left = 624
    Top = 80
    object aNov: TAction
      Caption = #1053#1086#1074
      ImageIndex = 10
      ShortCut = 116
      OnExecute = aNovExecute
    end
    object aAzuriraj: TAction
      Caption = #1040#1078#1091#1088#1080#1088#1072#1112
      ImageIndex = 12
      ShortCut = 117
      OnExecute = aAzurirajExecute
    end
    object aBrisi: TAction
      Caption = #1041#1088#1080#1096#1080
      ImageIndex = 11
      ShortCut = 119
      OnExecute = aBrisiExecute
    end
    object aSnimiIzgled: TAction
      Caption = #1057#1085#1080#1084#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      ImageIndex = 9
      ShortCut = 24659
      OnExecute = aSnimiIzgledExecute
    end
    object aZacuvajExcel: TAction
      Caption = #1047#1072#1095#1091#1074#1072#1112' '#1074#1086' Excel'
      ImageIndex = 9
      ShortCut = 24645
      OnExecute = aZacuvajExcelExecute
    end
    object aPecatiTabela: TAction
      Caption = #1055#1077#1095#1072#1090#1080
      ImageIndex = 30
      OnExecute = aPecatiTabelaExecute
    end
    object aHelp: TAction
      Caption = #1055#1086#1084#1086#1096
      ImageIndex = 24
    end
    object aIzlez: TAction
      Caption = #1048#1079#1083#1077#1079
      ImageIndex = 25
      OnExecute = aIzlezExecute
    end
    object aZapisi: TAction
      Caption = #1047#1072#1087#1080#1096#1080
      ImageIndex = 7
      ShortCut = 120
      OnExecute = aZapisiExecute
    end
    object aOtkazi: TAction
      Caption = #1054#1090#1082#1072#1078#1080
      ImageIndex = 21
      ShortCut = 27
      OnExecute = aOtkaziExecute
    end
    object aRefresh: TAction
      Caption = 'aRefresh'
      ImageIndex = 18
      ShortCut = 118
      OnExecute = aRefreshExecute
    end
    object aSnimiPecatenje: TAction
      Caption = #1057#1085#1080#1084#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      ImageIndex = 39
      OnExecute = aSnimiPecatenjeExecute
    end
    object aPodesuvanjePecatenje: TAction
      Caption = #1044#1080#1079#1072#1112#1085' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090
      ImageIndex = 38
      OnExecute = aPodesuvanjePecatenjeExecute
    end
    object aBrisiPodesuvanjePecatenje: TAction
      Caption = #1041#1088#1080#1096#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      ImageIndex = 37
      OnExecute = aBrisiPodesuvanjePecatenjeExecute
    end
    object aPageSetup: TAction
      Caption = #1057#1077#1090#1080#1088#1072#1114#1077' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072
      ImageIndex = 38
      OnExecute = aPageSetupExecute
    end
    object aFormConfig: TAction
      Caption = 'aFormConfig'
      ShortCut = 24699
      OnExecute = aFormConfigExecute
    end
    object aBrisiIzgled: TAction
      Caption = #1041#1088#1080#1096#1080' '#1075#1086' '#1079#1072#1095#1091#1074#1072#1085#1080#1086#1090' '#1080#1079#1075#1083#1077#1076
      ImageIndex = 56
      OnExecute = aBrisiIzgledExecute
    end
    object aListaRN: TAction
      Caption = #1051#1080#1089#1090#1072' '#1085#1072' '#1056#1053
      ImageIndex = 8
      OnExecute = aListaRNExecute
    end
    object aNova: TAction
      Caption = #1053#1086#1074#1072
      ImageIndex = 10
      OnExecute = aNovaExecute
    end
    object aNovv: TAction
      Caption = #1053#1086#1074
      ImageIndex = 10
      OnExecute = aNovvExecute
    end
    object aAzurirajS: TAction
      Caption = #1040#1078#1091#1088#1080#1088#1072#1112
      ImageIndex = 12
      OnExecute = aAzurirajSExecute
    end
    object aAzurirajj: TAction
      Caption = #1040#1078#1091#1088#1080#1088#1072#1112
      ImageIndex = 12
      OnExecute = aAzurirajjExecute
    end
    object aBrisiS: TAction
      Caption = #1041#1088#1080#1096#1080
      ImageIndex = 11
      OnExecute = aBrisiSExecute
    end
    object aBrisii: TAction
      Caption = #1041#1088#1080#1096#1080
      ImageIndex = 11
      OnExecute = aBrisiiExecute
    end
    object aPogledRN: TAction
      Caption = #1055#1086#1075#1083#1077#1076' '#1085#1072' '#1056#1053
      OnExecute = aPogledRNExecute
    end
    object aSiteRN: TAction
      Caption = #1057#1080#1090#1077' '#1056#1053
      ImageIndex = 1
      OnExecute = aSiteRNExecute
    end
    object aOtvoreniRN: TAction
      Caption = #1054#1090#1074#1086#1088#1077#1085#1080' '#1056#1053
      ImageIndex = 2
      OnExecute = aOtvoreniRNExecute
    end
    object aSmeniStatus: TAction
      Caption = 'aSmeniStatus'
      OnExecute = aSmeniStatusExecute
    end
    object aPromeniStatus: TAction
      Caption = #1055#1088#1086#1084#1077#1085#1080' '#1089#1090#1072#1090#1091#1089
      OnExecute = aPromeniStatusExecute
    end
    object Action1: TAction
      Caption = 'Action1'
    end
  end
  object dxComponentPrinter1: TdxComponentPrinter
    CurrentLink = dxComponentPrinter1Link1
    Version = 0
    Left = 768
    Top = 56
    object dxComponentPrinter1Link1: TdxGridReportLink
      Component = cxGrid1
      PrinterPage.DMPaper = 9
      PrinterPage.Footer = 6350
      PrinterPage.GrayShading = True
      PrinterPage.Header = 6350
      PrinterPage.Margins.Bottom = 12700
      PrinterPage.Margins.Left = 12700
      PrinterPage.Margins.Right = 12700
      PrinterPage.Margins.Top = 12700
      PrinterPage.Orientation = poLandscape
      PrinterPage.PageFooter.RightTitle.Strings = (
        #1057#1090#1088'.[Page #] '#1086#1076' [Total Pages]')
      PrinterPage.PageSize.X = 210000
      PrinterPage.PageSize.Y = 297000
      PrinterPage._dxMeasurementUnits_ = 0
      PrinterPage._dxLastMU_ = 2
      ReportDocument.CreationDate = 40336.425359629630000000
      OptionsFormatting.UseNativeStyles = True
      OptionsOnEveryPage.Caption = False
      OptionsOnEveryPage.FilterBar = False
      OptionsView.Caption = False
      OptionsView.FilterBar = False
      StyleRepository = dmRes.cxStyleRepositoryPrinting
      Styles.StyleSheet = dmRes.dxGridReportLinkStyleSheet
      BuiltInReportLink = True
    end
  end
  object dxBarScreenTipRepository1: TdxBarScreenTipRepository
    Left = 504
    Top = 32
    object tipSnimiIzgled: TdxBarScreenTip
      Header.Text = #1057#1085#1080#1084#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      Description.Text = #1047#1072#1095#1091#1074#1072#1112' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090' '#1085#1072' '#1092#1086#1088#1084#1072#1090#1072' '#1080' '#1085#1072' '#1090#1072#1073#1077#1083#1072#1090#1072
    end
    object tipZacuvajExcel: TdxBarScreenTip
      Header.Text = #1047#1072#1095#1091#1074#1072#1112' '#1074#1086' Excel'
      Description.Text = #1057#1085#1080#1084#1080' '#1075#1080' '#1087#1086#1076#1072#1090#1086#1094#1080#1090#1077' '#1086#1076' '#1090#1072#1073#1077#1083#1072#1090#1072' '#1074#1086' Excel '#1092#1086#1088#1084#1072#1090
    end
    object tipDizajnReport: TdxBarScreenTip
      Header.Text = #1044#1080#1079#1072#1112#1085' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090
      Description.Text = #1055#1086#1076#1077#1089#1091#1074#1072#1114#1072' '#1085#1072' '#1080#1079#1075#1083#1077#1076#1086#1090' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090' '#1085#1072' '#1090#1072#1073#1077#1083#1072#1090#1072
    end
    object tipSetiranjeStrana: TdxBarScreenTip
      Header.Text = #1057#1077#1090#1080#1088#1072#1114#1077' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072
      Description.Text = #1055#1086#1076#1077#1089#1091#1074#1072#1114#1077' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072' '#1085#1072' '#1082#1086#1112#1072' '#1116#1077' '#1089#1077' '#1087#1077#1095#1072#1090#1080' '#1090#1072#1073#1077#1083#1072#1090#1072
    end
    object tipSnimiKonf: TdxBarScreenTip
      Header.Text = #1057#1085#1080#1084#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      Description.Text = #1047#1072#1095#1091#1074#1072#1112' '#1075#1080' '#1087#1086#1076#1077#1089#1091#1074#1072#1114#1072#1090#1072' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090' '#1080' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072' '#1079#1072' '#1087#1077#1095#1072#1090#1077#1114#1077
    end
    object tipBrisiKonf: TdxBarScreenTip
      Header.Text = #1041#1088#1080#1096#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      Description.Text = #1048#1079#1073#1088#1080#1096#1080' '#1075#1080' '#1087#1086#1076#1077#1089#1091#1074#1072#1114#1072#1090#1072' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090' '#1080' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072' '#1079#1072' '#1087#1077#1095#1072#1090#1077#1114#1077
    end
  end
  object OpenPictureDialog1: TOpenPictureDialog
    Left = 392
    Top = 56
  end
  object cxPropertiesStore1: TcxPropertiesStore
    Components = <>
    StorageName = 'cxPropertiesStore1'
    Left = 392
    Top = 8
  end
end
