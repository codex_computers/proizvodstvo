inherited frmPosledovatelnost: TfrmPosledovatelnost
  Caption = #1055#1086#1089#1083#1077#1076#1086#1074#1072#1090#1077#1083#1085#1086#1089#1090' '#1085#1072' '#1050#1086#1085#1090#1088#1086#1083#1085#1072' '#1058#1086#1095#1082#1072
  ClientHeight = 591
  ExplicitWidth = 749
  ExplicitHeight = 629
  PixelsPerInch = 96
  TextHeight = 13
  inherited lPanel: TPanel
    Height = 286
    ExplicitHeight = 286
    inherited cxGrid1: TcxGrid
      Height = 282
      ExplicitHeight = 282
      inherited cxGrid1DBTableView1: TcxGridDBTableView
        DataController.DataSource = dm.dsPosledovatelnostRE
        object cxGrid1DBTableView1ID: TcxGridDBColumn
          Caption = #1064#1080#1092#1088#1072
          DataBinding.FieldName = 'ID'
          Visible = False
        end
        object cxGrid1DBTableView1SEKVENCA: TcxGridDBColumn
          Caption = #1057#1077#1082#1074#1077#1085#1094#1072
          DataBinding.FieldName = 'SEKVENCA'
        end
        object cxGrid1DBTableView1ID_KT: TcxGridDBColumn
          Caption = #1064#1080#1092#1088#1072' '#1085#1072' '#1050#1058
          DataBinding.FieldName = 'ID_KT'
        end
        object cxGrid1DBTableView1NAZIV_KT: TcxGridDBColumn
          Caption = #1050#1086#1085#1090#1088#1086#1083#1085#1072' '#1090#1086#1095#1082#1072
          DataBinding.FieldName = 'NAZIV_KT'
        end
        object cxGrid1DBTableView1ID_FIRMA: TcxGridDBColumn
          Caption = #1060#1080#1088#1084#1072
          DataBinding.FieldName = 'ID_FIRMA'
          Visible = False
        end
        object cxGrid1DBTableView1NIVO: TcxGridDBColumn
          Caption = #1053#1080#1074#1086
          DataBinding.FieldName = 'NIVO'
        end
        object cxGrid1DBTableView1SLEDNA_KT: TcxGridDBColumn
          Caption = #1064#1080#1092#1088#1072' '#1085#1072' '#1089#1083#1077#1076#1085#1072' '#1050#1058
          DataBinding.FieldName = 'SLEDNA_KT'
          Visible = False
        end
        object cxGrid1DBTableView1NAZIV_SLEDNA_KT: TcxGridDBColumn
          Caption = #1057#1083#1077#1076#1085#1072' '#1050#1058
          DataBinding.FieldName = 'NAZIV_SLEDNA_KT'
        end
      end
    end
  end
  inherited dPanel: TPanel
    Top = 412
    Height = 156
    ExplicitTop = 412
    ExplicitHeight = 156
    inherited Label1: TLabel
      Left = 380
      Top = 121
      Visible = False
      ExplicitLeft = 380
      ExplicitTop = 121
    end
    object Label7: TLabel [1]
      Left = 17
      Top = 32
      Width = 126
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1050#1086#1085#1090#1088#1086#1083#1085#1072' '#1090#1086#1095#1082#1072' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label2: TLabel [2]
      Left = 72
      Top = 54
      Width = 70
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1057#1077#1082#1074#1077#1085#1094#1072' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label3: TLabel [3]
      Left = 16
      Top = 78
      Width = 126
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1057#1083#1077#1076#1085#1072' '#1050#1058' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label4: TLabel [4]
      Left = 72
      Top = 100
      Width = 70
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1053#1080#1074#1086' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    inherited Sifra: TcxDBTextEdit
      Tag = 0
      Left = 436
      Top = 118
      TabOrder = 8
      Visible = False
      ExplicitLeft = 436
      ExplicitTop = 118
    end
    inherited OtkaziButton: TcxButton
      Top = 116
      TabOrder = 7
      ExplicitTop = 116
    end
    inherited ZapisiButton: TcxButton
      Top = 116
      TabOrder = 6
      ExplicitTop = 116
    end
    object txtKT: TcxDBTextEdit
      Tag = 1
      Left = 148
      Top = 29
      BeepOnEnter = False
      DataBinding.DataField = 'ID_KT'
      DataBinding.DataSource = dm.dsPosledovatelnostRE
      ParentFont = False
      Properties.BeepOnError = True
      StyleDisabled.BorderStyle = ebsUltraFlat
      StyleDisabled.Color = clWindow
      StyleDisabled.TextColor = clWindowText
      TabOrder = 0
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 86
    end
    object cbKT: TcxDBLookupComboBox
      Left = 235
      Top = 29
      Anchors = [akRight, akBottom]
      DataBinding.DataField = 'ID_KT'
      DataBinding.DataSource = dm.dsPosledovatelnostRE
      Properties.ClearKey = 46
      Properties.DropDownSizeable = True
      Properties.ImmediatePost = True
      Properties.KeyFieldNames = 'ID'
      Properties.ListColumns = <
        item
          FieldName = 'ID'
        end
        item
          FieldName = 'naziv'
        end>
      Properties.ListFieldIndex = 1
      Properties.ListSource = dm.dsKT
      StyleDisabled.BorderStyle = ebsUltraFlat
      StyleDisabled.Color = clWindow
      StyleDisabled.TextColor = clWindowText
      TabOrder = 1
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 357
    end
    object txtSekvenca: TcxDBTextEdit
      Tag = 1
      Left = 148
      Top = 51
      BeepOnEnter = False
      DataBinding.DataField = 'SEKVENCA'
      DataBinding.DataSource = dm.dsPosledovatelnostRE
      ParentFont = False
      Properties.BeepOnError = True
      Properties.CharCase = ecUpperCase
      Style.Shadow = False
      TabOrder = 2
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 86
    end
    object txtSlednaKT: TcxDBTextEdit
      Left = 148
      Top = 73
      BeepOnEnter = False
      DataBinding.DataField = 'SLEDNA_KT'
      DataBinding.DataSource = dm.dsPosledovatelnostRE
      ParentFont = False
      Properties.BeepOnError = True
      StyleDisabled.BorderStyle = ebsUltraFlat
      StyleDisabled.Color = clWindow
      StyleDisabled.TextColor = clWindowText
      TabOrder = 3
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 86
    end
    object cbSlednaKT: TcxDBLookupComboBox
      Left = 235
      Top = 73
      Anchors = [akRight, akBottom]
      DataBinding.DataField = 'SLEDNA_KT'
      DataBinding.DataSource = dm.dsPosledovatelnostRE
      Properties.ClearKey = 46
      Properties.DropDownSizeable = True
      Properties.ImmediatePost = True
      Properties.KeyFieldNames = 'ID'
      Properties.ListColumns = <
        item
          FieldName = 'ID'
        end
        item
          FieldName = 'naziv'
        end>
      Properties.ListFieldIndex = 1
      Properties.ListSource = dm.dsKT
      StyleDisabled.BorderStyle = ebsUltraFlat
      StyleDisabled.Color = clWindow
      StyleDisabled.TextColor = clWindowText
      TabOrder = 4
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 357
    end
    object txtNivo: TcxDBTextEdit
      Tag = 1
      Left = 148
      Top = 97
      BeepOnEnter = False
      DataBinding.DataField = 'NIVO'
      DataBinding.DataSource = dm.dsPosledovatelnostRE
      ParentFont = False
      Properties.BeepOnError = True
      Properties.CharCase = ecUpperCase
      Style.Shadow = False
      TabOrder = 5
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 86
    end
  end
  inherited dxRibbon1: TdxRibbon
    inherited dxRibbon1Tab1: TdxRibbonTab
      Index = 0
    end
    inherited dxRibbon1Tab2: TdxRibbonTab
      Index = 1
    end
  end
  inherited StatusBar1: TdxRibbonStatusBar
    Top = 568
    ExplicitTop = 568
  end
  inherited dxBarManager1: TdxBarManager
    Top = 200
    DockControlHeights = (
      0
      0
      0
      0)
    inherited dxBarManager1Bar1: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 69
      FloatClientHeight = 167
    end
    inherited dxBarManager1Bar2: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 149
      FloatClientHeight = 130
    end
    inherited dxBarManager1Bar3: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 59
      FloatClientHeight = 108
    end
    inherited dxBarManager1Bar4: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 141
      FloatClientHeight = 221
    end
    inherited dxBarManager1BarTabela: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 174
      FloatClientHeight = 113
    end
  end
  inherited dxComponentPrinter1: TdxComponentPrinter
    inherited dxComponentPrinter1Link1: TdxGridReportLink
      ReportDocument.CreationDate = 41185.473791585650000000
      AssignedFormatValues = [fvDate, fvTime, fvPageNumber]
      Styles.BandHeader = nil
      Styles.Caption = nil
      Styles.CardCaptionRow = nil
      Styles.CardRowCaption = nil
      Styles.Content = nil
      Styles.ContentEven = nil
      Styles.ContentOdd = nil
      Styles.FilterBar = nil
      Styles.Footer = nil
      Styles.Group = nil
      Styles.Header = nil
      Styles.Preview = nil
      Styles.Selection = nil
      BuiltInReportLink = True
    end
  end
end
