object frmLagerKT: TfrmLagerKT
  Left = 128
  Top = 212
  BorderIcons = [biSystemMenu, biMaximize]
  ClientHeight = 692
  ClientWidth = 1100
  Color = clBtnFace
  Constraints.MinHeight = 500
  Constraints.MinWidth = 610
  Font.Charset = RUSSIAN_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  ShowHint = True
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object dxRibbon1: TdxRibbon
    Left = 0
    Top = 0
    Width = 1100
    Height = 126
    BarManager = dxBarManager1
    ColorSchemeName = 'Blue'
    Contexts = <>
    TabOrder = 0
    TabStop = False
    object dxRibbon1Tab1: TdxRibbonTab
      Active = True
      Caption = #1052#1077#1085#1080
      Groups = <
        item
          ToolbarName = 'dxBarManager1Bar1'
        end
        item
          ToolbarName = 'dxBarManager1Bar2'
        end
        item
          ToolbarName = 'dxBarManager1Bar3'
        end>
      Index = 0
    end
    object dxRibbon1Tab2: TdxRibbonTab
      Caption = #1055#1086#1076#1077#1089#1091#1074#1072#1114#1072
      Groups = <
        item
          ToolbarName = 'dxBarManager1Bar4'
        end
        item
          ToolbarName = 'dxBarManager1BarIzgledGrid'
        end>
      Index = 1
    end
  end
  object StatusBar1: TdxRibbonStatusBar
    Left = 0
    Top = 669
    Width = 1100
    Height = 23
    Panels = <
      item
        PanelStyleClassName = 'TdxStatusBarTextPanelStyle'
        Text = 'F9 - '#1055#1088#1080#1082#1072#1078#1080', F10 - '#1055#1077#1095#1072#1090#1080',  Esc - '#1048#1079#1083#1077#1079
      end>
    Ribbon = dxRibbon1
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clDefault
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
  end
  object Panel1: TPanel
    Left = 0
    Top = 126
    Width = 1100
    Height = 81
    Align = alTop
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 2
    object cxlbl1: TcxLabel
      Left = 44
      Top = 31
      AutoSize = False
      Caption = #1047#1040' '#1050#1054#1053#1058#1056#1054#1051#1053#1040' '#1058#1054#1063#1050#1040
      ParentFont = False
      Style.Font.Charset = RUSSIAN_CHARSET
      Style.Font.Color = clWindowText
      Style.Font.Height = -11
      Style.Font.Name = 'Tahoma'
      Style.Font.Style = [fsBold]
      Style.TextColor = clRed
      Style.IsFontAssigned = True
      Transparent = True
      Height = 25
      Width = 137
    end
    object cxButton1: TcxButton
      Left = 584
      Top = 30
      Width = 129
      Height = 25
      Action = aPrikazi
      TabOrder = 2
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object cbKT: TcxLookupComboBox
      Left = 187
      Top = 30
      ParentFont = False
      Properties.DropDownAutoSize = True
      Properties.DropDownSizeable = True
      Properties.ImmediateDropDownWhenActivated = True
      Properties.ImmediatePost = True
      Properties.KeyFieldNames = 'ID'
      Properties.ListColumns = <
        item
          FieldName = 'id'
        end
        item
          FieldName = 'NAZIV'
        end>
      Properties.ListFieldIndex = 1
      Properties.ListOptions.SyncMode = True
      Properties.ListSource = dm.dsKT
      Style.Font.Charset = RUSSIAN_CHARSET
      Style.Font.Color = clWindowText
      Style.Font.Height = -13
      Style.Font.Name = 'Tahoma'
      Style.Font.Style = []
      Style.IsFontAssigned = True
      TabOrder = 1
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      Width = 302
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 207
    Width = 1100
    Height = 462
    Align = alClient
    TabOrder = 3
    object cxGrid1: TcxGrid
      Left = 1
      Top = 1
      Width = 1098
      Height = 460
      Align = alClient
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -12
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      LookAndFeel.Kind = lfUltraFlat
      LookAndFeel.NativeStyle = False
      ExplicitLeft = 0
      ExplicitTop = -4
      object cxGrid1DBTableView1: TcxGridDBTableView
        OnKeyDown = cxGrid1DBTableView1KeyDown
        Navigator.Buttons.CustomButtons = <>
        Navigator.InfoPanel.Visible = True
        OnEditValueChanged = cxGrid1DBTableView1EditValueChanged
        OnFocusedItemChanged = cxGrid1DBTableView1FocusedItemChanged
        DataController.DataSource = dsLagerKT
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        FilterRow.InfoText = #1050#1083#1080#1082#1085#1077#1090#1077' '#1079#1072' '#1076#1072' '#1076#1077#1092#1080#1085#1080#1088#1072#1090#1077' '#1092#1080#1083#1090#1077#1088
        FilterRow.Visible = True
        Images = dmRes.cxSmallImages
        OptionsBehavior.IncSearch = True
        OptionsCustomize.ColumnsQuickCustomization = True
        OptionsData.CancelOnExit = False
        OptionsData.Deleting = False
        OptionsData.DeletingConfirmation = False
        OptionsData.Inserting = False
        OptionsView.Footer = True
        OptionsView.Indicator = True
        Preview.Visible = True
        object cxGrid1DBTableView1ID: TcxGridDBColumn
          DataBinding.FieldName = 'ID'
          Visible = False
        end
        object cxGrid1DBTableView1REF_NO: TcxGridDBColumn
          Caption = #1056#1077#1092'.'#1073#1088
          DataBinding.FieldName = 'REF_NO'
          Options.Editing = False
          Width = 233
        end
        object cxGrid1DBTableView1LOT: TcxGridDBColumn
          Caption = #1051#1086#1090
          DataBinding.FieldName = 'LOT'
          Options.Editing = False
          Width = 78
        end
        object cxGrid1DBTableView1VID_ARTIKAL: TcxGridDBColumn
          Caption = #1042#1080#1076
          DataBinding.FieldName = 'VID_ARTIKAL'
          Visible = False
          Options.Editing = False
          Width = 20
        end
        object cxGrid1DBTableView1ARTIKAL: TcxGridDBColumn
          Caption = #1064#1080#1092#1088#1072
          DataBinding.FieldName = 'ARTIKAL'
          Visible = False
          Options.Editing = False
          Width = 64
        end
        object cxGrid1DBTableView1NAZIV_ARTIKAL: TcxGridDBColumn
          Caption = #1040#1088#1090#1080#1082#1072#1083
          DataBinding.FieldName = 'NAZIV_ARTIKAL'
          Options.Editing = False
          Width = 415
        end
        object cxGrid1DBTableView1TAPACIR_SEDISTE: TcxGridDBColumn
          Caption = #1058#1072#1087#1072#1094#1080#1088' '#1089#1077#1076
          DataBinding.FieldName = 'TAPACIR_SEDISTE'
          Visible = False
          Options.Editing = False
          Width = 109
        end
        object cxGrid1DBTableView1TAPACIR_NAZAD: TcxGridDBColumn
          Caption = #1058#1072#1087#1072#1094#1080#1088' '#1085#1072#1079#1072#1076
          DataBinding.FieldName = 'TAPACIR_NAZAD'
          Visible = False
          Options.Editing = False
          Width = 150
        end
        object cxGrid1DBTableView1SUNGER: TcxGridDBColumn
          Caption = #1057#1091#1085#1107#1077#1088
          DataBinding.FieldName = 'SUNGER'
          Visible = False
          Options.Editing = False
          Width = 150
        end
        object cxGrid1DBTableView1BOJA_NOGARKI: TcxGridDBColumn
          Caption = #1041#1086#1112#1072' '#1085#1086#1075#1072#1088#1082#1080
          DataBinding.FieldName = 'BOJA_NOGARKI'
          Visible = False
          Options.Editing = False
          Width = 150
        end
        object cxGrid1DBTableView1LABELS: TcxGridDBColumn
          DataBinding.FieldName = 'LABELS'
          Visible = False
          Options.Editing = False
          Width = 60
        end
        object cxGrid1DBTableView1GLIDERS: TcxGridDBColumn
          DataBinding.FieldName = 'GLIDERS'
          Visible = False
          Options.Editing = False
          Width = 150
        end
        object cxGrid1DBTableView1OSTANATI: TcxGridDBColumn
          Caption = #1051#1040#1043#1045#1056
          DataBinding.FieldName = 'LAGER'
          Options.Editing = False
          Width = 115
        end
        object cxGrid1DBTableView1KOLICINA_PROIZVEDENA: TcxGridDBColumn
          Caption = #1042#1083#1077#1079
          DataBinding.FieldName = 'KOL_VLEZ'
          HeaderHint = #1045#1085#1090#1077#1088' '#1080#1083#1080' '#1044#1074#1086#1077#1085' '#1082#1083#1080#1082' '#1079#1072' '#1087#1088#1086#1084#1077#1085#1072
          HeaderImageIndex = 72
          Styles.Content = dmRes.Autumn
          Width = 96
        end
        object cxGrid1DBTableView1KOLICINA_POMINATA: TcxGridDBColumn
          Caption = #1048#1079#1083#1077#1079
          DataBinding.FieldName = 'KOL_IZLEZ'
          HeaderHint = #1045#1085#1090#1077#1088' '#1080#1083#1080' '#1044#1074#1086#1077#1085' '#1082#1083#1080#1082' '#1079#1072' '#1087#1088#1086#1084#1077#1085#1072
          HeaderImageIndex = 72
          Styles.Content = dmRes.Autumn
          Width = 79
        end
        object cxGrid1DBTableView1NAZIV2: TcxGridDBColumn
          DataBinding.FieldName = 'NAZIV2'
          Visible = False
          Options.Editing = False
        end
        object cxGrid1DBTableView1CUSTOMER_NAME: TcxGridDBColumn
          DataBinding.FieldName = 'CUSTOMER_NAME'
          Visible = False
          Options.Editing = False
        end
        object cxGrid1DBTableView1ZABELESKA: TcxGridDBColumn
          DataBinding.FieldName = 'ZABELESKA'
          Visible = False
          Options.Editing = False
        end
        object cxGrid1DBTableView1ID_RABOTEN_NALOG: TcxGridDBColumn
          DataBinding.FieldName = 'ID_RABOTEN_NALOG'
          Visible = False
          Options.Editing = False
        end
      end
      object cxGrid1Level1: TcxGridLevel
        GridView = cxGrid1DBTableView1
      end
    end
  end
  object cxGridPopupMenu1: TcxGridPopupMenu
    Grid = cxGrid1
    PopupMenus = <>
    Left = 51
    Top = 16
  end
  object PopupMenu1: TPopupMenu
    Left = 16
    Top = 51
    object N1: TMenuItem
      Caption = #1057#1085#1080#1084#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      ShortCut = 49235
      OnClick = SaveToIniFileExecute
    end
    object Excel1: TMenuItem
      Caption = #1057#1085#1080#1084#1080' '#1075#1086' '#1074#1086' Excel'
      ShortCut = 49221
    end
  end
  object dxBarManager1: TdxBarManager
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Segoe UI'
    Font.Style = []
    Categories.Strings = (
      'Default')
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    ImageOptions.Images = dmRes.cxSmallImages
    ImageOptions.LargeImages = dmRes.cxLargeImages
    PopupMenuLinks = <>
    UseSystemFont = True
    Left = 121
    Top = 16
    PixelsPerInch = 96
    object dxBarManager1Bar1: TdxBar
      Caption = 'Custom 1'
      CaptionButtons = <>
      DockedDockingStyle = dsTop
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 715
      FloatTop = 157
      FloatClientWidth = 51
      FloatClientHeight = 120
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton1'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton2'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarLargeButton3'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = False
      WholeRow = False
    end
    object dxBarManager1Bar2: TdxBar
      Caption = #1040#1082#1094#1080#1080
      CaptionButtons = <>
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 789
      FloatTop = 120
      FloatClientWidth = 51
      FloatClientHeight = 120
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton10'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton5'
        end
        item
          Visible = True
          ItemName = 'dxbrlrgbtn1'
        end
        item
          Visible = True
          ItemName = 'dxBarSubItem1'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarLargeButton18'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar3: TdxBar
      Caption = #1048#1079#1083#1077#1079
      CaptionButtons = <>
      DockedLeft = 377
      DockedTop = 0
      FloatLeft = 718
      FloatTop = 62
      FloatClientWidth = 51
      FloatClientHeight = 80
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton7'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton8'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar4: TdxBar
      Caption = #1055#1077#1095#1072#1090#1077#1114#1077
      CaptionButtons = <>
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 676
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton12'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton15'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton9'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarLargeButton13'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1BarIzgledGrid: TdxBar
      Caption = #1058#1072#1073#1077#1083#1072
      CaptionButtons = <>
      DockedLeft = 363
      DockedTop = 0
      FloatLeft = 808
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLBtnSnimiIzgled'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarLBtnBrisiIzgled'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarLargeButton2: TdxBarLargeButton
      Action = aAzuriraj
      Category = 0
    end
    object dxBarLargeButton3: TdxBarLargeButton
      Action = aBrisi
      Category = 0
    end
    object dxBarLargeButton4: TdxBarLargeButton
      Action = aSnimiIzgled
      Category = 0
      ScreenTip = tipSnimiIzgled
    end
    object dxBarLargeButton5: TdxBarLargeButton
      Action = aZacuvajExcel
      Category = 0
      ScreenTip = tipZacuvajExcel
    end
    object dxBarLargeButton6: TdxBarLargeButton
      Action = aPecatiTabela
      Caption = #1055#1077#1095#1072#1090#1080' '#1090#1072#1073#1077#1083#1072
      Category = 0
    end
    object dxBarLargeButton7: TdxBarLargeButton
      Action = aHelp
      Category = 0
    end
    object dxBarLargeButton8: TdxBarLargeButton
      Action = aIzlez
      Category = 0
    end
    object dxBarLargeButton1: TdxBarLargeButton
      Action = aNov
      Category = 0
    end
    object dxBarLargeButton9: TdxBarLargeButton
      Action = aSnimiPecatenje
      Category = 0
      ScreenTip = tipSnimiKonf
    end
    object dxBarLargeButton11: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object ribbonFilterReset: TdxBarLargeButton
      Caption = #1056#1077#1089#1077#1090#1080#1088#1072#1112' '#1092#1080#1083#1090#1077#1088
      Category = 0
      Hint = #1056#1077#1089#1077#1090#1080#1088#1072#1112' '#1092#1080#1083#1090#1077#1088
      Visible = ivAlways
    end
    object dxBarLargeButton12: TdxBarLargeButton
      Action = aPodesuvanjePecatenje
      Category = 0
      ScreenTip = tipDizajnReport
    end
    object dxBarLargeButton13: TdxBarLargeButton
      Action = aBrisiPodesuvanjePecatenje
      Category = 0
      ScreenTip = tipBrisiKonf
    end
    object dxBarLargeButton14: TdxBarLargeButton
      Caption = #1048#1079#1073#1088#1080#1096#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      Category = 0
      Visible = ivAlways
      LargeImageIndex = 40
    end
    object dxBarLargeButton15: TdxBarLargeButton
      Action = aPageSetup
      Category = 0
      ScreenTip = tipSetiranjeStrana
    end
    object dxBarLargeButton16: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object dxBarLargeButton17: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object cxBarEditItem1: TcxBarEditItem
      Caption = 'New Item'
      Category = 0
      Hint = 'New Item'
      Visible = ivAlways
      PropertiesClassName = 'TcxCheckBoxProperties'
    end
    object dxBarSubItem1: TdxBarSubItem
      Caption = #1055#1077#1095#1072#1090#1077#1114#1077
      Category = 0
      Visible = ivAlways
      ImageIndex = 30
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton6'
        end>
    end
    object dxBarLBtnSnimiIzgled: TdxBarLargeButton
      Action = aSnimiIzgled
      Category = 0
    end
    object dxBarLBtnBrisiIzgled: TdxBarLargeButton
      Action = aBrisiIzgled
      Category = 0
    end
    object dxBarLargeButton10: TdxBarLargeButton
      Action = aSpustiSoberi
      Category = 0
    end
    object dxbrlrgbtn1: TdxBarLargeButton
      Action = aPecati
      Category = 0
    end
    object dxBarLargeButton18: TdxBarLargeButton
      Action = aPromeni
      Category = 0
      Visible = ivNever
    end
  end
  object ActionList1: TActionList
    Images = dmRes.cxSmallImages
    Left = 16
    Top = 16
    object aNov: TAction
      Caption = #1053#1086#1074
      ImageIndex = 10
      ShortCut = 116
      OnExecute = aNovExecute
    end
    object aAzuriraj: TAction
      Caption = #1040#1078#1091#1088#1080#1088#1072#1112
      ImageIndex = 12
      ShortCut = 117
      OnExecute = aAzurirajExecute
    end
    object aBrisi: TAction
      Caption = #1041#1088#1080#1096#1080
      ImageIndex = 11
      ShortCut = 119
      OnExecute = aBrisiExecute
    end
    object aSnimiIzgled: TAction
      Caption = #1057#1085#1080#1084#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      ImageIndex = 9
      ShortCut = 24659
      OnExecute = aSnimiIzgledExecute
    end
    object aZacuvajExcel: TAction
      Caption = #1047#1072#1095#1091#1074#1072#1112' '#1074#1086' Excel'
      ImageIndex = 9
      ShortCut = 24645
      OnExecute = aZacuvajExcelExecute
    end
    object aPecatiTabela: TAction
      Caption = #1055#1077#1095#1072#1090#1080
      ImageIndex = 30
      OnExecute = aPecatiTabelaExecute
    end
    object aHelp: TAction
      Caption = #1055#1086#1084#1086#1096
      ImageIndex = 24
    end
    object aIzlez: TAction
      Caption = #1048#1079#1083#1077#1079
      ImageIndex = 25
      OnExecute = aIzlezExecute
    end
    object aZapisi: TAction
      Caption = #1047#1072#1087#1080#1096#1080
      ImageIndex = 7
      OnExecute = aZapisiExecute
    end
    object aOtkazi: TAction
      Caption = #1054#1090#1082#1072#1078#1080
      ImageIndex = 21
      ShortCut = 27
      OnExecute = aOtkaziExecute
    end
    object aRefresh: TAction
      Caption = 'aRefresh'
      ImageIndex = 18
      ShortCut = 118
      OnExecute = aRefreshExecute
    end
    object aSnimiPecatenje: TAction
      Caption = #1057#1085#1080#1084#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      ImageIndex = 39
      OnExecute = aSnimiPecatenjeExecute
    end
    object aPodesuvanjePecatenje: TAction
      Caption = #1044#1080#1079#1072#1112#1085' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090
      ImageIndex = 38
      OnExecute = aPodesuvanjePecatenjeExecute
    end
    object aBrisiPodesuvanjePecatenje: TAction
      Caption = #1041#1088#1080#1096#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      ImageIndex = 37
      OnExecute = aBrisiPodesuvanjePecatenjeExecute
    end
    object aPageSetup: TAction
      Caption = #1057#1077#1090#1080#1088#1072#1114#1077' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072
      ImageIndex = 38
      OnExecute = aPageSetupExecute
    end
    object aFormConfig: TAction
      Caption = 'aFormConfig'
      ShortCut = 24699
      OnExecute = aFormConfigExecute
    end
    object aBrisiIzgled: TAction
      Caption = #1041#1088#1080#1096#1080' '#1075#1086' '#1079#1072#1095#1091#1074#1072#1085#1080#1086#1090' '#1080#1079#1075#1083#1077#1076
      ImageIndex = 56
      OnExecute = aBrisiIzgledExecute
    end
    object aSpustiSoberi: TAction
      Caption = #1057#1087#1091#1096#1090#1080'/'#1057#1086#1073#1077#1088#1080' '#1058#1072#1073#1077#1083#1072
      ImageIndex = 65
      OnExecute = aSpustiSoberiExecute
    end
    object aPrikazi: TAction
      Caption = #1055#1088#1080#1082#1072#1078#1080
      ImageIndex = 19
      ShortCut = 120
      OnExecute = aPrikaziExecute
    end
    object aDizajner: TAction
      Caption = 'aDizajner'
      ShortCut = 24697
      OnExecute = aDizajnerExecute
    end
    object aPecati: TAction
      Caption = #1055#1077#1095#1072#1090#1080
      ImageIndex = 30
      ShortCut = 121
      OnExecute = aPecatiExecute
    end
    object aPromeni: TAction
      Caption = #1055#1088'o'#1084#1077#1085#1080' '#1051#1072#1075#1077#1088
      ImageIndex = 18
      OnExecute = aPromeniExecute
    end
  end
  object dxComponentPrinter1: TdxComponentPrinter
    CurrentLink = dxComponentPrinter1Link1
    Version = 0
    Left = 191
    Top = 16
    object dxComponentPrinter1Link1: TdxGridReportLink
      DateFormat = 0
      PageNumberFormat = pnfNumeral
      PrinterPage.DMPaper = 9
      PrinterPage.Footer = 6350
      PrinterPage.GrayShading = True
      PrinterPage.Header = 6350
      PrinterPage.Margins.Bottom = 12700
      PrinterPage.Margins.Left = 12700
      PrinterPage.Margins.Right = 12700
      PrinterPage.Margins.Top = 12700
      PrinterPage.Orientation = poLandscape
      PrinterPage.PageFooter.RightTitle.Strings = (
        #1057#1090#1088'.[Page #] '#1086#1076' [Total Pages]')
      PrinterPage.PageSize.X = 210000
      PrinterPage.PageSize.Y = 297000
      PrinterPage._dxMeasurementUnits_ = 0
      PrinterPage._dxLastMU_ = 2
      ReportDocument.CreationDate = 40336.425359629630000000
      TimeFormat = 0
      OptionsFormatting.UseNativeStyles = True
      OptionsOnEveryPage.Caption = False
      OptionsOnEveryPage.FilterBar = False
      OptionsView.Caption = False
      OptionsView.FilterBar = False
      StyleRepository = dmRes.cxStyleRepositoryPrinting
      Styles.StyleSheet = dmRes.dxGridReportLinkStyleSheet
      BuiltInReportLink = True
    end
  end
  object dxBarScreenTipRepository1: TdxBarScreenTipRepository
    Left = 156
    Top = 16
    PixelsPerInch = 96
    object tipSnimiIzgled: TdxBarScreenTip
      Header.Text = #1057#1085#1080#1084#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      Description.Text = #1047#1072#1095#1091#1074#1072#1112' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090' '#1085#1072' '#1092#1086#1088#1084#1072#1090#1072' '#1080' '#1085#1072' '#1090#1072#1073#1077#1083#1072#1090#1072
    end
    object tipZacuvajExcel: TdxBarScreenTip
      Header.Text = #1047#1072#1095#1091#1074#1072#1112' '#1074#1086' Excel'
      Description.Text = #1057#1085#1080#1084#1080' '#1075#1080' '#1087#1086#1076#1072#1090#1086#1094#1080#1090#1077' '#1086#1076' '#1090#1072#1073#1077#1083#1072#1090#1072' '#1074#1086' Excel '#1092#1086#1088#1084#1072#1090
    end
    object tipDizajnReport: TdxBarScreenTip
      Header.Text = #1044#1080#1079#1072#1112#1085' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090
      Description.Text = #1055#1086#1076#1077#1089#1091#1074#1072#1114#1072' '#1085#1072' '#1080#1079#1075#1083#1077#1076#1086#1090' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090' '#1085#1072' '#1090#1072#1073#1077#1083#1072#1090#1072
    end
    object tipSetiranjeStrana: TdxBarScreenTip
      Header.Text = #1057#1077#1090#1080#1088#1072#1114#1077' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072
      Description.Text = #1055#1086#1076#1077#1089#1091#1074#1072#1114#1077' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072' '#1085#1072' '#1082#1086#1112#1072' '#1116#1077' '#1089#1077' '#1087#1077#1095#1072#1090#1080' '#1090#1072#1073#1077#1083#1072#1090#1072
    end
    object tipSnimiKonf: TdxBarScreenTip
      Header.Text = #1057#1085#1080#1084#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      Description.Text = #1047#1072#1095#1091#1074#1072#1112' '#1075#1080' '#1087#1086#1076#1077#1089#1091#1074#1072#1114#1072#1090#1072' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090' '#1080' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072' '#1079#1072' '#1087#1077#1095#1072#1090#1077#1114#1077
    end
    object tipBrisiKonf: TdxBarScreenTip
      Header.Text = #1041#1088#1080#1096#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      Description.Text = #1048#1079#1073#1088#1080#1096#1080' '#1075#1080' '#1087#1086#1076#1077#1089#1091#1074#1072#1114#1072#1090#1072' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090' '#1080' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072' '#1079#1072' '#1087#1077#1095#1072#1090#1077#1114#1077
    end
  end
  object tblLagerKT: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE MAN_KT_LAGER'
      'SET '
      '    KOL_VLEZ = :KOL_VLEZ,'
      '    KOL_IZLEZ = :KOL_IZLEZ'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    RefreshSQL.Strings = (
      'select'
      '       kl.id,'
      '       A.NAZIV NAZIV_ARTIKAL,'
      '       rns.id_raboten_nalog,'
      '       A.NAZIV2,'
      '       ps.customer_name,'
      '       RNS.VID_ARTIKAL,'
      '       RNS.ARTIKAL,'
      '       P.REF_NO,'
      '       PS.TAPACIR_SEDISTE,'
      '       PS.TAPACIR_NAZAD,'
      '       PS.SUNGER,'
      '       PS.BOJA_NOGARKI,'
      '       PS.LABELS,'
      '       PS.GLIDERS,'
      '       PS.ZABELESKA,'
      '       kl.kol_vlez,'
      '       kl.kol_izlez,'
      '       kl.lager'
      'from MAN_RABOTEN_NALOG_STAVKA RNS'
      
        'inner join MAN_PORACKA_STAVKA PS on PS.ID = RNS.ID_PORACKA_STAVK' +
        'A and PS.STATUS <> 3 and RNS.STATUS <> 3'
      
        'inner join MAN_PORACKA P on P.ID = PS.ID_PORACKA and PS.STATUS <' +
        '> 3'
      
        'inner join MTR_ARTIKAL A on A.ARTVID = RNS.VID_ARTIKAL and A.ID ' +
        '= RNS.ARTIKAL'
      
        'inner join man_kt_lager kl on kl.lot = rns.lot and kl.id_kt = :i' +
        'd_kt'
      'where(  kl.lager <> 0'
      '     ) and (     KL.ID = :OLD_ID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      'select'
      '       kl.id,'
      '       A.NAZIV NAZIV_ARTIKAL,'
      '       rns.id_raboten_nalog,'
      '       A.NAZIV2,'
      '       ps.customer_name,'
      '       RNS.VID_ARTIKAL,'
      '       RNS.ARTIKAL,'
      '       P.REF_NO,'
      '       PS.TAPACIR_SEDISTE,'
      '       PS.TAPACIR_NAZAD,'
      '       PS.SUNGER,'
      '       PS.BOJA_NOGARKI,'
      '       PS.LABELS,'
      '       PS.GLIDERS,'
      '       PS.ZABELESKA,'
      '       kl.lot,'
      '       kl.kol_vlez,'
      '       kl.kol_izlez,'
      '       kl.lager'
      'from MAN_RABOTEN_NALOG_STAVKA RNS'
      
        'inner join MAN_PORACKA_STAVKA PS on PS.ID = RNS.ID_PORACKA_STAVK' +
        'A and PS.STATUS <> 3 and RNS.STATUS <> 3'
      
        'inner join MAN_PORACKA P on P.ID = PS.ID_PORACKA and PS.STATUS <' +
        '> 3'
      
        'inner join MTR_ARTIKAL A on A.ARTVID = RNS.VID_ARTIKAL and A.ID ' +
        '= RNS.ARTIKAL'
      
        'inner join man_kt_lager kl on kl.lot = rns.lot and kl.id_kt = :i' +
        'd_kt'
      'where kl.lager <> 0')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 226
    Top = 16
    object tblLagerKTNAZIV_ARTIKAL: TFIBStringField
      FieldName = 'NAZIV_ARTIKAL'
      Size = 500
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblLagerKTNAZIV2: TFIBStringField
      FieldName = 'NAZIV2'
      Size = 500
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblLagerKTCUSTOMER_NAME: TFIBStringField
      FieldName = 'CUSTOMER_NAME'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblLagerKTREF_NO: TFIBStringField
      FieldName = 'REF_NO'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblLagerKTTAPACIR_SEDISTE: TFIBStringField
      FieldName = 'TAPACIR_SEDISTE'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblLagerKTTAPACIR_NAZAD: TFIBStringField
      FieldName = 'TAPACIR_NAZAD'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblLagerKTSUNGER: TFIBStringField
      FieldName = 'SUNGER'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblLagerKTBOJA_NOGARKI: TFIBStringField
      FieldName = 'BOJA_NOGARKI'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblLagerKTLABELS: TFIBStringField
      FieldName = 'LABELS'
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblLagerKTGLIDERS: TFIBStringField
      FieldName = 'GLIDERS'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblLagerKTZABELESKA: TFIBStringField
      FieldName = 'ZABELESKA'
      Size = 500
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblLagerKTID_RABOTEN_NALOG: TFIBBCDField
      FieldName = 'ID_RABOTEN_NALOG'
      Size = 0
    end
    object tblLagerKTVID_ARTIKAL: TFIBIntegerField
      FieldName = 'VID_ARTIKAL'
    end
    object tblLagerKTARTIKAL: TFIBIntegerField
      FieldName = 'ARTIKAL'
    end
    object tblLagerKTKOL_VLEZ: TFIBIntegerField
      FieldName = 'KOL_VLEZ'
    end
    object tblLagerKTKOL_IZLEZ: TFIBIntegerField
      FieldName = 'KOL_IZLEZ'
    end
    object tblLagerKTLAGER: TFIBIntegerField
      FieldName = 'LAGER'
    end
    object tblLagerKTID: TFIBIntegerField
      FieldName = 'ID'
    end
    object tblLagerKTLOT: TFIBStringField
      FieldName = 'LOT'
      Size = 30
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object dsLagerKT: TDataSource
    DataSet = tblLagerKT
    Left = 86
    Top = 16
  end
end
