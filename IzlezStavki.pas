unit IzlezStavki;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ExtCtrls, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, dxSkinsCore, dxSkinBlack, dxSkinBlue,
  dxSkinBlueprint, dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide,
  dxSkinDevExpressDarkStyle, dxSkinDevExpressStyle, dxSkinFoggy,
  dxSkinGlassOceans, dxSkinHighContrast, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven,
  dxSkinSevenClassic, dxSkinSharp, dxSkinSharpPlus, dxSkinSilver,
  dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008, dxSkinTheAsphaltWorld,
  dxSkinsDefaultPainters, dxSkinValentine, dxSkinVS2010, dxSkinWhiteprint,
  dxSkinXmas2008Blue, dxSkinsdxStatusBarPainter, dxStatusBar, cxStyles,
  dxSkinscxPCPainter, cxCustomData, cxFilter, cxData, cxDataStorage, cxEdit,
  Data.DB, cxDBData, cxGridLevel, cxClasses, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid, Vcl.Menus,
  Vcl.ActnList, Vcl.StdCtrls, cxButtons, FIBDataSet, pFIBDataSet, FIBQuery,
  pFIBQuery, pFIBStoredProc, cxContainer, Vcl.Imaging.jpeg, cxImage,
  dxSkinOffice2013White, cxNavigator, System.Actions, dxSkinMetropolis,
  dxSkinMetropolisDark, dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray;

type
  TfrmIzlezStavki = class(TForm)
    Panel1: TPanel;
    dxStatusBar1: TdxStatusBar;
    Panel2: TPanel;
    cxGrid1: TcxGrid;
    cxGrid1Level1: TcxGridLevel;
    ZapisiButtonKol: TcxButton;
    OtkaziButtonKol: TcxButton;
    ActionList1: TActionList;
    aVnesi: TAction;
    aOtkazi: TAction;
    cxGrid1TableView1: TcxGridTableView;
    LOT_NO: TcxGridColumn;
    spInputOutScan: TpFIBStoredProc;
    cxImage1: TcxImage;
    procedure aOtkaziExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure cxGrid1TableView1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure aVnesiExecute(Sender: TObject);
    procedure Parsiraj(LOT_NO : string);
    procedure InsertUpdateArtikal(art_vid : integer; art_sif : integer; kolicina : double);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    { Private declarations }
  public
    { Public declarations }
    tip_dok : integer;
  end;

var
  frmIzlezStavki: TfrmIzlezStavki;
  re_kt,kon_toc,operacija, od_kadeKT, sledna_kt : integer;
implementation

{$R *.dfm}

uses Utils, dmUnit, dmKonekcija, dmResources, DaNe, StringUtils;

procedure TfrmIzlezStavki.aOtkaziExecute(Sender: TObject);
begin
  if cxGrid1TableView1.DataController.RecordCount > 0 then
  begin
    frmDaNe := TfrmDaNe.Create(self, '���������� ������', '�������� �� �� ��������. ������ �� ������?', 1);
    if (frmDaNe.ShowModal = mrYes) then
    begin
      ModalResult := mrCancel;
      Close();
    end
    else
    begin

    end;
  end
  else
  begin
    ModalResult := mrCancel;
    Close();
  end;

end;

procedure TfrmIzlezStavki.aVnesiExecute(Sender: TObject);
var
  kraj : boolean;
begin
  if cxGrid1TableView1.DataController.RecordCount > 0 then
  begin
    cxGrid1TableView1.DataController.GoToFirst;
    kraj := false;
    while (kraj = false) do
    begin
      Parsiraj(LOT_NO.EditValue);
      kraj := cxGrid1TableView1.DataController.IsEOF;
      cxGrid1TableView1.DataController.GoToNext;
    end;
  end;

  Close;
end;

procedure TfrmIzlezStavki.cxGrid1TableView1KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  if (Key = VK_RETURN) then ShowMessage('����');
end;

procedure TfrmIzlezStavki.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  ActivateKeyboardLayout($042F042F, KLF_REORDER);
end;

procedure TfrmIzlezStavki.FormShow(Sender: TObject);
begin
  ActivateKeyboardLayout($04090409, KLF_REORDER);

  cxGrid1.SetFocus;
  cxGrid1TableView1.DataController.Insert;
end;

procedure TfrmIzlezStavki.Parsiraj(LOT_NO : string);
var
  scan_barcode, scan_kolicina, scan_lot : string;
begin
  scan_barcode := ExtractText(LOT_NO,'(01)','(10)');
  scan_lot := ExtractText(LOT_NO,'(10)','(37)');
  scan_kolicina := ExtractText(LOT_NO,'(37)','');

//  spInputOutScan.Close;
//
//  if (tip_dok = 12) then
//    spInputOutScan.ParamByName('IN_OUT_ID').Value := dm.tblPovratnicaDobavuvacID.Value
//  else
//  if (tip_dok = 13) then
//    spInputOutScan.ParamByName('IN_OUT_ID').Value := dm.tblKusokID.Value
//  else
//    spInputOutScan.ParamByName('IN_OUT_ID').Value := dm.tblOutID.Value;
//
//  spInputOutScan.ParamByName('IN_ART_BARCODE').Value := scan_barcode;
//  spInputOutScan.ParamByName('IN_LOT').Value := scan_lot;
//  spInputOutScan.ParamByName('IN_KOLICINA').Value := scan_kolicina;
//  spInputOutScan.ExecProc;
end;

procedure TfrmIzlezStavki.InsertUpdateArtikal(art_vid : integer; art_sif : integer; kolicina : double);
begin
//
end;



end.
