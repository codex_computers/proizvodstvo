unit LagerArtikli;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData, cxEdit,
  DB, cxDBData, cxGridCustomPopupMenu, cxGridPopupMenu,
  ComCtrls, cxContainer, cxTextEdit, cxDBEdit, StdCtrls, cxGridLevel,
  cxClasses, cxControls, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGrid, ExtCtrls, Menus, ActnList,
  cxGridExportLink, cxExport, cxLookAndFeelPainters, cxDataStorage,
  cxButtons, dxStatusBar, dxRibbonStatusBar, dxRibbon, dxBar, dxBarExtItems,
  dxPSGlbl, dxPSUtl, dxPSEngn, dxPrnPg, dxBkgnd, dxWrap, dxPrnDev,
  dxPSCompsProvider, dxPSFillPatterns, dxPSEdgePatterns, dxPSCore, dxPScxCommon,
  dxPScxGridLnk, cxPC, cxMemo, cxLocalization, cxLookAndFeels,
  dxPSPDFExportCore, dxPSPDFExport, cxDrawTextUtils, dxPSPrVwStd, dxPSPrVwAdv,
  dxPSPrVwRibbon, dxPScxEditorProducers, dxPScxExtEditorProducers,
  dxPScxPageControlProducer, dxPgsDlg, cxCheckBox, cxBarEditItem, dxSkinsCore,
  dxSkinBlack, dxSkinBlue, dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom,
  dxSkinDarkSide, dxSkinFoggy, dxSkinGlassOceans, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven,
  dxSkinSharp, dxSkinSilver, dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008,
  dxSkinsDefaultPainters, dxSkinValentine, dxSkinXmas2008Blue,
  dxSkinsdxRibbonPainter, dxSkinsdxBarPainter, dxSkinscxPCPainter,
  dxBarSkinnedCustForm, cxDropDownEdit, cxCalendar, cxDBLookupComboBox,
  cxMaskEdit, cxLookupEdit, cxDBLookupEdit, cxDBExtLookupComboBox, cxGroupBox,
  cxRadioGroup, cxSplitter, cxGridChartView, cxGridDBChartView, dxRibbonSkins,
  cxPCdxBarPopupMenu, dxPScxGridLayoutViewLnk, dxScreenTip,
  cxPivotGridChartConnection, dxSkinBlueprint, dxSkinDevExpressDarkStyle,
  dxSkinDevExpressStyle, dxSkinHighContrast, dxSkinSevenClassic,
  dxSkinSharpPlus, dxSkinTheAsphaltWorld, dxSkinVS2010, dxSkinWhiteprint,
  FIBDataSet, pFIBDataSet, dxSkinOffice2013White, dxCore, cxDateUtils,
  cxNavigator, System.Actions;

type
//  niza = Array[1..5] of Variant;

  TfrmLagerArtikli = class(TForm)
    cxGridPopupMenu1: TcxGridPopupMenu;
    PopupMenu1: TPopupMenu;
    N1: TMenuItem;
    Excel1: TMenuItem;
    dxRibbon1Tab1: TdxRibbonTab;
    dxRibbon1: TdxRibbon;
    StatusBar1: TdxRibbonStatusBar;
    dxBarManager1: TdxBarManager;
    dxBarManager1Bar2: TdxBar;
    dxBarManager1Bar3: TdxBar;
    dxBarLargeButton2: TdxBarLargeButton;
    dxBarLargeButton3: TdxBarLargeButton;
    dxBarLargeButton4: TdxBarLargeButton;
    dxBarLargeButton5: TdxBarLargeButton;
    dxBarLargeButton6: TdxBarLargeButton;
    dxBarLargeButton7: TdxBarLargeButton;
    dxBarLargeButton8: TdxBarLargeButton;
    dxBarLargeButton1: TdxBarLargeButton;
    ActionList1: TActionList;
    aNov: TAction;
    aAzuriraj: TAction;
    aBrisi: TAction;
    aSnimiIzgled: TAction;
    aZacuvajExcel: TAction;
    aPecatiTabela: TAction;
    aHelp: TAction;
    aIzlez: TAction;
    aZapisi: TAction;
    aOtkazi: TAction;
    aRefresh: TAction;
    dxRibbon1Tab2: TdxRibbonTab;
    dxBarManager1Bar4: TdxBar;
    dxBarLargeButton9: TdxBarLargeButton;
    aSnimiPecatenje: TAction;
    dxComponentPrinter1: TdxComponentPrinter;
    dxComponentPrinter1Link1: TdxGridReportLink;
    dxBarLargeButton11: TdxBarLargeButton;
    ribbonFilterReset: TdxBarLargeButton;
    aPodesuvanjePecatenje: TAction;
    dxBarLargeButton12: TdxBarLargeButton;
    aBrisiPodesuvanjePecatenje: TAction;
    dxBarLargeButton13: TdxBarLargeButton;
    dxBarLargeButton14: TdxBarLargeButton;
    aPageSetup: TAction;
    dxBarLargeButton15: TdxBarLargeButton;
    dxBarLargeButton16: TdxBarLargeButton;
    dxBarLargeButton17: TdxBarLargeButton;
    cxBarEditItem1: TcxBarEditItem;
    dxBarSubItem1: TdxBarSubItem;
    aFormConfig: TAction;
    dxBarScreenTipRepository1: TdxBarScreenTipRepository;
    tipSnimiIzgled: TdxBarScreenTip;
    tipZacuvajExcel: TdxBarScreenTip;
    tipDizajnReport: TdxBarScreenTip;
    tipSetiranjeStrana: TdxBarScreenTip;
    tipSnimiKonf: TdxBarScreenTip;
    tipBrisiKonf: TdxBarScreenTip;
    dPanel: TPanel;
    lPanel: TPanel;
    lblPartner: TLabel;
    Label3: TLabel;
    DoDatum: TcxDateEdit;
    ZapisiButton: TcxButton;
    aPrikazi: TAction;
    cxPageControl1: TcxPageControl;
    cxTabTabelaren: TcxTabSheet;
    cxTabGrafik: TcxTabSheet;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1LevelFIFO: TcxGridLevel;
    cxSplitter1: TcxSplitter;
    cxGrid2: TcxGrid;
    cxGrid2Level1: TcxGridLevel;
    cxGrid2DBChartView1: TcxGridDBChartView;
    dxBarLargeButton10: TdxBarLargeButton;
    dxBarLargeButton18: TdxBarLargeButton;
    aDizajn: TAction;
    dxBarLargeButton19: TdxBarLargeButton;
    aPecatiKartica: TAction;
    dxBarSubItem2: TdxBarSubItem;
    cxPivotGridChartConnection1: TcxPivotGridChartConnection;
    dxBarLargeButton20: TdxBarLargeButton;
    aPecatiGrafik: TAction;
    dxComponentPrinter1Link2: TdxGridReportLink;
    dxBarLargeButton21: TdxBarLargeButton;
    dxBarLargeButton22: TdxBarLargeButton;
    dxBarLargeButton23: TdxBarLargeButton;
    dxBarLargeButton24: TdxBarLargeButton;
    aSpustiSoberi: TAction;
    dxBarLargeButton25: TdxBarLargeButton;
    dxBarLargeButton26: TdxBarLargeButton;
    aDizajnSint: TAction;
    dxBarManager1Bar5: TdxBar;
    dxBarLargeButton27: TdxBarLargeButton;
    dxBarLargeButton28: TdxBarLargeButton;
    aBrisiIzgled: TAction;
    lcbMagacin: TcxLookupComboBox;
    cxGrid1DBTableView1O_ART_VID: TcxGridDBColumn;
    cxGrid1DBTableView1O_ART_SIF: TcxGridDBColumn;
    cxGrid1DBTableView1ARTIKAL_NAZIV: TcxGridDBColumn;
    dxBarLargeButton29: TdxBarLargeButton;
    aMagacin: TAction;
    cxGrid1LevelSerija: TcxGridLevel;
    cxGrid1DBTableView2: TcxGridDBTableView;
    cxGrid1DBTableView2O_ART_VID: TcxGridDBColumn;
    cxGrid1DBTableView2O_ART_SIF: TcxGridDBColumn;
    cxGrid1DBTableView2ARTIKAL_NAZIV: TcxGridDBColumn;
    cxGrid1DBTableView2O_VLEZ: TcxGridDBColumn;
    cxGrid1DBTableView2O_IZLEZ: TcxGridDBColumn;
    cxGrid1DBTableView2O_LAGER: TcxGridDBColumn;
    cxGrid1DBTableView2O_REF_ID: TcxGridDBColumn;
    tblReMagacin: TpFIBDataSet;
    tblReMagacinID: TFIBIntegerField;
    tblReMagacinGODINA: TFIBIntegerField;
    tblReMagacinADRESA: TFIBStringField;
    tblReMagacinMESTO: TFIBIntegerField;
    tblReMagacinMESTO_NAZIV: TFIBStringField;
    tblReMagacinODGOVOREN: TFIBStringField;
    tblReMagacinKONTO: TFIBStringField;
    tblReMagacinKONTO_T1: TFIBStringField;
    tblReMagacinKONTO_T2: TFIBStringField;
    tblReMagacinKONTO_P: TFIBStringField;
    tblReMagacinOSLOBODUVANJE: TFIBSmallIntField;
    tblReMagacinZADOLZUVANJE: TFIBSmallIntField;
    tblReMagacinNAZIV_KONTO: TFIBStringField;
    tblReMagacinNAZIV_KONTO_T1: TFIBStringField;
    tblReMagacinNAZIV_KONTO_T2: TFIBStringField;
    tblReMagacinNAZIV_KONTO_P: TFIBStringField;
    tblReMagacinNAZIV: TFIBStringField;
    tblReMagacinTIP_PARTNER: TFIBIntegerField;
    tblReMagacinPARTNER: TFIBIntegerField;
    tblReMagacinNAZIV_PARTNER: TFIBStringField;
    tblReMagacinKOREN: TFIBIntegerField;
    dsReMagacin: TDataSource;
    cxGrid1DBTableView2O_VLEZ_NEKONTROLIRAN: TcxGridDBColumn;
    aHideFields: TAction;
    cxGrid1DBTableView2O_LOT_NO: TcxGridDBColumn;
    cxGrid1DBTableView1O_VLEZ: TcxGridDBColumn;
    cxGrid1DBTableView1O_VLEZ_NEKONTROLIRAN: TcxGridDBColumn;
    cxGrid1DBTableView1O_IZLEZ: TcxGridDBColumn;
    cxGrid1DBTableView1O_LAGER: TcxGridDBColumn;

    procedure cxDBTextEditAllEnter(Sender: TObject);
    procedure cxDBTextEditAllExit(Sender: TObject);
    procedure EnterKakoTab(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    //procedure PrvPosledenTab(panel:TPanel;var posledna,prva:TWinControl);
    procedure SaveToIniFileExecute(Sender: TObject);
    procedure cxGrid1DBTableView1KeyPress(Sender: TObject; var Key: Char);
    procedure aZapisiExecute(Sender: TObject);
    procedure aOtkaziExecute(Sender: TObject);
    procedure aNovExecute(Sender: TObject);
    procedure aAzurirajExecute(Sender: TObject);
    procedure aBrisiExecute(Sender: TObject);
    procedure aZacuvajExcelExecute(Sender: TObject);
    procedure aRefreshExecute(Sender: TObject);
    procedure aSnimiIzgledExecute(Sender: TObject);
    procedure aIzlezExecute(Sender: TObject);
    procedure aPecatiTabelaExecute(Sender: TObject);
    procedure aSnimiPecatenjeExecute(Sender: TObject);
    procedure zacuvajPrintVoIni(ime:AnsiString);
    procedure procitajPrintOdIni(ime:AnsiString);
    procedure aPodesuvanjePecatenjeExecute(Sender: TObject);
    procedure aBrisiPodesuvanjePecatenjeExecute(Sender: TObject);
    procedure aPageSetupExecute(Sender: TObject);
    procedure cxGrid1DBTableView1FilterCustomization(
      Sender: TcxCustomGridTableView; var ADone: Boolean);
    procedure aFormConfigExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure PartnerPropertiesChange(Sender: TObject);
    procedure aPrikaziExecute(Sender: TObject);
    procedure TipSifraExit(Sender: TObject);
    procedure cxGrid2DBChartView1GetValueHint(Sender: TcxGridChartView;
      ASeries: TcxGridChartSeries; AValueIndex: Integer; var AHint: string);
    procedure aPecatiKarticaExecute(Sender: TObject);
    procedure aDizajnExecute(Sender: TObject);
    procedure aPecatiGrafikExecute(Sender: TObject);
    procedure aKarticaMailExecute(Sender: TObject);
    procedure KarticaIzvestaj(po_mail : boolean);
    procedure aSpustiSoberiExecute(Sender: TObject);
    procedure SintKarticaIzvestaj(po_mail : boolean);
    procedure aPecatiSintetickaKarticaExecute(Sender: TObject);
    procedure aDizajnSintExecute(Sender: TObject);
    procedure aBrisiIzgledExecute(Sender: TObject);
    procedure aMagacinExecute(Sender: TObject);
    procedure tblReMagacinBeforeOpen(DataSet: TDataSet);
    procedure aHideFieldsExecute(Sender: TObject);
  private
    { Private declarations }

    _sifra_kluc :array[1..10] of string;  // �������� ���������� �� ������ �� ������� �� ������������� ���

  protected
    show_print : boolean;
    show_prikaz, sobrano : boolean;
    mod_rabota : integer;
    show_fr_print : boolean;
    list : TList;
    inserting : boolean;
    prva, posledna :TWinControl;
    procedure prefrli;

  public
    { Public declarations }
    show_preview : boolean;

    constructor Create(Owner : TComponent; insert : boolean = true; mod_na_rabota : integer = -1);reintroduce; overload;

    procedure SetSifra(br: integer; s : string);
    function GetSifra(br: integer) : string;

    //������� �� ���������� ������������ _sifra_kluc
    property sifra_kluc[br :integer]: string read GetSifra write SetSifra;
  end;

var
  frmLagerArtikli: TfrmLagerArtikli;

implementation

uses DaNe, dmKonekcija, Utils, dmResources, FormConfig, dmUnit, dmMaticni,
  Magacin;

{$R *.dfm}
//------------------------------------------------------------------------------

constructor TfrmLagerArtikli.Create(Owner : TComponent; insert : boolean = true; mod_na_rabota : integer = -1);
begin
inherited Create(Owner);
//	�������� ���� ������� � �������� �� ����������� ���������
//	insert = true -> ����� ���� ����� ���������� �� ������� �� Insert Mode
//  insert = false -> ����� ���� ����� ������� �� ����� �� ������. �� ������� ����� ����� �� �� �������� F5
  inserting := insert;
  mod_rabota := mod_na_rabota;
end;

//	����� �� �����������. �� ��������� ������� �� ������, �� �������� ������� �� ���� ��������,
//	�� ������� ������� �� ������ ������ � �� ������� �� Insert Mode
procedure TfrmLagerArtikli.aNovExecute(Sender: TObject);
begin

end;

//	����� �� ���������. �� ��������� ������� �� ������, �� �������� ������� �� ���� ��������,
//	�� ������� ������� �� ������ ������ � �� ������� �� Edit Mode
procedure TfrmLagerArtikli.aAzurirajExecute(Sender: TObject);
begin

end;

//	����� �� ������ �� ������������� �����
procedure TfrmLagerArtikli.aBrisiExecute(Sender: TObject);
begin
//  if ((cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) and
//     (cxGrid1DBTableView1.DataController.RecordCount <> 0)) then
//    cxGrid1DBTableView1.DataController.DataSet.Delete();
end;

procedure TfrmLagerArtikli.aBrisiIzgledExecute(Sender: TObject);
begin
  brisiGridVoBaza(Name,cxGrid1DBTableView1);
  brisiGridVoBaza(Name,cxGrid1DBTableView2);
//  brisiPivotVoBaza(Name,cxDBPivotGrid1); �� pivot ��� ���
  BrisiFormaIzgled(self);
end;

//	����� �� ���������� �� ����������
procedure TfrmLagerArtikli.aRefreshExecute(Sender: TObject);
begin
  cxGrid1DBTableView1.DataController.Filter.Root.Clear; //���������� �� ��������
  cxGrid1DBTableView1.DataController.DataSet.Refresh;
end;

//	����� �� ����� �� ������� �� ������� �� ������� �����
procedure TfrmLagerArtikli.aIzlezExecute(Sender: TObject);
begin
  Close;
end;

procedure TfrmLagerArtikli.aKarticaMailExecute(Sender: TObject);
begin
  KarticaIzvestaj(true);
end;

procedure TfrmLagerArtikli.aMagacinExecute(Sender: TObject);
begin
  frmMagacin := TfrmMagacin.Create(nil);
  frmMagacin.ShowModal;
  frmMagacin.Free;

  Caption := '���������� �� : ' + IntToStr(tblReMagacinID.Value) + ' - ' + tblReMagacinNAZIV.Value;
end;

//	����� �� ������� �� �������� �� ������ �� ���� (Utils.pas)
procedure TfrmLagerArtikli.aSnimiIzgledExecute(Sender: TObject);
begin
  zacuvajGridVoBaza(Name,cxGrid1DBTableView1);
  zacuvajGridVoBaza(Name,cxGrid1DBTableView2);
  ZacuvajFormaIzgled(self);
end;

//	����� �� ������� �� ������ �� Excel ������ (Utils.pas)
procedure TfrmLagerArtikli.aZacuvajExcelExecute(Sender: TObject);
begin
  zacuvajVoExcel(cxGrid1, Caption);
end;

//  ��������� �� ������ �� ������� �� ���������� �� ����� �� Enter
procedure TfrmLagerArtikli.EnterKakoTab(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  kom : TWinControl;
begin
    kom := Sender as TWinControl;
    case Key of
        VK_DOWN:
        begin
          PostMessage(Handle,WM_NextDlgCtl,0,0);
          key:=0;
        end;
        VK_UP:
        begin
          PostMessage(Handle,WM_NextDlgCtl,1,0);
          key:=0;
        end;
        VK_RETURN:
        begin
          PostMessage(Handle,WM_NextDlgCtl,0,0);
        end;
        VK_INSERT:
        begin

        end;
    end;
end;

//	������� �� ��� �� ������ ��� �������� �� �����
procedure TfrmLagerArtikli.cxDBTextEditAllEnter(Sender: TObject);
begin
    TEdit(Sender).Color:=clSkyBlue;
end;

//	������� �� ��� �� ������ ��� ����� �� �����
procedure TfrmLagerArtikli.cxDBTextEditAllExit(Sender: TObject);
begin
    TEdit(Sender).Color:=clWhite;
end;

procedure TfrmLagerArtikli.cxGrid1DBTableView1FilterCustomization(
  Sender: TcxCustomGridTableView; var ADone: Boolean);
begin
end;

//  ��������� �� ������ �� ��������� �� ���������� ���� �� ��������
//  ��������� ��: br - ����� ��� �� ������ (�� 0 �� 9) �� ��������� �� ����� �� ����� �� ���������� ����
//  s - ��������� �� ������ �� ������ (���������� �� string �� ����� �� integer �� IntToStr ���������)
procedure TfrmLagerArtikli.SetSifra(br: integer; s : string);
begin
  _sifra_kluc[br] := s;
end;

procedure TfrmLagerArtikli.TipSifraExit(Sender: TObject);
begin

end;

//  ������� �� ������ �� ��������� �� ���������� ���� �� ��������
//  ��������� ��: br - ����� ��� �� ������ (�� 0 �� 9) �� ��������� �� ����� �� ����� �� ���������� ����
function TfrmLagerArtikli.GetSifra(br: integer) : string;
begin
  Result := _sifra_kluc[br];
end;

procedure TfrmLagerArtikli.PartnerPropertiesChange(Sender: TObject);
begin

end;

//  �� ������� �� ������� ��� ����������� �� Master-�� �� �������� �� ��������� �� ������ ����� SetSifra
procedure TfrmLagerArtikli.prefrli;
begin
end;

procedure TfrmLagerArtikli.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    //	��������� ��� ��������� �� �������
//    if (cxGrid1DBTableView1.DataController.DataSource.State = dsEdit) or (cxGrid1DBTableView1.DataController.DataSource.State = dsInsert) then
//    begin
//        frmDaNe := TfrmDaNe.Create(self, '���������� ��������', '���������� �� �� ��������. ���� ������ �� �� �������?', 1);
//        if (frmDaNe.ShowModal <> mrYes) then
//        begin
//            cxGrid1DBTableView1.DataController.DataSet.Cancel;
//            Action := caFree;
//        end
//        else
//          if (Validacija(dPanel) = false) then
//          begin
//            cxGrid1DBTableView1.DataController.DataSet.Post;
//            Action := caFree;
//          end
//          else Action := caNone;
//    end;
end;

procedure TfrmLagerArtikli.FormCreate(Sender: TObject);
begin
  ProcitajFormaIzgled(self);
  if (dmRes.use_skin = true) then dxRibbon1.ColorSchemeName := dmRes.skin_name;

  show_prikaz := false;
  show_print := false;
  show_fr_print := false;
  show_preview := true;
end;

//------------------------------------------------------------------------------

procedure TfrmLagerArtikli.FormShow(Sender: TObject);
var kraj_god : Extended;
begin

  //  ������� �� ������������ ������������/���������� �� ���������� �� ���� �����
    SpremiForma(self);

  //	�� ��������� �� ������� ������ ��� ���������� � ���� � ��� �������� �� �������
    PrvPosledenTab(dPanel,posledna,prva);
//    dxBarManager1Bar1.Caption := Caption;
    dxComponentPrinter1Link1.ReportTitle.Text := Caption;

  //	������� �� ������������ �� ������ �� ������
    procitajGridOdBaza(Name,cxGrid1DBTableView1,false,false);
    procitajGridOdBaza(Name,cxGrid1DBTableView2,false,false);
  //	������� �� ������������ �� ��������� �� ������
    procitajPrintOdBaza(Name,cxGrid1DBTableView1.Name, dxComponentPrinter1Link1);

    kraj_god := StrToDate('31.12.' + godina);
    if kraj_god > Date() then
      DoDatum.Date := Date()
    else
      DoDatum.Date := kraj_god;

    sobrano := true;

    tblReMagacin.Close;
    tblReMagacin.Open;

    dm.tblLagerArtikli.Close;
    lcbMagacin.EditValue := tblReMagacinID.Value;
    lcbMagacin.SetFocus;

    aHideFields.Execute;
end;
//------------------------------------------------------------------------------

procedure TfrmLagerArtikli.SaveToIniFileExecute(Sender: TObject);
begin
//    cxGrid1DBTableView1.StoreToIniFile(Name,True,[]);
end;
//------------------------------------------------------------------------------

procedure TfrmLagerArtikli.cxGrid1DBTableView1KeyPress(Sender: TObject;
  var Key: Char);
begin
  if(Ord(Key) <> VK_RETURN) then cxSortiraj(cxGrid1DBTableView1);
end;

procedure TfrmLagerArtikli.cxGrid2DBChartView1GetValueHint(
  Sender: TcxGridChartView; ASeries: TcxGridChartSeries; AValueIndex: Integer;
  var AHint: string);
begin
  AHint := '����� : '+ VarToStr(FormatFloat('###,##0.00',ASeries.VisibleValues[AValueIndex])) + ' �������� ����� : ' + sender.Categories.VisibleDisplayTexts[AValueIndex];
end;


//  ����� �� �����
procedure TfrmLagerArtikli.aZapisiExecute(Sender: TObject);
var
  st: TDataSetState;
begin
//  ZapisiButton.SetFocus;

//  st := cxGrid1DBTableView1.DataController.DataSet.State;
//  if st in [dsEdit,dsInsert] then
//  begin
//    if (Validacija(dPanel) = false) then
//    begin
//      if ((st = dsInsert) and inserting) then
//      begin
//        cxGrid1DBTableView1.DataController.DataSet.Post;
//        aNov.Execute;
//      end;
//
//      if ((st = dsInsert) and (not inserting)) then
//      begin
//        cxGrid1DBTableView1.DataController.DataSet.Post;
//        dPanel.Enabled:=false;
//        lPanel.Enabled:=true;
//        cxGrid1.SetFocus;
//      end;
//
//      if (st = dsEdit) then
//      begin
//        cxGrid1DBTableView1.DataController.DataSet.Post;
//        dPanel.Enabled:=false;
//        lPanel.Enabled:=true;
//        cxGrid1.SetFocus;
//      end;
//    end;
//  end;
end;

//	����� �� ���������� �� �������
procedure TfrmLagerArtikli.aOtkaziExecute(Sender: TObject);
begin
//  if (cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) then
//  begin
//      ModalResult := mrCancel;
      Close();
//  end
//  else
//  begin
//      cxGrid1DBTableView1.DataController.DataSet.Cancel;
//      RestoreControls(dPanel);
//      dPanel.Enabled := false;
//      lPanel.Enabled := true;
//      cxGrid1.SetFocus;
//  end;
end;

//----------------------------------------------------------------------------------
// ��������� �� ��������, ���������� �� ��������� � �������� �� Print ��������
//----------------------------------------------------------------------------------

//	����� �� �������� �� �������� �� �������
procedure TfrmLagerArtikli.aPageSetupExecute(Sender: TObject);
begin
  dxComponentPrinter1Link1.PageSetup;
end;

//	����� �� ������� �� ������
procedure TfrmLagerArtikli.aPecatiGrafikExecute(Sender: TObject);
begin
  dxComponentPrinter1Link2.ReportTitle.Text := '����� �����';

  dxComponentPrinter1Link2.PrinterPage.PageHeader.RightTitle.Clear;
  dxComponentPrinter1Link2.PrinterPage.PageHeader.RightTitle.Add(dmkon.firma_naziv);
  dxComponentPrinter1Link2.PrinterPage.PageHeader.RightTitle.Add(DateTimeToStr(Now));

  dxComponentPrinter1Link2.PrinterPage.PageHeader.LeftTitle.Clear;
  dxComponentPrinter1Link2.PrinterPage.PageHeader.LeftTitle.Add('������� : ' + lcbMagacin.EditValue + ' - ' + lcbMagacin.Text);
  dxComponentPrinter1Link2.PrinterPage.PageHeader.LeftTitle.Add('����� �� : ' + DoDatum.Text);

  dxComponentPrinter1.Preview(true, dxComponentPrinter1Link2);
end;

procedure TfrmLagerArtikli.aPecatiKarticaExecute(Sender: TObject);
begin
  KarticaIzvestaj(false);
end;

procedure TfrmLagerArtikli.aPecatiSintetickaKarticaExecute(Sender: TObject);
begin
  SintKarticaIzvestaj(false);
end;

procedure TfrmLagerArtikli.KarticaIzvestaj(po_mail : boolean);
begin
  ZapisiButton.SetFocus;
  if (Validacija(dPanel) = false) then
  begin
    try
      //

    except
      ShowMessage('�� ���� �� �� ������� ���������!');
    end;
  end;

end;

procedure TfrmLagerArtikli.SintKarticaIzvestaj(po_mail : boolean);
begin
  ZapisiButton.SetFocus;
  if (Validacija(dPanel) = false) then
  begin
    try
      //

    except
      ShowMessage('�� ���� �� �� ������� ���������!');
    end;
  end;

end;

procedure TfrmLagerArtikli.tblReMagacinBeforeOpen(DataSet: TDataSet);
begin
  tblReMagacin.ParamByName('user').Value := dmKon.user;
  tblReMagacin.ParamByName('app').Value := dmKon.aplikacija;
end;

procedure TfrmLagerArtikli.aPecatiTabelaExecute(Sender: TObject);
begin
  if cxGrid1LevelFIFO.Active = true then
    dxComponentPrinter1Link1.ReportTitle.Text := '����� �����';
  if cxGrid1LevelSerija.Active = true then
    dxComponentPrinter1Link1.ReportTitle.Text := '����� �����(�� ���ȣ�)';

  dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Clear;
  dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Add(dmkon.firma_naziv);
  dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Add(DateTimeToStr(Now));

  dxComponentPrinter1Link1.PrinterPage.PageHeader.LeftTitle.Clear;
  dxComponentPrinter1Link1.PrinterPage.PageHeader.LeftTitle.Add('������� : ' + IntToStr(lcbMagacin.EditValue) + ' - ' + tblReMagacinNAZIV.Value);
  dxComponentPrinter1Link1.PrinterPage.PageHeader.LeftTitle.Add('����� �� : ' + DoDatum.Text);
  dxComponentPrinter1.Preview(true, dxComponentPrinter1Link1);
end;

//	����� �� ���������� �� ��������� �� �������
procedure TfrmLagerArtikli.aPodesuvanjePecatenjeExecute(Sender: TObject);
begin
  dxComponentPrinter1Link1.DesignReport();
end;

procedure TfrmLagerArtikli.aPrikaziExecute(Sender: TObject);
var i : integer;
begin
  ZapisiButton.SetFocus;

  if ((lcbMagacin.Text <> '') and (DoDatum.Text <> '')) then
  begin
    dm.tblLagerArtikli.Close;
    dm.tblLagerArtikli.ParamByName('re').Value := lcbMagacin.EditValue;
    dm.tblLagerArtikli.ParamByName('datum').Value := DoDatum.EditValue;
    dm.tblLagerArtikli.ParamByName('firma').Value := dmkon.firma_id;
    dm.tblLagerArtikli.Open;

    if (tblReMagacinOSLOBODUVANJE.Value = 0) then
    begin
      // FIFO
      dm.tblLagerArtikliSerija.Close; // zatvori go po serija
      cxGrid1LevelSerija.Visible := false;
    end;

    if (tblReMagacinOSLOBODUVANJE.Value = 3) then
    begin
      // �� �����
      dm.tblLagerArtikliSerija.Close;
      dm.tblLagerArtikliSerija.ParamByName('re').Value := lcbMagacin.EditValue;
      dm.tblLagerArtikliSerija.ParamByName('datum').Value := DoDatum.EditValue;
      dm.tblLagerArtikliSerija.Open;

      cxGrid1LevelSerija.Visible := true;
      //cxGrid1LevelSerija.Active := true;
    end;

    cxGrid1LevelFIFO.Active := true;
  end
  else
  begin
    ShowMessage('������! �������� ������� � ����� �� �� �� �� �������� �������!');
    lcbMagacin.SetFocus;
  end;
end;

procedure TfrmLagerArtikli.aSnimiPecatenjeExecute(Sender: TObject);
begin
  zacuvajPrintVoBaza(Name,cxGrid1DBTableView1.Name,dxComponentPrinter1Link1);
end;

procedure TfrmLagerArtikli.aSpustiSoberiExecute(Sender: TObject);
begin
  if (sobrano = true) then
  begin
    cxGrid1DBTableView1.ViewData.Expand(false);
    sobrano := false;
  end
  else
  begin
    cxGrid1DBTableView1.ViewData.Collapse(false);
    sobrano := true;
  end;
end;

//	����� �� ������ �� ����������� �������� �� ������� (Utils.pas)
procedure TfrmLagerArtikli.aBrisiPodesuvanjePecatenjeExecute(Sender: TObject);
begin
  brisiPrintOdBaza(Name,cxGrid1DBTableView1.Name, dxComponentPrinter1Link1);
end;

procedure TfrmLagerArtikli.aDizajnExecute(Sender: TObject);
begin
  if (Validacija(dPanel) = false) then
  begin
    try
      dmRes.Spremi('FIN',272);
      dmRes.frxReport1.DesignReport();
    except
      ShowMessage('�� ���� �� �� ������� ���������!');
    end;
  end;
end;

procedure TfrmLagerArtikli.aDizajnSintExecute(Sender: TObject);
begin
  if (Validacija(dPanel) = false) then
  begin
    try
      dmRes.Spremi('FIN',284);
      dmRes.frxReport1.DesignReport();
    except
      ShowMessage('�� ���� �� �� ������� ���������!');
    end;
  end;
end;

//  ����� �� ����� �� ����� �� ������������� �� �������� � ��������������� ��
//  �������(������ ���������� ��������)
procedure TfrmLagerArtikli.aFormConfigExecute(Sender: TObject);
begin
  frmFormConfig := TfrmFormConfig.Create(Application);
  frmFormConfig.formPtr := Addr(Self);
  frmFormConfig.ShowModal;
  frmFormConfig.Free;
end;

procedure TfrmLagerArtikli.aHideFieldsExecute(Sender: TObject);
var
  i, j, broj, broj2 : integer;
begin
  broj := cxGrid1DBTableView1.ColumnCount;
  broj2 := cxGrid1DBTableView2.ColumnCount;
  if mod_rabota = 0 then
  begin
    // skrij gi 1 i 2
    for i := 0 to (broj-1) do
    begin
      if cxGrid1DBTableView1.Columns[i].Tag = 1 then
      begin
        cxGrid1DBTableView1.Columns[i].VisibleForCustomization := false;
        cxGrid1DBTableView1.Columns[i].Visible := false;
      end;
      if cxGrid1DBTableView1.Columns[i].Tag = 2 then
      begin
        cxGrid1DBTableView1.Columns[i].VisibleForCustomization := false;
        cxGrid1DBTableView1.Columns[i].Visible := false;
      end;
    end;

    // skrij gi 1 i 2 TABLEVIEW2
    for j := 0 to (broj2-1) do
    begin
      if cxGrid1DBTableView2.Columns[j].Tag = 1 then
      begin
        cxGrid1DBTableView2.Columns[j].VisibleForCustomization := false;
        cxGrid1DBTableView2.Columns[j].Visible := false;
      end;
      if cxGrid1DBTableView2.Columns[j].Tag = 2 then
      begin
        cxGrid1DBTableView2.Columns[j].VisibleForCustomization := false;
        cxGrid1DBTableView2.Columns[j].Visible := false;
      end;
    end;
  end
  else
  if mod_rabota = 1 then
  begin
    // skrij gi 2
    for i := 0 to (broj-1) do
    begin
      if cxGrid1DBTableView1.Columns[i].Tag = 2 then
      begin
        cxGrid1DBTableView1.Columns[i].VisibleForCustomization := false;
        cxGrid1DBTableView1.Columns[i].Visible := false;
      end;
    end;

    // skrij gi 2  TABLEVIEW2
    for j := 0 to (broj2-1) do
    begin
      if cxGrid1DBTableView2.Columns[j].Tag = 2 then
      begin
        cxGrid1DBTableView2.Columns[j].VisibleForCustomization := false;
        cxGrid1DBTableView2.Columns[j].Visible := false;
      end;
    end;
  end
  else
  if mod_rabota = 2 then
  begin
    // pojavi gi site
  end
end;

//----------------------------------------------------------------------------------
// ���� -> ��������� �� ��������, ���������� �� ��������� � �������� �� Print ��������
//----------------------------------------------------------------------------------

// ��������� �� ���������� �� ��������� � �������� �� Print �������� �� .ini ��������
procedure TfrmLagerArtikli.zacuvajPrintVoIni(ime:AnsiString);
var
  AStream: TFileStream;
begin
  AStream := TFileStream.Create(dmKon.RabotenDir + ime + 'Print.ini', fmCreate);
  try
    AStream.WriteComponent(dxComponentPrinter1Link1);
  finally
    AStream.Free;
  end;

end;

// ��������� �� ������ �� ��������� � �������� �� Print �������� �������� �� .ini ��������
procedure TfrmLagerArtikli.procitajPrintOdIni(ime:AnsiString);
var
  AStream: TFileStream;
begin
  AStream := TFileStream.Create(dmKon.RabotenDir + ime + 'Print.ini', fmOpenRead);
  try
    dxComponentPrinter1Link1.RestoreDefaults;
    AStream.ReadComponent(dxComponentPrinter1Link1);
  finally
    AStream.Free;
  end;
end;


end.
