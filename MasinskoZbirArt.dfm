object frmMasinskoZbirArt: TfrmMasinskoZbirArt
  Left = 128
  Top = 212
  BorderIcons = [biSystemMenu, biMaximize]
  Caption = #1058#1086#1074#1072#1088#1077#1085' '#1051#1080#1089#1090' '#1052#1072#1096#1080#1085#1089#1082#1086' - '#1047#1073#1080#1088#1085#1086
  ClientHeight = 752
  ClientWidth = 1084
  Color = clBtnFace
  Constraints.MinHeight = 500
  Constraints.MinWidth = 610
  Font.Charset = RUSSIAN_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  ShowHint = True
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object dxRibbon1: TdxRibbon
    Left = 0
    Top = 0
    Width = 1084
    Height = 126
    BarManager = dxBarManager1
    ColorSchemeName = 'Blue'
    Contexts = <>
    TabOrder = 0
    TabStop = False
    object dxRibbon1Tab1: TdxRibbonTab
      Active = True
      Caption = #1052#1077#1085#1080
      Groups = <
        item
          ToolbarName = 'dxBarManager1Bar1'
        end
        item
          ToolbarName = 'dxBarManager1Bar2'
        end
        item
          ToolbarName = 'dxBarManager1Bar3'
        end>
      Index = 0
    end
    object dxRibbon1Tab2: TdxRibbonTab
      Caption = #1055#1086#1076#1077#1089#1091#1074#1072#1114#1072
      Groups = <
        item
          ToolbarName = 'dxBarManager1Bar4'
        end
        item
          ToolbarName = 'dxBarManager1BarIzgledGrid'
        end>
      Index = 1
    end
  end
  object StatusBar1: TdxRibbonStatusBar
    Left = 0
    Top = 729
    Width = 1084
    Height = 23
    Panels = <
      item
        PanelStyleClassName = 'TdxStatusBarTextPanelStyle'
        Text = 'F9 - '#1055#1088#1080#1082#1072#1078#1080', F10 - '#1055#1077#1095#1072#1090#1080',  Esc - '#1048#1079#1083#1077#1079
      end>
    Ribbon = dxRibbon1
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clDefault
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
  end
  object Panel1: TPanel
    Left = 0
    Top = 126
    Width = 1084
    Height = 81
    Align = alTop
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 2
    object cxlbl1: TcxLabel
      Left = 44
      Top = 31
      AutoSize = False
      Caption = #1048#1047#1041#1045#1056#1045#1058#1045' '#1050#1040#1052#1048#1054#1053#1048
      ParentFont = False
      Style.Font.Charset = RUSSIAN_CHARSET
      Style.Font.Color = clWindowText
      Style.Font.Height = -11
      Style.Font.Name = 'Tahoma'
      Style.Font.Style = [fsBold]
      Style.TextColor = clRed
      Style.IsFontAssigned = True
      Transparent = True
      Height = 25
      Width = 137
    end
    object cxButton1: TcxButton
      Left = 584
      Top = 26
      Width = 129
      Height = 25
      Action = aPrikazi
      TabOrder = 1
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -15
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object btLista: TcxButtonEdit
      Left = 176
      Top = 27
      ParentFont = False
      Properties.Buttons = <
        item
          Default = True
          Kind = bkEllipsis
        end>
      Properties.OnButtonClick = cxButtonEdit1PropertiesButtonClick
      Style.Font.Charset = RUSSIAN_CHARSET
      Style.Font.Color = clWindowText
      Style.Font.Height = -13
      Style.Font.Name = 'Tahoma'
      Style.Font.Style = []
      Style.IsFontAssigned = True
      TabOrder = 2
      Width = 361
    end
  end
  object Panel2: TPanel
    Left = 0
    Top = 207
    Width = 1084
    Height = 522
    Align = alClient
    TabOrder = 3
    object cxGrid1: TcxGrid
      Left = 1
      Top = 1
      Width = 1082
      Height = 520
      Align = alClient
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      LookAndFeel.Kind = lfUltraFlat
      LookAndFeel.NativeStyle = False
      object cxGrid1DBTableView1: TcxGridDBTableView
        OnKeyDown = cxGrid1DBTableView1KeyDown
        Navigator.Buttons.CustomButtons = <>
        Navigator.InfoPanel.Visible = True
        OnEditValueChanged = cxGrid1DBTableView1EditValueChanged
        OnFocusedItemChanged = cxGrid1DBTableView1FocusedItemChanged
        DataController.DataSource = dsLagerKT
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        FilterRow.InfoText = #1050#1083#1080#1082#1085#1077#1090#1077' '#1079#1072' '#1076#1072' '#1076#1077#1092#1080#1085#1080#1088#1072#1090#1077' '#1092#1080#1083#1090#1077#1088
        FilterRow.Visible = True
        Images = dmRes.cxSmallImages
        OptionsBehavior.IncSearch = True
        OptionsCustomize.ColumnsQuickCustomization = True
        OptionsData.CancelOnExit = False
        OptionsData.Deleting = False
        OptionsData.DeletingConfirmation = False
        OptionsData.Editing = False
        OptionsData.Inserting = False
        OptionsView.CellAutoHeight = True
        OptionsView.ColumnAutoWidth = True
        OptionsView.Footer = True
        OptionsView.Indicator = True
        Preview.Visible = True
        object cxGrid1DBTableView1VID_ARTIKAL: TcxGridDBColumn
          Caption = #1042#1080#1076
          DataBinding.FieldName = 'VID_ARTIKAL'
          HeaderAlignmentHorz = taCenter
          Options.Editing = False
          Width = 62
        end
        object cxGrid1DBTableView1ARTIKAL: TcxGridDBColumn
          Caption = #1064#1080#1092#1088#1072
          DataBinding.FieldName = 'ARTIKAL'
          HeaderAlignmentHorz = taCenter
          Options.Editing = False
          Width = 70
        end
        object cxGrid1DBTableView1NAZIV_ARTIKAL: TcxGridDBColumn
          Caption = #1052#1086#1076#1077#1083
          DataBinding.FieldName = 'NAZIV_ARTIKAL'
          HeaderAlignmentHorz = taCenter
          Options.Editing = False
          Width = 339
        end
        object cxGrid1DBTableView1BOJA_NOGARKI: TcxGridDBColumn
          Caption = #1041#1086#1112#1072' '#1085#1072' '#1076#1088#1074#1086
          DataBinding.FieldName = 'BOJA_NOGARKI'
          HeaderAlignmentHorz = taCenter
          Options.Editing = False
          Width = 158
        end
        object cxGrid1DBTableView1VK_KOLICINA: TcxGridDBColumn
          Caption = #1042#1082'. '#1082#1086#1083#1080#1095#1080#1085#1072
          DataBinding.FieldName = 'VK_KOLICINA'
          HeaderAlignmentHorz = taCenter
          Width = 82
        end
        object cxGrid1DBTableView1ALL_TRAILERS: TcxGridDBColumn
          Caption = #1050#1072#1084#1080#1086#1085#1080
          DataBinding.FieldName = 'ALL_TRAILERS'
          HeaderAlignmentHorz = taCenter
          Width = 357
        end
        object cxGrid1DBTableView1OPIS_MAT: TcxGridDBColumn
          DataBinding.FieldName = 'OPIS_MAT'
          Visible = False
        end
        object cxGrid1DBTableView1SKU: TcxGridDBColumn
          DataBinding.FieldName = 'SKU'
          HeaderAlignmentHorz = taCenter
          Width = 50
        end
        object cxGrid1DBTableView1ISPORAKA_OD_DATUM: TcxGridDBColumn
          Caption = #1044#1072#1090#1091#1084' '#1080#1089#1087#1086#1088#1072#1082#1072
          DataBinding.FieldName = 'ISPORAKA_OD_DATUM'
          Width = 50
        end
        object cxGrid1DBTableView1ISPORAKA_DO_DATUM: TcxGridDBColumn
          Caption = #1044#1072#1090#1091#1084' '#1076#1086' '#1080#1089#1087#1086#1088#1072#1082#1072
          DataBinding.FieldName = 'ISPORAKA_DO_DATUM'
          Visible = False
        end
      end
      object cxGrid1Level1: TcxGridLevel
        GridView = cxGrid1DBTableView1
      end
    end
  end
  object cxGridPopupMenu1: TcxGridPopupMenu
    Grid = cxGrid1
    PopupMenus = <>
    Left = 48
    Top = 676
  end
  object PopupMenu1: TPopupMenu
    Left = 80
    Top = 708
    object N1: TMenuItem
      Caption = #1057#1085#1080#1084#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      ShortCut = 49235
      OnClick = SaveToIniFileExecute
    end
    object Excel1: TMenuItem
      Caption = #1057#1085#1080#1084#1080' '#1075#1086' '#1074#1086' Excel'
      ShortCut = 49221
    end
  end
  object dxBarManager1: TdxBarManager
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Segoe UI'
    Font.Style = []
    Categories.Strings = (
      'Default')
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    ImageOptions.Images = dmRes.cxSmallImages
    ImageOptions.LargeImages = dmRes.cxLargeImages
    PopupMenuLinks = <>
    UseSystemFont = True
    Left = 112
    Top = 676
    PixelsPerInch = 96
    object dxBarManager1Bar1: TdxBar
      Caption = 'Custom 1'
      CaptionButtons = <>
      DockedDockingStyle = dsTop
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 715
      FloatTop = 157
      FloatClientWidth = 51
      FloatClientHeight = 120
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton1'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton2'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarLargeButton3'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = False
      WholeRow = False
    end
    object dxBarManager1Bar2: TdxBar
      Caption = #1040#1082#1094#1080#1080
      CaptionButtons = <>
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 789
      FloatTop = 120
      FloatClientWidth = 51
      FloatClientHeight = 120
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton10'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton5'
        end
        item
          Visible = True
          ItemName = 'dxbrlrgbtn1'
        end
        item
          Visible = True
          ItemName = 'dxBarSubItem1'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar3: TdxBar
      Caption = #1048#1079#1083#1077#1079
      CaptionButtons = <>
      DockedLeft = 301
      DockedTop = 0
      FloatLeft = 718
      FloatTop = 62
      FloatClientWidth = 51
      FloatClientHeight = 80
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton7'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton8'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar4: TdxBar
      Caption = #1055#1077#1095#1072#1090#1077#1114#1077
      CaptionButtons = <>
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 676
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton12'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton15'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton9'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarLargeButton13'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1BarIzgledGrid: TdxBar
      Caption = #1058#1072#1073#1077#1083#1072
      CaptionButtons = <>
      DockedLeft = 363
      DockedTop = 0
      FloatLeft = 808
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLBtnSnimiIzgled'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarLBtnBrisiIzgled'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarLargeButton2: TdxBarLargeButton
      Action = aAzuriraj
      Category = 0
    end
    object dxBarLargeButton3: TdxBarLargeButton
      Action = aBrisi
      Category = 0
    end
    object dxBarLargeButton4: TdxBarLargeButton
      Action = aSnimiIzgled
      Category = 0
      ScreenTip = tipSnimiIzgled
    end
    object dxBarLargeButton5: TdxBarLargeButton
      Action = aZacuvajExcel
      Category = 0
      ScreenTip = tipZacuvajExcel
    end
    object dxBarLargeButton6: TdxBarLargeButton
      Action = aPecatiTabela
      Caption = #1055#1077#1095#1072#1090#1080' '#1090#1072#1073#1077#1083#1072
      Category = 0
    end
    object dxBarLargeButton7: TdxBarLargeButton
      Action = aHelp
      Category = 0
    end
    object dxBarLargeButton8: TdxBarLargeButton
      Action = aIzlez
      Category = 0
    end
    object dxBarLargeButton1: TdxBarLargeButton
      Action = aNov
      Category = 0
    end
    object dxBarLargeButton9: TdxBarLargeButton
      Action = aSnimiPecatenje
      Category = 0
      ScreenTip = tipSnimiKonf
    end
    object dxBarLargeButton11: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object ribbonFilterReset: TdxBarLargeButton
      Caption = #1056#1077#1089#1077#1090#1080#1088#1072#1112' '#1092#1080#1083#1090#1077#1088
      Category = 0
      Hint = #1056#1077#1089#1077#1090#1080#1088#1072#1112' '#1092#1080#1083#1090#1077#1088
      Visible = ivAlways
    end
    object dxBarLargeButton12: TdxBarLargeButton
      Action = aPodesuvanjePecatenje
      Category = 0
      ScreenTip = tipDizajnReport
    end
    object dxBarLargeButton13: TdxBarLargeButton
      Action = aBrisiPodesuvanjePecatenje
      Category = 0
      ScreenTip = tipBrisiKonf
    end
    object dxBarLargeButton14: TdxBarLargeButton
      Caption = #1048#1079#1073#1088#1080#1096#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      Category = 0
      Visible = ivAlways
      LargeImageIndex = 40
    end
    object dxBarLargeButton15: TdxBarLargeButton
      Action = aPageSetup
      Category = 0
      ScreenTip = tipSetiranjeStrana
    end
    object dxBarLargeButton16: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object dxBarLargeButton17: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object cxBarEditItem1: TcxBarEditItem
      Caption = 'New Item'
      Category = 0
      Hint = 'New Item'
      Visible = ivAlways
      PropertiesClassName = 'TcxCheckBoxProperties'
    end
    object dxBarSubItem1: TdxBarSubItem
      Caption = #1055#1077#1095#1072#1090#1077#1114#1077
      Category = 0
      Visible = ivAlways
      ImageIndex = 30
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton6'
        end>
    end
    object dxBarLBtnSnimiIzgled: TdxBarLargeButton
      Action = aSnimiIzgled
      Category = 0
    end
    object dxBarLBtnBrisiIzgled: TdxBarLargeButton
      Action = aBrisiIzgled
      Category = 0
    end
    object dxBarLargeButton10: TdxBarLargeButton
      Action = aSpustiSoberi
      Category = 0
      Visible = ivNever
    end
    object dxbrlrgbtn1: TdxBarLargeButton
      Action = aPecati
      Category = 0
    end
    object dxBarLargeButton18: TdxBarLargeButton
      Action = aPromeni
      Category = 0
      Visible = ivNever
    end
  end
  object ActionList1: TActionList
    Images = dmRes.cxSmallImages
    Left = 16
    Top = 676
    object aNov: TAction
      Caption = #1053#1086#1074
      ImageIndex = 10
      ShortCut = 116
      OnExecute = aNovExecute
    end
    object aAzuriraj: TAction
      Caption = #1040#1078#1091#1088#1080#1088#1072#1112
      ImageIndex = 12
      ShortCut = 117
      OnExecute = aAzurirajExecute
    end
    object aBrisi: TAction
      Caption = #1041#1088#1080#1096#1080
      ImageIndex = 11
      ShortCut = 119
      OnExecute = aBrisiExecute
    end
    object aSnimiIzgled: TAction
      Caption = #1057#1085#1080#1084#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      ImageIndex = 9
      ShortCut = 24659
      OnExecute = aSnimiIzgledExecute
    end
    object aZacuvajExcel: TAction
      Caption = #1047#1072#1095#1091#1074#1072#1112' '#1074#1086' Excel'
      ImageIndex = 9
      ShortCut = 24645
      OnExecute = aZacuvajExcelExecute
    end
    object aPecatiTabela: TAction
      Caption = #1055#1077#1095#1072#1090#1080
      ImageIndex = 30
      OnExecute = aPecatiTabelaExecute
    end
    object aHelp: TAction
      Caption = #1055#1086#1084#1086#1096
      ImageIndex = 24
    end
    object aIzlez: TAction
      Caption = #1048#1079#1083#1077#1079
      ImageIndex = 25
      OnExecute = aIzlezExecute
    end
    object aZapisi: TAction
      Caption = #1047#1072#1087#1080#1096#1080
      ImageIndex = 7
      OnExecute = aZapisiExecute
    end
    object aOtkazi: TAction
      Caption = #1054#1090#1082#1072#1078#1080
      ImageIndex = 21
      ShortCut = 27
      OnExecute = aOtkaziExecute
    end
    object aRefresh: TAction
      Caption = 'aRefresh'
      ImageIndex = 18
      ShortCut = 118
      OnExecute = aRefreshExecute
    end
    object aSnimiPecatenje: TAction
      Caption = #1057#1085#1080#1084#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      ImageIndex = 39
      OnExecute = aSnimiPecatenjeExecute
    end
    object aPodesuvanjePecatenje: TAction
      Caption = #1044#1080#1079#1072#1112#1085' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090
      ImageIndex = 38
      OnExecute = aPodesuvanjePecatenjeExecute
    end
    object aBrisiPodesuvanjePecatenje: TAction
      Caption = #1041#1088#1080#1096#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      ImageIndex = 37
      OnExecute = aBrisiPodesuvanjePecatenjeExecute
    end
    object aPageSetup: TAction
      Caption = #1057#1077#1090#1080#1088#1072#1114#1077' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072
      ImageIndex = 38
      OnExecute = aPageSetupExecute
    end
    object aFormConfig: TAction
      Caption = 'aFormConfig'
      ShortCut = 24699
      OnExecute = aFormConfigExecute
    end
    object aBrisiIzgled: TAction
      Caption = #1041#1088#1080#1096#1080' '#1075#1086' '#1079#1072#1095#1091#1074#1072#1085#1080#1086#1090' '#1080#1079#1075#1083#1077#1076
      ImageIndex = 56
      OnExecute = aBrisiIzgledExecute
    end
    object aSpustiSoberi: TAction
      Caption = #1057#1087#1091#1096#1090#1080'/'#1057#1086#1073#1077#1088#1080' '#1058#1072#1073#1077#1083#1072
      ImageIndex = 65
      OnExecute = aSpustiSoberiExecute
    end
    object aPrikazi: TAction
      Caption = #1055#1088#1080#1082#1072#1078#1080
      ImageIndex = 19
      ShortCut = 120
      OnExecute = aPrikaziExecute
    end
    object aDizajner: TAction
      Caption = 'aDizajner'
      ShortCut = 24697
      OnExecute = aDizajnerExecute
    end
    object aPecati: TAction
      Caption = #1055#1077#1095#1072#1090#1080
      ImageIndex = 30
      ShortCut = 121
      OnExecute = aPecatiExecute
    end
    object aPromeni: TAction
      Caption = #1055#1088'o'#1084#1077#1085#1080' '#1051#1072#1075#1077#1088
      ImageIndex = 18
      OnExecute = aPromeniExecute
    end
    object aListaTrailers: TAction
      Caption = 'aListaTrailers'
      OnExecute = aListaTrailersExecute
    end
  end
  object dxComponentPrinter1: TdxComponentPrinter
    CurrentLink = dxComponentPrinter1Link1
    Version = 0
    Left = 16
    Top = 708
    object dxComponentPrinter1Link1: TdxGridReportLink
      DateFormat = 0
      PageNumberFormat = pnfNumeral
      PrinterPage.DMPaper = 9
      PrinterPage.Footer = 6350
      PrinterPage.GrayShading = True
      PrinterPage.Header = 6350
      PrinterPage.Margins.Bottom = 12700
      PrinterPage.Margins.Left = 12700
      PrinterPage.Margins.Right = 12700
      PrinterPage.Margins.Top = 12700
      PrinterPage.Orientation = poLandscape
      PrinterPage.PageFooter.RightTitle.Strings = (
        #1057#1090#1088'.[Page #] '#1086#1076' [Total Pages]')
      PrinterPage.PageSize.X = 210000
      PrinterPage.PageSize.Y = 297000
      PrinterPage._dxMeasurementUnits_ = 0
      PrinterPage._dxLastMU_ = 2
      ReportDocument.CreationDate = 40336.425359629630000000
      TimeFormat = 0
      OptionsFormatting.UseNativeStyles = True
      OptionsOnEveryPage.Caption = False
      OptionsOnEveryPage.FilterBar = False
      OptionsView.Caption = False
      OptionsView.FilterBar = False
      StyleRepository = dmRes.cxStyleRepositoryPrinting
      Styles.StyleSheet = dmRes.dxGridReportLinkStyleSheet
      BuiltInReportLink = True
    end
  end
  object dxBarScreenTipRepository1: TdxBarScreenTipRepository
    Left = 144
    Top = 676
    PixelsPerInch = 96
    object tipSnimiIzgled: TdxBarScreenTip
      Header.Text = #1057#1085#1080#1084#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      Description.Text = #1047#1072#1095#1091#1074#1072#1112' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090' '#1085#1072' '#1092#1086#1088#1084#1072#1090#1072' '#1080' '#1085#1072' '#1090#1072#1073#1077#1083#1072#1090#1072
    end
    object tipZacuvajExcel: TdxBarScreenTip
      Header.Text = #1047#1072#1095#1091#1074#1072#1112' '#1074#1086' Excel'
      Description.Text = #1057#1085#1080#1084#1080' '#1075#1080' '#1087#1086#1076#1072#1090#1086#1094#1080#1090#1077' '#1086#1076' '#1090#1072#1073#1077#1083#1072#1090#1072' '#1074#1086' Excel '#1092#1086#1088#1084#1072#1090
    end
    object tipDizajnReport: TdxBarScreenTip
      Header.Text = #1044#1080#1079#1072#1112#1085' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090
      Description.Text = #1055#1086#1076#1077#1089#1091#1074#1072#1114#1072' '#1085#1072' '#1080#1079#1075#1083#1077#1076#1086#1090' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090' '#1085#1072' '#1090#1072#1073#1077#1083#1072#1090#1072
    end
    object tipSetiranjeStrana: TdxBarScreenTip
      Header.Text = #1057#1077#1090#1080#1088#1072#1114#1077' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072
      Description.Text = #1055#1086#1076#1077#1089#1091#1074#1072#1114#1077' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072' '#1085#1072' '#1082#1086#1112#1072' '#1116#1077' '#1089#1077' '#1087#1077#1095#1072#1090#1080' '#1090#1072#1073#1077#1083#1072#1090#1072
    end
    object tipSnimiKonf: TdxBarScreenTip
      Header.Text = #1057#1085#1080#1084#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      Description.Text = #1047#1072#1095#1091#1074#1072#1112' '#1075#1080' '#1087#1086#1076#1077#1089#1091#1074#1072#1114#1072#1090#1072' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090' '#1080' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072' '#1079#1072' '#1087#1077#1095#1072#1090#1077#1114#1077
    end
    object tipBrisiKonf: TdxBarScreenTip
      Header.Text = #1041#1088#1080#1096#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      Description.Text = #1048#1079#1073#1088#1080#1096#1080' '#1075#1080' '#1087#1086#1076#1077#1089#1091#1074#1072#1114#1072#1090#1072' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090' '#1080' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072' '#1079#1072' '#1087#1077#1095#1072#1090#1077#1114#1077
    end
  end
  object tblLagerKT: TpFIBDataSet
    RefreshSQL.Strings = (
      
        'select P.OPIS_MAT, P.BOJA_NOGARKI, P.VID_ARTIKAL, P.ARTIKAL, P.N' +
        'AZIV_ARTIKAL, P.VK_KOLICINA, P.ALL_TRAILERS  , p.SKU , p.ISPORAK' +
        'A_OD_DATUM,p.ISPORAKA_DO_DATUM'
      'from PROC_MAN_MF_ZIRNO(:IN_ID) P')
    SelectSQL.Strings = (
      
        'select P.OPIS_MAT, P.BOJA_NOGARKI, P.VID_ARTIKAL, P.ARTIKAL, P.N' +
        'AZIV_ARTIKAL, P.VK_KOLICINA, P.ALL_TRAILERS  , p.sku, p.ISPORAKA' +
        '_OD_DATUM, p.ISPORAKA_do_DATUM'
      'from PROC_MAN_MF_ZIRNO(:IN_ID) P'
      'order by P.ARTIKAL, P.NAZIV_ARTIKAL  ')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 48
    Top = 708
    object tblLagerKTOPIS_MAT: TFIBStringField
      FieldName = 'OPIS_MAT'
      Size = 2117
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblLagerKTVID_ARTIKAL: TFIBIntegerField
      FieldName = 'VID_ARTIKAL'
    end
    object tblLagerKTARTIKAL: TFIBIntegerField
      FieldName = 'ARTIKAL'
    end
    object tblLagerKTNAZIV_ARTIKAL: TFIBStringField
      FieldName = 'NAZIV_ARTIKAL'
      Size = 500
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblLagerKTVK_KOLICINA: TFIBFloatField
      FieldName = 'VK_KOLICINA'
    end
    object tblLagerKTALL_TRAILERS: TFIBStringField
      FieldName = 'ALL_TRAILERS'
      Size = 500
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblLagerKTBOJA_NOGARKI: TFIBStringField
      FieldName = 'BOJA_NOGARKI'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblLagerKTSKU: TFIBStringField
      FieldName = 'SKU'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblLagerKTISPORAKA_OD_DATUM: TFIBDateField
      FieldName = 'ISPORAKA_OD_DATUM'
    end
    object tblLagerKTISPORAKA_DO_DATUM: TFIBDateField
      FieldName = 'ISPORAKA_DO_DATUM'
    end
  end
  object dsLagerKT: TDataSource
    DataSet = tblLagerKT
    Left = 80
    Top = 676
  end
end
