unit KolicinaBroj;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  cxContainer, cxEdit, cxLabel, cxTextEdit, ExtCtrls, StdCtrls, FIBQuery,
  pFIBQuery, dxSkinsCore, dxSkinBlack, dxSkinBlue, dxSkinCaramel, dxSkinCoffee,
  dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy, dxSkinGlassOceans,
  dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky,
  dxSkinMcSkin, dxSkinMoneyTwins, dxSkinOffice2007Black, dxSkinOffice2007Blue,
  dxSkinOffice2007Green, dxSkinOffice2007Pink, dxSkinOffice2007Silver,
  dxSkinOffice2010Black, dxSkinOffice2010Blue, dxSkinOffice2010Silver,
  dxSkinPumpkin, dxSkinSeven, dxSkinSharp, dxSkinSilver, dxSkinSpringTime,
  dxSkinStardust, dxSkinSummer2008, dxSkinsDefaultPainters, dxSkinValentine,
  dxSkinXmas2008Blue, dxSkinBlueprint, dxSkinDevExpressDarkStyle,
  dxSkinDevExpressStyle, dxSkinHighContrast, dxSkinMetropolis,
  dxSkinMetropolisDark, dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray,
  dxSkinOffice2013White, dxSkinOffice2016Colorful, dxSkinOffice2016Dark,
  dxSkinSevenClassic, dxSkinSharpPlus, dxSkinTheAsphaltWorld,
  dxSkinVisualStudio2013Blue, dxSkinVisualStudio2013Dark,
  dxSkinVisualStudio2013Light, dxSkinVS2010, dxSkinWhiteprint, System.Actions,
  Vcl.ActnList;

type
  TfrmKolicinaBroj = class(TForm)
    Panel5: TPanel;
    txtKolicinaBroj: TcxTextEdit;
    cxLabel21: TcxLabel;
    qUpdateSetup: TpFIBQuery;
    qSiteId: TpFIBQuery;
    ActionList1: TActionList;
    aExit: TAction;
    procedure txtKolicinaBrojKeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormShow(Sender: TObject);
    procedure aExitExecute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
    da:Integer;
  end;

var
  frmKolicinaBroj: TfrmKolicinaBroj;

implementation

uses dmKonekcija, dmUnit;

{$R *.dfm}

procedure TfrmKolicinaBroj.aExitExecute(Sender: TObject);
begin
  Close;
end;

procedure TfrmKolicinaBroj.FormShow(Sender: TObject);
begin
 //   qSiteId.ExecQuery;
    txtKolicinaBroj.SetFocus;

end;

procedure TfrmKolicinaBroj.txtKolicinaBrojKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var v1:String;
begin
    if key=VK_RETURN then
    begin
         if txtKolicinaBroj.Text <> '' then
           begin
               da := 1;
               ModalResult := mrYes;
              // Close;
           end
          else
          begin
             ShowMessage('������� �������� �� ��������!');
             ModalResult := mrNo;
             da := 0;
             Abort;
          end;
    end
    else

    if key=VK_ESCAPE then
    begin
      ModalResult := mrCancel;
      //Close;
    end;
end;

end.
