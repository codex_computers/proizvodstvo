object frmUpDownStream: TfrmUpDownStream
  Left = 128
  Top = 212
  BorderIcons = [biSystemMenu, biMaximize]
  Caption = #1055#1088#1077#1075#1083#1077#1076#1080
  ClientHeight = 682
  ClientWidth = 851
  Color = clBtnFace
  Constraints.MinHeight = 500
  Constraints.MinWidth = 610
  Font.Charset = RUSSIAN_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  ShowHint = True
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object dxRibbon1: TdxRibbon
    Left = 0
    Top = 0
    Width = 851
    Height = 126
    BarManager = dxBarManager1
    ColorSchemeName = 'Blue'
    Contexts = <>
    TabOrder = 0
    TabStop = False
    object dxRibbon1Tab1: TdxRibbonTab
      Active = True
      Caption = #1052#1077#1085#1080
      Groups = <
        item
          ToolbarName = 'dxBarManager1Bar2'
        end
        item
          ToolbarName = 'dxBarManager1Bar3'
        end>
      Index = 0
    end
    object dxRibbon1Tab2: TdxRibbonTab
      Caption = #1055#1086#1076#1077#1089#1091#1074#1072#1114#1072
      Groups = <
        item
          ToolbarName = 'dxBarManager1Bar4'
        end
        item
          ToolbarName = 'dxBarManager1BarIzgledGrid'
        end>
      Index = 1
    end
  end
  object StatusBar1: TdxRibbonStatusBar
    Left = 0
    Top = 659
    Width = 851
    Height = 23
    Panels = <
      item
        PanelStyleClassName = 'TdxStatusBarTextPanelStyle'
        Text = 'Esc - '#1054#1090#1082#1072#1078#1080' / '#1048#1079#1083#1077#1079
      end>
    Ribbon = dxRibbon1
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clDefault
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
  end
  object Panel1: TPanel
    Left = 0
    Top = 126
    Width = 851
    Height = 533
    Align = alClient
    Caption = 'Panel1'
    TabOrder = 4
    object cxPageControl1: TcxPageControl
      Left = 1
      Top = 1
      Width = 849
      Height = 531
      Align = alClient
      TabOrder = 0
      Properties.ActivePage = tsProizvod
      ClientRectBottom = 531
      ClientRectRight = 849
      ClientRectTop = 24
      object tsProizvod: TcxTabSheet
        Caption = #1057#1091#1088#1086#1074#1080#1085#1080', '#1091#1087#1086#1090#1088#1077#1073#1077#1085#1080' '#1074#1086' '#1075#1086#1090#1086#1074' '#1087#1088#1086#1080#1079#1074#1086#1076', '#1089#1086' '#1051#1054#1058' '#1073#1088#1086#1077#1074#1080' '
        ImageIndex = 0
        object Panel3: TPanel
          Left = 0
          Top = 0
          Width = 849
          Height = 507
          Align = alClient
          TabOrder = 0
          object ts1Panel: TPanel
            Left = 1
            Top = 1
            Width = 847
            Height = 96
            Align = alTop
            TabOrder = 0
            object cxLabel1: TcxLabel
              Left = 18
              Top = 21
              Caption = #1047#1072' '#1051#1086#1090' '#1073#1088#1086#1112' '#1079#1072' '#1075#1086#1090#1086#1074' '#1087#1088#1086#1080#1079#1074#1086#1076
              Style.TextColor = clRed
              Properties.Alignment.Horz = taRightJustify
              Properties.WordWrap = True
              Transparent = True
              Width = 159
              AnchorX = 177
            end
            object cbUp: TcxLookupComboBox
              Tag = 1
              Left = 187
              Top = 20
              Hint = #1048#1079#1073#1077#1088#1080' '#1051#1054#1058' '#1085#1072' '#1075#1086#1090#1086#1074' '#1087#1088#1086#1080#1079#1074#1086#1076
              Properties.DropDownAutoSize = True
              Properties.DropDownSizeable = True
              Properties.KeyFieldNames = 'VID_PROIZVOD;proizvod'
              Properties.ListColumns = <
                item
                  Width = 30
                  FieldName = 'LOT_PROIZVOD'
                end
                item
                  Width = 20
                  FieldName = 'vid_proizvod'
                end
                item
                  Width = 20
                  FieldName = 'proizvod'
                end
                item
                  Width = 100
                  FieldName = 'naziv_proizvod'
                end>
              Properties.ListOptions.SyncMode = True
              Properties.ListSource = dsLotProizvod
              TabOrder = 1
              OnEnter = cxDBTextEditAllEnter
              OnExit = cxDBTextEditAllExit
              OnKeyDown = EnterKakoTab
              Width = 240
            end
            object cxLabel8: TcxLabel
              Left = 71
              Top = 47
              Caption = #1040#1088#1090#1080#1082#1072#1083' / '#1055#1088#1086#1080#1079#1074#1086#1076
              Style.TextColor = clRed
              Properties.Alignment.Horz = taRightJustify
              Properties.WordWrap = True
              Transparent = True
              Width = 106
              AnchorX = 177
            end
            object txtVidA: TcxTextEdit
              Left = 187
              Top = 46
              Properties.ReadOnly = True
              TabOrder = 3
              Width = 48
            end
            object txtA: TcxTextEdit
              Left = 235
              Top = 46
              Properties.ReadOnly = True
              TabOrder = 4
              Width = 48
            end
            object cbA: TcxTextEdit
              Left = 283
              Top = 46
              Properties.ReadOnly = True
              TabOrder = 5
              Width = 336
            end
            object cxButton1: TcxButton
              Left = 653
              Top = 44
              Width = 93
              Height = 25
              Caption = #1055#1088#1080#1082#1072#1078#1080
              TabOrder = 6
              OnClick = cxButton1Click
            end
          end
          object cxGrid1: TcxGrid
            Left = 1
            Top = 97
            Width = 847
            Height = 409
            Align = alClient
            Font.Charset = RUSSIAN_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            TabOrder = 1
            ExplicitLeft = 2
            ExplicitTop = 93
            object cxGrid1DBTableView1: TcxGridDBTableView
              DataController.DataSource = dm.dsUpStream
              DataController.Options = [dcoAssignGroupingValues, dcoAssignMasterDetailKeys, dcoSaveExpanding, dcoGroupsAlwaysExpanded]
              DataController.Summary.DefaultGroupSummaryItems = <>
              DataController.Summary.FooterSummaryItems = <
                item
                  Format = '#,##0.00'
                  Kind = skAverage
                  OnGetText = cxGrid1DBTableView1TcxGridDBDataControllerTcxDataSummaryFooterSummaryItems0GetText
                  FieldName = 'KOLICINA_PROIZVOD'
                  Column = cxGrid1DBTableView1KOLICINA_PROIZVOD
                end>
              DataController.Summary.SummaryGroups = <
                item
                  Links = <
                    item
                      Column = cxGrid1DBTableView1VID_PROIZVOD
                    end
                    item
                      Column = cxGrid1DBTableView1PROIZVOD
                    end
                    item
                      Column = cxGrid1DBTableView1MERKA_KOLICINA
                    end
                    item
                      Column = cxGrid1DBTableView1DATUM_RN
                    end
                    item
                      Column = cxGrid1DBTableView1LOT_PROIZVOD
                    end
                    item
                      Column = cxGrid1DBTableView1NAZIV_PROIZVOD
                    end
                    item
                      Column = cxGrid1DBTableView1ART_VID
                    end
                    item
                      Column = cxGrid1DBTableView1ART_SIF
                    end
                    item
                      Column = cxGrid1DBTableView1NAZIV_SUROVINA
                    end
                    item
                      Column = cxGrid1DBTableView1PAKETI
                    end
                    item
                      Column = cxGrid1DBTableView1IZV_EM
                    end
                    item
                      Column = cxGrid1DBTableView1BR_PAKET
                    end
                    item
                      Column = cxGrid1DBTableView1LOT_NO
                    end
                    item
                      Column = cxGrid1DBTableView1KOLICINA_SUROVINA
                    end
                    item
                      Column = cxGrid1DBTableView1MERKA
                    end
                    item
                      Column = cxGrid1DBTableView1KOLICINA_PROIZVOD
                    end>
                  SummaryItems = <
                    item
                      Format = '#,##0.00'
                      Kind = skAverage
                      OnGetText = cxGrid1DBTableView1TcxGridDBDataControllerTcxDataSummarySummaryGroups0SummaryItems0GetText
                      FieldName = 'KOLICINA_PROIZVOD'
                      Column = cxGrid1DBTableView1KOLICINA_PROIZVOD
                    end>
                end>
              FilterRow.InfoText = #1050#1083#1080#1082#1085#1077#1090#1077' '#1090#1091#1082#1072' '#1079#1072' '#1076#1072' '#1076#1077#1092#1080#1085#1080#1088#1072#1090#1077' '#1092#1080#1083#1090#1077#1088
              FilterRow.Visible = True
              OptionsCustomize.ColumnsQuickCustomization = True
              OptionsData.Deleting = False
              OptionsData.Editing = False
              OptionsData.Inserting = False
              OptionsView.NoDataToDisplayInfoText = '<'#1053#1077#1084#1072' '#1087#1086#1076#1072#1090#1086#1094#1080'>'
              OptionsView.GroupFooterMultiSummaries = True
              OptionsView.GroupFooters = gfVisibleWhenExpanded
              Preview.Place = ppTop
              Styles.Preview = dm.cxStyle2
              object cxGrid1DBTableView1VID_PROIZVOD: TcxGridDBColumn
                Caption = #1042#1080#1076' '#1085#1072' '#1087#1088#1086#1080#1079#1074#1086#1076
                DataBinding.FieldName = 'VID_PROIZVOD'
                Visible = False
              end
              object cxGrid1DBTableView1PROIZVOD: TcxGridDBColumn
                Caption = #1064#1080#1092#1088#1072' '#1085#1072' '#1087#1088#1086#1080#1079#1074#1086#1076
                DataBinding.FieldName = 'PROIZVOD'
                Visible = False
              end
              object cxGrid1DBTableView1MERKA_KOLICINA: TcxGridDBColumn
                Caption = #1052#1077#1088#1082#1072
                DataBinding.FieldName = 'MERKA_KOLICINA'
                Visible = False
              end
              object cxGrid1DBTableView1DATUM_RN: TcxGridDBColumn
                Caption = #1044#1072#1090#1091#1084' '#1056#1053
                DataBinding.FieldName = 'DATUM_RN'
                Visible = False
              end
              object cxGrid1DBTableView1LOT_PROIZVOD: TcxGridDBColumn
                Caption = #1051#1086#1090' '#1055#1088#1086#1080#1079#1074#1086#1076
                DataBinding.FieldName = 'LOT_PROIZVOD'
                Visible = False
                Width = 118
              end
              object cxGrid1DBTableView1ART_VID: TcxGridDBColumn
                Caption = #1042#1080#1076' '#1085#1072' '#1089#1091#1088#1086#1074#1080#1085#1072
                DataBinding.FieldName = 'ART_VID'
                Width = 23
              end
              object cxGrid1DBTableView1ART_SIF: TcxGridDBColumn
                Caption = #1064#1080#1092#1088#1072' '#1085#1072' '#1089#1091#1088#1086#1074#1080#1085#1072
                DataBinding.FieldName = 'ART_SIF'
                Width = 44
              end
              object cxGrid1DBTableView1NAZIV_PROIZVOD: TcxGridDBColumn
                Caption = #1055#1088#1086#1080#1079#1074#1086#1076
                DataBinding.FieldName = 'NAZIV_PROIZVOD'
                Visible = False
                GroupIndex = 0
                SortIndex = 0
                SortOrder = soAscending
                Styles.Content = dmRes.cxStyle22
                Width = 131
              end
              object cxGrid1DBTableView1NAZIV_SUROVINA: TcxGridDBColumn
                Caption = #1057#1091#1088#1086#1074#1080#1085#1072
                DataBinding.FieldName = 'NAZIV_SUROVINA'
                Width = 377
              end
              object cxGrid1DBTableView1PAKETI: TcxGridDBColumn
                DataBinding.FieldName = 'PAKETI'
                Visible = False
              end
              object cxGrid1DBTableView1IZV_EM: TcxGridDBColumn
                DataBinding.FieldName = 'IZV_EM'
                Visible = False
              end
              object cxGrid1DBTableView1BR_PAKET: TcxGridDBColumn
                DataBinding.FieldName = 'BR_PAKET'
                Visible = False
              end
              object cxGrid1DBTableView1LOT_NO: TcxGridDBColumn
                Caption = #1051#1086#1090' '#1089#1091#1088#1086#1074#1080#1085#1072
                DataBinding.FieldName = 'LOT_NO'
                Width = 113
              end
              object cxGrid1DBTableView1KOLICINA_SUROVINA: TcxGridDBColumn
                Caption = #1050#1086#1083#1080#1095#1080#1085#1072' '#1085#1072' '#1089#1091#1088#1086#1074#1080#1085#1072
                DataBinding.FieldName = 'KOLICINA_SUROVINA'
                Width = 104
              end
              object cxGrid1DBTableView1MERKA: TcxGridDBColumn
                Caption = #1052#1077#1088#1082#1072
                DataBinding.FieldName = 'MERKA'
                Width = 106
              end
              object cxGrid1DBTableView1KOLICINA_PROIZVOD: TcxGridDBColumn
                Caption = #1050#1086#1083#1080#1095#1080#1085#1072' '#1085#1072' '#1087#1088#1086#1080#1079#1074#1086#1076
                DataBinding.FieldName = 'KOLICINA_PROIZVOD'
                Visible = False
              end
            end
            object cxGrid1Level1: TcxGridLevel
              Caption = #1057#1091#1088#1086#1074#1080#1085#1080', '#1091#1087#1086#1090#1088#1077#1073#1077#1085#1080' '#1074#1086' '#1075#1086#1090#1086#1074' '#1087#1088#1086#1080#1079#1074#1086#1076', '#1089#1086' '#1051#1054#1058' '#1073#1088#1086#1077#1074#1080' '
              GridView = cxGrid1DBTableView1
              Options.DetailTabsPosition = dtpTop
            end
          end
        end
      end
      object tsSurovina: TcxTabSheet
        Caption = #1057#1091#1088#1086#1074#1080#1085#1072', '#1091#1087#1086#1090#1088#1077#1073#1077#1085#1072' '#1074#1086' '#1075#1086#1090#1086#1074#1080' '#1087#1088#1086#1080#1079#1074#1086#1076#1080', '#1089#1086' '#1051#1054#1058' '#1073#1088#1086#1077#1074#1080' '
        ImageIndex = 1
        object Panel5: TPanel
          Left = 0
          Top = 0
          Width = 849
          Height = 507
          Align = alClient
          Caption = 'Panel5'
          TabOrder = 0
          object Panel2: TPanel
            Left = 1
            Top = 1
            Width = 847
            Height = 96
            Align = alTop
            TabOrder = 0
            object cxLabel2: TcxLabel
              Left = 8
              Top = 20
              AutoSize = False
              Caption = #1047#1072' '#1051#1086#1090' '#1073#1088#1086#1112' '#1085#1072' '#1089#1091#1088#1086#1074#1080#1085#1072#1090#1072
              Style.TextColor = clRed
              Properties.Alignment.Horz = taRightJustify
              Properties.WordWrap = True
              Transparent = True
              Height = 25
              Width = 145
              AnchorX = 153
            end
            object cbDown: TcxLookupComboBox
              Tag = 1
              Left = 159
              Top = 19
              Hint = #1048#1079#1073#1077#1088#1080' '#1051#1054#1058' '#1085#1072' '#1089#1091#1088#1086#1074#1080#1085#1072
              Properties.DropDownAutoSize = True
              Properties.DropDownSizeable = True
              Properties.KeyFieldNames = 'VID_surovina;surovina'
              Properties.ListColumns = <
                item
                  Caption = #1051#1086#1090
                  Width = 30
                  FieldName = 'LOT_SUROVINA'
                end
                item
                  Caption = #1042#1080#1076
                  Width = 20
                  FieldName = 'vid_surovina'
                end
                item
                  Caption = #1057#1091#1088#1086#1074#1080#1085#1072
                  Width = 20
                  FieldName = 'surovina'
                end
                item
                  Caption = #1053#1072#1079#1080#1074
                  Width = 100
                  FieldName = 'naziv_surovina'
                end>
              Properties.ListOptions.SyncMode = True
              Properties.ListSource = dsLotSurovina
              TabOrder = 1
              OnEnter = cxDBTextEditAllEnter
              OnExit = cxDBTextEditAllExit
              OnKeyDown = EnterKakoTab
              Width = 240
            end
            object cxLabel3: TcxLabel
              Left = 47
              Top = 47
              Caption = #1040#1088#1090#1080#1082#1072#1083' / '#1057#1091#1088#1086#1074#1080#1085#1072
              Style.TextColor = clRed
              Properties.Alignment.Horz = taRightJustify
              Properties.WordWrap = True
              Transparent = True
              Width = 106
              AnchorX = 153
            end
            object txtVS: TcxTextEdit
              Left = 159
              Top = 46
              Properties.ReadOnly = True
              TabOrder = 3
              Width = 48
            end
            object txtS: TcxTextEdit
              Left = 207
              Top = 46
              Properties.ReadOnly = True
              TabOrder = 4
              Width = 48
            end
            object cbSurovina: TcxTextEdit
              Left = 255
              Top = 46
              Properties.ReadOnly = True
              TabOrder = 5
              Width = 336
            end
            object cxButton2: TcxButton
              Left = 635
              Top = 44
              Width = 93
              Height = 25
              Caption = #1055#1088#1080#1082#1072#1078#1080
              TabOrder = 6
              OnClick = cxButton2Click
            end
          end
          object cxGrid2: TcxGrid
            Left = 1
            Top = 97
            Width = 847
            Height = 409
            Align = alClient
            Font.Charset = RUSSIAN_CHARSET
            Font.Color = clWindowText
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            TabOrder = 1
            ExplicitTop = 93
            object cxGridDBTableView1: TcxGridDBTableView
              DataController.DataSource = dm.dsDownStream
              DataController.Options = [dcoAssignGroupingValues, dcoAssignMasterDetailKeys, dcoSaveExpanding, dcoGroupsAlwaysExpanded]
              DataController.Summary.DefaultGroupSummaryItems = <>
              DataController.Summary.FooterSummaryItems = <>
              DataController.Summary.SummaryGroups = <>
              FilterRow.InfoText = #1050#1083#1080#1082#1085#1077#1090#1077' '#1090#1091#1082#1072' '#1079#1072' '#1076#1072' '#1076#1077#1092#1080#1085#1080#1088#1072#1090#1077' '#1092#1080#1083#1090#1077#1088
              FilterRow.Visible = True
              OptionsCustomize.ColumnsQuickCustomization = True
              OptionsData.Deleting = False
              OptionsData.Editing = False
              OptionsData.Inserting = False
              OptionsView.NoDataToDisplayInfoText = '<'#1053#1077#1084#1072' '#1087#1086#1076#1072#1090#1086#1094#1080'>'
              OptionsView.Footer = True
              OptionsView.GroupFooterMultiSummaries = True
              OptionsView.GroupFooters = gfVisibleWhenExpanded
              Styles.Preview = dm.cxStyle2
              object cxGridDBTableView1ART_VID: TcxGridDBColumn
                Caption = #1042#1080#1076
                DataBinding.FieldName = 'ART_VID'
                Visible = False
              end
              object cxGridDBTableView1ART_SIF: TcxGridDBColumn
                Caption = #1064#1080#1092#1088#1072
                DataBinding.FieldName = 'ART_SIF'
                Visible = False
              end
              object cxGridDBTableView1NAZIV_SUROVINA: TcxGridDBColumn
                Caption = #1057#1091#1088#1086#1074#1080#1085#1072
                DataBinding.FieldName = 'NAZIV_SUROVINA'
                Visible = False
                GroupIndex = 0
              end
              object cxGridDBTableView1LOT_NO: TcxGridDBColumn
                Caption = #1051#1086#1090' '#1073#1088#1086#1112
                DataBinding.FieldName = 'LOT_NO'
                Visible = False
              end
              object cxGridDBTableView1VID_PROIZVOD: TcxGridDBColumn
                Caption = #1042#1080#1076
                DataBinding.FieldName = 'VID_PROIZVOD'
                Width = 20
              end
              object cxGridDBTableView1PROIZVOD: TcxGridDBColumn
                Caption = #1064#1080#1092#1088#1072
                DataBinding.FieldName = 'PROIZVOD'
                Width = 48
              end
              object cxGridDBTableView1NAZIV_PROIZVOD: TcxGridDBColumn
                Caption = #1055#1088#1086#1080#1079#1074#1086#1076
                DataBinding.FieldName = 'NAZIV_PROIZVOD'
                Width = 239
              end
              object cxGridDBTableView1LOT_PROIZVOD: TcxGridDBColumn
                Caption = #1051#1086#1090
                DataBinding.FieldName = 'LOT_PROIZVOD'
                Width = 109
              end
              object cxGridDBTableView1KOLICINA_PROIZVOD: TcxGridDBColumn
                Caption = #1050#1086#1083#1080#1095#1080#1085#1072' '#1087#1088#1086#1080#1079#1074#1086#1076
                DataBinding.FieldName = 'KOLICINA_PROIZVOD'
                Width = 75
              end
              object cxGridDBTableView1MERKA_KOLICINA: TcxGridDBColumn
                Caption = #1052#1077#1088#1082#1072
                DataBinding.FieldName = 'MERKA_KOLICINA'
                Width = 45
              end
              object cxGridDBTableView1DATUM_RN: TcxGridDBColumn
                Caption = #1044#1072#1090#1091#1084' '#1056#1053
                DataBinding.FieldName = 'DATUM_RN'
                Width = 79
              end
              object cxGridDBTableView1KOLICINA_SUROVINA: TcxGridDBColumn
                Caption = #1050#1086#1083#1080#1095#1080#1085#1072' '#1089#1091#1088#1086#1074#1080#1085#1072
                DataBinding.FieldName = 'KOLICINA_SUROVINA'
                Width = 75
              end
              object cxGridDBTableView1MERKA: TcxGridDBColumn
                Caption = #1052#1077#1088#1082#1072
                DataBinding.FieldName = 'MERKA'
                Width = 58
              end
            end
            object cxGridLevel1: TcxGridLevel
              Caption = #1057#1091#1088#1086#1074#1080#1085#1072', '#1091#1087#1086#1090#1088#1077#1073#1077#1085#1072' '#1074#1086' '#1075#1086#1090#1086#1074#1080' '#1087#1088#1086#1080#1079#1074#1086#1076#1080', '#1089#1086' '#1051#1054#1058' '#1073#1088#1086#1077#1074#1080' '
              GridView = cxGridDBTableView1
            end
          end
        end
      end
    end
  end
  object cxGridPopupMenu1: TcxGridPopupMenu
    PopupMenus = <>
    Left = 560
    Top = 408
  end
  object PopupMenu1: TPopupMenu
    Left = 480
    Top = 488
    object N1: TMenuItem
      Caption = #1057#1085#1080#1084#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      ShortCut = 49235
      OnClick = SaveToIniFileExecute
    end
    object Excel1: TMenuItem
      Caption = #1057#1085#1080#1084#1080' '#1075#1086' '#1074#1086' Excel'
      ShortCut = 49221
    end
  end
  object dxBarManager1: TdxBarManager
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Segoe UI'
    Font.Style = []
    Categories.Strings = (
      'Default')
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    ImageOptions.Images = dmRes.cxSmallImages
    ImageOptions.LargeImages = dmRes.cxLargeImages
    PopupMenuLinks = <>
    UseSystemFont = True
    Left = 560
    Top = 464
    DockControlHeights = (
      0
      0
      0
      0)
    object dxBarManager1Bar2: TdxBar
      Caption = #1040#1082#1094#1080#1080
      CaptionButtons = <>
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 789
      FloatTop = 120
      FloatClientWidth = 51
      FloatClientHeight = 120
      ItemLinks = <
        item
          Visible = True
          ItemName = 'btUp'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarLargeButton10'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton5'
        end
        item
          Visible = True
          ItemName = 'dxBarSubItem1'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar3: TdxBar
      Caption = #1048#1079#1083#1077#1079
      CaptionButtons = <>
      DockedLeft = 309
      DockedTop = 0
      FloatLeft = 718
      FloatTop = 62
      FloatClientWidth = 51
      FloatClientHeight = 80
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton7'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton8'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar4: TdxBar
      Caption = #1055#1077#1095#1072#1090#1077#1114#1077
      CaptionButtons = <>
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 676
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton12'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton15'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton9'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarLargeButton13'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1BarIzgledGrid: TdxBar
      Caption = #1058#1072#1073#1077#1083#1072
      CaptionButtons = <>
      DockedLeft = 363
      DockedTop = 0
      FloatLeft = 808
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLBtnSnimiIzgled'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarLBtnBrisiIzgled'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarLargeButton2: TdxBarLargeButton
      Action = aAzuriraj
      Category = 0
    end
    object dxBarLargeButton3: TdxBarLargeButton
      Action = aBrisi
      Category = 0
    end
    object dxBarLargeButton4: TdxBarLargeButton
      Action = aSnimiIzgled
      Category = 0
      ScreenTip = tipSnimiIzgled
    end
    object dxBarLargeButton5: TdxBarLargeButton
      Action = aZacuvajExcel
      Category = 0
      ScreenTip = tipZacuvajExcel
    end
    object dxBarLargeButton6: TdxBarLargeButton
      Action = aPecatiTabela
      Caption = #1055#1077#1095#1072#1090#1080' '#1090#1072#1073#1077#1083#1072
      Category = 0
    end
    object dxBarLargeButton7: TdxBarLargeButton
      Action = aHelp
      Category = 0
    end
    object dxBarLargeButton8: TdxBarLargeButton
      Action = aIzlez
      Category = 0
    end
    object dxBarLargeButton1: TdxBarLargeButton
      Action = aNov
      Category = 0
    end
    object dxBarLargeButton9: TdxBarLargeButton
      Action = aSnimiPecatenje
      Category = 0
      ScreenTip = tipSnimiKonf
    end
    object dxBarLargeButton11: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object ribbonFilterReset: TdxBarLargeButton
      Caption = #1056#1077#1089#1077#1090#1080#1088#1072#1112' '#1092#1080#1083#1090#1077#1088
      Category = 0
      Hint = #1056#1077#1089#1077#1090#1080#1088#1072#1112' '#1092#1080#1083#1090#1077#1088
      Visible = ivAlways
    end
    object dxBarLargeButton12: TdxBarLargeButton
      Action = aPodesuvanjePecatenje
      Category = 0
      ScreenTip = tipDizajnReport
    end
    object dxBarLargeButton13: TdxBarLargeButton
      Action = aBrisiPodesuvanjePecatenje
      Category = 0
      ScreenTip = tipBrisiKonf
    end
    object dxBarLargeButton14: TdxBarLargeButton
      Caption = #1048#1079#1073#1088#1080#1096#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      Category = 0
      Visible = ivAlways
      LargeImageIndex = 40
    end
    object dxBarLargeButton15: TdxBarLargeButton
      Action = aPageSetup
      Category = 0
      ScreenTip = tipSetiranjeStrana
    end
    object dxBarLargeButton16: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object dxBarLargeButton17: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object cxBarEditItem1: TcxBarEditItem
      Caption = 'New Item'
      Category = 0
      Hint = 'New Item'
      Visible = ivAlways
      PropertiesClassName = 'TcxCheckBoxProperties'
    end
    object dxBarSubItem1: TdxBarSubItem
      Caption = #1055#1077#1095#1072#1090#1077#1114#1077
      Category = 0
      Visible = ivAlways
      ImageIndex = 30
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton6'
        end>
    end
    object dxBarLBtnSnimiIzgled: TdxBarLargeButton
      Action = aSnimiIzgled
      Category = 0
    end
    object dxBarLBtnBrisiIzgled: TdxBarLargeButton
      Action = aBrisiIzgled
      Category = 0
    end
    object dxBarLargeButton10: TdxBarLargeButton
      Action = aSpustiSoberi
      Category = 0
    end
    object btUp: TdxBarLargeButton
      Action = aUp
      Caption = #1055#1077#1095#1072#1090#1080
      Category = 0
      LargeImageIndex = 54
    end
    object btDown: TdxBarLargeButton
      Action = aUp
      Category = 0
    end
  end
  object ActionList1: TActionList
    Images = dmRes.cxSmallImages
    Left = 272
    Top = 480
    object aNov: TAction
      Caption = #1053#1086#1074
      ImageIndex = 10
      ShortCut = 116
      OnExecute = aNovExecute
    end
    object aAzuriraj: TAction
      Caption = #1040#1078#1091#1088#1080#1088#1072#1112
      ImageIndex = 12
      ShortCut = 117
      OnExecute = aAzurirajExecute
    end
    object aBrisi: TAction
      Caption = #1041#1088#1080#1096#1080
      ImageIndex = 11
      ShortCut = 119
      OnExecute = aBrisiExecute
    end
    object aSnimiIzgled: TAction
      Caption = #1057#1085#1080#1084#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      ImageIndex = 9
      ShortCut = 24659
      OnExecute = aSnimiIzgledExecute
    end
    object aZacuvajExcel: TAction
      Caption = #1047#1072#1095#1091#1074#1072#1112' '#1074#1086' Excel'
      ImageIndex = 9
      ShortCut = 24645
      OnExecute = aZacuvajExcelExecute
    end
    object aPecatiTabela: TAction
      Caption = #1055#1077#1095#1072#1090#1080
      ImageIndex = 30
      OnExecute = aPecatiTabelaExecute
    end
    object aHelp: TAction
      Caption = #1055#1086#1084#1086#1096
      ImageIndex = 24
    end
    object aIzlez: TAction
      Caption = #1048#1079#1083#1077#1079
      ImageIndex = 25
      OnExecute = aIzlezExecute
    end
    object aZapisi: TAction
      Caption = #1047#1072#1087#1080#1096#1080
      ImageIndex = 7
      ShortCut = 120
      OnExecute = aZapisiExecute
    end
    object aOtkazi: TAction
      Caption = #1054#1090#1082#1072#1078#1080
      ImageIndex = 21
      ShortCut = 27
      OnExecute = aOtkaziExecute
    end
    object aRefresh: TAction
      Caption = 'aRefresh'
      ImageIndex = 18
      ShortCut = 118
      OnExecute = aRefreshExecute
    end
    object aSnimiPecatenje: TAction
      Caption = #1057#1085#1080#1084#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      ImageIndex = 39
      OnExecute = aSnimiPecatenjeExecute
    end
    object aPodesuvanjePecatenje: TAction
      Caption = #1044#1080#1079#1072#1112#1085' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090
      ImageIndex = 38
      OnExecute = aPodesuvanjePecatenjeExecute
    end
    object aBrisiPodesuvanjePecatenje: TAction
      Caption = #1041#1088#1080#1096#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      ImageIndex = 37
      OnExecute = aBrisiPodesuvanjePecatenjeExecute
    end
    object aPageSetup: TAction
      Caption = #1057#1077#1090#1080#1088#1072#1114#1077' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072
      ImageIndex = 38
      OnExecute = aPageSetupExecute
    end
    object aFormConfig: TAction
      Caption = 'aFormConfig'
      ShortCut = 24699
      OnExecute = aFormConfigExecute
    end
    object aBrisiIzgled: TAction
      Caption = #1041#1088#1080#1096#1080' '#1075#1086' '#1079#1072#1095#1091#1074#1072#1085#1080#1086#1090' '#1080#1079#1075#1083#1077#1076
      ImageIndex = 56
      OnExecute = aBrisiIzgledExecute
    end
    object aSpustiSoberi: TAction
      Caption = #1057#1087#1091#1096#1090#1080'/'#1057#1086#1073#1077#1088#1080' '#1058#1072#1073#1077#1083#1072
      ImageIndex = 65
      OnExecute = aSpustiSoberiExecute
    end
    object aUp: TAction
      Caption = 'UpStream'
      ShortCut = 121
      OnExecute = aUpExecute
    end
    object aUpDizajner: TAction
      Caption = 'aUpDizajner'
      ShortCut = 24697
      OnExecute = aUpDizajnerExecute
    end
  end
  object dxComponentPrinter1: TdxComponentPrinter
    CurrentLink = dxComponentPrinter1Link1
    Version = 0
    Left = 184
    Top = 544
    object dxComponentPrinter1Link1: TdxGridReportLink
      Component = cxGrid1
      PrinterPage.DMPaper = 9
      PrinterPage.Footer = 6350
      PrinterPage.GrayShading = True
      PrinterPage.Header = 6350
      PrinterPage.Margins.Bottom = 12700
      PrinterPage.Margins.Left = 12700
      PrinterPage.Margins.Right = 12700
      PrinterPage.Margins.Top = 12700
      PrinterPage.Orientation = poLandscape
      PrinterPage.PageFooter.RightTitle.Strings = (
        #1057#1090#1088'.[Page #] '#1086#1076' [Total Pages]')
      PrinterPage.PageSize.X = 210000
      PrinterPage.PageSize.Y = 297000
      PrinterPage._dxMeasurementUnits_ = 0
      PrinterPage._dxLastMU_ = 2
      ReportDocument.CreationDate = 40336.425359629630000000
      OptionsFormatting.UseNativeStyles = True
      OptionsOnEveryPage.Caption = False
      OptionsOnEveryPage.FilterBar = False
      OptionsView.Caption = False
      OptionsView.FilterBar = False
      StyleRepository = dmRes.cxStyleRepositoryPrinting
      Styles.StyleSheet = dmRes.dxGridReportLinkStyleSheet
      BuiltInReportLink = True
    end
  end
  object dxBarScreenTipRepository1: TdxBarScreenTipRepository
    Left = 408
    Top = 552
    object tipSnimiIzgled: TdxBarScreenTip
      Header.Text = #1057#1085#1080#1084#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      Description.Text = #1047#1072#1095#1091#1074#1072#1112' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090' '#1085#1072' '#1092#1086#1088#1084#1072#1090#1072' '#1080' '#1085#1072' '#1090#1072#1073#1077#1083#1072#1090#1072
    end
    object tipZacuvajExcel: TdxBarScreenTip
      Header.Text = #1047#1072#1095#1091#1074#1072#1112' '#1074#1086' Excel'
      Description.Text = #1057#1085#1080#1084#1080' '#1075#1080' '#1087#1086#1076#1072#1090#1086#1094#1080#1090#1077' '#1086#1076' '#1090#1072#1073#1077#1083#1072#1090#1072' '#1074#1086' Excel '#1092#1086#1088#1084#1072#1090
    end
    object tipDizajnReport: TdxBarScreenTip
      Header.Text = #1044#1080#1079#1072#1112#1085' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090
      Description.Text = #1055#1086#1076#1077#1089#1091#1074#1072#1114#1072' '#1085#1072' '#1080#1079#1075#1083#1077#1076#1086#1090' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090' '#1085#1072' '#1090#1072#1073#1077#1083#1072#1090#1072
    end
    object tipSetiranjeStrana: TdxBarScreenTip
      Header.Text = #1057#1077#1090#1080#1088#1072#1114#1077' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072
      Description.Text = #1055#1086#1076#1077#1089#1091#1074#1072#1114#1077' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072' '#1085#1072' '#1082#1086#1112#1072' '#1116#1077' '#1089#1077' '#1087#1077#1095#1072#1090#1080' '#1090#1072#1073#1077#1083#1072#1090#1072
    end
    object tipSnimiKonf: TdxBarScreenTip
      Header.Text = #1057#1085#1080#1084#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      Description.Text = #1047#1072#1095#1091#1074#1072#1112' '#1075#1080' '#1087#1086#1076#1077#1089#1091#1074#1072#1114#1072#1090#1072' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090' '#1080' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072' '#1079#1072' '#1087#1077#1095#1072#1090#1077#1114#1077
    end
    object tipBrisiKonf: TdxBarScreenTip
      Header.Text = #1041#1088#1080#1096#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      Description.Text = #1048#1079#1073#1088#1080#1096#1080' '#1075#1080' '#1087#1086#1076#1077#1089#1091#1074#1072#1114#1072#1090#1072' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090' '#1080' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072' '#1079#1072' '#1087#1077#1095#1072#1090#1077#1114#1077
    end
  end
  object cxPropertiesStore1: TcxPropertiesStore
    Components = <>
    StorageName = 'cxPropertiesStore1'
    Left = 688
    Top = 32
  end
  object tblLotProizvod: TpFIBDataSet
    RefreshSQL.Strings = (
      'select'
      '      rns.vid_artikal vid_proizvod,'
      '      rns.artikal proizvod,'
      '      pr.naziv naziv_proizvod,'
      '      rns.lot lot_proizvod'
      ''
      'From'
      ' MAN_RABOTEN_NALOG rn'
      
        'Inner Join MAN_RABOTEN_NALOG_STAVKA RNS On RNS.ID_RABOTEN_NALOG ' +
        '= RN.ID'
      
        'inner join mtr_artikal pr on pr.artvid = rns.vid_artikal and pr.' +
        'id = rns.artikal'
      '--left Join MTR_OUT M On RN.ID = M.WO_REF_ID'
      '--left Join MTR_OUT_S MS On MS.MTR_OUT_ID = M.ID'
      
        '--left Join MTR_ARTIKAL A On A.ARTVID = MS.ART_VID And A.ID = MS' +
        '.ART_SIF'
      '--Where rns.lot = :lot'
      '--Group By 1, 2, 3,4,5,6,7,8,9,10,11,12, 13, 14')
    SelectSQL.Strings = (
      'select'
      '      rns.vid_artikal vid_proizvod,'
      '      rns.artikal proizvod,'
      '      pr.naziv naziv_proizvod,'
      '      rns.lot lot_proizvod'
      ''
      'From'
      ' MAN_RABOTEN_NALOG rn'
      
        'Inner Join MAN_RABOTEN_NALOG_STAVKA RNS On RNS.ID_RABOTEN_NALOG ' +
        '= RN.ID'
      
        'inner join mtr_artikal pr on pr.artvid = rns.vid_artikal and pr.' +
        'id = rns.artikal'
      '--left Join MTR_OUT M On RN.ID = M.WO_REF_ID'
      '--left Join MTR_OUT_S MS On MS.MTR_OUT_ID = M.ID'
      
        '--left Join MTR_ARTIKAL A On A.ARTVID = MS.ART_VID And A.ID = MS' +
        '.ART_SIF'
      '--Where rns.lot = :lot'
      '--Group By 1, 2, 3,4,5,6,7,8,9,10,11,12, 13, 14')
    AutoUpdateOptions.UpdateTableName = 'MAN_PROIZVODSTVENA_LINIJA'
    AutoUpdateOptions.KeyFields = 'ID'
    AutoUpdateOptions.GeneratorName = 'GEN_MAN_PROI_LINIJA_I'
    AutoUpdateOptions.WhenGetGenID = wgBeforePost
    AutoUpdateOptions.AutoParamsToFields = True
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 604
    Top = 118
    object tblLotProizvodLOT_PROIZVOD: TFIBStringField
      DisplayLabel = #1051#1086#1090
      FieldName = 'LOT_PROIZVOD'
      Size = 30
      EmptyStrToNull = True
    end
    object tblLotProizvodVID_PROIZVOD: TFIBIntegerField
      DisplayLabel = #1042#1080#1076
      FieldName = 'VID_PROIZVOD'
    end
    object tblLotProizvodPROIZVOD: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072
      FieldName = 'PROIZVOD'
    end
    object tblLotProizvodNAZIV_PROIZVOD: TFIBStringField
      DisplayLabel = #1055#1088#1086#1080#1079#1074#1086#1076
      FieldName = 'NAZIV_PROIZVOD'
      Size = 500
      EmptyStrToNull = True
    end
  end
  object dsLotProizvod: TDataSource
    DataSet = tblLotProizvod
    Left = 660
    Top = 117
  end
  object tblLotSurovina: TpFIBDataSet
    RefreshSQL.Strings = (
      'select   distinct'
      '      ms.art_vid vid_surovina,'
      '      ms.art_sif surovina,'
      '      a.naziv naziv_surovina,'
      '      ms.lot_no lot_surovina'
      'From MTR_OUT M'
      'inner Join MTR_OUT_S MS On MS.MTR_OUT_ID = M.ID'
      
        'inner join MTR_ARTIKAL A On A.ARTVID = MS.ART_VID And A.ID = MS.' +
        'ART_SIF'
      'Where ms.lot_no is not null')
    SelectSQL.Strings = (
      'select   distinct'
      '      ms.art_vid vid_surovina,'
      '      ms.art_sif surovina,'
      '      a.naziv naziv_surovina,'
      '      ms.lot_no lot_surovina'
      'From MTR_OUT M'
      'inner Join MTR_OUT_S MS On MS.MTR_OUT_ID = M.ID'
      
        'inner join MTR_ARTIKAL A On A.ARTVID = MS.ART_VID And A.ID = MS.' +
        'ART_SIF'
      'Where ms.lot_no is not null')
    AutoUpdateOptions.UpdateTableName = 'MAN_PROIZVODSTVENA_LINIJA'
    AutoUpdateOptions.KeyFields = 'ID'
    AutoUpdateOptions.GeneratorName = 'GEN_MAN_PROI_LINIJA_I'
    AutoUpdateOptions.WhenGetGenID = wgBeforePost
    AutoUpdateOptions.AutoParamsToFields = True
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 724
    Top = 118
    object tblLotSurovinaVID_SUROVINA: TFIBIntegerField
      FieldName = 'VID_SUROVINA'
    end
    object tblLotSurovinaSUROVINA: TFIBIntegerField
      FieldName = 'SUROVINA'
    end
    object tblLotSurovinaNAZIV_SUROVINA: TFIBStringField
      FieldName = 'NAZIV_SUROVINA'
      Size = 500
      EmptyStrToNull = True
    end
    object tblLotSurovinaLOT_SUROVINA: TFIBStringField
      FieldName = 'LOT_SUROVINA'
      Size = 30
      EmptyStrToNull = True
    end
  end
  object dsLotSurovina: TDataSource
    DataSet = tblLotSurovina
    Left = 780
    Top = 117
  end
  object dxComponentPrinter2: TdxComponentPrinter
    CurrentLink = dxComponentPrinter2Link1
    Version = 0
    Left = 184
    Top = 464
    object dxComponentPrinter2Link1: TdxGridReportLink
      Component = cxGrid2
      PrinterPage.DMPaper = 9
      PrinterPage.Footer = 6350
      PrinterPage.Header = 6350
      PrinterPage.Margins.Bottom = 12700
      PrinterPage.Margins.Left = 12700
      PrinterPage.Margins.Right = 12700
      PrinterPage.Margins.Top = 12700
      PrinterPage.PageSize.X = 210000
      PrinterPage.PageSize.Y = 297000
      PrinterPage._dxMeasurementUnits_ = 0
      PrinterPage._dxLastMU_ = 2
      BuiltInReportLink = True
    end
  end
end
