unit Homogena;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData, cxEdit,
  DB, cxDBData, cxGridCustomPopupMenu, cxGridPopupMenu,
  ComCtrls, cxContainer, cxTextEdit, cxDBEdit, StdCtrls, cxGridLevel,
  cxClasses, cxControls, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGrid, ExtCtrls, Menus, ActnList,
  cxGridExportLink, cxExport, cxLookAndFeelPainters, cxDataStorage,
  cxButtons, dxStatusBar, dxRibbonStatusBar, dxRibbon, dxBar, dxBarExtItems,
  dxPSGlbl, dxPSUtl, dxPSEngn, dxPrnPg, dxBkgnd, dxWrap, dxPrnDev,
  dxPSCompsProvider, dxPSFillPatterns, dxPSEdgePatterns, dxPSCore, dxPScxCommon,
  cxPC, cxMemo, cxLocalization, cxLookAndFeels,
  dxPSPDFExportCore, dxPSPDFExport, cxDrawTextUtils, dxPSPrVwStd, dxPSPrVwAdv,
  dxPSPrVwRibbon, dxPScxEditorProducers, dxPScxExtEditorProducers,
  dxPScxPageControlProducer, dxPgsDlg, cxCheckBox, cxBarEditItem, dxSkinsCore,
  dxSkinBlack, dxSkinBlue, dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom,
  dxSkinDarkSide, dxSkinFoggy, dxSkinGlassOceans, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven,
  dxSkinSharp, dxSkinSilver, dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008,
  dxSkinsDefaultPainters, dxSkinValentine, dxSkinXmas2008Blue,
  dxSkinsdxRibbonPainter, dxSkinsdxBarPainter, dxSkinscxPCPainter,
  dxBarSkinnedCustForm, cxImageComboBox, cxGroupBox, cxRadioGroup,
  dxRibbonSkins, cxPCdxBarPopupMenu, dxPScxGridLnk, dxPScxGridLayoutViewLnk,
  dxScreenTip, dxSkinBlueprint, dxSkinDevExpressDarkStyle,
  dxSkinDevExpressStyle, dxSkinHighContrast, dxSkinSevenClassic,
  dxSkinSharpPlus, dxSkinTheAsphaltWorld, dxSkinVS2010, dxSkinWhiteprint,
  FIBDataSet, pFIBDataSet, cxDropDownEdit, cxCalendar, cxMaskEdit, cxLookupEdit,
  cxDBLookupEdit, cxDBLookupComboBox, cxLabel, cxDBExtLookupComboBox,dmResources,
  cxDBLabel,dateutils, cxImage, dxSkinOffice2013White, cxNavigator,
  System.Actions, dxSkinMetropolis, dxSkinMetropolisDark,
  dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray, dxBarBuiltInMenu,
  dxSkinOffice2016Colorful, dxSkinOffice2016Dark, dxSkinVisualStudio2013Blue,
  dxSkinVisualStudio2013Dark, dxSkinVisualStudio2013Light;

type
//  niza = Array[1..5] of Variant;

  TfrmHomogena = class(TForm)
    cxGridPopupMenu1: TcxGridPopupMenu;
    PopupMenu1: TPopupMenu;
    N1: TMenuItem;
    Excel1: TMenuItem;
    dxBarManager1: TdxBarManager;
    dxBarManager1Bar1: TdxBar;
    dxBarManager1Bar2: TdxBar;
    dxBarManager1Bar3: TdxBar;
    dxBarLargeButton2: TdxBarLargeButton;
    dxBarLargeButton3: TdxBarLargeButton;
    dxBarLargeButton4: TdxBarLargeButton;
    dxBarLargeButton5: TdxBarLargeButton;
    dxBarLargeButton6: TdxBarLargeButton;
    dxBarLargeButton7: TdxBarLargeButton;
    dxBarLargeButton8: TdxBarLargeButton;
    dxBarLargeButton1: TdxBarLargeButton;
    ActionList1: TActionList;
    aNov: TAction;
    aAzuriraj: TAction;
    aBrisi: TAction;
    aSnimiIzgled: TAction;
    aZacuvajExcel: TAction;
    aPecatiTabela: TAction;
    aHelp: TAction;
    aIzlez: TAction;
    aZapisi: TAction;
    aOtkazi: TAction;
    aRefresh: TAction;
    dxBarManager1Bar4: TdxBar;
    dxBarLargeButton9: TdxBarLargeButton;
    aSnimiPecatenje: TAction;
    dxBarLargeButton11: TdxBarLargeButton;
    ribbonFilterReset: TdxBarLargeButton;
    aPodesuvanjePecatenje: TAction;
    dxBarLargeButton12: TdxBarLargeButton;
    aBrisiPodesuvanjePecatenje: TAction;
    dxBarLargeButton13: TdxBarLargeButton;
    dxBarLargeButton14: TdxBarLargeButton;
    aPageSetup: TAction;
    dxBarLargeButton15: TdxBarLargeButton;
    dxBarLargeButton16: TdxBarLargeButton;
    dxBarLargeButton17: TdxBarLargeButton;
    cxBarEditItem1: TcxBarEditItem;
    dxBarSubItem1: TdxBarSubItem;
    aFormConfig: TAction;
    dxBarScreenTipRepository1: TdxBarScreenTipRepository;
    tipSnimiIzgled: TdxBarScreenTip;
    tipZacuvajExcel: TdxBarScreenTip;
    tipDizajnReport: TdxBarScreenTip;
    tipSetiranjeStrana: TdxBarScreenTip;
    tipSnimiKonf: TdxBarScreenTip;
    tipBrisiKonf: TdxBarScreenTip;
    dPanel: TPanel;
    lPanel: TPanel;
    cxPageControl1: TcxPageControl;
    Osnovno: TcxTabSheet;
    Napredno: TcxTabSheet;
    Label1: TLabel;
    Sifra: TcxDBTextEdit;
    OtkaziButton: TcxButton;
    ZapisiButton: TcxButton;
    dPanel2: TPanel;
    dxComponentPrinter1: TdxComponentPrinter;
    dxComponentPrinter1Link1: TdxGridReportLink;
    dxBarManager1BarSnimiIzgled: TdxBar;
    dxBarLBtnSnimiIzgled: TdxBarLargeButton;
    dxBarLBtnBrisiIzgled: TdxBarLargeButton;
    aBrisiIzgled: TAction;
    cxLabel8: TcxLabel;
    cxLabel7: TcxLabel;
    rgVidEtiketa: TcxDBRadioGroup;
    cxGroupBox1: TcxGroupBox;
    Label3: TLabel;
    txtGTIN: TcxDBTextEdit;
    txtSSCC: TcxDBTextEdit;
    txtBarkod: TcxDBTextEdit;
    Label4: TLabel;
    Label5: TLabel;
    cbRN: TcxDBLookupComboBox;
    txtDatum: TcxDBDateEdit;
    cxLabel1: TcxLabel;
    txtVidArtikal: TcxDBTextEdit;
    txtArtikal: TcxDBTextEdit;
    cxDBTextEdit5: TcxDBTextEdit;
    cbNazivArtikal: TcxLookupComboBox;
    cxLabel39: TcxLabel;
    lblMerkaP: TcxDBLabel;
    aPecatiEtiketa: TAction;
    txtLot: TcxDBTextEdit;
    cxLabel2: TcxLabel;
    aDizajner: TAction;
    dxBarLargeButton10: TdxBarLargeButton;
    cxLabel43: TcxLabel;
    txtSerija: TcxDBTextEdit;
    aBarkod: TAction;
    btBarkod: TcxButton;
    cxLabel5: TcxLabel;
    lblMerka: TcxDBLabel;
    cxLabel10: TcxLabel;
    txtKolicinaBroj: TcxDBTextEdit;
    txtKolicinaEdMerka: TcxDBTextEdit;
    cxLabel3: TcxLabel;
    cxGridPopupMenu2: TcxGridPopupMenu;
    cxGrid2: TcxGrid;
    cxGrid2DBTableView1: TcxGridDBTableView;
    cxGrid2DBTableView1VID_ARTIKAL: TcxGridDBColumn;
    cxGrid2DBTableView1ARTIKAL: TcxGridDBColumn;
    cxGrid2DBTableView1NAZIV_ARTIKAL: TcxGridDBColumn;
    cxGrid2DBTableView1GTIN: TcxGridDBColumn;
    cxGrid2DBTableView1SSCC: TcxGridDBColumn;
    cxGrid2DBTableView1SERISKI_BROJ: TcxGridDBColumn;
    cxGrid2DBTableView1BARKOD: TcxGridDBColumn;
    cxGrid2DBTableView1LOT: TcxGridDBColumn;
    cxGrid2DBTableView1KOLICINA: TcxGridDBColumn;
    cxGrid2DBTableView1KOLICINA_VO_PAKUVANJE: TcxGridDBColumn;
    cxGrid2DBTableView1ID_PAKUVANJE: TcxGridDBColumn;
    cxGrid2DBTableView1DATUM_PAK: TcxGridDBColumn;
    cxGrid2DBTableView1KOLICINA_BROJ: TcxGridDBColumn;
    cxGrid2Level1: TcxGridLevel;
    aAvtomatski: TAction;
    aPogled: TAction;
    Timer1: TTimer;
    Panel4: TPanel;
    Label2: TLabel;
    txtSSCC_e: TcxDBTextEdit;
    aPresmetaj: TAction;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1DBTableView1DATUM: TcxGridDBColumn;
    cxGrid1DBTableView1TIP_ETIKETA: TcxGridDBColumn;
    cxGrid1DBTableView1ID: TcxGridDBColumn;
    cxGrid1DBTableView1ID_RABOTEN_NALOG: TcxGridDBColumn;
    cxGrid1DBTableView1BR_RN: TcxGridDBColumn;
    cxGrid1DBTableView1DATUM_RN: TcxGridDBColumn;
    cxGrid1DBTableView1SSCC: TcxGridDBColumn;
    cxGrid1DBTableView1GODINA: TcxGridDBColumn;
    cxGrid1DBTableView2: TcxGridDBTableView;
    cxGrid1DBTableView2NAZIV_ARTIKAL: TcxGridDBColumn;
    cxGrid1DBTableView2GTIN: TcxGridDBColumn;
    cxGrid1DBTableView2SSCC: TcxGridDBColumn;
    cxGrid1DBTableView2SERISKI_BROJ: TcxGridDBColumn;
    cxGrid1DBTableView2BARKOD: TcxGridDBColumn;
    cxGrid1DBTableView2LOT: TcxGridDBColumn;
    cxGrid1DBTableView2KOLICINA: TcxGridDBColumn;
    cxGrid1DBTableView2KOLICINA_VO_PAKUVANJE: TcxGridDBColumn;
    cxGrid1DBTableView2KOLICINA_BROJ: TcxGridDBColumn;
    cxGrid1DBTableView2DATUM_PAK: TcxGridDBColumn;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1Level2: TcxGridLevel;
    aIzberi: TAction;
    btIzberiProiz: TcxButton;
    aAvtomatskiK: TAction;
    aNovK: TAction;
    aZapisiK: TAction;
    aPresmetajK: TAction;
    aBarkodK: TAction;
    Panel1: TPanel;
    Zapisi2: TcxButton;
    Otkazi2: TcxButton;
    lblDelA: TcxLabel;
    txtSifraSD: TcxDBTextEdit;
    cbSosDel: TcxDBLookupComboBox;
    lblDelC: TcxLabel;
    txtImenitel: TcxDBTextEdit;
    txtPakuvanje: TcxDBTextEdit;
    aPresmetajSD: TAction;
    btIzberiSD: TcxButton;
    aIzberiSD: TAction;

    procedure cxDBTextEditAllEnter(Sender: TObject);
    procedure cxDBTextEditAllExit(Sender: TObject);
    procedure EnterKakoTab(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    //procedure PrvPosledenTab(panel:TPanel;var posledna,prva:TWinControl);
    procedure SaveToIniFileExecute(Sender: TObject);
    procedure cxGrid1DBTableView1KeyPress(Sender: TObject; var Key: Char);
    procedure aZapisiExecute(Sender: TObject);
    procedure aOtkaziExecute(Sender: TObject);
    procedure aNovExecute(Sender: TObject);
    procedure aAzurirajExecute(Sender: TObject);
    procedure aBrisiExecute(Sender: TObject);
    procedure aZacuvajExcelExecute(Sender: TObject);
    procedure aRefreshExecute(Sender: TObject);
    procedure aSnimiIzgledExecute(Sender: TObject);
    procedure aIzlezExecute(Sender: TObject);
    procedure aPecatiTabelaExecute(Sender: TObject);
    procedure aSnimiPecatenjeExecute(Sender: TObject);
    procedure zacuvajPrintVoIni(ime:AnsiString);
    procedure procitajPrintOdIni(ime:AnsiString);
    procedure aPodesuvanjePecatenjeExecute(Sender: TObject);
    procedure aBrisiPodesuvanjePecatenjeExecute(Sender: TObject);
    procedure aPageSetupExecute(Sender: TObject);
    procedure aFormConfigExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure cxGrid1DBTableView1DblClick(Sender: TObject);
    procedure cxGrid1DBTableView1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure cxPageControl1Change(Sender: TObject);
    procedure aBrisiIzgledExecute(Sender: TObject);
    procedure SetirajLukap(sender:TObject; tabela:TDataSet; tip:TcxDBTextEdit; sifra:TcxDBTextEdit; lukap:TcxLookupComboBox);
    procedure dPanel2Click(Sender: TObject);
    procedure cxGrid1DBTableView1FocusedRecordChanged(
      Sender: TcxCustomGridTableView; APrevFocusedRecord,
      AFocusedRecord: TcxCustomGridRecord;
      ANewItemRecordFocusingChanged: Boolean);
    procedure aPecatiEtiketaExecute(Sender: TObject);
    procedure aDizajnerExecute(Sender: TObject);
    procedure aBarkodExecute(Sender: TObject);
    procedure Barcode1D_Code1281Change(Sender: TObject);
    procedure Image1Click(Sender: TObject);
    procedure cxGrid2DBTableView1DblClick(Sender: TObject);
    procedure cxGrid2DBTableView1EditKeyDown(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Word;
      Shift: TShiftState);
    procedure cxGrid1DBTableView1DataControllerDetailCollapsed(
      ADataController: TcxCustomDataController; ARecordIndex: Integer);
    procedure cxGrid1DBTableView1DataControllerDetailExpanded(
      ADataController: TcxCustomDataController; ARecordIndex: Integer);
    procedure cxGrid1DBTableView1DataControllerDetailExpanding(
      ADataController: TcxCustomDataController; ARecordIndex: Integer;
      var AAllow: Boolean);
    procedure cxGrid1DBTableView1MouseEnter(Sender: TObject);
    procedure cxGrid2DBTableView1MouseEnter(Sender: TObject);
    procedure FormKeyDown(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure aAvtomatskiExecute(Sender: TObject);
    procedure aPogledExecute(Sender: TObject);
    procedure Timer1Timer(Sender: TObject);
    procedure aPresmetajExecute(Sender: TObject);
    procedure aIzberiExecute(Sender: TObject);
    procedure aAvtomatskiKExecute(Sender: TObject);
    procedure aNovKExecute(Sender: TObject);
    procedure aZapisiKExecute(Sender: TObject);
    procedure aPresmetajKExecute(Sender: TObject);
    procedure aBarkodKExecute(Sender: TObject);
    procedure aPresmetajSDExecute(Sender: TObject);
    procedure aIzberiSDExecute(Sender: TObject);

  private
    { Private declarations }

    _sifra_kluc :array[1..10] of string;  // �������� ���������� �� ������ �� ������� �� ������������� ���

  protected
    list : TList;
    inserting : boolean;
    prva, posledna :TWinControl;
    procedure prefrli;

  public
    { Public declarations }
    constructor Create(Owner : TComponent; insert : boolean = true);reintroduce; overload;

    procedure SetSifra(br: integer; s : string);
    function GetSifra(br: integer) : string;

    //������� �� ���������� ������������ _sifra_kluc
    property sifra_kluc[br :integer]: string read GetSifra write SetSifra;
    var id_rn_stavka, rn : LongInt;
  end;

  type
  WriteThread = class(TThread)
  private
     i:byte;
    protected
    procedure Execute; override;
    procedure DoSomething;
  end;


var
  frmHomogena: TfrmHomogena;
  rData : TRepositoryData;
  na_koj_grid,pojavi:byte;
  writememo:WriteThread;

  //rn:LongInt;
  var rn:LongInt;
      ima : Boolean;
       tl:integer;
implementation



uses DaNe, dmKonekcija, Utils, FormConfig,  dmMaticni, dmUnitBK, dmUnit,
  IzberiProizovod, IzberiSD;

{$R *.dfm}
//------------------------------------------------------------------------------

constructor TfrmHomogena.Create(Owner : TComponent; insert : boolean = true);
begin
inherited Create(Owner);
//	�������� ���� ������� � �������� �� ����������� ���������
//	insert = true -> ����� ���� ����� ���������� �� ������� �� Insert Mode
//  insert = false -> ����� ���� ����� ������� �� ����� �� ������. �� ������� ����� ����� �� �� �������� F5
  inserting := insert;
  ima := False;
end;

//	����� �� �����������. �� ��������� ������� �� ������, �� �������� ������� �� ���� ��������,
//	�� ������� ������� �� ������ ������ � �� ������� �� Insert Mode
procedure TfrmHomogena.aNovExecute(Sender: TObject);
var broj:string;  i : integer;
begin
 // if(cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) then
 // begin
   if cxPageControl1.ActivePageIndex = 0 then
    begin
      dPanel.Enabled:=True;
      lPanel.Enabled:=False;
   //   prva.SetFocus;
      cxGrid1DBTableView1.DataController.DataSet.Insert;

      dmBK.MaxEtiketa.Close;
      dmBK.MaxEtiketa.ExecQuery;

      for I := 1 to (7-(Length(inttostr(dmBK.MaxEtiketa.FldByName['maks'].AsInteger+1)))) do
      begin
       broj := broj+'0';
      end;
       broj := kompaniski_broj + broj + inttostr(dmBK.MaxEtiketa.FldByName['maks'].AsInteger+1);
       dmBK.Kontrolen_broj(broj);
       dmBK.tblEtiketaSSCC.Value := '(00)' + broj + inttostr(kb);

   end
  else
    begin
       dPanel2.Enabled:=True;
        lPanel.Enabled := false;
       if dmBK.tblIzberiArtikal.RecordCount = 1 then
       begin
         dmBK.tblEtiketaStavka.Insert;
         dmBK.tblEtiketaStavkaVID_ARTIKAL.Value := dmBK.tblIzberiArtikalARTVID.Value;
         dmBK.tblEtiketaStavkaARTIKAL.Value := dmBK.tblIzberiArtikalID.Value;

         SetirajLukap(sender,dmBK.tblIzberiArtikal,txtVidArtikal,txtArtikal, cbNazivArtikal);
         //btBarkod.SetFocus;
       end
       else
       begin
          cbNazivArtikal.Clear;
          //txtVidArtikal.SetFocus;
          dmBK.tblEtiketaStavka.Insert;
       end;
  // end
  end

end;

procedure TfrmHomogena.aNovKExecute(Sender: TObject);
var broj:string;  i : integer;
begin
 // if(cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) then
 // begin
   if cxPageControl1.ActivePageIndex = 0 then
    begin
      dPanel.Enabled:=True;
      lPanel.Enabled:=False;
   //   prva.SetFocus;
      cxGrid1DBTableView1.DataController.DataSet.Insert;

      dmBK.MaxEtiketa.Close;
      dmBK.MaxEtiketa.ExecQuery;

      for I := 1 to (7-(Length(inttostr(dmBK.MaxEtiketa.FldByName['maks'].AsInteger+1)))) do
      begin
       broj := broj+'0';
      end;
       broj := kompaniski_broj + broj + inttostr(dmBK.MaxEtiketa.FldByName['maks'].AsInteger+1);
       dmBK.Kontrolen_broj(broj);
       dmBK.tblEtiketaSSCC.Value := '(00)' + broj + inttostr(kb);

   end
  else
    begin
       dPanel2.Enabled:=True;
        lPanel.Enabled := false;
       if dmBK.tblIzberiA.RecordCount = 1 then
       begin
         dmBK.tblEtiketaStavka.Insert;
         dmBK.tblEtiketaStavkaVID_ARTIKAL.Value := dmBK.tblIzberiAARTVID.Value;
         dmBK.tblEtiketaStavkaARTIKAL.Value := dmBK.tblIzberiAID.Value;

         SetirajLukap(sender,dmBK.tblIzberiA,txtVidArtikal,txtArtikal, cbNazivArtikal);
         //btBarkod.SetFocus;
       end
       else
       begin
          cbNazivArtikal.Clear;
          //txtVidArtikal.SetFocus;
          dmBK.tblEtiketaStavka.Insert;
       end;
  // end
  end
end;

//	����� �� ���������. �� ��������� ������� �� ������, �� �������� ������� �� ���� ��������,
//	�� ������� ������� �� ������ ������ � �� ������� �� Edit Mode
procedure TfrmHomogena.aAvtomatskiExecute(Sender: TObject);
begin
//if pojavi = 1 then
//begin
//   writememo.Terminate;
 //  dmRes.frxReport1.shpr Destroy;
  // pojavi := 0;
//end;
   //if tag = 1 then close;
cxPageControl1.ActivePageIndex := 0;
dmBK.tblIzberiRN.Close;
dmBK.tblIzberiRN.ParamByName('rn').Value := rn;
dmBK.tblIzberiRN.Open;
dmBK.tblIzberiArtikal.Close;
//dmBK.tblIzberiArtikal.ParamByName('vid_artikal').Value := dm.tblRNVlezVID_ARTIKAL.Value;
dmBK.tblIzberiArtikal.ParamByName('id_rns').Value := id_rn_stavka;
dmBK.tblIzberiArtikal.Open;

dm.tblSosDel.Close;
dm.tblSosDel.ParamByName('lot').AsString := dmBK.tblIzberiArtikalLOT.Value;
dm.tblSosDel.Open;


if not dmBK.tblIzberiRN.IsEmpty then
 begin
   if dmBK.tblIzberiRN.RecordCount = 1 then
     begin
         aNov.Execute;
         dmBK.tblEtiketaID_RABOTEN_NALOG.Value := rn;
         aZapisi.Execute;
         dmBK.tblEtiketaStavkaVID_ARTIKAL.Value := dmBK.tblIzberiArtikalARTVID.Value;
         dmBK.tblEtiketaStavkaARTIKAL.Value := dmBK.tblIzberiArtikalID.Value;
         aBarkod.Execute;

        if Tag = 0 then  aZapisi.Execute
        else
        begin

        end;
       //  aPecatiEtiketa.Execute;
       //  aPogled.Execute;


     end
     else
     begin
         ShowMessage('��� ���ō� ������� �����!');
         Abort;
     end;
 end
 else
 begin
   ShowMessage('���� ������� �����!');
   Abort;
 end;

//     dmBK.tblRNStavka.Edit;
//     dmBK.tblRNStavkaLINIJA.Value := 2;
//     dmBK.tblRNStavka.Post;
//end;
end;

procedure TfrmHomogena.aAvtomatskiKExecute(Sender: TObject);
begin
//if pojavi = 1 then
//begin
//   writememo.Terminate;
 //  dmRes.frxReport1.shpr Destroy;
  // pojavi := 0;
//end;
   //if tag = 1 then close;
cxPageControl1.ActivePageIndex := 0;

//dmBK.tblIzberiKamion.Close;
//dmBK.tblIzberiKamion.ParamByName('t').Value := tl;
//dmBK.tblIzberiKamion.ParamByName('status').AsString := '0';
//dmBK.tblIzberiKamion.Open;
//
//dmBK.tblIzberiA.Close;
//dmBK.tblIzberiA.Open;

//if not dmBK.tblIzberiKamion.IsEmpty then
// begin
 //  if dmBK.tblIzberiKamion.RecordCount = 1 then
   //  begin

         aNovK.Execute;
         dmBK.tblEtiketaID_RABOTEN_NALOG.Value := rn;

         aZapisiK.Execute;
         dmBK.tblEtiketaStavkaVID_ARTIKAL.Value := dmBK.tblIzberiAARTVID.Value;
         dmBK.tblEtiketaStavkaARTIKAL.Value := dmBK.tblIzberiAID.Value;
        // dmbk.tblEtiketaStavkaID_POR_S_T.Value := dmBK.tblIzberiAid
         aBarkodK.Execute;

        if Tag = 0 then  aZapisiK.Execute
        else
        begin

        end;
       //  aPecatiEtiketa.Execute;
       //  aPogled.Execute;


//     end
   //  else
   //  begin
   //      ShowMessage('��� ���ō� ������� �����!');
   //      Abort;
   //  end;
// end
// else
// begin
//   ShowMessage('���� ������� �����!');
//   Abort;
// end;

//     dmBK.tblRNStavka.Edit;
//     dmBK.tblRNStavkaLINIJA.Value := 2;
//     dmBK.tblRNStavka.Post;
//end;

end;

procedure TfrmHomogena.aAzurirajExecute(Sender: TObject);
begin
  if(cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) then
  begin
  if (cxPageControl1.ActivePageIndex = 0) and (cxGrid1.IsFocused) then
    begin
    dPanel.Enabled:=True;
   // dPanel2.Enabled:=True;
    lPanel.Enabled:=False;
   // prva.SetFocus;
    cxGrid1DBTableView1.DataController.DataSet.Edit;
  end
  else
  if ((cxPageControl1.ActivePageIndex = 0) and (cxGrid2.IsFocused))
     or (cxPageControl1.ActivePageIndex = 1)
   then
  begin
    cxPageControl1.ActivePageIndex := 1;
    dPanel2.Enabled:=True;
    lPanel.Enabled:=False;
   // txtVidArtikal.SetFocus;
    dmBK.tblEtiketaStavka.Edit;
  end;
  end
  else ShowMessage('������ �������� �� ��� �������� �� �� ��������� �������/r/n � ����� ������� �� ������� ��� �����!');
end;

//	����� �� ������ �� ������������� �����
procedure TfrmHomogena.aBrisiExecute(Sender: TObject);
begin
if cxPageControl1.ActivePage = Osnovno then
  begin
  if (cxGrid1DBTableView1.DataController.DataSet.RecordCount <> 0) and
    (cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) then
     cxGrid1DBTableView1.DataController.DataSet.Delete();
  end
  else
  begin
  if(dmBK.tblEtiketaStavka.State = dsBrowse) and (not dmBK.tblEtiketaStavka.IsEmpty) then
  begin
     dmBK.tblEtiketaStavka.Delete();
   //  dmBK.tblIzberiArtikal.Close;
     cbNazivArtikal.Clear;
    // cbAmbalaza.Clear;
     lblMerkaP.Clear;
  end;
  end;
end;

procedure TfrmHomogena.aBrisiIzgledExecute(Sender: TObject);
begin
  brisiGridVoBaza(Name,cxGrid1DBTableView1);
  BrisiFormaIzgled(self);
end;

//	����� �� ���������� �� ����������
procedure TfrmHomogena.aRefreshExecute(Sender: TObject);
begin
  cxGrid1DBTableView1.DataController.Filter.Root.Clear; //���������� �� ��������
  cxGrid1DBTableView1.DataController.DataSet.Refresh;
end;

//	����� �� ����� �� ������� �� ������� �� ������� �����
procedure TfrmHomogena.aIzberiExecute(Sender: TObject);
begin
  frmIzberiProizvod := TfrmIzberiProizvod.Create(Self);
  frmIzberiProizvod.Caption := '�������� ��������/�, ��� �� ����� �� ���� ����� �� '+cbNazivArtikal.Text;
  frmIzberiProizvod.ShowModal;
  if frmIzberiProizvod.Tag in [1, 2] then
  begin
   ima := True;
  end;

  frmIzberiProizvod.Free;
  if ima then aZapisi.Execute;

end;

procedure TfrmHomogena.aIzberiSDExecute(Sender: TObject);
begin
   frmIzberiSD := TfrmIzberiSD.Create(self);
   frmIzberiSD.tblIzberiSD.Open;
   frmIzberiSD.lot := dmBK.tblIzberiArtikalLOT.Value; //tblEtiketaStavkaLOT.AsString;
   frmIzberiSD.ShowModal;

   dm.tblSosDel.Close;
   dm.tblSosDel.ParamByName('lot').AsString := dmBK.tblIzberiArtikalLOT.Value;
   dm.tblSosDel.Open;

   frmIzberiSD.Free;
end;

procedure TfrmHomogena.aIzlezExecute(Sender: TObject);
begin
  Close;
end;

//	����� �� ������� �� �������� �� ������ �� ���� (Utils.pas)
procedure TfrmHomogena.aSnimiIzgledExecute(Sender: TObject);
begin
  zacuvajGridVoBaza(Name,cxGrid1DBTableView1);
  ZacuvajFormaIzgled(self);
end;

//	����� �� ������� �� ������ �� Excel ������ (Utils.pas)
procedure TfrmHomogena.aZacuvajExcelExecute(Sender: TObject);
begin
  zacuvajVoExcel(cxGrid1, Caption);
end;

//  ��������� �� ������ �� ������� �� ���������� �� ����� �� Enter
procedure TfrmHomogena.EnterKakoTab(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var  kom : TWinControl;
begin
   kom := Sender as TWinControl;
    case Key of
        VK_DOWN:
        begin
          PostMessage(Handle,WM_NextDlgCtl,0,0);
          key:=0;
        end;
        VK_UP:
        begin
          PostMessage(Handle,WM_NextDlgCtl,1,0);
          key:=0;
        end;
        VK_RETURN:
        begin
          PostMessage(Handle,WM_NextDlgCtl,0,0);
        end;
         	VK_INSERT:
        begin
          if (kom = cbNazivArtikal) or (kom = txtVidArtikal) or (kom = txtArtikal) then
          begin
//            frmNurkoRepository := TfrmNurkoRepository.Create(nil,Name,rData);
//            frmNurkoRepository.kontrola_naziv := kom.Name;
//            frmNurkoRepository.ShowModal;
//
//            if (frmNurkoRepository.ModalResult = mrOk) then
//              cbNazivArtikal.EditValue := frmNurkoRepository.cxGrid1Level1.GridView.DataController.GetRecordId(frmNurkoRepository.cxGrid1Level1.GridView.DataController.FocusedRecordIndex);
//
//            frmNurkoRepository.Free;
       	  end
//          else
//          if (kom = cbSurovina) or (kom = txtVidSurovina) or (kom = txtSurovina) then
//          begin
//            frmNurkoRepository := TfrmNurkoRepository.Create(nil,Name,rData);
//            frmNurkoRepository.kontrola_naziv := kom.Name;
//            frmNurkoRepository.ShowModal;
//
//            if (frmNurkoRepository.ModalResult = mrOk) then
//              cbSurovina.EditValue := frmNurkoRepository.cxGrid1Level1.GridView.DataController.GetRecordId(frmNurkoRepository.cxGrid1Level1.GridView.DataController.FocusedRecordIndex);
//
//            frmNurkoRepository.Free;
//       	  end
    end;
    end;
end;

//	������� �� ��� �� ������ ��� �������� �� �����
procedure TfrmHomogena.cxDBTextEditAllEnter(Sender: TObject);
begin
    TEdit(Sender).Color:=clSkyBlue;
end;

//	������� �� ��� �� ������ ��� ����� �� �����
procedure TfrmHomogena.cxDBTextEditAllExit(Sender: TObject);
var broj:string;  i : integer;
begin
    TEdit(Sender).Color:=clWhite;

   if (dmBK.tblEtiketaStavka.State) in [dsInsert, dsEdit] then
     begin
     if ((Sender as TWinControl)= cbNazivArtikal) then
        begin
            if (cbNazivArtikal.Text <>'') then
            begin
              txtVidArtikal.Text := dmBK.tblIzberiArtikalARTVID.AsString;//cbNazivArtikal.EditValue[0];
	            txtArtikal.Text := dmBK.tblIzberiArtikalID.AsString;

              //dmBK.tblNormativO.Close;
              //dmBK.tblNormativO.ParamByName('mas_id').va
            end
            else
            begin
              dmBK.tblEtiketaStavkaVID_ARTIKAL.Clear;
              dmBK.tblEtiketaStavkaARTIKAL.Clear;
            end;
         end;
         if ((Sender as TWinControl)= txtArtikal) or ((Sender as TWinControl)= txtVidArtikal) then
         begin
             SetirajLukap(sender,dmBK.tblIzberiArtikal,txtVidArtikal,txtArtikal, cbNazivArtikal);
//            if dmBK.tblIzberiArtikal.Locate('VID_ARTIKAL;ARTIKAL', VarArrayOf([txtVidArtikal.Text,txtArtikal.Text]),[]) then
//            begin
//               txtGTIN.Text := dmBK.tblIzberiArtikalBARKOD.AsString;
//               txtSSCC.Text := '(00)'
//            end;
          end;
         if ((Sender as TWinControl)= txtArtikal) or ((Sender as TWinControl)= txtVidArtikal) or
            ((Sender as TWinControl)= cbNazivArtikal) then
            begin
//                if ((txtArtikal.Text <> '') or (txtVidArtikal.Text <> '')) then
//                begin
//                   //dmBK.tblEtiketaStavkaBARKOD.Value := dmBK.tblIzberiArtikalBARKOD.AsString;
//                   dmBK.MaxEtiketaStavka.Close;
//                   dmBK.MaxEtiketaStavka.ExecQuery;
//                   for I := 1 to (7-(Length(inttostr(dmBK.MaxEtiketaStavka.FldByName['maks'].AsInteger+1)))) do
//                   begin
//                    broj := broj+'0';
//                   end;
//                    broj := kompaniski_broj + broj + inttostr(dmBK.MaxEtiketaStavka.FldByName['maks'].AsInteger+1);
//                    dmBK.Kontrolen_broj(broj);
//                    dmBK.tblEtiketaStavkaSSCC.Value := '(00)' + broj + inttostr(kb);
//                    dmBK.tblEtiketaStavkaBARKOD.Value := '(01)' + dmBK.tblIzberiArtikalBARKOD.AsString +
//                                                       //'(15)' + copy(inttostr(yearof(dmBK.tblIzberiArtikalDATUM_DO.Value)),3,2) + Copy(('0' + IntToStr(MonthOf(dmBK.tblIzberiArtikalDATUM_DO.Value))),Length(IntToStr(MonthOf(dmBK.tblIzberiArtikalDATUM_DO.Value))),2) +
//                                                       //         Copy(('0' + IntToStr(DayOf(dmBK.tblIzberiArtikalDATUM_DO.Value))),Length(IntToStr(DayOf(dmBK.tblIzberiArtikalDATUM_DO.Value))),2) +
//                                                      // '(13)' + copy(inttostr(yearof(dmBK.tblIzberiArtikalDATUM_PAK.Value)),3,2) + Copy(('0' + IntToStr(MonthOf(dmBK.tblIzberiArtikalDATUM_PAK.Value))),Length(IntToStr(MonthOf(dmBK.tblIzberiArtikalDATUM_PAK.Value))),2) +
//                                                       //         Copy(('0' + IntToStr(DayOf(dmBK.tblIzberiArtikalDATUM_PAK.Value))),Length(IntToStr(DayOf(dmBK.tblIzberiArtikalDATUM_PAK.Value))),2) +
//                                                       '(10)' + dmBK.tblIzberiArtikalLOT.Value +
//                                                       '(37)' + dmBK.tblIzberiArtikalKOLICINA_VO_PAKUVANJE.AsString;
//                end;

            end;

         //     if ((txtAmbalaza.Text <> '') or (txtVidAmbalaza.Text <> '')) then
           //     begin
           //       SetirajLukap(sender,dmBK.tblAmbalaza,txtVidAmbalaza,txtAmbalaza, cbAmbalaza);
           //     end;

         if ((Sender as TWinControl)= txtKolicinaBroj) then
         begin
             if Tag = 1 then aPresmetaj.Execute;
         end;

         if ((Sender as TWinControl)= txtImenitel) then
         begin
             if Tag = 2 then aPresmetajSD.Execute;
         end;
     end

end;

procedure TfrmHomogena.cxGrid1DBTableView1DataControllerDetailCollapsed(
  ADataController: TcxCustomDataController; ARecordIndex: Integer);
begin
  inherited;
  ADataController.ClearDetailLinkObject(ARecordIndex, 0);
  dmBK.tblEtiketaStavka.Open;
end;

procedure TfrmHomogena.cxGrid1DBTableView1DataControllerDetailExpanded(
  ADataController: TcxCustomDataController; ARecordIndex: Integer);
begin
  ADataController.FocusedRecordIndex := ARecordIndex;
end;

procedure TfrmHomogena.cxGrid1DBTableView1DataControllerDetailExpanding(
  ADataController: TcxCustomDataController; ARecordIndex: Integer;
  var AAllow: Boolean);
begin
  ADataController.CollapseDetails;
end;

procedure TfrmHomogena.cxGrid1DBTableView1DblClick(Sender: TObject);
begin
  if (inserting = false) then prefrli;
end;

procedure TfrmHomogena.cxGrid1DBTableView1FocusedRecordChanged(
  Sender: TcxCustomGridTableView; APrevFocusedRecord,
  AFocusedRecord: TcxCustomGridRecord; ANewItemRecordFocusingChanged: Boolean);
begin
//   dmBK.tblIzberiArtikal.open;
if (dmBK.tblEtiketa.State = dsBrowse) and (dmBK.tblEtiketaStavka.State = dsbrowse) then
  begin
   dmBK.dsIzberiArtikal.Enabled := true;
   SetirajLukap(sender,dmBK.tblIzberiArtikal,txtVidArtikal,txtArtikal, cbNazivArtikal);
  // SetirajLukap(sender,dmBK.tblAmbalaza,txtVidAmbalaza,txtAmbalaza, cbAmbalaza);
   if txtPakuvanje.Text = '' then
      dmBK.tblIzvedenaEM.Close
   else
   dmBK.tblIzvedenaEM.CloseOpen(true);
  end;
end;

//  ��������� �� ������ �� ��������� �� ���������� ���� �� ��������
//  ��������� ��: br - ����� ��� �� ������ (�� 0 �� 9) �� ��������� �� ����� �� ����� �� ���������� ����
//  s - ��������� �� ������ �� ������ (���������� �� string �� ����� �� integer �� IntToStr ���������)
procedure TfrmHomogena.SetSifra(br: integer; s : string);
begin
  _sifra_kluc[br] := s;
end;

procedure TfrmHomogena.Timer1Timer(Sender: TObject);
begin
// if aPogled.Execute then
//    dmres.frxReport1.Destroy;
end;

//  ������� �� ������ �� ��������� �� ���������� ���� �� ��������
//  ��������� ��: br - ����� ��� �� ������ (�� 0 �� 9) �� ��������� �� ����� �� ����� �� ���������� ����
function TfrmHomogena.GetSifra(br: integer) : string;
begin
  Result := _sifra_kluc[br];
end;

procedure TfrmHomogena.Image1Click(Sender: TObject);
begin

end;

//  �� ������� �� ������� ��� ����������� �� Master-�� �� �������� �� ��������� �� ������ ����� SetSifra
procedure TfrmHomogena.prefrli;
begin
  //ModalResult := mrOk;
  //SetSifra(0, dmBK.tblKontenPlanSIFRA.Value);
end;

procedure TfrmHomogena.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    //	��������� ��� ��������� �� �������

end;
procedure TfrmHomogena.FormCreate(Sender: TObject);
begin
//  ProcitajFormaIzgled(self);
 // if (dmRes.use_skin = true) then dxRibbon1.ColorSchemeName := dmRes.skin_name;

//  rData := TRepositoryData.Create();

   // dmBK.tblIzberiRN.Close;
//    dmBK.tblIzberiRN.ParamByName('status').Value := 1;
  //  dm.tblSosDel.Close;
   // dm.tblSosDel.Open;

 //   dmBK.tblIzberiRN.ParamByName('rn').Value := dm.tblRNVlezID.Value;
//    dmBK.tblIzberiRN.open;
   // dmBK.tblIzberiArtikal.Close;
    //dmBK.tblIzberiArtikal.ParamByName('rn').Value := dm.tblRNVlezID.Value;
 //   dmBK.tblIzberiArtikal.Close;
//    dmBK.tblIzberiArtikal.ParamByName('vid_artikal').Value := dm.tblRNVlezVID_ARTIKAL.Value;
//    dmBK.tblIzberiArtikal.ParamByName('artikal').Value := dm.tblRNVlezARTIKAL.Value;
 //   dmBK.tblIzberiArtikal.ParamByName('id_rns').AsLong := id_rn_stavka;
 //   dmBK.tblIzberiArtikal.Open;

//    dm.tblSosDel.Close;
//    dm.tblSosDel.ParamByName('lot').AsString := dmBK.tblIzberiArtikalLOT.Value;
//    dm.tblSosDel.Open;

    dmBK.tblAmbalaza.Close;
    dmBK.tblAmbalaza.Open;

    //dmBK.tblIzvedenaEM.Close;
    dmBK.tblIzvedenaEM.ParamByName('vid_artikal').AsString := '%';
    dmBK.tblIzvedenaEM.ParamByName('artikal').AsString := '%';
    dmBK.tblIzvedenaEM.Open;

    //dmBK.tblEtiketa.Close;
    dmBK.tblEtiketa.Open;
    dmBK.tblEtiketaStavka.Open;

        SpremiForma(self);
  //	�� ��������� �� ������� ������ ��� ���������� � ���� � ��� �������� �� �������
    PrvPosledenTab(dPanel,posledna,prva);
    dxBarManager1Bar1.Caption := Caption;
    dxComponentPrinter1Link1.ReportTitle.Text := Caption;

  //	������� �� ������������ �� ������ �� ������
    procitajGridOdBaza(Name,cxGrid1DBTableView1,false,false);
  //	������� �� ������������ �� ��������� �� ������
    procitajPrintOdBaza(Name,cxGrid1DBTableView1.Name, dxComponentPrinter1Link1);

//    dmBK.tblIzvedenaEM.Close;
//    dmBK.tblIzvedenaEM.ParamByName('vid_artikal').AsString := '%';
//    dmBK.tblIzvedenaEM.ParamByName('artikal').AsString := '%';
//    dmBK.tblIzvedenaEM.Open;
//
//    dmBK.tblEtiketa.Close;
//    dmBK.tblEtiketa.Open;
//    dmBK.tblEtiketaStavka.Open;
//    dmBK.tblIzberiRN.Close;
//    dmBK.tblIzberiRN.open;
//   // dmBK.tblIzberiArtikal.Close;
//    dmBK.tblIzberiArtikal.Open;
//
//    dmBK.tblAmbalaza.Close;
//    dmBK.tblAmbalaza.Open;

    cxPageControl1.ActivePage := Osnovno;
   // cxGrid1.SetFocus;
  //  dmBK.tblIzberiRN.Locate('ID',VarArrayOf([dmBK.tblEtiketaID_RABOTEN_NALOG.Value]),[]);
    pojavi := 0;

    dmBK.qMax.Close;
    dmBK.qMax.ExecQuery;
end;

procedure TfrmHomogena.FormKeyDown(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
   if ((GetKeyState(VK_CONTROL) and 67) = 67) and//(GetKeyState(VK_CONTROL) AND  128) = 128)
 //  and ((GetKeyState(ord('8')) AND  128) = 128)
    ((GetKeyState(ord('P')) and 67) = 67)  then
   begin
     aNov.Execute;
     //dmBK.tblEtiketaID_RABOTEN_NALOG.Value := dmBK.tblIzberiRNID.Value;
     //dmBK.tblEtiketaID_TRAILER.value := tl;
  //   aZapisi.Execute;
     dmBK.tblEtiketaStavkaVID_ARTIKAL.Value := dmBK.tblIzberiArtikalARTVID.Value;
     dmBK.tblEtiketaStavkaARTIKAL.Value := dmBK.tblIzberiArtikalID.Value;
     aBarkod.Execute;
     aZapisi.Execute;
     aPecatiEtiketa.Execute;
   end;

end;

//------------------------------------------------------------------------------

procedure TfrmHomogena.FormShow(Sender: TObject);
begin

    aAvtomatski.Execute;

  //  ������� �� ������������ ������������/���������� �� ���������� �� ���� �����
//    SpremiForma(self);
//  //	�� ��������� �� ������� ������ ��� ���������� � ���� � ��� �������� �� �������
//    PrvPosledenTab(dPanel,posledna,prva);
//    dxBarManager1Bar1.Caption := Caption;
//    dxComponentPrinter1Link1.ReportTitle.Text := Caption;
//
//  //	������� �� ������������ �� ������ �� ������
//    procitajGridOdBaza(Name,cxGrid1DBTableView1,false,false);
//  //	������� �� ������������ �� ��������� �� ������
//    procitajPrintOdBaza(Name,cxGrid1DBTableView1.Name, dxComponentPrinter1Link1);
//
////    dmBK.tblIzvedenaEM.Close;
////    dmBK.tblIzvedenaEM.ParamByName('vid_artikal').AsString := '%';
////    dmBK.tblIzvedenaEM.ParamByName('artikal').AsString := '%';
////    dmBK.tblIzvedenaEM.Open;
////
////    dmBK.tblEtiketa.Close;
////    dmBK.tblEtiketa.Open;
////    dmBK.tblEtiketaStavka.Open;
////    dmBK.tblIzberiRN.Close;
////    dmBK.tblIzberiRN.open;
////   // dmBK.tblIzberiArtikal.Close;
////    dmBK.tblIzberiArtikal.Open;
////
////    dmBK.tblAmbalaza.Close;
////    dmBK.tblAmbalaza.Open;
//
//    cxPageControl1.ActivePage := Osnovno;
//    cxGrid1.SetFocus;
//    dmBK.tblIzberiRN.Locate('ID',VarArrayOf([dmBK.tblEtiketaID_RABOTEN_NALOG.Value]),[]);
//    pojavi := 0;
   // dmMat.tblNurko.Open;

  // 	cbNazivArtikal.RepositoryItem := dmRes.InitRepository(-101, 'cbNazivArtikal', Name, rData);
    //cbSurovina.RepositoryItem := dmRes.InitRepository(-102, 'cbSurovina', Name, rData);
  //  cbAmbalaza.RepositoryItem := dmRes.InitRepository(-103, 'cbAmbalaza', Name, rData);
end;
//------------------------------------------------------------------------------

procedure TfrmHomogena.SaveToIniFileExecute(Sender: TObject);
begin
//    cxGrid1DBTableView1.StoreToIniFile(Name,True,[]);
end;
//------------------------------------------------------------------------------

procedure TfrmHomogena.cxGrid1DBTableView1KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  case Key of
    VK_RETURN:
    begin
      if (inserting = false) then prefrli;
    end;

  end;
end;

procedure TfrmHomogena.cxGrid1DBTableView1KeyPress(Sender: TObject;
  var Key: Char);
begin
  if(Ord(Key) <> VK_RETURN) then cxSortiraj(cxGrid1DBTableView1);
end;

procedure TfrmHomogena.cxGrid1DBTableView1MouseEnter(Sender: TObject);
begin
   na_koj_grid := 1;
end;

procedure TfrmHomogena.cxGrid2DBTableView1DblClick(Sender: TObject);
begin
  if dmBK.tblEtiketa.State = dsBrowse then
  begin
    cxPageControl1.ActivePage := Napredno;
  end;
end;

procedure TfrmHomogena.cxGrid2DBTableView1EditKeyDown(
  Sender: TcxCustomGridTableView; AItem: TcxCustomGridTableItem;
  AEdit: TcxCustomEdit; var Key: Word; Shift: TShiftState);
begin
  if key = VK_RETURN then
  begin
    cxPageControl1.ActivePage := Napredno;
  end;
end;

procedure TfrmHomogena.cxGrid2DBTableView1MouseEnter(Sender: TObject);
begin
  na_koj_grid := 2;
end;

procedure TfrmHomogena.cxPageControl1Change(Sender: TObject);
begin
//if (dmBK.tblEtiketaStavka.State = dsinsert) or (dmBK.tblEtiketaStavka.IsEmpty) then
//    dmBK.dsIzberiArtikal.Enabled := False
//  else
//    dmBK.dsIzberiArtikal.Enabled := true;
  if cxPageControl1.ActivePageIndex = 0 then
  begin
    PrvPosledenTab(dPanel,posledna,prva);
  end
  else
  begin
//  if dmBK.tblIzberiArtikal.State = dsInactive then
//     ShowMessage('zatvorena');
  if not (dmBK.tblEtiketaID_RABOTEN_NALOG.IsNull) then
   begin
    //  dmBK.tblIzberiArtikal.Close;
    //  dmBK.tblIzberiArtikal.ParamByName('id').AsInteger := dmBK.tblEtiketaID_RABOTEN_NALOG.Value;
    //  dmBK.tblIzberiArtikal.Open;
   // dmBK.tblIzberiArtikal.Open;
    PrvPosledenTab(dPanel2,posledna,prva);
    SetirajLukap(sender,dmBK.tblIzberiArtikal,txtVidArtikal,txtArtikal, cbNazivArtikal);
   // SetirajLukap(sender,dmBK.tblAmbalaza,txtVidAmbalaza,txtAmbalaza, cbAmbalaza);
    if txtPakuvanje.Text = '' then
      dmBK.tblIzvedenaEM.Close
     else
      dmBK.tblIzvedenaEM.CloseOpen(true);
   end;
  // Napredno.Caption := '��������� �� �� ��� '+dmBK.tblEtiketaBR_RN.AsString;
//    SetirajLukap(sender,dmBK.tblIzberiArtikal,txtVidArtikal,txtArtikal, cbNazivArtikal);
  end;
end;



procedure TfrmHomogena.dPanel2Click(Sender: TObject);
begin

end;

//  ����� �� �����
procedure TfrmHomogena.aZapisiExecute(Sender: TObject);
var
  st: TDataSetState;
begin
  if (cxPageControl1.ActivePageIndex = 0) then
  begin
  st := cxGrid1DBTableView1.DataController.DataSet.State;
  if st in [dsEdit,dsInsert] then
  begin
//    if (Validacija(dPanel) = false) then
  //  begin
  //    begin


          if ((st = dsInsert) and inserting) then
          begin
            cxGrid1DBTableView1.DataController.DataSet.Post;
            dPanel.Enabled := false;
            //dmBK.tblIzberiArtikal.Close;
           // dmBK.tblIzberiArtikal.ParamByName('linija').Value := linija;
           // dmBK.tblIzberiArtikal.Open;
          // if avtomatski = 0 then
            cxPageControl1.ActivePageIndex := 1;
            aNov.Execute;
          end;

          if ((st = dsInsert) and (not inserting)) then
          begin

            cxGrid1DBTableView1.DataController.DataSet.Post;
          //  cxPageControl1.ActivePageIndex := 0;
            dPanel.Enabled:=false;
           // dPanel2.Enabled:=false;
            lPanel.Enabled:=true;
          //  cxGrid1.SetFocus;
          end;

          if (st = dsEdit) then
          begin
            cxGrid1DBTableView1.DataController.DataSet.Post;
            //cxPageControl1.ActivePageIndex := 0;
            dPanel.Enabled:=false;
            //dPanel2.Enabled:=false;
            lPanel.Enabled:=true;
        //    cxGrid1.SetFocus;
          end;
      end;
    //end;
 // end;
  end
  else
  begin
    //  if (cxPageControl1.ActivePageIndex = 0) then ZapisiButton.SetFocus;
    if (cxPageControl1.ActivePageIndex = 1) then
    //Zapisi2.SetFocus;
  begin
  st := dmBK.tblEtiketaStavka.State;
  if st in [dsEdit,dsInsert] then
  begin

    //cxPageControl1.ActivePageIndex := 0;
//    if (Validacija(dPanel2) = false) then
    begin
        if Tag = 1 then aPresmetaj.Execute;
        if Tag = 2 then aPresmetajSD.Execute;
      begin
          if ((st = dsInsert) and inserting) then
          begin
           if ima then
            dmBK.tblEtiketaStavkaBR_KUTIJA.Value := dmBK.qmax.FldByName['MAKS'].Value;
            dmBK.tblEtiketaStavka.Post;
            Close;
           ///lpanel.Enabled := true;
          //  cxGrid1.SetFocus;
            //cxPageControl1.ActivePageIndex := 0;
           // aNov.Execute;
          end;

//          if Tag in [2] then aPresmetajSD.Execute;
//      begin
//          if ((st = dsInsert) and inserting) then
//          begin
//           if ima then
//            dmBK.tblEtiketaStavkaBR_KUTIJA.Value := dmBK.qmax.FldByName['MAKS'].Value;
//            dmBK.tblEtiketaStavka.Post;
//            Close;
//           ///lpanel.Enabled := true;
//          //  cxGrid1.SetFocus;
//            //cxPageControl1.ActivePageIndex := 0;
//           // aNov.Execute;
//          end;
//      end;


          if ((st = dsInsert) and (not inserting)) then
          begin
            dmBK.tblEtiketaStavka.Post;
           // cxPageControl1.ActivePageIndex := 0;
           // dPanel.Enabled:=false;
           // dPanel2.Enabled:=false;
          //  lPanel.Enabled:=true;
           // cxGrid1.SetFocus;
          end;

          if (st = dsEdit) then
          begin
            dmBK.tblEtiketaStavka.Post;
            //cxPageControl1.ActivePageIndex := 0;
            //dPanel.Enabled:=false;
           // dPanel2.Enabled:=false;
           // lPanel.Enabled:=true;
        //    cxGrid1.SetFocus;
          end;
      end;
    end;
  end;
  end;
  end;

end;

procedure TfrmHomogena.aZapisiKExecute(Sender: TObject);
var
  st: TDataSetState;
begin
  if (cxPageControl1.ActivePageIndex = 0) then
  begin
  st := cxGrid1DBTableView1.DataController.DataSet.State;
  if st in [dsEdit,dsInsert] then
  begin
          if ((st = dsInsert) and inserting) then
          begin
            cxGrid1DBTableView1.DataController.DataSet.Post;
            dPanel.Enabled := false;
            cxPageControl1.ActivePageIndex := 1;
            aNov.Execute;
          end;

          if ((st = dsInsert) and (not inserting)) then
          begin

            cxGrid1DBTableView1.DataController.DataSet.Post;
            dPanel.Enabled:=false;
            lPanel.Enabled:=true;

          end;

          if (st = dsEdit) then
          begin
            cxGrid1DBTableView1.DataController.DataSet.Post;
            dPanel.Enabled:=false;
            lPanel.Enabled:=true;
          end;
      end;
  end
  else
  begin
    if (cxPageControl1.ActivePageIndex = 1) then
  begin
  st := dmBK.tblEtiketaStavka.State;
  if st in [dsEdit,dsInsert] then
  begin
    begin
        if Tag = 1 then aPresmetajK.Execute;
        if Tag = 2 then aPresmetajSD.Execute;
      begin
          if ((st = dsInsert) and inserting) then
          begin
           if ima then
            dmBK.tblEtiketaStavkaBR_KUTIJA.Value := dmBK.qmax.FldByName['MAKS'].Value;
            dmBK.tblEtiketaStavka.Post;
            Close;
          end;

          if ((st = dsInsert) and (not inserting)) then
          begin
            dmBK.tblEtiketaStavka.Post;
          end;

          if (st = dsEdit) then
          begin
            dmBK.tblEtiketaStavka.Post;
          end;
      end;
    end;
  end;
  end;
  end;


end;

procedure TfrmHomogena.Barcode1D_Code1281Change(Sender: TObject);
begin

end;

//	����� �� ���������� �� �������
procedure TfrmHomogena.aOtkaziExecute(Sender: TObject);
begin
if cxPageControl1.ActivePage = Osnovno then
  begin
  if (cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) then
  begin
      ModalResult := mrCancel;
      Close();
  end
  else
  begin
      cxGrid1DBTableView1.DataController.DataSet.Cancel;
      RestoreControls(dPanel);
      dPanel.Enabled := false;
      dPanel2.Enabled := false;
      lPanel.Enabled := true;
     // cxGrid1.SetFocus;
  end
  end
  else
  begin
     if (dmBK.tblEtiketaStavka.State = dsBrowse) then
        begin
            ModalResult := mrCancel;
            Close();
        end
        else
        begin
            dmBK.tblEtiketaStavka.Cancel;
          //  RestoreControls(dPanel2);
   //         dmBK.dsizberiArtikal.Enabled := false;
          //  cxGrid1.SetFocus;
          Close;
        end
  end;
end;

//----------------------------------------------------------------------------------
// ��������� �� ��������, ���������� �� ��������� � �������� �� Print ��������
//----------------------------------------------------------------------------------

//	����� �� �������� �� �������� �� �������
procedure TfrmHomogena.aPageSetupExecute(Sender: TObject);
begin
  dxComponentPrinter1Link1.PageSetup;
end;

//	����� �� ������� �� ������
procedure TfrmHomogena.aPecatiEtiketaExecute(Sender: TObject);
begin
// �� �� ������� ����������, ���� �� ���� ������� �� �� ���� �� �� ������ �� ���������! �����

if not (dmBK.tblEtiketa.State in [dsEdit,dsInsert]) and (dmBK.tblEtiketaStavka.State = dsBrowse) then
 begin
  dmRes.Spremi('MAN', 2);
  dmKon.tblSqlReport.ParamByName('id').AsInteger :=dmBK.tblEtiketaID.Value;;
  dmKon.tblSqlReport.Open;
  dmRes.frxReport1.PrintOptions.ShowDialog := false;
  dmres.frxReport1.PrepareReport(true);
  dmRes.frxReport1.Print;
 end;
end;

procedure TfrmHomogena.aPecatiTabelaExecute(Sender: TObject);
begin
  dxComponentPrinter1Link1.ReportTitle.Text := Caption;

  //dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Clear;
  dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Add(dmkon.firma_naziv);

  dxComponentPrinter1Link1.PrinterPage.PageHeader.LeftTitle.Clear;
  //dxComponentPrinter1Link1.PrinterPage.PageHeader.LeftTitle.Add('������� : ' + TipSifra.Text + '-' + IdSifra.Text + ' ' + Partner.Text);

  dxComponentPrinter1.Preview(true, dxComponentPrinter1Link1);
end;

//	����� �� ���������� �� ��������� �� �������
procedure TfrmHomogena.aPodesuvanjePecatenjeExecute(Sender: TObject);
begin
  dxComponentPrinter1Link1.DesignReport();
end;

procedure TfrmHomogena.aPogledExecute(Sender: TObject);
begin
 pojavi := 1;
// Timer1.Enabled := true;
 // writememo:=WriteThread.Create;
//
if not (dmBK.tblEtiketa.State in [dsEdit,dsInsert]) and (dmBK.tblEtiketaStavka.State = dsBrowse) then
 begin
  dmRes.Spremi('MAN', 2);
  dmKon.tblSqlReport.ParamByName('id').AsInteger :=dmBK.tblEtiketaID.Value;;
  dmKon.tblSqlReport.Open;


   //  Turn off/on some thisng that don't work great in the designer
   //  frxReport1.Preview := nil;
   //  frxReport1.PreviewOptions.MDIChild := false;
     dmres.frxReport1.PreviewOptions.Modal := false;
     dmres.frxReport1.PreviewOptions.Maximized := False;
    // dmres.frxReport1..Position := poScreenCenter;
     //dmres.frxReport1.PreviewOptions.Buttons.print:= False;
  //   SaveOnAfterPrint := frxReport1.OnAfterPrint;
 //    frxReport1.OnAfterPrint := nil;

       //  Register design save function
   //  MainForm.frxDesigner1.OnSaveReport := frxDesigner1SaveReport;
  //   MainForm.frxDesigner1.OnLoadReport := frxDesigner1LoadReport;
     try
     //  revertStream.Clear;
     //  frxReport1.SaveToStream(revertStream);
       // ActiveControl := cxgrid1;
     //  self.WindowState := wsMinimized;
        dmRes.frxReport1.PrintOptions.ShowDialog := false;
        dmRes.frxReport1.PrepareReport(True);
       // ActiveControl := cxgrid1;
        dmres.frxReport1.ShowReport;
  ///      dmres.frxReport1.Visible := false;

     //  dmres.frxReport1.Visible := True;

     //   Abort;
     finally
         //  Turn then back on/off
       //frxReport1.Preview := frxPreview1;
     //  frxReport1.PreviewOptions.MDIChild := true;
     //  frxReport1.PreviewOptions.Modal := false;
      // frxReport1.EngineOptions.SilentMode := true;
      // frxReport1.OnAfterPrint := SaveOnAfterPrint;
      ActiveControl := cxgrid1;
    //   MainForm.frxDesigner1.OnSaveReport := nil;
    //   MainForm.frxDesigner1.OnLoadReport := nil;
     end;

   //
 // dmRes.frxReport1.ShowReport(true);
 //close;
  //dmRes.frxReport1.PrintOptions.ShowDialog := false;
 //dmres.frxReport1.Destroy;
 // Timer1.Enabled := False;

 end;
end;

procedure TfrmHomogena.aPresmetajExecute(Sender: TObject);
var broj:string;  i : integer;  def:AnsiString;
begin
              // SetirajLukap(sender,dmBK.tblIzberiArtikal,txtVidArtikal,txtArtikal, cbNazivArtikal);
               //    txtVidAmbalaza.text := dmBK.tblIzberiArtikalVID_AMBALAZA.AsString;
              //     txtAmbalaza.Text := dmBK.tblIzberiArtikalAMBALAZA.AsString;
               //    txtPakuvanje.Text :=  dmBK.tblIzberiArtikalID_PAKUVANJE.AsString;
              //     txtKolicinaArtikal.Text := dmBK.tblIzberiArtikalKOLICINA_VO_PAKUVANJE.AsString;//dmBK.tblIzberiArtikalKOLICINA.AsString;
                //   txtKolicinaBroj.Text := dmBK.tblIzberiArtikalKOLICINA_BROJ.AsString;
               //    txtKolicinaEdMerka.Text := dmBK.tblIzberiArtikalKOLICINA.AsString; //dmBK.tblIzberiArtikalKOLICINA_VO_PAKUVANJE.AsString;
                //   txtSerija.Text := dmBK.tblIzberiArtikalSERISKI_BROJ.AsString;
              //     lblMerka.Caption := dmBK.tblIzberiArtikalMERKA.AsString;
                     dmBK.tblEtiketaStavkaID_RNS.Value := dmBK.tblIzberiArtikalID_RNS.Value;
                 //  SetirajLukap(sender,dmBK.tblAmbalaza,txtVidAmbalaza,txtAmbalaza, cbAmbalaza);
               //    if txtPakuvanje.Text = '' then
               //       dmBK.tblIzvedenaEM.Close
              //     else
               //    begin
                     dmBK.tblIzvedenaEM.Close;
                     dmBK.tblIzvedenaEM.ParamByName('artikal').Value := dmBK.tblIzberiArtikalID.Value;
                     dmBK.tblIzvedenaEM.ParamByName('vid_artikal').Value := dmBK.tblIzberiArtikalARTVID.Value;
                     dmBK.tblIzvedenaEM.open;
                //   end;
                                   //dmBK.tblEtiketaStavkaBARKOD.Value := dmBK.tblIzberiArtikalBARKOD.AsString;
                     dmBK.tblEtiketaStavkaGTIN.Value := dmBK.tblIzberiArtikalBARKOD.Value;
                     dmBK.tblEtiketaStavkaLOT.Value := dmBK.tblIzberiArtikalLOT.Value;

                     dmBK.MaxEtiketaStavka.Close;
                     dmBK.MaxEtiketaStavka.ExecQuery;

//                   if tag = 1 then
//                   begin
//                    txtKolicinaBroj.SetFocus;
//                    Abort;
//                   end;

                   if not dmBK.tblIzvedenaEMKOEFICIENT.IsNull then
                     def := dmBK.tblIzvedenaEMKOEFICIENT.Value
                   else
                     def := '';

                   for I := 1 to (7-(Length(inttostr(dmBK.MaxEtiketaStavka.FldByName['maks'].AsInteger+1)))) do
                   begin
                    broj := broj+'0';
                   end;
                    broj := kompaniski_broj + broj + inttostr(dmBK.MaxEtiketaStavka.FldByName['maks'].AsInteger+1);
                    dmBK.Kontrolen_broj(broj);
                    dmBK.tblEtiketaStavkaSSCC.Value := '(00)' + broj + inttostr(kb);
//                    if (def <> '') and (tag = 0) then
//
//                      dmBK.tblEtiketaStavkaBARKOD.Value := '(01)' + dmBK.tblIzberiArtikalBARKOD.AsString +
//                                                       //'(15)' + copy(inttostr(yearof(dmBK.tblIzberiArtikalDATUM_DO.Value)),3,2) + Copy(('0' + IntToStr(MonthOf(dmBK.tblIzberiArtikalDATUM_DO.Value))),Length(IntToStr(MonthOf(dmBK.tblIzberiArtikalDATUM_DO.Value))),2) +
//                                                       //         Copy(('0' + IntToStr(DayOf(dmBK.tblIzberiArtikalDATUM_DO.Value))),Length(IntToStr(DayOf(dmBK.tblIzberiArtikalDATUM_DO.Value))),2) +
//                                                      // '(13)' + copy(inttostr(yearof(dmBK.tblIzberiArtikalDATUM_PAK.Value)),3,2) + Copy(('0' + IntToStr(MonthOf(dmBK.tblIzberiArtikalDATUM_PAK.Value))),Length(IntToStr(MonthOf(dmBK.tblIzberiArtikalDATUM_PAK.Value))),2) +
//                                                       //         Copy(('0' + IntToStr(DayOf(dmBK.tblIzberiArtikalDATUM_PAK.Value))),Length(IntToStr(DayOf(dmBK.tblIzberiArtikalDATUM_PAK.Value))),2) +
//                                                       '(10)' + dmBK.tblIzberiArtikalLOT.Value +
//                                                       //'(37)' + dmBK.tblIzberiArtikalKOLICINA_BROJ.AsString;
//
//                                                       '(37)' +floattostr( strtofloat(def))
//                   else
//                   if (def = '') and (tag = 0) then
//
//                     dmBK.tblEtiketaStavkaBARKOD.Value := '(01)' + dmBK.tblIzberiArtikalBARKOD.AsString +
//                                                       //'(15)' + copy(inttostr(yearof(dmBK.tblIzberiArtikalDATUM_DO.Value)),3,2) + Copy(('0' + IntToStr(MonthOf(dmBK.tblIzberiArtikalDATUM_DO.Value))),Length(IntToStr(MonthOf(dmBK.tblIzberiArtikalDATUM_DO.Value))),2) +
//                                                       //         Copy(('0' + IntToStr(DayOf(dmBK.tblIzberiArtikalDATUM_DO.Value))),Length(IntToStr(DayOf(dmBK.tblIzberiArtikalDATUM_DO.Value))),2) +
//                                                      // '(13)' + copy(inttostr(yearof(dmBK.tblIzberiArtikalDATUM_PAK.Value)),3,2) + Copy(('0' + IntToStr(MonthOf(dmBK.tblIzberiArtikalDATUM_PAK.Value))),Length(IntToStr(MonthOf(dmBK.tblIzberiArtikalDATUM_PAK.Value))),2) +
//                                                       //         Copy(('0' + IntToStr(DayOf(dmBK.tblIzberiArtikalDATUM_PAK.Value))),Length(IntToStr(DayOf(dmBK.tblIzberiArtikalDATUM_PAK.Value))),2) +
//                                                       '(10)' + dmBK.tblIzberiArtikalLOT.Value +
//                                                       //'(37)' + dmBK.tblIzberiArtikalKOLICINA_BROJ.AsString;
//
//                                                       '(37)' +'1';    //dmBK.tblIzberiArtikalKOLICINA_REALNA.Asstring;
//                   if tag = 1 then
                     dmBK.tblEtiketaStavkaBARKOD.Value := '(01)' + dmBK.tblIzberiArtikalBARKOD.AsString +
                                                       //'(15)' + copy(inttostr(yearof(dmBK.tblIzberiArtikalDATUM_DO.Value)),3,2) + Copy(('0' + IntToStr(MonthOf(dmBK.tblIzberiArtikalDATUM_DO.Value))),Length(IntToStr(MonthOf(dmBK.tblIzberiArtikalDATUM_DO.Value))),2) +
                                                       //         Copy(('0' + IntToStr(DayOf(dmBK.tblIzberiArtikalDATUM_DO.Value))),Length(IntToStr(DayOf(dmBK.tblIzberiArtikalDATUM_DO.Value))),2) +
                                                      // '(13)' + copy(inttostr(yearof(dmBK.tblIzberiArtikalDATUM_PAK.Value)),3,2) + Copy(('0' + IntToStr(MonthOf(dmBK.tblIzberiArtikalDATUM_PAK.Value))),Length(IntToStr(MonthOf(dmBK.tblIzberiArtikalDATUM_PAK.Value))),2) +
                                                       //         Copy(('0' + IntToStr(DayOf(dmBK.tblIzberiArtikalDATUM_PAK.Value))),Length(IntToStr(DayOf(dmBK.tblIzberiArtikalDATUM_PAK.Value))),2) +
                                                       '(10)' + dmBK.tblIzberiArtikalLOT.Value +
                                                       //'(37)' + dmBK.tblIzberiArtikalKOLICINA_BROJ.AsString;

                                                       '(37)' +txtKolicinaBroj.Text;


             //   end;
end;

procedure TfrmHomogena.aPresmetajKExecute(Sender: TObject);
var broj:string;  i : integer;  def:AnsiString;
begin
                   dmBK.tblEtiketaStavkaID_RNS.Value := dmBK.tblIzberiAID_RNS.Value;

                   dmBK.tblIzvedenaEM.Close;
                   dmBK.tblIzvedenaEM.ParamByName('artikal').Value := dmBK.tblIzberiAID.Value;
                   dmBK.tblIzvedenaEM.ParamByName('vid_artikal').Value := dmBK.tblIzberiAARTVID.Value;
                   dmBK.tblIzvedenaEM.open;

                   dmBK.tblEtiketaStavkaGTIN.Value := dmBK.tblIzberiABARKOD.Value;
                   dmBK.tblEtiketaStavkaLOT.Value := dmBK.tblIzberiALOT.Value;

                   dmBK.MaxEtiketaStavka.Close;
                   dmBK.MaxEtiketaStavka.ExecQuery;


                   if not dmBK.tblIzvedenaEMKOEFICIENT.IsNull then
                     def := dmBK.tblIzvedenaEMKOEFICIENT.Value
                   else
                     def := '';

                   for I := 1 to (7-(Length(inttostr(dmBK.MaxEtiketaStavka.FldByName['maks'].AsInteger+1)))) do
                   begin
                    broj := broj+'0';
                   end;
                    broj := kompaniski_broj + broj + inttostr(dmBK.MaxEtiketaStavka.FldByName['maks'].AsInteger+1);
                    dmBK.Kontrolen_broj(broj);
                    dmBK.tblEtiketaStavkaSSCC.Value := '(00)' + broj + inttostr(kb);
                    dmBK.tblEtiketaStavkaBARKOD.Value := '(01)' + dmBK.tblIzberiArtikalBARKOD.AsString +
                                                       '(10)' + dmBK.tblIzberiArtikalLOT.Value +

                                                       '(37)' +txtKolicinaBroj.Text;

end;

procedure TfrmHomogena.aPresmetajSDExecute(Sender: TObject);
var broj:string;  i : integer;  def:AnsiString;
begin
                   dmBK.tblEtiketaStavkaID_RNS.Value := dmBK.tblIzberiArtikalID_RNS.Value;

                   dmBK.tblIzvedenaEM.Close;
                   dmBK.tblIzvedenaEM.ParamByName('artikal').Value := dmBK.tblIzberiAID.Value;
                   dmBK.tblIzvedenaEM.ParamByName('vid_artikal').Value := dmBK.tblIzberiAARTVID.Value;
                   dmBK.tblIzvedenaEM.open;

                   dmBK.tblEtiketaStavkaGTIN.Value := dmBK.tblIzberiArtikalBARKOD.Value;
                   dmBK.tblEtiketaStavkaLOT.Value := dmBK.tblIzberiArtikalLOT.Value;

                   dmBK.MaxEtiketaStavka.Close;
                   dmBK.MaxEtiketaStavka.ExecQuery;


                   if not dmBK.tblIzvedenaEMKOEFICIENT.IsNull then
                     def := dmBK.tblIzvedenaEMKOEFICIENT.Value
                   else
                     def := '';

                   for I := 1 to (7-(Length(inttostr(dmBK.MaxEtiketaStavka.FldByName['maks'].AsInteger+1)))) do
                   begin
                    broj := broj+'0';
                   end;
                    broj := kompaniski_broj + broj + inttostr(dmBK.MaxEtiketaStavka.FldByName['maks'].AsInteger+1);
                    dmBK.Kontrolen_broj(broj);
                    dmBK.tblEtiketaStavkaSSCC.Value := '(00)' + broj + inttostr(kb);
                    if Length(txtImenitel.Text)=1 then  txtImenitel.Text := '0'+ txtImenitel.Text;
                    if Length(txtSifraSD.Text)=1 then  txtSifraSD.Text := '0'+ txtSifraSD.Text;
                    dmBK.tblEtiketaStavkaBARKOD.Value := '(01)' + dmBK.tblIzberiArtikalBARKOD.AsString +
                                                         '(10)' + dmBK.tblIzberiArtikalLOT.Value +
                                                         '(37)' + txtKolicinaBroj.Text +
                                                         '(8026)' + txtSifraSD.Text + txtImenitel.Text;



end;

procedure TfrmHomogena.aSnimiPecatenjeExecute(Sender: TObject);
begin
  zacuvajPrintVoBaza(Name,cxGrid1DBTableView1.Name,dxComponentPrinter1Link1);
end;

//	����� �� ������ �� ����������� �������� �� ������� (Utils.pas)
procedure TfrmHomogena.aBrisiPodesuvanjePecatenjeExecute(Sender: TObject);
begin
  brisiPrintOdBaza(Name,cxGrid1DBTableView1.Name, dxComponentPrinter1Link1);
end;

procedure TfrmHomogena.aBarkodExecute(Sender: TObject);
var broj:string;  i : integer;  def:AnsiString;
begin
   if ((txtArtikal.Text <> '') or (txtVidArtikal.Text <> '')) then
                begin
                   if tag in [1, 2] then
                   begin
                    dpanel2.Enabled := True;
                    cxGroupBox1.Enabled := True;
                   // aPresmetaj.Execute;
                    txtKolicinaBroj.SetFocus;
                    Abort;
                   end;

                  // SetirajLukap(sender,dmBK.tblIzberiArtikal,txtVidArtikal,txtArtikal, cbNazivArtikal);
                   //txtVidAmbalaza.text := dmBK.tblIzberiArtikalVID_AMBALAZA.AsString;
                   //txtAmbalaza.Text := dmBK.tblIzberiArtikalAMBALAZA.AsString;
                  // txtPakuvanje.Text :=  dmBK.tblIzberiArtikalID_PAKUVANJE.AsString;
                 //  txtKolicinaArtikal.Text := dmBK.tblIzberiArtikalKOLICINA_VO_PAKUVANJE.AsString;//dmBK.tblIzberiArtikalKOLICINA.AsString;
                 //  txtKolicinaBroj.Text := dmBK.tblIzberiArtikalKOLICINA_BROJ.AsString;
                 //  txtKolicinaEdMerka.Text := dmBK.tblIzberiArtikalKOLICINA.AsString; //dmBK.tblIzberiArtikalKOLICINA_VO_PAKUVANJE.AsString;
                 //  txtSerija.Text := dmBK.tblIzberiArtikalSERISKI_BROJ.AsString;
                  // lblMerka.Caption := dmBK.tblIzberiArtikalMERKA.AsString;
                   dmBK.tblEtiketaStavkaID_RNS.Value := dmBK.tblIzberiArtikalID_RNS.Value;
                 //  SetirajLukap(sender,dmBK.tblAmbalaza,txtVidAmbalaza,txtAmbalaza, cbAmbalaza);
                   if (txtPakuvanje.Text = '') and (tag in [1, 2]) then
                      dmBK.tblIzvedenaEM.Close
                   else
                   begin
                     dmBK.tblIzvedenaEM.Close;
                     dmBK.tblIzvedenaEM.ParamByName('artikal').Value := dmBK.tblIzberiArtikalID.Value;
                     dmBK.tblIzvedenaEM.ParamByName('vid_artikal').Value := dmBK.tblIzberiArtikalARTVID.Value;
                     dmBK.tblIzvedenaEM.open;
                   end;
                                   //dmBK.tblEtiketaStavkaBARKOD.Value := dmBK.tblIzberiArtikalBARKOD.AsString;
                   dmBK.tblEtiketaStavkaGTIN.Value := dmBK.tblIzberiArtikalBARKOD.Value;
                   dmBK.tblEtiketaStavkaLOT.Value := dmBK.tblIzberiArtikalLOT.Value;
                 //  dmbk.tblEtiketaStavkaID_POR_S_T.Value := dmBK.tblIzberiArtikalid

                   dmBK.MaxEtiketaStavka.Close;
                   dmBK.MaxEtiketaStavka.ExecQuery;

//                   if tag = 1 then
//                   begin
//                    txtKolicinaBroj.SetFocus;
//                    Abort;
//                   end;

                   if not dmBK.tblIzvedenaEMKOEFICIENT.IsNull then
                     def := dmBK.tblIzvedenaEMKOEFICIENT.Value
                   else
                     def := '';

                   for I := 1 to (7-(Length(inttostr(dmBK.MaxEtiketaStavka.FldByName['maks'].AsInteger+1)))) do
                   begin
                    broj := broj+'0';
                   end;
                    broj := kompaniski_broj + broj + inttostr(dmBK.MaxEtiketaStavka.FldByName['maks'].AsInteger+1);
                    dmBK.Kontrolen_broj(broj);
                    dmBK.tblEtiketaStavkaSSCC.Value := '(00)' + broj + inttostr(kb);
                  //  if tag = 0 then dmbk.tblEtiketaStavkaKOLICINA_BROJ.AsString := def;
                    if (def <> '') and (tag = 0) then

                      dmBK.tblEtiketaStavkaBARKOD.Value := '(01)' + dmBK.tblIzberiArtikalBARKOD.AsString +
                                                       //'(15)' + copy(inttostr(yearof(dmBK.tblIzberiArtikalDATUM_DO.Value)),3,2) + Copy(('0' + IntToStr(MonthOf(dmBK.tblIzberiArtikalDATUM_DO.Value))),Length(IntToStr(MonthOf(dmBK.tblIzberiArtikalDATUM_DO.Value))),2) +
                                                       //         Copy(('0' + IntToStr(DayOf(dmBK.tblIzberiArtikalDATUM_DO.Value))),Length(IntToStr(DayOf(dmBK.tblIzberiArtikalDATUM_DO.Value))),2) +
                                                      // '(13)' + copy(inttostr(yearof(dmBK.tblIzberiArtikalDATUM_PAK.Value)),3,2) + Copy(('0' + IntToStr(MonthOf(dmBK.tblIzberiArtikalDATUM_PAK.Value))),Length(IntToStr(MonthOf(dmBK.tblIzberiArtikalDATUM_PAK.Value))),2) +
                                                       //         Copy(('0' + IntToStr(DayOf(dmBK.tblIzberiArtikalDATUM_PAK.Value))),Length(IntToStr(DayOf(dmBK.tblIzberiArtikalDATUM_PAK.Value))),2) +
                                                       '(10)' + dmBK.tblIzberiArtikalLOT.Value +
                                                       //'(37)' + dmBK.tblIzberiArtikalKOLICINA_BROJ.AsString;

                                                       '(37)' +floattostr( strtofloat(def))
                   else
                   if (def = '') and (tag = 0) then

                     dmBK.tblEtiketaStavkaBARKOD.Value := '(01)' + dmBK.tblIzberiArtikalBARKOD.AsString +
                                                       //'(15)' + copy(inttostr(yearof(dmBK.tblIzberiArtikalDATUM_DO.Value)),3,2) + Copy(('0' + IntToStr(MonthOf(dmBK.tblIzberiArtikalDATUM_DO.Value))),Length(IntToStr(MonthOf(dmBK.tblIzberiArtikalDATUM_DO.Value))),2) +
                                                       //         Copy(('0' + IntToStr(DayOf(dmBK.tblIzberiArtikalDATUM_DO.Value))),Length(IntToStr(DayOf(dmBK.tblIzberiArtikalDATUM_DO.Value))),2) +
                                                      // '(13)' + copy(inttostr(yearof(dmBK.tblIzberiArtikalDATUM_PAK.Value)),3,2) + Copy(('0' + IntToStr(MonthOf(dmBK.tblIzberiArtikalDATUM_PAK.Value))),Length(IntToStr(MonthOf(dmBK.tblIzberiArtikalDATUM_PAK.Value))),2) +
                                                       //         Copy(('0' + IntToStr(DayOf(dmBK.tblIzberiArtikalDATUM_PAK.Value))),Length(IntToStr(DayOf(dmBK.tblIzberiArtikalDATUM_PAK.Value))),2) +
                                                       '(10)' + dmBK.tblIzberiArtikalLOT.Value +
                                                       //'(37)' + dmBK.tblIzberiArtikalKOLICINA_BROJ.AsString;

                                                       '(37)' +'1';    //dmBK.tblIzberiArtikalKOLICINA_REALNA.Asstring;
//                   if tag = 1 then
//                     dmBK.tblEtiketaStavkaBARKOD.Value := '(01)' + dmBK.tblIzberiArtikalBARKOD.AsString +
//                                                       //'(15)' + copy(inttostr(yearof(dmBK.tblIzberiArtikalDATUM_DO.Value)),3,2) + Copy(('0' + IntToStr(MonthOf(dmBK.tblIzberiArtikalDATUM_DO.Value))),Length(IntToStr(MonthOf(dmBK.tblIzberiArtikalDATUM_DO.Value))),2) +
//                                                       //         Copy(('0' + IntToStr(DayOf(dmBK.tblIzberiArtikalDATUM_DO.Value))),Length(IntToStr(DayOf(dmBK.tblIzberiArtikalDATUM_DO.Value))),2) +
//                                                      // '(13)' + copy(inttostr(yearof(dmBK.tblIzberiArtikalDATUM_PAK.Value)),3,2) + Copy(('0' + IntToStr(MonthOf(dmBK.tblIzberiArtikalDATUM_PAK.Value))),Length(IntToStr(MonthOf(dmBK.tblIzberiArtikalDATUM_PAK.Value))),2) +
//                                                       //         Copy(('0' + IntToStr(DayOf(dmBK.tblIzberiArtikalDATUM_PAK.Value))),Length(IntToStr(DayOf(dmBK.tblIzberiArtikalDATUM_PAK.Value))),2) +
//                                                       '(10)' + dmBK.tblIzberiArtikalLOT.Value +
//                                                       //'(37)' + dmBK.tblIzberiArtikalKOLICINA_BROJ.AsString;
//
//                                                       '(37)' +txtKolicinaBroj.Text;
//
               end;
          ActiveControl := Zapisi2;
end;

procedure TfrmHomogena.aBarkodKExecute(Sender: TObject);
var broj:string;  i : integer;  def:AnsiString;
begin
   if ((txtArtikal.Text <> '') or (txtVidArtikal.Text <> '')) then
                begin
                   if tag in [1, 2] then
                   begin
                    dpanel2.Enabled := True;
                    cxGroupBox1.Enabled := True;
                   // aPresmetaj.Execute;
                    txtKolicinaBroj.SetFocus;
                    Abort;
                   end;

                   dmBK.tblEtiketaStavkaID_RNS.Value := dmBK.tblIzberiAID_RNS.Value;
                   if (txtPakuvanje.Text = '') and (tag in [1, 2]) then
                      dmBK.tblIzvedenaEM.Close
                   else
                   begin
                     dmBK.tblIzvedenaEM.Close;
                     dmBK.tblIzvedenaEM.ParamByName('artikal').Value := dmBK.tblIzberiAID.Value;
                     dmBK.tblIzvedenaEM.ParamByName('vid_artikal').Value := dmBK.tblIzberiAARTVID.Value;
                     dmBK.tblIzvedenaEM.open;
                   end;
                   dmBK.tblEtiketaStavkaGTIN.Value := dmBK.tblIzberiABARKOD.Value;
                   dmBK.tblEtiketaStavkaLOT.Value := dmBK.tblIzberiALOT.Value;

                   dmBK.MaxEtiketaStavka.Close;
                   dmBK.MaxEtiketaStavka.ExecQuery;

                   if not dmBK.tblIzvedenaEMKOEFICIENT.IsNull then
                     def := dmBK.tblIzvedenaEMKOEFICIENT.Value
                   else
                     def := '';

                   for I := 1 to (7-(Length(inttostr(dmBK.MaxEtiketaStavka.FldByName['maks'].AsInteger+1)))) do
                   begin
                    broj := broj+'0';
                   end;
                    broj := kompaniski_broj + broj + inttostr(dmBK.MaxEtiketaStavka.FldByName['maks'].AsInteger+1);
                    dmBK.Kontrolen_broj(broj);
                    dmBK.tblEtiketaStavkaSSCC.Value := '(00)' + broj + inttostr(kb);
                  //  if tag = 0 then dmbk.tblEtiketaStavkaKOLICINA_BROJ.AsString := def;
                    if (def <> '') and (tag = 0) then

                      dmBK.tblEtiketaStavkaBARKOD.Value := '(01)' + dmBK.tblIzberiABARKOD.AsString +
                                                       '(10)' + dmBK.tblIzberiALOT.Value +

                                                       '(37)' +floattostr( strtofloat(def))
                   else
                   if (def = '') and (tag = 0) then

                     dmBK.tblEtiketaStavkaBARKOD.Value := '(01)' + dmBK.tblIzberiABARKOD.AsString +
                                                       '(10)' + dmBK.tblIzberiALOT.Value +

                                                       '(37)' +'1';    //dmBK.tblIzberiArtikalKOLICINA_REALNA.Asstring;
               end;
          ActiveControl := Zapisi2;
end;

procedure TfrmHomogena.aDizajnerExecute(Sender: TObject);
begin
if not (dmBK.tblEtiketa.State in [dsEdit,dsInsert]) and (dmBK.tblEtiketaStavka.State = dsBrowse) then
 begin
  dmRes.Spremi('MAN', 2);
  dmKon.tblSqlReport.ParamByName('id').AsInteger :=dmBK.tblEtiketaID.Value;;
  dmKon.tblSqlReport.Open;
  dmRes.frxReport1.DesignReport();
 end;
end;

//  ����� �� ����� �� ����� �� ������������� �� �������� � ��������������� ��
//  �������(������ ���������� ��������)
procedure TfrmHomogena.aFormConfigExecute(Sender: TObject);
begin
  frmFormConfig := TfrmFormConfig.Create(Application);
  frmFormConfig.formPtr := Addr(Self);
  frmFormConfig.ShowModal;
  frmFormConfig.Free;
end;

//----------------------------------------------------------------------------------
// ���� -> ��������� �� ��������, ���������� �� ��������� � �������� �� Print ��������
//----------------------------------------------------------------------------------

// ��������� �� ���������� �� ��������� � �������� �� Print �������� �� .ini ��������
procedure TfrmHomogena.zacuvajPrintVoIni(ime:AnsiString);
var
  AStream: TFileStream;
begin
  AStream := TFileStream.Create(dmKon.RabotenDir + ime + 'Print.ini', fmCreate);
  try
    AStream.WriteComponent(dxComponentPrinter1Link1);
  finally
    AStream.Free;
  end;

end;

// ��������� �� ������ �� ��������� � �������� �� Print �������� �������� �� .ini ��������
procedure TfrmHomogena.procitajPrintOdIni(ime:AnsiString);
var
  AStream: TFileStream;
begin
  AStream := TFileStream.Create(dmKon.RabotenDir + ime + 'Print.ini', fmOpenRead);
  try
    dxComponentPrinter1Link1.RestoreDefaults;
    AStream.ReadComponent(dxComponentPrinter1Link1);
  finally
    AStream.Free;
  end;
end;

procedure TfrmHomogena.SetirajLukap(sender:TObject; tabela:TDataSet; tip:TcxDBTextEdit; sifra:TcxDBTextEdit; lukap:TcxLookupComboBox);
begin
     if (tip.Text <>'') and (sifra.Text<>'')  then
         begin
          if(tabela.Locate('ARTVID;ID',VarArrayOf([tip.text,sifra.text]),[])) then
               lukap.Text:=tabela.FieldByName('NAZIV').Value;
         end

      //   lukap.EditValue := VarArrayOf([StrToInt(tip.text), StrToInt(sifra.Text)]);
      // end
       else
         lukap.Clear;
end;

procedure WriteThread.Execute;
begin
 while not Terminated do
   begin
   if not (dmBK.tblEtiketa.State in [dsEdit,dsInsert]) and (dmBK.tblEtiketaStavka.State = dsBrowse) then
    begin
      dmRes.Spremi('MAN', 2);
      dmKon.tblSqlReport.ParamByName('id').AsInteger :=dmBK.tblEtiketaID.Value;;
      dmKon.tblSqlReport.Open;
      dmRes.frxReport1.PrintOptions.ShowDialog := false;
    //  dmres.frxReport1.PrepareReport(true);
      dmRes.frxReport1.ShowReport;
        // inc(i);
        // form1.Memo1.Lines.Add(inttostr(i));
       Synchronize(DoSomething);
       end;
   end;
end;

procedure WriteThread.DoSomething;
begin
 if pojavi = 1 then
begin
   writememo.Terminate;
   pojavi := 0;
end
end;

//procedure TfrmHomogena.Button1Click(Sender: TObject);
//begin
//writememo:=WriteThread.Create;
//end;
//
//procedure TfrmHomogena.Button2Click(Sender: TObject);
//begin
//writememo.Terminate;
//end;


end.
