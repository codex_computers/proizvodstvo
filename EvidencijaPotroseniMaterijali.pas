unit EvidencijaPotroseniMaterijali;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData, cxEdit,
  DB, cxDBData, cxGridCustomPopupMenu, cxGridPopupMenu,
  ComCtrls, cxContainer, cxTextEdit, cxDBEdit, StdCtrls, cxGridLevel,
  cxClasses, cxControls, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGrid, ExtCtrls, Menus, ActnList,
  cxGridExportLink, cxExport, cxLookAndFeelPainters, cxDataStorage,
  cxButtons, dxStatusBar, dxRibbonStatusBar, dxRibbon, dxBar, dxBarExtItems,
  dxPSGlbl, dxPSUtl, dxPSEngn, dxPrnPg, dxBkgnd, dxWrap, dxPrnDev,
  dxPSCompsProvider, dxPSFillPatterns, dxPSEdgePatterns, dxPSCore, dxPScxCommon,
  cxPC, cxMemo, cxLocalization, cxLookAndFeels,
  dxPSPDFExportCore, dxPSPDFExport, cxDrawTextUtils, dxPSPrVwStd, dxPSPrVwAdv,
  dxPSPrVwRibbon, dxPScxEditorProducers, dxPScxExtEditorProducers,
  dxPScxPageControlProducer, dxPgsDlg, cxCheckBox, cxBarEditItem, dxSkinsCore,
  dxSkinBlack, dxSkinBlue, dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom,
  dxSkinDarkSide, dxSkinFoggy, dxSkinGlassOceans, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven,
  dxSkinSharp, dxSkinSilver, dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008,
  dxSkinsDefaultPainters, dxSkinValentine, dxSkinXmas2008Blue,
  dxSkinsdxRibbonPainter, dxSkinsdxBarPainter, dxSkinscxPCPainter,
  dxBarSkinnedCustForm, cxImageComboBox, cxGroupBox, cxRadioGroup,
  dxRibbonSkins, cxPCdxBarPopupMenu, dxPScxGridLnk, dxPScxGridLayoutViewLnk,
  dxScreenTip, dxSkinBlueprint, dxSkinDevExpressDarkStyle,
  dxSkinDevExpressStyle, dxSkinHighContrast, dxSkinSevenClassic,
  dxSkinSharpPlus, dxSkinTheAsphaltWorld, dxSkinVS2010, dxSkinWhiteprint,
  cxMaskEdit, cxDropDownEdit, cxLookupEdit, cxDBLookupEdit,
  cxDBExtLookupComboBox, dmResources, cxDBLookupComboBox, cxCalendar,
  FIBDataSet, pFIBDataSet, FIBQuery, pFIBQuery, pFIBStoredProc, cxLabel, Dateutils,
  cxDBLabel, cxButtonEdit, dxSkinOffice2013White, cxNavigator, System.Actions,
  dxSkinMetropolis, dxSkinMetropolisDark, dxSkinOffice2013DarkGray,
  dxSkinOffice2013LightGray, dxSkinOffice2016Colorful, dxSkinOffice2016Dark,
  dxSkinVisualStudio2013Blue, dxSkinVisualStudio2013Dark,
  dxSkinVisualStudio2013Light, dxRibbonCustomizationForm, dxBarBuiltInMenu;

type
//  niza = Array[1..5] of Variant;

  TfrmEvidencijaPotroseniMaterijali = class(TForm)
    cxGridPopupMenu1: TcxGridPopupMenu;
    PopupMenu1: TPopupMenu;
    N1: TMenuItem;
    Excel1: TMenuItem;
    dxRibbon1Tab1: TdxRibbonTab;
    dxRibbon1: TdxRibbon;
    StatusBar1: TdxRibbonStatusBar;
    dxBarManager1: TdxBarManager;
    dxBarManager1Bar1: TdxBar;
    dxBarManager1Bar2: TdxBar;
    dxBarManager1Bar3: TdxBar;
    dxBarLargeButton2: TdxBarLargeButton;
    dxBarLargeButton3: TdxBarLargeButton;
    dxBarLargeButton4: TdxBarLargeButton;
    dxBarLargeButton5: TdxBarLargeButton;
    dxBarLargeButton6: TdxBarLargeButton;
    dxBarLargeButton7: TdxBarLargeButton;
    dxBarLargeButton8: TdxBarLargeButton;
    dxBarLargeButton1: TdxBarLargeButton;
    ActionList1: TActionList;
    aNov: TAction;
    aAzuriraj: TAction;
    aBrisi: TAction;
    aSnimiIzgled: TAction;
    aZacuvajExcel: TAction;
    aPecatiTabela: TAction;
    aHelp: TAction;
    aIzlez: TAction;
    aZapisi: TAction;
    aOtkazi: TAction;
    aRefresh: TAction;
    dxRibbon1Tab2: TdxRibbonTab;
    dxBarManager1Bar4: TdxBar;
    dxBarLargeButton9: TdxBarLargeButton;
    aSnimiPecatenje: TAction;
    dxBarLargeButton11: TdxBarLargeButton;
    ribbonFilterReset: TdxBarLargeButton;
    aPodesuvanjePecatenje: TAction;
    dxBarLargeButton12: TdxBarLargeButton;
    aBrisiPodesuvanjePecatenje: TAction;
    dxBarLargeButton13: TdxBarLargeButton;
    dxBarLargeButton14: TdxBarLargeButton;
    aPageSetup: TAction;
    dxBarLargeButton15: TdxBarLargeButton;
    dxBarLargeButton16: TdxBarLargeButton;
    dxBarLargeButton17: TdxBarLargeButton;
    cxBarEditItem1: TcxBarEditItem;
    dxBarSubItem1: TdxBarSubItem;
    aFormConfig: TAction;
    dxBarScreenTipRepository1: TdxBarScreenTipRepository;
    tipSnimiIzgled: TdxBarScreenTip;
    tipZacuvajExcel: TdxBarScreenTip;
    tipDizajnReport: TdxBarScreenTip;
    tipSetiranjeStrana: TdxBarScreenTip;
    tipSnimiKonf: TdxBarScreenTip;
    tipBrisiKonf: TdxBarScreenTip;
    dPanelKol: TPanel;
    lPanel: TPanel;
    cxPageControlMenu: TcxPageControl;
    cxTabSheetKolicinski: TcxTabSheet;
    Label1: TLabel;
    ArtVid: TcxDBTextEdit;
    OtkaziButton: TcxButton;
    ZapisiButtonKol: TcxButton;
    dxComponentPrinter1: TdxComponentPrinter;
    dxComponentPrinter1Link1: TdxGridReportLink;
    dxBarManager1BarSnimiIzgled: TdxBar;
    dxBarLBtnSnimiIzgled: TdxBarLargeButton;
    dxBarLBtnBrisiIzgled: TdxBarLargeButton;
    aBrisiIzgled: TAction;
    ArtSifra: TcxDBTextEdit;
    cxExtArtikal: TcxExtLookupComboBox;
    aHideFields: TAction;
    cxTabSheetCustom: TcxTabSheet;
    dPanelCustom: TPanel;
    lblCustom1: TLabel;
    lblCustom2: TLabel;
    lblCustom3: TLabel;
    txtCustom1: TcxDBTextEdit;
    txtCustom2: TcxDBTextEdit;
    txtCustom3: TcxDBTextEdit;
    ZapisiButtonCustom: TcxButton;
    cxButton4: TcxButton;
    dxBarLargeButton10: TdxBarLargeButton;
    aCustomFieldsForm: TAction;
    dxBarManager1Bar5: TdxBar;
    dxBarLargeButton18: TdxBarLargeButton;
    aResetIzvedenaEM: TAction;
    dxBarLargeButton19: TdxBarLargeButton;
    dxBarLargeButton20: TdxBarLargeButton;
    dxBarLargeButton21: TdxBarLargeButton;
    dxBarLargeButton22: TdxBarLargeButton;
    aNovaEvidencija: TAction;
    aBrisiEvidencija: TAction;
    aBarajEvidencija: TAction;
    dxBarLargeButton23: TdxBarLargeButton;
    aMagacin: TAction;
    aResetNurkovci: TAction;
    aKreiraj: TAction;
    gbMerkaKolicina: TcxGroupBox;
    Label9: TLabel;
    Label10: TLabel;
    Label11: TLabel;
    Label8: TLabel;
    Paketi: TcxDBTextEdit;
    BrPaket: TcxDBTextEdit;
    lcbIzvedena: TcxDBLookupComboBox;
    Kolicina: TcxDBTextEdit;
    Label2: TLabel;
    Opis: TcxDBTextEdit;
    dxBarLargeButton24: TdxBarLargeButton;
    aOtvoriSerii: TAction;
    qryArtikalInfo: TpFIBQuery;
    aZemiArtikalInfo: TAction;
    lblMerka: TcxLabel;
    dsIzvedenaEM: TDataSource;
    tblIzvedenaEM: TpFIBDataSet;
    tblIzvedenaEMID: TFIBIntegerField;
    tblIzvedenaEMARTVID: TFIBIntegerField;
    tblIzvedenaEMARTSIF: TFIBIntegerField;
    tblIzvedenaEMIZVEDENA_EM: TFIBStringField;
    tblIzvedenaEMMERKA_NAZIV: TFIBStringField;
    tblIzvedenaEMKOEFICIENT: TFIBBCDField;
    tblIzvedenaEMDEFAULT_IZVEDENA_EM: TFIBIntegerField;
    aUpstream: TAction;
    dxBarLargeButton25: TdxBarLargeButton;
    aDesigner: TAction;
    aSurovinaOdN: TAction;
    dxBarLargeButton26: TdxBarLargeButton;
    Panel1: TPanel;
    cxGrid2: TcxGrid;
    cxGridDBTableView1: TcxGridDBTableView;
    cxGrid1DBTableView1VID_ARTIKAL_N: TcxGridDBColumn;
    cxGrid1DBTableView1ARTIKAL_N: TcxGridDBColumn;
    cxGrid1DBTableView1NAZIV_SUROVINA_N: TcxGridDBColumn;
    cxGrid1DBTableView1KOLICINA_N: TcxGridDBColumn;
    cxGrid1DBTableView1MERKA_N: TcxGridDBColumn;
    cxGrid1DBTableView2: TcxGridDBTableView;
    cxGrid1DBTableView2DBColumn: TcxGridDBColumn;
    cxGrid1DBTableView2DBColumn1: TcxGridDBColumn;
    cxGrid1DBTableView2ID_PORACKA: TcxGridDBColumn;
    cxGrid1DBTableView2DBColumn2: TcxGridDBColumn;
    cxGrid1DBTableView2DBColumn3: TcxGridDBColumn;
    cxGrid1DBTableView2DBColumn4: TcxGridDBColumn;
    cxGrid1DBTableView2DBColumn5: TcxGridDBColumn;
    cxGrid1DBTableView2DBColumn6: TcxGridDBColumn;
    cxGrid1DBTableView2DBColumn7: TcxGridDBColumn;
    cxGrid1DBTableView2DBColumn8: TcxGridDBColumn;
    cxGrid1DBTableView2DBColumn9: TcxGridDBColumn;
    cxGrid1DBTableView2LABELS: TcxGridDBColumn;
    cxGrid1DBTableView2GLIDERS: TcxGridDBColumn;
    cxGrid1DBTableView2DBColumn10: TcxGridDBColumn;
    cxGrid1DBTableView2DBColumn11: TcxGridDBColumn;
    cxGrid1DBTableView2DBColumn12: TcxGridDBColumn;
    cxGrid1DBTableView2DBColumn13: TcxGridDBColumn;
    cxGrid1DBTableView2STATUS: TcxGridDBColumn;
    cxGrid1DBTableView2Column1: TcxGridDBColumn;
    cxGridLevel1: TcxGridLevel;
    aVnesiOdNor: TAction;
    //N1: TMenuItem;
    cxGridDBTableView1VNESI: TcxGridDBColumn;
    Action1: TAction;
    dsOutStavka: TDataSource;
    tblOutStavka: TpFIBDataSet;
    tblOutStavkaID: TFIBBCDField;
    tblOutStavkaMTR_OUT_ID: TFIBBCDField;
    tblOutStavkaART_VID: TFIBIntegerField;
    tblOutStavkaART_SIF: TFIBIntegerField;
    tblOutStavkaART_NAZIV: TFIBStringField;
    tblOutStavkaKOLICINA: TFIBFloatField;
    tblOutStavkaPAKETI: TFIBIntegerField;
    tblOutStavkaIZV_EM: TFIBIntegerField;
    tblOutStavkaIZVEDENA_EM: TFIBStringField;
    tblOutStavkaBR_PAKET: TFIBBCDField;
    tblOutStavkaCENA_SO_DDV: TFIBFloatField;
    tblOutStavkaCENA_BEZ_DDV: TFIBFloatField;
    tblOutStavkaDANOK_PR: TFIBBCDField;
    tblOutStavkaRABAT_PR: TFIBBCDField;
    tblOutStavkaKNIG_CENA: TFIBFloatField;
    tblOutStavkaKNIG_IZNOS: TFIBBCDField;
    tblOutStavkaIZNOS_BEZ_DDV: TFIBBCDField;
    tblOutStavkaIZNOS_SO_DDV: TFIBBCDField;
    tblOutStavkaRABAT_IZNOS: TFIBBCDField;
    tblOutStavkaDANOK_IZNOS: TFIBBCDField;
    tblOutStavkaLOT_NO: TFIBStringField;
    tblOutStavkaVAZI_DO: TFIBDateField;
    tblOutStavkaTS_INS: TFIBDateTimeField;
    tblOutStavkaTS_UPD: TFIBDateTimeField;
    tblOutStavkaUSR_INS: TFIBStringField;
    tblOutStavkaUSR_UPD: TFIBStringField;
    tblOutStavkaCUSTOM1: TFIBStringField;
    tblOutStavkaCUSTOM2: TFIBStringField;
    tblOutStavkaCUSTOM3: TFIBStringField;
    tblOutStavkaREF_ID: TFIBBCDField;
    tblOutStavkaOPIS: TFIBStringField;
    tblOutStavkaART_MERKA: TFIBStringField;
    tblOutStavkaART_MERKA_KOL: TFIBStringField;
    //aZemiArtikalInfo: TAction;
    tblOutStavkaEBROJ: TFIBIntegerField;
    tblOutStavkaEGOD: TFIBIntegerField;
    tblOutStavkaRN_BROJ_GOD: TFIBStringField;
    tblOutStavkaSLEDLIVOST_KOLICINA: TFIBSmallIntField;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1DBTableView1ID: TcxGridDBColumn;
    cxGrid1DBTableView1SLEDLIVOST_KOLICINA: TcxGridDBColumn;
    cxGrid1DBTableView1MTR_OUT_ID: TcxGridDBColumn;
    cxGrid1DBTableView1ART_VID: TcxGridDBColumn;
    cxGrid1DBTableView1ART_SIF: TcxGridDBColumn;
    cxGrid1DBTableView1ART_NAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1KOLICINA: TcxGridDBColumn;
    cxGrid1DBTableView1IZV_EM: TcxGridDBColumn;
    cxGrid1DBTableView1IZVEDENA_EM: TcxGridDBColumn;
    cxGrid1DBTableView1PAKETI: TcxGridDBColumn;
    cxGrid1DBTableView1BR_PAKET: TcxGridDBColumn;
    cxGrid1DBTableView1CENA_SO_DDV: TcxGridDBColumn;
    cxGrid1DBTableView1CENA_BEZ_DDV: TcxGridDBColumn;
    cxGrid1DBTableView1DANOK_PR: TcxGridDBColumn;
    cxGrid1DBTableView1RABAT_PR: TcxGridDBColumn;
    cxGrid1DBTableView1DANOK_IZNOS: TcxGridDBColumn;
    cxGrid1DBTableView1RABAT_IZNOS: TcxGridDBColumn;
    cxGrid1DBTableView1IZNOS_SO_DDV: TcxGridDBColumn;
    cxGrid1DBTableView1IZNOS_BEZ_DDV: TcxGridDBColumn;
    cxGrid1DBTableView1KNIG_CENA: TcxGridDBColumn;
    cxGrid1DBTableView1KNIG_IZNOS: TcxGridDBColumn;
    cxGrid1DBTableView1LOT_NO: TcxGridDBColumn;
    cxGrid1DBTableView1VAZI_DO: TcxGridDBColumn;
    cxGrid1DBTableView1TS_INS: TcxGridDBColumn;
    cxGrid1DBTableView1TS_UPD: TcxGridDBColumn;
    cxGrid1DBTableView1USR_INS: TcxGridDBColumn;
    cxGrid1DBTableView1USR_UPD: TcxGridDBColumn;
    cxGrid1DBTableView1CUSTOM1: TcxGridDBColumn;
    cxGrid1DBTableView1CUSTOM2: TcxGridDBColumn;
    cxGrid1DBTableView1CUSTOM3: TcxGridDBColumn;
    cxGrid1DBTableView1REF_ID: TcxGridDBColumn;
    cxGrid1DBTableView1OPIS: TcxGridDBColumn;
    cxGrid1DBTableView1EBROJ: TcxGridDBColumn;
    cxGrid1DBTableView1EGOD: TcxGridDBColumn;
    cxGrid1DBTableView1RN_BROJ_GOD: TcxGridDBColumn;
    cxGrid1Level1: TcxGridLevel;
    tblOutStavkaLOT_RNS: TFIBStringField;
    tblOutStavkaNAZIV_PROIZVOD: TFIBStringField;
    cxGridDBTableView1RASTUR: TcxGridDBColumn;
    cxGroupBox1: TcxGroupBox;
    lbl1: TLabel;
    Lager: TcxDBTextEdit;
    cxButton1: TcxButton;
    dsLagerArtikal: TDataSource;
    tblLagerArtikal: TpFIBDataSet;
    tblLagerArtikalO_IZNOS_VLEZ: TFIBBCDField;
    tblLagerArtikalO_IZNOS_IZLEZ: TFIBBCDField;
    tblLagerArtikalO_IZNOS_DDV_VLEZ: TFIBBCDField;
    tblLagerArtikalO_IZNOS_DDV_IZLEZ: TFIBBCDField;
    tblLagerArtikalO_VLEZ: TFIBFloatField;
    tblLagerArtikalO_VLEZ_NEKONTROLIRAN: TFIBFloatField;
    tblLagerArtikalO_IZLEZ: TFIBFloatField;
    tblLagerArtikalO_LAGER: TFIBFloatField;
    tblLagerArtikalART_DDV: TFIBFloatField;
    tblLagerArtikalART_MERKA: TFIBStringField;
    tblLagerArtikalART_ZALIHA: TFIBIntegerField;
    aLikvidiranaEvidencija: TAction;
    aZemiMerka: TAction;

    procedure cxDBTextEditAllEnter(Sender: TObject);
    procedure cxDBTextEditAllExit(Sender: TObject);
    procedure EnterKakoTab(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    //procedure PrvPosledenTab(panel:TPanel;var posledna,prva:TWinControl);
    procedure SaveToIniFileExecute(Sender: TObject);
    procedure cxGrid1DBTableView1KeyPress(Sender: TObject; var Key: Char);
    procedure aZapisiExecute(Sender: TObject);
    procedure aOtkaziExecute(Sender: TObject);
    procedure aNovExecute(Sender: TObject);
    procedure aAzurirajExecute(Sender: TObject);
    procedure aBrisiExecute(Sender: TObject);
    procedure aZacuvajExcelExecute(Sender: TObject);
    procedure aRefreshExecute(Sender: TObject);
    procedure aSnimiIzgledExecute(Sender: TObject);
    procedure aIzlezExecute(Sender: TObject);
    procedure aPecatiTabelaExecute(Sender: TObject);
    procedure aSnimiPecatenjeExecute(Sender: TObject);
    procedure zacuvajPrintVoIni(ime:AnsiString);
    procedure procitajPrintOdIni(ime:AnsiString);
    procedure aPodesuvanjePecatenjeExecute(Sender: TObject);
    procedure aBrisiPodesuvanjePecatenjeExecute(Sender: TObject);
    procedure aPageSetupExecute(Sender: TObject);
    procedure cxGrid1DBTableView1FilterCustomization(
      Sender: TcxCustomGridTableView; var ADone: Boolean);
    procedure aFormConfigExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure cxGrid1DBTableView1DblClick(Sender: TObject);
    procedure cxGrid1DBTableView1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure cxPageControlMenuChange(Sender: TObject);
    procedure aBrisiIzgledExecute(Sender: TObject);
    procedure SpremiModNaRabota();
    procedure aHideFieldsExecute(Sender: TObject);
    procedure CustomFields();
    procedure aCustomFieldsFormExecute(Sender: TObject);
    procedure ArtSifraPropertiesChange(Sender: TObject);
    procedure ArtSifraSPropertiesChange(Sender: TObject);
    procedure NajdiArtikalVoCenovnik();
    procedure PresmetajNaIzlez(Sender: TObject);
    procedure CenaBezDDVPropertiesValidate(Sender: TObject;
      var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
    procedure lcbIzvedenaExit(Sender: TObject);
    procedure aMagacinExecute(Sender: TObject);
    procedure aResetNurkovciExecute(Sender: TObject);
    procedure cxExtArtikalExit(Sender: TObject);
    procedure SmeniNazivArtikal();
    procedure FormResize(Sender: TObject);
    procedure lcbIzvedenaEnter(Sender: TObject);
    procedure aResetIzvedenaEMExecute(Sender: TObject);
    procedure TabelaBeforeDelete(DataSet: TDataSet);
    procedure aOtvoriSeriiExecute(Sender: TObject);
    procedure aZemiArtikalInfoExecute(Sender: TObject);
    procedure aZemiMerkaExecute(Sender: TObject);
    procedure ArtSifraPropertiesEditValueChanged(Sender: TObject);
    procedure aUpstreamExecute(Sender: TObject);
    procedure aDesignerExecute(Sender: TObject);
    procedure aSurovinaOdNExecute(Sender: TObject);
    procedure aVnesiOdNorExecute(Sender: TObject);
 procedure aLikvidiranaEvidencijaExecute(Sender: TObject);
  private
    { Private declarations }

    _sifra_kluc :array[1..10] of string;  // �������� ���������� �� ������ �� ������� �� ������������� ���

  protected
    list : TList;
    inserting, zapisi_validacija : boolean;

    mod_rabota : integer;

    activPanelD :TPanel;
    activZapisi : TcxButton;
    custom1,custom2,custom3 : AnsiString;

    prva, posledna :TWinControl;
    procedure prefrli;

  public
    { Public declarations }
    evidencija_id : Int64;
    e_br, e_god, od_nor : integer;
    //evidencija_id : Int64;
   // e_br, e_god : integer;
    e_datum : TDateTime;
    art_kol : double;
    e_likvidirana : integer;
    art_lot_kol : double;

    constructor Create(Owner : TComponent; insert : boolean = true; mod_na_rabota : integer = -1);reintroduce; overload;

    procedure SetSifra(br: integer; s : string);
    function GetSifra(br: integer) : string;

    //������� �� ���������� ������������ _sifra_kluc
    property sifra_kluc[br :integer]: string read GetSifra write SetSifra;
  end;

var
  frmEvidencijaPotroseniMaterijali: TfrmEvidencijaPotroseniMaterijali;
  rData : TRepositoryData;

implementation

uses DaNe, dmKonekcija, Utils, FormConfig, CustomFields, NurkoRepository, Magacin,
  dmMaticni, dmUnit, EvidencijaSerijaKolicini, UpotrebeniSerii, PogledNormativ,
  OutLots;

{$R *.dfm}
//------------------------------------------------------------------------------

procedure TfrmEvidencijaPotroseniMaterijali.CenaBezDDVPropertiesValidate(Sender: TObject;
  var DisplayValue: Variant; var ErrorText: TCaption; var Error: Boolean);
begin
  IznosPolePropertiesValidate(Sender,DisplayValue,ErrorText,Error);
end;

constructor TfrmEvidencijaPotroseniMaterijali.Create(Owner : TComponent; insert : boolean = true; mod_na_rabota : integer = -1);
begin
inherited Create(Owner);
//	�������� ���� ������� � �������� �� ����������� ���������
//	insert = true -> ����� ���� ����� ���������� �� ������� �� Insert Mode
//  insert = false -> ����� ���� ����� ������� �� ����� �� ������. �� ������� ����� ����� �� �� �������� F5
  inserting := insert;

   //mod_rabota go opredeluva modot na rabota na formata (0 - Kolicinski, 1 - So ceni, 2 - Finansiski)
  mod_rabota := mod_na_rabota;
end;

//	����� �� �����������. �� ��������� ������� �� ������, �� �������� ������� �� ���� ��������,
//	�� ������� ������� �� ������ ������ � �� ������� �� Insert Mode
procedure TfrmEvidencijaPotroseniMaterijali.aNovExecute(Sender: TObject);
begin
  if(cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) then
  begin
    activPanelD.Enabled:=True;
    dPanelCustom.Enabled:=true;
    lPanel.Enabled:=False;
    lPanel.Enabled:=False;
    prva.SetFocus;
    cxGrid1DBTableView1.DataController.DataSet.Insert;

    tblOutStavkaMTR_OUT_ID.Value := evidencija_id;
    if od_nor >= 1 then
    begin
      //od_nor := 0;
      tblOutStavkaART_VID.Value := dm.tblPogledNormativVID_ARTIKAL_N.Value;
      tblOutStavkaART_SIF.Value := dm.tblPogledNormativARTIKAL_N.Value;
      if mod_rabota = 1 then
         tblOutStavkaKOLICINA.Value := dm.tblPogledNormativRASTUR.Value
      else
         tblOutStavkaKOLICINA.Value := dm.tblPogledNormativKOLICINA_N.Value;
      Kolicina.SetFocus;
     // Abort;
    end;

  end
  else ShowMessage('������ �������� �� ��� �������� �� �� ��������� �������,' + sLineBreak + '� ����� ������� �� ������� ��� �����!');
end;

//	����� �� ���������. �� ��������� ������� �� ������, �� �������� ������� �� ���� ��������,
//	�� ������� ������� �� ������ ������ � �� ������� �� Edit Mode
procedure TfrmEvidencijaPotroseniMaterijali.aAzurirajExecute(Sender: TObject);
begin
  if(cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) then
  begin
    activPanelD.Enabled:=True;
    dPanelCustom.Enabled:=true;
    lPanel.Enabled:=False;
    lPanel.Enabled:=False;
    prva.SetFocus;
    cxGrid1DBTableView1.DataController.DataSet.Edit;
    art_kol := tblOutStavkaKOLICINA.Value;

    if (dmMat.tblReMagacinSLEDLIVOST.Value = 1) then
    begin
      dm.qryPovrzaniOutLots.Close;
      dm.qryPovrzaniOutLots.ParamByName('OUT_S_ID').Value := tblOutStavkaID.Value;
      dm.qryPovrzaniOutLots.ExecQuery;
      art_lot_kol := dm.qryPovrzaniOutLots.FieldValue('LOT_KOLICINA', false);
    end
    else art_lot_kol := 0;
  end
  else ShowMessage('������ �������� �� ��� �������� �� �� ��������� �������,' + sLineBreak + '� ����� ������� �� ������� ��� �����!');
end;

//	����� �� ������ �� ������������� �����
procedure TfrmEvidencijaPotroseniMaterijali.aBrisiExecute(Sender: TObject);
begin
  if ((cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) and (cxGrid1DBTableView1.DataController.RecordCount > 0)) then
    cxGrid1DBTableView1.DataController.DataSet.Delete();

    dm.tblPogledNormativ.Close;
    dm.tblPogledNormativ.ParamByName('id').Value := evidencija_id;
    dm.tblPogledNormativ.Open;

end;

procedure TfrmEvidencijaPotroseniMaterijali.aBrisiIzgledExecute(Sender: TObject);
begin
  brisiGridVoBaza(Name,cxGrid1DBTableView1);
  BrisiFormaIzgled(self);
end;

//	����� �� ���������� �� ����������
procedure TfrmEvidencijaPotroseniMaterijali.aRefreshExecute(Sender: TObject);
begin
  cxGrid1DBTableView1.DataController.Filter.Root.Clear; //���������� �� ��������
  //cxGrid1DBTableView1.DataController.DataSet.Refresh;
  aResetNurkovci.Execute;
  tblOutStavka.FullRefresh;
end;

procedure TfrmEvidencijaPotroseniMaterijali.TabelaBeforeDelete(DataSet: TDataSet);
begin
    frmDaNe := TfrmDaNe.Create(self, '������ �� �����', '���� ��������� ������ �� �� ��������� �������?', 1);
    if (frmDaNe.ShowModal <> mrYes) then
        Abort;
end;

procedure TfrmEvidencijaPotroseniMaterijali.aResetIzvedenaEMExecute(
  Sender: TObject);
begin
  tblIzvedenaEM.Close;
  tblIzvedenaEM.ParamByName('artvid').Clear;
  tblIzvedenaEM.ParamByName('artsif').Clear;
  tblIzvedenaEM.Open;
end;

procedure TfrmEvidencijaPotroseniMaterijali.aResetNurkovciExecute(
  Sender: TObject);
begin
  //�������� �� ���������� �� ������ ����� ������ ��������� �� ������� �� ��������� �� sql-��
   dmRes.FreeRepository(rData);
   rData := TRepositoryData.Create();

   dmRes.setParam(':MAGACIN.ID',IntToStr(dmMat.tblReMagacinID.Value));

   cxExtArtikal.RepositoryItem := dmRes.InitRepositoryRefresh(-203, 'cxExtArtikal', Name, rData);
end;

procedure TfrmEvidencijaPotroseniMaterijali.ArtSifraPropertiesChange(Sender: TObject);
begin
  //if (mod_rabota = 0) then
  begin
    // smeni go nazivot na ext lookup-ot
    if (tblOutStavka.State in [dsInsert,dsEdit]) then
    begin
      if ((ArtVid.Text <> '') and (ArtSifra.Text <> '') and (IsStrANumber(ArtVid.Text)) and (IsStrANumber(ArtSifra.Text)))  then
      begin
        cxExtArtikal.EditValue := VarArrayOf([StrToInt(ArtVid.Text), StrToInt(ArtSifra.Text)]);
        //aZemiArtikalInfo.Execute;
      end
      else
      begin
        cxExtArtikal.Clear;
        lblMerka.Caption := '';
      end;

      tblOutStavkaIZV_EM.Clear;
    end
    else
    begin
      if (tblOutStavka.State = dsInactive) then cxExtArtikal.Clear
      else
      begin
        if ((not tblOutStavkaART_VID.IsNull) and (not tblOutStavkaART_SIF.isNull)) then
        begin
          cxExtArtikal.EditValue := VarArrayOf([tblOutStavkaART_VID.Value, tblOutStavkaART_SIF.Value]);
          //aZemiArtikalInfo.Execute;
        end
        else
        begin
          cxExtArtikal.Clear;
          lblMerka.Caption := '';
        end;
      end;
    end;
  end;
end;

procedure TfrmEvidencijaPotroseniMaterijali.ArtSifraPropertiesEditValueChanged(
  Sender: TObject);
begin
  aZemiMerka.Execute;
end;

procedure TfrmEvidencijaPotroseniMaterijali.ArtSifraSPropertiesChange(Sender: TObject);
begin

end;

//	����� �� ����� �� ������� �� ������� �� ������� �����
procedure TfrmEvidencijaPotroseniMaterijali.aIzlezExecute(Sender: TObject);
begin
  Close;
end;

procedure TfrmEvidencijaPotroseniMaterijali.aLikvidiranaEvidencijaExecute(
  Sender: TObject);
begin
  aNov.Enabled := false;
  aAzuriraj.Enabled := false;
  aBrisi.Enabled := false;
end;
procedure TfrmEvidencijaPotroseniMaterijali.aMagacinExecute(Sender: TObject);
begin
end;

//	����� �� ������� �� �������� �� ������ �� ���� (Utils.pas)
procedure TfrmEvidencijaPotroseniMaterijali.aSnimiIzgledExecute(Sender: TObject);
begin
  zacuvajGridVoBaza(Name,cxGrid1DBTableView1);
  ZacuvajFormaIzgled(self);
end;

//	����� �� ������� �� ������ �� Excel ������ (Utils.pas)
procedure TfrmEvidencijaPotroseniMaterijali.aZacuvajExcelExecute(Sender: TObject);
begin
  zacuvajVoExcel(cxGrid1, Caption);
end;

//  ��������� �� ������ �� ������� �� ���������� �� ����� �� Enter
procedure TfrmEvidencijaPotroseniMaterijali.EnterKakoTab(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  kom : TWinControl;
begin
    kom := Sender as TWinControl;
    case Key of
        VK_DOWN:
        begin
          PostMessage(Handle,WM_NextDlgCtl,0,0);
          key:=0;
        end;
        VK_UP:
        begin
          PostMessage(Handle,WM_NextDlgCtl,1,0);
          key:=0;
        end;
        VK_RETURN:
        begin
          PostMessage(Handle,WM_NextDlgCtl,0,0);
        end;
        VK_INSERT:
        begin
          if ((kom = cxExtArtikal) or (kom = ArtVid) or (kom = ArtSifra)) then
          begin
            frmNurkoRepository := TfrmNurkoRepository.Create(nil,Name,rData,tblOutStavka);
            frmNurkoRepository.kontrola_naziv := 'cxExtArtikal';
            frmNurkoRepository.ShowModal;

            if (frmNurkoRepository.ModalResult = mrOk) then
            begin
              cxExtArtikal.EditValue := frmNurkoRepository.cxGrid1Level1.GridView.DataController.GetRecordId(frmNurkoRepository.cxGrid1Level1.GridView.DataController.FocusedRecordIndex);
              SmeniNazivArtikal();
            end;

            frmNurkoRepository.Free;
          end;

        end;
    end;
end;

//	������� �� ��� �� ������ ��� �������� �� �����
procedure TfrmEvidencijaPotroseniMaterijali.cxDBTextEditAllEnter(Sender: TObject);
begin
    TEdit(Sender).Color:=clSkyBlue;
end;

//	������� �� ��� �� ������ ��� ����� �� �����
procedure TfrmEvidencijaPotroseniMaterijali.cxDBTextEditAllExit(Sender: TObject);
begin
    TEdit(Sender).Color:=clWhite;
end;

procedure TfrmEvidencijaPotroseniMaterijali.cxExtArtikalExit(Sender: TObject);
begin
  if (cxGrid1DBTableView1.DataController.DataSet.State = dsInsert) then
  begin
    SmeniNazivArtikal();
    aZemiArtikalInfo.Execute;
  end;

  if (cxGrid1DBTableView1.DataController.DataSet.State = dsEdit) then
  begin
    SmeniNazivArtikal();
    aZemiArtikalInfo.Execute;
  end;
end;

procedure TfrmEvidencijaPotroseniMaterijali.SmeniNazivArtikal();
begin
  if cxExtArtikal.Text <> '' then
	begin
		tblOutStavkaART_VID.Value := cxExtArtikal.EditValue[0];
		tblOutStavkaART_SIF.Value := cxExtArtikal.EditValue[1];
	end
	else
	begin
    tblOutStavkaART_VID.Clear;
		tblOutStavkaART_SIF.Clear;
	end;
end;

procedure TfrmEvidencijaPotroseniMaterijali.NajdiArtikalVoCenovnik();
begin
  // proveri dali ima cenovnik i dali e odbran artiklot

end;

procedure TfrmEvidencijaPotroseniMaterijali.cxGrid1DBTableView1DblClick(Sender: TObject);
begin
  if (inserting = false) then prefrli;
end;

procedure TfrmEvidencijaPotroseniMaterijali.cxGrid1DBTableView1FilterCustomization(
  Sender: TcxCustomGridTableView; var ADone: Boolean);
begin
end;

//  ��������� �� ������ �� ��������� �� ���������� ���� �� ��������
//  ��������� ��: br - ����� ��� �� ������ (�� 0 �� 9) �� ��������� �� ����� �� ����� �� ���������� ����
//  s - ��������� �� ������ �� ������ (���������� �� string �� ����� �� integer �� IntToStr ���������)
procedure TfrmEvidencijaPotroseniMaterijali.SetSifra(br: integer; s : string);
begin
  _sifra_kluc[br] := s;
end;

//  ������� �� ������ �� ��������� �� ���������� ���� �� ��������
//  ��������� ��: br - ����� ��� �� ������ (�� 0 �� 9) �� ��������� �� ����� �� ����� �� ���������� ����
function TfrmEvidencijaPotroseniMaterijali.GetSifra(br: integer) : string;
begin
  Result := _sifra_kluc[br];
end;

procedure TfrmEvidencijaPotroseniMaterijali.lcbIzvedenaEnter(Sender: TObject);
begin
  tblIzvedenaEM.Close;
  tblIzvedenaEM.ParamByName('artvid').Value := tblOutStavkaART_VID.Value;
  tblIzvedenaEM.ParamByName('artsif').Value := tblOutStavkaART_SIF.Value;
  tblIzvedenaEM.Open;

  if tblOutStavkaIZV_EM.IsNull then
  begin
    //ako e prazno togas probaj zemi go default-nata merka
    if tblIzvedenaEM.Locate('DEFAULT_IZVEDENA_EM', 1,[]) then
    begin
      tblOutStavkaIZV_EM.Value := tblIzvedenaEMID.Value;
    end;
  end;
end;

procedure TfrmEvidencijaPotroseniMaterijali.lcbIzvedenaExit(Sender: TObject);
begin
  if ((cxGrid1DBTableView1.DataController.DataSet.State in [dsEdit,dsInsert]) and (not tblOutStavkaIZV_EM.IsNull)) then
    tblOutStavkaBR_PAKET.Value := tblIzvedenaEMKOEFICIENT.Value; //lcbIzvedena.Properties.ListColumns.ColumnByFieldName('KOEFICIENT').Field.Value;
end;

//  �� ������� �� ������� ��� ����������� �� Master-�� �� �������� �� ��������� �� ������ ����� SetSifra
procedure TfrmEvidencijaPotroseniMaterijali.prefrli;
begin
  //ModalResult := mrOk;
  //SetSifra(0, dm.tblKontenPlanSIFRA.Value);
end;

procedure TfrmEvidencijaPotroseniMaterijali.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    //	��������� ��� ��������� �� �������
    if (cxGrid1DBTableView1.DataController.DataSource.State = dsEdit) or (cxGrid1DBTableView1.DataController.DataSource.State = dsInsert) then
    begin
        frmDaNe := TfrmDaNe.Create(self, '���������� ��������', '���������� �� �� ��������. ���� ������ �� �� �������?', 1);
        if (frmDaNe.ShowModal <> mrYes) then
        begin
            cxGrid1DBTableView1.DataController.DataSet.Cancel;
            Action := caFree;
            dmRes.FreeRepository(rData);
        end
        else
          if (Validacija(activPanelD) = false) then
          begin
            cxGrid1DBTableView1.DataController.DataSet.Post;
            Action := caFree;
            dmRes.FreeRepository(rData);
          end
          else Action := caNone;
    end
    else
      dmRes.FreeRepository(rData);
end;
procedure TfrmEvidencijaPotroseniMaterijali.FormCreate(Sender: TObject);
begin
 ProcitajFormaIzgled(self);
  if (dmRes.use_skin = true) then dxRibbon1.ColorSchemeName := dmRes.skin_name;

  e_likvidirana := 0;
  art_lot_kol := 0;
  rData := TRepositoryData.Create();  // kreirame instance od klasata TRepositoryData sto se naogja vo dmRes
end;

procedure TfrmEvidencijaPotroseniMaterijali.FormResize(Sender: TObject);
begin

end;

//------------------------------------------------------------------------------

procedure TfrmEvidencijaPotroseniMaterijali.FormShow(Sender: TObject);
begin
  //  ������� �� ������������ ������������/���������� �� ���������� �� ���� �����
    SpremiForma(self);
  //	�� ��������� �� ������� ������ ��� ���������� � ���� � ��� �������� �� �������
    //PrvPosledenTab(activPanelD,posledna,prva);
    dxBarManager1Bar1.Caption := Caption;
    dxComponentPrinter1Link1.ReportTitle.Text := Caption;

  //	������� �� ������������ �� ������ �� ������
    procitajGridOdBaza(Name,cxGrid1DBTableView1,false,false);
  //	������� �� ������������ �� ��������� �� ������
    procitajPrintOdBaza(Name,cxGrid1DBTableView1.Name, dxComponentPrinter1Link1);

    Caption := '��������� �� ��������� : ' + IntToStr(e_br) + '/' + IntToStr(e_god);
//    if (mod_rabota = 0) then
//      Caption := '��������� �� ��������� ��������� �� ������� : ' + IntToStr(dmMat.tblReMagacinID.Value) + ' - ' + dmMat.tblReMagacinNAZIV.Value
//    else
//      Caption := '��������� �� ���/����/������ �� ������� : ' + IntToStr(dmMat.tblReMagacinID.Value) + ' - ' + dmMat.tblReMagacinNAZIV.Value;

    //dmRes.setParam(':GOD',IntToStr(dm.godina));
    if (mod_rabota = 0) then
      dmRes.setParam(':TIP_DOK','14')
    else
      dmRes.setParam(':TIP_DOK','15');

    dmRes.setParam(':MAGACIN.ID',IntToStr(dmMat.tblReMagacinID.Value));

    cxExtArtikal.RepositoryItem := dmRes.InitRepository(-203, 'cxExtArtikal', Name, rData);

  	//procitajGridOdBaza(Name + '_' + IntToStr(rData.nurkoRepository[rData.br_repository]), rData.gridRepository[rData.br_repository] ,false,false);

    tblOutStavka.Close;
    tblOutStavka.ParamByName('EVIDENCIJA_ID').Value := evidencija_id;
    tblOutStavka.Open;

    aZemiMerka.Execute;

    SpremiModNaRabota();
    CustomFields();
    aHideFields.Execute;

     if (e_likvidirana = 1) then aLikvidiranaEvidencija.Execute;  // ako e likvidirana priemnicata ne dozvoluvaj promeni na stavkite
end;
//------------------------------------------------------------------------------

procedure TfrmEvidencijaPotroseniMaterijali.SaveToIniFileExecute(Sender: TObject);
begin
//    cxGrid1DBTableView1.StoreToIniFile(Name,True,[]);
end;
//------------------------------------------------------------------------------

procedure TfrmEvidencijaPotroseniMaterijali.cxGrid1DBTableView1KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  case Key of
    VK_RETURN:
    begin
      if (inserting = false) then prefrli;
    end;

  end;
end;

procedure TfrmEvidencijaPotroseniMaterijali.cxGrid1DBTableView1KeyPress(Sender: TObject;
  var Key: Char);
begin
  if(Ord(Key) <> VK_RETURN) then cxSortiraj(cxGrid1DBTableView1);
end;

procedure TfrmEvidencijaPotroseniMaterijali.cxPageControlMenuChange(Sender: TObject);
begin
    if cxPageControlMenu.ActivePage = cxTabSheetKolicinski  then
    begin
      PrvPosledenTab(dPanelKol,posledna,prva);
      activPanelD := dPanelKol;
      activZapisi := ZapisiButtonKol;
    end;

    if cxPageControlMenu.ActivePage = cxTabSheetCustom  then
    begin
      PrvPosledenTab(dPanelCustom,posledna,prva);
      activPanelD := dPanelCustom;
      activZapisi := ZapisiButtonCustom;
    end;
end;



//  ����� �� �����
procedure TfrmEvidencijaPotroseniMaterijali.aZapisiExecute(Sender: TObject);
var
  st: TDataSetState;
  prodolzi : boolean;
  br_na_serii : integer;
begin

//  try
//    st := cxGrid1DBTableView1.DataController.DataSet.State;
//    if st in [dsEdit,dsInsert] then
//    begin
//      activZapisi.SetFocus;
//      cxPageControlMenu.ActivePageIndex := 0;
//      if (Validacija(activPanelD) = false) then
//      begin
//        //cxPageControlMenu.ActivePageIndex := 1;
//        prodolzi := true;
//
//        // osloboduvanje po serija
//        if (dmMat.tblReMagacinOSLOBODUVANJE.Value = 3) then
//        begin
//          frmEvidencijaSerijaKolicini := TfrmEvidencijaSerijaKolicini.Create(nil, false);
//          br_na_serii := frmEvidencijaSerijaKolicini.initParams(dmMat.tblReMagacinID.Value, ArtVid.EditValue, ArtSifra.EditValue, Kolicina.EditValue);
//          if (br_na_serii = 0) then
//          begin
//            prodolzi := false;
//            ShowMessage('������! �� ��������� ������� ���� ������ �� ���������!');
//            frmEvidencijaSerijaKolicini.Free;
//            Kolicina.SetFocus;
//          end;
//          if (br_na_serii = 1) then
//          begin
//            frmEvidencijaSerijaKolicini.aZapisi.Execute;
//            prodolzi := frmEvidencijaSerijaKolicini.uspesno;
//            if (prodolzi = false) then frmEvidencijaSerijaKolicini.Free;
//          end;
//          if (br_na_serii > 1) then
//          begin
//            frmEvidencijaSerijaKolicini.ShowModal;
//            prodolzi := frmEvidencijaSerijaKolicini.uspesno;
//            if (prodolzi = false) then frmEvidencijaSerijaKolicini.Free;
//          end;
//
//        end;
//
//
//        if (prodolzi = true) then
//        begin
//            if ((st = dsInsert) and inserting) then
//            begin
//              cxGrid1DBTableView1.DataController.DataSet.Post;
//
//              dm.tblPogledNormativ.Close;
//              dm.tblPogledNormativ.ParamByName('id').Value := evidencija_id;
//              dm.tblPogledNormativ.Open;
//
//              if (dmMat.tblReMagacinOSLOBODUVANJE.Value = 3) then
//              begin
//                frmEvidencijaSerijaKolicini.GenerirajInOut(tblOutStavkaID.Value);
//                frmEvidencijaSerijaKolicini.Free;
//              end;
//              SpremiModNaRabota();
//              //cxPageControlMenu.ActivePageIndex := 0;
//         //    if od_nor = 0 then
//               aNov.Execute;
//            end;
//
//            if ((st = dsInsert) and (not inserting)) then
//            begin
//              cxGrid1DBTableView1.DataController.DataSet.Post;
//
//              dm.tblPogledNormativ.Close;
//              dm.tblPogledNormativ.ParamByName('id').Value := evidencija_id;
//              dm.tblPogledNormativ.Open;
//
//              if (od_nor = 1) and (not dm.tblPogledNormativ.IsEmpty) then
//              begin
//                aSurovinaOdN.Execute;
//              end;
//              od_nor := 0;
//
//              if (dmMat.tblReMagacinOSLOBODUVANJE.Value = 3) then
//              begin
//                frmEvidencijaSerijaKolicini.GenerirajInOut(tblOutStavkaID.Value);
//                frmEvidencijaSerijaKolicini.Free;
//              end;
//              aResetIzvedenaEM.Execute;
//
//              SpremiModNaRabota();
//              cxPageControlMenu.ActivePage.SetFocus;
//              activPanelD.Enabled:=false;
//
//              lPanel.Enabled:=true;
//              cxGrid1.SetFocus;
//            end;
//
//            if (st = dsEdit) then
//            begin
//              cxGrid1DBTableView1.DataController.DataSet.Post;
//              if (dmMat.tblReMagacinOSLOBODUVANJE.Value = 3) then
//              begin
//                frmEvidencijaSerijaKolicini.GenerirajInOut(tblOutStavkaID.Value);
//                frmEvidencijaSerijaKolicini.Free;
//              end;
//              aResetIzvedenaEM.Execute;
//
//              SpremiModNaRabota();
//              //cxPageControlMenu.ActivePageIndex := 0;
//              activPanelD.Enabled:=false;
//
//              lPanel.Enabled:=true;
//              cxGrid1.SetFocus;
//            end;
//        end;
//      end;
//
//    end;
//  finally


try
    st := cxGrid1DBTableView1.DataController.DataSet.State;
    if st in [dsEdit,dsInsert] then
    begin
      activZapisi.SetFocus;
      cxPageControlMenu.ActivePageIndex := 0;
      if (Validacija(activPanelD) = false) then
      begin
        //cxPageControlMenu.ActivePageIndex := 1;
        prodolzi := true;

        if (tblLagerArtikal.State <> dsBrowse) then aZemiArtikalInfo.Execute;

        if (dmMat.tblReMagacinNEGATIVEN_LAGER.Value = 0) then
        begin
          // ako artiklot ne se smeta kako zaliha (t.e e usluga ili sl.)
          if (tblLagerArtikalART_ZALIHA.Value <> 0) then
          begin
            // ako e za vo zaliha proveri go lagerot
            if (tblLagerArtikalO_LAGER.Value < tblOutStavkaKOLICINA.Value) then
            begin
              prodolzi := false;
              ShowMessage('������! �� ��������� ������� ���� ������ �� ���������!');
            end
            else
            if ((tblOutStavka.State = dsEdit) and (tblLagerArtikalO_LAGER.Value + art_kol < tblOutStavkaKOLICINA.Value)) then
            begin
              prodolzi := false;
              ShowMessage('������! �� ��������� ������� ���� ������ �� ���������!');
            end;
          end;
        end;

        // za sledlivost
        if ((dmMat.tblReMagacinSLEDLIVOST.Value = 1) and (tblLagerArtikalART_ZALIHA.Value <> 0)) then
        begin
          frmEvidencijaSerijaKolicini := TfrmEvidencijaSerijaKolicini.Create(nil, false);
          if (st = dsEdit) then
          begin
            if (Kolicina.EditValue - art_lot_kol) = 0 then br_na_serii := -1 // nema promena vo kolicinata, nema potreba od promena vo LOT-ovite
            else br_na_serii := frmEvidencijaSerijaKolicini.initParams(dmMat.tblReMagacinID.Value, ArtVid.EditValue, ArtSifra.EditValue, (Kolicina.EditValue - art_lot_kol), e_datum)
          end
          else
            br_na_serii := frmEvidencijaSerijaKolicini.initParams(dmMat.tblReMagacinID.Value, ArtVid.EditValue, ArtSifra.EditValue, Kolicina.EditValue, e_datum);

          if (br_na_serii = 0) then
          begin
            prodolzi := false;
            ShowMessage('������ ����������! �� ��������� ������� ���� ������ �� ���������!');
            frmEvidencijaSerijaKolicini.Free;
            Kolicina.SetFocus;
          end;
          if (br_na_serii = 1) then
          begin
            frmEvidencijaSerijaKolicini.aZapisi.Execute;
            if (not frmEvidencijaSerijaKolicini.uspesno) then prodolzi := false;
            if (prodolzi = false) then frmEvidencijaSerijaKolicini.Free;
          end;
          if (br_na_serii > 1) then
          begin
            frmEvidencijaSerijaKolicini.ShowModal;
            prodolzi := frmEvidencijaSerijaKolicini.uspesno;
            if (prodolzi = false) then frmEvidencijaSerijaKolicini.Free;
          end;

        end;

        if (prodolzi = true) then
        begin
            if ((st = dsInsert) and inserting) then
            begin
              cxGrid1DBTableView1.DataController.DataSet.Post;

              dm.tblPogledNormativ.Close;
              dm.tblPogledNormativ.ParamByName('id').Value := evidencija_id;
              dm.tblPogledNormativ.Open;

              if ((dmMat.tblReMagacinSLEDLIVOST.Value = 1) and (tblLagerArtikalART_ZALIHA.Value <> 0)) then
              begin
                frmEvidencijaSerijaKolicini.GenerirajInOut(tblOutStavkaID.Value);
                tblOutStavka.FullRefresh;
                frmEvidencijaSerijaKolicini.Free;
              end;
              SpremiModNaRabota();
              //cxPageControlMenu.ActivePageIndex := 0;

              tblLagerArtikal.Close;

              aNov.Execute;
            end;

            if ((st = dsInsert) and (not inserting)) then
            begin
              cxGrid1DBTableView1.DataController.DataSet.Post;

              dm.tblPogledNormativ.Close;
              dm.tblPogledNormativ.ParamByName('id').Value := evidencija_id;
              dm.tblPogledNormativ.Open;

              if (od_nor = 1) and (not dm.tblPogledNormativ.IsEmpty) then
              begin
                aSurovinaOdN.Execute;
              end;
              od_nor := 0;




              if ((dmMat.tblReMagacinSLEDLIVOST.Value = 1) and (tblLagerArtikalART_ZALIHA.Value <> 0)) then
              begin
                frmEvidencijaSerijaKolicini.GenerirajInOut(tblOutStavkaID.Value);
                tblOutStavka.FullRefresh;
                frmEvidencijaSerijaKolicini.Free;
              end;
              aResetIzvedenaEM.Execute;

              SpremiModNaRabota();
              cxPageControlMenu.ActivePage.SetFocus;
              activPanelD.Enabled:=false;

              lPanel.Enabled:=true;
              cxGrid1.SetFocus;

              tblLagerArtikal.Close;
            end;

            if (st = dsEdit) then
            begin
              cxGrid1DBTableView1.DataController.DataSet.Post;
              if ((dmMat.tblReMagacinSLEDLIVOST.Value = 1) and (tblLagerArtikalART_ZALIHA.Value <> 0)) then
              begin
                frmEvidencijaSerijaKolicini.GenerirajInOut(tblOutStavkaID.Value);
                tblOutStavka.FullRefresh;
                frmEvidencijaSerijaKolicini.Free;
              end;
              aResetIzvedenaEM.Execute;

              SpremiModNaRabota();
              //cxPageControlMenu.ActivePageIndex := 0;
              activPanelD.Enabled:=false;

              lPanel.Enabled:=true;
              cxGrid1.SetFocus;

              tblLagerArtikal.Close;
            end;
        end;
      end;

    end;
  finally

  end;
end;

procedure TfrmEvidencijaPotroseniMaterijali.aZemiArtikalInfoExecute(
  Sender: TObject);
begin
  if (tblOutStavka.State in [dsInsert,dsEdit]) then
  begin
    if ((not tblOutStavkaART_VID.isNull) and (not tblOutStavkaART_SIF.isNull))  then
    begin
      // zemi go lagerot na artiklot
      tblLagerArtikal.Close;
      tblLagerArtikal.ParamByName('RE').Value := dmMat.tblReMagacinID.Value;
      tblLagerArtikal.ParamByName('ARTVID').Value := tblOutStavkaART_VID.Value;
      tblLagerArtikal.ParamByName('ARTSIF').Value := tblOutStavkaART_SIF.Value;
      tblLagerArtikal.ParamByName('DATUM').Value := e_datum;
      tblLagerArtikal.Open;
    end
    else
    begin
      tblLagerArtikal.Close;
    end;
  end
  else
  begin
    tblLagerArtikal.Close;
  end;
end;

procedure TfrmEvidencijaPotroseniMaterijali.aZemiMerkaExecute(Sender: TObject);
begin
  if ((ArtVid.Text <> '') and (ArtSifra.Text <> '')) then
  begin
    qryArtikalInfo.Close;
    qryArtikalInfo.ParamByName('artvid').Value := ArtVid.Text;
    qryArtikalInfo.ParamByName('artsif').Value := ArtSifra.Text;
    qryArtikalInfo.ExecQuery;

    if (qryArtikalInfo.RecordCount > 0) then lblMerka.Caption := qryArtikalInfo.FieldValue('MERKA',false)
    else lblMerka.Caption := '';
  end
  else lblMerka.Caption := '';
end;

//	����� �� ���������� �� �������
procedure TfrmEvidencijaPotroseniMaterijali.aOtkaziExecute(Sender: TObject);
begin
    if (cxGrid1DBTableView1.DataController.DataSource.State in [dsBrowse, dsInactive]) then
    begin
        ModalResult := mrCancel;
        Close();
    end
    else
    begin
        cxGrid1DBTableView1.DataController.DataSet.Cancel;

        RestoreControls(activPanelD);
        activPanelD.Enabled := false;
        //od_nor := 0;
        lPanel.Enabled := true;
        cxGrid1.SetFocus;

        tblLagerArtikal.Close;
    end;
end;

procedure TfrmEvidencijaPotroseniMaterijali.aOtvoriSeriiExecute(
  Sender: TObject);
begin
if (tblOutStavka.RecordCount > 0) then
  begin
    frmOutLots := TfrmOutLots.Create(self,true);
    frmOutLots.evidencija_id := tblOutStavkaID.Value;
    frmOutLots.evidencija_artvid := tblOutStavkaART_VID.Value;
    frmOutLots.evidencija_artsif := tblOutStavkaART_SIF.Value;
    frmOutLots.evidencija_kolicina := tblOutStavkaKOLICINA.Value;
    frmOutLots.evidencija_sledvlivost := tblOutStavkaSLEDLIVOST_KOLICINA.Value;
    frmOutLots.evidencija_datum := e_datum;
    frmOutLots.ShowModal;
    frmOutLots.Free;

    tblOutStavka.FullRefresh;
  end;

//  if (tblOutStavka.RecordCount > 0) then
//  begin
//    if (dmMat.tblReMagacinOSLOBODUVANJE.Value = 3) then
//    begin
//      frmUpotrebeniSerii := TfrmUpotrebeniSerii.Create(self,false);
//      frmUpotrebeniSerii.out_s_id := tblOutStavkaID.Value;
//      frmUpotrebeniSerii.ShowModal;
//      frmUpotrebeniSerii.Free;
//    end
//    else
//      ShowMessage('��������� ������� �� � �� ������������ �� �����!');
//  end;
end;

//----------------------------------------------------------------------------------
// ��������� �� ��������, ���������� �� ��������� � �������� �� Print ��������
//----------------------------------------------------------------------------------

//	����� �� �������� �� �������� �� �������
procedure TfrmEvidencijaPotroseniMaterijali.aPageSetupExecute(Sender: TObject);
begin
  dxComponentPrinter1Link1.PageSetup;
end;

//	����� �� ������� �� ������
procedure TfrmEvidencijaPotroseniMaterijali.aPecatiTabelaExecute(Sender: TObject);
begin
  dxComponentPrinter1Link1.ReportTitle.Text := '��������� �� ���������';

  dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Clear;
  dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Add(dmkon.firma_naziv);

  dxComponentPrinter1Link1.PrinterPage.PageHeader.LeftTitle.Clear;
  dxComponentPrinter1Link1.PrinterPage.PageHeader.LeftTitle.Add('��������� �� : ' + IntToStr(tblOutStavkaEBROJ.Value) + '/' + IntToStr(tblOutStavkaEGOD.Value));
  dxComponentPrinter1Link1.PrinterPage.PageHeader.LeftTitle.Add('������� ����� : ' + tblOutStavkaRN_BROJ_GOD.Value);

  dxComponentPrinter1.Preview(true, dxComponentPrinter1Link1);
end;

//	����� �� ���������� �� ��������� �� �������
procedure TfrmEvidencijaPotroseniMaterijali.aPodesuvanjePecatenjeExecute(Sender: TObject);
begin
  dxComponentPrinter1Link1.DesignReport();
end;

procedure TfrmEvidencijaPotroseniMaterijali.aSnimiPecatenjeExecute(Sender: TObject);
begin
  zacuvajPrintVoBaza(Name,cxGrid1DBTableView1.Name,dxComponentPrinter1Link1);
end;

procedure TfrmEvidencijaPotroseniMaterijali.aSurovinaOdNExecute(
  Sender: TObject);
var i:integer;
begin
if tblOutStavka.State = dsBrowse then
begin
  // �� �� ������ �������������� ���������� �� ������ ��������
 //- dm.tblPogledNormativ.First;
//  for I := 0 to dm.tblPogledNormativ.RecordCount-1 do
 //  begin
     od_nor := 1;
     inserting := false;
     aNov.Execute;
     od_nor := 0;
 //  end;

end;

//   frmPogledNormativ := TfrmPogledNormativ.Create(self);
//   dm.tblPogledNormativ.Close;
//   dm.tblPogledNormativ.ParamByName('id').AsString := tblOutStavkaID_RNS.Value;
//   dm.tblPogledNormativ.Open;
//   frmPogledNormativ.ShowModal;
//   frmPogledNormativ.Free;
//   if not dm.tblPogledNormativVID_ARTIKAL_N.IsNull then
//   begin
//     aNov.Execute;
//     tblOutStavkaART_VID.Value := dm.tblPogledNormativVID_ARTIKAL_N.Value;
//     tblOutStavkaART_SIF.Value := dm.tblPogledNormativARTIKAL_N.Value;
//     tblOutStavkaKOLICINA.Value := dm.tblPogledNormativKOLICINA_N.Value;
//     Kolicina.SetFocus;
//   end;

end;

procedure TfrmEvidencijaPotroseniMaterijali.aUpstreamExecute(Sender: TObject);
begin
//if not (tblOutStavka.State in [dsEdit,dsInsert]) then
// begin
//  dmRes.Spremi('MAN', 4);
//  dmKon.tblSqlReport.ParamByName('lot').AsString :=tblOutStavkaLOT_rns.Value;
//  //dmKon.tblSqlReport.ParamByName('id').AsString := '%';
//  dmKon.tblSqlReport.Open;
//
//  dmRes.frxReport1.Variables.Variables['proizvod']:=quotedstr(tblOutStavkaNAZIV_PROIZVOD.AsString);
//  dmRes.frxReport1.Variables.Variables['lot_rns']:=QuotedStr(tblOutStavkaLOT_RNS.AsString);
//  dmRes.frxReport1.ShowReport();
// end;

end;

procedure TfrmEvidencijaPotroseniMaterijali.aVnesiOdNorExecute(Sender: TObject);
begin
if tblOutStavka.State = dsBrowse then
begin
    od_nor := 2;
    inserting := false;
    aNov.Execute;
    od_nor := 0;
end;
end;

//	����� �� ������ �� ����������� �������� �� ������� (Utils.pas)
procedure TfrmEvidencijaPotroseniMaterijali.aBrisiPodesuvanjePecatenjeExecute(Sender: TObject);
begin
  brisiPrintOdBaza(Name,cxGrid1DBTableView1.Name, dxComponentPrinter1Link1);
end;

procedure TfrmEvidencijaPotroseniMaterijali.aCustomFieldsFormExecute(Sender: TObject);
begin
  frmCustomFields := TfrmCustomFields.Create(self,true);
  frmCustomFields.ShowModal;
  frmCustomFields.Free;
end;

procedure TfrmEvidencijaPotroseniMaterijali.aDesignerExecute(Sender: TObject);
begin
   if not (tblOutStavka.State in [dsEdit,dsInsert]) then
 begin
  dmRes.Spremi('MAN', 4);
  dmKon.tblSqlReport.ParamByName('lot').AsString :=tblOutStavkaLOT_rns.Value;
  //dmKon.tblSqlReport.ParamByName('id').AsString := '%';
  dmKon.tblSqlReport.Open;

  dmRes.frxReport1.Variables.Variables['proizvod']:=QuotedStr(tblOutStavkaNAZIV_PROIZVOD.AsString);
  dmRes.frxReport1.Variables.Variables['lot_rns']:=QuotedStr(tblOutStavkaLOT_RNS.AsString);

  dmRes.frxReport1.DesignReport();
end;
end;

//  ����� �� ����� �� ����� �� ������������� �� �������� � ��������������� ��
//  �������(������ ���������� ��������)
procedure TfrmEvidencijaPotroseniMaterijali.aFormConfigExecute(Sender: TObject);
begin
  frmFormConfig := TfrmFormConfig.Create(Application);
  frmFormConfig.formPtr := Addr(Self);
  frmFormConfig.ShowModal;
  frmFormConfig.Free;
end;

procedure TfrmEvidencijaPotroseniMaterijali.aHideFieldsExecute(Sender: TObject);
begin

end;

//----------------------------------------------------------------------------------
// ���� -> ��������� �� ��������, ���������� �� ��������� � �������� �� Print ��������
//----------------------------------------------------------------------------------

// ��������� �� ���������� �� ��������� � �������� �� Print �������� �� .ini ��������
procedure TfrmEvidencijaPotroseniMaterijali.zacuvajPrintVoIni(ime:AnsiString);
var
  AStream: TFileStream;
begin
  AStream := TFileStream.Create(dmKon.RabotenDir + ime + 'Print.ini', fmCreate);
  try
    AStream.WriteComponent(dxComponentPrinter1Link1);
  finally
    AStream.Free;
  end;

end;

// ��������� �� ������ �� ��������� � �������� �� Print �������� �������� �� .ini ��������
procedure TfrmEvidencijaPotroseniMaterijali.procitajPrintOdIni(ime:AnsiString);
var
  AStream: TFileStream;
begin
  AStream := TFileStream.Create(dmKon.RabotenDir + ime + 'Print.ini', fmOpenRead);
  try
    dxComponentPrinter1Link1.RestoreDefaults;
    AStream.ReadComponent(dxComponentPrinter1Link1);
  finally
    AStream.Free;
  end;
end;

procedure TfrmEvidencijaPotroseniMaterijali.SpremiModNaRabota();
begin
  //mod_rabota go opredeluva modot na rabota na formata (0 - Potrosok, 1 - Rastur,krs,kala)
  //if (mod_rabota = 0) then
  begin
    cxPageControlMenu.ActivePage := cxTabSheetKolicinski;

    PrvPosledenTab(dPanelKol,posledna,prva);
    activPanelD := dPanelKol;
    activZapisi := ZapisiButtonKol;
  end;

 // ������ ����������
  if (dmMat.tblReMagacinSLEDLIVOST.Value = 1) then
  begin
    aOtvoriSerii.Enabled := true;
    //aSkenirajLot.Enabled := true;

    cxGrid1DBTableView1SLEDLIVOST_KOLICINA.Visible := true;
  end;
end;

procedure TfrmEvidencijaPotroseniMaterijali.CustomFields();
begin
  custom1 := getCustomField('MTR_OUT_S','CUSTOM1');
  custom2 := getCustomField('MTR_OUT_S','CUSTOM2');
  custom3 := getCustomField('MTR_OUT_S','CUSTOM3');

  if custom1 <> '' then
  begin
    cxGrid1DBTableView1CUSTOM1.Visible := true;
    cxGrid1DBTableView1CUSTOM1.VisibleForCustomization := true;
    cxGrid1DBTableView1CUSTOM1.Caption := custom1;

    lblCustom1.Visible := true;
    lblCustom1.Caption := custom1 + ' :';
    txtCustom1.Visible := true;
  end;
  if custom2 <> '' then
  begin
    cxGrid1DBTableView1CUSTOM2.Visible := true;
    cxGrid1DBTableView1CUSTOM2.VisibleForCustomization := true;
    cxGrid1DBTableView1CUSTOM2.Caption := custom2;

    lblCustom2.Visible := true;
    lblCustom2.Caption := custom2 + ' :';
    txtCustom2.Visible := true;
  end;
  if custom3 <> '' then
  begin
    cxGrid1DBTableView1CUSTOM3.Visible := true;
    cxGrid1DBTableView1CUSTOM3.VisibleForCustomization := true;
    cxGrid1DBTableView1CUSTOM3.Caption := custom3;

    lblCustom3.Visible := true;
    lblCustom3.Caption := custom3 + ' :';
    txtCustom3.Visible := true;
  end;
end;

procedure TfrmEvidencijaPotroseniMaterijali.PresmetajNaIzlez(Sender: TObject);
var kontrola : TcxDBTextEdit;
begin
  if (cxGrid1DBTableView1.DataController.DataSet.State in [dsInsert,dsEdit]) then
  begin
    // zemi go imeto na kontrolata od koja e izlezeno
    kontrola := Sender as TcxDBTextEdit;

    //////////////////////////////////////////////////////////////////////////////////////////////////////
    // za kolicinite i paketite
    //////////////////////////////////////////////////////////////////////////////////////////////////////

    if (kontrola.Name = 'Paketi') or (kontrola.Name = 'BrPaket') then
    begin
      if ((not tblOutStavkaPAKETI.IsNull) and (not tblOutStavkaBR_PAKET.IsNull)) then
        tblOutStavkaKOLICINA.Value := tblOutStavkaPAKETI.Value * tblOutStavkaBR_PAKET.Value;
    end;

  end;
end;





end.
