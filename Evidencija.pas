unit Evidencija;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData, cxEdit,
  DB, cxDBData, cxGridCustomPopupMenu, cxGridPopupMenu,
  ComCtrls, cxContainer, cxTextEdit, cxDBEdit, StdCtrls, cxGridLevel,
  cxClasses, cxControls, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGrid, ExtCtrls, Menus, ActnList,
  cxGridExportLink, cxExport, cxLookAndFeelPainters, cxDataStorage,
  cxButtons, dxStatusBar, dxRibbonStatusBar, dxRibbon, dxBar, dxBarExtItems,
  dxPSGlbl, dxPSUtl, dxPSEngn, dxPrnPg, dxBkgnd, dxWrap, dxPrnDev,
  dxPSCompsProvider, dxPSFillPatterns, dxPSEdgePatterns, dxPSCore, dxPScxCommon,
  cxPC, cxMemo, cxLocalization, cxLookAndFeels,
  dxPSPDFExportCore, dxPSPDFExport, cxDrawTextUtils, dxPSPrVwStd, dxPSPrVwAdv,
  dxPSPrVwRibbon, dxPScxEditorProducers, dxPScxExtEditorProducers,
  dxPScxPageControlProducer, dxPgsDlg, cxCheckBox, cxBarEditItem, dxSkinsCore,
  dxSkinOffice2007Blue, dxSkinscxPCPainter, dxSkinsdxRibbonPainter,
  dxSkinsdxBarPainter, dxBarSkinnedCustForm, dxSkinBlack, dxSkinBlue,
  dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy,
  dxSkinGlassOceans, dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky,
  dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins, dxSkinOffice2007Black,
  dxSkinOffice2007Green, dxSkinOffice2007Pink, dxSkinOffice2007Silver,
  dxSkinPumpkin, dxSkinSeven, dxSkinSharp, dxSkinSilver, dxSkinSpringTime,
  dxSkinStardust, dxSkinSummer2008, dxSkinsDefaultPainters, dxSkinValentine,
  dxSkinXmas2008Blue, cxDropDownEdit, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxRibbonSkins, dxPScxGridLnk,
  dxPScxGridLayoutViewLnk, dxScreenTip, dxSkinBlueprint,
  dxSkinDevExpressDarkStyle, dxSkinDevExpressStyle, dxSkinHighContrast,
  dxSkinSevenClassic, dxSkinSharpPlus, dxSkinTheAsphaltWorld, dxSkinVS2010,
  dxSkinWhiteprint, dxPScxPivotGridLnk, dmResources, FIBDataSet, pFIBDataSet,
  cxMaskEdit, cxLookupEdit, cxDBLookupEdit, cxDBExtLookupComboBox, cxCalendar,
  FIBQuery, pFIBQuery, pFIBStoredProc, dxSkinOffice2013White, cxNavigator,
  System.Actions, cxDBLookupComboBox, dxSkinMetropolis, dxSkinMetropolisDark,
  dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray, dxRibbonCustomizationForm,
  dxSkinOffice2016Colorful, dxSkinOffice2016Dark, dxSkinVisualStudio2013Blue,
  dxSkinVisualStudio2013Dark, dxSkinVisualStudio2013Light;

type
//  niza = Array[1..5] of Variant;

  TfrmEvidencija = class(TForm)
    dPanel: TPanel;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    lPanel: TPanel;
    cxGridPopupMenu1: TcxGridPopupMenu;
    PopupMenu1: TPopupMenu;
    N1: TMenuItem;
    Excel1: TMenuItem;
    ZapisiButton: TcxButton;
    OtkaziButton: TcxButton;
    dxRibbon1Tab1: TdxRibbonTab;
    dxRibbon1: TdxRibbon;
    StatusBar1: TdxRibbonStatusBar;
    dxBarManager1: TdxBarManager;
    dxBarManager1Bar1: TdxBar;
    dxBarManager1Bar2: TdxBar;
    dxBarManager1Bar3: TdxBar;
    dxBarLargeButton2: TdxBarLargeButton;
    dxBarLargeButton3: TdxBarLargeButton;
    dxBarLargeButton4: TdxBarLargeButton;
    dxBarLargeButton5: TdxBarLargeButton;
    dxBarLargeButton6: TdxBarLargeButton;
    dxBarLargeButton7: TdxBarLargeButton;
    dxBarLargeButton8: TdxBarLargeButton;
    dxBarLargeButton1: TdxBarLargeButton;
    ActionList1: TActionList;
    aNov: TAction;
    aAzuriraj: TAction;
    aBrisi: TAction;
    aSnimiIzgled: TAction;
    aZacuvajExcel: TAction;
    aPecatiTabela: TAction;
    aHelp: TAction;
    aIzlez: TAction;
    aZapisi: TAction;
    aOtkazi: TAction;
    aRefresh: TAction;
    dxRibbon1Tab2: TdxRibbonTab;
    dxBarManager1Bar4: TdxBar;
    dxBarLargeButton9: TdxBarLargeButton;
    aSnimiPecatenje: TAction;
    dxComponentPrinter1: TdxComponentPrinter;
    dxComponentPrinter1Link1: TdxGridReportLink;
    ribbonFilterReset: TdxBarLargeButton;
    aPodesuvanjePecatenje: TAction;
    dxBarLargeButton12: TdxBarLargeButton;
    aBrisiPodesuvanjePecatenje: TAction;
    dxBarLargeButton13: TdxBarLargeButton;
    dxBarLargeButton14: TdxBarLargeButton;
    aPageSetup: TAction;
    dxBarLargeButton15: TdxBarLargeButton;
    dxBarSubItem1: TdxBarSubItem;
    aFormConfig: TAction;
    dxBarScreenTipRepository1: TdxBarScreenTipRepository;
    tipSnimiIzgled: TdxBarScreenTip;
    tipZacuvajExcel: TdxBarScreenTip;
    tipDizajnReport: TdxBarScreenTip;
    tipSetiranjeStrana: TdxBarScreenTip;
    tipSnimiKonf: TdxBarScreenTip;
    tipBrisiKonf: TdxBarScreenTip;
    cxBarEditItem1: TcxBarEditItem;
    dxBarManager1BarTabela: TdxBar;
    dxBarLBtnSnimiIzgled: TdxBarLargeButton;
    dxBarLBtnBrisiIzgled: TdxBarLargeButton;
    aBrisiIzgled: TAction;
    dxBarLargeButtonSoberi: TdxBarLargeButton;
    aSpustiSoberi: TAction;
    dsEvidencija: TDataSource;
    tblEvidencija: TpFIBDataSet;
    tblEvidencijaID: TFIBBCDField;
    tblEvidencijaGODINA: TFIBIntegerField;
    tblEvidencijaRE: TFIBIntegerField;
    tblEvidencijaMAG_NAZIV: TFIBStringField;
    tblEvidencijaTIP_DOK: TFIBIntegerField;
    tblEvidencijaTIP_DOK_NAZIV: TFIBStringField;
    tblEvidencijaBROJ: TFIBIntegerField;
    tblEvidencijaDATUM: TFIBDateField;
    tblEvidencijaTP: TFIBIntegerField;
    tblEvidencijaP: TFIBIntegerField;
    tblEvidencijaPARTNER_NAZIV: TFIBStringField;
    tblEvidencijaMESTO_NAZIV: TFIBStringField;
    tblEvidencijaRE2: TFIBIntegerField;
    tblEvidencijaRE2_NAZIV: TFIBStringField;
    tblEvidencijaOBJEKT: TFIBIntegerField;
    tblEvidencijaOBJ_NAZIV: TFIBStringField;
    tblEvidencijaOBJ_TP: TFIBIntegerField;
    tblEvidencijaOBJ_P: TFIBIntegerField;
    tblEvidencijaOBJ_PARTNER_NAZIV: TFIBStringField;
    tblEvidencijaOBJ_REGION: TFIBIntegerField;
    tblEvidencijaOBJ_REGION_NAZIV: TFIBStringField;
    tblEvidencijaOBJ_TIP: TFIBIntegerField;
    tblEvidencijaOBJ_ADRESA: TFIBStringField;
    tblEvidencijaOBJ_MESTO: TFIBIntegerField;
    tblEvidencijaOBJ_MESTO_NAZIV: TFIBStringField;
    tblEvidencijaOPIS: TFIBStringField;
    tblEvidencijaREF_NO: TFIBStringField;
    tblEvidencijaVALUTA: TFIBStringField;
    tblEvidencijaVAL_NAZIV: TFIBStringField;
    tblEvidencijaKURS: TFIBBCDField;
    tblEvidencijaCENOVNIK: TFIBIntegerField;
    tblEvidencijaCEN_OPIS: TFIBStringField;
    tblEvidencijaCEN_VAL: TFIBStringField;
    tblEvidencijaCEN_DDV: TFIBSmallIntField;
    tblEvidencijaCEN_AKTIVEN: TFIBSmallIntField;
    tblEvidencijaCEN_VAZI_OD: TFIBDateField;
    tblEvidencijaCEN_TIP: TFIBSmallIntField;
    tblEvidencijaFAKTURA_ID: TFIBBCDField;
    tblEvidencijaTIP_KNIZENJE: TFIBIntegerField;
    tblEvidencijaTK_NAZIV: TFIBStringField;
    tblEvidencijaTK_KNIZENJE: TFIBIntegerField;
    tblEvidencijaTIP_NALOG: TFIBStringField;
    tblEvidencijaNALOG_DATUM: TFIBDateField;
    tblEvidencijaNALOG_OPIS: TFIBStringField;
    tblEvidencijaNALOG_VID: TFIBSmallIntField;
    tblEvidencijaREF_ID: TFIBBCDField;
    tblEvidencijaSO_REF_ID: TFIBIntegerField;
    tblEvidencijaSAL_PREDMET: TFIBStringField;
    tblEvidencijaSAL_BROJ: TFIBIntegerField;
    tblEvidencijaSAL_GODINA: TFIBIntegerField;
    tblEvidencijaAKTIVEN: TFIBSmallIntField;
    tblEvidencijaKONTROLOR: TFIBStringField;
    tblEvidencijaKONTROLIRAN: TFIBSmallIntField;
    tblEvidencijaKONTROLIRAN_NA: TFIBDateTimeField;
    tblEvidencijaLIKVIDIRAN: TFIBSmallIntField;
    tblEvidencijaLIKVIDIRAN_NA: TFIBDateTimeField;
    tblEvidencijaLIKVIDATOR: TFIBStringField;
    tblEvidencijaPECATEN: TFIBSmallIntField;
    tblEvidencijaPECATEN_NA: TFIBDateTimeField;
    tblEvidencijaPECATEN_OD: TFIBStringField;
    tblEvidencijaSITE_ID: TFIBSmallIntField;
    tblEvidencijaCUSTOM1: TFIBStringField;
    tblEvidencijaCUSTOM2: TFIBStringField;
    tblEvidencijaCUSTOM3: TFIBStringField;
    tblEvidencijaTS_INS: TFIBDateTimeField;
    tblEvidencijaTS_UPD: TFIBDateTimeField;
    tblEvidencijaUSR_INS: TFIBStringField;
    tblEvidencijaUSR_UPD: TFIBStringField;
    tblEvidencijaPOVRATNICA_RE: TFIBIntegerField;
    tblEvidencijaPOVRATNICA_TD: TFIBIntegerField;
    tblEvidencijaPOVRATNICA_BR: TFIBIntegerField;
    tblEvidencijaSTORNO_FLAG: TFIBSmallIntField;
    tblEvidencijaSTORNO_DATUM: TFIBDateField;
    tblEvidencijaSTORNO_USER: TFIBStringField;
    tblEvidencijaSTORNO_NALOG_ID: TFIBBCDField;
    tblEvidencijaSTORNO_TIP_NALOG: TFIBStringField;
    tblEvidencijaSTORNO_NALOG: TFIBIntegerField;
    tblEvidencijaSTORNO_NALOG_DATUM: TFIBDateField;
    tblEvidencijaSTORNO_NALOG_OPIS: TFIBStringField;
    tblEvidencijaSTORNO_NALOG_VID: TFIBSmallIntField;
    tblEvidencijaSTORNO_ZABELESKA: TFIBStringField;
    tblEvidencijaWO_REF_ID: TFIBBCDField;
    tblEvidencijaRN_BROJ_GOD: TFIBStringField;
    tblEvidencijaRN_DATUM_PLANIRAN_POCETOK: TFIBDateTimeField;
    cxGrid1DBTableView1ID: TcxGridDBColumn;
    cxGrid1DBTableView1GODINA: TcxGridDBColumn;
    cxGrid1DBTableView1RE: TcxGridDBColumn;
    cxGrid1DBTableView1MAG_NAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1TIP_DOK: TcxGridDBColumn;
    cxGrid1DBTableView1TIP_DOK_NAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1BROJ: TcxGridDBColumn;
    cxGrid1DBTableView1DATUM: TcxGridDBColumn;
    cxGrid1DBTableView1TP: TcxGridDBColumn;
    cxGrid1DBTableView1P: TcxGridDBColumn;
    cxGrid1DBTableView1PARTNER_NAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1MESTO_NAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1RE2: TcxGridDBColumn;
    cxGrid1DBTableView1RE2_NAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1OBJEKT: TcxGridDBColumn;
    cxGrid1DBTableView1OBJ_NAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1OBJ_TP: TcxGridDBColumn;
    cxGrid1DBTableView1OBJ_P: TcxGridDBColumn;
    cxGrid1DBTableView1OBJ_PARTNER_NAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1OBJ_REGION: TcxGridDBColumn;
    cxGrid1DBTableView1OBJ_REGION_NAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1OBJ_TIP: TcxGridDBColumn;
    cxGrid1DBTableView1OBJ_ADRESA: TcxGridDBColumn;
    cxGrid1DBTableView1OBJ_MESTO: TcxGridDBColumn;
    cxGrid1DBTableView1OBJ_MESTO_NAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1OPIS: TcxGridDBColumn;
    cxGrid1DBTableView1REF_NO: TcxGridDBColumn;
    cxGrid1DBTableView1VALUTA: TcxGridDBColumn;
    cxGrid1DBTableView1VAL_NAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1KURS: TcxGridDBColumn;
    cxGrid1DBTableView1CENOVNIK: TcxGridDBColumn;
    cxGrid1DBTableView1CEN_OPIS: TcxGridDBColumn;
    cxGrid1DBTableView1CEN_VAL: TcxGridDBColumn;
    cxGrid1DBTableView1CEN_DDV: TcxGridDBColumn;
    cxGrid1DBTableView1CEN_AKTIVEN: TcxGridDBColumn;
    cxGrid1DBTableView1CEN_VAZI_OD: TcxGridDBColumn;
    cxGrid1DBTableView1CEN_TIP: TcxGridDBColumn;
    cxGrid1DBTableView1FAKTURA_ID: TcxGridDBColumn;
    cxGrid1DBTableView1TIP_KNIZENJE: TcxGridDBColumn;
    cxGrid1DBTableView1TK_NAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1TK_KNIZENJE: TcxGridDBColumn;
    cxGrid1DBTableView1TIP_NALOG: TcxGridDBColumn;
    cxGrid1DBTableView1NALOG_DATUM: TcxGridDBColumn;
    cxGrid1DBTableView1NALOG_OPIS: TcxGridDBColumn;
    cxGrid1DBTableView1NALOG_VID: TcxGridDBColumn;
    cxGrid1DBTableView1REF_ID: TcxGridDBColumn;
    cxGrid1DBTableView1SO_REF_ID: TcxGridDBColumn;
    cxGrid1DBTableView1SAL_PREDMET: TcxGridDBColumn;
    cxGrid1DBTableView1SAL_BROJ: TcxGridDBColumn;
    cxGrid1DBTableView1SAL_GODINA: TcxGridDBColumn;
    cxGrid1DBTableView1AKTIVEN: TcxGridDBColumn;
    cxGrid1DBTableView1KONTROLOR: TcxGridDBColumn;
    cxGrid1DBTableView1KONTROLIRAN: TcxGridDBColumn;
    cxGrid1DBTableView1KONTROLIRAN_NA: TcxGridDBColumn;
    cxGrid1DBTableView1LIKVIDIRAN: TcxGridDBColumn;
    cxGrid1DBTableView1LIKVIDIRAN_NA: TcxGridDBColumn;
    cxGrid1DBTableView1LIKVIDATOR: TcxGridDBColumn;
    cxGrid1DBTableView1PECATEN: TcxGridDBColumn;
    cxGrid1DBTableView1PECATEN_NA: TcxGridDBColumn;
    cxGrid1DBTableView1PECATEN_OD: TcxGridDBColumn;
    cxGrid1DBTableView1SITE_ID: TcxGridDBColumn;
    cxGrid1DBTableView1CUSTOM1: TcxGridDBColumn;
    cxGrid1DBTableView1CUSTOM2: TcxGridDBColumn;
    cxGrid1DBTableView1CUSTOM3: TcxGridDBColumn;
    cxGrid1DBTableView1TS_INS: TcxGridDBColumn;
    cxGrid1DBTableView1TS_UPD: TcxGridDBColumn;
    cxGrid1DBTableView1USR_INS: TcxGridDBColumn;
    cxGrid1DBTableView1USR_UPD: TcxGridDBColumn;
    cxGrid1DBTableView1POVRATNICA_RE: TcxGridDBColumn;
    cxGrid1DBTableView1POVRATNICA_TD: TcxGridDBColumn;
    cxGrid1DBTableView1POVRATNICA_BR: TcxGridDBColumn;
    cxGrid1DBTableView1STORNO_FLAG: TcxGridDBColumn;
    cxGrid1DBTableView1STORNO_DATUM: TcxGridDBColumn;
    cxGrid1DBTableView1STORNO_USER: TcxGridDBColumn;
    cxGrid1DBTableView1STORNO_NALOG_ID: TcxGridDBColumn;
    cxGrid1DBTableView1STORNO_TIP_NALOG: TcxGridDBColumn;
    cxGrid1DBTableView1STORNO_NALOG: TcxGridDBColumn;
    cxGrid1DBTableView1STORNO_NALOG_DATUM: TcxGridDBColumn;
    cxGrid1DBTableView1STORNO_NALOG_OPIS: TcxGridDBColumn;
    cxGrid1DBTableView1STORNO_NALOG_VID: TcxGridDBColumn;
    cxGrid1DBTableView1STORNO_ZABELESKA: TcxGridDBColumn;
    cxGrid1DBTableView1WO_REF_ID: TcxGridDBColumn;
    cxGrid1DBTableView1RN_BROJ_GOD: TcxGridDBColumn;
    cxGrid1DBTableView1RN_DATUM_PLANIRAN_POCETOK: TcxGridDBColumn;
    dxBarManager1Bar5: TdxBar;
    dxBarLargeButton10: TdxBarLargeButton;
    aMagacin: TAction;
    aResetNurkovci: TAction;
    Label2: TLabel;
    cxExtNalog: TcxDBExtLookupComboBox;
    Label3: TLabel;
    EvidencijaDatum: TcxDBDateEdit;
    Label1: TLabel;
    EvidencijaBroj: TcxDBTextEdit;
    qryZemiBroj: TpFIBQuery;
    dxBarLargeButton11: TdxBarLargeButton;
    aEditStavka: TAction;
    spBrisiEvidencija: TpFIBStoredProc;
    qryZemiReRabNalog: TpFIBQuery;
    Label4: TLabel;
    cxExtProizvod: TcxDBExtLookupComboBox;
    tblEvidencijaWO_S_REF_ID: TFIBBCDField;
    tblEvidencijaWO_ARTVID: TFIBIntegerField;
    tblEvidencijaWO_ARTSIF: TFIBIntegerField;
    tblEvidencijaWO_ARTIKAL: TFIBStringField;
    cxGrid1DBTableView1WO_S_REF_ID: TcxGridDBColumn;
    cxGrid1DBTableView1WO_ARTVID: TcxGridDBColumn;
    cxGrid1DBTableView1WO_ARTSIF: TcxGridDBColumn;
    cxGrid1DBTableView1WO_ARTIKAL: TcxGridDBColumn;
    dxBarLargeButton16: TdxBarLargeButton;
    aLikvidiraj: TAction;
    aGenerirajKnigCeni: TAction;
    aBrisiKnigCeni: TAction;
    dxBarSubItem2: TdxBarSubItem;
    dxBarLargeButton17: TdxBarLargeButton;
    aBrisiNalog: TAction;
    dxBarManager1Bar6: TdxBar;
    dxBarLargeButton18: TdxBarLargeButton;
    aPecatiPotrosok: TAction;
    aDizajnPotrosok: TAction;
    dxBarLargeButton19: TdxBarLargeButton;
    aPecatiPotrosokKnig: TAction;
    aDizajnPotrosokKnig: TAction;
    dxBarLargeButton20: TdxBarLargeButton;
    lbl1: TLabel;
    lcbNalog: TcxDBLookupComboBox;
    dxBarLargeButton21: TdxBarLargeButton;
    aAzurirajNalog: TAction;
    tblEvidencijaNALOG: TFIBBCDField;
    tblEvidencijaNALOG_BR: TFIBIntegerField;
    cxGrid1DBTableView1NALOG: TcxGridDBColumn;
    cxGrid1DBTableView1NALOG_BR: TcxGridDBColumn;
    dxBarManager1Bar7: TdxBar;
    dxBarLargeButton22: TdxBarLargeButton;
    aPotrosenoMatVS_Sumirano: TAction;

    procedure cxDBTextEditAllEnter(Sender: TObject);
    procedure cxDBTextEditAllExit(Sender: TObject);
    procedure EnterKakoTab(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    //procedure PrvPosledenTab(panel:TPanel;var posledna,prva:TWinControl);
    procedure SaveToIniFileExecute(Sender: TObject);
    procedure cxGrid1DBTableView1KeyPress(Sender: TObject; var Key: Char);
    procedure aZapisiExecute(Sender: TObject);
    procedure aOtkaziExecute(Sender: TObject);
    procedure aNovExecute(Sender: TObject);
    procedure aAzurirajExecute(Sender: TObject);
    procedure aBrisiExecute(Sender: TObject);
    procedure aZacuvajExcelExecute(Sender: TObject);
    procedure aRefreshExecute(Sender: TObject);
    procedure aSnimiIzgledExecute(Sender: TObject);
    procedure aIzlezExecute(Sender: TObject);
    procedure aPecatiTabelaExecute(Sender: TObject);
    procedure aSnimiPecatenjeExecute(Sender: TObject);
    procedure zacuvajPrintVoIni(ime:AnsiString);
    procedure procitajPrintOdIni(ime:AnsiString);
    procedure aPodesuvanjePecatenjeExecute(Sender: TObject);
    procedure aBrisiPodesuvanjePecatenjeExecute(Sender: TObject);
    procedure aPageSetupExecute(Sender: TObject);
    procedure aFormConfigExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure cxBarEditItem1PropertiesChange(Sender: TObject);
    procedure aBrisiIzgledExecute(Sender: TObject);
    procedure aSpustiSoberiExecute(Sender: TObject);
    procedure aMagacinExecute(Sender: TObject);
    procedure aResetNurkovciExecute(Sender: TObject);
    procedure tblEvidencijaBeforeOpen(DataSet: TDataSet);
    procedure aEditStavkaExecute(Sender: TObject);
    procedure cxGrid1DBTableView1DblClick(Sender: TObject);
    procedure cxGrid1DBTableView1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure cxExtProizvodEnter(Sender: TObject);
    procedure aLikvidirajExecute(Sender: TObject);
    procedure aGenerirajKnigCeniExecute(Sender: TObject);
    procedure aBrisiKnigCeniExecute(Sender: TObject);
    procedure cxGrid1DBTableView1StylesGetContentStyle(
      Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord;
      AItem: TcxCustomGridTableItem; var AStyle: TcxStyle);
    procedure aBrisiNalogExecute(Sender: TObject);
    procedure aPecatiPotrosokExecute(Sender: TObject);
    procedure aDizajnPotrosokExecute(Sender: TObject);
    procedure aPecatiPotrosokKnigExecute(Sender: TObject);
    procedure aDizajnPotrosokKnigExecute(Sender: TObject);
    procedure aAzurirajNalogExecute(Sender: TObject);
    procedure cxGrid1DBTableView1FocusedRecordChanged(
      Sender: TcxCustomGridTableView; APrevFocusedRecord,
      AFocusedRecord: TcxCustomGridRecord;
      ANewItemRecordFocusingChanged: Boolean);
    procedure PodesiLikvidirana(likvidirana : boolean);
    procedure aPotrosenoMatVS_SumiranoExecute(Sender: TObject);
  private
    { Private declarations }

    _sifra_kluc :array[1..10] of string;  // �������� ���������� �� ������ �� ������� �� ������������� ���

  protected
    list : TList;
    inserting, sobrano : boolean;
    prva, posledna :TWinControl;
    procedure prefrli;

  public
    { Public declarations }
    mod_rabota : integer;

    constructor Create(Owner : TComponent; insert : boolean = true);reintroduce; overload;

    procedure SetSifra(br: integer; s : string);
    function GetSifra(br: integer) : string;

    //������� �� ���������� ������������ _sifra_kluc
    property sifra_kluc[br :integer]: string read GetSifra write SetSifra;
  end;

var
  frmEvidencija: TfrmEvidencija;
  rData : TRepositoryData;

implementation

uses DaNe, dmKonekcija, Utils, FormConfig, dmMaticni, Magacin, dmUnit, NurkoRepository,
  EvidencijaPotroseniMaterijali;

{$R *.dfm}
//------------------------------------------------------------------------------

constructor TfrmEvidencija.Create(Owner : TComponent; insert : boolean = true);
begin
inherited Create(Owner);
//	�������� ���� ������� � �������� �� ����������� ���������
//	insert = true -> ����� ���� ����� ���������� �� ������� �� Insert Mode
//  insert = false -> ����� ���� ����� ������� �� ����� �� ������. �� ������� ����� ����� �� �� �������� F5
  inserting := insert;
end;

//	����� �� �����������. �� ��������� ������� �� ������, �� �������� ������� �� ���� ��������,
//	�� ������� ������� �� ������ ������ � �� ������� �� Insert Mode
procedure TfrmEvidencija.aNovExecute(Sender: TObject);
begin
  if(cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) then
  begin
    dPanel.Enabled:=True;
    lPanel.Enabled:=False;
    prva.SetFocus;
    cxGrid1DBTableView1.DataController.DataSet.Insert;

    qryZemiBroj.Close;
    qryZemiBroj.ParamByName('god').Value := godina;
    qryZemiBroj.ParamByName('re').Value := dmMat.tblReMagacinID.Value;
    if (mod_rabota = 0) then
      qryZemiBroj.ParamByName('tip').Value := 14
    else
      qryZemiBroj.ParamByName('tip').Value := 15;
    qryZemiBroj.ExecQuery;
    tblEvidencijaBROJ.Value := qryZemiBroj.FieldValue('maks', false) + 1;

    if (mod_rabota = 0) then
      tblEvidencijaTIP_DOK.Value := 14
    else
      tblEvidencijaTIP_DOK.Value := 15;
    tblEvidencijaRE.Value := dmMat.tblReMagacinID.Value;
    tblEvidencijaGODINA.AsString := godina;
    tblEvidencijaDATUM.Value := Date;
  end
  else ShowMessage('������ �������� �� ��� �������� �� �� ��������� �������,' + sLineBreak + '� ����� ������� �� ������� ��� �����!');
end;

//	����� �� ���������. �� ��������� ������� �� ������, �� �������� ������� �� ���� ��������,
//	�� ������� ������� �� ������ ������ � �� ������� �� Edit Mode
procedure TfrmEvidencija.aAzurirajExecute(Sender: TObject);
begin
  if(cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) then
  begin
    dPanel.Enabled:=True;
    lPanel.Enabled:=False;
    prva.SetFocus;
    cxGrid1DBTableView1.DataController.DataSet.Edit;
  end
  else ShowMessage('������ �������� �� ��� �������� �� �� ��������� �������,' + sLineBreak + '� ����� ������� �� ���������!');
end;

//	����� �� ������ �� ������������� �����
procedure TfrmEvidencija.aAzurirajNalogExecute(Sender: TObject);
begin
  if ((tblEvidencija.RecordCount > 0) and (tblEvidencija.State = dsBrowse)) then
  begin
    dPanel.Enabled:=True;
    lPanel.Enabled:=False;

    tblEvidencija.Edit;

    lcbNalog.SetFocus;
  end;
end;

procedure TfrmEvidencija.aBrisiExecute(Sender: TObject);
begin
  if (cxGrid1DBTableView1.DataController.RecordCount > 0) then
  begin
    if (cxGrid1DBTableView1.DataController.DataSource.State in [dsBrowse, dsInactive]) then
    begin
      frmDaNe := TfrmDaNe.Create(self, '�������', '����������� � ���� ��������� �� ����� ���������. ���� ������ �� ����������?', 1);
      if (frmDaNe.ShowModal = mrYes) then
      begin
        spBrisiEvidencija.Close;
        spBrisiEvidencija.ParamByName('in_mtr_out_id').Value := tblEvidencijaID.Value;
        spBrisiEvidencija.ExecProc;

        ShowMessage('����������� � ������� ���������!');
        tblEvidencija.FullRefresh;
      end;
    end
    else ShowMessage('������ �������� �� ��� �������� �� �� ��������� �������,' + sLineBreak + '� ����� ������� �� �� ��������� �����������!');
  end;
end;

procedure TfrmEvidencija.aBrisiIzgledExecute(Sender: TObject);
begin
  brisiGridVoBaza(Name,cxGrid1DBTableView1);
//  brisiPivotVoBaza(Name,cxDBPivotGrid1); �� pivot ��� ���
  BrisiFormaIzgled(self);
end;

procedure TfrmEvidencija.aBrisiKnigCeniExecute(Sender: TObject);
begin
  if (tblEvidencijaNALOG.isNull) then
  begin
    dm.spBrisiKnigCeni.Close;
    dm.spBrisiKnigCeni.ParamByName('IN_IN_ID').Clear;
    dm.spBrisiKnigCeni.ParamByName('IN_OUT_ID').Value := tblEvidencijaID.Value;
    dm.spBrisiKnigCeni.ExecQuery;

    ShowMessage('����. ������ �� ������� � ���������� �� � ����������!');

    tblEvidencija.Refresh;
  end
  else
    MessageError('����������� ��� ����� � �� � ��������� ������ �� ����. ������!');
end;

procedure TfrmEvidencija.aBrisiNalogExecute(Sender: TObject);
begin
  if ((tblEvidencija.RecordCount > 0) and (tblEvidencija.State = dsBrowse)) then
  begin
    tblEvidencija.Edit;
    tblEvidencijaNALOG.Clear;
    tblEvidencija.Post;

    ShowMessage('������� � �������� �� �����������!');
  end;
end;

//	����� �� ���������� �� ����������
procedure TfrmEvidencija.aRefreshExecute(Sender: TObject);
begin
  cxGrid1DBTableView1.DataController.Filter.Root.Clear; //���������� �� ��������
  cxGrid1DBTableView1.DataController.DataSet.Refresh;
end;

procedure TfrmEvidencija.aResetNurkovciExecute(Sender: TObject);
begin
    //�������� �� ���������� �� ������ ����� ������ ��������� �� ������� �� ��������� �� sql-��
   dmRes.FreeRepository(rData);
   rData := TRepositoryData.Create();

   dmRes.setParam(':MAGACIN.ID',IntToStr(dmMat.tblReMagacinID.Value));
   //if (cxExtNalog.EditValue <> Null) then
   dmRes.setParam(':WO.ID','%');

   cxExtNalog.RepositoryItem := dmRes.InitRepositoryRefresh(-209, 'cxExtNalog', Name, rData);
   cxExtProizvod.RepositoryItem := dmRes.InitRepositoryRefresh(-218, 'cxExtProizvod', Name, rData);
end;

//	����� �� ����� �� ������� �� ������� �� ������� �����
procedure TfrmEvidencija.aIzlezExecute(Sender: TObject);
begin
  Close;
end;

procedure TfrmEvidencija.aLikvidirajExecute(Sender: TObject);
begin
  if (tblEvidencija.RecordCount > 0) then
  begin
    if (tblEvidencijaLIKVIDIRAN.Value = 0) then
    begin
      if (tblEvidencija.State = dsBrowse) then
      begin
        frmDaNe := TfrmDaNe.Create(self, '�������!', '������ �� ���������� �� ����������(�������� ����. ����)?', 0);
        if (frmDaNe.ShowModal = mrYes) then
        begin
          aGenerirajKnigCeni.Execute;

          tblEvidencija.Edit;
          tblEvidencijaLIKVIDIRAN.Value := 1;
          tblEvidencijaLIKVIDATOR.Value := dmKon.user;
          tblEvidencijaLIKVIDIRAN_NA.Value := Now;
          tblEvidencija.Post;
        end;
      end
      else ShowMessage('������ �������� �� ��� �������� �� �� ��������� �������,' + sLineBreak + '� ����� ������� ��������!');
    end
    else ShowMessage('������! ���������� � ��� ����������!');
  end;
end;

procedure TfrmEvidencija.aMagacinExecute(Sender: TObject);
begin
  frmMagacin := TfrmMagacin.Create(nil);
  frmMagacin.ShowModal;
  frmMagacin.Free;

  if (mod_rabota = 0) then
    Caption := '��������� �� ��������� ��������� �� ������� : ' + IntToStr(dmMat.tblReMagacinID.Value) + ' - ' + dmMat.tblReMagacinNAZIV.Value
  else
    Caption := '��������� �� ���/����/������ �� ������� : ' + IntToStr(dmMat.tblReMagacinID.Value) + ' - ' + dmMat.tblReMagacinNAZIV.Value;

  aResetNurkovci.Execute; //�������� �� ���������� �� ������ ��������� �� ���������
  tblEvidencija.FullRefresh;
end;

//	����� �� ������� �� �������� �� ������ �� ���� (Utils.pas)
procedure TfrmEvidencija.aSnimiIzgledExecute(Sender: TObject);
begin
  zacuvajGridVoBaza(Name,cxGrid1DBTableView1);
  ZacuvajFormaIzgled(self);
end;

//	����� �� ������� �� ������ �� Excel ������ (Utils.pas)
procedure TfrmEvidencija.aZacuvajExcelExecute(Sender: TObject);
begin
  zacuvajVoExcel(cxGrid1, Caption);
end;

//  ��������� �� ������ �� ������� �� ���������� �� ����� �� Enter
procedure TfrmEvidencija.EnterKakoTab(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  kom : TWinControl;
begin
    kom := Sender as TWinControl;
    case Key of
        VK_DOWN:
        begin
          PostMessage(Handle,WM_NextDlgCtl,0,0);
          key:=0;
        end;
        VK_UP:
        begin
          PostMessage(Handle,WM_NextDlgCtl,1,0);
          key:=0;
        end;
        VK_RETURN:
        begin
          PostMessage(Handle,WM_NextDlgCtl,0,0);
        end;
        VK_INSERT:
        begin
          if (kom = cxExtNalog) then
          begin
            frmNurkoRepository := TfrmNurkoRepository.Create(nil,Name,rData);
            frmNurkoRepository.kontrola_naziv := 'cxExtNalog';
            frmNurkoRepository.ShowModal;

            if (frmNurkoRepository.ModalResult = mrOk) then
            begin
              cxExtNalog.EditValue := frmNurkoRepository.cxGrid1Level1.GridView.DataController.GetRecordId(frmNurkoRepository.cxGrid1Level1.GridView.DataController.FocusedRecordIndex);
            end;

            frmNurkoRepository.Free;
          end;

          if (kom = cxExtProizvod) then
          begin
            frmNurkoRepository := TfrmNurkoRepository.Create(nil,Name,rData);
            frmNurkoRepository.kontrola_naziv := 'cxExtProizvod';
            frmNurkoRepository.ShowModal;

            if (frmNurkoRepository.ModalResult = mrOk) then
            begin
              cxExtProizvod.EditValue := frmNurkoRepository.cxGrid1Level1.GridView.DataController.GetRecordId(frmNurkoRepository.cxGrid1Level1.GridView.DataController.FocusedRecordIndex);
            end;

            frmNurkoRepository.Free;
          end;

        end;
    end;
end;

//	������� �� ��� �� ������ ��� �������� �� �����
procedure TfrmEvidencija.cxBarEditItem1PropertiesChange(Sender: TObject);
begin
  dmRes.SkinPromeni(Sender);
  dxRibbon1.ColorSchemeName := dmRes.skin_name;
end;

procedure TfrmEvidencija.cxDBTextEditAllEnter(Sender: TObject);
begin
    TEdit(Sender).Color:=clSkyBlue;
end;

//	������� �� ��� �� ������ ��� ����� �� �����
procedure TfrmEvidencija.cxDBTextEditAllExit(Sender: TObject);
begin
    TEdit(Sender).Color:=clWhite;
end;

procedure TfrmEvidencija.cxExtProizvodEnter(Sender: TObject);
begin
  if (cxExtNalog.EditValue <> Null) then
  begin
    dmRes.setParam(':WO.ID',cxExtNalog.EditValue);
    dmRes.NurkoRefresh('cxExtProizvod', Name, rData);
  end;
end;

procedure TfrmEvidencija.cxGrid1DBTableView1DblClick(Sender: TObject);
begin
  if (tblEvidencija.VisibleRecordCount > 0) then aEditStavka.Execute;
end;

procedure TfrmEvidencija.cxGrid1DBTableView1FocusedRecordChanged(
  Sender: TcxCustomGridTableView; APrevFocusedRecord,
  AFocusedRecord: TcxCustomGridRecord; ANewItemRecordFocusingChanged: Boolean);
begin
  PodesiLikvidirana(tblEvidencijaLIKVIDIRAN.Value = 1);
end;

procedure TfrmEvidencija.PodesiLikvidirana(likvidirana : boolean);
begin
//
  if (likvidirana = true) then
  begin
    aAzuriraj.Enabled := false;
    aBrisi.Enabled := false;
  end
  else
  begin
    aAzuriraj.Enabled := true;
    aBrisi.Enabled := true;
  end;
end;

//  ��������� �� ������ �� ��������� �� ���������� ���� �� ��������
//  ��������� ��: br - ����� ��� �� ������ (�� 0 �� 9) �� ��������� �� ����� �� ����� �� ���������� ����
//  s - ��������� �� ������ �� ������ (���������� �� string �� ����� �� integer �� IntToStr ���������)
procedure TfrmEvidencija.SetSifra(br: integer; s : string);
begin
  _sifra_kluc[br] := s;
end;

procedure TfrmEvidencija.tblEvidencijaBeforeOpen(DataSet: TDataSet);
begin
  tblEvidencija.ParamByName('magacin').Value := dmMat.tblReMagacinID.Value;
  tblEvidencija.ParamByName('god').Value := godina;
end;

//  ������� �� ������ �� ��������� �� ���������� ���� �� ��������
//  ��������� ��: br - ����� ��� �� ������ (�� 0 �� 9) �� ��������� �� ����� �� ����� �� ���������� ����
function TfrmEvidencija.GetSifra(br: integer) : string;
begin
  Result := _sifra_kluc[br];
end;

//  �� ������� �� ������� ��� ����������� �� Master-�� �� �������� �� ��������� �� ������ ����� SetSifra
procedure TfrmEvidencija.prefrli;
begin
end;

procedure TfrmEvidencija.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    //	��������� ��� ��������� �� �������
    if (cxGrid1DBTableView1.DataController.DataSource.State = dsEdit) or (cxGrid1DBTableView1.DataController.DataSource.State = dsInsert) then
    begin
        frmDaNe := TfrmDaNe.Create(self, '���������� ��������', '���������� �� �� ��������. ���� ������ �� �� �������?', 1);
        if (frmDaNe.ShowModal <> mrYes) then
        begin
            cxGrid1DBTableView1.DataController.DataSet.Cancel;
            Action := caFree;
            dmRes.FreeRepository(rData);
        end
        else
          if (Validacija(dPanel) = false) then
          begin
            cxGrid1DBTableView1.DataController.DataSet.Post;
            Action := caFree;
            dmRes.FreeRepository(rData);
          end
          else Action := caNone;
    end
    else
      dmRes.FreeRepository(rData);
end;

procedure TfrmEvidencija.FormCreate(Sender: TObject);
begin
  ProcitajFormaIzgled(self);
  if (dmRes.use_skin = true) then dxRibbon1.ColorSchemeName := dmRes.skin_name;

  rData := TRepositoryData.Create();  // kreirame instance od klasata TRepositoryData sto se naogja vo dmRes
end;

//------------------------------------------------------------------------------

procedure TfrmEvidencija.FormShow(Sender: TObject);
begin
// ������� �� ������������ ������������/���������� �� ���������� �� ���� �����
    SpremiForma(self);
//	�� ��������� �� ������� ������ ��� ���������� � ���� � ��� �������� �� �������
    PrvPosledenTab(dPanel,posledna,prva);
    //dxBarManager1Bar1.Caption := Caption;
    dxComponentPrinter1Link1.ReportTitle.Text := Caption;

    if (mod_rabota = 0) then
      Caption := '��������� �� ��������� ��������� �� ������� : ' + IntToStr(dmMat.tblReMagacinID.Value) + ' - ' + dmMat.tblReMagacinNAZIV.Value
    else
      Caption := '��������� �� ���/����/������ �� ������� : ' + IntToStr(dmMat.tblReMagacinID.Value) + ' - ' + dmMat.tblReMagacinNAZIV.Value;

//	������� �� ������������ �� ������ �� ������
    procitajGridOdBaza(Name,cxGrid1DBTableView1,false,false);
//	������� �� ������������ �� ��������� �� ������
    procitajPrintOdBaza(Name,cxGrid1DBTableView1.Name, dxComponentPrinter1Link1);

    tblEvidencija.Close;
    if (mod_rabota = 0) then
      tblEvidencija.ParamByName('TIP').Value := 14
    else
      tblEvidencija.ParamByName('TIP').Value := 15;
    tblEvidencija.Open;

    sobrano := true;

    dmRes.setParam(':MAGACIN.ID',IntToStr(dmMat.tblReMagacinID.Value));

    dmRes.setParam(':ID_RE',IntToStr(dmKon.UserRE));
    cxExtNalog.RepositoryItem := dmRes.InitRepository(-209, 'cxExtNalog', Name, rData);

    dmRes.setParam(':WO.ID','%');
    cxExtProizvod.RepositoryItem := dmRes.InitRepository(-218, 'cxExtProizvod', Name, rData);

    if (mod_rabota <> 0) then
    begin
      aPecatiPotrosok.Caption := '������ ���/����/������';
      aPecatiPotrosokKnig.Caption := '������ ���/����/������ (�� ����. ����)';
    end;
end;
//------------------------------------------------------------------------------

procedure TfrmEvidencija.SaveToIniFileExecute(Sender: TObject);
begin
    cxGrid1DBTableView1.StoreToIniFile(Name,True,[]);
end;
//------------------------------------------------------------------------------

procedure TfrmEvidencija.cxGrid1DBTableView1KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  if ((Key = VK_RETURN) and (tblEvidencija.VisibleRecordCount > 0)) then aEditStavka.Execute;
end;

procedure TfrmEvidencija.cxGrid1DBTableView1KeyPress(Sender: TObject;
  var Key: Char);
begin
  if(Ord(Key) <> VK_RETURN) then cxSortiraj(cxGrid1DBTableView1);
end;

procedure TfrmEvidencija.cxGrid1DBTableView1StylesGetContentStyle(
  Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord;
  AItem: TcxCustomGridTableItem; var AStyle: TcxStyle);
var
  AStatus, AKontroliran, ALikvidiran : Integer;
begin
  AStatus:=0;
  AKontroliran:=0;
  ALikvidiran:=0;

  if (not VarIsNull(TcxCustomGridRecord(ARecord).Values[cxGrid1DBTableView1STORNO_FLAG.Index])) and (not(ARecord is TcxGridGroupRow))
  then
  begin
    AStatus := TcxCustomGridRecord(ARecord).Values[cxGrid1DBTableView1STORNO_FLAG.Index];
  end
  else AStatus := 0;

  if (not VarIsNull(TcxCustomGridRecord(ARecord).Values[cxGrid1DBTableView1KONTROLIRAN.Index])) and (not(ARecord is TcxGridGroupRow))
  then
  begin
    AKontroliran := TcxCustomGridRecord(ARecord).Values[cxGrid1DBTableView1KONTROLIRAN.Index];
  end
  else AKontroliran := 0;

  if (not VarIsNull(TcxCustomGridRecord(ARecord).Values[cxGrid1DBTableView1LIKVIDIRAN.Index])) and (not(ARecord is TcxGridGroupRow))
  then
  begin
    ALikvidiran := TcxCustomGridRecord(ARecord).Values[cxGrid1DBTableView1LIKVIDIRAN.Index];
  end
  else ALikvidiran := 0;

  case AStatus of
//     0:AStyle:= dmRes.cxStyle1;
     1:AStyle:= dmRes.RedLight;
  end;

  if AStatus = 0 then
  begin
    if ALikvidiran = 1 then AStyle:= dmRes.Golden
    else
    begin
      if AKontroliran = 1 then AStyle:= dmRes.MoneyGreen;
    end;
  end;
end;

//  ����� �� �����
procedure TfrmEvidencija.aZapisiExecute(Sender: TObject);
var
  st: TDataSetState;
begin
  if(cxGrid1DBTableView1.DataController.DataSource.State <> dsBrowse) then
  begin
    ZapisiButton.SetFocus;

    st := cxGrid1DBTableView1.DataController.DataSet.State;
    if st in [dsEdit,dsInsert] then
    begin
      if (Validacija(dPanel) = false) then
      begin
        if (not tblEvidencijaWO_REF_ID.IsNull) then
        begin
          qryZemiReRabNalog.Close;
          qryZemiReRabNalog.ParamByName('WO_ID').Value := tblEvidencijaWO_REF_ID.Value;
          qryZemiReRabNalog.ExecQuery;
          tblEvidencijaRE2.Value := qryZemiReRabNalog.FieldValue('ID_RE',false);
        end
        else tblEvidencijaRE2.Clear;

        if ((st = dsInsert) and inserting) then
        begin
          cxGrid1DBTableView1.DataController.DataSet.Post;

          dmRes.setParam(':WO.ID','%');
          dmRes.NurkoRefresh('cxExtProizvod', Name, rData);

          aNov.Execute;
        end;

        if ((st = dsInsert) and (not inserting)) then
        begin
          cxGrid1DBTableView1.DataController.DataSet.Post;
          dPanel.Enabled:=false;
          lPanel.Enabled:=true;
          cxGrid1.SetFocus;

          dmRes.setParam(':WO.ID','%');
          dmRes.NurkoRefresh('cxExtProizvod', Name, rData);

          aEditStavka.Execute;
        end;

        if (st = dsEdit) then
        begin
          cxGrid1DBTableView1.DataController.DataSet.Post;
          dPanel.Enabled:=false;
          lPanel.Enabled:=true;
          cxGrid1.SetFocus;

          dmRes.setParam(':WO.ID','%');
          dmRes.NurkoRefresh('cxExtProizvod', Name, rData);

          aEditStavka.Execute;
        end;
      end;


    end;

  end;
end;

//	����� �� ���������� �� �������
procedure TfrmEvidencija.aOtkaziExecute(Sender: TObject);
begin
  if (cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) then
  begin
      ModalResult := mrCancel;
      Close();
  end
  else
  begin
      cxGrid1DBTableView1.DataController.DataSet.Cancel;
      RestoreControls(dPanel);
      dPanel.Enabled := false;
      lPanel.Enabled := true;
      cxGrid1.SetFocus;

      dmRes.setParam(':WO.ID','%');
      dmRes.NurkoRefresh('cxExtProizvod', Name, rData);
  end;
end;

//----------------------------------------------------------------------------------
// ��������� �� ��������, ���������� �� ��������� � �������� �� Print ��������
//----------------------------------------------------------------------------------

//	����� �� �������� �� �������� �� �������
procedure TfrmEvidencija.aPageSetupExecute(Sender: TObject);
begin
  dxComponentPrinter1Link1.PageSetup;
end;

//	����� �� ������� �� ������
procedure TfrmEvidencija.aPecatiPotrosokKnigExecute(Sender: TObject);
begin
  if cxGrid1DBTableView1.DataController.RecordCount > 0 then
  begin
    try
      dmRes.Spremi('MTR',9045);

      dmKon.tblSqlReport.ParamByName('id').Value := tblEvidencijaID.Value;
      dmKon.tblSqlReport.Open;

      dmRes.frxReport1.Variables.AddVariable('INPUT','MOD_RABOTA', mod_rabota);

      //dmRes.frxReport1.Variables.AddVariable('DATUM','DATA_OD', OdDatum.Date);
      //dmRes.frxReport1.Variables.AddVariable('DATUM','DATA_DO', DoDatum.Date);

      //dmRes.frxReport1.PrintOptions.ShowDialog := false;
      dmRes.frxReport1.ShowReport();
    except
      ShowMessage('�� ���� �� �� ������� ���������!');
    end;
  end;
end;

procedure TfrmEvidencija.aPecatiTabelaExecute(Sender: TObject);
begin
  dxComponentPrinter1Link1.ReportTitle.Text := '��������� �� ��������� ���������';

  dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Clear;
  dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Add(dmkon.firma_naziv);
  dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Add(DateTimeToStr(Now));

  dxComponentPrinter1Link1.PrinterPage.PageHeader.LeftTitle.Clear;
  dxComponentPrinter1Link1.PrinterPage.PageHeader.LeftTitle.Add('������� : ' + IntToStr(dmMat.tblReMagacinID.Value) + ' - ' + dmMat.tblReMagacinNAZIV.Value);

  dxComponentPrinter1.Preview(true, dxComponentPrinter1Link1);
end;

procedure TfrmEvidencija.aPecatiPotrosokExecute(Sender: TObject);
begin
  if cxGrid1DBTableView1.DataController.RecordCount > 0 then
  begin
    try
      dmRes.Spremi('MTR',9027);

      dmKon.tblSqlReport.ParamByName('id').Value := tblEvidencijaID.Value;
      dmKon.tblSqlReport.Open;

      dmRes.frxReport1.Variables.AddVariable('INPUT','MOD_RABOTA', mod_rabota);


      //dmRes.frxReport1.Variables.AddVariable('DATUM','DATA_DO', DoDatum.Date);

      //dmRes.frxReport1.PrintOptions.ShowDialog := false;
      dmRes.frxReport1.ShowReport();
    except
      ShowMessage('�� ���� �� �� ������� ���������!');
    end;
  end;
end;

//	����� �� ���������� �� ��������� �� �������
procedure TfrmEvidencija.aPodesuvanjePecatenjeExecute(Sender: TObject);
begin
  dxComponentPrinter1Link1.DesignReport();
end;

procedure TfrmEvidencija.aPotrosenoMatVS_SumiranoExecute(Sender: TObject);
begin
//if not (tblEvidencija.State in [dsEdit,dsInsert]) and (not tblEvidencijaWO_S_REF_ID.IsNull)  then
// begin
//  dmRes.Spremi('MAN', 5);
//  dmKon.tblSqlReport.ParamByName('id_rn').AsInteger :=tblEvidencijaWO_REF_ID.Value;
//  if not tblEvidencijaWO_S_REF_ID.IsNull then
//     dmKon.tblSqlReport.ParamByName('id').AsInteger :=tblEvidencijaWO_S_REF_ID.Value;
// // else
// //   dmKon.tblSqlReport.ParamByName('id').AsString := '%';
//  dmKon.tblSqlReport.Open;
//
//  dmRes.frxReport1.Variables.Variables['broj']:=QuotedStr(tblEvidencijaRN_BROJ_GOD.AsString);
//  dmRes.frxReport1.Variables.Variables['datum']:=tblEvidencijaDATUM.AsDateTime;
//
//  dmRes.frxReport1.ShowReport();
// end;


if not (tblEvidencija.State in [dsEdit,dsInsert]) and (not tblEvidencijaWO_S_REF_ID.IsNull)  then
 begin
  dmRes.Spremi('MAN', 3);
  dmKon.tblSqlReport.ParamByName('id_rn').AsInteger :=tblEvidencijaWO_REF_ID.Value;
  if not tblEvidencijaWO_S_REF_ID.IsNull then
     dmKon.tblSqlReport.ParamByName('id').AsInteger :=tblEvidencijaWO_S_REF_ID.Value;
//  else
//    dmKon.tblSqlReport.ParamByName('id').AsString := '%';
  dmKon.tblSqlReport.Open;

  dmRes.frxReport1.Variables.Variables['broj']:=QuotedStr(tblEvidencijaRN_BROJ_GOD.AsString);
  dmRes.frxReport1.Variables.Variables['datum']:=tblEvidencijaDATUM.AsDateTime;

  dmRes.frxReport1.ShowReport();
 end
 else
 if not (tblEvidencija.State in [dsEdit,dsInsert]) and (tblEvidencijaWO_S_REF_ID.IsNull)  then
 begin
   dmRes.Spremi('MAN', 5);
   dmKon.tblSqlReport.ParamByName('id_rn').AsInteger :=tblEvidencijaWO_REF_ID.Value;
 // if not tblEvidencijaWO_S_REF_ID.IsNull then
 //    dmKon.tblSqlReport.ParamByName('id').AsInteger :=tblEvidencijaWO_S_REF_ID.Value;
//  else
    dmKon.tblSqlReport.ParamByName('id').AsString := '%';
   dmKon.tblSqlReport.Open;

   dmRes.frxReport1.Variables.Variables['broj']:=QuotedStr(tblEvidencijaRN_BROJ_GOD.AsString);
   dmRes.frxReport1.Variables.Variables['datum']:=tblEvidencijaDATUM.AsDateTime;

   dmRes.frxReport1.ShowReport();
 end;
end;

procedure TfrmEvidencija.aSnimiPecatenjeExecute(Sender: TObject);
begin
  zacuvajPrintVoBaza(Name,cxGrid1DBTableView1.Name,dxComponentPrinter1Link1);
end;

//	����� �� �������� ��� �������� �� �������� �� ������ ���� ��� ��������� �� ����� ������
procedure TfrmEvidencija.aSpustiSoberiExecute(Sender: TObject);
begin
  if (sobrano = true) then
  begin
    cxGrid1DBTableView1.ViewData.Expand(false);
    sobrano := false;
  end
  else
  begin
    cxGrid1DBTableView1.ViewData.Collapse(false);
    sobrano := true;
  end;
end;

//	����� �� ������ �� ����������� �������� �� ������� (Utils.pas)
procedure TfrmEvidencija.aBrisiPodesuvanjePecatenjeExecute(Sender: TObject);
begin
  brisiPrintOdBaza(Name,cxGrid1DBTableView1.Name, dxComponentPrinter1Link1);
end;

procedure TfrmEvidencija.aDizajnPotrosokExecute(Sender: TObject);
begin
  try
    dmRes.Spremi('MTR',9027);
    dmRes.frxReport1.DesignReport();
  except
    ShowMessage('�� ���� �� �� ������� ���������!');
  end;
end;

procedure TfrmEvidencija.aDizajnPotrosokKnigExecute(Sender: TObject);
begin
  try
    dmRes.Spremi('MTR',9045);
    dmRes.frxReport1.DesignReport();
  except
    ShowMessage('�� ���� �� �� ������� ���������!');
  end;
end;

procedure TfrmEvidencija.aEditStavkaExecute(Sender: TObject);
begin
 if (tblEvidencija.RecordCount > 0) then
  begin
    if(tblEvidencija.State = dsBrowse) then
     begin
           frmEvidencijaPotroseniMaterijali := TfrmEvidencijaPotroseniMaterijali.Create(self, true, mod_rabota);
           if mod_rabota = 1 then
           begin
              frmEvidencijaPotroseniMaterijali.dxBarLargeButton26.Visible := ivNever;
            //  frmEvidencijaPotroseniMaterijali.Panel1.Visible := False;
              frmEvidencijaPotroseniMaterijali.cxGrid1DBTableView1KOLICINA_N.Visible := False;
           end
           else
           begin
             frmEvidencijaPotroseniMaterijali.cxGridDBTableView1RASTUR.Visible := False;
           end;

           frmEvidencijaPotroseniMaterijali.evidencija_id := tblEvidencijaID.Value;
           frmEvidencijaPotroseniMaterijali.e_br := tblEvidencijaBROJ.Value;
           frmEvidencijaPotroseniMaterijali.e_god := tblEvidencijaGODINA.Value;
           frmEvidencijaPotroseniMaterijali.e_datum := tblEvidencijaDATUM.Value;
           frmEvidencijaPotroseniMaterijali.e_likvidirana := tblEvidencijaLIKVIDIRAN.Value;

           frmEvidencijaPotroseniMaterijali.tblOutStavka.Close;
           frmEvidencijaPotroseniMaterijali.tblOutStavka.ParamByName('EVIDENCIJA_ID').Value := tblEvidencijaID.Value;

           frmEvidencijaPotroseniMaterijali.tblOutStavka.Open;

            dm.tblPogledNormativ.Close;
            dm.tblPogledNormativ.ParamByName('id').Value := tblEvidencijaID.Value;
            dm.tblPogledNormativ.Open;

           frmEvidencijaPotroseniMaterijali.ShowModal;
           frmEvidencijaPotroseniMaterijali.Free;


                   //     end;

      //        end;
                      // frmEvidencijaPotroseniMaterijali.ShowModal;
                     //  frmEvidencijaPotroseniMaterijali.Free;
    end
    else ShowMessage('������ �������� �� ��� �������� �� �� ��������� �������,' + sLineBreak + '� ����� ������� ��������!');
  end;
end;

//  ����� �� ����� �� ����� �� ������������� �� �������� � ��������������� ��
//  �������(������ ���������� ��������)
procedure TfrmEvidencija.aFormConfigExecute(Sender: TObject);
begin
  frmFormConfig := TfrmFormConfig.Create(Application);
  frmFormConfig.formPtr := Addr(Self);
  frmFormConfig.ShowModal;
  frmFormConfig.Free;
end;

procedure TfrmEvidencija.aGenerirajKnigCeniExecute(Sender: TObject);
begin
  if (tblEvidencija.RecordCount > 0) then
  begin
    dm.spGenerirajKnigOutCeni.Close;
    dm.spGenerirajKnigOutCeni.ParamByName('IN_OUT_ID').Value := tblEvidencijaID.Value;
    dm.spGenerirajKnigOutCeni.ExecProc;

    ShowMessage('����. ������ �� ��������� �� ����������!');
  end;
end;

//----------------------------------------------------------------------------------
// ���� -> ��������� �� ��������, ���������� �� ��������� � �������� �� Print ��������
//----------------------------------------------------------------------------------

// ��������� �� ���������� �� ��������� � �������� �� Print �������� �� .ini ��������
procedure TfrmEvidencija.zacuvajPrintVoIni(ime:AnsiString);
var
  AStream: TFileStream;
begin
  AStream := TFileStream.Create(dmKon.RabotenDir + ime + 'Print.ini', fmCreate);
  try
    AStream.WriteComponent(dxComponentPrinter1Link1);
  finally
    AStream.Free;
  end;

end;

// ��������� �� ������ �� ��������� � �������� �� Print �������� �������� �� .ini ��������
procedure TfrmEvidencija.procitajPrintOdIni(ime:AnsiString);
var
  AStream: TFileStream;
begin
  AStream := TFileStream.Create(dmKon.RabotenDir + ime + 'Print.ini', fmOpenRead);
  try
    dxComponentPrinter1Link1.RestoreDefaults;
    AStream.ReadComponent(dxComponentPrinter1Link1);
  finally
    AStream.Free;
  end;
end;

end.
