object frmHomogena: TfrmHomogena
  Left = 0
  Top = 0
  HorzScrollBar.ParentColor = False
  BorderIcons = [biSystemMenu, biMaximize]
  Caption = #1042#1085#1077#1089#1091#1074#1072#1114#1077' '#1085#1072' '#1073#1088#1086#1112' '#1085#1072' '#1087#1072#1088#1095#1080#1114#1072' '#1074#1086' '#1087#1072#1082#1091#1074#1072#1095#1082#1072' '#1082#1091#1090#1080#1112#1072
  ClientHeight = 445
  ClientWidth = 796
  Color = clBtnFace
  DefaultMonitor = dmDesktop
  Font.Charset = RUSSIAN_CHARSET
  Font.Color = clWindowText
  Font.Height = -15
  Font.Name = 'Tahoma'
  Font.Style = []
  FormStyle = fsMDIForm
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  ShowHint = True
  OnClose = FormClose
  OnCreate = FormCreate
  OnKeyDown = FormKeyDown
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 18
  object lPanel: TPanel
    Left = 0
    Top = 0
    Width = 796
    Height = 445
    Align = alClient
    BevelInner = bvLowered
    BevelOuter = bvSpace
    TabOrder = 0
    object cxGrid1: TcxGrid
      Left = 2
      Top = 2
      Width = 792
      Height = 441
      Align = alClient
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      Visible = False
      OnMouseEnter = cxGrid1DBTableView1MouseEnter
      object cxGrid1DBTableView1: TcxGridDBTableView
        OnDblClick = cxGrid1DBTableView1DblClick
        OnKeyDown = cxGrid1DBTableView1KeyDown
        Navigator.Buttons.CustomButtons = <>
        OnFocusedRecordChanged = cxGrid1DBTableView1FocusedRecordChanged
        DataController.DataSource = dmBK.dsEtiketa
        DataController.Filter.Options = [fcoCaseInsensitive]
        DataController.Options = [dcoAnsiSort, dcoCaseInsensitive, dcoAssignGroupingValues, dcoAssignMasterDetailKeys, dcoSaveExpanding]
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        DataController.OnDetailCollapsed = cxGrid1DBTableView1DataControllerDetailCollapsed
        DataController.OnDetailExpanding = cxGrid1DBTableView1DataControllerDetailExpanding
        DataController.OnDetailExpanded = cxGrid1DBTableView1DataControllerDetailExpanded
        Filtering.ColumnFilteredItemsList = True
        FilterRow.InfoText = #1050#1083#1080#1082#1085#1080' '#1086#1074#1076#1077' '#1079#1072' '#1076#1072' '#1076#1077#1092#1080#1085#1080#1088#1072#1096' '#1092#1080#1083#1090#1077#1088
        FilterRow.Visible = True
        FilterRow.ApplyChanges = fracImmediately
        OptionsBehavior.IncSearch = True
        OptionsCustomize.ColumnsQuickCustomization = True
        OptionsData.Deleting = False
        OptionsData.Editing = False
        OptionsData.Inserting = False
        OptionsView.NoDataToDisplayInfoText = '<'#1053#1077#1084#1072' '#1077#1090#1080#1082#1077#1090#1080'>'
        object cxGrid1DBTableView1DATUM: TcxGridDBColumn
          Caption = #1044#1072#1090#1091#1084
          DataBinding.FieldName = 'DATUM'
          Width = 151
        end
        object cxGrid1DBTableView1TIP_ETIKETA: TcxGridDBColumn
          Caption = #1058#1080#1087' '#1085#1072' '#1077#1090#1080#1082#1077#1090#1072
          DataBinding.FieldName = 'TIP_ETIKETA'
          PropertiesClassName = 'TcxImageComboBoxProperties'
          Properties.Items = <
            item
              Description = #1061#1086#1084#1086#1075#1077#1085#1072
              ImageIndex = 0
              Value = 0
            end
            item
              Description = #1061#1077#1090#1077#1088#1086#1075#1077#1085#1072
              Value = 1
            end>
          Width = 159
        end
        object cxGrid1DBTableView1ID: TcxGridDBColumn
          Caption = #1064#1080#1092#1088#1072
          DataBinding.FieldName = 'ID'
          Visible = False
        end
        object cxGrid1DBTableView1ID_RABOTEN_NALOG: TcxGridDBColumn
          Caption = #1064#1080#1092#1088#1072' '#1085#1072' '#1056#1053
          DataBinding.FieldName = 'ID_RABOTEN_NALOG'
        end
        object cxGrid1DBTableView1BR_RN: TcxGridDBColumn
          Caption = #1041#1088#1086#1112' '#1085#1072' '#1056#1053
          DataBinding.FieldName = 'BR_RN'
          Width = 77
        end
        object cxGrid1DBTableView1DATUM_RN: TcxGridDBColumn
          Caption = #1044#1072#1090#1091#1084' '#1085#1072' '#1056#1053
          DataBinding.FieldName = 'DATUM_RN'
          Width = 85
        end
        object cxGrid1DBTableView1SSCC: TcxGridDBColumn
          DataBinding.FieldName = 'SSCC'
          Width = 102
        end
        object cxGrid1DBTableView1GODINA: TcxGridDBColumn
          Caption = #1043#1086#1076#1080#1085#1072
          DataBinding.FieldName = 'GODINA'
          Width = 76
        end
      end
      object cxGrid1DBTableView2: TcxGridDBTableView
        Navigator.Buttons.CustomButtons = <>
        DataController.DataSource = dmBK.dsEtiketaStavka
        DataController.DetailKeyFieldNames = 'ID_ETIKETA'
        DataController.KeyFieldNames = 'ID'
        DataController.MasterKeyFieldNames = 'ID'
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        OptionsBehavior.IncSearch = True
        OptionsData.Deleting = False
        OptionsData.Editing = False
        OptionsData.Inserting = False
        OptionsView.GroupByBox = False
        object cxGrid1DBTableView2NAZIV_ARTIKAL: TcxGridDBColumn
          Caption = #1055#1088#1086#1080#1079#1074#1086#1076
          DataBinding.FieldName = 'NAZIV_ARTIKAL'
          Width = 185
        end
        object cxGrid1DBTableView2GTIN: TcxGridDBColumn
          DataBinding.FieldName = 'GTIN'
          Width = 85
        end
        object cxGrid1DBTableView2SSCC: TcxGridDBColumn
          DataBinding.FieldName = 'SSCC'
          Visible = False
          Width = 72
        end
        object cxGrid1DBTableView2SERISKI_BROJ: TcxGridDBColumn
          Caption = #1057#1077#1088#1080#1089#1082#1080' '#1073#1088#1086#1112
          DataBinding.FieldName = 'SERISKI_BROJ'
          Width = 69
        end
        object cxGrid1DBTableView2BARKOD: TcxGridDBColumn
          Caption = #1041#1072#1088#1082#1086#1076
          DataBinding.FieldName = 'BARKOD'
          Width = 185
        end
        object cxGrid1DBTableView2LOT: TcxGridDBColumn
          Caption = #1048#1085#1090#1077#1088#1077#1085' '#1051#1054#1058
          DataBinding.FieldName = 'LOT'
          Width = 73
        end
        object cxGrid1DBTableView2KOLICINA: TcxGridDBColumn
          Caption = #1050#1086#1083#1080#1095#1080#1085#1072
          DataBinding.FieldName = 'KOLICINA'
        end
        object cxGrid1DBTableView2KOLICINA_VO_PAKUVANJE: TcxGridDBColumn
          Caption = #1050#1086#1083#1080#1095#1080#1085#1072' '#1074#1086' '#1087#1072#1082#1091#1074#1072#1114#1077
          DataBinding.FieldName = 'KOLICINA_VO_PAKUVANJE'
          Width = 54
        end
        object cxGrid1DBTableView2KOLICINA_BROJ: TcxGridDBColumn
          Caption = #1041#1088#1086#1112' '#1085#1072' '#1087#1072#1088#1095#1080#1114#1072' / '#1096#1080#1096#1080#1114#1072
          DataBinding.FieldName = 'KOLICINA_BROJ'
          Width = 28
        end
        object cxGrid1DBTableView2DATUM_PAK: TcxGridDBColumn
          Caption = #1044#1072#1090#1091#1084
          DataBinding.FieldName = 'DATUM_PAK'
        end
      end
      object cxGrid1Level1: TcxGridLevel
        GridView = cxGrid1DBTableView1
        object cxGrid1Level2: TcxGridLevel
          GridView = cxGrid1DBTableView2
        end
      end
    end
  end
  object cxPageControl1: TcxPageControl
    Left = 0
    Top = 0
    Width = 796
    Height = 445
    Align = alClient
    TabOrder = 1
    TabStop = False
    Properties.ActivePage = Napredno
    Properties.CustomButtons.Buttons = <>
    Properties.OwnerDraw = True
    Properties.TabHeight = 25
    Properties.TabWidth = 80
    LookAndFeel.NativeStyle = False
    OnChange = cxPageControl1Change
    ClientRectBottom = 445
    ClientRectRight = 796
    ClientRectTop = 29
    object Osnovno: TcxTabSheet
      Caption = #1045#1090#1080#1082#1077#1090#1072
      ImageIndex = 0
      TabVisible = False
      object dPanel: TPanel
        Left = 0
        Top = 0
        Width = 796
        Height = 271
        Align = alClient
        BevelOuter = bvSpace
        Color = 15790320
        Enabled = False
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clDefault
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentBackground = False
        ParentFont = False
        PopupMenu = PopupMenu1
        TabOrder = 0
        object Label1: TLabel
          Left = 600
          Top = 50
          Width = 50
          Height = 13
          Alignment = taRightJustify
          AutoSize = False
          Caption = #1064#1080#1092#1088#1072' :'
          Font.Charset = RUSSIAN_CHARSET
          Font.Color = clRed
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
          Visible = False
        end
        object Label2: TLabel
          Left = 37
          Top = 110
          Width = 60
          Height = 14
          Alignment = taRightJustify
          AutoSize = False
          Caption = 'SSCC'
          Font.Charset = RUSSIAN_CHARSET
          Font.Color = clNavy
          Font.Height = -12
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          WordWrap = True
        end
        object OtkaziButton: TcxButton
          Left = 471
          Top = 101
          Width = 101
          Height = 25
          Action = aOtkazi
          Colors.Pressed = clGradientActiveCaption
          TabOrder = 3
          Font.Charset = RUSSIAN_CHARSET
          Font.Color = clDefault
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object Sifra: TcxDBTextEdit
          Left = 656
          Top = 47
          BeepOnEnter = False
          DataBinding.DataField = 'SIFRA'
          ParentFont = False
          Properties.BeepOnError = True
          Properties.CharCase = ecUpperCase
          Style.Shadow = False
          TabOrder = 6
          Visible = False
          OnKeyDown = EnterKakoTab
          Width = 80
        end
        object ZapisiButton: TcxButton
          Left = 364
          Top = 101
          Width = 101
          Height = 25
          Action = aZapisi
          Colors.Pressed = clGradientActiveCaption
          TabOrder = 2
          Font.Charset = RUSSIAN_CHARSET
          Font.Color = clDefault
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object cxLabel8: TcxLabel
          Left = 31
          Top = 45
          Caption = #1041#1088#1086#1112' '#1085#1072' '#1056#1053
          ParentFont = False
          Style.Font.Charset = DEFAULT_CHARSET
          Style.Font.Color = clNavy
          Style.Font.Height = -13
          Style.Font.Name = 'Tahoma'
          Style.Font.Style = []
          Style.TextColor = clRed
          Style.IsFontAssigned = True
          Transparent = True
        end
        object cxLabel7: TcxLabel
          Left = 58
          Top = 76
          Caption = #1044#1072#1090#1091#1084
          ParentFont = False
          Style.Font.Charset = RUSSIAN_CHARSET
          Style.Font.Color = clDefault
          Style.Font.Height = -13
          Style.Font.Name = 'Tahoma'
          Style.Font.Style = []
          Style.TextColor = clRed
          Style.IsFontAssigned = True
          Transparent = True
        end
        object rgVidEtiketa: TcxDBRadioGroup
          Left = 461
          Top = 18
          Caption = ' '#1058#1080#1087' '#1085#1072' '#1077#1090#1080#1082#1077#1090#1072' '
          DataBinding.DataField = 'TIP_ETIKETA'
          DataBinding.DataSource = dmBK.dsEtiketa
          ParentFont = False
          Properties.Columns = 2
          Properties.DefaultValue = 0
          Properties.Items = <
            item
              Caption = #1061#1086#1084#1086#1075#1077#1085#1072
              Value = 0
            end
            item
              Caption = #1061#1077#1090#1077#1088#1086#1075#1077#1085#1072
              Value = 1
            end>
          Style.Font.Charset = RUSSIAN_CHARSET
          Style.Font.Color = clDefault
          Style.Font.Height = -13
          Style.Font.Name = 'Tahoma'
          Style.Font.Style = []
          Style.IsFontAssigned = True
          TabOrder = 8
          Visible = False
          Height = 60
          Width = 247
        end
        object cbRN: TcxDBLookupComboBox
          Tag = 1
          Left = 104
          Top = 45
          Hint = #1048#1079#1073#1077#1088#1077#1090#1077' '#1056#1072#1073#1086#1090#1077#1085' '#1053#1072#1083#1086#1075' '#1079#1072' '#1075#1077#1085#1077#1088#1080#1088#1072#1114#1077' '#1085#1072' '#1077#1090#1080#1082#1077#1090#1080
          DataBinding.DataField = 'ID_RABOTEN_NALOG'
          ParentFont = False
          Properties.DropDownAutoSize = True
          Properties.DropDownSizeable = True
          Properties.ImmediatePost = True
          Properties.KeyFieldNames = 'ID'
          Properties.ListColumns = <
            item
              Caption = #1064#1080#1092#1088#1072
              Width = 20
              FieldName = 'id'
            end
            item
              Width = 100
              FieldName = 'BROJ'
            end
            item
              Caption = #1044#1072#1090#1091#1084
              Width = 50
              FieldName = 'datum'
            end>
          Properties.ListFieldIndex = 1
          Properties.ListOptions.SyncMode = True
          Properties.ListSource = dm.dsIzberiRN
          Style.Font.Charset = RUSSIAN_CHARSET
          Style.Font.Color = clDefault
          Style.Font.Height = -13
          Style.Font.Name = 'Tahoma'
          Style.Font.Style = []
          Style.IsFontAssigned = True
          TabOrder = 0
          OnEnter = cxDBTextEditAllEnter
          OnExit = cxDBTextEditAllExit
          OnKeyDown = EnterKakoTab
          Width = 358
        end
        object txtDatum: TcxDBDateEdit
          Tag = 1
          Left = 107
          Top = 75
          Hint = #1044#1072#1090#1091#1084' '#1085#1072' '#1077#1090#1080#1082#1077#1090#1072
          DataBinding.DataField = 'DATUM'
          DataBinding.DataSource = dmBK.dsEtiketa
          ParentFont = False
          Style.Font.Charset = RUSSIAN_CHARSET
          Style.Font.Color = clDefault
          Style.Font.Height = -13
          Style.Font.Name = 'Tahoma'
          Style.Font.Style = []
          Style.IsFontAssigned = True
          TabOrder = 1
          OnEnter = cxDBTextEditAllEnter
          OnExit = cxDBTextEditAllExit
          OnKeyDown = EnterKakoTab
          Width = 185
        end
        object cxDBTextEdit5: TcxDBTextEdit
          Left = 656
          Top = 20
          DataBinding.DataField = 'ID_RABOTEN_NALOG'
          TabOrder = 7
          Visible = False
          Width = 121
        end
        object cxLabel3: TcxLabel
          Left = 1
          Top = 1
          Align = alTop
          AutoSize = False
          Caption = '     '#1043#1077#1085#1077#1088#1080#1088#1072#1114#1077' '#1085#1072' '#1077#1090#1080#1082#1077#1090#1072' '#1087#1086' '#1056#1072#1073#1086#1090#1077#1085' '#1085#1072#1083#1086#1075
          ParentColor = False
          ParentFont = False
          Style.Color = clActiveCaption
          Style.Font.Charset = RUSSIAN_CHARSET
          Style.Font.Color = clDefault
          Style.Font.Height = -12
          Style.Font.Name = 'Tahoma'
          Style.Font.Style = [fsBold]
          Style.TextColor = clWindow
          Style.IsFontAssigned = True
          Properties.Alignment.Horz = taLeftJustify
          Properties.Alignment.Vert = taVCenter
          Height = 24
          Width = 794
          AnchorY = 13
        end
        object Panel4: TPanel
          Left = 792
          Top = 39
          Width = 185
          Height = 122
          TabOrder = 10
        end
        object txtSSCC_e: TcxDBTextEdit
          Left = 104
          Top = 103
          DataBinding.DataField = 'SSCC'
          DataBinding.DataSource = dmBK.dsEtiketa
          Enabled = False
          StyleDisabled.Color = clWindow
          StyleDisabled.TextColor = clWindowText
          TabOrder = 11
          OnEnter = cxDBTextEditAllEnter
          OnExit = cxDBTextEditAllExit
          OnKeyDown = EnterKakoTab
          Width = 209
        end
      end
      object cxGrid2: TcxGrid
        Left = 0
        Top = 271
        Width = 796
        Height = 145
        Align = alBottom
        TabOrder = 1
        RootLevelOptions.DetailTabsPosition = dtpTop
        OnMouseLeave = cxGrid2DBTableView1MouseEnter
        ExplicitTop = 294
        ExplicitWidth = 803
        object cxGrid2DBTableView1: TcxGridDBTableView
          OnDblClick = cxGrid2DBTableView1DblClick
          Navigator.Buttons.CustomButtons = <>
          OnEditKeyDown = cxGrid2DBTableView1EditKeyDown
          DataController.DataSource = dmBK.dsEtiketaStavka
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          OptionsBehavior.IncSearch = True
          OptionsCustomize.ColumnsQuickCustomization = True
          OptionsData.Deleting = False
          OptionsData.Editing = False
          OptionsData.Inserting = False
          OptionsView.NoDataToDisplayInfoText = '<'#1053#1077#1084#1072' '#1087#1088#1086#1080#1079#1074#1086#1076' '#1079#1072' '#1075#1077#1085#1077#1088#1080#1088#1072#1114#1077' '#1085#1072' '#1073#1072#1088#1082#1086#1076#1086#1074#1080'>'
          OptionsView.GroupByBox = False
          OptionsView.Indicator = True
          object cxGrid2DBTableView1VID_ARTIKAL: TcxGridDBColumn
            Caption = #1042#1080#1076' '#1085#1072' '#1072#1088#1090#1080#1082#1072#1083
            DataBinding.FieldName = 'VID_ARTIKAL'
          end
          object cxGrid2DBTableView1ARTIKAL: TcxGridDBColumn
            Caption = #1064#1080#1092#1088#1072' '#1085#1072' '#1072#1088#1090#1080#1082#1072#1083
            DataBinding.FieldName = 'ARTIKAL'
            Width = 41
          end
          object cxGrid2DBTableView1NAZIV_ARTIKAL: TcxGridDBColumn
            Caption = #1040#1088#1090#1080#1082#1072#1083'/'#1055#1088#1086#1080#1079#1074#1086#1076
            DataBinding.FieldName = 'NAZIV_ARTIKAL'
            Width = 261
          end
          object cxGrid2DBTableView1GTIN: TcxGridDBColumn
            DataBinding.FieldName = 'GTIN'
            Width = 118
          end
          object cxGrid2DBTableView1SSCC: TcxGridDBColumn
            DataBinding.FieldName = 'SSCC'
            Width = 222
          end
          object cxGrid2DBTableView1SERISKI_BROJ: TcxGridDBColumn
            Caption = #1057#1077#1088#1080#1089#1082#1080' '#1073#1088#1086#1112
            DataBinding.FieldName = 'SERISKI_BROJ'
          end
          object cxGrid2DBTableView1BARKOD: TcxGridDBColumn
            Caption = #1041#1072#1088#1082#1086#1076
            DataBinding.FieldName = 'BARKOD'
            Width = 140
          end
          object cxGrid2DBTableView1LOT: TcxGridDBColumn
            Caption = #1048#1085#1090#1077#1088#1077#1085' '#1051#1054#1058
            DataBinding.FieldName = 'LOT'
            Width = 161
          end
          object cxGrid2DBTableView1KOLICINA: TcxGridDBColumn
            Caption = #1050#1086#1083#1080#1095#1080#1085#1072
            DataBinding.FieldName = 'KOLICINA'
          end
          object cxGrid2DBTableView1KOLICINA_VO_PAKUVANJE: TcxGridDBColumn
            Caption = #1050#1086#1083#1080#1095#1080#1080#1085#1072' '#1074#1086' '#1087#1072#1082#1091#1074#1072#1114#1077
            DataBinding.FieldName = 'KOLICINA_VO_PAKUVANJE'
          end
          object cxGrid2DBTableView1ID_PAKUVANJE: TcxGridDBColumn
            DataBinding.FieldName = 'ID_PAKUVANJE'
            Visible = False
          end
          object cxGrid2DBTableView1DATUM_PAK: TcxGridDBColumn
            Caption = #1044#1072#1090#1091#1084
            DataBinding.FieldName = 'DATUM_PAK'
          end
          object cxGrid2DBTableView1KOLICINA_BROJ: TcxGridDBColumn
            Caption = #1050#1086#1083#1080#1095#1080#1085#1072' '#1074#1086' '#1087#1072#1088#1095#1080#1114#1072'/'#1096#1080#1096#1080#1114#1072
            DataBinding.FieldName = 'KOLICINA_BROJ'
          end
        end
        object cxGrid2Level1: TcxGridLevel
          Caption = #1057#1090#1072#1074#1082#1080' / '#1073#1072#1088#1082#1086#1076#1086#1074#1080' '#1085#1072' '#1087#1088#1086#1080#1079#1074#1086#1076#1086#1090
          GridView = cxGrid2DBTableView1
          Options.DetailFrameColor = clBlack
          Options.DetailTabsPosition = dtpTop
        end
      end
    end
    object Napredno: TcxTabSheet
      ParentCustomHint = False
      Caption = ' '#1041#1072#1088#1082#1086#1076#1086#1074#1080'  '
      Color = clBtnFace
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ImageIndex = 1
      ParentColor = False
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      object dPanel2: TPanel
        Left = 0
        Top = 0
        Width = 796
        Height = 375
        Align = alClient
        Color = 15790320
        Enabled = False
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentBackground = False
        ParentFont = False
        TabOrder = 0
        OnClick = dPanel2Click
        object cxGroupBox1: TcxGroupBox
          Left = 32
          Top = 66
          Hint = #1055#1086#1075#1083#1077#1076' '#1085#1072' '#1080#1079#1075#1077#1085#1077#1088#1080#1088#1072#1085#1080#1090#1077' '#1073#1072#1088#1082#1086#1076#1086#1074#1080
          Caption = ' '#1055#1086#1075#1083#1077#1076' '#1085#1072' '#1080#1079#1075#1077#1085#1077#1088#1080#1088#1072#1085#1080#1090#1077' '#1073#1072#1088#1082#1086#1076#1086#1074#1080' '
          Enabled = False
          ParentFont = False
          Style.Font.Charset = RUSSIAN_CHARSET
          Style.Font.Color = clWindowText
          Style.Font.Height = -15
          Style.Font.Name = 'Tahoma'
          Style.Font.Style = []
          Style.IsFontAssigned = True
          TabOrder = 4
          Height = 263
          Width = 724
          object Label3: TLabel
            Left = 72
            Top = 149
            Width = 60
            Height = 20
            Alignment = taRightJustify
            AutoSize = False
            Caption = 'GTIN'
            Font.Charset = RUSSIAN_CHARSET
            Font.Color = clNavy
            Font.Height = -15
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            WordWrap = True
          end
          object Label4: TLabel
            Left = 412
            Top = 203
            Width = 60
            Height = 14
            Alignment = taRightJustify
            AutoSize = False
            Caption = 'SSCC'
            Font.Charset = RUSSIAN_CHARSET
            Font.Color = clNavy
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            Visible = False
            WordWrap = True
          end
          object Label5: TLabel
            Left = 72
            Top = 119
            Width = 60
            Height = 17
            Alignment = taRightJustify
            AutoSize = False
            Caption = #1041#1072#1088#1082#1086#1076
            Font.Charset = RUSSIAN_CHARSET
            Font.Color = clNavy
            Font.Height = -15
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            WordWrap = True
          end
          object txtGTIN: TcxDBTextEdit
            Left = 139
            Top = 146
            DataBinding.DataField = 'GTIN'
            DataBinding.DataSource = dmBK.dsEtiketaStavka
            Enabled = False
            StyleDisabled.Color = clWindow
            StyleDisabled.TextColor = clWindowText
            TabOrder = 2
            OnEnter = cxDBTextEditAllEnter
            OnExit = cxDBTextEditAllExit
            OnKeyDown = EnterKakoTab
            Width = 209
          end
          object txtSSCC: TcxDBTextEdit
            Left = 479
            Top = 200
            DataBinding.DataField = 'SSCC'
            Enabled = False
            StyleDisabled.Color = clWindow
            StyleDisabled.TextColor = clWindowText
            TabOrder = 1
            Visible = False
            OnEnter = cxDBTextEditAllEnter
            OnExit = cxDBTextEditAllExit
            OnKeyDown = EnterKakoTab
            Width = 209
          end
          object txtBarkod: TcxDBTextEdit
            Left = 139
            Top = 116
            DataBinding.DataField = 'BARKOD'
            DataBinding.DataSource = dmBK.dsEtiketaStavka
            Enabled = False
            StyleDisabled.Color = clWindow
            StyleDisabled.TextColor = clWindowText
            TabOrder = 0
            OnEnter = cxDBTextEditAllEnter
            OnExit = cxDBTextEditAllExit
            OnKeyDown = EnterKakoTab
            Width = 449
          end
          object cxLabel39: TcxLabel
            Left = 592
            Top = 36
            AutoSize = False
            Caption = #1055#1072#1082#1091#1074#1072#1114#1077
            Style.TextColor = clNavy
            Properties.Alignment.Horz = taRightJustify
            Transparent = True
            Visible = False
            Height = 17
            Width = 60
            AnchorX = 652
          end
          object lblMerkaP: TcxDBLabel
            Left = 558
            Top = 0
            AutoSize = True
            DataBinding.DataField = 'IZVEDENA_EM'
            Properties.Alignment.Horz = taLeftJustify
            Transparent = True
            Visible = False
          end
          object txtLot: TcxDBTextEdit
            Left = 139
            Top = 177
            Hint = #1051#1086#1090' '#1085#1072' '#1087#1088#1086#1080#1079#1074#1086#1076#1086#1090
            DataBinding.DataField = 'LOT'
            DataBinding.DataSource = dmBK.dsEtiketaStavka
            Enabled = False
            ParentFont = False
            Style.BorderStyle = ebsUltraFlat
            Style.Font.Charset = RUSSIAN_CHARSET
            Style.Font.Color = clWindowText
            Style.Font.Height = -15
            Style.Font.Name = 'Tahoma'
            Style.Font.Style = []
            Style.LookAndFeel.NativeStyle = False
            Style.LookAndFeel.SkinName = ''
            Style.TextStyle = []
            Style.TransparentBorder = False
            Style.IsFontAssigned = True
            StyleDisabled.Color = clWindow
            StyleDisabled.LookAndFeel.NativeStyle = False
            StyleDisabled.LookAndFeel.SkinName = ''
            StyleDisabled.TextColor = clWindowText
            StyleDisabled.TextStyle = []
            StyleFocused.LookAndFeel.NativeStyle = False
            StyleFocused.LookAndFeel.SkinName = ''
            StyleHot.LookAndFeel.NativeStyle = False
            StyleHot.LookAndFeel.SkinName = ''
            TabOrder = 5
            OnEnter = cxDBTextEditAllEnter
            OnExit = cxDBTextEditAllExit
            OnKeyDown = EnterKakoTab
            Width = 209
          end
          object cxLabel2: TcxLabel
            Left = 72
            Top = 177
            AutoSize = False
            Caption = #1051#1086#1090
            Style.TextColor = clNavy
            Properties.Alignment.Horz = taRightJustify
            Transparent = True
            Height = 32
            Width = 60
            AnchorX = 132
          end
          object cxLabel43: TcxLabel
            Left = 438
            Top = 147
            Caption = #1057#1077#1088#1080#1112#1072
            Style.TextColor = clBlue
            Properties.Alignment.Horz = taRightJustify
            Transparent = True
            Visible = False
            AnchorX = 487
          end
          object txtSerija: TcxDBTextEdit
            Left = 495
            Top = 147
            Hint = #1057#1077#1088#1080#1089#1082#1080' '#1085#1072' '#1087#1088#1086#1080#1079#1074#1086#1076#1086#1090
            DataBinding.DataField = 'SERISKI_BROJ1'
            DataBinding.DataSource = dmBK.dsEtiketaStavka
            Enabled = False
            ParentFont = False
            Style.BorderStyle = ebsUltraFlat
            Style.Font.Charset = RUSSIAN_CHARSET
            Style.Font.Color = clWindowText
            Style.Font.Height = -11
            Style.Font.Name = 'Tahoma'
            Style.Font.Style = []
            Style.LookAndFeel.NativeStyle = False
            Style.LookAndFeel.SkinName = ''
            Style.TextStyle = []
            Style.TransparentBorder = False
            Style.IsFontAssigned = True
            StyleDisabled.Color = clWindow
            StyleDisabled.LookAndFeel.NativeStyle = False
            StyleDisabled.LookAndFeel.SkinName = ''
            StyleDisabled.TextColor = clWindowText
            StyleDisabled.TextStyle = []
            StyleFocused.LookAndFeel.NativeStyle = False
            StyleFocused.LookAndFeel.SkinName = ''
            StyleHot.LookAndFeel.NativeStyle = False
            StyleHot.LookAndFeel.SkinName = ''
            TabOrder = 8
            Visible = False
            OnEnter = cxDBTextEditAllEnter
            OnExit = cxDBTextEditAllExit
            OnKeyDown = EnterKakoTab
            Width = 69
          end
          object cxLabel5: TcxLabel
            Left = 399
            Top = 175
            Hint = #1053#1077#1090#1086' '#1082#1086#1083#1080#1095#1080#1085#1072' '#1086#1076' '#1072#1088#1090#1080#1082#1083#1086#1090
            AutoSize = False
            Caption = #1050#1086#1083#1080#1095#1080#1085#1072' ('#1085#1077#1090#1086')'
            Style.TextColor = clRed
            Properties.Alignment.Horz = taRightJustify
            Properties.WordWrap = True
            Transparent = True
            Visible = False
            Height = 17
            Width = 90
            AnchorX = 489
          end
          object lblMerka: TcxDBLabel
            Left = 589
            Top = 172
            AutoSize = True
            DataBinding.DataField = 'MERKA'
            Properties.Alignment.Horz = taLeftJustify
            Transparent = True
            Visible = False
          end
          object cxLabel10: TcxLabel
            Left = 72
            Top = 33
            Hint = #1041#1088#1086#1112' '#1085#1072' '#1087#1072#1088#1095#1080#1114#1072' '#1086#1076' '#1087#1088#1086#1080#1079#1074#1086#1076#1086#1090
            Caption = #1041#1088#1086#1112' '#1085#1072' '#1087#1072#1088#1095#1080#1114#1072
            Style.TextColor = clRed
            Properties.Alignment.Horz = taRightJustify
            Properties.WordWrap = True
            Transparent = True
            Width = 64
            AnchorX = 136
          end
          object txtKolicinaBroj: TcxDBTextEdit
            Tag = 1
            Left = 142
            Top = 46
            Hint = #1041#1088#1086#1112' '#1085#1072' '#1077#1076#1080#1085#1077#1095#1085#1080' '#1087#1072#1082#1091#1074#1072#1114#1072
            AutoSize = False
            DataBinding.DataField = 'KOLICINA_BROJ'
            DataBinding.DataSource = dmBK.dsEtiketaStavka
            ParentFont = False
            Style.BorderStyle = ebsUltraFlat
            Style.Font.Charset = RUSSIAN_CHARSET
            Style.Font.Color = clWindowText
            Style.Font.Height = -15
            Style.Font.Name = 'Tahoma'
            Style.Font.Style = []
            Style.LookAndFeel.NativeStyle = False
            Style.LookAndFeel.SkinName = ''
            Style.TextStyle = []
            Style.TransparentBorder = False
            Style.IsFontAssigned = True
            StyleDisabled.LookAndFeel.NativeStyle = False
            StyleDisabled.LookAndFeel.SkinName = ''
            StyleFocused.LookAndFeel.NativeStyle = False
            StyleFocused.LookAndFeel.SkinName = ''
            StyleHot.LookAndFeel.NativeStyle = False
            StyleHot.LookAndFeel.SkinName = ''
            TabOrder = 12
            OnEnter = cxDBTextEditAllEnter
            OnExit = cxDBTextEditAllExit
            OnKeyDown = EnterKakoTab
            Height = 23
            Width = 70
          end
          object txtKolicinaEdMerka: TcxDBTextEdit
            Tag = 1
            Left = 496
            Top = 172
            Hint = #1050#1086#1083#1080#1095#1080#1085#1072' '#1082#1086#1112#1072' '#1090#1088#1077#1073#1072' '#1076#1072' '#1089#1077' '#1085#1072#1087#1090#1072#1074#1080' '#1087#1086' '#1056#1053
            DataBinding.DataField = 'KOLICINA'
            DataBinding.DataSource = dmBK.dsEtiketaStavka
            ParentFont = False
            Style.BorderStyle = ebsUltraFlat
            Style.Font.Charset = RUSSIAN_CHARSET
            Style.Font.Color = clWindowText
            Style.Font.Height = -11
            Style.Font.Name = 'Tahoma'
            Style.Font.Style = []
            Style.LookAndFeel.NativeStyle = False
            Style.LookAndFeel.SkinName = ''
            Style.TextStyle = []
            Style.TransparentBorder = False
            Style.IsFontAssigned = True
            StyleDisabled.LookAndFeel.NativeStyle = False
            StyleDisabled.LookAndFeel.SkinName = ''
            StyleFocused.LookAndFeel.NativeStyle = False
            StyleFocused.LookAndFeel.SkinName = ''
            StyleHot.LookAndFeel.NativeStyle = False
            StyleHot.LookAndFeel.SkinName = ''
            TabOrder = 15
            Visible = False
            OnEnter = cxDBTextEditAllEnter
            OnExit = cxDBTextEditAllExit
            OnKeyDown = EnterKakoTab
            Width = 92
          end
          object btIzberiProiz: TcxButton
            Left = 232
            Top = 45
            Width = 160
            Height = 25
            Action = aIzberi
            BiDiMode = bdLeftToRight
            ParentBiDiMode = False
            TabOrder = 14
            Font.Charset = RUSSIAN_CHARSET
            Font.Color = clWindowText
            Font.Height = -15
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
          end
          object lblDelA: TcxLabel
            Left = 56
            Top = 79
            AutoSize = False
            Caption = #1044#1077#1083' '#1086#1076' '#1072#1088#1090#1080#1082#1083#1086#1090
            Enabled = False
            Style.TextColor = clNavy
            Properties.Alignment.Horz = taRightJustify
            Properties.WordWrap = True
            Transparent = True
            Height = 40
            Width = 78
            AnchorX = 134
          end
          object txtSifraSD: TcxDBTextEdit
            Left = 141
            Top = 86
            Hint = #1055#1072#1082#1091#1074#1072#1114#1077' '#1085#1072' '#1075#1086#1090#1086#1074#1080#1086#1090' '#1087#1088#1086#1080#1079#1074#1086#1076
            DataBinding.DataField = 'ID_SOS_DEL'
            DataBinding.DataSource = dmBK.dsEtiketaStavka
            Enabled = False
            ParentFont = False
            Style.BorderStyle = ebsUltraFlat
            Style.Font.Charset = RUSSIAN_CHARSET
            Style.Font.Color = clWindowText
            Style.Font.Height = -15
            Style.Font.Name = 'Tahoma'
            Style.Font.Style = []
            Style.LookAndFeel.NativeStyle = False
            Style.LookAndFeel.SkinName = ''
            Style.TextStyle = []
            Style.TransparentBorder = False
            Style.IsFontAssigned = True
            StyleDisabled.Color = clWindow
            StyleDisabled.LookAndFeel.NativeStyle = False
            StyleDisabled.LookAndFeel.SkinName = ''
            StyleDisabled.TextColor = clWindowText
            StyleDisabled.TextStyle = []
            StyleFocused.LookAndFeel.NativeStyle = False
            StyleFocused.LookAndFeel.SkinName = ''
            StyleHot.LookAndFeel.NativeStyle = False
            StyleHot.LookAndFeel.SkinName = ''
            TabOrder = 16
            OnEnter = cxDBTextEditAllEnter
            OnExit = cxDBTextEditAllExit
            OnKeyDown = EnterKakoTab
            Width = 71
          end
          object cbSosDel: TcxDBLookupComboBox
            Left = 217
            Top = 86
            DataBinding.DataField = 'ID_SOS_DEL'
            DataBinding.DataSource = dmBK.dsEtiketaStavka
            Enabled = False
            Properties.DropDownSizeable = True
            Properties.ImmediatePost = True
            Properties.KeyFieldNames = 'SOS_DEL'
            Properties.ListColumns = <
              item
                FieldName = 'SOS_DEL'
              end
              item
                FieldName = 'NAZIV'
              end>
            Properties.ListFieldIndex = 1
            Properties.ListOptions.SyncMode = True
            Properties.ListSource = dm.dsSosDel
            StyleDisabled.Color = clWindow
            StyleDisabled.TextColor = clWindowText
            TabOrder = 17
            OnEnter = cxDBTextEditAllEnter
            OnExit = cxDBTextEditAllExit
            OnKeyDown = EnterKakoTab
            Width = 189
          end
          object lblDelC: TcxLabel
            Left = 412
            Top = 79
            AutoSize = False
            Caption = #1044#1077#1083' '#1086#1076' '#1094#1077#1083#1086#1090#1086
            Enabled = False
            Style.TextColor = clNavy
            Properties.Alignment.Horz = taRightJustify
            Properties.WordWrap = True
            Transparent = True
            Height = 40
            Width = 67
            AnchorX = 479
          end
          object txtImenitel: TcxDBTextEdit
            Tag = 1
            Left = 490
            Top = 86
            Hint = #1050#1086#1083#1080#1095#1080#1085#1072' '#1082#1086#1112#1072' '#1090#1088#1077#1073#1072' '#1076#1072' '#1089#1077' '#1085#1072#1087#1088#1072#1074#1080' '#1087#1086' '#1056#1053
            DataBinding.DataField = 'IMENITEL'
            DataBinding.DataSource = dmBK.dsEtiketaStavka
            Enabled = False
            ParentFont = False
            Style.BorderStyle = ebsUltraFlat
            Style.Font.Charset = RUSSIAN_CHARSET
            Style.Font.Color = clWindowText
            Style.Font.Height = -15
            Style.Font.Name = 'Tahoma'
            Style.Font.Style = []
            Style.LookAndFeel.NativeStyle = False
            Style.LookAndFeel.SkinName = ''
            Style.TextStyle = []
            Style.TransparentBorder = False
            Style.IsFontAssigned = True
            StyleDisabled.Color = clWindow
            StyleDisabled.LookAndFeel.NativeStyle = False
            StyleDisabled.LookAndFeel.SkinName = ''
            StyleDisabled.TextColor = clWindowText
            StyleDisabled.TextStyle = []
            StyleFocused.LookAndFeel.NativeStyle = False
            StyleFocused.LookAndFeel.SkinName = ''
            StyleHot.LookAndFeel.NativeStyle = False
            StyleHot.LookAndFeel.SkinName = ''
            TabOrder = 20
            OnEnter = cxDBTextEditAllEnter
            OnExit = cxDBTextEditAllExit
            OnKeyDown = EnterKakoTab
            Width = 98
          end
          object txtPakuvanje: TcxDBTextEdit
            Left = 659
            Top = 36
            Hint = #1055#1072#1082#1091#1074#1072#1114#1077' '#1085#1072' '#1075#1086#1090#1086#1074#1080#1086#1090' '#1087#1088#1086#1080#1079#1074#1086#1076
            DataBinding.DataField = 'ID_PAKUVANJE'
            DataBinding.DataSource = dmBK.dsEtiketaStavka
            Enabled = False
            ParentFont = False
            Style.BorderStyle = ebsUltraFlat
            Style.Font.Charset = RUSSIAN_CHARSET
            Style.Font.Color = clWindowText
            Style.Font.Height = -11
            Style.Font.Name = 'Tahoma'
            Style.Font.Style = []
            Style.LookAndFeel.NativeStyle = False
            Style.LookAndFeel.SkinName = ''
            Style.TextStyle = []
            Style.TransparentBorder = False
            Style.IsFontAssigned = True
            StyleDisabled.Color = clWindow
            StyleDisabled.LookAndFeel.NativeStyle = False
            StyleDisabled.LookAndFeel.SkinName = ''
            StyleDisabled.TextColor = clWindowText
            StyleDisabled.TextStyle = []
            StyleFocused.LookAndFeel.NativeStyle = False
            StyleFocused.LookAndFeel.SkinName = ''
            StyleHot.LookAndFeel.NativeStyle = False
            StyleHot.LookAndFeel.SkinName = ''
            TabOrder = 21
            Visible = False
            OnEnter = cxDBTextEditAllEnter
            OnExit = cxDBTextEditAllExit
            OnKeyDown = EnterKakoTab
            Width = 56
          end
          object btIzberiSD: TcxButton
            Left = 232
            Top = 45
            Width = 160
            Height = 25
            Hint = #1048#1079#1073#1077#1088#1080' '#1089#1086#1089#1090#1072#1074#1085#1080' '#1076#1077#1083#1086#1074#1080
            Action = aIzberiSD
            BiDiMode = bdLeftToRight
            Caption = #1048#1079#1073#1077#1088#1080' '#1057#1086#1089'. '#1044#1077#1083#1086#1074#1080
            OptionsImage.ImageIndex = 85
            OptionsImage.Images = dmRes.cxSmallImages
            ParentBiDiMode = False
            TabOrder = 13
            Visible = False
            Font.Charset = RUSSIAN_CHARSET
            Font.Color = clWindowText
            Font.Height = -15
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
          end
        end
        object cxLabel1: TcxLabel
          Left = 2
          Top = 10
          Caption = #1040#1088#1090#1080#1082#1072#1083' / '#1055#1088#1086#1080#1079#1074#1086#1076
          ParentFont = False
          Style.Font.Charset = RUSSIAN_CHARSET
          Style.Font.Color = clWindowText
          Style.Font.Height = -15
          Style.Font.Name = 'Tahoma'
          Style.Font.Style = []
          Style.TextColor = clRed
          Style.IsFontAssigned = True
          Properties.Alignment.Horz = taRightJustify
          Properties.WordWrap = True
          Transparent = True
          Width = 76
          AnchorX = 78
        end
        object txtVidArtikal: TcxDBTextEdit
          Tag = 1
          Left = 104
          Top = 15
          Hint = #1042#1080#1076' '#1085#1072' '#1072#1088#1090#1080#1082#1072#1083
          DataBinding.DataField = 'VID_ARTIKAL'
          DataBinding.DataSource = dmBK.dsEtiketaStavka
          Enabled = False
          ParentFont = False
          Style.Font.Charset = DEFAULT_CHARSET
          Style.Font.Color = clWindowText
          Style.Font.Height = -15
          Style.Font.Name = 'Tahoma'
          Style.Font.Style = []
          Style.IsFontAssigned = True
          StyleDisabled.BorderColor = clBtnText
          StyleDisabled.Color = clWindow
          StyleDisabled.TextColor = clWindowText
          TabOrder = 0
          OnEnter = cxDBTextEditAllEnter
          OnExit = cxDBTextEditAllExit
          OnKeyDown = EnterKakoTab
          Width = 73
        end
        object txtArtikal: TcxDBTextEdit
          Tag = 1
          Left = 178
          Top = 15
          Hint = #1064#1080#1092#1088#1072' '#1085#1072' '#1072#1088#1090#1080#1082#1072#1083
          DataBinding.DataField = 'ARTIKAL'
          DataBinding.DataSource = dmBK.dsEtiketaStavka
          Enabled = False
          ParentFont = False
          Style.Font.Charset = DEFAULT_CHARSET
          Style.Font.Color = clWindowText
          Style.Font.Height = -15
          Style.Font.Name = 'Tahoma'
          Style.Font.Style = []
          Style.IsFontAssigned = True
          StyleDisabled.BorderColor = clBtnText
          StyleDisabled.Color = clWindow
          StyleDisabled.TextColor = clWindowText
          TabOrder = 1
          OnEnter = cxDBTextEditAllEnter
          OnExit = cxDBTextEditAllExit
          OnKeyDown = EnterKakoTab
          Width = 65
        end
        object cbNazivArtikal: TcxLookupComboBox
          Left = 244
          Top = 15
          Enabled = False
          ParentFont = False
          Properties.ClearKey = 46
          Properties.DropDownAutoSize = True
          Properties.DropDownSizeable = True
          Properties.KeyFieldNames = 'ARTVID;id'
          Properties.ListColumns = <
            item
              Caption = #1042#1080#1076' '#1072#1088#1090#1080#1082#1072#1083
              Width = 20
              FieldName = 'artvid'
            end
            item
              Caption = #1064#1080#1092#1088#1072' '#1085#1072' '#1072#1088#1090#1080#1082#1072#1083
              Width = 20
              FieldName = 'id'
            end
            item
              Caption = #1053#1072#1079#1080#1074
              Width = 150
              FieldName = 'naziv'
            end>
          Properties.ListFieldIndex = 2
          Properties.ListOptions.SyncMode = True
          Properties.ListSource = dmBK.dsIzberiArtikal
          Style.Font.Charset = RUSSIAN_CHARSET
          Style.Font.Color = clWindowText
          Style.Font.Height = -15
          Style.Font.Name = 'Tahoma'
          Style.Font.Style = []
          Style.IsFontAssigned = True
          StyleDisabled.BorderColor = clBtnText
          StyleDisabled.Color = clWindow
          StyleDisabled.TextColor = clWindowText
          TabOrder = 2
          OnEnter = cxDBTextEditAllEnter
          OnExit = cxDBTextEditAllExit
          OnKeyDown = EnterKakoTab
          Width = 427
        end
        object btBarkod: TcxButton
          Left = 685
          Top = 15
          Width = 104
          Height = 25
          Action = aBarkod
          TabOrder = 3
          Visible = False
          Font.Charset = RUSSIAN_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
      end
      object Panel1: TPanel
        Left = 0
        Top = 375
        Width = 796
        Height = 41
        Align = alBottom
        TabOrder = 1
        object Zapisi2: TcxButton
          Left = 560
          Top = 8
          Width = 95
          Height = 25
          Action = aZapisi
          Colors.Pressed = clGradientActiveCaption
          TabOrder = 0
          Font.Charset = RUSSIAN_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object Otkazi2: TcxButton
          Left = 671
          Top = 8
          Width = 85
          Height = 25
          Action = aOtkazi
          Colors.Pressed = clGradientActiveCaption
          TabOrder = 1
          Font.Charset = RUSSIAN_CHARSET
          Font.Color = clWindowText
          Font.Height = -15
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
      end
    end
  end
  object cxGridPopupMenu1: TcxGridPopupMenu
    PopupMenus = <>
    Left = 48
    Top = 16
  end
  object PopupMenu1: TPopupMenu
    Left = 48
    Top = 48
    object N1: TMenuItem
      Caption = #1057#1085#1080#1084#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      ShortCut = 49235
      OnClick = SaveToIniFileExecute
    end
    object Excel1: TMenuItem
      Caption = #1057#1085#1080#1084#1080' '#1075#1086' '#1074#1086' Excel'
      ShortCut = 49221
    end
  end
  object dxBarManager1: TdxBarManager
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Segoe UI'
    Font.Style = []
    Categories.Strings = (
      'Default')
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    ImageOptions.LargeImages = dmRes.cxLargeImages
    PopupMenuLinks = <>
    UseSystemFont = False
    Left = 112
    Top = 16
    PixelsPerInch = 96
    object dxBarManager1Bar1: TdxBar
      CaptionButtons = <>
      DockedDockingStyle = dsNone
      DockedLeft = 0
      DockedTop = 0
      DockingStyle = dsNone
      FloatLeft = 715
      FloatTop = 157
      FloatClientWidth = 100
      FloatClientHeight = 54
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton10'
        end>
      OneOnRow = False
      Row = 2
      UseOwnFont = False
      Visible = False
      WholeRow = False
    end
    object dxBarManager1Bar2: TdxBar
      Caption = #1040#1082#1094#1080#1080
      CaptionButtons = <>
      DockedDockingStyle = dsNone
      DockedLeft = 121
      DockedTop = 0
      DockingStyle = dsNone
      FloatLeft = 789
      FloatTop = 120
      FloatClientWidth = 77
      FloatClientHeight = 21
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarSubItem1'
        end>
      OneOnRow = False
      Row = 2
      UseOwnFont = False
      Visible = False
      WholeRow = False
    end
    object dxBarManager1Bar3: TdxBar
      Caption = #1048#1079#1083#1077#1079
      CaptionButtons = <>
      DockedDockingStyle = dsNone
      DockedLeft = 146
      DockedTop = 0
      DockingStyle = dsNone
      FloatLeft = 718
      FloatTop = 62
      FloatClientWidth = 59
      FloatClientHeight = 108
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton7'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton8'
        end>
      OneOnRow = False
      Row = 1
      UseOwnFont = False
      Visible = False
      WholeRow = False
    end
    object dxBarManager1Bar4: TdxBar
      Caption = #1055#1077#1095#1072#1090#1077#1114#1077
      CaptionButtons = <>
      DockedDockingStyle = dsNone
      DockedLeft = 0
      DockedTop = 0
      DockingStyle = dsNone
      FloatLeft = 676
      FloatTop = 240
      FloatClientWidth = 141
      FloatClientHeight = 221
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton12'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton15'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton9'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarLargeButton13'
        end>
      OneOnRow = False
      Row = 1
      UseOwnFont = False
      Visible = False
      WholeRow = False
    end
    object dxBarManager1BarSnimiIzgled: TdxBar
      Caption = #1058#1072#1073#1077#1083#1072
      CaptionButtons = <>
      DockedDockingStyle = dsNone
      DockedLeft = 363
      DockedTop = 0
      DockingStyle = dsNone
      FloatLeft = 868
      FloatTop = 8
      FloatClientWidth = 174
      FloatClientHeight = 113
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLBtnSnimiIzgled'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarLBtnBrisiIzgled'
        end>
      OneOnRow = True
      Row = 0
      UseOwnFont = False
      Visible = False
      WholeRow = False
    end
    object dxBarLargeButton2: TdxBarLargeButton
      Action = aAzuriraj
      Category = 0
    end
    object dxBarLargeButton3: TdxBarLargeButton
      Action = aBrisi
      Category = 0
    end
    object dxBarLargeButton4: TdxBarLargeButton
      Action = aSnimiIzgled
      Category = 0
      ScreenTip = tipSnimiIzgled
    end
    object dxBarLargeButton5: TdxBarLargeButton
      Action = aZacuvajExcel
      Category = 0
      ScreenTip = tipZacuvajExcel
    end
    object dxBarLargeButton6: TdxBarLargeButton
      Action = aPecatiTabela
      Caption = #1055#1077#1095#1072#1090#1080' '#1090#1072#1073#1077#1083#1072
      Category = 0
    end
    object dxBarLargeButton7: TdxBarLargeButton
      Action = aHelp
      Category = 0
    end
    object dxBarLargeButton8: TdxBarLargeButton
      Action = aIzlez
      Category = 0
    end
    object dxBarLargeButton1: TdxBarLargeButton
      Action = aNov
      Category = 0
    end
    object dxBarLargeButton9: TdxBarLargeButton
      Action = aSnimiPecatenje
      Category = 0
      ScreenTip = tipSnimiKonf
    end
    object dxBarLargeButton11: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object ribbonFilterReset: TdxBarLargeButton
      Caption = #1056#1077#1089#1077#1090#1080#1088#1072#1112' '#1092#1080#1083#1090#1077#1088
      Category = 0
      Hint = #1056#1077#1089#1077#1090#1080#1088#1072#1112' '#1092#1080#1083#1090#1077#1088
      Visible = ivAlways
    end
    object dxBarLargeButton12: TdxBarLargeButton
      Action = aPodesuvanjePecatenje
      Category = 0
      ScreenTip = tipDizajnReport
    end
    object dxBarLargeButton13: TdxBarLargeButton
      Action = aBrisiPodesuvanjePecatenje
      Category = 0
      ScreenTip = tipBrisiKonf
    end
    object dxBarLargeButton14: TdxBarLargeButton
      Caption = #1048#1079#1073#1088#1080#1096#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      Category = 0
      Visible = ivAlways
      LargeImageIndex = 40
    end
    object dxBarLargeButton15: TdxBarLargeButton
      Action = aPageSetup
      Category = 0
      ScreenTip = tipSetiranjeStrana
    end
    object dxBarLargeButton16: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object dxBarLargeButton17: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object cxBarEditItem1: TcxBarEditItem
      Caption = 'New Item'
      Category = 0
      Hint = 'New Item'
      Visible = ivAlways
      PropertiesClassName = 'TcxCheckBoxProperties'
    end
    object dxBarSubItem1: TdxBarSubItem
      Caption = #1055#1077#1095#1072#1090#1077#1114#1077
      Category = 0
      Visible = ivAlways
      ImageIndex = 30
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton6'
        end>
    end
    object dxBarLBtnSnimiIzgled: TdxBarLargeButton
      Action = aSnimiIzgled
      Category = 0
    end
    object dxBarLBtnBrisiIzgled: TdxBarLargeButton
      Action = aBrisiIzgled
      Category = 0
    end
    object dxBarLargeButton10: TdxBarLargeButton
      Action = aPogled
      Category = 0
    end
  end
  object ActionList1: TActionList
    Images = dmRes.cxSmallImages
    Left = 16
    Top = 16
    object aNov: TAction
      Caption = #1053#1086#1074
      ImageIndex = 10
      ShortCut = 116
      OnExecute = aNovExecute
    end
    object aAzuriraj: TAction
      Caption = #1040#1078#1091#1088#1080#1088#1072#1112
      ImageIndex = 12
      ShortCut = 117
      OnExecute = aAzurirajExecute
    end
    object aBrisi: TAction
      Caption = #1041#1088#1080#1096#1080
      ImageIndex = 11
      ShortCut = 119
      OnExecute = aBrisiExecute
    end
    object aSnimiIzgled: TAction
      Caption = #1057#1085#1080#1084#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      ImageIndex = 9
      ShortCut = 24659
      OnExecute = aSnimiIzgledExecute
    end
    object aZacuvajExcel: TAction
      Caption = #1047#1072#1095#1091#1074#1072#1112' '#1074#1086' Excel'
      ImageIndex = 9
      ShortCut = 24645
      OnExecute = aZacuvajExcelExecute
    end
    object aPecatiTabela: TAction
      Caption = #1055#1077#1095#1072#1090#1080
      ImageIndex = 30
      OnExecute = aPecatiTabelaExecute
    end
    object aHelp: TAction
      Caption = #1055#1086#1084#1086#1096
      ImageIndex = 24
    end
    object aIzlez: TAction
      Caption = #1048#1079#1083#1077#1079
      ImageIndex = 25
      OnExecute = aIzlezExecute
    end
    object aZapisi: TAction
      Caption = #1047#1072#1087#1080#1096#1080
      ImageIndex = 7
      ShortCut = 120
      OnExecute = aZapisiExecute
    end
    object aOtkazi: TAction
      Caption = #1054#1090#1082#1072#1078#1080
      ImageIndex = 21
      ShortCut = 27
      OnExecute = aOtkaziExecute
    end
    object aRefresh: TAction
      Caption = 'aRefresh'
      ImageIndex = 18
      ShortCut = 118
      OnExecute = aRefreshExecute
    end
    object aSnimiPecatenje: TAction
      Caption = #1057#1085#1080#1084#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      ImageIndex = 39
      OnExecute = aSnimiPecatenjeExecute
    end
    object aPodesuvanjePecatenje: TAction
      Caption = #1044#1080#1079#1072#1112#1085' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090
      ImageIndex = 38
      OnExecute = aPodesuvanjePecatenjeExecute
    end
    object aBrisiPodesuvanjePecatenje: TAction
      Caption = #1041#1088#1080#1096#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      ImageIndex = 37
      OnExecute = aBrisiPodesuvanjePecatenjeExecute
    end
    object aPageSetup: TAction
      Caption = #1057#1077#1090#1080#1088#1072#1114#1077' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072
      ImageIndex = 38
      OnExecute = aPageSetupExecute
    end
    object aFormConfig: TAction
      Caption = 'aFormConfig'
      ShortCut = 24699
      OnExecute = aFormConfigExecute
    end
    object aBrisiIzgled: TAction
      Caption = #1041#1088#1080#1096#1080' '#1075#1086' '#1079#1072#1095#1091#1074#1072#1085#1080#1086#1090' '#1080#1079#1075#1083#1077#1076
      ImageIndex = 56
      OnExecute = aBrisiIzgledExecute
    end
    object aPecatiEtiketa: TAction
      Caption = #1055#1077#1095#1072#1090#1080' '#1077#1090#1080#1082#1077#1090#1072
      ImageIndex = 35
      OnExecute = aPecatiEtiketaExecute
    end
    object aDizajner: TAction
      Caption = 'aDizajner'
      ShortCut = 24697
      OnExecute = aDizajnerExecute
    end
    object aBarkod: TAction
      Caption = #1041#1072#1088#1082#1086#1076' '
      ImageIndex = 47
      OnExecute = aBarkodExecute
    end
    object aAvtomatski: TAction
      Caption = 'aAvtomatski'
      ShortCut = 16464
      OnExecute = aAvtomatskiExecute
    end
    object aPogled: TAction
      Caption = #1055#1077#1095#1072#1090#1080' '#1045#1090#1080#1082#1077#1090#1072
      ImageIndex = 94
      OnExecute = aPogledExecute
    end
    object aPresmetaj: TAction
      Caption = 'aPresmetaj'
      OnExecute = aPresmetajExecute
    end
    object aIzberi: TAction
      Caption = #1048#1079#1073#1077#1088#1080' '#1087#1088#1086#1080#1079#1074#1086#1076
      ImageIndex = 50
      OnExecute = aIzberiExecute
    end
    object aAvtomatskiK: TAction
      Caption = 'aAvtomatskiK'
      OnExecute = aAvtomatskiKExecute
    end
    object aNovK: TAction
      Caption = 'aNovK'
      OnExecute = aNovKExecute
    end
    object aZapisiK: TAction
      Caption = 'aZapisiK'
      OnExecute = aZapisiKExecute
    end
    object aPresmetajK: TAction
      Caption = 'aPresmetajK'
      OnExecute = aPresmetajKExecute
    end
    object aBarkodK: TAction
      Caption = 'aBarkodK'
      OnExecute = aBarkodKExecute
    end
    object aPresmetajSD: TAction
      Caption = 'aPresmetajSD'
      OnExecute = aPresmetajSDExecute
    end
    object aIzberiSD: TAction
      Caption = 'aIzberiSD'
      OnExecute = aIzberiSDExecute
    end
  end
  object dxBarScreenTipRepository1: TdxBarScreenTipRepository
    Left = 144
    Top = 16
    PixelsPerInch = 96
    object tipSnimiIzgled: TdxBarScreenTip
      Header.Text = #1057#1085#1080#1084#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      Description.Text = #1047#1072#1095#1091#1074#1072#1112' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090' '#1085#1072' '#1092#1086#1088#1084#1072#1090#1072' '#1080' '#1085#1072' '#1090#1072#1073#1077#1083#1072#1090#1072
    end
    object tipZacuvajExcel: TdxBarScreenTip
      Header.Text = #1047#1072#1095#1091#1074#1072#1112' '#1074#1086' Excel'
      Description.Text = #1057#1085#1080#1084#1080' '#1075#1080' '#1087#1086#1076#1072#1090#1086#1094#1080#1090#1077' '#1086#1076' '#1090#1072#1073#1077#1083#1072#1090#1072' '#1074#1086' Excel '#1092#1086#1088#1084#1072#1090
    end
    object tipDizajnReport: TdxBarScreenTip
      Header.Text = #1044#1080#1079#1072#1112#1085' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090
      Description.Text = #1055#1086#1076#1077#1089#1091#1074#1072#1114#1072' '#1085#1072' '#1080#1079#1075#1083#1077#1076#1086#1090' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090' '#1085#1072' '#1090#1072#1073#1077#1083#1072#1090#1072
    end
    object tipSetiranjeStrana: TdxBarScreenTip
      Header.Text = #1057#1077#1090#1080#1088#1072#1114#1077' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072
      Description.Text = #1055#1086#1076#1077#1089#1091#1074#1072#1114#1077' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072' '#1085#1072' '#1082#1086#1112#1072' '#1116#1077' '#1089#1077' '#1087#1077#1095#1072#1090#1080' '#1090#1072#1073#1077#1083#1072#1090#1072
    end
    object tipSnimiKonf: TdxBarScreenTip
      Header.Text = #1057#1085#1080#1084#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      Description.Text = #1047#1072#1095#1091#1074#1072#1112' '#1075#1080' '#1087#1086#1076#1077#1089#1091#1074#1072#1114#1072#1090#1072' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090' '#1080' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072' '#1079#1072' '#1087#1077#1095#1072#1090#1077#1114#1077
    end
    object tipBrisiKonf: TdxBarScreenTip
      Header.Text = #1041#1088#1080#1096#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      Description.Text = #1048#1079#1073#1088#1080#1096#1080' '#1075#1080' '#1087#1086#1076#1077#1089#1091#1074#1072#1114#1072#1090#1072' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090' '#1080' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072' '#1079#1072' '#1087#1077#1095#1072#1090#1077#1114#1077
    end
  end
  object dxComponentPrinter1: TdxComponentPrinter
    CurrentLink = dxComponentPrinter1Link1
    Version = 0
    Left = 16
    Top = 48
    object dxComponentPrinter1Link1: TdxGridReportLink
      PrinterPage.DMPaper = 9
      PrinterPage.Footer = 6350
      PrinterPage.GrayShading = True
      PrinterPage.Header = 6350
      PrinterPage.Margins.Bottom = 12700
      PrinterPage.Margins.Left = 12700
      PrinterPage.Margins.Right = 12700
      PrinterPage.Margins.Top = 12700
      PrinterPage.Orientation = poLandscape
      PrinterPage.PageFooter.RightTitle.Strings = (
        #1057#1090#1088'.[Page #] '#1086#1076' [Total Pages]')
      PrinterPage.PageHeader.RightTitle.Strings = (
        #1044#1072#1090#1091#1084' : [Date & Time Printed]')
      PrinterPage.PageSize.X = 210000
      PrinterPage.PageSize.Y = 297000
      PrinterPage._dxMeasurementUnits_ = 0
      PrinterPage._dxLastMU_ = 2
      ReportDocument.CreationDate = 40336.425359629630000000
      OptionsFormatting.UseNativeStyles = True
      OptionsOnEveryPage.Caption = False
      OptionsOnEveryPage.FilterBar = False
      OptionsView.Caption = False
      OptionsView.FilterBar = False
      BuiltInReportLink = True
    end
  end
  object cxGridPopupMenu2: TcxGridPopupMenu
    PopupMenus = <>
    Left = 80
    Top = 16
  end
  object Timer1: TTimer
    Enabled = False
    Interval = 10000
    OnTimer = Timer1Timer
    Left = 80
    Top = 48
  end
end
