object frmEvidencija: TfrmEvidencija
  Left = 128
  Top = 212
  ActiveControl = cxGrid1
  BorderIcons = [biSystemMenu, biMaximize]
  Caption = #1045#1074#1080#1076#1077#1085#1094#1080#1112#1072' '#1085#1072' '#1087#1086#1090#1088#1086#1096#1077#1085#1080' '#1084#1072#1090#1077#1088#1080#1112#1072#1083#1080
  ClientHeight = 642
  ClientWidth = 974
  Color = clBtnFace
  Constraints.MinHeight = 500
  Constraints.MinWidth = 610
  Font.Charset = RUSSIAN_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  ShowHint = True
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object lPanel: TPanel
    Left = 0
    Top = 126
    Width = 974
    Height = 416
    Align = alClient
    BevelInner = bvLowered
    BevelOuter = bvSpace
    TabOrder = 0
    object cxGrid1: TcxGrid
      Left = 2
      Top = 2
      Width = 970
      Height = 412
      Align = alClient
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      object cxGrid1DBTableView1: TcxGridDBTableView
        OnDblClick = cxGrid1DBTableView1DblClick
        OnKeyDown = cxGrid1DBTableView1KeyDown
        OnKeyPress = cxGrid1DBTableView1KeyPress
        Navigator.Buttons.CustomButtons = <>
        OnFocusedRecordChanged = cxGrid1DBTableView1FocusedRecordChanged
        DataController.DataSource = dsEvidencija
        DataController.Filter.Options = [fcoCaseInsensitive]
        DataController.Options = [dcoAnsiSort, dcoAssignGroupingValues, dcoAssignMasterDetailKeys, dcoSaveExpanding]
        DataController.Summary.DefaultGroupSummaryItems = <
          item
            Kind = skCount
            Position = spFooter
            Column = cxGrid1DBTableView1BROJ
          end>
        DataController.Summary.FooterSummaryItems = <
          item
            Kind = skCount
            Column = cxGrid1DBTableView1BROJ
          end>
        DataController.Summary.SummaryGroups = <>
        Filtering.ColumnFilteredItemsList = True
        FilterRow.InfoText = #1050#1083#1080#1082#1085#1080' '#1086#1074#1076#1077' '#1079#1072' '#1076#1072' '#1076#1077#1092#1080#1085#1080#1088#1072#1096' '#1092#1080#1083#1090#1077#1088
        FilterRow.Visible = True
        FilterRow.ApplyChanges = fracImmediately
        OptionsBehavior.CellHints = True
        OptionsBehavior.IncSearch = True
        OptionsCustomize.ColumnsQuickCustomization = True
        OptionsData.Deleting = False
        OptionsData.Editing = False
        OptionsData.Inserting = False
        OptionsSelection.HideFocusRectOnExit = False
        OptionsView.NoDataToDisplayInfoText = '<'#1053#1077#1084#1072' '#1077#1074#1080#1076#1077#1085#1094#1080#1080'>'
        OptionsView.Footer = True
        OptionsView.GroupFooters = gfVisibleWhenExpanded
        Styles.OnGetContentStyle = cxGrid1DBTableView1StylesGetContentStyle
        object cxGrid1DBTableView1ID: TcxGridDBColumn
          DataBinding.FieldName = 'ID'
          Visible = False
        end
        object cxGrid1DBTableView1RE: TcxGridDBColumn
          DataBinding.FieldName = 'RE'
          Visible = False
        end
        object cxGrid1DBTableView1BROJ: TcxGridDBColumn
          DataBinding.FieldName = 'BROJ'
          Width = 72
        end
        object cxGrid1DBTableView1GODINA: TcxGridDBColumn
          DataBinding.FieldName = 'GODINA'
        end
        object cxGrid1DBTableView1MAG_NAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'MAG_NAZIV'
          Visible = False
          Width = 150
        end
        object cxGrid1DBTableView1TIP_DOK: TcxGridDBColumn
          DataBinding.FieldName = 'TIP_DOK'
          Visible = False
        end
        object cxGrid1DBTableView1TIP_DOK_NAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'TIP_DOK_NAZIV'
          Visible = False
          Width = 150
        end
        object cxGrid1DBTableView1DATUM: TcxGridDBColumn
          DataBinding.FieldName = 'DATUM'
          Width = 102
        end
        object cxGrid1DBTableView1TP: TcxGridDBColumn
          DataBinding.FieldName = 'TP'
          Visible = False
        end
        object cxGrid1DBTableView1P: TcxGridDBColumn
          DataBinding.FieldName = 'P'
          Visible = False
        end
        object cxGrid1DBTableView1PARTNER_NAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'PARTNER_NAZIV'
          Visible = False
          Width = 250
        end
        object cxGrid1DBTableView1MESTO_NAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'MESTO_NAZIV'
          Visible = False
          Width = 100
        end
        object cxGrid1DBTableView1RE2: TcxGridDBColumn
          DataBinding.FieldName = 'RE2'
          Visible = False
        end
        object cxGrid1DBTableView1RE2_NAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'RE2_NAZIV'
          Visible = False
          Width = 150
        end
        object cxGrid1DBTableView1OBJEKT: TcxGridDBColumn
          DataBinding.FieldName = 'OBJEKT'
          Visible = False
        end
        object cxGrid1DBTableView1OBJ_NAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'OBJ_NAZIV'
          Visible = False
          Width = 150
        end
        object cxGrid1DBTableView1OBJ_TP: TcxGridDBColumn
          DataBinding.FieldName = 'OBJ_TP'
          Visible = False
        end
        object cxGrid1DBTableView1OBJ_P: TcxGridDBColumn
          DataBinding.FieldName = 'OBJ_P'
          Visible = False
        end
        object cxGrid1DBTableView1OBJ_PARTNER_NAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'OBJ_PARTNER_NAZIV'
          Visible = False
          Width = 150
        end
        object cxGrid1DBTableView1OBJ_REGION: TcxGridDBColumn
          DataBinding.FieldName = 'OBJ_REGION'
          Visible = False
        end
        object cxGrid1DBTableView1OBJ_REGION_NAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'OBJ_REGION_NAZIV'
          Visible = False
          Width = 100
        end
        object cxGrid1DBTableView1OBJ_TIP: TcxGridDBColumn
          DataBinding.FieldName = 'OBJ_TIP'
          Visible = False
        end
        object cxGrid1DBTableView1OBJ_ADRESA: TcxGridDBColumn
          DataBinding.FieldName = 'OBJ_ADRESA'
          Visible = False
          Width = 100
        end
        object cxGrid1DBTableView1OBJ_MESTO: TcxGridDBColumn
          DataBinding.FieldName = 'OBJ_MESTO'
          Visible = False
        end
        object cxGrid1DBTableView1OBJ_MESTO_NAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'OBJ_MESTO_NAZIV'
          Visible = False
        end
        object cxGrid1DBTableView1OPIS: TcxGridDBColumn
          DataBinding.FieldName = 'OPIS'
          Visible = False
          Width = 200
        end
        object cxGrid1DBTableView1REF_NO: TcxGridDBColumn
          DataBinding.FieldName = 'REF_NO'
          Visible = False
          Width = 237
        end
        object cxGrid1DBTableView1VALUTA: TcxGridDBColumn
          DataBinding.FieldName = 'VALUTA'
          Visible = False
        end
        object cxGrid1DBTableView1VAL_NAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'VAL_NAZIV'
          Visible = False
          Width = 154
        end
        object cxGrid1DBTableView1KURS: TcxGridDBColumn
          DataBinding.FieldName = 'KURS'
          Visible = False
        end
        object cxGrid1DBTableView1CENOVNIK: TcxGridDBColumn
          DataBinding.FieldName = 'CENOVNIK'
          Visible = False
        end
        object cxGrid1DBTableView1CEN_OPIS: TcxGridDBColumn
          DataBinding.FieldName = 'CEN_OPIS'
          Visible = False
          Width = 150
        end
        object cxGrid1DBTableView1CEN_VAL: TcxGridDBColumn
          DataBinding.FieldName = 'CEN_VAL'
          Visible = False
        end
        object cxGrid1DBTableView1CEN_DDV: TcxGridDBColumn
          DataBinding.FieldName = 'CEN_DDV'
          Visible = False
        end
        object cxGrid1DBTableView1CEN_AKTIVEN: TcxGridDBColumn
          DataBinding.FieldName = 'CEN_AKTIVEN'
          Visible = False
        end
        object cxGrid1DBTableView1CEN_VAZI_OD: TcxGridDBColumn
          DataBinding.FieldName = 'CEN_VAZI_OD'
          Visible = False
        end
        object cxGrid1DBTableView1CEN_TIP: TcxGridDBColumn
          DataBinding.FieldName = 'CEN_TIP'
          Visible = False
        end
        object cxGrid1DBTableView1FAKTURA_ID: TcxGridDBColumn
          DataBinding.FieldName = 'FAKTURA_ID'
          Visible = False
        end
        object cxGrid1DBTableView1TIP_KNIZENJE: TcxGridDBColumn
          DataBinding.FieldName = 'TIP_KNIZENJE'
          Visible = False
        end
        object cxGrid1DBTableView1TK_NAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'TK_NAZIV'
          Visible = False
        end
        object cxGrid1DBTableView1TK_KNIZENJE: TcxGridDBColumn
          DataBinding.FieldName = 'TK_KNIZENJE'
          Visible = False
        end
        object cxGrid1DBTableView1TIP_NALOG: TcxGridDBColumn
          DataBinding.FieldName = 'TIP_NALOG'
          Visible = False
        end
        object cxGrid1DBTableView1NALOG: TcxGridDBColumn
          DataBinding.FieldName = 'NALOG'
          Visible = False
        end
        object cxGrid1DBTableView1NALOG_DATUM: TcxGridDBColumn
          DataBinding.FieldName = 'NALOG_DATUM'
          Visible = False
        end
        object cxGrid1DBTableView1NALOG_OPIS: TcxGridDBColumn
          DataBinding.FieldName = 'NALOG_OPIS'
          Visible = False
          Width = 150
        end
        object cxGrid1DBTableView1NALOG_VID: TcxGridDBColumn
          DataBinding.FieldName = 'NALOG_VID'
          Visible = False
        end
        object cxGrid1DBTableView1REF_ID: TcxGridDBColumn
          DataBinding.FieldName = 'REF_ID'
          Visible = False
        end
        object cxGrid1DBTableView1WO_REF_ID: TcxGridDBColumn
          DataBinding.FieldName = 'WO_REF_ID'
          Width = 95
        end
        object cxGrid1DBTableView1RN_BROJ_GOD: TcxGridDBColumn
          DataBinding.FieldName = 'RN_BROJ_GOD'
          Width = 179
        end
        object cxGrid1DBTableView1RN_DATUM_PLANIRAN_POCETOK: TcxGridDBColumn
          DataBinding.FieldName = 'RN_DATUM_PLANIRAN_POCETOK'
          Visible = False
          Width = 135
        end
        object cxGrid1DBTableView1WO_S_REF_ID: TcxGridDBColumn
          DataBinding.FieldName = 'WO_S_REF_ID'
          Visible = False
          Width = 84
        end
        object cxGrid1DBTableView1WO_ARTVID: TcxGridDBColumn
          DataBinding.FieldName = 'WO_ARTVID'
          Width = 45
        end
        object cxGrid1DBTableView1WO_ARTSIF: TcxGridDBColumn
          DataBinding.FieldName = 'WO_ARTSIF'
          Width = 60
        end
        object cxGrid1DBTableView1WO_ARTIKAL: TcxGridDBColumn
          DataBinding.FieldName = 'WO_ARTIKAL'
          Width = 254
        end
        object cxGrid1DBTableView1SO_REF_ID: TcxGridDBColumn
          DataBinding.FieldName = 'SO_REF_ID'
          Visible = False
        end
        object cxGrid1DBTableView1SAL_PREDMET: TcxGridDBColumn
          DataBinding.FieldName = 'SAL_PREDMET'
          Visible = False
          Width = 150
        end
        object cxGrid1DBTableView1SAL_BROJ: TcxGridDBColumn
          DataBinding.FieldName = 'SAL_BROJ'
          Visible = False
        end
        object cxGrid1DBTableView1SAL_GODINA: TcxGridDBColumn
          DataBinding.FieldName = 'SAL_GODINA'
          Visible = False
        end
        object cxGrid1DBTableView1AKTIVEN: TcxGridDBColumn
          DataBinding.FieldName = 'AKTIVEN'
          Visible = False
        end
        object cxGrid1DBTableView1KONTROLOR: TcxGridDBColumn
          DataBinding.FieldName = 'KONTROLOR'
          Visible = False
        end
        object cxGrid1DBTableView1KONTROLIRAN: TcxGridDBColumn
          DataBinding.FieldName = 'KONTROLIRAN'
          Visible = False
        end
        object cxGrid1DBTableView1KONTROLIRAN_NA: TcxGridDBColumn
          DataBinding.FieldName = 'KONTROLIRAN_NA'
          Visible = False
        end
        object cxGrid1DBTableView1LIKVIDIRAN: TcxGridDBColumn
          DataBinding.FieldName = 'LIKVIDIRAN'
          Visible = False
        end
        object cxGrid1DBTableView1LIKVIDIRAN_NA: TcxGridDBColumn
          DataBinding.FieldName = 'LIKVIDIRAN_NA'
          Visible = False
        end
        object cxGrid1DBTableView1LIKVIDATOR: TcxGridDBColumn
          DataBinding.FieldName = 'LIKVIDATOR'
          Visible = False
        end
        object cxGrid1DBTableView1PECATEN: TcxGridDBColumn
          DataBinding.FieldName = 'PECATEN'
          Visible = False
        end
        object cxGrid1DBTableView1PECATEN_NA: TcxGridDBColumn
          DataBinding.FieldName = 'PECATEN_NA'
          Visible = False
        end
        object cxGrid1DBTableView1PECATEN_OD: TcxGridDBColumn
          DataBinding.FieldName = 'PECATEN_OD'
          Visible = False
        end
        object cxGrid1DBTableView1SITE_ID: TcxGridDBColumn
          DataBinding.FieldName = 'SITE_ID'
          Visible = False
        end
        object cxGrid1DBTableView1CUSTOM1: TcxGridDBColumn
          DataBinding.FieldName = 'CUSTOM1'
          Visible = False
          Width = 100
        end
        object cxGrid1DBTableView1CUSTOM2: TcxGridDBColumn
          DataBinding.FieldName = 'CUSTOM2'
          Visible = False
          Width = 100
        end
        object cxGrid1DBTableView1CUSTOM3: TcxGridDBColumn
          DataBinding.FieldName = 'CUSTOM3'
          Visible = False
          Width = 100
        end
        object cxGrid1DBTableView1TS_INS: TcxGridDBColumn
          DataBinding.FieldName = 'TS_INS'
          Visible = False
        end
        object cxGrid1DBTableView1TS_UPD: TcxGridDBColumn
          DataBinding.FieldName = 'TS_UPD'
          Visible = False
        end
        object cxGrid1DBTableView1USR_INS: TcxGridDBColumn
          DataBinding.FieldName = 'USR_INS'
          Visible = False
        end
        object cxGrid1DBTableView1USR_UPD: TcxGridDBColumn
          DataBinding.FieldName = 'USR_UPD'
          Visible = False
        end
        object cxGrid1DBTableView1POVRATNICA_RE: TcxGridDBColumn
          DataBinding.FieldName = 'POVRATNICA_RE'
          Visible = False
        end
        object cxGrid1DBTableView1POVRATNICA_TD: TcxGridDBColumn
          DataBinding.FieldName = 'POVRATNICA_TD'
          Visible = False
        end
        object cxGrid1DBTableView1POVRATNICA_BR: TcxGridDBColumn
          DataBinding.FieldName = 'POVRATNICA_BR'
          Visible = False
        end
        object cxGrid1DBTableView1STORNO_FLAG: TcxGridDBColumn
          DataBinding.FieldName = 'STORNO_FLAG'
          Visible = False
        end
        object cxGrid1DBTableView1STORNO_DATUM: TcxGridDBColumn
          DataBinding.FieldName = 'STORNO_DATUM'
          Visible = False
        end
        object cxGrid1DBTableView1STORNO_USER: TcxGridDBColumn
          DataBinding.FieldName = 'STORNO_USER'
          Visible = False
        end
        object cxGrid1DBTableView1STORNO_NALOG_ID: TcxGridDBColumn
          DataBinding.FieldName = 'STORNO_NALOG_ID'
          Visible = False
        end
        object cxGrid1DBTableView1STORNO_TIP_NALOG: TcxGridDBColumn
          DataBinding.FieldName = 'STORNO_TIP_NALOG'
          Visible = False
        end
        object cxGrid1DBTableView1STORNO_NALOG: TcxGridDBColumn
          DataBinding.FieldName = 'STORNO_NALOG'
          Visible = False
        end
        object cxGrid1DBTableView1STORNO_NALOG_DATUM: TcxGridDBColumn
          DataBinding.FieldName = 'STORNO_NALOG_DATUM'
          Visible = False
          Width = 123
        end
        object cxGrid1DBTableView1STORNO_NALOG_OPIS: TcxGridDBColumn
          DataBinding.FieldName = 'STORNO_NALOG_OPIS'
          Visible = False
          Width = 150
        end
        object cxGrid1DBTableView1STORNO_NALOG_VID: TcxGridDBColumn
          DataBinding.FieldName = 'STORNO_NALOG_VID'
          Visible = False
        end
        object cxGrid1DBTableView1STORNO_ZABELESKA: TcxGridDBColumn
          DataBinding.FieldName = 'STORNO_ZABELESKA'
          Visible = False
          Width = 150
        end
        object cxGrid1DBTableView1NALOG_BR: TcxGridDBColumn
          DataBinding.FieldName = 'NALOG_BR'
          Width = 51
        end
      end
      object cxGrid1Level1: TcxGridLevel
        GridView = cxGrid1DBTableView1
      end
    end
  end
  object dPanel: TPanel
    Left = 0
    Top = 542
    Width = 974
    Height = 77
    Align = alBottom
    BevelInner = bvLowered
    BevelOuter = bvSpace
    Color = 15790320
    Enabled = False
    ParentBackground = False
    PopupMenu = PopupMenu1
    TabOrder = 1
    DesignSize = (
      974
      77)
    object Label2: TLabel
      Left = 177
      Top = 17
      Width = 67
      Height = 13
      Alignment = taRightJustify
      Caption = #1056#1072#1073'. '#1085#1072#1083#1086#1075' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clBlue
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label3: TLabel
      Left = 17
      Top = 44
      Width = 45
      Height = 13
      Alignment = taRightJustify
      Caption = #1044#1072#1090#1091#1084' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label1: TLabel
      Left = 31
      Top = 17
      Width = 31
      Height = 13
      Caption = #1041#1088#1086#1112' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label4: TLabel
      Left = 181
      Top = 44
      Width = 63
      Height = 13
      Alignment = taRightJustify
      Caption = #1055#1088#1086#1080#1079#1074#1086#1076' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clBlue
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object lbl1: TLabel
      Left = 469
      Top = 17
      Width = 110
      Height = 13
      Alignment = taRightJustify
      Caption = #1053#1072#1083#1086#1075' '#1087#1088#1086#1082#1085#1080#1078#1072#1085' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clBlue
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object OtkaziButton: TcxButton
      Left = 883
      Top = 37
      Width = 75
      Height = 25
      Action = aOtkazi
      Anchors = [akRight, akBottom]
      Colors.Pressed = clGradientActiveCaption
      TabOrder = 5
    end
    object ZapisiButton: TcxButton
      Left = 802
      Top = 37
      Width = 75
      Height = 25
      Action = aZapisi
      Anchors = [akRight, akBottom]
      Colors.Pressed = clGradientActiveCaption
      TabOrder = 4
    end
    object cxExtNalog: TcxDBExtLookupComboBox
      Left = 250
      Top = 14
      DataBinding.DataField = 'WO_REF_ID'
      DataBinding.DataSource = dsEvidencija
      TabOrder = 2
      OnKeyDown = EnterKakoTab
      Width = 139
    end
    object EvidencijaDatum: TcxDBDateEdit
      Tag = 1
      Left = 68
      Top = 41
      DataBinding.DataField = 'DATUM'
      DataBinding.DataSource = dsEvidencija
      TabOrder = 1
      OnKeyDown = EnterKakoTab
      Width = 100
    end
    object EvidencijaBroj: TcxDBTextEdit
      Tag = 1
      Left = 68
      Top = 14
      BeepOnEnter = False
      DataBinding.DataField = 'BROJ'
      DataBinding.DataSource = dsEvidencija
      ParentFont = False
      Properties.BeepOnError = True
      Style.Shadow = False
      TabOrder = 0
      OnKeyDown = EnterKakoTab
      Width = 100
    end
    object cxExtProizvod: TcxDBExtLookupComboBox
      Left = 250
      Top = 41
      DataBinding.DataField = 'WO_S_REF_ID'
      DataBinding.DataSource = dsEvidencija
      TabOrder = 3
      OnEnter = cxExtProizvodEnter
      OnKeyDown = EnterKakoTab
      Width = 461
    end
    object lcbNalog: TcxDBLookupComboBox
      Left = 585
      Top = 14
      TabStop = False
      BeepOnEnter = False
      DataBinding.DataField = 'NALOG'
      DataBinding.DataSource = dsEvidencija
      Properties.ClearKey = 46
      Properties.DropDownAutoSize = True
      Properties.DropDownSizeable = True
      Properties.ListColumns = <>
      Properties.ListOptions.AnsiSort = True
      Properties.ListOptions.CaseInsensitive = True
      TabOrder = 6
      OnKeyDown = EnterKakoTab
      Width = 126
    end
  end
  object dxRibbon1: TdxRibbon
    Left = 0
    Top = 0
    Width = 974
    Height = 126
    BarManager = dxBarManager1
    ColorSchemeName = 'Blue'
    Contexts = <>
    TabOrder = 2
    TabStop = False
    object dxRibbon1Tab1: TdxRibbonTab
      Active = True
      Caption = #1052#1077#1085#1080
      Groups = <
        item
          Caption = #1052#1072#1075#1072#1094#1080#1085
          ToolbarName = 'dxBarManager1Bar5'
        end
        item
          Caption = #1045#1074#1080#1076#1077#1085#1094#1080#1112#1072
          ToolbarName = 'dxBarManager1Bar1'
        end
        item
          ToolbarName = 'dxBarManager1Bar7'
        end
        item
          Caption = #1044#1086#1082#1091#1084#1077#1085#1090
          ToolbarName = 'dxBarManager1Bar6'
        end
        item
          ToolbarName = 'dxBarManager1Bar2'
        end
        item
          ToolbarName = 'dxBarManager1Bar3'
        end>
      Index = 0
    end
    object dxRibbon1Tab2: TdxRibbonTab
      Caption = #1055#1086#1076#1077#1089#1091#1074#1072#1114#1072
      Groups = <
        item
          ToolbarName = 'dxBarManager1Bar4'
        end
        item
          ToolbarName = 'dxBarManager1BarTabela'
        end>
      Index = 1
    end
  end
  object StatusBar1: TdxRibbonStatusBar
    Left = 0
    Top = 619
    Width = 974
    Height = 23
    Panels = <
      item
        PanelStyleClassName = 'TdxStatusBarTextPanelStyle'
        Text = 
          ' F5 - '#1053#1086#1074#1072', F6 - '#1040#1078#1091#1088#1080#1088#1072#1112', F7 - '#1054#1089#1074#1077#1078#1080', F8 - '#1041#1088#1080#1096#1080', F9 - '#1047#1072#1087#1080#1096#1080',' +
          ' Esc - '#1054#1090#1082#1072#1078#1080' / '#1048#1079#1083#1077#1079
      end>
    Ribbon = dxRibbon1
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clDefault
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
  end
  object cxGridPopupMenu1: TcxGridPopupMenu
    Grid = cxGrid1
    PopupMenus = <>
    Left = 51
    Top = 16
  end
  object PopupMenu1: TPopupMenu
    Left = 121
    Top = 51
    object N1: TMenuItem
      Caption = #1057#1085#1080#1084#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      ShortCut = 49235
      OnClick = SaveToIniFileExecute
    end
    object Excel1: TMenuItem
      Caption = #1057#1085#1080#1084#1080' '#1075#1086' '#1074#1086' Excel'
      ShortCut = 49221
    end
  end
  object dxBarManager1: TdxBarManager
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Segoe UI'
    Font.Style = []
    Categories.Strings = (
      'Default')
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    ImageOptions.Images = dmRes.cxSmallImages
    ImageOptions.LargeImages = dmRes.cxLargeImages
    PopupMenuLinks = <>
    UseSystemFont = True
    Left = 121
    Top = 16
    PixelsPerInch = 96
    object dxBarManager1Bar1: TdxBar
      Caption = 'Custom 1'
      CaptionButtons = <>
      DockedLeft = 74
      DockedTop = 0
      FloatLeft = 715
      FloatTop = 157
      FloatClientWidth = 51
      FloatClientHeight = 120
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton1'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton2'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton11'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarLargeButton3'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar2: TdxBar
      Caption = #1040#1082#1094#1080#1080
      CaptionButtons = <>
      DockedLeft = 567
      DockedTop = 0
      FloatLeft = 789
      FloatTop = 120
      FloatClientWidth = 51
      FloatClientHeight = 120
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButtonSoberi'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton5'
        end
        item
          Visible = True
          ItemName = 'dxBarSubItem1'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar3: TdxBar
      Caption = #1048#1079#1083#1077#1079
      CaptionButtons = <>
      DockedLeft = 812
      DockedTop = 0
      FloatLeft = 718
      FloatTop = 62
      FloatClientWidth = 51
      FloatClientHeight = 80
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton7'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton8'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar4: TdxBar
      Caption = #1055#1077#1095#1072#1090#1077#1114#1077
      CaptionButtons = <>
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 676
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton12'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton15'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton9'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarLargeButton13'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1BarTabela: TdxBar
      Caption = #1058#1072#1073#1077#1083#1072
      CaptionButtons = <>
      DockedLeft = 363
      DockedTop = 0
      FloatLeft = 767
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLBtnSnimiIzgled'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarLBtnBrisiIzgled'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar5: TdxBar
      Caption = 'Custom 2'
      CaptionButtons = <>
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 767
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton10'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar6: TdxBar
      Caption = 'Custom 3'
      CaptionButtons = <>
      DockedLeft = 481
      DockedTop = 0
      FloatLeft = 998
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarSubItem2'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar7: TdxBar
      Caption = 'Custom 4'
      CaptionButtons = <>
      DockedLeft = 338
      DockedTop = 0
      FloatLeft = 1008
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton22'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarLargeButton2: TdxBarLargeButton
      Action = aAzuriraj
      Category = 0
    end
    object dxBarLargeButton3: TdxBarLargeButton
      Action = aBrisi
      Category = 0
    end
    object dxBarLargeButton4: TdxBarLargeButton
      Action = aSnimiIzgled
      Category = 0
      ScreenTip = tipSnimiIzgled
    end
    object dxBarLargeButton5: TdxBarLargeButton
      Action = aZacuvajExcel
      Category = 0
      ScreenTip = tipZacuvajExcel
    end
    object dxBarLargeButton6: TdxBarLargeButton
      Action = aPecatiTabela
      Caption = #1055#1077#1095#1072#1090#1080' '#1090#1072#1073#1077#1083#1072
      Category = 0
    end
    object dxBarLargeButton7: TdxBarLargeButton
      Action = aHelp
      Category = 0
    end
    object dxBarLargeButton8: TdxBarLargeButton
      Action = aIzlez
      Category = 0
    end
    object dxBarLargeButton1: TdxBarLargeButton
      Action = aNov
      Caption = #1053#1086#1074#1072
      Category = 0
    end
    object dxBarLargeButton9: TdxBarLargeButton
      Action = aSnimiPecatenje
      Category = 0
      ScreenTip = tipSnimiKonf
    end
    object ribbonFilterReset: TdxBarLargeButton
      Caption = #1056#1077#1089#1077#1090#1080#1088#1072#1112' '#1092#1080#1083#1090#1077#1088
      Category = 0
      Hint = #1056#1077#1089#1077#1090#1080#1088#1072#1112' '#1092#1080#1083#1090#1077#1088
      Visible = ivAlways
    end
    object dxBarLargeButton12: TdxBarLargeButton
      Action = aPodesuvanjePecatenje
      Category = 0
      ScreenTip = tipDizajnReport
    end
    object dxBarLargeButton13: TdxBarLargeButton
      Action = aBrisiPodesuvanjePecatenje
      Category = 0
      ScreenTip = tipBrisiKonf
    end
    object dxBarLargeButton14: TdxBarLargeButton
      Caption = #1048#1079#1073#1088#1080#1096#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      Category = 0
      Visible = ivAlways
      LargeImageIndex = 40
    end
    object dxBarLargeButton15: TdxBarLargeButton
      Action = aPageSetup
      Category = 0
      ScreenTip = tipSetiranjeStrana
    end
    object dxBarSubItem1: TdxBarSubItem
      Caption = #1055#1077#1095#1072#1090#1077#1114#1077
      Category = 0
      Visible = ivAlways
      ImageIndex = 30
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton18'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton19'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarLargeButton6'
        end>
    end
    object cxBarEditItem1: TcxBarEditItem
      Caption = 'Skin :'
      Category = 0
      Hint = 'Skin :'
      Visible = ivAlways
      Width = 160
      PropertiesClassName = 'TcxComboBoxProperties'
      Properties.OnChange = cxBarEditItem1PropertiesChange
    end
    object dxBarLBtnSnimiIzgled: TdxBarLargeButton
      Action = aSnimiIzgled
      Category = 0
    end
    object dxBarLBtnBrisiIzgled: TdxBarLargeButton
      Action = aBrisiIzgled
      Category = 0
    end
    object dxBarLargeButtonSoberi: TdxBarLargeButton
      Action = aSpustiSoberi
      Category = 0
    end
    object dxBarLargeButton10: TdxBarLargeButton
      Action = aMagacin
      Category = 0
    end
    object dxBarLargeButton11: TdxBarLargeButton
      Action = aEditStavka
      Category = 0
    end
    object dxBarLargeButton16: TdxBarLargeButton
      Action = aLikvidiraj
      Category = 0
    end
    object dxBarSubItem2: TdxBarSubItem
      Caption = #1045#1074#1080#1076#1077#1085#1094#1080#1112#1072
      Category = 0
      Visible = ivAlways
      ImageIndex = 94
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton16'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton17'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarLargeButton20'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton21'
        end>
    end
    object dxBarLargeButton17: TdxBarLargeButton
      Action = aBrisiKnigCeni
      Category = 0
    end
    object dxBarLargeButton18: TdxBarLargeButton
      Action = aPecatiPotrosok
      Category = 0
    end
    object dxBarLargeButton19: TdxBarLargeButton
      Action = aPecatiPotrosokKnig
      Category = 0
    end
    object dxBarLargeButton20: TdxBarLargeButton
      Action = aBrisiNalog
      Category = 0
    end
    object dxBarLargeButton21: TdxBarLargeButton
      Action = aAzurirajNalog
      Category = 0
    end
    object dxBarLargeButton22: TdxBarLargeButton
      Action = aPotrosenoMatVS_Sumirano
      Category = 0
    end
  end
  object ActionList1: TActionList
    Images = dmRes.cxSmallImages
    Left = 16
    Top = 16
    object aNov: TAction
      Caption = #1053#1086#1074
      ImageIndex = 10
      ShortCut = 116
      OnExecute = aNovExecute
    end
    object aAzuriraj: TAction
      Caption = #1040#1078#1091#1088#1080#1088#1072#1112
      ImageIndex = 12
      ShortCut = 117
      OnExecute = aAzurirajExecute
    end
    object aBrisi: TAction
      Caption = #1041#1088#1080#1096#1080
      ImageIndex = 11
      ShortCut = 119
      OnExecute = aBrisiExecute
    end
    object aSnimiIzgled: TAction
      Caption = #1057#1085#1080#1084#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      ImageIndex = 9
      ShortCut = 24659
      OnExecute = aSnimiIzgledExecute
    end
    object aZacuvajExcel: TAction
      Caption = #1047#1072#1095#1091#1074#1072#1112' '#1074#1086' Excel'
      ImageIndex = 9
      ShortCut = 24645
      OnExecute = aZacuvajExcelExecute
    end
    object aPecatiTabela: TAction
      Caption = #1055#1077#1095#1072#1090#1080
      ImageIndex = 30
      OnExecute = aPecatiTabelaExecute
    end
    object aHelp: TAction
      Caption = #1055#1086#1084#1086#1096
      ImageIndex = 24
    end
    object aIzlez: TAction
      Caption = #1048#1079#1083#1077#1079
      ImageIndex = 25
      OnExecute = aIzlezExecute
    end
    object aZapisi: TAction
      Caption = #1047#1072#1087#1080#1096#1080
      ImageIndex = 7
      ShortCut = 120
      OnExecute = aZapisiExecute
    end
    object aOtkazi: TAction
      Caption = #1054#1090#1082#1072#1078#1080
      ImageIndex = 21
      ShortCut = 27
      OnExecute = aOtkaziExecute
    end
    object aRefresh: TAction
      Caption = 'aRefresh'
      ImageIndex = 18
      ShortCut = 118
      OnExecute = aRefreshExecute
    end
    object aSnimiPecatenje: TAction
      Caption = #1057#1085#1080#1084#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      ImageIndex = 39
      OnExecute = aSnimiPecatenjeExecute
    end
    object aPodesuvanjePecatenje: TAction
      Caption = #1044#1080#1079#1072#1112#1085' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090
      ImageIndex = 38
      OnExecute = aPodesuvanjePecatenjeExecute
    end
    object aBrisiPodesuvanjePecatenje: TAction
      Caption = #1041#1088#1080#1096#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      ImageIndex = 37
      OnExecute = aBrisiPodesuvanjePecatenjeExecute
    end
    object aPageSetup: TAction
      Caption = #1057#1077#1090#1080#1088#1072#1114#1077' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072
      ImageIndex = 38
      OnExecute = aPageSetupExecute
    end
    object aFormConfig: TAction
      Caption = 'aFormConfig'
      ShortCut = 24699
      OnExecute = aFormConfigExecute
    end
    object aBrisiIzgled: TAction
      Caption = #1041#1088#1080#1096#1080' '#1075#1086' '#1079#1072#1095#1091#1074#1072#1085#1080#1086#1090' '#1080#1079#1075#1083#1077#1076
      ImageIndex = 56
      ShortCut = 24644
      OnExecute = aBrisiIzgledExecute
    end
    object aSpustiSoberi: TAction
      Caption = #1057#1087#1091#1096#1090#1080'/'#1057#1086#1073#1077#1088#1080' '#1058#1072#1073#1077#1083#1072
      ImageIndex = 65
      OnExecute = aSpustiSoberiExecute
    end
    object aMagacin: TAction
      Caption = #1052#1072#1075#1072#1094#1080#1085
      Hint = #1055#1088#1086#1084#1077#1085#1080' '#1075#1086' '#1084#1086#1084#1077#1085#1090#1072#1083#1085#1080#1086#1090' '#1084#1072#1075#1072#1094#1080#1085
      ImageIndex = 53
      ShortCut = 113
      OnExecute = aMagacinExecute
    end
    object aResetNurkovci: TAction
      Caption = 'aResetNurkovci'
      OnExecute = aResetNurkovciExecute
    end
    object aEditStavka: TAction
      Caption = #1054#1090#1074#1086#1088#1080' '#1084#1072#1090#1077#1088#1080#1112#1072#1083#1080
      Hint = #1054#1090#1074#1086#1088#1080' '#1112#1072' '#1083#1080#1089#1090#1072#1090#1072' '#1085#1072' '#1084#1072#1090#1077#1088#1080#1112#1072#1083#1080' '#1079#1072' '#1077#1074#1080#1076#1077#1085#1094#1080#1112#1072#1090#1072
      ImageIndex = 89
      OnExecute = aEditStavkaExecute
    end
    object aLikvidiraj: TAction
      Caption = #1051#1080#1082#1074#1080#1076#1080#1088#1072#1112' ('#1075#1077#1085#1077#1088#1080#1088#1072#1112' '#1082#1085#1080#1075'. '#1094#1077#1085#1072')'
      ImageIndex = 17
      OnExecute = aLikvidirajExecute
    end
    object aGenerirajKnigCeni: TAction
      Caption = #1043#1077#1085#1077#1088#1080#1088#1072#1112' '#1082#1085#1080#1075'. '#1094#1077#1085#1080
      OnExecute = aGenerirajKnigCeniExecute
    end
    object aBrisiKnigCeni: TAction
      Caption = #1041#1088#1080#1096#1080' '#1082#1085#1080#1075'. '#1094#1077#1085#1080
      ImageIndex = 13
      OnExecute = aBrisiKnigCeniExecute
    end
    object aBrisiNalog: TAction
      Caption = #1041#1088#1080#1096#1080' '#1085#1072#1083#1086#1075' '#1086#1076' '#1060#1080#1085#1072#1085#1089#1086#1074#1086
      ImageIndex = 40
      OnExecute = aBrisiNalogExecute
    end
    object aPecatiPotrosok: TAction
      Caption = #1055#1077#1095#1072#1090#1080' '#1087#1086#1090#1088#1086#1096#1086#1082
      ImageIndex = 30
      ShortCut = 121
      OnExecute = aPecatiPotrosokExecute
    end
    object aDizajnPotrosok: TAction
      Caption = 'aDizajnPotrosok'
      ShortCut = 24697
      OnExecute = aDizajnPotrosokExecute
    end
    object aPecatiPotrosokKnig: TAction
      Caption = #1055#1077#1095#1072#1090#1080' '#1087#1086#1090#1088#1086#1096#1086#1082' ('#1089#1086' '#1082#1085#1080#1075'. '#1094#1077#1085#1072')'
      ImageIndex = 30
      ShortCut = 122
      OnExecute = aPecatiPotrosokKnigExecute
    end
    object aDizajnPotrosokKnig: TAction
      Caption = 'aDizajnPotrosokKnig'
      ShortCut = 24698
      OnExecute = aDizajnPotrosokKnigExecute
    end
    object aAzurirajNalog: TAction
      Caption = #1040#1078#1091#1088#1080#1088#1072#1112' '#1085#1072#1083#1086#1075' '#1086#1076' '#1060#1080#1085#1072#1085#1089#1086#1074#1086
      ImageIndex = 41
      OnExecute = aAzurirajNalogExecute
    end
    object aPotrosenoMatVS_Sumirano: TAction
      Caption = #1055#1086#1090#1088#1086#1096#1077#1085' '#1084#1072#1090#1077#1088#1080#1112#1072#1083' '#1089#1087#1088#1077#1084#1072' '#1085#1086#1088#1084#1072#1090#1080#1074
      ImageIndex = 33
      OnExecute = aPotrosenoMatVS_SumiranoExecute
    end
  end
  object dxComponentPrinter1: TdxComponentPrinter
    CurrentLink = dxComponentPrinter1Link1
    Version = 0
    Left = 191
    Top = 16
    object dxComponentPrinter1Link1: TdxGridReportLink
      Active = True
      Component = cxGrid1
      DateFormat = 0
      PageNumberFormat = pnfNumeral
      PrinterPage.DMPaper = 9
      PrinterPage.Footer = 6350
      PrinterPage.GrayShading = True
      PrinterPage.Header = 6350
      PrinterPage.Margins.Bottom = 12700
      PrinterPage.Margins.Left = 12700
      PrinterPage.Margins.Right = 12700
      PrinterPage.Margins.Top = 18000
      PrinterPage.Orientation = poLandscape
      PrinterPage.PageFooter.RightTitle.Strings = (
        #1057#1090#1088'.[Page #] '#1086#1076' [Total Pages]')
      PrinterPage.PageHeader.RightTitle.Strings = (
        '')
      PrinterPage.PageSize.X = 210000
      PrinterPage.PageSize.Y = 297000
      PrinterPage._dxMeasurementUnits_ = 0
      PrinterPage._dxLastMU_ = 2
      ReportDocument.CreationDate = 44616.371414282410000000
      TimeFormat = 0
      OptionsFormatting.UseNativeStyles = True
      OptionsOnEveryPage.FilterBar = False
      OptionsView.Caption = False
      OptionsView.FilterBar = False
      StyleRepository = dmRes.cxStyleRepositoryPrinting
      Styles.StyleSheet = dmRes.dxGridReportLinkStyleSheet
      BuiltInReportLink = True
    end
  end
  object dxBarScreenTipRepository1: TdxBarScreenTipRepository
    Left = 156
    Top = 16
    PixelsPerInch = 96
    object tipSnimiIzgled: TdxBarScreenTip
      Header.Text = #1057#1085#1080#1084#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      Description.Text = #1047#1072#1095#1091#1074#1072#1112' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090' '#1085#1072' '#1092#1086#1088#1084#1072#1090#1072' '#1080' '#1085#1072' '#1090#1072#1073#1077#1083#1072#1090#1072
    end
    object tipZacuvajExcel: TdxBarScreenTip
      Header.Text = #1047#1072#1095#1091#1074#1072#1112' '#1074#1086' Excel'
      Description.Text = #1057#1085#1080#1084#1080' '#1075#1080' '#1087#1086#1076#1072#1090#1086#1094#1080#1090#1077' '#1086#1076' '#1090#1072#1073#1077#1083#1072#1090#1072' '#1074#1086' Excel '#1092#1086#1088#1084#1072#1090
    end
    object tipDizajnReport: TdxBarScreenTip
      Header.Text = #1044#1080#1079#1072#1112#1085' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090
      Description.Text = #1055#1086#1076#1077#1089#1091#1074#1072#1114#1072' '#1085#1072' '#1080#1079#1075#1083#1077#1076#1086#1090' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090' '#1085#1072' '#1090#1072#1073#1077#1083#1072#1090#1072
    end
    object tipSetiranjeStrana: TdxBarScreenTip
      Header.Text = #1057#1077#1090#1080#1088#1072#1114#1077' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072
      Description.Text = #1055#1086#1076#1077#1089#1091#1074#1072#1114#1077' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072' '#1085#1072' '#1082#1086#1112#1072' '#1116#1077' '#1089#1077' '#1087#1077#1095#1072#1090#1080' '#1090#1072#1073#1077#1083#1072#1090#1072
    end
    object tipSnimiKonf: TdxBarScreenTip
      Header.Text = #1057#1085#1080#1084#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      Description.Text = #1047#1072#1095#1091#1074#1072#1112' '#1075#1080' '#1087#1086#1076#1077#1089#1091#1074#1072#1114#1072#1090#1072' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090' '#1080' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072' '#1079#1072' '#1087#1077#1095#1072#1090#1077#1114#1077
    end
    object tipBrisiKonf: TdxBarScreenTip
      Header.Text = #1041#1088#1080#1096#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      Description.Text = #1048#1079#1073#1088#1080#1096#1080' '#1075#1080' '#1087#1086#1076#1077#1089#1091#1074#1072#1114#1072#1090#1072' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090' '#1080' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072' '#1079#1072' '#1087#1077#1095#1072#1090#1077#1114#1077
    end
  end
  object dsEvidencija: TDataSource
    DataSet = tblEvidencija
    Left = 86
    Top = 16
  end
  object tblEvidencija: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE MTR_OUT'
      'SET '
      '    ID = :ID,'
      '    GODINA = :GODINA,'
      '    RE = :RE,'
      '    TIP_DOK = :TIP_DOK,'
      '    BROJ = :BROJ,'
      '    DATUM = :DATUM,'
      '    TP = :TP,'
      '    P = :P,'
      '    RE2 = :RE2,'
      '    OBJEKT = :OBJEKT,'
      '    OPIS = :OPIS,'
      '    REF_NO = :REF_NO,'
      '    VALUTA = :VALUTA,'
      '    KURS = :KURS,'
      '    CENOVNIK = :CENOVNIK,'
      '    FAKTURA_ID = :FAKTURA_ID,'
      '    TIP_KNIZENJE = :TIP_KNIZENJE,'
      '    NALOG = :NALOG,'
      '    REF_ID = :REF_ID,'
      '    SO_REF_ID = :SO_REF_ID,'
      '    WO_REF_ID = :WO_REF_ID,'
      '    WO_S_REF_ID = :WO_S_REF_ID,'
      '    AKTIVEN = :AKTIVEN,'
      '    KONTROLOR = :KONTROLOR,'
      '    KONTROLIRAN = :KONTROLIRAN,'
      '    KONTROLIRAN_NA = :KONTROLIRAN_NA,'
      '    LIKVIDIRAN = :LIKVIDIRAN,'
      '    LIKVIDIRAN_NA = :LIKVIDIRAN_NA,'
      '    LIKVIDATOR = :LIKVIDATOR,'
      '    PECATEN = :PECATEN,'
      '    PECATEN_NA = :PECATEN_NA,'
      '    PECATEN_OD = :PECATEN_OD,'
      '    SITE_ID = :SITE_ID,'
      '    STORNO_FLAG = :STORNO_FLAG,'
      '    STORNO_DATUM = :STORNO_DATUM,'
      '    STORNO_USER = :STORNO_USER,'
      '    STORNO_NALOG = :STORNO_NALOG_ID,'
      '    STORNO_ZABELESKA = :STORNO_ZABELESKA,'
      '    CUSTOM1 = :CUSTOM1,'
      '    CUSTOM2 = :CUSTOM2,'
      '    CUSTOM3 = :CUSTOM3,'
      '    TS_INS = :TS_INS,'
      '    TS_UPD = :TS_UPD,'
      '    USR_INS = :USR_INS,'
      '    USR_UPD = :USR_UPD'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    MTR_OUT'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO MTR_OUT('
      '    ID,'
      '    GODINA,'
      '    RE,'
      '    TIP_DOK,'
      '    BROJ,'
      '    DATUM,'
      '    TP,'
      '    P,'
      '    RE2,'
      '    OBJEKT,'
      '    OPIS,'
      '    REF_NO,'
      '    VALUTA,'
      '    KURS,'
      '    CENOVNIK,'
      '    FAKTURA_ID,'
      '    TIP_KNIZENJE,'
      '    NALOG,'
      '    REF_ID,'
      '    SO_REF_ID,'
      '    WO_REF_ID,'
      '    WO_S_REF_ID,'
      '    AKTIVEN,'
      '    KONTROLOR,'
      '    KONTROLIRAN,'
      '    KONTROLIRAN_NA,'
      '    LIKVIDIRAN,'
      '    LIKVIDIRAN_NA,'
      '    LIKVIDATOR,'
      '    PECATEN,'
      '    PECATEN_NA,'
      '    PECATEN_OD,'
      '    SITE_ID,'
      '    STORNO_FLAG,'
      '    STORNO_DATUM,'
      '    STORNO_USER,'
      '    STORNO_NALOG,'
      '    STORNO_ZABELESKA,'
      '    CUSTOM1,'
      '    CUSTOM2,'
      '    CUSTOM3,'
      '    TS_INS,'
      '    TS_UPD,'
      '    USR_INS,'
      '    USR_UPD'
      ')'
      'VALUES('
      '    :ID,'
      '    :GODINA,'
      '    :RE,'
      '    :TIP_DOK,'
      '    :BROJ,'
      '    :DATUM,'
      '    :TP,'
      '    :P,'
      '    :RE2,'
      '    :OBJEKT,'
      '    :OPIS,'
      '    :REF_NO,'
      '    :VALUTA,'
      '    :KURS,'
      '    :CENOVNIK,'
      '    :FAKTURA_ID,'
      '    :TIP_KNIZENJE,'
      '    :NALOG,'
      '    :REF_ID,'
      '    :SO_REF_ID,'
      '    :WO_REF_ID,'
      '    :WO_S_REF_ID,'
      '    :AKTIVEN,'
      '    :KONTROLOR,'
      '    :KONTROLIRAN,'
      '    :KONTROLIRAN_NA,'
      '    :LIKVIDIRAN,'
      '    :LIKVIDIRAN_NA,'
      '    :LIKVIDATOR,'
      '    :PECATEN,'
      '    :PECATEN_NA,'
      '    :PECATEN_OD,'
      '    :SITE_ID,'
      '    :STORNO_FLAG,'
      '    :STORNO_DATUM,'
      '    :STORNO_USER,'
      '    :STORNO_NALOG_ID,'
      '    :STORNO_ZABELESKA,'
      '    :CUSTOM1,'
      '    :CUSTOM2,'
      '    :CUSTOM3,'
      '    :TS_INS,'
      '    :TS_UPD,'
      '    :USR_INS,'
      '    :USR_UPD'
      ')')
    RefreshSQL.Strings = (
      'select O.ID,'
      '       O.GODINA,'
      '       O.RE,'
      '       RE.NAZIV as MAG_NAZIV,'
      '       O.TIP_DOK,'
      '       TD.NAZIV as TIP_DOK_NAZIV,'
      '       O.BROJ,'
      '       O.DATUM,'
      '       O.TP,'
      '       O.P,'
      '       P.NAZIV as PARTNER_NAZIV,'
      '       M.NAZIV as MESTO_NAZIV,'
      '       O.RE2,'
      '       R.NAZIV as RE2_NAZIV,'
      '       O.OBJEKT,'
      '       MO.NAZIV as OBJ_NAZIV,'
      '       MO.TP as OBJ_TP,'
      '       MO.P as OBJ_P,'
      '       MP.NAZIV as OBJ_PARTNER_NAZIV,'
      '       MO.REGION as OBJ_REGION,'
      '       MR.NAZIV as OBJ_REGION_NAZIV,'
      '       MO.TIP as OBJ_TIP,'
      '       MO.ADRESA as OBJ_ADRESA,'
      '       MO.MESTO as OBJ_MESTO,'
      '       MM.NAZIV as OBJ_MESTO_NAZIV,'
      '       O.OPIS,'
      '       O.REF_NO,'
      '       O.VALUTA,'
      '       V.NAZIV as VAL_NAZIV,'
      '       O.KURS,'
      '       O.CENOVNIK,'
      '       C.OPIS as CEN_OPIS,'
      '       C.VALUTA as CEN_VAL,'
      '       C.SO_DDV as CEN_DDV,'
      '       C.AKTIVEN as CEN_AKTIVEN,'
      '       C.VAZI_OD as CEN_VAZI_OD,'
      '       C.TIP as CEN_TIP,'
      '       O.FAKTURA_ID,'
      '       O.TIP_KNIZENJE,'
      '       TK.NAZIV as TK_NAZIV,'
      '       TK.KNIZENJE as TK_KNIZENJE,'
      '       O.NALOG,'
      '       NG.TIP_NALOG,'
      '       NG.NALOG as NALOG_BR,'
      '       NG.DATUM as NALOG_DATUM,'
      '       NG.OPIS as NALOG_OPIS,'
      '       NG.VID_NALOG as NALOG_VID,'
      '       O.REF_ID,'
      '       O2.RE as POVRATNICA_RE,'
      '       O2.TIP_DOK as POVRATNICA_TD,'
      '       O2.BROJ as POVRATNICA_BR,'
      '       O.SO_REF_ID,'
      '       SG.PREDMET as SAL_PREDMET,'
      '       SG.BROJ as SAL_BROJ,'
      '       SG.GODINA as SAL_GODINA,'
      '       O.WO_REF_ID,'
      '       O.WO_S_REF_ID,'
      '       RNS.VID_ARTIKAL as WO_ARTVID,'
      '       RNS.ARTIKAL as WO_ARTSIF,'
      '       A.NAZIV as WO_ARTIKAL,'
      '       RN.BROJ || '#39'/'#39' || RN.GODINA as RN_BROJ_GOD,'
      '       RN.DATUM_PLANIRAN_POCETOK as RN_DATUM_PLANIRAN_POCETOK,'
      '       O.AKTIVEN,'
      '       O.KONTROLOR,'
      '       O.KONTROLIRAN,'
      '       O.KONTROLIRAN_NA,'
      '       O.LIKVIDIRAN,'
      '       O.LIKVIDIRAN_NA,'
      '       O.LIKVIDATOR,'
      '       O.PECATEN,'
      '       O.PECATEN_NA,'
      '       O.PECATEN_OD,'
      '       O.SITE_ID,'
      '       O.STORNO_FLAG,'
      '       O.STORNO_DATUM,'
      '       O.STORNO_USER,'
      '       O.STORNO_NALOG as STORNO_NALOG_ID,'
      '       NG.TIP_NALOG as STORNO_TIP_NALOG,'
      '       NG.NALOG as STORNO_NALOG,'
      '       NG.DATUM as STORNO_NALOG_DATUM,'
      '       NG.OPIS as STORNO_NALOG_OPIS,'
      '       NG.VID_NALOG as STORNO_NALOG_VID,'
      '       O.STORNO_ZABELESKA,'
      '       O.CUSTOM1,'
      '       O.CUSTOM2,'
      '       O.CUSTOM3,'
      '       O.TS_INS,'
      '       O.TS_UPD,'
      '       O.USR_INS,'
      '       O.USR_UPD'
      'from MTR_OUT O'
      'join MTR_RE_MAGACIN RM on RM.ID = O.RE'
      'join MAT_RE RE on RE.ID = RM.ID'
      'join MTR_TIP_DOK TD on TD.ID = O.TIP_DOK'
      'left join MAT_PARTNER P on P.TIP_PARTNER = O.TP and P.ID = O.P'
      'left join MAT_MESTO M on M.ID = P.MESTO'
      'left join MAT_RE R on R.ID = O.RE2'
      'left join MAT_OBJEKT MO on MO.ID = O.OBJEKT'
      
        'left join MAT_PARTNER MP on MP.TIP_PARTNER = MO.TP and MP.ID = M' +
        'O.P'
      'left join MAT_MESTO MM on MM.ID = MO.MESTO'
      'left join MAT_REGIONI MR on MR.ID = MO.REGION'
      'left join MAT_VALUTA V on V.ID = O.VALUTA'
      'left join MTR_CENOVNIK C on C.ID = O.CENOVNIK'
      
        'left join FIN_TIP_KNIZENJE TK on TK.TIP_KNIZENJE = O.TIP_KNIZENJ' +
        'E'
      'left join FIN_NG NG on NG.ID = O.NALOG'
      'left join FIN_NG NG2 on NG2.ID = O.STORNO_NALOG'
      'left join SAL_POR_GLAVA SG on SG.ID = O.SO_REF_ID'
      'left join MAN_RABOTEN_NALOG RN on RN.ID = O.WO_REF_ID'
      'left join MAN_RABOTEN_NALOG_STAVKA RNS on RNS.ID = O.WO_S_REF_ID'
      
        'left join MTR_ARTIKAL A on A.ARTVID = RNS.VID_ARTIKAL and A.ID =' +
        ' RNS.ARTIKAL'
      'left join MTR_OUT O2 on O2.ID = O.REF_ID'
      'where(  O.RE = :MAGACIN'
      '      and O.GODINA = :GOD'
      '      and O.TIP_DOK = :TIP -- 0-potrosok, 1-krs'
      '     ) and (     O.ID = :OLD_ID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      'select O.ID,'
      '       O.GODINA,'
      '       O.RE,'
      '       RE.NAZIV as MAG_NAZIV,'
      '       O.TIP_DOK,'
      '       TD.NAZIV as TIP_DOK_NAZIV,'
      '       O.BROJ,'
      '       O.DATUM,'
      '       O.TP,'
      '       O.P,'
      '       P.NAZIV as PARTNER_NAZIV,'
      '       M.NAZIV as MESTO_NAZIV,'
      '       O.RE2,'
      '       R.NAZIV as RE2_NAZIV,'
      '       O.OBJEKT,'
      '       MO.NAZIV as OBJ_NAZIV,'
      '       MO.TP as OBJ_TP,'
      '       MO.P as OBJ_P,'
      '       MP.NAZIV as OBJ_PARTNER_NAZIV,'
      '       MO.REGION as OBJ_REGION,'
      '       MR.NAZIV as OBJ_REGION_NAZIV,'
      '       MO.TIP as OBJ_TIP,'
      '       MO.ADRESA as OBJ_ADRESA,'
      '       MO.MESTO as OBJ_MESTO,'
      '       MM.NAZIV as OBJ_MESTO_NAZIV,'
      '       O.OPIS,'
      '       O.REF_NO,'
      '       O.VALUTA,'
      '       V.NAZIV as VAL_NAZIV,'
      '       O.KURS,'
      '       O.CENOVNIK,'
      '       C.OPIS as CEN_OPIS,'
      '       C.VALUTA as CEN_VAL,'
      '       C.SO_DDV as CEN_DDV,'
      '       C.AKTIVEN as CEN_AKTIVEN,'
      '       C.VAZI_OD as CEN_VAZI_OD,'
      '       C.TIP as CEN_TIP,'
      '       O.FAKTURA_ID,'
      '       O.TIP_KNIZENJE,'
      '       TK.NAZIV as TK_NAZIV,'
      '       TK.KNIZENJE as TK_KNIZENJE,'
      '       O.NALOG,'
      '       NG.TIP_NALOG,'
      '       NG.NALOG as NALOG_BR,'
      '       NG.DATUM as NALOG_DATUM,'
      '       NG.OPIS as NALOG_OPIS,'
      '       NG.VID_NALOG as NALOG_VID,'
      '       O.REF_ID,'
      '       O2.RE as POVRATNICA_RE,'
      '       O2.TIP_DOK as POVRATNICA_TD,'
      '       O2.BROJ as POVRATNICA_BR,'
      '       O.SO_REF_ID,'
      '       SG.PREDMET as SAL_PREDMET,'
      '       SG.BROJ as SAL_BROJ,'
      '       SG.GODINA as SAL_GODINA,'
      '       O.WO_REF_ID,'
      '       O.WO_S_REF_ID,'
      '       RNS.VID_ARTIKAL as WO_ARTVID,'
      '       RNS.ARTIKAL as WO_ARTSIF,'
      '       A.NAZIV as WO_ARTIKAL,'
      '       RN.BROJ || '#39'/'#39' || RN.GODINA as RN_BROJ_GOD,'
      '       RN.DATUM_PLANIRAN_POCETOK as RN_DATUM_PLANIRAN_POCETOK,'
      '       O.AKTIVEN,'
      '       O.KONTROLOR,'
      '       O.KONTROLIRAN,'
      '       O.KONTROLIRAN_NA,'
      '       O.LIKVIDIRAN,'
      '       O.LIKVIDIRAN_NA,'
      '       O.LIKVIDATOR,'
      '       O.PECATEN,'
      '       O.PECATEN_NA,'
      '       O.PECATEN_OD,'
      '       O.SITE_ID,'
      '       O.STORNO_FLAG,'
      '       O.STORNO_DATUM,'
      '       O.STORNO_USER,'
      '       O.STORNO_NALOG as STORNO_NALOG_ID,'
      '       NG.TIP_NALOG as STORNO_TIP_NALOG,'
      '       NG.NALOG as STORNO_NALOG,'
      '       NG.DATUM as STORNO_NALOG_DATUM,'
      '       NG.OPIS as STORNO_NALOG_OPIS,'
      '       NG.VID_NALOG as STORNO_NALOG_VID,'
      '       O.STORNO_ZABELESKA,'
      '       O.CUSTOM1,'
      '       O.CUSTOM2,'
      '       O.CUSTOM3,'
      '       O.TS_INS,'
      '       O.TS_UPD,'
      '       O.USR_INS,'
      '       O.USR_UPD'
      'from MTR_OUT O'
      'join MTR_RE_MAGACIN RM on RM.ID = O.RE'
      'join MAT_RE RE on RE.ID = RM.ID'
      'join MTR_TIP_DOK TD on TD.ID = O.TIP_DOK'
      'left join MAT_PARTNER P on P.TIP_PARTNER = O.TP and P.ID = O.P'
      'left join MAT_MESTO M on M.ID = P.MESTO'
      'left join MAT_RE R on R.ID = O.RE2'
      'left join MAT_OBJEKT MO on MO.ID = O.OBJEKT'
      
        'left join MAT_PARTNER MP on MP.TIP_PARTNER = MO.TP and MP.ID = M' +
        'O.P'
      'left join MAT_MESTO MM on MM.ID = MO.MESTO'
      'left join MAT_REGIONI MR on MR.ID = MO.REGION'
      'left join MAT_VALUTA V on V.ID = O.VALUTA'
      'left join MTR_CENOVNIK C on C.ID = O.CENOVNIK'
      
        'left join FIN_TIP_KNIZENJE TK on TK.TIP_KNIZENJE = O.TIP_KNIZENJ' +
        'E'
      'left join FIN_NG NG on NG.ID = O.NALOG'
      'left join FIN_NG NG2 on NG2.ID = O.STORNO_NALOG'
      'left join SAL_POR_GLAVA SG on SG.ID = O.SO_REF_ID'
      'left join MAN_RABOTEN_NALOG RN on RN.ID = O.WO_REF_ID'
      'left join MAN_RABOTEN_NALOG_STAVKA RNS on RNS.ID = O.WO_S_REF_ID'
      
        'left join MTR_ARTIKAL A on A.ARTVID = RNS.VID_ARTIKAL and A.ID =' +
        ' RNS.ARTIKAL'
      'left join MTR_OUT O2 on O2.ID = O.REF_ID'
      'where O.RE = :MAGACIN'
      '      and O.GODINA = :GOD'
      '      and O.TIP_DOK = :TIP -- 0-potrosok, 1-krs'
      'order by O.ID desc  ')
    AutoUpdateOptions.UpdateTableName = 'MTR_OUT'
    AutoUpdateOptions.KeyFields = 'ID'
    AutoUpdateOptions.GeneratorName = 'GEN_MTR_OUT_ID'
    AutoUpdateOptions.WhenGetGenID = wgBeforePost
    BeforeOpen = tblEvidencijaBeforeOpen
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 226
    Top = 16
    poSQLINT64ToBCD = True
    object tblEvidencijaID: TFIBBCDField
      DisplayLabel = #1064#1080#1092'.'
      FieldName = 'ID'
      Size = 0
    end
    object tblEvidencijaGODINA: TFIBIntegerField
      DisplayLabel = #1043#1086#1076#1080#1085#1072
      FieldName = 'GODINA'
    end
    object tblEvidencijaRE: TFIBIntegerField
      DisplayLabel = #1056#1045
      FieldName = 'RE'
    end
    object tblEvidencijaMAG_NAZIV: TFIBStringField
      DisplayLabel = #1052#1072#1075'. '#1085#1072#1079#1080#1074
      FieldName = 'MAG_NAZIV'
      Size = 255
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblEvidencijaTIP_DOK: TFIBIntegerField
      DisplayLabel = #1058#1080#1087' '#1076#1086#1082'.'
      FieldName = 'TIP_DOK'
    end
    object tblEvidencijaTIP_DOK_NAZIV: TFIBStringField
      DisplayLabel = #1058#1080#1087' '#1076#1086#1082'. '#1085#1072#1079#1080#1074
      FieldName = 'TIP_DOK_NAZIV'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblEvidencijaBROJ: TFIBIntegerField
      DisplayLabel = #1041#1088#1086#1112
      FieldName = 'BROJ'
    end
    object tblEvidencijaDATUM: TFIBDateField
      DisplayLabel = #1044#1072#1090#1091#1084
      FieldName = 'DATUM'
    end
    object tblEvidencijaTP: TFIBIntegerField
      DisplayLabel = #1058#1055
      FieldName = 'TP'
    end
    object tblEvidencijaP: TFIBIntegerField
      DisplayLabel = #1055
      FieldName = 'P'
    end
    object tblEvidencijaPARTNER_NAZIV: TFIBStringField
      DisplayLabel = #1055#1072#1088#1090#1085#1077#1088
      FieldName = 'PARTNER_NAZIV'
      Size = 255
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblEvidencijaMESTO_NAZIV: TFIBStringField
      DisplayLabel = #1052#1077#1089#1090#1086
      FieldName = 'MESTO_NAZIV'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblEvidencijaRE2: TFIBIntegerField
      DisplayLabel = #1056#1072#1073'.'#1077#1076'.'
      FieldName = 'RE2'
    end
    object tblEvidencijaRE2_NAZIV: TFIBStringField
      DisplayLabel = #1056#1072#1073'.'#1077#1076'. '#1085#1072#1079#1080#1074
      FieldName = 'RE2_NAZIV'
      Size = 255
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblEvidencijaOBJEKT: TFIBIntegerField
      DisplayLabel = #1054#1073#1112'. '#1096#1080#1092'.'
      FieldName = 'OBJEKT'
    end
    object tblEvidencijaOBJ_NAZIV: TFIBStringField
      DisplayLabel = #1054#1073#1112#1077#1082#1090
      FieldName = 'OBJ_NAZIV'
      Size = 255
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblEvidencijaOBJ_TP: TFIBIntegerField
      DisplayLabel = #1054#1073#1112'. '#1058#1055
      FieldName = 'OBJ_TP'
    end
    object tblEvidencijaOBJ_P: TFIBIntegerField
      DisplayLabel = #1054#1073#1112'. '#1055
      FieldName = 'OBJ_P'
    end
    object tblEvidencijaOBJ_PARTNER_NAZIV: TFIBStringField
      DisplayLabel = #1054#1073#1112'. '#1087#1072#1088#1090#1085#1077#1088
      FieldName = 'OBJ_PARTNER_NAZIV'
      Size = 255
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblEvidencijaOBJ_REGION: TFIBIntegerField
      DisplayLabel = #1054#1073#1112'. '#1088#1077#1075#1080#1086#1085' '#1096#1080#1092'.'
      FieldName = 'OBJ_REGION'
    end
    object tblEvidencijaOBJ_REGION_NAZIV: TFIBStringField
      DisplayLabel = #1054#1073#1112'. '#1088#1077#1075#1080#1086#1085
      FieldName = 'OBJ_REGION_NAZIV'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblEvidencijaOBJ_TIP: TFIBIntegerField
      DisplayLabel = #1054#1073#1112'. '#1090#1080#1087
      FieldName = 'OBJ_TIP'
    end
    object tblEvidencijaOBJ_ADRESA: TFIBStringField
      DisplayLabel = #1054#1073#1112'. '#1072#1076#1088#1077#1089#1072
      FieldName = 'OBJ_ADRESA'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblEvidencijaOBJ_MESTO: TFIBIntegerField
      DisplayLabel = #1054#1073#1112'. '#1084#1077#1089#1090#1086' '#1096#1080#1092'.'
      FieldName = 'OBJ_MESTO'
    end
    object tblEvidencijaOBJ_MESTO_NAZIV: TFIBStringField
      DisplayLabel = #1054#1073#1112'. '#1084#1077#1089#1090#1086
      FieldName = 'OBJ_MESTO_NAZIV'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblEvidencijaOPIS: TFIBStringField
      DisplayLabel = #1054#1087#1080#1089
      FieldName = 'OPIS'
      Size = 200
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblEvidencijaREF_NO: TFIBStringField
      FieldName = 'REF_NO'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblEvidencijaVALUTA: TFIBStringField
      DisplayLabel = #1042#1072#1083#1091#1090#1072
      FieldName = 'VALUTA'
      Size = 3
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblEvidencijaVAL_NAZIV: TFIBStringField
      DisplayLabel = #1042#1072#1083'. '#1085#1072#1079#1080#1074
      FieldName = 'VAL_NAZIV'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblEvidencijaKURS: TFIBBCDField
      DisplayLabel = #1050#1091#1088#1089
      FieldName = 'KURS'
      Size = 5
    end
    object tblEvidencijaCENOVNIK: TFIBIntegerField
      DisplayLabel = #1062#1077#1085#1086#1074#1085#1080#1082
      FieldName = 'CENOVNIK'
    end
    object tblEvidencijaCEN_OPIS: TFIBStringField
      DisplayLabel = #1062#1077#1085'. '#1086#1087#1080#1089
      FieldName = 'CEN_OPIS'
      Size = 255
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblEvidencijaCEN_VAL: TFIBStringField
      DisplayLabel = #1062#1077#1085'. '#1074#1072#1083#1091#1090#1072
      FieldName = 'CEN_VAL'
      Size = 3
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblEvidencijaCEN_DDV: TFIBSmallIntField
      DisplayLabel = #1062#1077#1085'. '#1044#1044#1042
      FieldName = 'CEN_DDV'
    end
    object tblEvidencijaCEN_AKTIVEN: TFIBSmallIntField
      DisplayLabel = #1062#1077#1085'. '#1072#1082#1090#1080#1074#1077#1085
      FieldName = 'CEN_AKTIVEN'
    end
    object tblEvidencijaCEN_VAZI_OD: TFIBDateField
      DisplayLabel = #1062#1077#1085'. '#1074#1072#1078#1080' '#1086#1076
      FieldName = 'CEN_VAZI_OD'
    end
    object tblEvidencijaCEN_TIP: TFIBSmallIntField
      DisplayLabel = #1062#1077#1085'. '#1090#1080#1087
      FieldName = 'CEN_TIP'
    end
    object tblEvidencijaFAKTURA_ID: TFIBBCDField
      DisplayLabel = #1060#1072#1082#1090#1091#1088#1072' '#1096#1080#1092'.'
      FieldName = 'FAKTURA_ID'
      Size = 0
    end
    object tblEvidencijaTIP_KNIZENJE: TFIBIntegerField
      DisplayLabel = #1058#1080#1087' '#1082#1085#1080#1078'. '#1096#1080#1092'.'
      FieldName = 'TIP_KNIZENJE'
    end
    object tblEvidencijaTK_NAZIV: TFIBStringField
      DisplayLabel = #1058#1080#1087' '#1082#1085#1080#1078#1077#1114#1077
      FieldName = 'TK_NAZIV'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblEvidencijaTK_KNIZENJE: TFIBIntegerField
      DisplayLabel = #1058#1080#1087' '#1082#1085#1080#1078'. '#1073#1088#1086#1112
      FieldName = 'TK_KNIZENJE'
    end
    object tblEvidencijaTIP_NALOG: TFIBStringField
      DisplayLabel = #1058#1080#1087' '#1085#1072#1083#1086#1075
      FieldName = 'TIP_NALOG'
      Size = 5
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblEvidencijaNALOG: TFIBBCDField
      DisplayLabel = #1053#1072#1083#1086#1075' '#1096#1080#1092'.'
      FieldName = 'NALOG'
      Size = 0
    end
    object tblEvidencijaNALOG_BR: TFIBIntegerField
      DisplayLabel = #1053#1072#1083#1086#1075
      FieldName = 'NALOG_BR'
    end
    object tblEvidencijaNALOG_DATUM: TFIBDateField
      DisplayLabel = #1053#1072#1083#1086#1075' '#1076#1072#1090#1091#1084
      FieldName = 'NALOG_DATUM'
    end
    object tblEvidencijaNALOG_OPIS: TFIBStringField
      DisplayLabel = #1053#1072#1083#1086#1075' '#1086#1087#1080#1089
      FieldName = 'NALOG_OPIS'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblEvidencijaNALOG_VID: TFIBSmallIntField
      DisplayLabel = #1053#1072#1083#1086#1075' '#1074#1080#1076
      FieldName = 'NALOG_VID'
    end
    object tblEvidencijaREF_ID: TFIBBCDField
      FieldName = 'REF_ID'
      Size = 0
    end
    object tblEvidencijaSO_REF_ID: TFIBIntegerField
      DisplayLabel = #1055#1088#1086#1076#1072#1078#1073#1072' '#1096#1080#1092'.'
      FieldName = 'SO_REF_ID'
    end
    object tblEvidencijaSAL_PREDMET: TFIBStringField
      DisplayLabel = #1055#1088#1086#1076#1072#1078#1073#1072' '#1087#1088#1077#1076#1084#1077#1090
      FieldName = 'SAL_PREDMET'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblEvidencijaSAL_BROJ: TFIBIntegerField
      DisplayLabel = #1055#1088#1086#1076#1072#1078#1073#1072' '#1073#1088'.'
      FieldName = 'SAL_BROJ'
    end
    object tblEvidencijaSAL_GODINA: TFIBIntegerField
      DisplayLabel = #1055#1088#1086#1076#1072#1078#1073#1072' '#1075#1086#1076'.'
      FieldName = 'SAL_GODINA'
    end
    object tblEvidencijaAKTIVEN: TFIBSmallIntField
      DisplayLabel = #1040#1082#1090#1080#1074#1077#1085
      FieldName = 'AKTIVEN'
    end
    object tblEvidencijaKONTROLOR: TFIBStringField
      DisplayLabel = #1050#1086#1085#1090#1088#1086#1083#1086#1088
      FieldName = 'KONTROLOR'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblEvidencijaKONTROLIRAN: TFIBSmallIntField
      DisplayLabel = #1050#1086#1085#1090#1088#1086#1083#1080#1088#1072#1085#1086
      FieldName = 'KONTROLIRAN'
    end
    object tblEvidencijaKONTROLIRAN_NA: TFIBDateTimeField
      DisplayLabel = #1044#1072#1090#1091#1084' '#1082#1086#1085#1090#1088#1086#1083#1072
      FieldName = 'KONTROLIRAN_NA'
    end
    object tblEvidencijaLIKVIDIRAN: TFIBSmallIntField
      DisplayLabel = #1051#1080#1082#1074#1080#1076#1080#1088#1072#1085#1086
      FieldName = 'LIKVIDIRAN'
    end
    object tblEvidencijaLIKVIDIRAN_NA: TFIBDateTimeField
      DisplayLabel = #1044#1072#1090#1091#1084' '#1083#1080#1082#1074#1080#1076#1080#1088#1072#1085#1086
      FieldName = 'LIKVIDIRAN_NA'
    end
    object tblEvidencijaLIKVIDATOR: TFIBStringField
      DisplayLabel = #1051#1080#1082#1074#1080#1076#1072#1090#1086#1088
      FieldName = 'LIKVIDATOR'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblEvidencijaPECATEN: TFIBSmallIntField
      DisplayLabel = #1055#1077#1095#1072#1090#1077#1085#1086
      FieldName = 'PECATEN'
    end
    object tblEvidencijaPECATEN_NA: TFIBDateTimeField
      DisplayLabel = #1044#1072#1090#1091#1084' '#1087#1077#1095#1072#1090#1077#1085#1086
      FieldName = 'PECATEN_NA'
    end
    object tblEvidencijaPECATEN_OD: TFIBStringField
      DisplayLabel = #1055#1077#1095#1072#1090#1077#1085#1086' '#1086#1076
      FieldName = 'PECATEN_OD'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblEvidencijaSITE_ID: TFIBSmallIntField
      FieldName = 'SITE_ID'
    end
    object tblEvidencijaCUSTOM1: TFIBStringField
      FieldName = 'CUSTOM1'
      Size = 200
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblEvidencijaCUSTOM2: TFIBStringField
      FieldName = 'CUSTOM2'
      Size = 200
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblEvidencijaCUSTOM3: TFIBStringField
      FieldName = 'CUSTOM3'
      Size = 200
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblEvidencijaTS_INS: TFIBDateTimeField
      FieldName = 'TS_INS'
    end
    object tblEvidencijaTS_UPD: TFIBDateTimeField
      FieldName = 'TS_UPD'
    end
    object tblEvidencijaUSR_INS: TFIBStringField
      FieldName = 'USR_INS'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblEvidencijaUSR_UPD: TFIBStringField
      FieldName = 'USR_UPD'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblEvidencijaPOVRATNICA_RE: TFIBIntegerField
      DisplayLabel = #1055#1086#1074#1088#1072#1090#1085#1080#1094#1072' '#1084#1072#1075'.'
      FieldName = 'POVRATNICA_RE'
    end
    object tblEvidencijaPOVRATNICA_TD: TFIBIntegerField
      DisplayLabel = #1055#1086#1074#1088#1072#1090#1085#1080#1094#1072' '#1090#1080#1087' '#1076#1086#1082'.'
      FieldName = 'POVRATNICA_TD'
    end
    object tblEvidencijaPOVRATNICA_BR: TFIBIntegerField
      DisplayLabel = #1055#1086#1074#1088#1072#1090#1085#1080#1094#1072' '#1073#1088'.'
      FieldName = 'POVRATNICA_BR'
    end
    object tblEvidencijaSTORNO_FLAG: TFIBSmallIntField
      DisplayLabel = #1057#1090#1086#1088#1085#1080#1088#1072#1085#1086
      FieldName = 'STORNO_FLAG'
    end
    object tblEvidencijaSTORNO_DATUM: TFIBDateField
      DisplayLabel = #1057#1090#1086#1088#1085#1086' '#1076#1072#1090#1091#1084
      FieldName = 'STORNO_DATUM'
    end
    object tblEvidencijaSTORNO_USER: TFIBStringField
      DisplayLabel = #1057#1090#1086#1088#1085#1080#1088#1072#1083
      FieldName = 'STORNO_USER'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblEvidencijaSTORNO_NALOG_ID: TFIBBCDField
      DisplayLabel = #1057#1090#1086#1088#1085#1086' '#1085#1072#1083#1086#1075' '#1096#1080#1092'.'
      FieldName = 'STORNO_NALOG_ID'
      Size = 0
    end
    object tblEvidencijaSTORNO_TIP_NALOG: TFIBStringField
      DisplayLabel = #1057#1090#1086#1088#1085#1086' '#1090#1080#1087' '#1085#1072#1083#1086#1075
      FieldName = 'STORNO_TIP_NALOG'
      Size = 5
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblEvidencijaSTORNO_NALOG: TFIBIntegerField
      DisplayLabel = #1057#1090#1086#1088#1085#1086' '#1085#1072#1083#1086#1075
      FieldName = 'STORNO_NALOG'
    end
    object tblEvidencijaSTORNO_NALOG_DATUM: TFIBDateField
      DisplayLabel = #1057#1090#1086#1088#1085#1086' '#1085#1072#1083#1086#1075' '#1076#1072#1090#1091#1084
      FieldName = 'STORNO_NALOG_DATUM'
    end
    object tblEvidencijaSTORNO_NALOG_OPIS: TFIBStringField
      DisplayLabel = #1057#1090#1086#1088#1085#1086' '#1085#1072#1083#1086#1075' '#1086#1087#1080#1089
      FieldName = 'STORNO_NALOG_OPIS'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblEvidencijaSTORNO_NALOG_VID: TFIBSmallIntField
      DisplayLabel = #1057#1090#1086#1088#1085#1086' '#1074#1080#1076' '#1085#1072#1083#1086#1075
      FieldName = 'STORNO_NALOG_VID'
    end
    object tblEvidencijaSTORNO_ZABELESKA: TFIBStringField
      DisplayLabel = #1057#1090#1086#1088#1085#1086' '#1079#1072#1073#1077#1083#1077#1096#1082#1072
      FieldName = 'STORNO_ZABELESKA'
      Size = 200
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblEvidencijaWO_REF_ID: TFIBBCDField
      DisplayLabel = #1056#1072#1073'. '#1085#1072#1083#1086#1075'. '#1096#1080#1092'.'
      FieldName = 'WO_REF_ID'
      Size = 0
    end
    object tblEvidencijaRN_BROJ_GOD: TFIBStringField
      DisplayLabel = #1056#1072#1073'. '#1085#1072#1083#1086#1075
      FieldName = 'RN_BROJ_GOD'
      Size = 112
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblEvidencijaRN_DATUM_PLANIRAN_POCETOK: TFIBDateTimeField
      DisplayLabel = #1056#1072#1073'. '#1085#1072#1083#1086#1075' '#1076#1072#1090#1091#1084' '#1087#1083#1072#1085'.'
      FieldName = 'RN_DATUM_PLANIRAN_POCETOK'
    end
    object tblEvidencijaWO_S_REF_ID: TFIBBCDField
      DisplayLabel = #1055#1088#1086#1080#1079#1074#1086#1076' '#1096#1080#1092'.'
      FieldName = 'WO_S_REF_ID'
      Size = 0
    end
    object tblEvidencijaWO_ARTVID: TFIBIntegerField
      DisplayLabel = #1055'. '#1074#1080#1076
      FieldName = 'WO_ARTVID'
    end
    object tblEvidencijaWO_ARTSIF: TFIBIntegerField
      DisplayLabel = #1055'. '#1096#1080#1092'.'
      FieldName = 'WO_ARTSIF'
    end
    object tblEvidencijaWO_ARTIKAL: TFIBStringField
      DisplayLabel = #1055#1088#1086#1080#1079#1074#1086#1076
      FieldName = 'WO_ARTIKAL'
      Size = 500
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object qryZemiBroj: TpFIBQuery
    Transaction = dmKon.qTransaction
    Database = dmKon.fibBaza
    SQL.Strings = (
      'select coalesce(max(o.broj),0) as maks from mtr_out o'
      'where o.godina = :god and o.re = :re and o.tip_dok = :tip')
    Left = 51
    Top = 51
    qoAutoCommit = True
    qoStartTransaction = True
  end
  object spBrisiEvidencija: TpFIBStoredProc
    Transaction = dmKon.tProc
    Database = dmKon.fibBaza
    SQL.Strings = (
      'EXECUTE PROCEDURE PROC_MTR_EVIDENCIJA_BRISI (:IN_MTR_OUT_ID)')
    StoredProcName = 'PROC_MTR_EVIDENCIJA_BRISI'
    Left = 86
    Top = 51
    qoAutoCommit = True
    qoStartTransaction = True
  end
  object qryZemiReRabNalog: TpFIBQuery
    Transaction = dmKon.qTransaction
    Database = dmKon.fibBaza
    SQL.Strings = (
      'select RN.ID_RE'
      'from MAN_RABOTEN_NALOG RN'
      'where RN.ID = :WO_ID')
    Left = 16
    Top = 51
    qoAutoCommit = True
    qoStartTransaction = True
  end
end
