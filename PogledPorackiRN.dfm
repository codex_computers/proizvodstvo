object frmPogledPorackiRN: TfrmPogledPorackiRN
  Left = 128
  Top = 212
  BorderIcons = [biSystemMenu, biMaximize]
  Caption = #1055#1086#1088#1072#1095#1082#1080' '#1074#1086' '#1056#1072#1073#1086#1090#1077#1085' '#1085#1072#1083#1086#1075
  ClientHeight = 700
  ClientWidth = 1000
  Color = clBtnFace
  Constraints.MinHeight = 500
  Constraints.MinWidth = 610
  Font.Charset = RUSSIAN_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  ShowHint = True
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object dxRibbon1: TdxRibbon
    Left = 0
    Top = 0
    Width = 1000
    Height = 126
    BarManager = dxBarManager1
    ColorSchemeName = 'Blue'
    Contexts = <>
    TabOrder = 0
    TabStop = False
    object dxRibbon1Tab1: TdxRibbonTab
      Active = True
      Caption = #1052#1077#1085#1080
      Groups = <
        item
          ToolbarName = 'dxBarManager1Bar1'
        end
        item
          ToolbarName = 'dxBarManager1Bar2'
        end
        item
          ToolbarName = 'dxBarManager1Bar3'
        end>
      Index = 0
    end
    object dxRibbon1Tab2: TdxRibbonTab
      Caption = #1055#1086#1076#1077#1089#1091#1074#1072#1114#1072
      Groups = <
        item
          ToolbarName = 'dxBarManager1Bar4'
        end
        item
          ToolbarName = 'dxBarManager1BarIzgledGrid'
        end>
      Index = 1
    end
  end
  object StatusBar1: TdxRibbonStatusBar
    Left = 0
    Top = 677
    Width = 1000
    Height = 23
    Panels = <
      item
        PanelStyleClassName = 'TdxStatusBarTextPanelStyle'
        Text = 'F5 - '#1041#1088#1080#1096#1080' '#1055#1086#1088#1072#1095#1082#1080' '#1086#1076' '#1056#1053', Esc - '#1054#1090#1082#1072#1078#1080' / '#1048#1079#1083#1077#1079
      end>
    Ribbon = dxRibbon1
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clDefault
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
  end
  object lPanel: TPanel
    Left = 0
    Top = 126
    Width = 480
    Height = 350
    Align = alClient
    TabOrder = 2
    object cxGrid1: TcxGrid
      Left = 1
      Top = 1
      Width = 478
      Height = 348
      Align = alClient
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      RootLevelOptions.DetailTabsPosition = dtpTop
      object cxGrid1DBTableView1: TcxGridDBTableView
        Navigator.Buttons.CustomButtons = <>
        DataController.DataSource = dm.dsPorackaRN
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        FilterRow.InfoText = #1050#1083#1080#1082#1085#1077#1090#1077' '#1090#1091#1082#1072' '#1079#1072' '#1076#1072' '#1076#1077#1092#1080#1085#1080#1088#1072#1090#1077' '#1092#1080#1083#1090#1077#1088
        FilterRow.Visible = True
        OptionsBehavior.IncSearch = True
        OptionsCustomize.ColumnsQuickCustomization = True
        OptionsData.Deleting = False
        OptionsData.Inserting = False
        OptionsSelection.MultiSelect = True
        OptionsSelection.HideFocusRectOnExit = False
        OptionsSelection.HideSelection = True
        OptionsView.NoDataToDisplayInfoText = '<'#1053#1077#1084#1072' '#1087#1086#1076#1072#1090#1086#1094#1080'>'
        OptionsView.Footer = True
        OptionsView.GroupFooters = gfAlwaysVisible
        object cxGrid1DBTableView1Izberi: TcxGridDBColumn
          Caption = #1048#1079#1073#1077#1088#1080
          DataBinding.ValueType = 'Boolean'
          PropertiesClassName = 'TcxCheckBoxProperties'
          Properties.ImmediatePost = True
          Properties.NullStyle = nssUnchecked
          Width = 49
        end
        object cxGrid1DBTableView1ID: TcxGridDBColumn
          Caption = #1064#1080#1092#1088#1072' '#1085#1072' '#1055#1086#1088#1072#1095#1082#1072
          DataBinding.FieldName = 'ID'
          Visible = False
          Options.Editing = False
          Width = 47
        end
        object cxGrid1DBTableView1BROJ: TcxGridDBColumn
          Caption = #1041#1088#1086#1112
          DataBinding.FieldName = 'BROJ'
          Options.Editing = False
          Width = 115
        end
        object cxGrid1DBTableView1REF_NO: TcxGridDBColumn
          Caption = #1056#1077#1092#1077#1088#1077#1085#1090#1077#1085' '#1073#1088#1086#1112
          DataBinding.FieldName = 'REF_NO'
          Options.Editing = False
          Width = 155
        end
        object cxGrid1DBTableView1DATUM: TcxGridDBColumn
          Caption = #1044#1072#1090#1091#1084
          DataBinding.FieldName = 'DATUM'
          Options.Editing = False
          Width = 125
        end
        object cxGrid1DBTableView1ID_RN: TcxGridDBColumn
          Caption = #1064#1080#1092#1088#1072' '#1085#1072' '#1056#1053
          DataBinding.FieldName = 'ID_RN'
          Visible = False
          Options.Editing = False
        end
        object cxGrid1DBTableView1STATUS: TcxGridDBColumn
          Caption = #1057#1090#1072#1090#1091#1089
          DataBinding.FieldName = 'STATUS'
          Visible = False
          Options.Editing = False
        end
        object cxGrid1DBTableView1ID_RE: TcxGridDBColumn
          Caption = #1064#1080#1092#1088#1072' '#1085#1072' '#1056#1045
          DataBinding.FieldName = 'ID_RE'
          Visible = False
          Options.Editing = False
        end
        object cxGrid1DBTableView1GODINA: TcxGridDBColumn
          Caption = #1043#1086#1076#1080#1085#1072
          DataBinding.FieldName = 'GODINA'
          Visible = False
          Options.Editing = False
        end
      end
      object cxGrid1DBTableView2: TcxGridDBTableView
        Navigator.Buttons.CustomButtons = <>
        DataController.DataSource = dm.dsIzberiPStavki
        DataController.DetailKeyFieldNames = #1064#1080#1092#1088#1072
        DataController.KeyFieldNames = 'ID_PORACKA'
        DataController.MasterKeyFieldNames = 'ID'
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        FilterRow.InfoText = #1050#1083#1080#1082#1085#1090#1077#1090#1077' '#1090#1091#1082#1072' '#1079#1072' '#1076#1072' '#1076#1077#1092#1080#1085#1080#1088#1072#1090#1077' '#1092#1080#1083#1090#1077#1088
        FilterRow.Visible = True
        OptionsBehavior.IncSearch = True
        OptionsCustomize.ColumnsQuickCustomization = True
        OptionsData.Deleting = False
        OptionsData.Inserting = False
        OptionsView.GroupByBox = False
        object cxGrid1DBTableView2DBColumn: TcxGridDBColumn
          DataBinding.FieldName = #1064#1080#1092#1088#1072
          Options.Editing = False
          Width = 74
        end
        object cxGrid1DBTableView2DBColumn1: TcxGridDBColumn
          DataBinding.FieldName = #1041#1088#1086#1112
          Options.Editing = False
          Width = 48
        end
        object cxGrid1DBTableView2ID_PORACKA: TcxGridDBColumn
          DataBinding.FieldName = 'ID_PORACKA'
          Visible = False
          Options.Editing = False
          Width = 74
        end
        object cxGrid1DBTableView2DBColumn2: TcxGridDBColumn
          DataBinding.FieldName = #1042#1080#1076' '#1085#1072' '#1087#1088#1086#1080#1079#1074#1086#1076
          Options.Editing = False
          Width = 31
        end
        object cxGrid1DBTableView2DBColumn3: TcxGridDBColumn
          DataBinding.FieldName = #1064#1080#1092#1088#1072' '#1085#1072' '#1087#1088#1086#1080#1079#1074#1086#1076
          Options.Editing = False
          Width = 43
        end
        object cxGrid1DBTableView2DBColumn4: TcxGridDBColumn
          DataBinding.FieldName = #1055#1088#1086#1080#1079#1074#1086#1076
          Options.Editing = False
          Width = 481
        end
        object cxGrid1DBTableView2DBColumn5: TcxGridDBColumn
          DataBinding.FieldName = #1057#1083#1080#1082#1072
          Options.Editing = False
          Width = 74
        end
        object cxGrid1DBTableView2DBColumn6: TcxGridDBColumn
          DataBinding.FieldName = #1058#1072#1087#1072#1094#1080#1088' '#1089#1077#1076#1080#1096#1090#1077
          Options.Editing = False
          Width = 172
        end
        object cxGrid1DBTableView2DBColumn7: TcxGridDBColumn
          DataBinding.FieldName = #1058#1072#1087#1072#1094#1080#1088' '#1085#1072#1079#1072#1076
          Options.Editing = False
          Width = 182
        end
        object cxGrid1DBTableView2DBColumn8: TcxGridDBColumn
          DataBinding.FieldName = #1057#1091#1085#1107#1077#1088
          Options.Editing = False
          Width = 134
        end
        object cxGrid1DBTableView2DBColumn9: TcxGridDBColumn
          DataBinding.FieldName = #1041#1088#1086#1112' '#1085#1072' '#1085#1086#1075#1072#1088#1082#1080
          Options.Editing = False
          Width = 207
        end
        object cxGrid1DBTableView2LABELS: TcxGridDBColumn
          DataBinding.FieldName = 'LABELS'
          Options.Editing = False
          Width = 144
        end
        object cxGrid1DBTableView2GLIDERS: TcxGridDBColumn
          DataBinding.FieldName = 'GLIDERS'
          Options.Editing = False
          Width = 150
        end
        object cxGrid1DBTableView2DBColumn10: TcxGridDBColumn
          DataBinding.FieldName = #1050#1086#1083#1080#1095#1080#1085#1072
          Options.Editing = False
          Width = 74
        end
        object cxGrid1DBTableView2DBColumn11: TcxGridDBColumn
          DataBinding.FieldName = #1047#1072#1073#1077#1083#1077#1096#1082#1072
          Options.Editing = False
          Width = 188
        end
        object cxGrid1DBTableView2DBColumn12: TcxGridDBColumn
          DataBinding.FieldName = #1054#1076' '#1076#1072#1090#1091#1084' '#1085#1072' '#1080#1089#1087#1086#1088#1072#1082#1072
          Options.Editing = False
          Width = 74
        end
        object cxGrid1DBTableView2DBColumn13: TcxGridDBColumn
          DataBinding.FieldName = #1044#1086' '#1076#1072#1090#1091#1084' '#1085#1072' '#1080#1089#1087#1086#1088#1072#1082#1072
          Options.Editing = False
          Width = 74
        end
        object cxGrid1DBTableView2STATUS: TcxGridDBColumn
          DataBinding.FieldName = 'STATUS'
          Visible = False
          Options.Editing = False
          Width = 74
        end
        object cxGrid1DBTableView2Column1: TcxGridDBColumn
          Caption = #1048#1079#1073#1077#1088#1080
          DataBinding.ValueType = 'String'
          PropertiesClassName = 'TcxCheckBoxProperties'
          Properties.ValueChecked = '1'
          Properties.ValueGrayed = '2'
          Properties.ValueUnchecked = '0'
        end
      end
      object cxGrid1Level1: TcxGridLevel
        Caption = ' '#1055#1086#1088#1072#1095#1082#1080', '#1082#1086#1080'  '#1089#1077' '#1074#1083#1077#1079#1077#1085#1080' '#1074#1086' '#1056#1072#1073#1086#1090#1077#1085' '#1085#1072#1083#1086#1075' '
        GridView = cxGrid1DBTableView1
      end
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 484
    Width = 1000
    Height = 193
    Align = alBottom
    TabOrder = 3
    object cxGrid3: TcxGrid
      Left = 1
      Top = 1
      Width = 998
      Height = 191
      Align = alClient
      TabOrder = 0
      Visible = False
      RootLevelOptions.DetailTabsPosition = dtpTop
      object cxGrid3DBTableView1: TcxGridDBTableView
        Navigator.Buttons.CustomButtons = <>
        DataController.DataSource = dm.dsIzberiPElement
        DataController.Options = [dcoAnsiSort, dcoAssignGroupingValues, dcoAssignMasterDetailKeys, dcoSaveExpanding]
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        OptionsBehavior.IncSearch = True
        OptionsCustomize.ColumnsQuickCustomization = True
        OptionsData.Deleting = False
        OptionsData.Editing = False
        OptionsData.Inserting = False
        OptionsView.NoDataToDisplayInfoText = '<'#1053#1077#1084#1072' '#1087#1086#1076#1072#1090#1086#1094#1080'>'
        OptionsView.Footer = True
        OptionsView.FooterMultiSummaries = True
        OptionsView.GroupByBox = False
        OptionsView.Indicator = True
        object cxGrid3DBTableView1ID: TcxGridDBColumn
          Caption = #1064#1080#1092#1088#1072
          DataBinding.FieldName = 'ID'
          Visible = False
        end
        object cxGrid3DBTableView1ID_PORACKA_STAVKA: TcxGridDBColumn
          Caption = #1064#1080#1092#1088#1072' '#1085#1072' '#1089#1090#1072#1074#1082#1072
          DataBinding.FieldName = 'ID_PORACKA_STAVKA'
          Visible = False
        end
        object cxGrid3DBTableView1VID_SUROVINA: TcxGridDBColumn
          Caption = #1042#1080#1076' '#1085#1072' '#1089#1091#1088#1086#1074#1080#1085#1072
          DataBinding.FieldName = 'VID_SUROVINA'
        end
        object cxGrid3DBTableView1SUROVINA: TcxGridDBColumn
          Caption = #1064#1080#1092#1088#1072' '#1085#1072' '#1089#1091#1088#1086#1074#1080#1085#1072
          DataBinding.FieldName = 'SUROVINA'
          Width = 87
        end
        object cxGrid3DBTableView1NAZIV_ELEMENT: TcxGridDBColumn
          Caption = #1057#1091#1088#1086#1074#1080#1085#1072
          DataBinding.FieldName = 'NAZIV_ELEMENT'
          Width = 296
        end
        object cxGrid3DBTableView1KOLICINA: TcxGridDBColumn
          Caption = #1050#1086#1083#1080#1095#1080#1085#1072
          DataBinding.FieldName = 'KOLICINA'
          Width = 70
        end
        object cxGrid3DBTableView1MERKA: TcxGridDBColumn
          Caption = #1052#1077#1088#1082#1072
          DataBinding.FieldName = 'MERKA'
          Visible = False
        end
        object cxGrid3DBTableView1NAZIV_MERKA: TcxGridDBColumn
          Caption = #1052#1077#1088#1082#1072
          DataBinding.FieldName = 'NAZIV_MERKA'
          Width = 135
        end
        object cxGrid3DBTableView1ZABELESKA: TcxGridDBColumn
          Caption = #1047#1072#1073#1077#1083#1077#1096#1082#1072
          DataBinding.FieldName = 'ZABELESKA'
          Width = 359
        end
      end
      object cxGrid3Level1: TcxGridLevel
        Caption = ' '#1057#1091#1088#1086#1074#1080#1085#1080' '#1074#1086' '#1055#1086#1088#1072#1095#1082#1072' '
        GridView = cxGrid3DBTableView1
      end
    end
  end
  object dPanel: TPanel
    Left = 488
    Top = 126
    Width = 512
    Height = 350
    Align = alRight
    TabOrder = 4
    object cxGrid2: TcxGrid
      Left = 1
      Top = 1
      Width = 510
      Height = 348
      Align = alClient
      TabOrder = 0
      RootLevelOptions.DetailTabsPosition = dtpTop
      object cxGrid2DBTableView1: TcxGridDBTableView
        Navigator.Buttons.CustomButtons = <>
        DataController.DataSource = dm.dsPorackaRNStavka
        DataController.Options = [dcoAnsiSort, dcoAssignGroupingValues, dcoAssignMasterDetailKeys, dcoSaveExpanding]
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        FilterRow.InfoText = #1050#1083#1080#1082#1085#1077#1090#1077' '#1090#1091#1082#1072' '#1079#1072' '#1076#1072' '#1076#1077#1092#1080#1085#1080#1088#1072#1090#1077' '#1092#1080#1083#1090#1077#1088
        FilterRow.Visible = True
        OptionsBehavior.IncSearch = True
        OptionsCustomize.ColumnsQuickCustomization = True
        OptionsData.Deleting = False
        OptionsData.Editing = False
        OptionsData.Inserting = False
        OptionsView.NoDataToDisplayInfoText = '<'#1053#1077#1084#1072' '#1087#1086#1076#1072#1090#1086#1094#1080'>'
        OptionsView.Footer = True
        object cxGrid2DBTableView1ID: TcxGridDBColumn
          Caption = #1064#1080#1092#1088#1072
          DataBinding.FieldName = 'ID'
        end
        object cxGrid2DBTableView1BROJ: TcxGridDBColumn
          Caption = #1041#1088#1086#1112
          DataBinding.FieldName = 'BROJ'
          Visible = False
        end
        object cxGrid2DBTableView1ID_PORACKA: TcxGridDBColumn
          Caption = #1064#1080#1092#1088#1072' '#1085#1072' '#1087#1086#1088#1072#1095#1082#1072
          DataBinding.FieldName = 'ID_PORACKA'
          Visible = False
        end
        object cxGrid2DBTableView1VID_ARTIKAL: TcxGridDBColumn
          Caption = #1042#1080#1076' '#1085#1072' '#1072#1088#1090#1080#1082#1072#1083
          DataBinding.FieldName = 'VID_ARTIKAL'
          Width = 30
        end
        object cxGrid2DBTableView1ARTIKAL: TcxGridDBColumn
          Caption = #1064#1080#1092#1088#1072' '#1085#1072' '#1072#1088#1090#1080#1082#1072#1083
          DataBinding.FieldName = 'ARTIKAL'
          Width = 44
        end
        object cxGrid2DBTableView1NAZIV_ARTIKAL: TcxGridDBColumn
          Caption = #1040#1088#1090#1080#1082#1072#1083
          DataBinding.FieldName = 'NAZIV_ARTIKAL'
          Width = 197
        end
        object cxGrid2DBTableView1KOLICINA: TcxGridDBColumn
          Caption = #1050#1086#1083#1080#1095#1080#1085#1072
          DataBinding.FieldName = 'KOLICINA'
          Width = 59
        end
        object cxGrid2DBTableView1SERISKI_BROJ: TcxGridDBColumn
          Caption = #1057#1077#1088#1080#1089#1082#1080' '#1073#1088#1086#1112
          DataBinding.FieldName = 'SERISKI_BROJ'
          Visible = False
        end
        object cxGrid2DBTableView1ZABELESKA: TcxGridDBColumn
          Caption = #1047#1072#1073#1077#1083#1077#1096#1082#1072
          DataBinding.FieldName = 'ZABELESKA'
          Width = 177
        end
        object cxGrid2DBTableView1ISPORAKA_OD_DATUM: TcxGridDBColumn
          Caption = #1048#1089#1087#1086#1088#1072#1082#1072' '#1086#1076' '#1076#1072#1090#1091#1084
          DataBinding.FieldName = 'ISPORAKA_OD_DATUM'
        end
        object cxGrid2DBTableView1ISPORAKA_DO_DATUM: TcxGridDBColumn
          Caption = #1048#1089#1087#1086#1088#1072#1082#1072' '#1076#1086' '#1076#1072#1090#1091#1084
          DataBinding.FieldName = 'ISPORAKA_DO_DATUM'
        end
        object cxGrid2DBTableView1STATUS: TcxGridDBColumn
          Caption = #1057#1090#1072#1090#1091#1089
          DataBinding.FieldName = 'STATUS'
          Visible = False
        end
        object cxGrid2DBTableView1NAZIV2: TcxGridDBColumn
          Caption = #1044#1088#1091#1075' '#1085#1072#1079#1080#1074
          DataBinding.FieldName = 'NAZIV2'
          Visible = False
        end
      end
      object cxGrid2Level1: TcxGridLevel
        Caption = #1057#1090#1072#1074#1082#1080' '#1085#1072' '#1055#1086#1088#1072#1095#1082#1072
        GridView = cxGrid2DBTableView1
      end
    end
  end
  object cxSplitter1: TcxSplitter
    Left = 480
    Top = 126
    Width = 8
    Height = 350
    AlignSplitter = salRight
    Control = dPanel
  end
  object cxSplitter2: TcxSplitter
    Left = 0
    Top = 476
    Width = 1000
    Height = 8
    AlignSplitter = salBottom
    Control = Panel1
  end
  object cxGridPopupMenu1: TcxGridPopupMenu
    Grid = cxGrid1
    PopupMenus = <>
    Left = 368
    Top = 472
  end
  object PopupMenu1: TPopupMenu
    Left = 480
    Top = 472
    object N1: TMenuItem
      Caption = #1057#1085#1080#1084#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      ShortCut = 49235
      OnClick = SaveToIniFileExecute
    end
    object Excel1: TMenuItem
      Caption = #1057#1085#1080#1084#1080' '#1075#1086' '#1074#1086' Excel'
      ShortCut = 49221
    end
  end
  object dxBarManager1: TdxBarManager
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Segoe UI'
    Font.Style = []
    Categories.Strings = (
      'Default')
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    ImageOptions.Images = dmRes.cxSmallImages
    ImageOptions.LargeImages = dmRes.cxLargeImages
    PopupMenuLinks = <>
    UseSystemFont = True
    Left = 560
    Top = 472
    PixelsPerInch = 96
    object dxBarManager1Bar1: TdxBar
      Caption = 'Custom 1'
      CaptionButtons = <>
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 715
      FloatTop = 157
      FloatClientWidth = 51
      FloatClientHeight = 120
      ItemLinks = <
        item
          ViewLayout = ivlGlyphControlCaption
          Visible = True
          ItemName = 'cbSite'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarLargeButton1'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar2: TdxBar
      Caption = #1040#1082#1094#1080#1080
      CaptionButtons = <>
      DockedLeft = 230
      DockedTop = 0
      FloatLeft = 789
      FloatTop = 120
      FloatClientWidth = 51
      FloatClientHeight = 120
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton10'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton5'
        end
        item
          Visible = True
          ItemName = 'dxBarSubItem1'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar3: TdxBar
      Caption = #1048#1079#1083#1077#1079
      CaptionButtons = <>
      DockedLeft = 475
      DockedTop = 0
      FloatLeft = 718
      FloatTop = 62
      FloatClientWidth = 51
      FloatClientHeight = 80
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton7'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton8'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar4: TdxBar
      Caption = #1055#1077#1095#1072#1090#1077#1114#1077
      CaptionButtons = <>
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 676
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton12'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton15'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton9'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarLargeButton13'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1BarIzgledGrid: TdxBar
      Caption = #1058#1072#1073#1077#1083#1072
      CaptionButtons = <>
      DockedLeft = 363
      DockedTop = 0
      FloatLeft = 808
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLBtnSnimiIzgled'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarLBtnBrisiIzgled'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarLargeButton2: TdxBarLargeButton
      Action = aAzuriraj
      Category = 0
    end
    object dxBarLargeButton3: TdxBarLargeButton
      Action = aBrisi
      Category = 0
    end
    object dxBarLargeButton4: TdxBarLargeButton
      Action = aSnimiIzgled
      Category = 0
      ScreenTip = tipSnimiIzgled
    end
    object dxBarLargeButton5: TdxBarLargeButton
      Action = aZacuvajExcel
      Category = 0
      ScreenTip = tipZacuvajExcel
    end
    object dxBarLargeButton6: TdxBarLargeButton
      Action = aPecatiTabela
      Caption = #1055#1077#1095#1072#1090#1080' '#1090#1072#1073#1077#1083#1072
      Category = 0
    end
    object dxBarLargeButton7: TdxBarLargeButton
      Action = aHelp
      Category = 0
    end
    object dxBarLargeButton8: TdxBarLargeButton
      Action = aIzlez
      Category = 0
    end
    object dxBarLargeButton1: TdxBarLargeButton
      Action = aBrisiPorRN
      Category = 0
      Hint = #1050#1088#1077#1080#1088#1072#1112' '#1056#1072#1073#1086#1090#1077#1085' '#1085#1072#1083#1086#1075' '#1086#1076' '#1080#1079#1073#1088#1072#1085#1080#1090#1077' '#1055#1086#1088#1072#1095#1082#1080
    end
    object dxBarLargeButton9: TdxBarLargeButton
      Action = aSnimiPecatenje
      Category = 0
      ScreenTip = tipSnimiKonf
    end
    object dxBarLargeButton11: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object ribbonFilterReset: TdxBarLargeButton
      Caption = #1056#1077#1089#1077#1090#1080#1088#1072#1112' '#1092#1080#1083#1090#1077#1088
      Category = 0
      Hint = #1056#1077#1089#1077#1090#1080#1088#1072#1112' '#1092#1080#1083#1090#1077#1088
      Visible = ivAlways
    end
    object dxBarLargeButton12: TdxBarLargeButton
      Action = aPodesuvanjePecatenje
      Category = 0
      ScreenTip = tipDizajnReport
    end
    object dxBarLargeButton13: TdxBarLargeButton
      Action = aBrisiPodesuvanjePecatenje
      Category = 0
      ScreenTip = tipBrisiKonf
    end
    object dxBarLargeButton14: TdxBarLargeButton
      Caption = #1048#1079#1073#1088#1080#1096#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      Category = 0
      Visible = ivAlways
      LargeImageIndex = 40
    end
    object dxBarLargeButton15: TdxBarLargeButton
      Action = aPageSetup
      Category = 0
      ScreenTip = tipSetiranjeStrana
    end
    object dxBarLargeButton16: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object dxBarLargeButton17: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object dxBarSubItem1: TdxBarSubItem
      Caption = #1055#1077#1095#1072#1090#1077#1114#1077
      Category = 0
      Visible = ivAlways
      ImageIndex = 30
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton6'
        end>
    end
    object dxBarLBtnSnimiIzgled: TdxBarLargeButton
      Action = aSnimiIzgled
      Category = 0
    end
    object dxBarLBtnBrisiIzgled: TdxBarLargeButton
      Action = aBrisiIzgled
      Category = 0
    end
    object dxBarLargeButton10: TdxBarLargeButton
      Action = aSpustiSoberi
      Category = 0
    end
    object cbSite: TcxBarEditItem
      Action = aSiteStavki
      Caption = ' '#1057#1080#1090#1077' '#1055#1086#1088#1072#1095#1082#1080'  '
      Category = 0
      ShowCaption = True
      Width = 0
      PropertiesClassName = 'TcxCheckBoxProperties'
      Properties.ImmediatePost = True
      Properties.NullStyle = nssUnchecked
      Properties.OnChange = cbSitePropertiesChange
    end
    object dxBarLargeButton18: TdxBarLargeButton
      Action = aDodadiVoRN
      Category = 0
      Visible = ivNever
    end
  end
  object ActionList1: TActionList
    Images = dmRes.cxSmallImages
    Left = 184
    Top = 320
    object aNov: TAction
      Caption = #1053#1086#1074
      ImageIndex = 10
      OnExecute = aNovExecute
    end
    object aAzuriraj: TAction
      Caption = #1040#1078#1091#1088#1080#1088#1072#1112
      ImageIndex = 12
      ShortCut = 117
      OnExecute = aAzurirajExecute
    end
    object aBrisi: TAction
      Caption = #1041#1088#1080#1096#1080
      ImageIndex = 11
      ShortCut = 119
      OnExecute = aBrisiExecute
    end
    object aSnimiIzgled: TAction
      Caption = #1057#1085#1080#1084#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      ImageIndex = 9
      ShortCut = 24659
      OnExecute = aSnimiIzgledExecute
    end
    object aZacuvajExcel: TAction
      Caption = #1047#1072#1095#1091#1074#1072#1112' '#1074#1086' Excel'
      ImageIndex = 9
      ShortCut = 24645
      OnExecute = aZacuvajExcelExecute
    end
    object aPecatiTabela: TAction
      Caption = #1055#1077#1095#1072#1090#1080
      ImageIndex = 30
      OnExecute = aPecatiTabelaExecute
    end
    object aHelp: TAction
      Caption = #1055#1086#1084#1086#1096
      ImageIndex = 24
    end
    object aIzlez: TAction
      Caption = #1048#1079#1083#1077#1079
      ImageIndex = 25
      OnExecute = aIzlezExecute
    end
    object aZapisi: TAction
      Caption = #1047#1072#1087#1080#1096#1080
      ImageIndex = 7
      ShortCut = 120
      OnExecute = aZapisiExecute
    end
    object aOtkazi: TAction
      Caption = #1054#1090#1082#1072#1078#1080
      ImageIndex = 21
      ShortCut = 27
      OnExecute = aOtkaziExecute
    end
    object aRefresh: TAction
      Caption = 'aRefresh'
      ImageIndex = 18
      ShortCut = 118
      OnExecute = aRefreshExecute
    end
    object aSnimiPecatenje: TAction
      Caption = #1057#1085#1080#1084#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      ImageIndex = 39
      OnExecute = aSnimiPecatenjeExecute
    end
    object aPodesuvanjePecatenje: TAction
      Caption = #1044#1080#1079#1072#1112#1085' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090
      ImageIndex = 38
      OnExecute = aPodesuvanjePecatenjeExecute
    end
    object aBrisiPodesuvanjePecatenje: TAction
      Caption = #1041#1088#1080#1096#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      ImageIndex = 37
      OnExecute = aBrisiPodesuvanjePecatenjeExecute
    end
    object aPageSetup: TAction
      Caption = #1057#1077#1090#1080#1088#1072#1114#1077' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072
      ImageIndex = 38
      OnExecute = aPageSetupExecute
    end
    object aFormConfig: TAction
      Caption = 'aFormConfig'
      ShortCut = 24699
      OnExecute = aFormConfigExecute
    end
    object aBrisiIzgled: TAction
      Caption = #1041#1088#1080#1096#1080' '#1075#1086' '#1079#1072#1095#1091#1074#1072#1085#1080#1086#1090' '#1080#1079#1075#1083#1077#1076
      ImageIndex = 56
      OnExecute = aBrisiIzgledExecute
    end
    object aSpustiSoberi: TAction
      Caption = #1057#1087#1091#1096#1090#1080'/'#1057#1086#1073#1077#1088#1080' '#1058#1072#1073#1077#1083#1072
      ImageIndex = 65
      OnExecute = aSpustiSoberiExecute
    end
    object aBrisiPorRN: TAction
      Caption = #1041#1088#1080#1096#1080' '#1055#1086#1088#1072#1095#1082#1080' '#1086#1076' '#1056#1053
      ImageIndex = 17
      ShortCut = 119
      OnExecute = aBrisiPorRNExecute
    end
    object aSiteStavki: TAction
      Caption = 'aSiteStavki'
      OnExecute = aSteStavkiExecute
    end
    object aDodadiVoRN: TAction
      Caption = #1044#1086#1076#1072#1076#1080' '#1074#1086' '#1087#1086#1089#1090#1086#1077#1095#1082#1080' '#1056#1053
      ImageIndex = 17
      OnExecute = aDodadiVoRNExecute
    end
  end
  object dxComponentPrinter1: TdxComponentPrinter
    CurrentLink = dxComponentPrinter1Link1
    Version = 0
    Left = 184
    Top = 544
    object dxComponentPrinter1Link1: TdxGridReportLink
      Component = cxGrid1
      DateFormat = 0
      PageNumberFormat = pnfNumeral
      PrinterPage.DMPaper = 9
      PrinterPage.Footer = 6350
      PrinterPage.GrayShading = True
      PrinterPage.Header = 6350
      PrinterPage.Margins.Bottom = 12700
      PrinterPage.Margins.Left = 12700
      PrinterPage.Margins.Right = 12700
      PrinterPage.Margins.Top = 12700
      PrinterPage.Orientation = poLandscape
      PrinterPage.PageFooter.RightTitle.Strings = (
        #1057#1090#1088'.[Page #] '#1086#1076' [Total Pages]')
      PrinterPage.PageSize.X = 210000
      PrinterPage.PageSize.Y = 297000
      PrinterPage._dxMeasurementUnits_ = 0
      PrinterPage._dxLastMU_ = 2
      ReportDocument.CreationDate = 40336.425359629630000000
      TimeFormat = 0
      OptionsFormatting.UseNativeStyles = True
      OptionsOnEveryPage.Caption = False
      OptionsOnEveryPage.FilterBar = False
      OptionsView.Caption = False
      OptionsView.FilterBar = False
      StyleRepository = dmRes.cxStyleRepositoryPrinting
      Styles.StyleSheet = dmRes.dxGridReportLinkStyleSheet
      BuiltInReportLink = True
    end
  end
  object dxBarScreenTipRepository1: TdxBarScreenTipRepository
    Left = 344
    Top = 416
    PixelsPerInch = 96
    object tipSnimiIzgled: TdxBarScreenTip
      Header.Text = #1057#1085#1080#1084#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      Description.Text = #1047#1072#1095#1091#1074#1072#1112' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090' '#1085#1072' '#1092#1086#1088#1084#1072#1090#1072' '#1080' '#1085#1072' '#1090#1072#1073#1077#1083#1072#1090#1072
    end
    object tipZacuvajExcel: TdxBarScreenTip
      Header.Text = #1047#1072#1095#1091#1074#1072#1112' '#1074#1086' Excel'
      Description.Text = #1057#1085#1080#1084#1080' '#1075#1080' '#1087#1086#1076#1072#1090#1086#1094#1080#1090#1077' '#1086#1076' '#1090#1072#1073#1077#1083#1072#1090#1072' '#1074#1086' Excel '#1092#1086#1088#1084#1072#1090
    end
    object tipDizajnReport: TdxBarScreenTip
      Header.Text = #1044#1080#1079#1072#1112#1085' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090
      Description.Text = #1055#1086#1076#1077#1089#1091#1074#1072#1114#1072' '#1085#1072' '#1080#1079#1075#1083#1077#1076#1086#1090' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090' '#1085#1072' '#1090#1072#1073#1077#1083#1072#1090#1072
    end
    object tipSetiranjeStrana: TdxBarScreenTip
      Header.Text = #1057#1077#1090#1080#1088#1072#1114#1077' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072
      Description.Text = #1055#1086#1076#1077#1089#1091#1074#1072#1114#1077' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072' '#1085#1072' '#1082#1086#1112#1072' '#1116#1077' '#1089#1077' '#1087#1077#1095#1072#1090#1080' '#1090#1072#1073#1077#1083#1072#1090#1072
    end
    object tipSnimiKonf: TdxBarScreenTip
      Header.Text = #1057#1085#1080#1084#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      Description.Text = #1047#1072#1095#1091#1074#1072#1112' '#1075#1080' '#1087#1086#1076#1077#1089#1091#1074#1072#1114#1072#1090#1072' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090' '#1080' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072' '#1079#1072' '#1087#1077#1095#1072#1090#1077#1114#1077
    end
    object tipBrisiKonf: TdxBarScreenTip
      Header.Text = #1041#1088#1080#1096#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      Description.Text = #1048#1079#1073#1088#1080#1096#1080' '#1075#1080' '#1087#1086#1076#1077#1089#1091#1074#1072#1114#1072#1090#1072' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090' '#1080' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072' '#1079#1072' '#1087#1077#1095#1072#1090#1077#1114#1077
    end
  end
end
