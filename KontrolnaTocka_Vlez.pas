unit KontrolnaTocka_Vlez;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData, cxEdit,
  DB, cxDBData, cxGridCustomPopupMenu, cxGridPopupMenu,
  ComCtrls, cxContainer, cxTextEdit, cxDBEdit, StdCtrls, cxGridLevel,
  cxClasses, cxControls, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGrid, ExtCtrls, Menus, ActnList,
  cxGridExportLink, cxExport, cxLookAndFeelPainters, cxDataStorage,
  cxButtons, dxStatusBar, dxRibbonStatusBar, dxRibbon, dxBar, dxBarExtItems,
  dxPSGlbl, dxPSUtl, dxPSEngn, dxPrnPg, dxBkgnd, dxWrap, dxPrnDev,
  dxPSCompsProvider, dxPSFillPatterns, dxPSEdgePatterns, dxPSCore, dxPScxCommon,
  cxPC, cxMemo, cxLocalization, cxLookAndFeels,
  dxPSPDFExportCore, dxPSPDFExport, cxDrawTextUtils, dxPSPrVwStd, dxPSPrVwAdv,
  dxPSPrVwRibbon, dxPScxEditorProducers, dxPScxExtEditorProducers,
  dxPScxPageControlProducer, dxPgsDlg, cxCheckBox, cxBarEditItem, dxSkinsCore,
  dxSkinBlack, dxSkinBlue, dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom,
  dxSkinDarkSide, dxSkinFoggy, dxSkinGlassOceans, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven,
  dxSkinSharp, dxSkinSilver, dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008,
  dxSkinsDefaultPainters, dxSkinValentine, dxSkinXmas2008Blue,
  dxSkinsdxRibbonPainter, dxSkinsdxBarPainter, dxSkinscxPCPainter,
  dxBarSkinnedCustForm, cxDropDownEdit, cxCalendar, cxDBLookupComboBox,
  cxMaskEdit, cxLookupEdit, cxDBLookupEdit, cxDBExtLookupComboBox, cxGroupBox,
  cxRadioGroup, dxRibbonSkins, dxPScxGridLnk, dxPScxGridLayoutViewLnk,
  dxScreenTip, dxSkinBlueprint, dxSkinDevExpressDarkStyle,
  dxSkinDevExpressStyle, dxSkinHighContrast, dxSkinSevenClassic,
  dxSkinSharpPlus, dxSkinTheAsphaltWorld, dxSkinVS2010, dxSkinWhiteprint, dmResources,
  cxLabel, DateUtils, dxSkinOffice2013White, cxNavigator, System.Actions,
  dxSkinMetropolis, dxSkinMetropolisDark, dxSkinOffice2013DarkGray,
  dxSkinOffice2013LightGray, dxRibbonCustomizationForm;

type
//  niza = Array[1..5] of Variant;

  TfrmKontrolnaTockaVlez = class(TForm)
    cxGridPopupMenu1: TcxGridPopupMenu;
    PopupMenu1: TPopupMenu;
    N1: TMenuItem;
    Excel1: TMenuItem;
    dxRibbon1Tab1: TdxRibbonTab;
    dxRibbon1: TdxRibbon;
    StatusBar1: TdxRibbonStatusBar;
    dxBarManager1: TdxBarManager;
    dxBarManager1Bar1: TdxBar;
    dxBarManager1Bar2: TdxBar;
    dxBarManager1Bar3: TdxBar;
    dxBarLargeButton2: TdxBarLargeButton;
    dxBarLargeButton3: TdxBarLargeButton;
    dxBarLargeButton4: TdxBarLargeButton;
    dxBarLargeButton5: TdxBarLargeButton;
    dxBarLargeButton6: TdxBarLargeButton;
    dxBarLargeButton7: TdxBarLargeButton;
    dxBarLargeButton8: TdxBarLargeButton;
    dxBarLargeButton1: TdxBarLargeButton;
    ActionList1: TActionList;
    aNov: TAction;
    aAzuriraj: TAction;
    aBrisi: TAction;
    aSnimiIzgled: TAction;
    aZacuvajExcel: TAction;
    aPecatiTabela: TAction;
    aHelp: TAction;
    aIzlez: TAction;
    aZapisi: TAction;
    aOtkazi: TAction;
    aRefresh: TAction;
    dxRibbon1Tab2: TdxRibbonTab;
    dxBarManager1Bar4: TdxBar;
    dxBarLargeButton9: TdxBarLargeButton;
    aSnimiPecatenje: TAction;
    dxComponentPrinter1: TdxComponentPrinter;
    dxComponentPrinter1Link1: TdxGridReportLink;
    dxBarLargeButton11: TdxBarLargeButton;
    ribbonFilterReset: TdxBarLargeButton;
    aPodesuvanjePecatenje: TAction;
    dxBarLargeButton12: TdxBarLargeButton;
    aBrisiPodesuvanjePecatenje: TAction;
    dxBarLargeButton13: TdxBarLargeButton;
    dxBarLargeButton14: TdxBarLargeButton;
    aPageSetup: TAction;
    dxBarLargeButton15: TdxBarLargeButton;
    dxBarLargeButton16: TdxBarLargeButton;
    dxBarLargeButton17: TdxBarLargeButton;
    cxBarEditItem1: TcxBarEditItem;
    dxBarSubItem1: TdxBarSubItem;
    aFormConfig: TAction;
    dxBarScreenTipRepository1: TdxBarScreenTipRepository;
    tipSnimiIzgled: TdxBarScreenTip;
    tipZacuvajExcel: TdxBarScreenTip;
    tipDizajnReport: TdxBarScreenTip;
    tipSetiranjeStrana: TdxBarScreenTip;
    tipSnimiKonf: TdxBarScreenTip;
    tipBrisiKonf: TdxBarScreenTip;
    dxBarManager1BarIzgledGrid: TdxBar;
    dxBarLBtnSnimiIzgled: TdxBarLargeButton;
    dxBarLBtnBrisiIzgled: TdxBarLargeButton;
    aBrisiIzgled: TAction;
    aSpustiSoberi: TAction;
    dxBarLargeButton10: TdxBarLargeButton;
    lPanel: TPanel;
    dPanel: TPanel;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    Label7: TLabel;
    txtRE: TcxDBTextEdit;
    Label2: TLabel;
    txtPrimeni: TcxDBTextEdit;
    Label3: TLabel;
    ZapisiButton: TcxButton;
    OtkaziButton: TcxButton;
    txtDatum: TcxDBDateEdit;
    cxLabel3: TcxLabel;
    txtVidArtikal: TcxDBTextEdit;
    txtArtikal: TcxDBTextEdit;
    cbArtikal: TcxLookupComboBox;
    cbRE: TcxDBLookupComboBox;
    Label4: TLabel;
    txtKT: TcxDBTextEdit;
    cbKT: TcxDBLookupComboBox;
    dxBarManager1Bar5: TdxBar;
    cbGodina: TcxBarEditItem;
    cxGroupBox1: TcxGroupBox;
    Label5: TLabel;
    txtRN: TcxDBTextEdit;
    cxLabel27: TcxLabel;
    txtOperacija: TcxDBTextEdit;
    cbOperacija: TcxDBLookupComboBox;
    cxGrid1DBTableView1ID: TcxGridDBColumn;
    cxGrid1DBTableView1ID_KT: TcxGridDBColumn;
    cxGrid1DBTableView1ID_EKT: TcxGridDBColumn;
    cxGrid1DBTableView1DATUM: TcxGridDBColumn;
    cxGrid1DBTableView1NAZIV_KT: TcxGridDBColumn;
    cxGrid1DBTableView1ID_RE: TcxGridDBColumn;
    cxGrid1DBTableView1NAZIV_RE: TcxGridDBColumn;
    cxGrid1DBTableView1VID_ARTIKAL: TcxGridDBColumn;
    cxGrid1DBTableView1ARTIKAL: TcxGridDBColumn;
    cxGrid1DBTableView1NAZIV_ARTIKAL: TcxGridDBColumn;
    cxGrid1DBTableView1ID_RABOTEN_NALOG: TcxGridDBColumn;
    cxGrid1DBTableView1NAZIV_PARTNER_PORACKA: TcxGridDBColumn;
    cxGrid1DBTableView1GODINA: TcxGridDBColumn;
    Label1: TLabel;
    txtZavrseni: TcxDBTextEdit;
    rgEvidencijaVlez: TcxRadioGroup;
    cxLabel1: TcxLabel;
    txtVidProizvod: TcxDBTextEdit;
    txtProizvod: TcxDBTextEdit;
    cbRN: TcxDBLookupComboBox;
    cbProizvod: TcxDBTextEdit;
    cxGrid1DBTableView1PRIMENI: TcxGridDBColumn;
    Label6: TLabel;
    txtEKT: TcxDBTextEdit;
    cbEKT: TcxDBLookupComboBox;
    cxGrid1DBTableView1NAZIV_EKT: TcxGridDBColumn;
    cxGrid1DBTableView1BROJRN: TcxGridDBColumn;

    procedure cxDBTextEditAllEnter(Sender: TObject);
    procedure cxDBTextEditAllExit(Sender: TObject);
    procedure EnterKakoTab(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    //procedure PrvPosledenTab(panel:TPanel;var posledna,prva:TWinControl);
    procedure SaveToIniFileExecute(Sender: TObject);
    procedure cxGrid1DBTableView1KeyPress(Sender: TObject; var Key: Char);
    procedure aZapisiExecute(Sender: TObject);
    procedure aOtkaziExecute(Sender: TObject);
    procedure aNovExecute(Sender: TObject);
    procedure aAzurirajExecute(Sender: TObject);
    procedure aBrisiExecute(Sender: TObject);
    procedure aZacuvajExcelExecute(Sender: TObject);
    procedure aRefreshExecute(Sender: TObject);
    procedure aSnimiIzgledExecute(Sender: TObject);
    procedure aIzlezExecute(Sender: TObject);
    procedure aPecatiTabelaExecute(Sender: TObject);
    procedure aSnimiPecatenjeExecute(Sender: TObject);
    procedure zacuvajPrintVoIni(ime:AnsiString);
    procedure procitajPrintOdIni(ime:AnsiString);
    procedure aPodesuvanjePecatenjeExecute(Sender: TObject);
    procedure aBrisiPodesuvanjePecatenjeExecute(Sender: TObject);
    procedure aPageSetupExecute(Sender: TObject);
    procedure aFormConfigExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure PartnerPropertiesChange(Sender: TObject);
    procedure aBrisiIzgledExecute(Sender: TObject);
    procedure aSpustiSoberiExecute(Sender: TObject);
    procedure cbVremenskaMEPropertiesChange(Sender: TObject);
    procedure cbGodinaChange(Sender: TObject);
    procedure dxRibbon1TabChanging(Sender: TdxCustomRibbon;
      ANewTab: TdxRibbonTab; var Allow: Boolean);
    procedure cxGrid1DBTableView1FocusedRecordChanged(
      Sender: TcxCustomGridTableView; APrevFocusedRecord,
      AFocusedRecord: TcxCustomGridRecord;
      ANewItemRecordFocusingChanged: Boolean);
    procedure cxGrid1DBTableView1TcxGridDBDataControllerTcxDataSummaryFooterSummaryItems0GetText(
      Sender: TcxDataSummaryItem; const AValue: Variant; AIsFooter: Boolean;
      var AText: string);
    procedure rgEvidencijaVlezPropertiesChange(Sender: TObject);
    procedure SetirajLukap(sender:TObject; tabela:TDataSet; tip:TcxDBTextEdit; sifra:TcxDBTextEdit; lukap:TcxLookupComboBox);
  private
    { Private declarations }

    _sifra_kluc :array[1..10] of string;
    procedure EvidencijaKT;
    procedure OtvoriSE;
    procedure KTiliRN;  // �������� ���������� �� ������ �� ������� �� ������������� ���

  protected
    list : TList;
    inserting, sobrano : boolean;
    prva, posledna :TWinControl;
    procedure prefrli;

  public
    { Public declarations }
    constructor Create(Owner : TComponent; insert : boolean = true);reintroduce; overload;

    procedure SetSifra(br: integer; s : string);
    function GetSifra(br: integer) : string;

    //������� �� ���������� ������������ _sifra_kluc
    property sifra_kluc[br :integer]: string read GetSifra write SetSifra;
  end;

var
  frmKontrolnaTockaVlez: TfrmKontrolnaTockaVlez;
  rData : TRepositoryData;

implementation

uses DaNe, dmKonekcija, Utils, FormConfig, NurkoRepository, dmMaticni, dmSystem,
  dmUnit;

{$R *.dfm}
//------------------------------------------------------------------------------

constructor TfrmKontrolnaTockaVlez.Create(Owner : TComponent; insert : boolean = true);
begin
inherited Create(Owner);
//	�������� ���� ������� � �������� �� ����������� ���������
//	insert = true -> ����� ���� ����� ���������� �� ������� �� Insert Mode
//  insert = false -> ����� ���� ����� ������� �� ����� �� ������. �� ������� ����� ����� �� �� �������� F5
  inserting := insert;
end;

//	����� �� �����������. �� ��������� ������� �� ������, �� �������� ������� �� ���� ��������,
//	�� ������� ������� �� ������ ������ � �� ������� �� Insert Mode
procedure TfrmKontrolnaTockaVlez.aNovExecute(Sender: TObject);
begin
  if(cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) then
  begin
    dPanel.Enabled:=True;
    lPanel.Enabled:=False;
    dm.tblRNVlez.Close;
    prva.SetFocus;
    cxGrid1DBTableView1.DataController.DataSet.Insert;
    cbArtikal.Clear;
  end
  else ShowMessage('������ �������� �� ��� �������� �� �� ��������� �������/r/n � ����� ������� �� ������� ��� �����!');
end;

//	����� �� ���������. �� ��������� ������� �� ������, �� �������� ������� �� ���� ��������,
//	�� ������� ������� �� ������ ������ � �� ������� �� Edit Mode
procedure TfrmKontrolnaTockaVlez.aAzurirajExecute(Sender: TObject);
begin
  if(cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) then
  begin
    dPanel.Enabled:=True;
    lPanel.Enabled:=False;
    prva.SetFocus;
    cxGrid1DBTableView1.DataController.DataSet.Edit;
  end
  else ShowMessage('������ �������� �� ��� �������� �� �� ��������� �������/r/n � ����� ������� �� ������� ��� �����!');
end;

//	����� �� ������ �� ������������� �����
procedure TfrmKontrolnaTockaVlez.aBrisiExecute(Sender: TObject);
begin
  if ((cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) and
     (cxGrid1DBTableView1.DataController.RecordCount <> 0)) then
    cxGrid1DBTableView1.DataController.DataSet.Delete();
end;

procedure TfrmKontrolnaTockaVlez.aBrisiIzgledExecute(Sender: TObject);
begin
  brisiGridVoBaza(Name,cxGrid1DBTableView1);
//  brisiPivotVoBaza(Name,cxDBPivotGrid1); �� pivot ��� ���
  BrisiFormaIzgled(self);
end;

//	����� �� ���������� �� ����������
procedure TfrmKontrolnaTockaVlez.aRefreshExecute(Sender: TObject);
begin
  cxGrid1DBTableView1.DataController.Filter.Root.Clear; //���������� �� ��������
  cxGrid1DBTableView1.DataController.DataSet.Refresh;
end;

//	����� �� ����� �� ������� �� ������� �� ������� �����
procedure TfrmKontrolnaTockaVlez.aIzlezExecute(Sender: TObject);
begin
  Close;
end;

//	����� �� ������� �� �������� �� ������ �� ���� (Utils.pas)
procedure TfrmKontrolnaTockaVlez.aSnimiIzgledExecute(Sender: TObject);
begin
  zacuvajGridVoBaza(Name,cxGrid1DBTableView1);
  ZacuvajFormaIzgled(self);
end;

//	����� �� ������� �� ������ �� Excel ������ (Utils.pas)
procedure TfrmKontrolnaTockaVlez.aZacuvajExcelExecute(Sender: TObject);
begin
  zacuvajVoExcel(cxGrid1, Caption);
end;

//  ��������� �� ������ �� ������� �� ���������� �� ����� �� Enter
procedure TfrmKontrolnaTockaVlez.EnterKakoTab(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  kom : TWinControl;
begin
    kom := Sender as TWinControl;
    case Key of
        VK_DOWN:
        begin
          PostMessage(Handle,WM_NextDlgCtl,0,0);
          key:=0;
        end;
        VK_UP:
        begin
          PostMessage(Handle,WM_NextDlgCtl,1,0);
          key:=0;
        end;
        VK_RETURN:
        begin
          PostMessage(Handle,WM_NextDlgCtl,0,0);
        end;
        VK_INSERT:
        begin
          //  �� ����� ��������
//          if (kom = cxExtLookupComboBox1)  then
//          begin
//            frmNurkoRepository := TfrmNurkoRepository.Create(nil,Name,rData);
//            frmNurkoRepository.kontrola_naziv := kom.Name;
//            frmNurkoRepository.ShowModal;
//
//            if (frmNurkoRepository.ModalResult = mrOk) then
//              cxExtLookupComboBox1.EditValue := frmNurkoRepository.cxGrid1Level1.GridView.DataController.GetRecordId(frmNurkoRepository.cxGrid1Level1.GridView.DataController.FocusedRecordIndex);
//
//            frmNurkoRepository.Free;
//       	 end;
	end;
    end;
end;

//	������� �� ��� �� ������ ��� �������� �� �����
procedure TfrmKontrolnaTockaVlez.cxDBTextEditAllEnter(Sender: TObject);
begin
//       if (TcxDBLookupComboBox(Sender) = cbOperacija) then
//       begin
//            dm.tblKTOperacija.Close;
//         if cbkt.Text <> '' then
//            dm.tblKTOperacija.ParamByName('id_kt').AsString := txtKT.Text;
//            dm.tblKTOperacija.Open;
//       end;

       if (TcxDBLookupComboBox(Sender) = cbKT) then
       begin
            dm.tblKT.Close;
         if cbRE.Text <> '' then
            dm.tblKT.ParamByName('id_re').AsString := txtRE.Text;
            dm.tblKT.Open;
        end;

//       if (TcxDBLookupComboBox(Sender) = cbRN) then
//       begin
//             KTiliRN;
//        end;
        if (TcxDBLookupComboBox(Sender) = cbEKT) then
       begin
             KTiliRN;
        end;

//        if (TcxDBLookupComboBox(sender) = cbRN) then
//          begin
//           dm.tblIzberiRN.Close;
//           dm.tblIzberiRN.ParamByName('status').AsString := '1';
//           dm.tblIzberiRN.ParamByName('godina').AsString := cbGodina.EditValue;
//          if txtRE.Text =inttostr(masinsko) then
//             dm.tblIzberiRN.ParamByName('id_re').Asinteger := masinsko
//          else
//             dm.tblIzberiRN.ParamByName('id_re').AsString := '%';
//           dm.tblIzberiRN.Open;
//       end;
//        if TcxDBLookupComboBox(Sender)= cbSKT then
//         begin
//           dm.tblKT.Close;
//           dm.tblKT.ParamByName('id_re').AsString := '%';
//           dm.tblKT.Open;
//         end;

      if (not dm.tblRNVlezID_RABOTEN_NALOG.IsNull) and (not dm.tblArtVoRN.Active) then
       begin
           dm.tblArtVoRN.Close;
           dm.tblArtVoRN.ParamByName('id').Value := dm.tblRNVlezID_RABOTEN_NALOG.Value;
           dm.tblArtVoRN.Open;
       end;
    TEdit(Sender).Color:=clSkyBlue;
end;

//	������� �� ��� �� ������ ��� ����� �� �����
procedure TfrmKontrolnaTockaVlez.cxDBTextEditAllExit(Sender: TObject);
begin

  if (dm.tblKTVlez.State) in [dsInsert, dsEdit] then
    begin
   if ((Sender as TWinControl)= cbArtikal) then
        begin
            if (cbArtikal.Text <>'') then
            begin
              txtVidArtikal.Text := dm.tblArtVoRNARTVID.AsString;
              txtArtikal.Text := dm.tblArtVoRNID.AsString;
            end
            else
            begin
              dm.tblKTVlezVID_ARTIKAL.Clear;
              dm.tblKTVlezARTIKAL.Clear;
            end;
         end;

         if ((Sender as TWinControl)= txtArtikal) or ((Sender as TWinControl)= txtVidArtikal) then
         begin
             SetirajLukap(sender,dm.tblArtVoRN,txtVidArtikal,txtArtikal, cbArtikal);
         end;
    end;
    TEdit(Sender).Color:=clWhite;
end;

procedure TfrmKontrolnaTockaVlez.cxGrid1DBTableView1FocusedRecordChanged(
  Sender: TcxCustomGridTableView; APrevFocusedRecord,
  AFocusedRecord: TcxCustomGridRecord; ANewItemRecordFocusingChanged: Boolean);
begin
  if dm.tblKTVlez.State = dsBrowse then
  begin
    cbArtikal.Text := dm.tblKTVlezNAZIV_ARTIKAL.Value;
  //  KTiliRN;

 if not dm.tblKTVlezID_EKT.IsNull then
 begin
    rgEvidencijaVlez.ItemIndex := 0;
    Label5.Visible := false;
    txtrn.Visible := false;
    cbRN.Visible := false;
    Label6.Visible := true;
    txtEKT.Visible := true;
    cbEKT.Visible := true;
    dm.tblRNVlez.Close;
    dm.tblRNVlez.ParamByName('id_kt').AsString := txtKT.Text;
    dm.tblRNVlez.Open;
  end
  else
  begin
    rgEvidencijaVlez.ItemIndex := 1;
    Label5.Visible := true;
    txtrn.Visible := true;
    cbRN.Visible := true;
    Label6.Visible :=false;
    txtEKT.Visible := False;
    cbEKT.Visible := False;
    dm.tblRNVlez.Close;
    dm.tblRNVlez.ParamByName('id_kt').AsString := '%';
    dm.tblRNVlez.Open;
  end;
  end;

end;

//  ��������� �� ������ �� ��������� �� ���������� ���� �� ��������
//  ��������� ��: br - ����� ��� �� ������ (�� 0 �� 9) �� ��������� �� ����� �� ����� �� ���������� ����
//  s - ��������� �� ������ �� ������ (���������� �� string �� ����� �� integer �� IntToStr ���������)
procedure TfrmKontrolnaTockaVlez.SetSifra(br: integer; s : string);
begin
  _sifra_kluc[br] := s;
end;

//  ������� �� ������ �� ��������� �� ���������� ���� �� ��������
//  ��������� ��: br - ����� ��� �� ������ (�� 0 �� 9) �� ��������� �� ����� �� ����� �� ���������� ����
function TfrmKontrolnaTockaVlez.GetSifra(br: integer) : string;
begin
  Result := _sifra_kluc[br];
end;

procedure TfrmKontrolnaTockaVlez.KTiliRN;
begin
  // txtKolicina.Text := dm.tblIzberiRNKOLICINA.AsString;
  //    if (TcxDBLookupComboBox(Sender) = cbKT) then
  //       begin
  //            dm.tblKTOperacija.Close;
  //         if cbkt.Text <> '' then
  //            dm.tblKTOperacija.ParamByName('id_kt').AsString := txtKT.Text;
  //            dm.tblKTOperacija.Open;
  //       end;

  if rgEvidencijaVlez.ItemIndex = 0 then
  begin
    dm.tblRNVlez.Close;
    dm.tblRNVlez.ParamByName('id_kt').AsString := txtKT.Text;
    dm.tblRNVlez.Open;
  end
  else
  begin
    dm.tblRNVlez.Close;
    dm.tblRNVlez.ParamByName('id_kt').AsString := '%';
    dm.tblRNVlez.Open;
  end;
end;

procedure TfrmKontrolnaTockaVlez.OtvoriSE;
begin
  dm.tblKTOperacija.Close;
  dm.tblKTOperacija.ParamByName('id_kt').AsString := '%';
  dm.tblKTOperacija.Open;
  dm.tblKT.Close;
  dm.tblKT.ParamByName('id_re').AsString := '%';
  dm.tblKT.Open;
if (dm.tblKTVlez.RecordCount > 0) then
   begin
      dm.tblRNVlez.Close;
      dm.tblRNVlez.ParamByName('id_kt').AsString := '%';
      dm.tblRNVlez.Open;
   end;
  dm.tblArtVoRN.Close;
  dm.tblArtVoRN.ParamByName('id').AsString := '%';
  dm.tblArtVoRN.Open;
end;

procedure TfrmKontrolnaTockaVlez.EvidencijaKT;
begin
  dm.tblKTVlez.Close;
  dm.tblKTVlez.ParamByName('godina').AsVariant := cbGodina.EditValue;
  dm.tblKTVlez.Open;
end;

procedure TfrmKontrolnaTockaVlez.PartnerPropertiesChange(Sender: TObject);
begin

end;

//  �� ������� �� ������� ��� ����������� �� Master-�� �� �������� �� ��������� �� ������ ����� SetSifra
procedure TfrmKontrolnaTockaVlez.prefrli;
begin
end;

procedure TfrmKontrolnaTockaVlez.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    //	��������� ��� ��������� �� �������
    if (cxGrid1DBTableView1.DataController.DataSource.State = dsEdit) or (cxGrid1DBTableView1.DataController.DataSource.State = dsInsert) then
    begin
        frmDaNe := TfrmDaNe.Create(self, '���������� ��������', '���������� �� �� ��������. ���� ������ �� �� �������?', 1);
        if (frmDaNe.ShowModal <> mrYes) then
        begin
            cxGrid1DBTableView1.DataController.DataSet.Cancel;
            dm.tblKontrolnaTocka.Close;
            Action := caFree;
        end
        else
          if (Validacija(dPanel) = false) then
          begin
            cxGrid1DBTableView1.DataController.DataSet.Post;
            dm.tblKontrolnaTocka.Close;
            Action := caFree;
          end
          else Action := caNone;
    end;
end;
procedure TfrmKontrolnaTockaVlez.FormCreate(Sender: TObject);
begin
  ProcitajFormaIzgled(self);
  if (dmRes.use_skin = true) then dxRibbon1.ColorSchemeName := dmRes.skin_name;
  cbGodina.EditValue := YearOf(Now);
  rData := TRepositoryData.Create();  // kreirame instance od klasata TRepositoryData sto se naogja vo dmRes
  //dm.tblKontrolnaTocka.Open;
//  dm.tblIzberiRN.Close;
//  dm.tblIzberiRN.ParamByName('status').AsString := '1';
//  dm.tblIzberiRN.ParamByName('godina').AsString := cbGodina.EditValue;
//  dm.tblIzberiRN.ParamByName('id_re').AsString := '%';
//  dm.tblIzberiRN.Open;
  EvidencijaKT;
//   dm.tblKTOperacija.Close;
//   dm.tblKTOperacija.ParamByName('id_kt').AsString := '%';
//   dm.tblKTOperacija.Open;
  OtvoriSE;
end;

//------------------------------------------------------------------------------

procedure TfrmKontrolnaTockaVlez.FormShow(Sender: TObject);
begin
  //  ������� �� ������������ ������������/���������� �� ���������� �� ���� �����
    SpremiForma(self);
  //	�� ��������� �� ������� ������ ��� ���������� � ���� � ��� �������� �� �������
    PrvPosledenTab(dPanel,posledna,prva);
    dxBarManager1Bar1.Caption := Caption;
    dxComponentPrinter1Link1.ReportTitle.Text := Caption;

  //	������� �� ������������ �� ������ �� ������
    procitajGridOdBaza(Name,cxGrid1DBTableView1,false,false);
  //	������� �� ������������ �� ��������� �� ������
    procitajPrintOdBaza(Name,cxGrid1DBTableView1.Name, dxComponentPrinter1Link1);
    sobrano := true;

 //     if dm.tblKTVlez.State = dsBrowse then
 // begin
    cbArtikal.Text := dm.tblKTVlezNAZIV_ARTIKAL.Value;

    rgEvidencijaVlez.ItemIndex := 0;
    KTiliRN;
 // end;
//  �������� �� ����� �� ������ �� cxExtLookupComboBox ��� cxDBExtLookupComboBox ��������
    //cxExtLookupComboBox1.RepositoryItem := dmRes.InitRepository(23, 'cxExtLookupComboBox1', Name, rData);
//  	procitajGridOdBaza(Name + '_' + IntToStr(rData.nurkoRepository[rData.br_repository]), rData.gridRepository[rData.br_repository] ,false,false);
end;
//------------------------------------------------------------------------------

procedure TfrmKontrolnaTockaVlez.SaveToIniFileExecute(Sender: TObject);
begin
    cxGrid1DBTableView1.StoreToIniFile(Name,True,[]);
end;
//------------------------------------------------------------------------------

procedure TfrmKontrolnaTockaVlez.cxGrid1DBTableView1KeyPress(Sender: TObject;
  var Key: Char);
begin
  if(Ord(Key) <> VK_RETURN) then cxSortiraj(cxGrid1DBTableView1);
end;

procedure TfrmKontrolnaTockaVlez.cxGrid1DBTableView1TcxGridDBDataControllerTcxDataSummaryFooterSummaryItems0GetText(
  Sender: TcxDataSummaryItem; const AValue: Variant; AIsFooter: Boolean;
  var AText: string);
begin
    AText := '������: '+vartostr(AValue)+' ��';
end;

procedure TfrmKontrolnaTockaVlez.dxRibbon1TabChanging(Sender: TdxCustomRibbon;
  ANewTab: TdxRibbonTab; var Allow: Boolean);
begin
   if ANewTab = dxRibbon1Tab1 then
   begin
     cbGodina.Visible := ivAlways;
   end
   else
   begin
     cbGodina.Visible := ivNever;
   end;

end;

//  ����� �� �����
procedure TfrmKontrolnaTockaVlez.aZapisiExecute(Sender: TObject);
var
  st: TDataSetState;
begin
//  ZapisiButton.SetFocus;
  dm.tblKTVlezGODINA.AsVariant := cbGodina.EditValue;
  st := cxGrid1DBTableView1.DataController.DataSet.State;
  if st in [dsEdit,dsInsert] then
  begin
    if (Validacija(dPanel) = false) then
    begin
    //if rgEvidencijaVlez.ItemIndex = 1 then
  //  begin
    //    dm.tblKTVlezID_RABOTEN_NALOG.Value := dm.tblRNVlezID_RABOTEN_NALOG.Value;
     //   dm.tblKTVlezID_EKT.Clear;
  //  end
  //  else
   // begin
        dm.tblKTVlezID_RABOTEN_NALOG.Value := dm.tblRNVlezID_RABOTEN_NALOG.Value;
        dm.tblKTVlezID_EKT.Value := dm.tblRNVlezID.Value;
   // end;
      if ((st = dsInsert) and inserting) then
      begin
      //  dm.tblKTVlez.AsVariant := cbGodina.EditValue;

        cxGrid1DBTableView1.DataController.DataSet.Post;



        aNov.Execute;
      end;

      if ((st = dsInsert) and (not inserting)) then
      begin
      //  dm.tblKTVlez.AsVariant := cbGodina.EditValue;
        cxGrid1DBTableView1.DataController.DataSet.Post;
        dPanel.Enabled:=false;
        lPanel.Enabled:=true;
        cxGrid1.SetFocus;
      end;

      if (st = dsEdit) then
      begin
        cxGrid1DBTableView1.DataController.DataSet.Post;
        dPanel.Enabled:=false;
        lPanel.Enabled:=true;
        cxGrid1.SetFocus;
      end;
    end;
  end;
end;

procedure TfrmKontrolnaTockaVlez.cbGodinaChange(Sender: TObject);
begin
  EvidencijaKT;
end;

procedure TfrmKontrolnaTockaVlez.cbVremenskaMEPropertiesChange(Sender: TObject);
begin

end;

//	����� �� ���������� �� �������
procedure TfrmKontrolnaTockaVlez.aOtkaziExecute(Sender: TObject);
begin
  if (cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) then
  begin
      ModalResult := mrCancel;
      Close();
  end
  else
  begin
      cxGrid1DBTableView1.DataController.DataSet.Cancel;
      RestoreControls(dPanel);
      dPanel.Enabled := false;
      lPanel.Enabled := true;
      cxGrid1.SetFocus;
  end;
end;

//----------------------------------------------------------------------------------
// ��������� �� ��������, ���������� �� ��������� � �������� �� Print ��������
//----------------------------------------------------------------------------------

//	����� �� �������� �� �������� �� �������
procedure TfrmKontrolnaTockaVlez.aPageSetupExecute(Sender: TObject);
begin
  dxComponentPrinter1Link1.PageSetup;
end;

//	����� �� ������� �� ������
procedure TfrmKontrolnaTockaVlez.aPecatiTabelaExecute(Sender: TObject);
begin
  dxComponentPrinter1Link1.ReportTitle.Text := Caption;

  dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Clear;
  dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Add(dmkon.firma_naziv);
  dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Add(DateTimeToStr(Now));

  dxComponentPrinter1Link1.PrinterPage.PageHeader.LeftTitle.Clear;
  //dxComponentPrinter1Link1.PrinterPage.PageHeader.LeftTitle.Add('������� : ' + TipSifra.Text + '-' + IdSifra.Text + ' ' + Partner.Text);

  dxComponentPrinter1.Preview(true, dxComponentPrinter1Link1);
end;

//	����� �� ���������� �� ��������� �� �������
procedure TfrmKontrolnaTockaVlez.aPodesuvanjePecatenjeExecute(Sender: TObject);
begin
  dxComponentPrinter1Link1.DesignReport();
end;

procedure TfrmKontrolnaTockaVlez.aSnimiPecatenjeExecute(Sender: TObject);
begin
  zacuvajPrintVoBaza(Name,cxGrid1DBTableView1.Name,dxComponentPrinter1Link1);
end;

//	����� �� �������� ��� �������� �� �������� �� ������ ���� ��� ��������� �� ����� ������
procedure TfrmKontrolnaTockaVlez.aSpustiSoberiExecute(Sender: TObject);
begin
  if (sobrano = true) then
  begin
    //cxGrid1DBTableView1.ViewData.Expand(false);
    sobrano := false;
  end
  else
  begin
    //cxGrid1DBTableView1.ViewData.Collapse(false);
    sobrano := true;
  end;
end;

//	����� �� ������ �� ����������� �������� �� ������� (Utils.pas)
procedure TfrmKontrolnaTockaVlez.aBrisiPodesuvanjePecatenjeExecute(Sender: TObject);
begin
  brisiPrintOdBaza(Name,cxGrid1DBTableView1.Name, dxComponentPrinter1Link1);
end;

//  ����� �� ����� �� ����� �� ������������� �� �������� � ��������������� ��
//  �������(������ ���������� ��������)
procedure TfrmKontrolnaTockaVlez.aFormConfigExecute(Sender: TObject);
begin
  frmFormConfig := TfrmFormConfig.Create(Application);
  frmFormConfig.formPtr := Addr(Self);
  frmFormConfig.ShowModal;
  frmFormConfig.Free;
end;

//----------------------------------------------------------------------------------
// ���� -> ��������� �� ��������, ���������� �� ��������� � �������� �� Print ��������
//----------------------------------------------------------------------------------

// ��������� �� ���������� �� ��������� � �������� �� Print �������� �� .ini ��������
procedure TfrmKontrolnaTockaVlez.zacuvajPrintVoIni(ime:AnsiString);
var
  AStream: TFileStream;
begin
  AStream := TFileStream.Create(dmKon.RabotenDir + ime + 'Print.ini', fmCreate);
  try
    AStream.WriteComponent(dxComponentPrinter1Link1);
  finally
    AStream.Free;
  end;

end;

// ��������� �� ������ �� ��������� � �������� �� Print �������� �������� �� .ini ��������
procedure TfrmKontrolnaTockaVlez.procitajPrintOdIni(ime:AnsiString);
var
  AStream: TFileStream;
begin
  AStream := TFileStream.Create(dmKon.RabotenDir + ime + 'Print.ini', fmOpenRead);
  try
    dxComponentPrinter1Link1.RestoreDefaults;
    AStream.ReadComponent(dxComponentPrinter1Link1);
  finally
    AStream.Free;
  end;
end;


procedure TfrmKontrolnaTockaVlez.rgEvidencijaVlezPropertiesChange(
  Sender: TObject);
begin
  //KTiliRN;
//if dm.tblKTVlez.State <> dsBrowse then
// begin
// if not dm.tblKTVlezID_EKT.IsNull then
 if rgEvidencijaVlez.ItemIndex = 0 then

 begin
    //rgEvidencijaVlez.ItemIndex := 0;
    Label5.Visible := false;
    txtrn.Visible := false;
    cbRN.Visible := false;
    Label6.Visible := true;
    txtEKT.Visible := true;
    cbEKT.Visible := true;
    KTiliRN;
  end
  else
  begin
    //rgEvidencijaVlez.ItemIndex := 1;
    Label5.Visible := true;
    txtrn.Visible := true;
    cbRN.Visible := true;
    Label6.Visible :=false;
    txtEKT.Visible := False;
    cbEKT.Visible := False;
    KTiliRN;
  end;
// end;
end;

procedure TfrmKontrolnaTockaVlez.SetirajLukap(sender:TObject; tabela:TDataSet; tip:TcxDBTextEdit; sifra:TcxDBTextEdit; lukap:TcxLookupComboBox);
begin
         if (tip.Text <>'') and (sifra.Text<>'')  then
         begin
          if(tabela.Locate('ARTVID;ID',VarArrayOf([tip.text,sifra.text]),[])) then
               lukap.Text:=tabela.FieldByName('NAZIV').Value;
         end
         else
         begin
            lukap.Clear;
         end;
end;

end.
