inherited frmNormativ: TfrmNormativ
  ActiveControl = nil
  Caption = #1053#1086#1088#1084#1072#1090#1080#1074
  ClientHeight = 741
  ClientWidth = 1024
  ExplicitWidth = 1040
  ExplicitHeight = 780
  PixelsPerInch = 96
  TextHeight = 13
  inherited lPanel: TPanel
    Width = 262
    Height = 369
    Align = alLeft
    ExplicitWidth = 262
    ExplicitHeight = 369
    inherited cxGrid1: TcxGrid
      Top = 23
      Width = 258
      Height = 344
      ExplicitTop = 23
      ExplicitWidth = 258
      ExplicitHeight = 344
      inherited cxGrid1DBTableView1: TcxGridDBTableView
        OnFocusedRecordChanged = cxGrid1DBTableView1FocusedRecordChanged
        DataController.DataSource = dm.dsNormativG
        DataController.Filter.OnChanged = cxGrid1DBTableView1DataControllerFilterChanged
        DataController.Filter.Active = True
        OptionsView.GroupByBox = False
        object cxGrid1DBTableView1NAZIV_ARIKAL: TcxGridDBColumn
          Caption = #1053#1072#1079#1080#1074
          DataBinding.FieldName = 'NAZIV_ARTIKAL'
          Width = 238
        end
        object cxGrid1DBTableView1ID: TcxGridDBColumn
          DataBinding.FieldName = 'ID'
          Visible = False
        end
        object cxGrid1DBTableView1ID_TIP_NORMATIV: TcxGridDBColumn
          DataBinding.FieldName = 'ID_TIP_NORMATIV'
          Visible = False
        end
        object cxGrid1DBTableView1NAZIV_TIP_NORMATIV: TcxGridDBColumn
          DataBinding.FieldName = 'NAZIV_TIP_NORMATIV'
          Visible = False
        end
        object cxGrid1DBTableView1VID_ARTIKAL: TcxGridDBColumn
          DataBinding.FieldName = 'VID_ARTIKAL'
          Visible = False
        end
        object cxGrid1DBTableView1ARTIKAL: TcxGridDBColumn
          DataBinding.FieldName = 'ARTIKAL'
          Visible = False
        end
        object cxGrid1DBTableView1KOLICINA: TcxGridDBColumn
          DataBinding.FieldName = 'KOLICINA'
          Visible = False
        end
        object cxGrid1DBTableView1MERKA: TcxGridDBColumn
          DataBinding.FieldName = 'MERKA'
          Visible = False
        end
        object cxGrid1DBTableView1AKTIVEN: TcxGridDBColumn
          DataBinding.FieldName = 'AKTIVEN'
          Visible = False
        end
        object cxGrid1DBTableView1ID_OPERACIJA: TcxGridDBColumn
          DataBinding.FieldName = 'ID_OPERACIJA'
          Visible = False
        end
        object cxGrid1DBTableView1KOREN: TcxGridDBColumn
          DataBinding.FieldName = 'KOREN'
          Visible = False
        end
        object cxGrid1DBTableView1POTEKLO: TcxGridDBColumn
          DataBinding.FieldName = 'POTEKLO'
          Visible = False
        end
        object cxGrid1DBTableView1SEKVENCA: TcxGridDBColumn
          DataBinding.FieldName = 'SEKVENCA'
          Visible = False
        end
        object cxGrid1DBTableView1DATUM_OD: TcxGridDBColumn
          DataBinding.FieldName = 'DATUM_OD'
          Visible = False
        end
        object cxGrid1DBTableView1DATUM_DO: TcxGridDBColumn
          DataBinding.FieldName = 'DATUM_DO'
          Visible = False
        end
        object cxGrid1DBTableView1FLAG: TcxGridDBColumn
          DataBinding.FieldName = 'FLAG'
          Visible = False
        end
        object cxGrid1DBTableView1SLIKA: TcxGridDBColumn
          DataBinding.FieldName = 'SLIKA'
          Visible = False
        end
        object cxGrid1DBTableView1MALA_SLIKA: TcxGridDBColumn
          DataBinding.FieldName = 'MALA_SLIKA'
          Visible = False
        end
      end
      object cxGrid1DBTableView2: TcxGridDBTableView [1]
        Navigator.Buttons.CustomButtons = <>
        DataController.DataSource = dm.dsNormativ
        DataController.DetailKeyFieldNames = 'KOREN'
        DataController.KeyFieldNames = 'ID'
        DataController.MasterKeyFieldNames = 'ID'
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        OptionsData.Deleting = False
        OptionsData.Editing = False
        OptionsData.Inserting = False
        OptionsView.GroupByBox = False
        OptionsView.Header = False
        object cxGrid1DBTableView2NAZIV_ARTIKAL: TcxGridDBColumn
          Caption = #1040#1088#1090#1080#1082#1072#1083
          DataBinding.FieldName = 'NAZIV_ARTIKAL'
          SortIndex = 0
          SortOrder = soDescending
          Width = 205
        end
        object cxGrid1DBTableView2ID: TcxGridDBColumn
          DataBinding.FieldName = 'ID'
          Visible = False
        end
        object cxGrid1DBTableView2VID_ARTIKAL: TcxGridDBColumn
          DataBinding.FieldName = 'VID_ARTIKAL'
          Visible = False
        end
        object cxGrid1DBTableView2ARTIKAL: TcxGridDBColumn
          DataBinding.FieldName = 'ARTIKAL'
          Visible = False
        end
        object cxGrid1DBTableView2AKTIVEN: TcxGridDBColumn
          DataBinding.FieldName = 'AKTIVEN'
          Visible = False
        end
        object cxGrid1DBTableView2KOLICINA: TcxGridDBColumn
          DataBinding.FieldName = 'KOLICINA'
          Visible = False
        end
        object cxGrid1DBTableView2MERKA: TcxGridDBColumn
          DataBinding.FieldName = 'MERKA'
          Visible = False
        end
        object cxGrid1DBTableView2ID_TIP_NORMATIV: TcxGridDBColumn
          DataBinding.FieldName = 'ID_TIP_NORMATIV'
          Visible = False
        end
        object cxGrid1DBTableView2NAZIV_TIP_NORMATIV: TcxGridDBColumn
          DataBinding.FieldName = 'NAZIV_TIP_NORMATIV'
          Visible = False
        end
        object cxGrid1DBTableView2ID_OPERACIJA: TcxGridDBColumn
          DataBinding.FieldName = 'ID_OPERACIJA'
          Visible = False
        end
        object cxGrid1DBTableView2SEKVENCA: TcxGridDBColumn
          DataBinding.FieldName = 'SEKVENCA'
          Visible = False
        end
        object cxGrid1DBTableView2POTEKLO: TcxGridDBColumn
          DataBinding.FieldName = 'POTEKLO'
          Visible = False
        end
        object cxGrid1DBTableView2DATUM_OD: TcxGridDBColumn
          DataBinding.FieldName = 'DATUM_OD'
          Visible = False
        end
        object cxGrid1DBTableView2DATUM_DO: TcxGridDBColumn
          DataBinding.FieldName = 'DATUM_DO'
          Visible = False
        end
        object cxGrid1DBTableView2KOREN: TcxGridDBColumn
          DataBinding.FieldName = 'KOREN'
          Visible = False
        end
        object cxGrid1DBTableView2FLAG: TcxGridDBColumn
          DataBinding.FieldName = 'FLAG'
          Visible = False
        end
        object cxGrid1DBTableView2SLIKA: TcxGridDBColumn
          DataBinding.FieldName = 'SLIKA'
          Visible = False
        end
        object cxGrid1DBTableView2MALA_SLIKA: TcxGridDBColumn
          DataBinding.FieldName = 'MALA_SLIKA'
          Visible = False
        end
      end
      inherited cxGrid1Level1: TcxGridLevel
        object cxGrid1Level2: TcxGridLevel
          GridView = cxGrid1DBTableView2
        end
      end
    end
    object txtSearchBox: TSearchBox
      Left = 2
      Top = 2
      Width = 258
      Height = 21
      Hint = #1042#1085#1077#1089#1077#1090#1077' '#1076#1077#1083' '#1086#1076' '#1085#1072#1079#1080#1074#1086#1090' '#1085#1072' '#1087#1088#1086#1080#1079#1074#1086#1076#1086#1090' '#1080' '#1082#1083#1080#1082#1085#1077#1090#1077' '#1045#1085#1090#1077#1088
      Align = alTop
      TabOrder = 1
      OnKeyDown = txtSearchBoxKeyDown
    end
  end
  inherited dPanel: TPanel
    Left = 264
    Top = 126
    Width = 760
    Height = 369
    Align = alRight
    Anchors = [akLeft, akTop, akRight, akBottom]
    Enabled = True
    ExplicitLeft = 264
    ExplicitTop = 126
    ExplicitWidth = 760
    ExplicitHeight = 369
    inherited Label1: TLabel
      Left = 549
      Top = 30
      ExplicitLeft = 549
      ExplicitTop = 30
    end
    inherited Sifra: TcxDBTextEdit
      Tag = 0
      Left = 605
      Top = 27
      Anchors = [akLeft, akTop, akRight, akBottom]
      ExplicitLeft = 605
      ExplicitTop = 27
      ExplicitWidth = 304
      Width = 304
    end
    object cxGroupBox2: TcxGroupBox [2]
      Left = 2
      Top = 2
      Align = alTop
      Caption = '  '#1053#1086#1088#1084#1072#1090#1080#1074' '#1079#1072' '#1072#1088#1090#1080#1082#1083#1086#1090'  ...'
      ParentBackground = False
      ParentFont = False
      Style.Font.Charset = RUSSIAN_CHARSET
      Style.Font.Color = clWindowText
      Style.Font.Height = -12
      Style.Font.Name = 'Tahoma'
      Style.Font.Style = []
      Style.TextStyle = [fsBold]
      Style.TransparentBorder = False
      Style.IsFontAssigned = True
      TabOrder = 1
      OnExit = cxGroupBox2Exit
      DesignSize = (
        756
        120)
      Height = 120
      Width = 756
      object cxLabel6: TcxLabel
        Left = 46
        Top = 36
        Caption = #1040#1088#1090#1080#1082#1072#1083
        Style.TextColor = clRed
        Properties.WordWrap = True
        Transparent = True
        Width = 51
      end
      object txtVidArtikal: TcxDBTextEdit
        Tag = 1
        Left = 101
        Top = 34
        Hint = #1042#1080#1076' '#1085#1072' '#1072#1088#1090#1080#1082#1072#1083'/'#1087#1088#1086#1080#1079#1074#1086#1076
        DataBinding.DataField = 'VID_ARTIKAL'
        DataBinding.DataSource = dm.dsNormativ
        ParentFont = False
        TabOrder = 0
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 59
      end
      object txtArtikal: TcxDBTextEdit
        Tag = 1
        Left = 160
        Top = 34
        Hint = #1064#1080#1092#1088#1072' '#1085#1072' '#1072#1088#1090#1080#1082#1072#1083' / '#1087#1088#1086#1080#1079#1074#1086#1076
        DataBinding.DataField = 'ARTIKAL'
        DataBinding.DataSource = dm.dsNormativ
        ParentFont = False
        TabOrder = 1
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 58
      end
      object cbNazivArtikal: TcxLookupComboBox
        Left = 217
        Top = 34
        Hint = '* '#1044#1074#1086#1077#1085' '#1082#1083#1080#1082' (Insert), '#1079#1072' '#1080#1079#1073#1086#1088' ('#1074#1085#1077#1089') '#1085#1072' '#1072#1088#1090#1080#1082#1072#1083
        Properties.DropDownAutoSize = True
        Properties.DropDownSizeable = True
        Properties.KeyFieldNames = 'ARTVID;id'
        Properties.ListColumns = <
          item
            Width = 30
            FieldName = 'ARTVID'
          end
          item
            Width = 40
            FieldName = 'id'
          end
          item
            Width = 150
            FieldName = 'naziv'
          end>
        Properties.ListFieldIndex = 2
        Properties.ListOptions.SyncMode = True
        Properties.ListSource = dmMat.dsArtikal
        TabOrder = 2
        OnDblClick = cbNazivArtikalClick
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 436
      end
      object cxLabel7: TcxLabel
        Left = 55
        Top = 89
        Caption = #1058#1080#1087' '#1085#1086#1088#1084#1072#1090#1080#1074
        ParentColor = False
        Style.Color = 15790320
        Style.TextColor = clNavy
        Transparent = True
        Visible = False
      end
      object txtTipNormativ: TcxDBTextEdit
        Tag = 1
        Left = 144
        Top = 90
        DataBinding.DataField = 'ID_TIP_NORMATIV'
        DataBinding.DataSource = dm.dsNormativ
        Style.BorderStyle = ebsUltraFlat
        Style.LookAndFeel.NativeStyle = False
        Style.LookAndFeel.SkinName = ''
        Style.TextStyle = []
        Style.TransparentBorder = False
        StyleDisabled.LookAndFeel.NativeStyle = False
        StyleDisabled.LookAndFeel.SkinName = ''
        StyleDisabled.TextColor = clWindowText
        StyleFocused.LookAndFeel.NativeStyle = False
        StyleFocused.LookAndFeel.SkinName = ''
        StyleHot.LookAndFeel.NativeStyle = False
        StyleHot.LookAndFeel.SkinName = ''
        TabOrder = 4
        Visible = False
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 56
      end
      object cbTipNormativ: TcxDBLookupComboBox
        Left = 202
        Top = 89
        Hint = #1050#1083#1080#1082#1085#1077#1090#1077' '#1076#1074#1072' '#1087#1072#1090#1080' '#1079#1072' '#1074#1085#1077#1089' '#1085#1072' '#1085#1086#1074' '#1090#1080#1087' '#1085#1072' '#1085#1086#1088#1084#1072#1090#1080#1074
        AutoSize = False
        DataBinding.DataField = 'ID_TIP_NORMATIV'
        DataBinding.DataSource = dm.dsNormativ
        Properties.ClearKey = 46
        Properties.DropDownAutoSize = True
        Properties.DropDownSizeable = True
        Properties.ImmediatePost = True
        Properties.KeyFieldNames = 'ID'
        Properties.ListColumns = <
          item
            Width = 50
            FieldName = 'NAZIV'
          end>
        Properties.ListSource = dm.dsTipNormativ
        TabOrder = 5
        Visible = False
        OnDblClick = DblClick
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Height = 21
        Width = 243
      end
      object cxLabel8: TcxLabel
        Left = 547
        Top = 61
        Caption = #1050#1086#1088#1077#1085
        Style.TextColor = clNavy
        Transparent = True
        Visible = False
      end
      object txtKoren: TcxDBTextEdit
        Left = 590
        Top = 62
        DataBinding.DataField = 'KOREN'
        DataBinding.DataSource = dm.dsNormativ
        ParentFont = False
        Style.BorderStyle = ebsUltraFlat
        Style.Font.Charset = RUSSIAN_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -11
        Style.Font.Name = 'Tahoma'
        Style.Font.Style = []
        Style.LookAndFeel.NativeStyle = False
        Style.LookAndFeel.SkinName = ''
        Style.TextStyle = []
        Style.TransparentBorder = False
        Style.IsFontAssigned = True
        StyleDisabled.LookAndFeel.NativeStyle = False
        StyleDisabled.LookAndFeel.SkinName = ''
        StyleDisabled.TextColor = clWindowText
        StyleFocused.LookAndFeel.NativeStyle = False
        StyleFocused.LookAndFeel.SkinName = ''
        StyleHot.LookAndFeel.NativeStyle = False
        StyleHot.LookAndFeel.SkinName = ''
        TabOrder = 3
        Visible = False
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 56
      end
      object cxLabel10: TcxLabel
        Left = 41
        Top = 62
        Caption = #1044#1072#1090#1091#1084' '#1086#1076
        Style.TextColor = clNavy
        Transparent = True
      end
      object cxLabel11: TcxLabel
        Left = 225
        Top = 62
        Caption = #1044#1072#1090#1091#1084' '#1076#1086
        Style.TextColor = clNavy
        Transparent = True
      end
      object txtDatumDo: TcxDBDateEdit
        Left = 286
        Top = 61
        Hint = #1044#1072#1090#1091#1084' '#1076#1086' '#1082#1086#1075#1072' '#1074#1072#1078#1080' '#1085#1086#1088#1084#1072#1090#1080#1074#1086#1090
        DataBinding.DataField = 'DATUM_DO'
        DataBinding.DataSource = dm.dsNormativ
        TabOrder = 7
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 121
      end
      object txtDatumOd: TcxDBDateEdit
        Left = 101
        Top = 61
        Hint = #1044#1072#1090#1091#1084' '#1086#1076' '#1082#1086#1075#1072' '#1074#1072#1078#1080' '#1085#1086#1088#1084#1072#1090#1080#1074#1086#1090
        DataBinding.DataField = 'DATUM_OD'
        DataBinding.DataSource = dm.dsNormativ
        TabOrder = 6
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 117
      end
      object cxDBImage1: TcxDBImage
        Left = 670
        Top = 31
        Hint = #1057#1083#1080#1082#1072' '#1085#1072' '#1072#1088#1090#1080#1082#1083#1086#1090' / '#1087#1088#1086#1080#1079#1074#1086#1076#1086#1090
        DataBinding.DataField = 'SLIKA'
        DataBinding.DataSource = dm.dsNormativ
        Properties.GraphicClassName = 'TdxSmartImage'
        Properties.GraphicTransparency = gtTransparent
        Style.LookAndFeel.NativeStyle = False
        Style.Shadow = False
        StyleDisabled.LookAndFeel.NativeStyle = False
        StyleFocused.LookAndFeel.NativeStyle = False
        StyleHot.LookAndFeel.NativeStyle = False
        TabOrder = 13
        Height = 60
        Width = 75
      end
      object cxGroupBox3: TcxGroupBox
        Left = 0
        Top = -2
        Anchors = [akLeft, akTop, akRight, akBottom]
        Caption = '  '#1053#1086#1088#1084#1072#1090#1080#1074' '#1079#1072' '#1072#1088#1090#1080#1082#1083#1086#1090'  ...'
        Enabled = False
        ParentBackground = False
        ParentFont = False
        Style.Font.Charset = RUSSIAN_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -12
        Style.Font.Name = 'Tahoma'
        Style.Font.Style = []
        Style.Shadow = False
        Style.TextStyle = [fsBold]
        Style.TransparentBorder = False
        Style.IsFontAssigned = True
        TabOrder = 14
        Visible = False
        Height = 130
        Width = 912
        object cxLabel17: TcxLabel
          Left = 46
          Top = 36
          Caption = #1040#1088#1090#1080#1082#1072#1083
          Style.TextColor = clRed
          Properties.WordWrap = True
          Transparent = True
          Width = 51
        end
        object cxLabel18: TcxLabel
          Left = 96
          Top = 88
          Caption = #1058#1080#1087' '#1085#1086#1088#1084#1072#1090#1080#1074
          ParentColor = False
          Style.Color = 15790320
          Style.TextColor = clNavy
          Transparent = True
          Visible = False
        end
        object cxLabel19: TcxLabel
          Left = 555
          Top = 62
          Caption = #1050#1086#1088#1077#1085
          Style.TextColor = clNavy
          Transparent = True
          Visible = False
        end
        object txtK: TcxDBTextEdit
          Left = 594
          Top = 63
          DataBinding.DataField = 'KOREN'
          DataBinding.DataSource = dm.dsNormativ
          ParentFont = False
          Style.BorderStyle = ebsUltraFlat
          Style.Font.Charset = RUSSIAN_CHARSET
          Style.Font.Color = clWindowText
          Style.Font.Height = -11
          Style.Font.Name = 'Tahoma'
          Style.Font.Style = []
          Style.LookAndFeel.NativeStyle = False
          Style.LookAndFeel.SkinName = ''
          Style.TextStyle = []
          Style.TransparentBorder = False
          Style.IsFontAssigned = True
          StyleDisabled.LookAndFeel.NativeStyle = False
          StyleDisabled.LookAndFeel.SkinName = ''
          StyleDisabled.TextColor = clWindowText
          StyleFocused.LookAndFeel.NativeStyle = False
          StyleFocused.LookAndFeel.SkinName = ''
          StyleHot.LookAndFeel.NativeStyle = False
          StyleHot.LookAndFeel.SkinName = ''
          TabOrder = 0
          Visible = False
          Width = 56
        end
        object cxLabel22: TcxLabel
          Left = 42
          Top = 62
          Caption = #1044#1072#1090#1091#1084' '#1086#1076
          Style.TextColor = clNavy
          Transparent = True
        end
        object cxLabel23: TcxLabel
          Left = 280
          Top = 62
          Caption = #1044#1072#1090#1091#1084' '#1076#1086
          Style.TextColor = clNavy
          Transparent = True
        end
        object txtA: TcxTextEdit
          Left = 157
          Top = 31
          Hint = #1064#1080#1092#1088#1072' '#1085#1072' '#1072#1088#1090#1080#1082#1072#1083
          TabOrder = 6
          Width = 57
        end
        object cbA: TcxTextEdit
          Left = 214
          Top = 31
          TabOrder = 7
          Width = 433
        end
        object txtVidA: TcxTextEdit
          Left = 104
          Top = 31
          Hint = #1042#1080#1076' '#1085#1072' '#1072#1088#1090#1080#1082#1072#1083
          TabOrder = 8
          Width = 53
        end
        object cbTN: TcxTextEdit
          Left = 237
          Top = 86
          TabOrder = 9
          Visible = False
          Width = 249
        end
        object txtTN: TcxTextEdit
          Left = 184
          Top = 86
          TabOrder = 10
          Visible = False
          Width = 53
        end
        object txtDod: TcxDateEdit
          Left = 103
          Top = 61
          TabOrder = 11
          Width = 116
        end
        object txtDdo: TcxDateEdit
          Left = 339
          Top = 61
          TabOrder = 12
          Width = 118
        end
        object cxDBImage2: TcxDBImage
          Left = 670
          Top = 31
          DataBinding.DataField = 'SLIKA'
          DataBinding.DataSource = dm.dsNormativ
          Properties.GraphicClassName = 'TdxSmartImage'
          Style.BorderStyle = ebsUltraFlat
          Style.Color = clWindow
          Style.Edges = [bLeft, bTop, bRight, bBottom]
          Style.HotTrack = True
          Style.LookAndFeel.NativeStyle = False
          Style.Shadow = False
          Style.TextStyle = []
          Style.TransparentBorder = True
          StyleDisabled.LookAndFeel.NativeStyle = False
          StyleFocused.LookAndFeel.NativeStyle = False
          StyleHot.LookAndFeel.NativeStyle = False
          TabOrder = 13
          Height = 60
          Width = 75
        end
      end
    end
    object PanelSurovini: TPanel [3]
      Left = 2
      Top = 241
      Width = 756
      Height = 126
      Align = alClient
      TabOrder = 2
      object cxGrid4: TcxGrid
        Left = 1
        Top = 1
        Width = 754
        Height = 124
        Align = alClient
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
        RootLevelOptions.DetailTabsPosition = dtpTop
        object cxGrid4DBTableView1: TcxGridDBTableView
          Navigator.Buttons.CustomButtons = <>
          DataController.DataSource = dm.dsNormativO
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          object cxGrid4DBTableView1ID: TcxGridDBColumn
            Caption = #1064#1080#1092#1088#1072' '#1085#1072' '#1085#1086#1088#1084#1072#1090#1080#1074
            DataBinding.FieldName = 'ID'
          end
          object cxGrid4DBTableView1VID_ARTIKAL: TcxGridDBColumn
            Caption = #1042#1080#1076' '#1085#1072' '#1072#1088#1090#1080#1082#1072#1083
            DataBinding.FieldName = 'VID_ARTIKAL'
          end
          object cxGrid4DBTableView1ARTIKAL: TcxGridDBColumn
            Caption = #1064#1080#1092#1088#1072' '#1085#1072' '#1072#1088#1090#1080#1082#1072#1083
            DataBinding.FieldName = 'ARTIKAL'
            Width = 114
          end
          object cxGrid4DBTableView1NAZIV_ARTIKAL: TcxGridDBColumn
            Caption = #1040#1088#1090#1080#1082#1072#1083
            DataBinding.FieldName = 'NAZIV_ARTIKAL'
            Width = 229
          end
          object cxGrid4DBTableView1KOLICINA: TcxGridDBColumn
            Caption = #1050#1086#1083#1080#1095#1080#1085#1072
            DataBinding.FieldName = 'KOLICINA'
          end
          object cxGrid4DBTableView1MERKA: TcxGridDBColumn
            Caption = #1052#1077#1088#1082#1072
            DataBinding.FieldName = 'MERKA'
            Width = 62
          end
          object cxGrid4DBTableView1SEKVENCA: TcxGridDBColumn
            Caption = #1057#1077#1082#1074#1077#1085#1094#1072
            DataBinding.FieldName = 'SEKVENCA'
          end
          object cxGrid4DBTableView1DATUM_OD: TcxGridDBColumn
            Caption = #1044#1072#1090#1091#1084' '#1086#1076
            DataBinding.FieldName = 'DATUM_OD'
          end
          object cxGrid4DBTableView1DATUM_DO: TcxGridDBColumn
            Caption = #1044#1072#1090#1091#1084' '#1076#1086
            DataBinding.FieldName = 'DATUM_DO'
          end
        end
        object cxGrid4DBBandedTableView1: TcxGridDBBandedTableView
          Navigator.Buttons.CustomButtons = <>
          DataController.DataSource = dm.dsNormativ
          DataController.DetailKeyFieldNames = 'KOREN'
          DataController.Filter.Active = True
          DataController.KeyFieldNames = 'ID'
          DataController.Options = [dcoAnsiSort, dcoAssignGroupingValues, dcoAssignMasterDetailKeys, dcoSaveExpanding]
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          FilterRow.InfoText = #1050#1083#1080#1082#1085#1077#1090#1077' '#1090#1091#1082#1072' '#1079#1072' '#1076#1072' '#1076#1077#1092#1080#1085#1080#1088#1072#1090#1077' '#1092#1080#1083#1090#1077#1088
          FilterRow.Visible = True
          OptionsBehavior.IncSearch = True
          OptionsCustomize.ColumnsQuickCustomization = True
          OptionsData.Deleting = False
          OptionsData.Editing = False
          OptionsData.Inserting = False
          OptionsView.ColumnAutoWidth = True
          OptionsView.GroupByBox = False
          OptionsView.Indicator = True
          OptionsView.BandCaptionsInColumnAlternateCaption = True
          Bands = <
            item
              Caption = #1057#1091#1088#1086#1074#1080#1085#1080' '#1079#1072' '#1080#1079#1073#1088#1072#1085#1080#1086#1090' '#1072#1088#1090#1080#1082#1072#1083
              HeaderAlignmentHorz = taLeftJustify
              Styles.Header = dmRes.Autumn
              Width = 1522
            end>
          object cxGrid4DBBandedTableView1SEKVENCA: TcxGridDBBandedColumn
            Caption = #1057#1077#1082#1074#1077#1085#1094#1072
            DataBinding.FieldName = 'SEKVENCA'
            Width = 238
            Position.BandIndex = 0
            Position.ColIndex = 6
            Position.RowIndex = 0
          end
          object cxGrid4DBBandedTableView1ID: TcxGridDBBandedColumn
            Caption = #1064#1080#1092#1088#1072' '#1085#1072' '#1085#1086#1088#1084#1072#1090#1080#1074
            DataBinding.FieldName = 'ID'
            Visible = False
            Width = 65
            Position.BandIndex = 0
            Position.ColIndex = 0
            Position.RowIndex = 0
          end
          object cxGrid4DBBandedTableView1VID_ARTIKAL: TcxGridDBBandedColumn
            Caption = #1042#1080#1076' '#1085#1072' '#1072#1088#1090#1080#1082#1072#1083
            DataBinding.FieldName = 'VID_ARTIKAL'
            Width = 84
            Position.BandIndex = 0
            Position.ColIndex = 1
            Position.RowIndex = 0
          end
          object cxGrid4DBBandedTableView1ARTIKAL: TcxGridDBBandedColumn
            Caption = #1064#1080#1092#1088#1072' '#1085#1072' '#1072#1088#1090#1080#1082#1072#1083
            DataBinding.FieldName = 'ARTIKAL'
            Width = 78
            Position.BandIndex = 0
            Position.ColIndex = 2
            Position.RowIndex = 0
          end
          object cxGrid4DBBandedTableView1NAZIV_ARTIKAL: TcxGridDBBandedColumn
            Caption = #1040#1088#1090#1080#1082#1072#1083
            DataBinding.FieldName = 'NAZIV_ARTIKAL'
            Width = 360
            Position.BandIndex = 0
            Position.ColIndex = 3
            Position.RowIndex = 0
          end
          object cxGrid4DBBandedTableView1KOLICINA: TcxGridDBBandedColumn
            Caption = #1050#1086#1083#1080#1095#1080#1085#1072' '
            DataBinding.FieldName = 'KOLICINA'
            Width = 43
            Position.BandIndex = 0
            Position.ColIndex = 4
            Position.RowIndex = 0
          end
          object cxGrid4DBBandedTableView1MERKA: TcxGridDBBandedColumn
            Caption = #1052#1077#1088#1082#1072
            DataBinding.FieldName = 'MERKA'
            Width = 30
            Position.BandIndex = 0
            Position.ColIndex = 5
            Position.RowIndex = 0
          end
          object cxGrid4DBBandedTableView1KOREN: TcxGridDBBandedColumn
            DataBinding.FieldName = 'KOREN'
            Visible = False
            Position.BandIndex = 0
            Position.ColIndex = 7
            Position.RowIndex = 0
          end
        end
        object cxGrid4DBBandedTableView2: TcxGridDBBandedTableView
          Navigator.Buttons.CustomButtons = <>
          DataController.DataSource = dm.dsNormativOperacija
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          FilterRow.InfoText = #1050#1083#1080#1082#1085#1077#1090#1077' '#1090#1091#1082#1072' '#1079#1072' '#1076#1072' '#1076#1077#1092#1080#1085#1080#1088#1072#1090#1077' '#1092#1080#1083#1090#1077#1088
          FilterRow.Visible = True
          OptionsBehavior.IncSearch = True
          OptionsCustomize.ColumnsQuickCustomization = True
          OptionsCustomize.BandsQuickCustomization = True
          OptionsData.Deleting = False
          OptionsData.Editing = False
          OptionsData.Inserting = False
          OptionsView.GroupByBox = False
          OptionsView.Indicator = True
          Bands = <
            item
              Caption = #1054#1087#1077#1088#1072#1094#1080#1080' '#1085#1072' '#1085#1086#1088#1084#1072#1090#1080#1074#1086#1090
              HeaderAlignmentHorz = taLeftJustify
              Styles.Header = dmRes.Autumn
            end>
          object cxGrid4DBBandedTableView2ID: TcxGridDBBandedColumn
            Caption = #1064#1080#1092#1088#1072
            DataBinding.FieldName = 'ID'
            Position.BandIndex = 0
            Position.ColIndex = 0
            Position.RowIndex = 0
          end
          object cxGrid4DBBandedTableView2ID_PR_OPERACIJA: TcxGridDBBandedColumn
            Caption = #1064#1080#1092#1088#1072' '#1085#1072' '#1086#1087#1077#1088#1072#1094#1080#1112#1072
            DataBinding.FieldName = 'ID_PR_OPERACIJA'
            Position.BandIndex = 0
            Position.ColIndex = 1
            Position.RowIndex = 0
          end
          object cxGrid4DBBandedTableView2OPERACIJA: TcxGridDBBandedColumn
            Caption = #1054#1087#1077#1088#1072#1094#1080#1112#1072
            DataBinding.FieldName = 'OPERACIJA'
            Position.BandIndex = 0
            Position.ColIndex = 2
            Position.RowIndex = 0
          end
          object cxGrid4DBBandedTableView2SEKVENCA: TcxGridDBBandedColumn
            Caption = #1057#1077#1082#1074#1077#1085#1094#1072
            DataBinding.FieldName = 'SEKVENCA'
            Position.BandIndex = 0
            Position.ColIndex = 3
            Position.RowIndex = 0
          end
          object cxGrid4DBBandedTableView2POTREBNO_VREME: TcxGridDBBandedColumn
            Caption = #1055#1086#1090#1088#1077#1073#1085#1086' '#1074#1088#1077#1084#1077
            DataBinding.FieldName = 'POTREBNO_VREME'
            Width = 50
            Position.BandIndex = 0
            Position.ColIndex = 4
            Position.RowIndex = 0
          end
          object cxGrid4DBBandedTableView2MERNA_EDINICA: TcxGridDBBandedColumn
            Caption = #1052#1077#1088'.'#1045#1076#1080#1085'.'
            DataBinding.FieldName = 'MERNA_EDINICA'
            Position.BandIndex = 0
            Position.ColIndex = 5
            Position.RowIndex = 0
          end
        end
        object cxGrid4Level1: TcxGridLevel
          Caption = #1057#1091#1088#1086#1074#1080#1085#1080
          GridView = cxGrid4DBBandedTableView1
        end
        object cxGrid4Level2: TcxGridLevel
          Caption = #1054#1087#1077#1088#1072#1094#1080#1080
          GridView = cxGrid4DBBandedTableView2
        end
      end
    end
    inherited OtkaziButton: TcxButton
      Left = 671
      Top = 158
      TabOrder = 3
      Visible = False
      ExplicitLeft = 671
      ExplicitTop = 158
    end
    inherited ZapisiButton: TcxButton
      Left = 590
      Top = 164
      TabOrder = 4
      Visible = False
      ExplicitLeft = 590
      ExplicitTop = 164
    end
    object pcSurOpe: TcxPageControl
      Left = 2
      Top = 122
      Width = 756
      Height = 119
      Align = alTop
      TabOrder = 5
      Visible = False
      Properties.ActivePage = tsSurovini
      Properties.CustomButtons.Buttons = <>
      ClientRectBottom = 119
      ClientRectRight = 756
      ClientRectTop = 24
      object tsSurovini: TcxTabSheet
        Caption = #1057#1091#1088#1086#1074#1080#1085#1080
        object PanelVnesSurovina: TPanel
          Left = 0
          Top = 0
          Width = 756
          Height = 95
          Align = alClient
          Enabled = False
          TabOrder = 0
          object cxLabel3: TcxLabel
            Left = 28
            Top = 5
            Caption = #1057#1091#1088#1086#1074#1080#1085#1072' ('#1084#1072#1090#1077#1088#1080#1112#1072#1083')'
            Style.TextColor = clRed
            Properties.WordWrap = True
            Transparent = True
            Width = 63
          end
          object txtVidSurovina: TcxDBTextEdit
            Tag = 1
            Left = 102
            Top = 12
            Hint = #1042#1080#1076' '#1085#1072' '#1089#1091#1088#1086#1074#1080#1085#1072
            DataBinding.DataField = 'VID_ARTIKAL'
            DataBinding.DataSource = dm.dsNormativ
            ParentFont = False
            TabOrder = 1
            OnEnter = cxDBTextEditAllEnter
            OnExit = cxDBTextEditAllExit
            OnKeyDown = EnterKakoTab
            Width = 59
          end
          object txtSurovina: TcxDBTextEdit
            Tag = 1
            Left = 160
            Top = 12
            Hint = #1064#1080#1092#1088#1072' '#1085#1072' '#1089#1091#1088#1086#1074#1080#1085#1072
            DataBinding.DataField = 'ARTIKAL'
            DataBinding.DataSource = dm.dsNormativ
            ParentFont = False
            TabOrder = 2
            OnEnter = cxDBTextEditAllEnter
            OnExit = cxDBTextEditAllExit
            OnKeyDown = EnterKakoTab
            Width = 58
          end
          object cbSurovina: TcxLookupComboBox
            Left = 224
            Top = 15
            Hint = #1053#1072#1079#1080#1074' '#1085#1072' '#1089#1091#1088#1086#1074#1080#1085#1072
            Properties.DropDownAutoSize = True
            Properties.DropDownSizeable = True
            Properties.KeyFieldNames = 'ARTVID;id'
            Properties.ListColumns = <
              item
                Width = 30
                FieldName = 'ARTVID'
              end
              item
                Width = 40
                FieldName = 'id'
              end
              item
                Width = 150
                FieldName = 'naziv'
              end>
            Properties.ListFieldIndex = 2
            Properties.ListOptions.SyncMode = True
            Properties.ListSource = dm.dsArtikal
            TabOrder = 3
            OnEnter = cxDBTextEditAllEnter
            OnExit = cxDBTextEditAllExit
            OnKeyDown = EnterKakoTab
            Width = 414
          end
          object cxLabel15: TcxLabel
            Left = 28
            Top = 37
            AutoSize = False
            Caption = #1056#1077#1076#1086#1089#1083#1077#1076' ('#1089#1077#1082#1074#1077#1085#1094#1072')'
            Style.TextColor = clNavy
            Properties.WordWrap = True
            Transparent = True
            Height = 31
            Width = 63
          end
          object txtSekvenca: TcxDBTextEdit
            Left = 102
            Top = 43
            Hint = #1056#1077#1076#1086#1089#1083#1077#1076' '#1085#1072' '#1089#1091#1088#1086#1074#1080#1085#1072#1090#1072
            DataBinding.DataField = 'SEKVENCA'
            DataBinding.DataSource = dm.dsNormativ
            ParentFont = False
            Style.BorderStyle = ebsUltraFlat
            Style.Font.Charset = RUSSIAN_CHARSET
            Style.Font.Color = clWindowText
            Style.Font.Height = -11
            Style.Font.Name = 'Tahoma'
            Style.Font.Style = []
            Style.LookAndFeel.NativeStyle = False
            Style.LookAndFeel.SkinName = ''
            Style.TextStyle = []
            Style.TransparentBorder = False
            Style.IsFontAssigned = True
            StyleDisabled.LookAndFeel.NativeStyle = False
            StyleDisabled.LookAndFeel.SkinName = ''
            StyleFocused.LookAndFeel.NativeStyle = False
            StyleFocused.LookAndFeel.SkinName = ''
            StyleHot.LookAndFeel.NativeStyle = False
            StyleHot.LookAndFeel.SkinName = ''
            TabOrder = 5
            OnEnter = cxDBTextEditAllEnter
            OnExit = cxDBTextEditAllExit
            OnKeyDown = EnterKakoTab
            Width = 116
          end
          object cxLabel4: TcxLabel
            Left = 253
            Top = 45
            Caption = #1050#1086#1083#1080#1095#1080#1085#1072
            Style.TextColor = clRed
            Transparent = True
          end
          object txtKolicina: TcxDBTextEdit
            Tag = 1
            Left = 314
            Top = 43
            Hint = #1050#1086#1083#1080#1095#1080#1085#1072' '#1085#1072' '#1089#1091#1088#1086#1074#1080#1085#1072#1090#1072', '#1087#1086#1090#1088#1077#1073#1085#1086' '#1079#1072' '#1085#1086#1088#1084#1072#1090#1080#1074#1086#1090
            DataBinding.DataField = 'KOLICINA'
            DataBinding.DataSource = dm.dsNormativ
            ParentFont = False
            Style.BorderStyle = ebsUltraFlat
            Style.Font.Charset = RUSSIAN_CHARSET
            Style.Font.Color = clWindowText
            Style.Font.Height = -11
            Style.Font.Name = 'Tahoma'
            Style.Font.Style = []
            Style.LookAndFeel.NativeStyle = False
            Style.LookAndFeel.SkinName = ''
            Style.TextStyle = []
            Style.TransparentBorder = False
            Style.IsFontAssigned = True
            StyleDisabled.LookAndFeel.NativeStyle = False
            StyleDisabled.LookAndFeel.SkinName = ''
            StyleFocused.LookAndFeel.NativeStyle = False
            StyleFocused.LookAndFeel.SkinName = ''
            StyleHot.LookAndFeel.NativeStyle = False
            StyleHot.LookAndFeel.SkinName = ''
            TabOrder = 7
            OnEnter = cxDBTextEditAllEnter
            OnExit = cxDBTextEditAllExit
            OnKeyDown = EnterKakoTab
            Width = 59
          end
          object cxLabel5: TcxLabel
            Left = 394
            Top = 45
            Caption = #1052#1077#1088#1082#1072
            Style.TextColor = clRed
            Transparent = True
          end
          object txtMerka: TcxDBTextEdit
            Tag = 1
            Left = 430
            Top = 43
            Hint = #1064#1080#1092#1088#1072' '#1085#1072' '#1077#1076#1080#1085#1080#1094#1072' '#1084#1077#1088#1082#1072
            DataBinding.DataField = 'MERKA'
            DataBinding.DataSource = dm.dsNormativ
            ParentFont = False
            Style.BorderStyle = ebsUltraFlat
            Style.Font.Charset = RUSSIAN_CHARSET
            Style.Font.Color = clWindowText
            Style.Font.Height = -11
            Style.Font.Name = 'Tahoma'
            Style.Font.Style = []
            Style.LookAndFeel.NativeStyle = False
            Style.LookAndFeel.SkinName = ''
            Style.TextStyle = []
            Style.TransparentBorder = False
            Style.IsFontAssigned = True
            StyleDisabled.LookAndFeel.NativeStyle = False
            StyleDisabled.LookAndFeel.SkinName = ''
            StyleDisabled.TextColor = clWindowText
            StyleFocused.LookAndFeel.NativeStyle = False
            StyleFocused.LookAndFeel.SkinName = ''
            StyleHot.LookAndFeel.NativeStyle = False
            StyleHot.LookAndFeel.SkinName = ''
            TabOrder = 9
            OnEnter = cxDBTextEditAllEnter
            OnExit = cxDBTextEditAllExit
            OnKeyDown = EnterKakoTab
            Width = 56
          end
          object cbMerka: TcxDBLookupComboBox
            Left = 486
            Top = 42
            Hint = #1053#1072#1079#1080#1074' '#1085#1072' '#1077#1076#1085#1080#1094#1072' '#1084#1077#1088#1082#1072
            AutoSize = False
            DataBinding.DataField = 'MERKA'
            DataBinding.DataSource = dm.dsNormativ
            Properties.DropDownAutoSize = True
            Properties.DropDownSizeable = True
            Properties.KeyFieldNames = 'ID'
            Properties.ListColumns = <
              item
                FieldName = 'NAZIV'
              end>
            Properties.ListSource = dmMat.dsMerka
            TabOrder = 10
            OnEnter = cxDBTextEditAllEnter
            OnExit = cxDBTextEditAllExit
            OnKeyDown = EnterKakoTab
            Height = 21
            Width = 145
          end
          object cbAktiven: TcxDBCheckBox
            Left = 663
            Top = 12
            Caption = #1040#1082#1090#1080#1074#1077#1085
            DataBinding.DataField = 'AKTIVEN'
            DataBinding.DataSource = dm.dsNormativ
            Properties.Alignment = taLeftJustify
            Properties.ValueChecked = 1
            Properties.ValueUnchecked = 0
            Style.TextColor = clRed
            TabOrder = 13
            Transparent = True
            OnEnter = cxDBTextEditAllEnter
            OnExit = cxDBTextEditAllExit
            OnKeyDown = EnterKakoTab
            Width = 67
          end
          object ZapisiButton1: TcxButton
            Left = 593
            Top = 66
            Width = 73
            Height = 20
            Action = aZapisi
            Colors.Pressed = clGradientActiveCaption
            TabOrder = 11
          end
          object pnormativorg: TcxButton
            Left = 671
            Top = 66
            Width = 73
            Height = 20
            Action = aOtkazi
            Colors.Pressed = clGradientActiveCaption
            TabOrder = 12
          end
        end
      end
      object tsOperacii: TcxTabSheet
        Caption = #1054#1087#1077#1088#1072#1094#1080#1080
        ImageIndex = 1
        object PanelVnesOperacija: TPanel
          Left = 0
          Top = 0
          Width = 756
          Height = 95
          Align = alClient
          Enabled = False
          TabOrder = 0
          DesignSize = (
            756
            95)
          object cxLabel27: TcxLabel
            Left = 34
            Top = 20
            Caption = #1054#1087#1077#1088#1072#1094#1080#1112#1072
            Style.TextColor = clRed
            Transparent = True
          end
          object cxLabel28: TcxLabel
            Left = 263
            Top = 42
            Caption = #1056#1077#1076#1086#1089#1083#1077#1076' ('#1089#1077#1082#1074#1077#1085#1094#1072')'
            Style.TextColor = clNavy
            Properties.WordWrap = True
            Transparent = True
            Width = 59
          end
          object cxButton5: TcxButton
            Left = 528
            Top = 65
            Width = 75
            Height = 25
            Action = aZapisiOperacija
            Anchors = [akRight, akBottom]
            Colors.Pressed = clGradientActiveCaption
            TabOrder = 6
          end
          object cxButton6: TcxButton
            Left = 609
            Top = 65
            Width = 75
            Height = 25
            Action = aOtkazi
            Anchors = [akRight, akBottom]
            Colors.Pressed = clGradientActiveCaption
            TabOrder = 8
          end
          object txtSekvencaO: TcxDBTextEdit
            Left = 328
            Top = 47
            Hint = #1056#1077#1076#1086#1089#1083#1077#1076' '#1085#1072' '#1086#1087#1077#1088#1072#1094#1080#1112#1072#1090#1072
            DataBinding.DataField = 'SEKVENCA'
            DataBinding.DataSource = dm.dsNormativOperacija
            ParentFont = False
            Style.BorderStyle = ebsUltraFlat
            Style.Font.Charset = RUSSIAN_CHARSET
            Style.Font.Color = clWindowText
            Style.Font.Height = -11
            Style.Font.Name = 'Tahoma'
            Style.Font.Style = []
            Style.LookAndFeel.NativeStyle = False
            Style.LookAndFeel.SkinName = ''
            Style.TextStyle = []
            Style.TransparentBorder = False
            Style.IsFontAssigned = True
            StyleDisabled.LookAndFeel.NativeStyle = False
            StyleDisabled.LookAndFeel.SkinName = ''
            StyleFocused.LookAndFeel.NativeStyle = False
            StyleFocused.LookAndFeel.SkinName = ''
            StyleHot.LookAndFeel.NativeStyle = False
            StyleHot.LookAndFeel.SkinName = ''
            TabOrder = 4
            OnEnter = cxDBTextEditAllEnter
            OnExit = cxDBTextEditAllExit
            OnKeyDown = EnterKakoTab
            Width = 116
          end
          object cbOperacija: TcxDBLookupComboBox
            Left = 154
            Top = 20
            Hint = #1053#1072#1079#1080#1074' '#1085#1072' '#1086#1087#1077#1088#1072#1094#1080#1112#1072
            DataBinding.DataField = 'ID_PR_OPERACIJA'
            DataBinding.DataSource = dm.dsNormativOperacija
            Properties.DropDownAutoSize = True
            Properties.DropDownSizeable = True
            Properties.ImmediatePost = True
            Properties.KeyFieldNames = 'ID'
            Properties.ListColumns = <
              item
                Width = 150
                FieldName = 'NAZIV_OPERACIJA'
              end
              item
                Caption = #1055#1088#1086#1080#1079#1074#1086#1076#1089#1090#1074#1077#1085' '#1088#1077#1089#1091#1088#1089
                Width = 150
                FieldName = 'proizvodstven_resurs'
              end>
            Properties.ListOptions.SyncMode = True
            Properties.ListSource = dm.dsOperacijaPR
            TabOrder = 1
            OnEnter = cxDBTextEditAllEnter
            OnExit = cxDBTextEditAllExit
            OnKeyDown = EnterKakoTab
            Width = 399
          end
          object txtOperacija: TcxDBTextEdit
            Tag = 1
            Left = 97
            Top = 21
            Hint = #1064#1080#1092#1088#1072' '#1085#1072' '#1086#1087#1077#1088#1072#1094#1080#1112#1072
            DataBinding.DataField = 'ID_PR_OPERACIJA'
            DataBinding.DataSource = dm.dsNormativOperacija
            ParentFont = False
            Style.BorderStyle = ebsUltraFlat
            Style.LookAndFeel.NativeStyle = False
            Style.LookAndFeel.SkinName = ''
            Style.TextStyle = []
            Style.TransparentBorder = False
            StyleDisabled.LookAndFeel.NativeStyle = False
            StyleDisabled.LookAndFeel.SkinName = ''
            StyleFocused.LookAndFeel.NativeStyle = False
            StyleFocused.LookAndFeel.SkinName = ''
            StyleHot.LookAndFeel.NativeStyle = False
            StyleHot.LookAndFeel.SkinName = ''
            TabOrder = 0
            OnEnter = cxDBTextEditAllEnter
            OnExit = cxDBTextEditAllExit
            OnKeyDown = EnterKakoTab
            Width = 55
          end
          object txtPotrebnoVreme: TcxDBTextEdit
            Left = 97
            Top = 47
            Hint = #1055#1086#1090#1088#1077#1073#1085#1086' '#1074#1088#1077#1084#1077
            DataBinding.DataField = 'POTREBNO_VREME'
            DataBinding.DataSource = dm.dsNormativOperacija
            ParentFont = False
            Properties.ReadOnly = False
            Style.BorderStyle = ebsUltraFlat
            Style.Font.Charset = RUSSIAN_CHARSET
            Style.Font.Color = clWindowText
            Style.Font.Height = -11
            Style.Font.Name = 'Tahoma'
            Style.Font.Style = []
            Style.LookAndFeel.NativeStyle = False
            Style.LookAndFeel.SkinName = ''
            Style.TextStyle = []
            Style.TransparentBorder = False
            Style.IsFontAssigned = True
            StyleDisabled.LookAndFeel.NativeStyle = False
            StyleDisabled.LookAndFeel.SkinName = ''
            StyleFocused.LookAndFeel.NativeStyle = False
            StyleFocused.LookAndFeel.SkinName = ''
            StyleHot.LookAndFeel.NativeStyle = False
            StyleHot.LookAndFeel.SkinName = ''
            TabOrder = 2
            OnEnter = cxDBTextEditAllEnter
            OnExit = cxDBTextEditAllExit
            OnKeyDown = EnterKakoTab
            Width = 55
          end
          object cxlbl1: TcxLabel
            Left = 32
            Top = 42
            Caption = #1055#1086#1090#1088#1077#1073#1085#1086' '#1074#1088#1077#1084#1077
            Style.TextColor = clNavy
            Properties.WordWrap = True
            Transparent = True
            Width = 53
          end
          object cxDBLabel1: TcxDBLabel
            Left = 155
            Top = 49
            Hint = #1042#1088#1077#1084#1077#1085#1089#1082#1072' '#1084#1077#1088#1082#1072
            DataBinding.DataField = 'MERNA_EDINICA'
            DataBinding.DataSource = dm.dsNormativOperacija
            Transparent = True
            Height = 17
            Width = 38
          end
        end
      end
    end
  end
  inherited dxRibbon1: TdxRibbon
    Width = 1024
    ExplicitWidth = 1024
    inherited dxRibbon1Tab1: TdxRibbonTab
      Groups = <
        item
          ToolbarName = 'dxBarManager1Bar1'
        end
        item
          ToolbarName = 'dxBarManager1Bar5'
        end
        item
          Caption = #1054#1087#1077#1088#1072#1094#1080#1112#1072
          ToolbarName = 'dxBarManager1Bar7'
        end
        item
          Caption = #1056#1077#1074#1080#1079#1080#1112#1072
          ToolbarName = 'dxBarManager1Bar6'
        end
        item
          ToolbarName = 'dxBarManager1Bar2'
        end
        item
          ToolbarName = 'dxBarManager1Bar3'
        end>
      Index = 0
    end
    inherited dxRibbon1Tab2: TdxRibbonTab
      Index = 1
    end
  end
  inherited StatusBar1: TdxRibbonStatusBar
    Top = 718
    Width = 1024
    ExplicitTop = 718
    ExplicitWidth = 1024
  end
  object Panel1: TPanel [4]
    Left = 0
    Top = 503
    Width = 1024
    Height = 215
    Align = alBottom
    TabOrder = 4
    object cxPageControl1: TcxPageControl
      Left = 266
      Top = 1
      Width = 757
      Height = 213
      Align = alRight
      Anchors = [akLeft, akTop, akRight, akBottom]
      TabOrder = 0
      Properties.ActivePage = tsRevizija
      Properties.CustomButtons.Buttons = <>
      ClientRectBottom = 213
      ClientRectRight = 757
      ClientRectTop = 24
      object tsRevizija: TcxTabSheet
        Caption = #1056#1077#1074#1080#1079#1080#1112#1072
        ImageIndex = 1
        object PanelRevizija: TPanel
          Left = 0
          Top = 0
          Width = 757
          Height = 189
          Align = alClient
          TabOrder = 0
          object PanelRVnes: TPanel
            Left = 1
            Top = 1
            Width = 755
            Height = 76
            Align = alTop
            Enabled = False
            TabOrder = 0
            DesignSize = (
              755
              76)
            object cxLabel12: TcxLabel
              Left = 21
              Top = 14
              Caption = #1054#1087#1080#1089
              Style.TextColor = clRed
              Transparent = True
            end
            object txtNaziv: TcxDBTextEdit
              Tag = 1
              Left = 56
              Top = 12
              Hint = #1054#1087#1080#1089' '#1085#1072' '#1088#1077#1074#1080#1079#1080#1112#1072
              Anchors = [akLeft, akTop, akRight]
              DataBinding.DataField = 'NAZIV'
              DataBinding.DataSource = dm.dsNormativRevizija
              ParentFont = False
              Style.BorderStyle = ebsUltraFlat
              Style.Font.Charset = RUSSIAN_CHARSET
              Style.Font.Color = clWindowText
              Style.Font.Height = -11
              Style.Font.Name = 'Tahoma'
              Style.Font.Style = []
              Style.LookAndFeel.NativeStyle = False
              Style.LookAndFeel.SkinName = ''
              Style.TextStyle = []
              Style.TransparentBorder = False
              Style.IsFontAssigned = True
              StyleDisabled.LookAndFeel.NativeStyle = False
              StyleDisabled.LookAndFeel.SkinName = ''
              StyleFocused.LookAndFeel.NativeStyle = False
              StyleFocused.LookAndFeel.SkinName = ''
              StyleHot.LookAndFeel.NativeStyle = False
              StyleHot.LookAndFeel.SkinName = ''
              TabOrder = 0
              Width = 582
            end
            object txtDatum: TcxDBDateEdit
              Left = 56
              Top = 33
              Hint = #1044#1072#1090#1091#1084' '#1085#1072' '#1088#1077#1074#1080#1079#1080#1112#1072
              DataBinding.DataField = 'DATUM'
              DataBinding.DataSource = dm.dsNormativRevizija
              TabOrder = 1
              Width = 117
            end
            object cxLabel13: TcxLabel
              Left = 14
              Top = 37
              Caption = #1044#1072#1090#1091#1084
              Style.TextColor = clNavy
              Transparent = True
            end
            object cxButton1: TcxButton
              Left = 481
              Top = 45
              Width = 74
              Height = 20
              Action = aZapisiRevizija
              Anchors = [akRight, akBottom]
              Colors.Pressed = clGradientActiveCaption
              TabOrder = 4
            end
            object cxButton2: TcxButton
              Left = 562
              Top = 45
              Width = 74
              Height = 20
              Action = aOtkaziRevizija
              Anchors = [akRight, akBottom]
              Colors.Pressed = clGradientActiveCaption
              TabOrder = 5
            end
          end
          object PanelRGrid: TPanel
            Left = 1
            Top = 77
            Width = 755
            Height = 111
            Align = alClient
            TabOrder = 1
            object cxGrid3: TcxGrid
              Left = 1
              Top = 1
              Width = 753
              Height = 109
              Align = alClient
              TabOrder = 0
              object cxGrid3DBTableView1: TcxGridDBTableView
                Navigator.Buttons.CustomButtons = <>
                DataController.Summary.DefaultGroupSummaryItems = <>
                DataController.Summary.FooterSummaryItems = <>
                DataController.Summary.SummaryGroups = <>
              end
              object cxGrid3DBBandedTableView1: TcxGridDBBandedTableView
                Navigator.Buttons.CustomButtons = <>
                DataController.DataSource = dm.dsNormativRevizija
                DataController.Summary.DefaultGroupSummaryItems = <>
                DataController.Summary.FooterSummaryItems = <>
                DataController.Summary.SummaryGroups = <>
                Bands = <
                  item
                    Caption = #1056#1077#1074#1080#1079#1080#1112#1072
                    Width = 473
                  end>
                object cxGrid3DBBandedTableView1ID: TcxGridDBBandedColumn
                  Caption = #1064#1080#1092#1088#1072
                  DataBinding.FieldName = 'ID'
                  Visible = False
                  Position.BandIndex = 0
                  Position.ColIndex = 0
                  Position.RowIndex = 0
                end
                object cxGrid3DBBandedTableView1ID_NORMATIV: TcxGridDBBandedColumn
                  Caption = #1064#1080#1092#1088#1072' '#1085#1072' '#1085#1086#1088#1084#1072#1090#1080#1074
                  DataBinding.FieldName = 'ID_NORMATIV'
                  Visible = False
                  Position.BandIndex = 0
                  Position.ColIndex = 1
                  Position.RowIndex = 0
                end
                object cxGrid3DBBandedTableView1NAZIV: TcxGridDBBandedColumn
                  Caption = #1054#1087#1080#1089
                  DataBinding.FieldName = 'NAZIV'
                  Width = 380
                  Position.BandIndex = 0
                  Position.ColIndex = 2
                  Position.RowIndex = 0
                end
                object cxGrid3DBBandedTableView1DATUM: TcxGridDBBandedColumn
                  Caption = #1044#1072#1090#1091#1084
                  DataBinding.FieldName = 'DATUM'
                  Width = 93
                  Position.BandIndex = 0
                  Position.ColIndex = 3
                  Position.RowIndex = 0
                end
              end
              object cxGrid3Level1: TcxGridLevel
                GridView = cxGrid3DBBandedTableView1
              end
            end
          end
        end
      end
    end
    object tlNormativ: TcxDBTreeList
      Left = 1
      Top = 1
      Width = 265
      Height = 213
      Align = alClient
      Bands = <
        item
        end>
      DataController.DataSource = dm.dsNormativ
      DataController.ImageIndexField = 'NAZIV_ARTIKAL'
      DataController.KeyField = 'ID'
      DataController.StateIndexField = 'NAZIV_ARTIKAL'
      Navigator.Buttons.CustomButtons = <>
      OptionsData.Editing = False
      OptionsData.Deleting = False
      PopupMenu = PopupMenu2
      RootValue = -1
      TabOrder = 1
      OnChange = tlNormativChange
      OnClick = dxTreeClick
      ExplicitLeft = -3
      ExplicitTop = 6
      object cxDBTreeList1NAZIV_ARTIKAL: TcxDBTreeListColumn
        Caption.Text = #1040#1088#1090#1080#1082#1072#1083
        DataBinding.FieldName = 'NAZIV_ARTIKAL'
        Width = 194
        Position.ColIndex = 0
        Position.RowIndex = 0
        Position.BandIndex = 0
        Summary.FooterSummaryItems = <>
        Summary.GroupFooterSummaryItems = <>
      end
    end
  end
  object cxSplitter1: TcxSplitter [5]
    Left = 0
    Top = 495
    Width = 1024
    Height = 8
    HotZoneClassName = 'TcxMediaPlayer9Style'
    HotZone.SizePercent = 31
    AlignSplitter = salBottom
    Control = Panel1
  end
  inherited cxGridPopupMenu1: TcxGridPopupMenu
    Grid = cxGrid4
    Left = 48
    Top = 665
  end
  inherited PopupMenu1: TPopupMenu
    Left = 80
    Top = 697
    inherited N1: TMenuItem
      Visible = False
    end
    inherited Excel1: TMenuItem
      Visible = False
    end
    object N14: TMenuItem
      Caption = '-'
    end
  end
  inherited dxBarManager1: TdxBarManager
    Left = 80
    Top = 665
    PixelsPerInch = 96
    inherited dxBarManager1Bar1: TdxBar
      Caption = #1055#1088#1086#1080#1079#1074#1086#1076
      FloatClientWidth = 69
      FloatClientHeight = 162
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton25'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton1'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarLargeButton26'
        end>
    end
    inherited dxBarManager1Bar2: TdxBar
      DockedLeft = 751
      FloatClientWidth = 102
      FloatClientHeight = 166
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton10'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton11'
        end>
    end
    inherited dxBarManager1Bar3: TdxBar
      DockedLeft = 895
      FloatClientWidth = 59
      FloatClientHeight = 108
      UseOwnFont = True
    end
    inherited dxBarManager1Bar4: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 141
      FloatClientHeight = 221
    end
    inherited dxBarManager1BarTabela: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 174
      FloatClientHeight = 113
    end
    object dxBarManager1Bar5: TdxBar [5]
      Caption = #1057#1091#1088#1086#1074#1080#1085#1072
      CaptionButtons = <>
      DockedLeft = 220
      DockedTop = 0
      FloatLeft = 919
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton16'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton17'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton18'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar6: TdxBar [6]
      Caption = 'Custom 1'
      CaptionButtons = <>
      DockedLeft = 574
      DockedTop = 0
      FloatLeft = 1355
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton19'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton21'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton20'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar7: TdxBar [7]
      Caption = 'Custom 2'
      CaptionButtons = <>
      DockedLeft = 397
      DockedTop = 0
      FloatLeft = 1355
      FloatTop = 10
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton22'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton23'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton24'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarLargeButton10: TdxBarLargeButton
      Action = aAktiven
      Category = 0
      Visible = ivNever
    end
    object dxBarLargeButton11: TdxBarLargeButton
      Action = aNeaktiven
      Category = 0
      Visible = ivNever
    end
    object dxBarLargeButton16: TdxBarLargeButton
      Action = aNovaSurovina
      Caption = #1053#1086#1074#1072
      Category = 0
    end
    object dxBarLargeButton17: TdxBarLargeButton
      Action = aAzurirajSurovina
      Caption = #1040#1078#1091#1088#1080#1088#1072#1112
      Category = 0
    end
    object dxBarLargeButton18: TdxBarLargeButton
      Action = aBrisiSurovina
      Caption = #1041#1088#1080#1096#1080
      Category = 0
    end
    object dxBarLargeButton19: TdxBarLargeButton
      Action = aNovR
      Caption = #1053#1086#1074#1072
      Category = 0
    end
    object dxBarLargeButton20: TdxBarLargeButton
      Action = aBrisiR
      Caption = #1041#1088#1080#1096#1080
      Category = 0
    end
    object dxBarLargeButton21: TdxBarLargeButton
      Action = aAzurirajR
      Caption = #1040#1078#1091#1088#1080#1088#1072#1112
      Category = 0
    end
    object dxBarLargeButton22: TdxBarLargeButton
      Action = aNovaOperacija
      Caption = #1053#1086#1074#1072
      Category = 0
    end
    object dxBarLargeButton23: TdxBarLargeButton
      Action = aAzurirajOperacija
      Caption = #1040#1078#1091#1088#1080#1088#1072#1112
      Category = 0
    end
    object dxBarLargeButton24: TdxBarLargeButton
      Action = aBrisiOperacija
      Caption = #1041#1088#1080#1096#1080
      Category = 0
    end
    object dxBarLargeButton25: TdxBarLargeButton
      Action = aKopiraj
      Category = 0
      Hint = #1050#1086#1087#1080#1088#1072#1112' '#1087#1086#1089#1090#1086#1077#1095#1082#1080' '#1085#1086#1088#1084#1072#1090#1080#1074
    end
    object dxBarLargeButton26: TdxBarLargeButton
      Action = aSwitchView
      Category = 0
      Visible = ivNever
    end
    object dxBarLargeButton1: TdxBarLargeButton
      Action = aNov
      Category = 0
    end
  end
  inherited ActionList1: TActionList
    Left = 16
    Top = 665
    object aZapisiNormativ: TAction
      Caption = 'aZapisiNormativ'
      OnExecute = aZapisiNormativExecute
    end
    object aNovaSurovina: TAction
      Caption = #1053#1086#1074#1072' '#1089#1091#1088#1086#1074#1080#1085#1072
      ImageIndex = 10
      OnExecute = aNovaSurovinaExecute
    end
    object aAktiven: TAction
      Caption = #1040#1082#1090#1080#1074#1077#1085
      ImageIndex = 62
      OnExecute = aAktivenExecute
    end
    object aNeaktiven: TAction
      Caption = #1053#1077#1072#1082#1090#1080#1074#1077#1085
      ImageIndex = 61
      OnExecute = aNeaktivenExecute
    end
    object aAzurirajSurovina: TAction
      Caption = #1040#1078#1091#1088#1080#1088#1072#1112' '#1089#1091#1088#1086#1074#1080#1085#1072
      ImageIndex = 12
      OnExecute = aAzurirajSurovinaExecute
    end
    object aBrisiSurovina: TAction
      Caption = #1041#1088#1080#1096#1080' '#1089#1091#1088#1086#1074#1080#1085#1072
      ImageIndex = 13
      OnExecute = aBrisiSurovinaExecute
    end
    object aNovR: TAction
      Caption = #1053#1086#1074#1072' '#1088#1077#1074#1080#1079#1080#1112#1072
      ImageIndex = 10
      OnExecute = aNovRExecute
    end
    object aAzurirajR: TAction
      Caption = #1040#1078#1091#1088#1080#1088#1072#1112' '#1088#1077#1074#1080#1079#1080#1112#1072
      ImageIndex = 12
      OnExecute = aAzurirajRExecute
    end
    object aBrisiR: TAction
      Caption = #1041#1088#1080#1096#1080' '#1088#1077#1074#1080#1079#1080#1112#1072
      ImageIndex = 13
      OnExecute = aBrisiRExecute
    end
    object aZapisiRevizija: TAction
      Caption = #1047#1072#1087#1080#1096#1080
      ImageIndex = 6
      OnExecute = aZapisiRevizijaExecute
    end
    object aOtkaziRevizija: TAction
      Caption = #1054#1090#1082#1072#1078#1080
      ImageIndex = 21
      OnExecute = aOtkaziRevizijaExecute
    end
    object aVnesiNormativ: TAction
      Caption = #1042#1085#1077#1089#1080' '#1085#1086#1088#1084#1072#1090#1080#1074
      OnExecute = aVnesiNormativExecute
    end
    object aNovaOperacija: TAction
      Caption = #1053#1086#1074#1072' '#1086#1087#1077#1088#1072#1094#1080#1112#1072
      ImageIndex = 4
      OnExecute = aNovaOperacijaExecute
    end
    object aZapisiOperacija: TAction
      Caption = #1047#1072#1087#1080#1096#1080
      ImageIndex = 7
      OnExecute = aZapisiOperacijaExecute
    end
    object aOtkaziOperacija: TAction
      Caption = #1054#1090#1082#1072#1078#1080
      ImageIndex = 21
    end
    object aAzurirajOperacija: TAction
      Caption = #1040#1078#1091#1088#1080#1088#1072#1112' '#1086#1087#1077#1088#1072#1094#1080#1112#1072
      ImageIndex = 12
      OnExecute = aAzurirajOperacijaExecute
    end
    object aBrisiOperacija: TAction
      Caption = #1041#1088#1080#1096#1080' '#1086#1087#1077#1088#1072#1094#1080#1112#1072
      ImageIndex = 13
      OnExecute = aBrisiOperacijaExecute
    end
    object aArtikal: TAction
      Caption = #1040#1088#1090#1080#1082#1072#1083
      ShortCut = 45
      OnExecute = aArtikalExecute
    end
    object aKopiraj: TAction
      Caption = #1050#1086#1087#1080#1088#1072#1112' '#1087#1086#1089#1090#1086#1077#1095#1082#1080
      ImageIndex = 18
      OnExecute = aKopirajExecute
    end
    object aSwitchView: TAction
      Caption = #1057#1090#1077#1073#1083#1086'/'#1043#1088#1080#1076
      ImageIndex = 14
      OnExecute = aSwitchViewExecute
    end
    object aFilter: TAction
      Caption = 'aFilter'
      OnExecute = aFilterExecute
    end
  end
  inherited dxComponentPrinter1: TdxComponentPrinter
    Left = 144
    Top = 665
    inherited dxComponentPrinter1Link1: TdxGridReportLink
      ReportDocument.CreationDate = 41316.640212766200000000
      BuiltInReportLink = True
    end
  end
  inherited dxBarScreenTipRepository1: TdxBarScreenTipRepository
    Left = 112
    Top = 665
    PixelsPerInch = 96
  end
  object qPoteklo: TpFIBQuery
    Transaction = dmKon.qTransaction
    Database = dmKon.fibBaza
    SQL.Strings = (
      'select poteklo '
      'from man_normativ n '
      'where n.id like :koren ')
    Left = 16
    Top = 697
    qoAutoCommit = True
    qoStartTransaction = True
  end
  object PopupMenu2: TPopupMenu
    Left = 48
    Top = 697
    object N2: TMenuItem
      Action = aVnesiNormativ
      Caption = #1042#1085#1077#1089#1080'/'#1086#1079#1085#1072#1095#1080' '#1085#1086#1088#1084#1072#1090#1080#1074
    end
    object N11: TMenuItem
      Caption = '-'
    end
    object N13: TMenuItem
      Action = aNov
      Caption = #1053#1086#1074' '#1085#1086#1088#1084#1072#1090#1080#1074
    end
    object N12: TMenuItem
      Action = aKopiraj
      Caption = #1050#1086#1087#1080#1088#1072#1112' '#1087#1086#1089#1090#1086#1077#1095#1082#1080' '#1085#1086#1088#1084#1072#1090#1080#1074
    end
    object N15: TMenuItem
      Caption = '-'
    end
    object N16: TMenuItem
      Action = aAzuriraj
      Caption = #1040#1078#1091#1088#1080#1088#1072#1112' '#1085#1086#1088#1084#1072#1090#1080#1074
    end
    object N17: TMenuItem
      Action = aBrisi
      Caption = #1041#1088#1080#1096#1080' '#1085#1086#1088#1084#1072#1090#1080#1074
    end
    object N4: TMenuItem
      Caption = '-'
    end
    object N3: TMenuItem
      Action = aNovaSurovina
    end
    object N5: TMenuItem
      Action = aAzurirajSurovina
    end
    object N6: TMenuItem
      Action = aBrisiSurovina
    end
    object N7: TMenuItem
      Caption = '-'
    end
    object N8: TMenuItem
      Action = aNovaOperacija
    end
    object N9: TMenuItem
      Action = aAzurirajOperacija
    end
    object N10: TMenuItem
      Action = aBrisiOperacija
    end
  end
end
