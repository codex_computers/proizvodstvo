unit EvidencijaSerijaKolicini;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData, cxEdit,
  DB, cxDBData, cxGridCustomPopupMenu, cxGridPopupMenu,
  ComCtrls, cxContainer, cxTextEdit, cxDBEdit, StdCtrls, cxGridLevel,
  cxClasses, cxControls, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGrid, ExtCtrls, Menus, ActnList,
  cxGridExportLink, cxExport, cxLookAndFeelPainters, cxDataStorage,
  cxButtons, dxStatusBar, dxRibbonStatusBar, dxRibbon, dxBar, dxBarExtItems,
  dxPSGlbl, dxPSUtl, dxPSEngn, dxPrnPg, dxBkgnd, dxWrap, dxPrnDev,
  dxPSCompsProvider, dxPSFillPatterns, dxPSEdgePatterns, dxPSCore, dxPScxCommon,
  cxPC, cxMemo, cxLocalization, cxLookAndFeels,
  dxPSPDFExportCore, dxPSPDFExport, cxDrawTextUtils, dxPSPrVwStd, dxPSPrVwAdv,
  dxPSPrVwRibbon, dxPScxEditorProducers, dxPScxExtEditorProducers,
  dxPScxPageControlProducer, dxPgsDlg, cxCheckBox, cxBarEditItem, dxSkinsCore,
  dxSkinBlack, dxSkinBlue, dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom,
  dxSkinDarkSide, dxSkinFoggy, dxSkinGlassOceans, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven,
  dxSkinSharp, dxSkinSilver, dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008,
  dxSkinsDefaultPainters, dxSkinValentine, dxSkinXmas2008Blue,
  dxSkinsdxRibbonPainter, dxSkinsdxBarPainter, dxSkinscxPCPainter,
  dxBarSkinnedCustForm, cxDropDownEdit, cxCalendar, cxDBLookupComboBox,
  cxMaskEdit, cxLookupEdit, cxDBLookupEdit, cxDBExtLookupComboBox, cxGroupBox,
  cxRadioGroup, dxRibbonSkins, dxPScxGridLnk, dxPScxGridLayoutViewLnk,
  dxScreenTip, dxSkinBlueprint, dxSkinDevExpressDarkStyle,
  dxSkinDevExpressStyle, dxSkinHighContrast, dxSkinSevenClassic,
  dxSkinSharpPlus, dxSkinTheAsphaltWorld, dxSkinVS2010, dxSkinWhiteprint, dmResources,
  FIBDataSet, pFIBDataSet, cxCurrencyEdit, FIBQuery, pFIBQuery, pFIBStoredProc,
  dxPScxPivotGridLnk, dxSkinOffice2013White, cxNavigator, System.Actions;

type
//  niza = Array[1..5] of Variant;

  TfrmEvidencijaSerijaKolicini = class(TForm)
    cxGridPopupMenu1: TcxGridPopupMenu;
    PopupMenu1: TPopupMenu;
    N1: TMenuItem;
    Excel1: TMenuItem;
    dxRibbon1Tab1: TdxRibbonTab;
    dxRibbon1: TdxRibbon;
    StatusBar1: TdxRibbonStatusBar;
    dxBarManager1: TdxBarManager;
    dxBarManager1Bar1: TdxBar;
    dxBarManager1Bar2: TdxBar;
    dxBarManager1Bar3: TdxBar;
    dxBarLargeButton2: TdxBarLargeButton;
    dxBarLargeButton3: TdxBarLargeButton;
    dxBarLargeButton4: TdxBarLargeButton;
    dxBarLargeButton5: TdxBarLargeButton;
    dxBarLargeButton6: TdxBarLargeButton;
    dxBarLargeButton7: TdxBarLargeButton;
    dxBarLargeButton8: TdxBarLargeButton;
    dxBarLargeButton1: TdxBarLargeButton;
    ActionList1: TActionList;
    aNov: TAction;
    aAzuriraj: TAction;
    aBrisi: TAction;
    aSnimiIzgled: TAction;
    aZacuvajExcel: TAction;
    aPecatiTabela: TAction;
    aHelp: TAction;
    aIzlez: TAction;
    aZapisi: TAction;
    aOtkazi: TAction;
    aRefresh: TAction;
    dxRibbon1Tab2: TdxRibbonTab;
    dxBarManager1Bar4: TdxBar;
    dxBarLargeButton9: TdxBarLargeButton;
    aSnimiPecatenje: TAction;
    dxComponentPrinter1: TdxComponentPrinter;
    dxComponentPrinter1Link1: TdxGridReportLink;
    dxBarLargeButton11: TdxBarLargeButton;
    ribbonFilterReset: TdxBarLargeButton;
    aPodesuvanjePecatenje: TAction;
    dxBarLargeButton12: TdxBarLargeButton;
    aBrisiPodesuvanjePecatenje: TAction;
    dxBarLargeButton13: TdxBarLargeButton;
    dxBarLargeButton14: TdxBarLargeButton;
    aPageSetup: TAction;
    dxBarLargeButton15: TdxBarLargeButton;
    dxBarLargeButton16: TdxBarLargeButton;
    dxBarLargeButton17: TdxBarLargeButton;
    cxBarEditItem1: TcxBarEditItem;
    dxBarSubItem1: TdxBarSubItem;
    aFormConfig: TAction;
    dxBarScreenTipRepository1: TdxBarScreenTipRepository;
    tipSnimiIzgled: TdxBarScreenTip;
    tipZacuvajExcel: TdxBarScreenTip;
    tipDizajnReport: TdxBarScreenTip;
    tipSetiranjeStrana: TdxBarScreenTip;
    tipSnimiKonf: TdxBarScreenTip;
    tipBrisiKonf: TdxBarScreenTip;
    dxBarManager1BarIzgledGrid: TdxBar;
    dxBarLBtnSnimiIzgled: TdxBarLargeButton;
    dxBarLBtnBrisiIzgled: TdxBarLargeButton;
    aBrisiIzgled: TAction;
    aSpustiSoberi: TAction;
    dxBarLargeButton10: TdxBarLargeButton;
    dxBarLargeButton18: TdxBarLargeButton;
    dxBarLargeButton19: TdxBarLargeButton;
    Panel1: TPanel;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    tblArtikalSerija: TpFIBDataSet;
    dsArtikalSerija: TDataSource;
    tblArtikalSerijaO_VLEZ_KOLICINA: TFIBFloatField;
    tblArtikalSerijaO_IZLEZ_KOLICINA: TFIBFloatField;
    tblArtikalSerijaO_OSTATOK: TFIBFloatField;
    Panel2: TPanel;
    lblSelekcijaSuma: TLabel;
    lblStorn: TLabel;
    SumaSelekcija: TcxCurrencyEdit;
    Label1: TLabel;
    VnesenaKolicina: TcxCurrencyEdit;
    aSoberiSelektirani: TAction;
    spGenerirajInOutSerija: TpFIBStoredProc;
    Panel3: TPanel;
    Label3: TLabel;
    ZapisiButton: TcxButton;
    OtkaziButton: TcxButton;
    tblArtikalSerijaO_LOT_NO: TFIBStringField;
    cxGrid1DBTableView1O_LOT_NO: TcxGridDBColumn;
    cxGrid1DBTableView1O_VLEZ_KOLICINA: TcxGridDBColumn;
    cxGrid1DBTableView1O_IZLEZ_KOLICINA: TcxGridDBColumn;
    cxGrid1DBTableView1O_OSTATOK: TcxGridDBColumn;
    UPOTREBENO: TcxGridDBColumn;

    procedure cxDBTextEditAllEnter(Sender: TObject);
    procedure cxDBTextEditAllExit(Sender: TObject);
    procedure EnterKakoTab(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    //procedure PrvPosledenTab(panel:TPanel;var posledna,prva:TWinControl);
    procedure SaveToIniFileExecute(Sender: TObject);
    procedure cxGrid1DBTableView1KeyPress(Sender: TObject; var Key: Char);
    procedure aZapisiExecute(Sender: TObject);
    procedure aOtkaziExecute(Sender: TObject);
    procedure aNovExecute(Sender: TObject);
    procedure aAzurirajExecute(Sender: TObject);
    procedure aBrisiExecute(Sender: TObject);
    procedure aZacuvajExcelExecute(Sender: TObject);
    procedure aRefreshExecute(Sender: TObject);
    procedure aSnimiIzgledExecute(Sender: TObject);
    procedure aIzlezExecute(Sender: TObject);
    procedure aPecatiTabelaExecute(Sender: TObject);
    procedure aSnimiPecatenjeExecute(Sender: TObject);
    procedure zacuvajPrintVoIni(ime:AnsiString);
    procedure procitajPrintOdIni(ime:AnsiString);
    procedure aPodesuvanjePecatenjeExecute(Sender: TObject);
    procedure aBrisiPodesuvanjePecatenjeExecute(Sender: TObject);
    procedure aPageSetupExecute(Sender: TObject);
    procedure aFormConfigExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure PartnerPropertiesChange(Sender: TObject);
    procedure aBrisiIzgledExecute(Sender: TObject);
    procedure aSpustiSoberiExecute(Sender: TObject);
    function initParams(re : integer; artvid : integer; artsif : integer; kolicina : double; do_datum : TDateTime) : integer;
    procedure aSoberiSelektiraniExecute(Sender: TObject);
    procedure cxGrid1DBTableView1FocusedRecordChanged(
      Sender: TcxCustomGridTableView; APrevFocusedRecord,
      AFocusedRecord: TcxCustomGridRecord;
      ANewItemRecordFocusingChanged: Boolean);
    procedure GenerirajInOut(in_out_id : Int64);
    procedure cxGrid1DBTableView1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure cxGrid1DBTableView1EditKeyDown(Sender: TcxCustomGridTableView;
      AItem: TcxCustomGridTableItem; AEdit: TcxCustomEdit; var Key: Word;
      Shift: TShiftState);
    procedure tblArtikalSerijaBeforeOpen(DataSet: TDataSet);
    procedure UPOTREBENOSetStoredPropertyValue(Sender: TcxCustomGridTableItem;
      const AName: string; const AValue: Variant);

  private
    { Private declarations }

    _sifra_kluc :array[1..10] of string;  // �������� ���������� �� ������ �� ������� �� ������������� ���

  protected
    list : TList;
    inserting, sobrano : boolean;
    priemi_id, priemi_kolicini : AnsiString;
    priemi_br : integer;

    prva, posledna :TWinControl;
    procedure prefrli;

  public
    { Public declarations }
    in_re, in_artvid, in_artsif : integer;
    in_kolicina : double;
    in_do_datum : TDateTime;
    uspesno : boolean;
    bez_prikaz : boolean;

    constructor Create(Owner : TComponent; insert : boolean = true);reintroduce; overload;

    procedure SetSifra(br: integer; s : string);
    function GetSifra(br: integer) : string;

    //������� �� ���������� ������������ _sifra_kluc
    property sifra_kluc[br :integer]: string read GetSifra write SetSifra;
  end;

var
  frmEvidencijaSerijaKolicini: TfrmEvidencijaSerijaKolicini;
  rData : TRepositoryData;

implementation

uses DaNe, dmKonekcija, Utils, FormConfig, dmUnit;

{$R *.dfm}
//------------------------------------------------------------------------------

constructor TfrmEvidencijaSerijaKolicini.Create(Owner : TComponent; insert : boolean = true);
begin
inherited Create(Owner);
//	�������� ���� ������� � �������� �� ����������� ���������
//	insert = true -> ����� ���� ����� ���������� �� ������� �� Insert Mode
//  insert = false -> ����� ���� ����� ������� �� ����� �� ������. �� ������� ����� ����� �� �� �������� F5
  inserting := insert;
end;

//	����� �� �����������. �� ��������� ������� �� ������, �� �������� ������� �� ���� ��������,
//	�� ������� ������� �� ������ ������ � �� ������� �� Insert Mode
procedure TfrmEvidencijaSerijaKolicini.aNovExecute(Sender: TObject);
begin
//  if(cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) then
//  begin
//    dPanel.Enabled:=True;
//    lPanel.Enabled:=False;
//    prva.SetFocus;
//    cxGrid1DBTableView1.DataController.DataSet.Insert;
//  end
//  else ShowMessage('������ �������� �� ��� �������� �� �� ��������� �������,' + sLineBreak + '� ����� ������� �� ������� ��� �����!');
end;

//	����� �� ���������. �� ��������� ������� �� ������, �� �������� ������� �� ���� ��������,
//	�� ������� ������� �� ������ ������ � �� ������� �� Edit Mode
procedure TfrmEvidencijaSerijaKolicini.aAzurirajExecute(Sender: TObject);
begin
//  if(cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) then
//  begin
//    dPanel.Enabled:=True;
//    lPanel.Enabled:=False;
//    prva.SetFocus;
//    cxGrid1DBTableView1.DataController.DataSet.Edit;
//  end
//  else ShowMessage('������ �������� �� ��� �������� �� �� ��������� �������,' + sLineBreak + '� ����� ������� �� ������� ��� �����!');
end;

//	����� �� ������ �� ������������� �����
procedure TfrmEvidencijaSerijaKolicini.aBrisiExecute(Sender: TObject);
begin
//  if ((cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) and
//     (cxGrid1DBTableView1.DataController.RecordCount <> 0)) then
//    cxGrid1DBTableView1.DataController.DataSet.Delete();
end;

procedure TfrmEvidencijaSerijaKolicini.aBrisiIzgledExecute(Sender: TObject);
begin
  brisiGridVoBaza(Name,cxGrid1DBTableView1);
  //brisiPivotVoBaza(Name,cxDBPivotGrid1); �� pivot ��� ���
  BrisiFormaIzgled(self);
end;

//	����� �� ���������� �� ����������
procedure TfrmEvidencijaSerijaKolicini.aRefreshExecute(Sender: TObject);
begin
//  cxGrid1DBTableView1.DataController.Filter.Root.Clear; //���������� �� ��������
//  cxGrid1DBTableView1.DataController.DataSet.Refresh;
end;

//	����� �� ����� �� ������� �� ������� �� ������� �����
procedure TfrmEvidencijaSerijaKolicini.aIzlezExecute(Sender: TObject);
begin
  Close;
end;

//	����� �� ������� �� �������� �� ������ �� ���� (Utils.pas)
procedure TfrmEvidencijaSerijaKolicini.aSnimiIzgledExecute(Sender: TObject);
begin
  zacuvajGridVoBaza(Name,cxGrid1DBTableView1);
  ZacuvajFormaIzgled(self);
end;

//	����� �� ������� �� ������ �� Excel ������ (Utils.pas)
procedure TfrmEvidencijaSerijaKolicini.aZacuvajExcelExecute(Sender: TObject);
begin
//  zacuvajVoExcel(cxGrid1, Caption);
end;

//  ��������� �� ������ �� ������� �� ���������� �� ����� �� Enter
procedure TfrmEvidencijaSerijaKolicini.EnterKakoTab(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  kom : TWinControl;
begin
    kom := Sender as TWinControl;
    case Key of
        VK_DOWN:
        begin
          PostMessage(Handle,WM_NextDlgCtl,0,0);
          key:=0;
        end;
        VK_UP:
        begin
          PostMessage(Handle,WM_NextDlgCtl,1,0);
          key:=0;
        end;
        VK_RETURN:
        begin
          PostMessage(Handle,WM_NextDlgCtl,0,0);
        end;
        VK_INSERT:
        begin
          //  �� ����� ��������
//          if (kom = cxExtLookupComboBox1)  then
//          begin
//            frmNurkoRepository := TfrmNurkoRepository.Create(nil,Name,rData);
//            frmNurkoRepository.kontrola_naziv := kom.Name;
//            frmNurkoRepository.ShowModal;
//
//            if (frmNurkoRepository.ModalResult = mrOk) then
//              cxExtLookupComboBox1.EditValue := frmNurkoRepository.cxGrid1Level1.GridView.DataController.GetRecordId(frmNurkoRepository.cxGrid1Level1.GridView.DataController.FocusedRecordIndex);
//
//            frmNurkoRepository.Free;
//       	 end;
	end;
    end;
end;

//	������� �� ��� �� ������ ��� �������� �� �����
procedure TfrmEvidencijaSerijaKolicini.cxDBTextEditAllEnter(Sender: TObject);
begin
    TEdit(Sender).Color:=clSkyBlue;
end;

//	������� �� ��� �� ������ ��� ����� �� �����
procedure TfrmEvidencijaSerijaKolicini.cxDBTextEditAllExit(Sender: TObject);
begin
    TEdit(Sender).Color:=clWhite;
end;

function TfrmEvidencijaSerijaKolicini.initParams(re : integer; artvid : integer; artsif : integer; kolicina : double; do_datum : TDateTime) : integer;
begin
  in_re := re;
  in_artvid := artvid;
  in_artsif := artsif;
  in_kolicina := kolicina;
  in_do_datum := do_datum;

  tblArtikalSerija.Close;
  tblArtikalSerija.ParamByName('re').Value := in_re;
  tblArtikalSerija.ParamByName('art_vid').Value := in_artvid;
  tblArtikalSerija.ParamByName('art_sif').Value := in_artsif;
  tblArtikalSerija.ParamByName('IN_DO_DATUM').Value := in_do_datum;
  tblArtikalSerija.Open;

  tblArtikalSerija.FetchAll;
  result := tblArtikalSerija.RecordCount;
end;

procedure TfrmEvidencijaSerijaKolicini.cxGrid1DBTableView1EditKeyDown(
  Sender: TcxCustomGridTableView; AItem: TcxCustomGridTableItem;
  AEdit: TcxCustomEdit; var Key: Word; Shift: TShiftState);
begin
  if (Key = VK_RETURN) then
  begin
    if cxGrid1DBTableView1.Controller.FocusedRow.IsLast then
    begin
      ZapisiButton.SetFocus;
      aSoberiSelektirani.Execute;
    end;
  end;
end;

procedure TfrmEvidencijaSerijaKolicini.cxGrid1DBTableView1FocusedRecordChanged(
  Sender: TcxCustomGridTableView; APrevFocusedRecord,
  AFocusedRecord: TcxCustomGridRecord; ANewItemRecordFocusingChanged: Boolean);
begin
  aSoberiSelektirani.Execute;
end;

//  ��������� �� ������ �� ��������� �� ���������� ���� �� ��������
//  ��������� ��: br - ����� ��� �� ������ (�� 0 �� 9) �� ��������� �� ����� �� ����� �� ���������� ����
//  s - ��������� �� ������ �� ������ (���������� �� string �� ����� �� integer �� IntToStr ���������)
procedure TfrmEvidencijaSerijaKolicini.SetSifra(br: integer; s : string);
begin
  _sifra_kluc[br] := s;
end;

procedure TfrmEvidencijaSerijaKolicini.tblArtikalSerijaBeforeOpen(
  DataSet: TDataSet);
begin
  tblArtikalSerija.ParamByName('IN_GOD').Value := godina;
end;

procedure TfrmEvidencijaSerijaKolicini.UPOTREBENOSetStoredPropertyValue(
  Sender: TcxCustomGridTableItem; const AName: string; const AValue: Variant);
begin

end;

//  ������� �� ������ �� ��������� �� ���������� ���� �� ��������
//  ��������� ��: br - ����� ��� �� ������ (�� 0 �� 9) �� ��������� �� ����� �� ����� �� ���������� ����
function TfrmEvidencijaSerijaKolicini.GetSifra(br: integer) : string;
begin
  Result := _sifra_kluc[br];
end;

procedure TfrmEvidencijaSerijaKolicini.PartnerPropertiesChange(Sender: TObject);
begin

end;

//  �� ������� �� ������� ��� ����������� �� Master-�� �� �������� �� ��������� �� ������ ����� SetSifra
procedure TfrmEvidencijaSerijaKolicini.prefrli;
begin
end;

procedure TfrmEvidencijaSerijaKolicini.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    //	��������� ��� ��������� �� �������
//    if (cxGrid1DBTableView1.DataController.DataSource.State = dsEdit) or (cxGrid1DBTableView1.DataController.DataSource.State = dsInsert) then
//    begin
//        frmDaNe := TfrmDaNe.Create(self, '���������� ��������', '���������� �� �� ��������. ���� ������ �� �� �������?', 1);
//        if (frmDaNe.ShowModal <> mrYes) then
//        begin
//            cxGrid1DBTableView1.DataController.DataSet.Cancel;
//            Action := caFree;
//        end
//        else
//          if (Validacija(dPanel) = false) then
//          begin
//            cxGrid1DBTableView1.DataController.DataSet.Post;
//            Action := caFree;
//          end
//          else Action := caNone;
//    end;

  dmRes.FreeRepository(rData);
end;

procedure TfrmEvidencijaSerijaKolicini.FormCreate(Sender: TObject);
begin
  ProcitajFormaIzgled(self);
  if (dmRes.use_skin = true) then dxRibbon1.ColorSchemeName := dmRes.skin_name;

  rData := TRepositoryData.Create();  // kreirame instance od klasata TRepositoryData sto se naogja vo dmRes

  uspesno := false;
  bez_prikaz := true;
end;

//------------------------------------------------------------------------------

procedure TfrmEvidencijaSerijaKolicini.FormShow(Sender: TObject);
begin
  //  ������� �� ������������ ������������/���������� �� ���������� �� ���� �����
    SpremiForma(self);
  //	�� ��������� �� ������� ������ ��� ���������� � ���� � ��� �������� �� �������
//    PrvPosledenTab(dPanel,posledna,prva);
//    dxBarManager1Bar1.Caption := Caption;
//    dxComponentPrinter1Link1.ReportTitle.Text := Caption;

  //	������� �� ������������ �� ������ �� ������
    procitajGridOdBaza(Name,cxGrid1DBTableView1,false,false);
  //	������� �� ������������ �� ��������� �� ������
    procitajPrintOdBaza(Name,cxGrid1DBTableView1.Name, dxComponentPrinter1Link1);
    sobrano := true;

//  �������� �� ����� �� ������ �� cxExtLookupComboBox ��� cxDBExtLookupComboBox ��������
    //cxExtLookupComboBox1.RepositoryItem := dmRes.InitRepository(23, 'cxExtLookupComboBox1', Name, rData);
  	//procitajGridOdBaza(Name + '_' + IntToStr(rData.nurkoRepository[rData.br_repository]), rData.gridRepository[rData.br_repository] ,false,false);

    VnesenaKolicina.EditValue := in_kolicina;

    bez_prikaz := false;

    cxGrid1DBTableView1.DataController.GotoFirst;
    cxGrid1.SetFocus;
    UPOTREBENO.Focused := true;
    PostKeyEx32(VK_RETURN, [], False);
end;
//------------------------------------------------------------------------------

procedure TfrmEvidencijaSerijaKolicini.SaveToIniFileExecute(Sender: TObject);
begin
//    cxGrid1DBTableView1.StoreToIniFile(Name,True,[]);
end;
//------------------------------------------------------------------------------

procedure TfrmEvidencijaSerijaKolicini.cxGrid1DBTableView1KeyDown(
  Sender: TObject; var Key: Word; Shift: TShiftState);
begin
  if ((Key = VK_RETURN) and (cxGrid1DBTableView1.DataController.IsEOF)) then
  begin
    cxGrid1DBTableView1.DataController.GotoFirst;
  end
  else
  begin
   cxGrid1DBTableView1.DataController.GotoNext;
  end;
end;

procedure TfrmEvidencijaSerijaKolicini.cxGrid1DBTableView1KeyPress(Sender: TObject;
  var Key: Char);
begin
//  if(Ord(Key) <> VK_RETURN) then cxSortiraj(cxGrid1DBTableView1);
end;

//  ����� �� �����
procedure TfrmEvidencijaSerijaKolicini.aZapisiExecute(Sender: TObject);
var
  st: TDataSetState;
  oznaceno, kraj : bool;
  momentalen : integer;
begin
  // ako se povikuva Zapisi bez formata t.e za edna serija
  if (bez_prikaz = true) then
  begin
    if (tblArtikalSerijaO_OSTATOK.Value < in_kolicina) then
    begin
      // ako ostatokot vo serijata e pomal od vnesenata kolicina
      uspesno := false;
      ShowMessage('������! ��������� �������� � �������� �� �������� ��� �������� : ' + FloatToStr(tblArtikalSerijaO_OSTATOK.Value));
    end
    else
    begin
      priemi_id := '';
      priemi_kolicini := '';
      priemi_id := tblArtikalSerijaO_LOT_NO.AsString;
      priemi_kolicini := FloatToStr(in_kolicina);
      priemi_br := 1;
      uspesno := true;
      Close;
    end;
  end
  else
  // ako e povikana formata i e racno vnesena kolicinata po serii
  begin
    aSoberiSelektirani.Execute;

    if (VnesenaKolicina.EditValue <> SumaSelekcija.EditValue) then
    begin
      ShowMessage('������! ��������� � ��������� �������� �� �� ����!');
      uspesno := false;
    end
    else
    begin
      priemi_id := '';
      priemi_kolicini := '';
      momentalen := 1;
      priemi_br := 0;

      cxGrid1DBTableView1.Controller.GoToFirst(true);

      if tblArtikalSerija.IsEmpty then kraj := true
      else kraj := false;

      while (kraj = false) do
      begin
        if (UPOTREBENO.EditValue > 0) then
        begin
          if priemi_id <> '' then
            priemi_id := priemi_id + ';' + tblArtikalSerijaO_LOT_NO.AsString
          else
            priemi_id := tblArtikalSerijaO_LOT_NO.AsString;

          if priemi_kolicini <> '' then
            priemi_kolicini := priemi_kolicini + ';' + FloatToStr(UPOTREBENO.EditValue)
          else
            priemi_kolicini := FloatToStr(UPOTREBENO.EditValue);

          priemi_br := priemi_br + 1;
        end;

        kraj := cxGrid1DBTableView1.Controller.IsFinish;
        cxGrid1DBTableView1.Controller.GoToNext(true,true);
      end;
      uspesno := true;
      Close;
    end;
  end;
end;

//	����� �� ���������� �� �������
procedure TfrmEvidencijaSerijaKolicini.aOtkaziExecute(Sender: TObject);
begin
//  if (cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) then
//  begin
//      ModalResult := mrCancel;
      uspesno := false;
      Close();
//  end
//  else
//  begin
//      cxGrid1DBTableView1.DataController.DataSet.Cancel;
//      RestoreControls(dPanel);
//      dPanel.Enabled := false;
//      lPanel.Enabled := true;
//      cxGrid1.SetFocus;
//  end;
end;

//----------------------------------------------------------------------------------
// ��������� �� ��������, ���������� �� ��������� � �������� �� Print ��������
//----------------------------------------------------------------------------------

//	����� �� �������� �� �������� �� �������
procedure TfrmEvidencijaSerijaKolicini.aPageSetupExecute(Sender: TObject);
begin
  dxComponentPrinter1Link1.PageSetup;
end;

//	����� �� ������� �� ������
procedure TfrmEvidencijaSerijaKolicini.aPecatiTabelaExecute(Sender: TObject);
begin
  dxComponentPrinter1Link1.ReportTitle.Text := Caption;

  dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Clear;
  dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Add(dmkon.firma_naziv);
  dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Add(DateTimeToStr(Now));

  dxComponentPrinter1Link1.PrinterPage.PageHeader.LeftTitle.Clear;
  //dxComponentPrinter1Link1.PrinterPage.PageHeader.LeftTitle.Add('������� : ' + TipSifra.Text + '-' + IdSifra.Text + ' ' + Partner.Text);

  dxComponentPrinter1.Preview(true, dxComponentPrinter1Link1);
end;

//	����� �� ���������� �� ��������� �� �������
procedure TfrmEvidencijaSerijaKolicini.aPodesuvanjePecatenjeExecute(Sender: TObject);
begin
  dxComponentPrinter1Link1.DesignReport();
end;

procedure TfrmEvidencijaSerijaKolicini.aSnimiPecatenjeExecute(Sender: TObject);
begin
  zacuvajPrintVoBaza(Name,cxGrid1DBTableView1.Name,dxComponentPrinter1Link1);
end;


procedure TfrmEvidencijaSerijaKolicini.aSoberiSelektiraniExecute(
  Sender: TObject);
var
    suma : double;
    i:integer;
begin
    //cxGrid1DBTableView1.Controller.FocusNextCell(true);
    suma := 0;
    with cxGrid1DBTableView1.DataController do
      for I := 0 to RecordCount - 1 do
      begin
        if (DisplayTexts[i,Upotrebeno.Index] <> '') then
        begin
            if (values[i,Upotrebeno.Index] > values[i,cxGrid1DBTableView1O_OSTATOK.Index]) then
            begin
              ShowMessage('�������� ������� ��� �� ���������, ���������� � ��������!');
              values[i,Upotrebeno.Index] := values[i,cxGrid1DBTableView1O_OSTATOK.Index];
              //UPOTREBENO.EditValue := tblArtikalSerijaO_OSTATOK.Value;
            end;

            suma := suma + values[i,Upotrebeno.Index];
        end;

      end;
    SumaSelekcija.EditValue := suma;

    if (SumaSelekcija.EditValue = VnesenaKolicina.EditValue) then lblSelekcijaSuma.Font.Color := clGreen
    else lblSelekcijaSuma.Font.Color := clRed;
end;

//	����� �� �������� ��� �������� �� �������� �� ������ ���� ��� ��������� �� ����� ������
procedure TfrmEvidencijaSerijaKolicini.aSpustiSoberiExecute(Sender: TObject);
begin
  if (sobrano = true) then
  begin
    //cxGrid1DBTableView1.ViewData.Expand(false);
    sobrano := false;
  end
  else
  begin
    //cxGrid1DBTableView1.ViewData.Collapse(false);
    sobrano := true;
  end;
end;

//	����� �� ������ �� ����������� �������� �� ������� (Utils.pas)
procedure TfrmEvidencijaSerijaKolicini.aBrisiPodesuvanjePecatenjeExecute(Sender: TObject);
begin
  brisiPrintOdBaza(Name,cxGrid1DBTableView1.Name, dxComponentPrinter1Link1);
end;

//  ����� �� ����� �� ����� �� ������������� �� �������� � ��������������� ��
//  �������(������ ���������� ��������)
procedure TfrmEvidencijaSerijaKolicini.aFormConfigExecute(Sender: TObject);
begin
  frmFormConfig := TfrmFormConfig.Create(Application);
  frmFormConfig.formPtr := Addr(Self);
  frmFormConfig.ShowModal;
  frmFormConfig.Free;
end;

procedure TfrmEvidencijaSerijaKolicini.GenerirajInOut(in_out_id : Int64);
begin
  spGenerirajInOutSerija.Close;
  spGenerirajInOutSerija.ParamByName('IN_OUT_S_ID').Value := in_out_id;
  spGenerirajInOutSerija.ParamByName('IN_PRIEMI_ID').Value := priemi_id;
  spGenerirajInOutSerija.ParamByName('IN_PRIEMI_KOLICINA').Value := priemi_kolicini;
  spGenerirajInOutSerija.ParamByName('IN_PRIEMI_BR').Value := priemi_br;
  spGenerirajInOutSerija.ExecProc;
end;

//----------------------------------------------------------------------------------
// ���� -> ��������� �� ��������, ���������� �� ��������� � �������� �� Print ��������
//----------------------------------------------------------------------------------

// ��������� �� ���������� �� ��������� � �������� �� Print �������� �� .ini ��������
procedure TfrmEvidencijaSerijaKolicini.zacuvajPrintVoIni(ime:AnsiString);
var
  AStream: TFileStream;
begin
  AStream := TFileStream.Create(dmKon.RabotenDir + ime + 'Print.ini', fmCreate);
  try
    AStream.WriteComponent(dxComponentPrinter1Link1);
  finally
    AStream.Free;
  end;

end;

// ��������� �� ������ �� ��������� � �������� �� Print �������� �������� �� .ini ��������
procedure TfrmEvidencijaSerijaKolicini.procitajPrintOdIni(ime:AnsiString);
var
  AStream: TFileStream;
begin
  AStream := TFileStream.Create(dmKon.RabotenDir + ime + 'Print.ini', fmOpenRead);
  try
    dxComponentPrinter1Link1.RestoreDefaults;
    AStream.ReadComponent(dxComponentPrinter1Link1);
  finally
    AStream.Free;
  end;
end;


end.
