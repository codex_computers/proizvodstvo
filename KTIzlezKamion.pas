unit KTIzlezKamion;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ExtCtrls, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxStyles, dxSkinsCore, dxSkinBlack,
  dxSkinBlue, dxSkinBlueprint, dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom,
  dxSkinDarkSide, dxSkinDevExpressDarkStyle, dxSkinDevExpressStyle, dxSkinFoggy,
  dxSkinGlassOceans, dxSkinHighContrast, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven,
  dxSkinSevenClassic, dxSkinSharp, dxSkinSharpPlus, dxSkinSilver,
  dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008, dxSkinTheAsphaltWorld,
  dxSkinsDefaultPainters, dxSkinValentine, dxSkinVS2010, dxSkinWhiteprint,
  dxSkinXmas2008Blue, dxSkinscxPCPainter, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxEdit, Data.DB, cxDBData, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGridLevel, cxClasses, cxGridCustomView,
  cxGrid, cxTrackBar, cxSpinEdit, cxContainer, cxLabel, cxDBLabel,
  cxDropDownEdit, cxMRUEdit, cxDBEdit, cxHyperLinkEdit, cxTextEdit, cxMaskEdit,
  cxButtonEdit, cxImageComboBox, cxImage, Vcl.ActnList, DateUtils, Vcl.Menus,
  Vcl.StdCtrls, cxButtons, Vcl.ImgList, FIBQuery, pFIBQuery, cxCheckBox,
  cxGroupBox, FIBDataSet, pFIBDataSet, cxGridCustomPopupMenu, cxGridPopupMenu,
  dxSkinsdxBarPainter, dxRibbonSkins, dxSkinsdxRibbonPainter, dxRibbon, dxBar,
  cxBarEditItem, dxPSGlbl, dxPSUtl, dxPSEngn, dxPrnPg, dxBkgnd, dxWrap,
  dxPrnDev, dxPSCompsProvider, dxPSFillPatterns, dxPSEdgePatterns,
  dxPSPDFExportCore, dxPSPDFExport, cxDrawTextUtils, dxPSPrVwStd, dxPSPrVwAdv,
  dxPSPrVwRibbon, dxPScxPageControlProducer, dxPScxGridLnk,
  dxPScxGridLayoutViewLnk, dxPScxEditorProducers, dxPScxExtEditorProducers,
  dxPSCore, dxPScxCommon, dxSkinMetropolis, dxSkinMetropolisDark,
  dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray, dxSkinOffice2013White,
  cxNavigator, dxRibbonCustomizationForm, System.Actions,
  dxSkinOffice2016Colorful, dxSkinOffice2016Dark, dxSkinVisualStudio2013Blue,
  dxSkinVisualStudio2013Dark, dxSkinVisualStudio2013Light, System.ImageList;

type
  TfrmKTIzlezKamion = class(TForm)
    PanelDesno: TPanel;
    PanelSredina: TPanel;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1ID: TcxGridDBColumn;
    cxGrid1DBTableView1DATUM_PLANIRAN_POCETOK: TcxGridDBColumn;
    cxGrid1DBTableView1STATUS: TcxGridDBColumn;
    cxGrid1DBTableView1VID_ARTIKAL: TcxGridDBColumn;
    cxGrid1DBTableView1ARTIKAL: TcxGridDBColumn;
    cxGrid1DBTableView1KOLICINA: TcxGridDBColumn;
    cxGrid1DBTableView1OPIS_PROIZVOD: TcxGridDBColumn;
    cxGrid1DBTableView1NAZIV_PROIZVOD: TcxGridDBColumn;
    cxGrid1DBTableView1ZAVRSENI: TcxGridDBColumn;
    cxGrid1DBTableView1MINUS: TcxGridDBColumn;
    cxGrid1DBTableView1PLUS: TcxGridDBColumn;
    cxGrid1DBTableView1ID_RN_STAVKA: TcxGridDBColumn;
    cxGrid1DBTableView1ZAVRSENO: TcxGridDBColumn;
    cxImageList1: TcxImageList;
    cxImageList2: TcxImageList;
    cxMainLarge: TcxImageList;
    qUpdateRN: TpFIBUpdateObject;
    cxGrid1DBTableView1NAZIV_SLEDNA_KT: TcxGridDBColumn;
    tblRN: TpFIBDataSet;
    tblRNID: TFIBBCDField;
    tblRNBROJ: TFIBStringField;
    tblRNID_RE: TFIBIntegerField;
    tblRNDATUM_PLANIRAN_POCETOK: TFIBDateTimeField;
    tblRNDATUM_START: TFIBDateTimeField;
    tblRNSOURCE_DOKUMENT: TFIBBCDField;
    tblRNID_SD: TFIBBCDField;
    tblRNBROJ_SD: TFIBStringField;
    tblRNSTATUS: TFIBIntegerField;
    tblRNDATUM_PLANIRAN_KRAJ: TFIBDateTimeField;
    tblRNDATUM_KRAJ: TFIBDateTimeField;
    tblRNMERNA_EDINICA: TFIBStringField;
    tblRNZABELESKA: TFIBStringField;
    tblRNTP_PORACKA: TFIBIntegerField;
    tblRNP_PORACKA: TFIBIntegerField;
    tblRNNAZIV_PARTNER_PORACKA: TFIBStringField;
    dsRN: TDataSource;
    tblIzberiRN: TpFIBDataSet;
    tblIzberiRNSTATUS: TFIBIntegerField;
    tblIzberiRNVID_ARTIKAL: TFIBIntegerField;
    tblIzberiRNARTIKAL: TFIBIntegerField;
    tblIzberiRNOPIS_PROIZVOD: TFIBStringField;
    tblIzberiRNNAZIV_PROIZVOD: TFIBStringField;
    tblIzberiRNNAZIV_SLEDNA_KT: TFIBStringField;
    dsIzberiRN: TDataSource;
    tblKontrolnaTocka: TpFIBDataSet;
    tblKontrolnaTockaID: TFIBIntegerField;
    tblKontrolnaTockaID_KT: TFIBIntegerField;
    tblKontrolnaTockaNAZIV_KT: TFIBStringField;
    tblKontrolnaTockaNAZIV_RE: TFIBStringField;
    tblKontrolnaTockaVID_ARTIKAL: TFIBIntegerField;
    tblKontrolnaTockaARTIKAL: TFIBIntegerField;
    tblKontrolnaTockaNAZIV_ARTIKAL: TFIBStringField;
    tblKontrolnaTockaID_KT_OPERACIJA: TFIBIntegerField;
    tblKontrolnaTockaID_PR_OPERACIJA: TFIBIntegerField;
    tblKontrolnaTockaNAZIV_OPERACIJA: TFIBStringField;
    tblKontrolnaTockaZAVRSENO: TFIBFloatField;
    tblKontrolnaTockaDATUM: TFIBDateTimeField;
    tblKontrolnaTockaKORISNIK: TFIBStringField;
    tblKontrolnaTockaID_RE: TFIBIntegerField;
    tblKontrolnaTockaNAZIV_PARTNER_PORACKA: TFIBStringField;
    tblKontrolnaTockaGODINA: TFIBIntegerField;
    tblKontrolnaTockaSLEDNA_KT: TFIBIntegerField;
    tblKontrolnaTockaNAZIV_SLEDNA_KT: TFIBStringField;
    tblKontrolnaTockaID_RABOTEN_NALOG: TFIBBCDField;
    tblKontrolnaTockaID_RN_STAVKA: TFIBBCDField;
    dsKontrolnaTocka: TDataSource;
    qSetupM: TpFIBQuery;
    tblOperacijaPR: TpFIBDataSet;
    tblOperacijaPRID: TFIBIntegerField;
    tblOperacijaPRID_PROIZVODSTVEN_RESURS: TFIBIntegerField;
    tblOperacijaPRPROIZVODSTVEN_RESURS: TFIBStringField;
    tblOperacijaPRNAZIV_OPERACIJA: TFIBStringField;
    tblOperacijaPRSEKVENCA: TFIBIntegerField;
    tblOperacijaPRBROJ_CIKLUSI: TFIBIntegerField;
    tblOperacijaPRID_OPERACIJA: TFIBIntegerField;
    tblOperacijaPRPOTREBNO_VREME: TFIBFloatField;
    tblOperacijaPRMERNA_EDINICA: TFIBStringField;
    dsOperacijaPR: TDataSource;
    tblKTOperacija: TpFIBDataSet;
    tblKTOperacijaID: TFIBIntegerField;
    tblKTOperacijaID_KT: TFIBIntegerField;
    tblKTOperacijaID_PR_OPERACIJA: TFIBIntegerField;
    tblKTOperacijaAKTIVEN: TFIBIntegerField;
    tblKTOperacijaNAZIV_OPERACIJA: TFIBStringField;
    tblKTOperacijaNAZIV_PR: TFIBStringField;
    dsKTOperacija: TDataSource;
    tblPoslednaKT: TpFIBDataSet;
    tblPoslednaKTID: TFIBIntegerField;
    tblPoslednaKTID_RE: TFIBIntegerField;
    tblPoslednaKTNAZIV_RE: TFIBStringField;
    tblPoslednaKTVID_ARTIKAL: TFIBIntegerField;
    tblPoslednaKTARTIKAL: TFIBIntegerField;
    tblPoslednaKTNAZIV_ARTIKAL: TFIBStringField;
    tblPoslednaKTID_PR_OPERACIJA: TFIBIntegerField;
    tblPoslednaKTPROIZVODSTVEN_RESURS: TFIBStringField;
    tblPoslednaKTOPERACIJA: TFIBStringField;
    tblPoslednaKTZAVRSENO: TFIBFloatField;
    tblPoslednaKTDATUM: TFIBDateTimeField;
    tblPoslednaKTKORISNIK: TFIBStringField;
    tblPoslednaKTNAZIV_PARTNER_PORACKA: TFIBStringField;
    tblPoslednaKTID_KT: TFIBIntegerField;
    tblPoslednaKTID_KT_OPERACIJA: TFIBIntegerField;
    tblPoslednaKTID_RABOTEN_NALOG: TFIBBCDField;
    dsPoslednaKT: TDataSource;
    tblKtRe: TpFIBDataSet;
    tblKtReID: TFIBIntegerField;
    tblKtReNAZIV_KT: TFIBStringField;
    tblKtReID_RE: TFIBIntegerField;
    tblKtReNAZIV_RE: TFIBStringField;
    dsKtRe: TDataSource;
    cxGridPopupMenu1: TcxGridPopupMenu;
    cxGrid2: TcxGrid;
    cxGridDBTableView1: TcxGridDBTableView;
    cxGridDBColumn1: TcxGridDBColumn;
    cxGrid1DBTableView1NAZIV_PARTNER_PORACKA: TcxGridDBColumn;
    cxGridDBColumn2: TcxGridDBColumn;
    cxGridDBColumn3: TcxGridDBColumn;
    cxGrid1DBTableView1NAZIV_OKT: TcxGridDBColumn;
    cxGridDBColumn4: TcxGridDBColumn;
    cxGridDBColumn5: TcxGridDBColumn;
    cxGridDBColumn6: TcxGridDBColumn;
    cxGridDBColumn7: TcxGridDBColumn;
    cxGrid1DBTableView1ZATVORI: TcxGridDBColumn;
    cxGrid1DBTableView1NAZIV_KT: TcxGridDBColumn;
    cxGrid1DBTableView1ID_KT_OPERACIJA: TcxGridDBColumn;
    cxGrid1DBTableView1NAZIV_OPERACIJA: TcxGridDBColumn;
    cxGrid1DBTableView1ID_KT: TcxGridDBColumn;
    cxGridDBColumn9: TcxGridDBColumn;
    cxGridDBColumn10: TcxGridDBColumn;
    cxGridDBColumn11: TcxGridDBColumn;
    cxGrid1DBTableView1ID_EKT: TcxGridDBColumn;
    cxGrid1DBTableView1PRIMENI: TcxGridDBColumn;
    cxGridLevel1: TcxGridLevel;
    tblRNVlez: TpFIBDataSet;
    dsRNVlez: TDataSource;
    dsRNV: TDataSource;
    tblRNV: TpFIBDataSet;
    tblRNVID: TFIBBCDField;
    tblRNVBROJ: TFIBStringField;
    tblRNVID_RE: TFIBIntegerField;
    tblRNVDATUM_PLANIRAN_POCETOK: TFIBDateTimeField;
    tblRNVDATUM_START: TFIBDateTimeField;
    tblRNVSTATUS: TFIBIntegerField;
    tblRNVNAZIV_PARTNER_PORACKA: TFIBStringField;
    tblRNVID_OD_KT: TFIBIntegerField;
    tblRNVNAZIV_OD_KT: TFIBStringField;
    tblIzberiRNID_SLEDNA_KT: TFIBIntegerField;
    cxGridPopupMenu2: TcxGridPopupMenu;
    PanelDolu: TPanel;
    cxLabel1: TcxLabel;
    dxBarManager1: TdxBarManager;
    dxBarManager1Bar1: TdxBar;
    dxBarManager1Bar2: TdxBar;
    dxBarManager1Bar3: TdxBar;
    dxBarManager1Bar4: TdxBar;
    dxBarManager1BarIzgledGrid: TdxBar;
    dxBarLargeButton2: TdxBarLargeButton;
    dxBarLargeButton3: TdxBarLargeButton;
    dxBarLargeButton4: TdxBarLargeButton;
    dxBarLargeButton5: TdxBarLargeButton;
    dxBarLargeButton6: TdxBarLargeButton;
    dxBarLargeButton7: TdxBarLargeButton;
    dxBarLargeButton8: TdxBarLargeButton;
    dxBarLargeButton1: TdxBarLargeButton;
    dxBarLargeButton9: TdxBarLargeButton;
    dxBarLargeButton11: TdxBarLargeButton;
    ribbonFilterReset: TdxBarLargeButton;
    dxBarLargeButton12: TdxBarLargeButton;
    dxBarLargeButton13: TdxBarLargeButton;
    dxBarLargeButton14: TdxBarLargeButton;
    dxBarLargeButton15: TdxBarLargeButton;
    dxBarLargeButton16: TdxBarLargeButton;
    dxBarLargeButton17: TdxBarLargeButton;
    cxBarEditItem1: TcxBarEditItem;
    dxBarSubItem1: TdxBarSubItem;
    dxBarLBtnSnimiIzgled: TdxBarLargeButton;
    dxBarLBtnBrisiIzgled: TdxBarLargeButton;
    dxBarLargeButton10: TdxBarLargeButton;
    cbSite: TcxBarEditItem;
    dxRibbon1: TdxRibbon;
    dxRibbon1Tab1: TdxRibbonTab;
    dxRibbon1Tab2: TdxRibbonTab;
    dxBarManager1Bar5: TdxBar;
    dxBarManager1Bar6: TdxBar;
    dxBarManager1Bar7: TdxBar;
    dxBarManager1Bar8: TdxBar;
    ActionList1: TActionList;
    aNov: TAction;
    aAzuriraj: TAction;
    aBrisi: TAction;
    aSnimiIzgled: TAction;
    aZacuvajExcel: TAction;
    aPecatiTabela: TAction;
    aHelp: TAction;
    aIzlez: TAction;
    aZapisi: TAction;
    aOtkazi: TAction;
    aRefresh: TAction;
    aSnimiPecatenje: TAction;
    aPodesuvanjePecatenje: TAction;
    aBrisiPodesuvanjePecatenje: TAction;
    aPageSetup: TAction;
    aFormConfig: TAction;
    aBrisiIzgled: TAction;
    aSpustiSoberi: TAction;
    aKreirajRN: TAction;
    aSteStavki: TAction;
    dxComponentPrinter1: TdxComponentPrinter;
    dxComponentPrinter1Link1: TdxGridReportLink;
    dxComponentPrinter1Link2: TdxGridReportLink;
    PanelGore: TPanel;
    lblSlednaKT: TcxLabel;
    lblKT: TcxLabel;
    fbdtfldIzberiRNDATUM: TFIBDateField;
    tblRNVlezID_EKT: TFIBIntegerField;
    tblRNVlezID: TFIBIntegerField;
    tblRNVlezNAZIV_KT: TFIBStringField;
    tblRNVlezID_KT_OPERACIJA: TFIBIntegerField;
    tblRNVlezNAZIV_OPERACIJA: TFIBStringField;
    tblRNVlezID_TRAILER: TFIBIntegerField;
    tblRNVlezID_KT: TFIBIntegerField;
    tblRNVlezNAZIV_OKT: TFIBStringField;
    tblRNVlezNAZIV_PARTNER_PORACKA: TFIBStringField;
    tblRNVlezVID_ARTIKAL: TFIBIntegerField;
    tblRNVlezARTIKAL: TFIBIntegerField;
    tblRNVlezPROIZVOD: TFIBStringField;
    tblRNVlezZABELESKA: TFIBStringField;
    tblRNVlezPRIMENI: TFIBBCDField;
    pRNVlezZAVRSENO: TFIBFloatField;
    tblIzberiRNID: TFIBIntegerField;
    fbcdfldIzberiRNID_RN_STAVKA: TFIBBCDField;
    tblIzberiRNTAPACIR_SEDISTE: TFIBStringField;
    tblIzberiRNTAPACIR_NAZAD: TFIBStringField;
    tblIzberiRNSUNGER: TFIBStringField;
    tblIzberiRNBOJA_NOGARKI: TFIBStringField;
    tblIzberiRNLABELS: TFIBStringField;
    tblIzberiRNGLIDERS: TFIBStringField;
    tblIzberiRNREF_NO: TFIBStringField;
    cxGrid1DBTableView1TAPACIR_SEDISTE: TcxGridDBColumn;
    cxGrid1DBTableView1TAPACIR_NAZAD: TcxGridDBColumn;
    cxGrid1DBTableView1SUNGER: TcxGridDBColumn;
    cxGrid1DBTableView1BOJA_NOGARKI: TcxGridDBColumn;
    cxGrid1DBTableView1LABELS: TcxGridDBColumn;
    cxGrid1DBTableView1GLIDERS: TcxGridDBColumn;
    cxGrid1DBTableView1REF_NO: TcxGridDBColumn;
    tblIzberiRNKOLICINA: TFIBBCDField;
    tblIzberiRNNAZIV_PROIZVOD1: TFIBStringField;
    tblIzberiRNOPIS_PROIZVOD1: TFIBStringField;
    tblIzberiRNZAVRSENO: TFIBFloatField;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure OtvoriTabeli;
    procedure OtvoriRN;
    procedure cxGrid1DBTableView1MINUSPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure aPrezemiSiteExecute(Sender: TObject);
    procedure cxDBLabel2Click(Sender: TObject);
    procedure aNamaliExecute(Sender: TObject);
    procedure aZgolemiExecute(Sender: TObject);
    procedure OtvoriKT;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure aIzlezExecute(Sender: TObject);
    procedure aNamali10Execute(Sender: TObject);
    procedure aZgolemi10Execute(Sender: TObject);
    procedure cxButton1Click(Sender: TObject);
    procedure cxButton2Click(Sender: TObject);
    procedure cxGrid1DBTableView1FocusedRecordChanged(
      Sender: TcxCustomGridTableView; APrevFocusedRecord,
      AFocusedRecord: TcxCustomGridRecord;
      ANewItemRecordFocusingChanged: Boolean);
    procedure cxGridDBTableView1TcxGridDBDataControllerTcxDataSummaryFooterSummaryItems1GetText(
      Sender: TcxDataSummaryItem; const AValue: Variant; AIsFooter: Boolean;
      var AText: string);
    procedure cxGridDBTableView1TcxGridDBDataControllerTcxDataSummaryFooterSummaryItems0GetText(
      Sender: TcxDataSummaryItem; const AValue: Variant; AIsFooter: Boolean;
      var AText: string);
    procedure aZacuvajVoExcelExecute(Sender: TObject);
    procedure aPecatiTabelaExecute(Sender: TObject);
    procedure aPodesuvanjePecatenjeExecute(Sender: TObject);
    procedure aPageSetupExecute(Sender: TObject);
    procedure aSnimiPecatenjeExecute(Sender: TObject);
    procedure aBrisiPodesuvanjePecatenjeExecute(Sender: TObject);
    procedure aSnimiIzgledExecute(Sender: TObject);
    procedure aBrisiIzgledExecute(Sender: TObject);
    procedure FormResize(Sender: TObject);
  private
    { Private declarations }
  public
    p, id_kt:byte; id_trailer:LongInt;
    vid_artikal,artikal : string;
    { Public declarations }
  end;

var
  frmKTIzlezKamion: TfrmKTIzlezKamion;

implementation

{$R *.dfm}

uses dmUnit, dmKonekcija, dmMaticni, dmResources, dmSystem, mk, utils, cxConstantsMak, DaNe;

procedure TfrmKTIzlezKamion.aZacuvajVoExcelExecute(Sender: TObject);
begin
 if cxGrid1.IsFocused then
  zacuvajVoExcel(cxGrid1, Caption)
 else
  zacuvajVoExcel(cxGrid2, Caption)
 end;

procedure TfrmKTIzlezKamion.aZgolemi10Execute(Sender: TObject);
begin
 if cxGrid1DBTableView1ZAVRSENI.EditValue = null  then
     cxGrid1DBTableView1ZAVRSENI.EditValue := 0;
  cxGrid1DBTableView1ZAVRSENI.EditValue := cxGrid1DBTableView1ZAVRSENI.EditValue + 10;
end;

procedure TfrmKTIzlezKamion.aZgolemiExecute(Sender: TObject);
begin
  if cxGrid1DBTableView1ZAVRSENI.EditValue = null  then
     cxGrid1DBTableView1ZAVRSENI.EditValue := 0;
   cxGrid1DBTableView1ZAVRSENI.EditValue := cxGrid1DBTableView1ZAVRSENI.EditValue + 1;
end;

procedure TfrmKTIzlezKamion.aBrisiIzgledExecute(Sender: TObject);
begin
if cxgrid1.IsFocused then
begin
   brisiGridVoBaza(Name,cxGrid1DBTableView1);
   BrisiFormaIzgled(self);
end
else
begin
  brisiGridVoBaza(Name,cxGrid1DBTableView1);
  BrisiFormaIzgled(self);
end;
end;

procedure TfrmKTIzlezKamion.aBrisiPodesuvanjePecatenjeExecute(Sender: TObject);
begin
if cxgrid1.IsFocused then
  brisiPrintOdBaza(Name,cxGrid1DBTableView1.Name, dxComponentPrinter1Link1)
else
  brisiPrintOdBaza(Name,cxGridDBTableView1.Name, dxComponentPrinter1Link2);
end;

procedure TfrmKTIzlezKamion.aIzlezExecute(Sender: TObject);
begin
    Close;
end;

procedure TfrmKTIzlezKamion.aNamali10Execute(Sender: TObject);
begin
 if cxGrid1DBTableView1ZAVRSENI.EditValue = null  then
     cxGrid1DBTableView1ZAVRSENI.EditValue := 0;
  cxGrid1DBTableView1ZAVRSENI.EditValue := cxGrid1DBTableView1ZAVRSENI.EditValue - 10;
end;

procedure TfrmKTIzlezKamion.aNamaliExecute(Sender: TObject);
begin
  if cxGrid1DBTableView1ZAVRSENI.EditValue = null  then
     cxGrid1DBTableView1ZAVRSENI.EditValue := 0;
  cxGrid1DBTableView1ZAVRSENI.EditValue := cxGrid1DBTableView1ZAVRSENI.EditValue - 1;
end;

procedure TfrmKTIzlezKamion.aPageSetupExecute(Sender: TObject);
begin
if cxgrid1.IsFocused then
  dxComponentPrinter1Link1.PageSetup
else
  dxComponentPrinter1Link2.PageSetup;
end;

procedure TfrmKTIzlezKamion.aPecatiTabelaExecute(Sender: TObject);
begin
if cxgrid1.IsFocused then
 begin
  dxComponentPrinter1Link1.ReportTitle.Text := Caption+cxGrid1Level1.Caption;

  dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Clear;
  dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Add(dmkon.firma_naziv);
  dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Add(DateTimeToStr(Now));

  dxComponentPrinter1Link1.PrinterPage.PageHeader.LeftTitle.Clear;
  dxComponentPrinter1Link1.PrinterPage.PageHeader.LeftTitle.Add(lblKT.Caption);

  dxComponentPrinter1.Preview(true, dxComponentPrinter1Link1);
 end
 else
 begin
    dxComponentPrinter1Link2.ReportTitle.Text := Caption+cxGrid1DBTableView1NAZIV_OKT.Caption;;

    dxComponentPrinter1Link2.PrinterPage.PageHeader.RightTitle.Clear;
    dxComponentPrinter1Link2.PrinterPage.PageHeader.RightTitle.Add(dmkon.firma_naziv);
    dxComponentPrinter1Link2.PrinterPage.PageHeader.RightTitle.Add(DateTimeToStr(Now));

    dxComponentPrinter1Link2.PrinterPage.PageHeader.LeftTitle.Clear;
  //dxComponentPrinter1Link1.PrinterPage.PageHeader.LeftTitle.Add('������� : ' + TipSifra.Text + '-' + IdSifra.Text + ' ' + Partner.Text);
    dxComponentPrinter1Link2.PrinterPage.PageHeader.LeftTitle.Add(lblKT.Caption);
  dxComponentPrinter1.Preview(true, dxComponentPrinter1Link2);
 end;
end;

procedure TfrmKTIzlezKamion.aPodesuvanjePecatenjeExecute(Sender: TObject);
begin
if cxgrid1.IsFocused then
   dxComponentPrinter1Link1.DesignReport()
else
   dxComponentPrinter1Link2.DesignReport()
end;

procedure TfrmKTIzlezKamion.aPrezemiSiteExecute(Sender: TObject);
var i : integer;
begin
//  //�� �� �������� ���������� �� �� �� ��������
//  //������ �� ��� ���� � ������� ������
//  //tblKontrolnaTocka.Close;
//  //tblKontrolnaTocka.Open;
//  if btOznaciP.Focused then
//     begin
//              if lblSlednaKT.Caption = '' then
//              begin
//                ShowMessage('������� ������ ��������� �����!');
//                Abort;
//              end;
//
//            with cxGrid1DBTableView1.DataController do
//              for I := 0 to RecordCount - 1 do
//              begin
//                         tblKontrolnaTocka.insert;
//                         tblKontrolnaTockaID_KT.AsInteger := tblKorisnikKTID_KT.Value;
//                         tblKontrolnaTockaID_KT_OPERACIJA.Value := tblKTOperacijaID.Value;
//                         tblKontrolnaTockaID_RABOTEN_NALOG.AsInteger := Values[i,cxGrid1DBTableView1ID.Index];
//                         tblKontrolnaTockaID_RE.asInteger := dmKon.firma_id;
//                         tblKontrolnaTockaZAVRSENO.Value := Values[i,cxGrid1DBTableView1KOLICINA.Index];
//                         tblKontrolnaTockaVID_ARTIKAL.AsInteger := Values[i,cxGrid1DBTableView1VID_ARTIKAL.Index];
//                         tblKontrolnaTockaARTIKAL.AsInteger := Values[i,cxGrid1DBTableView1ARTIKAL.Index];
//                         tblKontrolnaTockaDATUM.Value := Now;
//                         tblKontrolnaTockaGODINA.AsInteger := YearOf(Now);
//                         tblKontrolnaTockaSLEDNA_KT.Value := tblKTID.Value;
//                         tblKontrolnaTockaID_RN_STAVKA.AsInteger := Values[i,cxGrid1DBTableView1ID_RN_STAVKA.Index];
//                         tblKontrolnaTocka.Post;
//
//                        // qUpdateRN.Close;
//                        // qUpdateRN.ParamByName('id').Value := Values[i,cxGrid1DBTableView1ID_RN_STAVKA.Index];
//                        // qUpdateRN.ExecQuery;
//              end;
//              OtvoriRN;
//     end
//     else
//     if btZapisi.Focused then
//     begin
//          if lblSlednaKT.Caption = '' then
//            begin
//              ShowMessage('������� ������ ��������� �����!');
//              Abort;
//            end;
//
//        with cxGrid1DBTableView1.DataController do
//              for I := 0 to RecordCount - 1 do
//              if Values[i,cxGrid1DBTableView1ZAVRSENI.Index] <> null then
//              begin
//                         tblKontrolnaTocka.insert;
//                         tblKontrolnaTockaID_KT.AsInteger := tblKorisnikKTID_KT.Value;
//                         tblKontrolnaTockaID_KT_OPERACIJA.Value := tblKTOperacijaID.Value;
//                         tblKontrolnaTockaID_RABOTEN_NALOG.AsInteger := Values[i,cxGrid1DBTableView1ID.Index];
//                         tblKontrolnaTockaID_RE.asInteger := dmKon.firma_id;
//                         tblKontrolnaTockaZAVRSENO.AsString := (Values[i,cxGrid1DBTableView1ZAVRSENI.Index]);
//                         tblKontrolnaTockaVID_ARTIKAL.AsInteger := Values[i,cxGrid1DBTableView1VID_ARTIKAL.Index];
//                         tblKontrolnaTockaARTIKAL.AsInteger := Values[i,cxGrid1DBTableView1ARTIKAL.Index];
//                         tblKontrolnaTockaDATUM.Value := Now;
//                         tblKontrolnaTockaGODINA.AsInteger := YearOf(Now);
//                         tblKontrolnaTockaSLEDNA_KT.Value := tblKTID.Value;
//                         tblKontrolnaTockaID_RN_STAVKA.AsInteger := Values[i,cxGrid1DBTableView1ID_RN_STAVKA.Index];
//                         tblKontrolnaTocka.Post;
//              end;
//              OtvoriRN;
//     end
//     else
//     if btResetiraj.Focused then
//     begin
//       with cxGrid1DBTableView1.DataController do
//              for I := 0 to RecordCount - 1 do
//              if Values[i,cxGrid1DBTableView1ZAVRSENI.Index] <> null then
//              begin
//                        Values[i,cxGrid1DBTableView1ZAVRSENI.Index] := null;
//              end;
//              OtvoriRN;
//     end
//     else
//     if btZavrseni.Focused then
//     begin
//       p := btZavrseni.Tag;
//       OtvoriRN;
//     end
//     else
//     if btSokrijZavrseni.Focused then
//     begin
//       p := btSokrijZavrseni.Tag;
//       OtvoriRN;
//     end
end;

procedure TfrmKTIzlezKamion.aSnimiIzgledExecute(Sender: TObject);
begin
if cxgrid1.IsFocused then
begin
   zacuvajGridVoBaza(Name,cxGrid1DBTableView1);
   ZacuvajFormaIzgled(self);
end
else
begin
   zacuvajGridVoBaza(Name,cxGridDBTableView1);
   ZacuvajFormaIzgled(Self);
end;
end;

procedure TfrmKTIzlezKamion.aSnimiPecatenjeExecute(Sender: TObject);
begin
if cxgrid1.IsFocused then
  zacuvajPrintVoBaza(Name,cxGrid1DBTableView1.Name,dxComponentPrinter1Link1)
else
  zacuvajPrintVoBaza(Name,cxGridDBTableView1.Name,dxComponentPrinter1Link2);
end;

procedure TfrmKTIzlezKamion.cxButton1Click(Sender: TObject);
begin
//  PanelRN.Visible := false;
end;

procedure TfrmKTIzlezKamion.cxButton2Click(Sender: TObject);
begin
 //   PanelSlednaKT.Visible := true;
end;

procedure TfrmKTIzlezKamion.cxDBLabel2Click(Sender: TObject);
begin
 //  PanelSlednaKT.Visible := true;
end;

procedure TfrmKTIzlezKamion.cxGrid1DBTableView1FocusedRecordChanged(
  Sender: TcxCustomGridTableView; APrevFocusedRecord,
  AFocusedRecord: TcxCustomGridRecord; ANewItemRecordFocusingChanged: Boolean);
begin
   // btSlednaKT.Caption := tblIzberiRNNAZIV_SLEDNA_KT.Value;
end;

procedure TfrmKTIzlezKamion.cxGrid1DBTableView1MINUSPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
  //�� ���� ���� �� �� ��������� ��� �� �� �������� ���������
end;

procedure TfrmKTIzlezKamion.cxGridDBTableView1TcxGridDBDataControllerTcxDataSummaryFooterSummaryItems0GetText(
  Sender: TcxDataSummaryItem; const AValue: Variant; AIsFooter: Boolean;
  var AText: string);
begin
//  AText :=  '������'+AValue;
end;

procedure TfrmKTIzlezKamion.cxGridDBTableView1TcxGridDBDataControllerTcxDataSummaryFooterSummaryItems1GetText(
  Sender: TcxDataSummaryItem; const AValue: Variant; AIsFooter: Boolean;
  var AText: string);
begin
  // AText :=  '������'+AValue;
end;

procedure TfrmKTIzlezKamion.FormClose(Sender: TObject; var Action: TCloseAction);
begin
   tblIzberiRN.Close;
end;

procedure TfrmKTIzlezKamion.FormCreate(Sender: TObject);
begin
  ProcitajFormaIzgled(self);
  if (dmRes.use_skin = true) then dxRibbon1.ColorSchemeName := dmRes.skin_name;
 // dm.tblAmbalaza.CloseOpen(true);
 // rData := TRepositoryData.Create();
end;

procedure TfrmKTIzlezKamion.FormResize(Sender: TObject);
begin
     PanelSredina.Height := PanelDolu.Height div 2;
end;

procedure TfrmKTIzlezKamion.FormShow(Sender: TObject);
begin
  frmMK := TfrmMK.Create(nil);
  frmMK.ShowModal;
  frmMK.Free;
  // ������� �� ������������ ������������/���������� �� ���������� �� ���� �����
  SpremiForma(self);
 // cbgodina.Text := inttostr(yearof(now));
//  tblRN.Close;
//  tblRN.ParamByName('status').Asstring := '1';
//  tblRN.ParamByName('godina').AsString := godina;
////  if masinsko<>0 then
////    tblRN.ParamByName('id_re').AsInteger := masinsko
////  else
//    tblRN.ParamByName('id_re').AsString := '%';
//  tblRN.Open;

//  tblRNV.Close;
//  tblRNV.ParamByName('status').Asstring := '1';
//  tblRNV.ParamByName('godina').AsString := '%';
////  if masinsko<>0 then
////    dm.tblRNV.ParamByName('id_re').AsInteger := masinsko
////  else
//  tblRNV.ParamByName('id_re').Value := id_re;
//  tblRNV.ParamByName('id_kt').AsInteger := id_kt;
//  tblRNV.Open;

 // Caption := '��������� �� ����� �� '+tblKorisnikKTNAZIV.Value;

 // if tblKorisnikKTID_KT.AsInteger in [19,20] then btPogledSivara.Visible := false;

  OtvoriTabeli;
//  OtvoriRN;
//  tsArtikal.TabVisible := false;
//  cxPageControl2.ActivePage := tsPoedinecni;
//  cxPageControl1.ActivePage := tsKontrolnaTocka;
//  cbArtikal.Text := tblKontrolnaTockaNAZIV_ARTIKAL.Value;
//  KtLayoutGrid.Site.Refresh;
//  KtLayoutGrid.Site.VScrollBar.Width := 40;
//  KtLayoutGrid.LayoutChanged(True);
//
//  RnLayoutGrid.Site.Refresh;
//  RnLayoutGrid.Site.VScrollBar.Width := 40;
//  RnLayoutGrid.LayoutChanged(True);

end;
procedure TfrmKTIzlezKamion.OtvoriTabeli;
begin
//   tblRePartner.Close;
//   tblRePartner.ParamByName('id').Asstring := dmKon.qSysUserPARTNER.AsString;
//   tblRePartner.ParamByName('tip_partner').Asstring := dmKon.qSysUserTIP_PARTNER.AsString;
//   tblRePartner.Open;
//
   rab_edinica := dmKon.qSysUserRE.Value;

   qSetupM.Close;
   qSetupM.ExecQuery;

   // komentirano na 08032013 - kje vidam za ponatamu
  // if qSetupM.FldByName['v1'].AsInteger = rab_edinica then
   //     masinsko := qSetupM.FldByName['v1'].AsInteger
  // else masinsko := 0;

 //  OtvoriKT;

//   tblOperacijaPR.close;
//   tblOperacijaPR.Open;

   OtvoriRN;

   tblPoslednaKT.Open;
   tblKtRe.Close;
   tblKtRe.ParamByName('id_re').AsInteger := rab_edinica;
   tblKtRe.Open;
   if tblKtRe.RecordCount = 1 then
   begin
    // kta := tblKtReID.Value;
   end
   else
   begin
    // kta := 0;
   end;
   tblKTOperacija.Close;
   tblKTOperacija.Open;
//   tblartikalCB.Close;
//  tblArtikalcb.ParamByName('godina').AsString := cbGodina.Text;
//   if masinsko <> 0 then
//      tblArtikalcb.ParamByName('id_re').AsInteger := masinsko
//    else
///////      tblArtikalcb.ParamByName('id_re').AsString := '%';
//   tblArtikalCB.ParamByName('rn').Value := tblIzberiRNID.Value;
//   tblArtikalCB.Open;
//   rab_edinica := qRePartner.FldByName['']
end;
procedure TfrmKTIzlezKamion.OtvoriRN;
begin

  tblIzberiRN.Close;
  tblIzberiRN.ParamByName('status').Asstring := '0';
  tblIzberiRN.ParamByName('godina').AsString := godina;
//  if masinsko<>0 then
//    tblIzberiRN.ParamByName('id_re').AsInteger := masinsko
//  else
 //   tblIzberiRN.ParamByName('id_re').Value := id_re;
  tblIzberiRN.ParamByName('id_trailer').Value := id_trailer;
  tblIzberiRN.ParamByName('kti').Value :=id_kt;   //kti;

 // tblIzberiRN.ParamByName('p').AsInteger := 1;
  tblIzberiRN.ParamByName('vid_artikal').AsString := vid_artikal;
  tblIzberiRN.ParamByName('artikal').AsString := artikal;
  tblIzberiRN.Open;


  tblRNVlez.Close;
  tblRNVlez.ParamByName('status').Asstring := '0';
 // dm.tblRNVlez.ParamByName('godina').AsString := '%';
//  if masinsko<>0 then
//    dm.tblRNVlez.ParamByName('id_re').AsInteger := masinsko
//  else
//    dm.tblRNVlez.ParamByName('id_re').AsString := '%';
  tblRNVlez.ParamByName('id_trailer').Value := id_trailer;
  tblRNVlez.ParamByName('id_kt').Value := id_kt;
  tblRNVlez.ParamByName('kt').Value := '%';  //tblRNVID_OD_KT.Value;

  tblRNVlez.ParamByName('p').AsInteger := 1;
  tblRNVlez.ParamByName('vid_artikal').AsString := vid_artikal;
  tblRNVlez.ParamByName('artikal').AsString := artikal;
  tblRNVlez.Open;
end;

procedure TfrmKTIzlezKamion.OtvoriKT;
begin
//  tblKontrolnaTocka.Close;
//  tblKontrolnaTocka.ParamByName('godina').AsString := '%';
//  tblKontrolnaTocka.ParamByName('id_kt').AsString := tblKorisnikKTID_KT.AsString;
////  tblKontrolnaTocka.ParamByName('korisnik').AsString := dmKon.qSysUserUSERNAME.AsString;
//  tblKontrolnaTocka.Open;
end;

end.
