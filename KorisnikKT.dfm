inherited frmKorisnikKT: TfrmKorisnikKT
  Caption = #1050#1086#1088#1080#1089#1085#1080#1082' '#1074#1086' '#1050#1086#1085#1090#1088#1086#1083#1085#1072' '#1058#1086#1095#1082#1072
  ExplicitWidth = 749
  ExplicitHeight = 592
  PixelsPerInch = 96
  TextHeight = 13
  inherited lPanel: TPanel
    Height = 275
    ExplicitHeight = 275
    inherited cxGrid1: TcxGrid
      Height = 271
      ExplicitHeight = 271
      inherited cxGrid1DBTableView1: TcxGridDBTableView
        DataController.DataSource = dm.dsKorisnikKT
        object cxGrid1DBTableView1ID_KT: TcxGridDBColumn
          Caption = #1064#1080#1092#1088#1072' '#1085#1072' '#1050#1058
          DataBinding.FieldName = 'ID_KT'
        end
        object cxGrid1DBTableView1KORISNIK: TcxGridDBColumn
          Caption = #1050#1086#1088#1080#1089#1085#1080#1082
          DataBinding.FieldName = 'KORISNIK'
        end
        object cxGrid1DBTableView1NAZIV: TcxGridDBColumn
          Caption = #1053#1072#1079#1080#1074
          DataBinding.FieldName = 'NAZIV'
        end
        object cxGrid1DBTableView1TIP_PARTNER: TcxGridDBColumn
          Caption = #1058#1080#1087' '#1085#1072' '#1087#1072#1088#1090#1085#1077#1088
          DataBinding.FieldName = 'TIP_PARTNER'
          Visible = False
        end
        object cxGrid1DBTableView1PARTNER: TcxGridDBColumn
          Caption = #1055#1072#1088#1090#1085#1077#1088
          DataBinding.FieldName = 'PARTNER'
          Visible = False
        end
      end
    end
  end
  inherited dPanel: TPanel
    Top = 401
    Height = 129
    ExplicitTop = 401
    ExplicitHeight = 129
    inherited Label1: TLabel
      Left = 419
      Top = 102
      Visible = False
      ExplicitLeft = 419
      ExplicitTop = 102
    end
    object Label4: TLabel [1]
      Left = 11
      Top = 23
      Width = 123
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1050#1086#1085#1090#1088#1086#1083#1085#1072' '#1090#1086#1095#1082#1072' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label2: TLabel [2]
      Left = 11
      Top = 50
      Width = 123
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1050#1086#1088#1080#1089#1085#1080#1082' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    inherited Sifra: TcxDBTextEdit
      Tag = 0
      Left = 475
      Top = 99
      TabOrder = 5
      Visible = False
      ExplicitLeft = 475
      ExplicitTop = 99
    end
    inherited OtkaziButton: TcxButton
      Top = 89
      TabOrder = 4
      ExplicitTop = 89
    end
    inherited ZapisiButton: TcxButton
      Top = 89
      TabOrder = 3
      ExplicitTop = 89
    end
    object txtKT: TcxDBTextEdit
      Tag = 1
      Left = 140
      Top = 20
      BeepOnEnter = False
      DataBinding.DataField = 'ID_KT'
      DataBinding.DataSource = dm.dsKorisnikKT
      ParentFont = False
      Properties.BeepOnError = True
      Properties.CharCase = ecUpperCase
      Style.Shadow = False
      TabOrder = 0
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 86
    end
    object cbKT: TcxDBLookupComboBox
      Left = 226
      Top = 20
      Anchors = [akRight, akBottom]
      DataBinding.DataField = 'ID_KT'
      DataBinding.DataSource = dm.dsKorisnikKT
      Properties.DropDownAutoSize = True
      Properties.DropDownSizeable = True
      Properties.ImmediatePost = True
      Properties.KeyFieldNames = 'ID'
      Properties.ListColumns = <
        item
          FieldName = 'ID'
        end
        item
          Width = 100
          FieldName = 'naziv'
        end>
      Properties.ListFieldIndex = 1
      Properties.ListSource = dm.dsKT
      TabOrder = 1
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 357
    end
    object cbKorisnik: TcxDBLookupComboBox
      Left = 140
      Top = 46
      DataBinding.DataField = 'KORISNIK'
      DataBinding.DataSource = dm.dsKorisnikKT
      Properties.DropDownAutoSize = True
      Properties.DropDownSizeable = True
      Properties.KeyFieldNames = 'USERNAME'
      Properties.ListColumns = <
        item
          FieldName = 'USERNAME'
        end>
      Properties.ListSource = dmKon.dsQSysUser
      TabOrder = 2
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 213
    end
  end
  inherited dxRibbon1: TdxRibbon
    inherited dxRibbon1Tab1: TdxRibbonTab
      Index = 0
    end
    inherited dxRibbon1Tab2: TdxRibbonTab
      Index = 1
    end
  end
  inherited dxBarManager1: TdxBarManager
    inherited dxBarManager1Bar1: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 69
      FloatClientHeight = 167
    end
    inherited dxBarManager1Bar2: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 149
      FloatClientHeight = 188
    end
    inherited dxBarManager1Bar3: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 59
      FloatClientHeight = 108
    end
    inherited dxBarManager1Bar4: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 141
      FloatClientHeight = 221
    end
    inherited dxBarManager1BarTabela: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 174
      FloatClientHeight = 113
    end
  end
  inherited dxComponentPrinter1: TdxComponentPrinter
    inherited dxComponentPrinter1Link1: TdxGridReportLink
      ReportDocument.CreationDate = 41194.619952743060000000
      BuiltInReportLink = True
    end
  end
end
