object frmIzvTovarnList: TfrmIzvTovarnList
  Left = 128
  Top = 212
  BorderIcons = [biSystemMenu, biMaximize]
  Caption = #1058#1086#1074#1072#1088#1077#1085' '#1051#1080#1089#1090
  ClientHeight = 705
  ClientWidth = 1050
  Color = clBtnFace
  Constraints.MinHeight = 500
  Constraints.MinWidth = 610
  Font.Charset = RUSSIAN_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  Position = poScreenCenter
  ShowHint = True
  WindowState = wsMaximized
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  TextHeight = 13
  object dxRibbon1: TdxRibbon
    Left = 0
    Top = 0
    Width = 1050
    Height = 126
    BarManager = dxBarManager1
    ColorSchemeName = 'Blue'
    Contexts = <>
    TabOrder = 0
    TabStop = False
    object dxRibbon1Tab1: TdxRibbonTab
      Active = True
      Caption = #1052#1077#1085#1080
      Groups = <
        item
          ToolbarName = 'dxBarManager1Bar2'
        end
        item
          ToolbarName = 'dxBarManager1Bar3'
        end>
      Index = 0
    end
    object dxRibbon1Tab2: TdxRibbonTab
      Caption = #1055#1086#1076#1077#1089#1091#1074#1072#1114#1072
      Groups = <
        item
          ToolbarName = 'dxBarManager1Bar4'
        end
        item
          ToolbarName = 'dxBarManager1BarIzgledGrid'
        end>
      Index = 1
    end
  end
  object StatusBar1: TdxRibbonStatusBar
    Left = 0
    Top = 682
    Width = 1050
    Height = 23
    Panels = <
      item
        PanelStyleClassName = 'TdxStatusBarTextPanelStyle'
        Bevel = dxpbNone
        Text = 'Esc - '#1054#1090#1082#1072#1078#1080' / '#1048#1079#1083#1077#1079
      end>
    Ribbon = dxRibbon1
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clDefault
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
  end
  object dPanel: TPanel
    Left = 0
    Top = 126
    Width = 1050
    Height = 35
    Align = alTop
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clNavy
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 2
    OnClick = dPanelClick
  end
  object lPanel: TPanel
    Left = 0
    Top = 161
    Width = 1050
    Height = 521
    Align = alClient
    TabOrder = 3
    object cxGrid1: TcxGrid
      Left = 1
      Top = 1
      Width = 1048
      Height = 519
      Align = alClient
      TabOrder = 0
      RootLevelOptions.DetailTabsPosition = dtpTop
      object cxGrid1DBTableView1: TcxGridDBTableView
        Navigator.Buttons.CustomButtons = <>
        ScrollbarAnnotations.CustomAnnotations = <>
        OnEditKeyDown = cxGrid1DBTableView1EditKeyDown
        OnEditValueChanged = cxGrid1DBTableView1EditValueChanged
        DataController.DataSource = dm.dsIzvTovarenList
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        FilterRow.InfoText = #1050#1083#1080#1082#1085#1077#1090#1077' '#1090#1091#1082#1072' '#1079#1072' '#1076#1072' '#1076#1077#1092#1080#1085#1080#1088#1072#1090#1077' '#1092#1080#1083#1090#1077#1088
        FilterRow.Visible = True
        OptionsBehavior.IncSearch = True
        OptionsCustomize.ColumnsQuickCustomization = True
        OptionsData.CancelOnExit = False
        OptionsData.Deleting = False
        OptionsData.DeletingConfirmation = False
        OptionsData.Inserting = False
        OptionsView.CellAutoHeight = True
        object cxGrid1DBTableView1NAZIV_TRAILER: TcxGridDBColumn
          Caption = 'Name Trailer'
          DataBinding.FieldName = 'NAZIV_TRAILER'
          Visible = False
          GroupIndex = 0
          HeaderAlignmentHorz = taCenter
          HeaderGlyphAlignmentHorz = taCenter
          Options.Editing = False
          SortIndex = 0
          SortOrder = soAscending
          Width = 118
        end
        object cxGrid1DBTableView1REF_NO: TcxGridDBColumn
          Caption = 'Ref No'
          DataBinding.FieldName = 'REF_NO'
          HeaderAlignmentHorz = taCenter
          HeaderGlyphAlignmentHorz = taCenter
          Options.Editing = False
          Width = 103
        end
        object cxGrid1DBTableView1Column1: TcxGridDBColumn
          Caption = 'Date'
          DataBinding.FieldName = 'DATUM_TRANSPORT'
          HeaderAlignmentHorz = taCenter
          Options.Editing = False
        end
        object cxGrid1DBTableView1NAZIV2: TcxGridDBColumn
          Caption = 'Model'
          DataBinding.FieldName = 'NAZIV2'
          Visible = False
          HeaderAlignmentHorz = taCenter
          HeaderGlyphAlignmentHorz = taCenter
          Options.Editing = False
          Width = 165
        end
        object cxGrid1DBTableView1TAPACIR_CEL_STOL: TcxGridDBColumn
          Caption = 'Whole Upholstery'
          DataBinding.FieldName = 'TAPACIR_CEL_STOL'
          HeaderAlignmentHorz = taCenter
          Options.Editing = False
          Width = 98
        end
        object cxGrid1DBTableView1TAPACIR_SEDISTE: TcxGridDBColumn
          Caption = 'Seat Upholstery'
          DataBinding.FieldName = 'TAPACIR_SEDISTE'
          HeaderAlignmentHorz = taCenter
          HeaderGlyphAlignmentHorz = taCenter
          Options.Editing = False
          Width = 174
        end
        object cxGrid1DBTableView1TAPACIR_NAZAD: TcxGridDBColumn
          Caption = 'Back Upholstery'
          DataBinding.FieldName = 'TAPACIR_NAZAD'
          HeaderAlignmentHorz = taCenter
          HeaderGlyphAlignmentHorz = taCenter
          Options.Editing = False
          Width = 178
        end
        object cxGrid1DBTableView1BOJA_NOGARKI: TcxGridDBColumn
          Caption = 'Leg color'
          DataBinding.FieldName = 'BOJA_NOGARKI'
          HeaderAlignmentHorz = taCenter
          HeaderGlyphAlignmentHorz = taCenter
          Options.Editing = False
          Width = 78
        end
        object cxGrid1DBTableView1BROJ_PORACKA_STAVKA: TcxGridDBColumn
          Caption = 'Order number'
          DataBinding.FieldName = 'BROJ_PORACKA_STAVKA'
          HeaderAlignmentHorz = taCenter
          HeaderGlyphAlignmentHorz = taCenter
          Options.Editing = False
          Width = 100
        end
        object cxGrid1DBTableView1KOL_TRAILER: TcxGridDBColumn
          Caption = 'Pcs'
          DataBinding.FieldName = 'KOL_TRAILER'
          HeaderAlignmentHorz = taCenter
          HeaderGlyphAlignmentHorz = taCenter
          Options.Editing = False
        end
        object cxGrid1DBTableView1GLIDERS: TcxGridDBColumn
          Caption = 'Gliders'
          DataBinding.FieldName = 'GLIDERS'
          HeaderAlignmentHorz = taCenter
          HeaderGlyphAlignmentHorz = taCenter
          Options.Editing = False
          Width = 179
        end
        object cxGrid1DBTableView1ZABELESKA: TcxGridDBColumn
          Caption = 'Remarks'
          DataBinding.FieldName = 'ZABELESKA'
          HeaderAlignmentHorz = taCenter
          HeaderGlyphAlignmentHorz = taCenter
          Options.Editing = False
          Width = 217
        end
        object cxGrid1DBTableView1ID: TcxGridDBColumn
          DataBinding.FieldName = 'ID'
          Visible = False
          HeaderAlignmentHorz = taCenter
          HeaderGlyphAlignmentHorz = taCenter
          Options.Editing = False
        end
        object cxGrid1DBTableView1OPIS: TcxGridDBColumn
          DataBinding.FieldName = 'OPIS'
          Visible = False
          HeaderAlignmentHorz = taCenter
          HeaderGlyphAlignmentHorz = taCenter
          Options.Editing = False
        end
        object cxGrid1DBTableView1ID_PORACKA_STAVKA: TcxGridDBColumn
          DataBinding.FieldName = 'ID_PORACKA_STAVKA'
          Visible = False
          HeaderAlignmentHorz = taCenter
          HeaderGlyphAlignmentHorz = taCenter
          Options.Editing = False
        end
        object cxGrid1DBTableView1ID_TRAILER: TcxGridDBColumn
          DataBinding.FieldName = 'ID_TRAILER'
          Visible = False
          HeaderAlignmentHorz = taCenter
          HeaderGlyphAlignmentHorz = taCenter
          Options.Editing = False
        end
        object cxGrid1DBTableView1VID_ARTIKAL: TcxGridDBColumn
          DataBinding.FieldName = 'VID_ARTIKAL'
          Visible = False
          HeaderAlignmentHorz = taCenter
          HeaderGlyphAlignmentHorz = taCenter
          Options.Editing = False
        end
        object cxGrid1DBTableView1ARTIKAL: TcxGridDBColumn
          DataBinding.FieldName = 'ARTIKAL'
          Visible = False
          HeaderAlignmentHorz = taCenter
          HeaderGlyphAlignmentHorz = taCenter
          Options.Editing = False
        end
        object cxGrid1DBTableView1NAZIV_ARTIKAL: TcxGridDBColumn
          DataBinding.FieldName = 'NAZIV_ARTIKAL'
          Visible = False
          HeaderAlignmentHorz = taCenter
          HeaderGlyphAlignmentHorz = taCenter
          Options.Editing = False
        end
        object cxGrid1DBTableView1CUSTOMER_NAME: TcxGridDBColumn
          Caption = 'Customer Name'
          DataBinding.FieldName = 'CUSTOMER_NAME'
          HeaderAlignmentHorz = taCenter
          Options.Editing = False
          Width = 100
        end
        object cxGrid1DBTableView1IZVEDENA_EM: TcxGridDBColumn
          DataBinding.FieldName = 'IZVEDENA_EM'
          Visible = False
          HeaderAlignmentHorz = taCenter
          HeaderGlyphAlignmentHorz = taCenter
          Options.Editing = False
        end
        object cxGrid1DBTableView1KOEFICIENT: TcxGridDBColumn
          DataBinding.FieldName = 'KOEFICIENT'
          Visible = False
          HeaderAlignmentHorz = taCenter
          HeaderGlyphAlignmentHorz = taCenter
          Options.Editing = False
        end
        object cxGrid1DBTableView1NAZIV_MERKA: TcxGridDBColumn
          DataBinding.FieldName = 'NAZIV_MERKA'
          Visible = False
          HeaderAlignmentHorz = taCenter
          HeaderGlyphAlignmentHorz = taCenter
          Options.Editing = False
        end
        object cxGrid1DBTableView1VK_KOLICINA: TcxGridDBColumn
          DataBinding.FieldName = 'VK_KOLICINA'
          Visible = False
          HeaderAlignmentHorz = taCenter
          HeaderGlyphAlignmentHorz = taCenter
          Options.Editing = False
        end
        object cxGrid1DBTableView1BR_KUTII: TcxGridDBColumn
          DataBinding.FieldName = 'BR_KUTII'
          Visible = False
          HeaderAlignmentHorz = taCenter
          HeaderGlyphAlignmentHorz = taCenter
          Options.Editing = False
        end
        object cxGrid1DBTableView1VOLUMEN: TcxGridDBColumn
          DataBinding.FieldName = 'VOLUMEN'
          Visible = False
          HeaderAlignmentHorz = taCenter
          HeaderGlyphAlignmentHorz = taCenter
          Options.Editing = False
        end
        object cxGrid1DBTableView1SUNGER: TcxGridDBColumn
          Caption = 'Foam'
          DataBinding.FieldName = 'SUNGER'
          HeaderAlignmentHorz = taCenter
          HeaderGlyphAlignmentHorz = taCenter
          Options.Editing = False
        end
        object cxGrid1DBTableView1LABELS: TcxGridDBColumn
          Caption = 'Labels'
          DataBinding.FieldName = 'LABELS'
          HeaderAlignmentHorz = taCenter
          HeaderGlyphAlignmentHorz = taCenter
          Options.Editing = False
        end
        object cxGrid1DBTableView1TIP_PARTNER: TcxGridDBColumn
          DataBinding.FieldName = 'TIP_PARTNER'
          Visible = False
          HeaderAlignmentHorz = taCenter
          HeaderGlyphAlignmentHorz = taCenter
          Options.Editing = False
        end
        object cxGrid1DBTableView1PARTNER: TcxGridDBColumn
          DataBinding.FieldName = 'PARTNER'
          Visible = False
          HeaderAlignmentHorz = taCenter
          HeaderGlyphAlignmentHorz = taCenter
          Options.Editing = False
        end
        object cxGrid1DBTableView1NAZIV_PARNER: TcxGridDBColumn
          DataBinding.FieldName = 'NAZIV_PARNER'
          Visible = False
          HeaderAlignmentHorz = taCenter
          HeaderGlyphAlignmentHorz = taCenter
          Options.Editing = False
        end
        object cxGrid1DBTableView1BROJ_RN: TcxGridDBColumn
          DataBinding.FieldName = 'BROJ_RN'
          Visible = False
          HeaderAlignmentHorz = taCenter
          Options.Editing = False
          Width = 100
        end
        object cxGrid1DBTableView1ISPORAKA_OD_DATUM: TcxGridDBColumn
          Caption = 'Order Date'
          DataBinding.FieldName = 'ISPORAKA_OD_DATUM'
          HeaderAlignmentHorz = taCenter
          Options.Editing = False
        end
        object cxGrid1DBTableView1ISPORAKA_DO_DATUM: TcxGridDBColumn
          Caption = 'Loading Date'
          DataBinding.FieldName = 'ISPORAKA_DO_DATUM'
          HeaderAlignmentHorz = taCenter
          Options.Editing = False
        end
        object cxGrid1DBTableView1LOT: TcxGridDBColumn
          DataBinding.FieldName = 'LOT'
          Visible = False
          HeaderAlignmentHorz = taCenter
          Options.Editing = False
        end
        object cxGrid1DBTableView1PALETA_BR1: TcxGridDBColumn
          DataBinding.FieldName = 'PALETA_BR1'
          Visible = False
          HeaderAlignmentHorz = taCenter
          Options.Editing = False
          Width = 66
        end
        object cxGrid1DBTableView1PALETA_BR2: TcxGridDBColumn
          DataBinding.FieldName = 'PALETA_BR2'
          Visible = False
          HeaderAlignmentHorz = taCenter
          Options.Editing = False
          Width = 67
        end
        object cxGrid1DBTableView1PALETA_BR3: TcxGridDBColumn
          DataBinding.FieldName = 'PALETA_BR3'
          Visible = False
          HeaderAlignmentHorz = taCenter
          Options.Editing = False
          Width = 73
        end
        object cxGrid1DBTableView1KEPER: TcxGridDBColumn
          Caption = 'Keper'
          DataBinding.FieldName = 'KEPER'
          HeaderAlignmentHorz = taCenter
          Options.Editing = False
        end
        object cxGrid1DBTableView1ZAB_TRAILER: TcxGridDBColumn
          Caption = 'Remark'
          DataBinding.FieldName = 'ZAB_TRAILER'
          HeaderAlignmentHorz = taCenter
          Width = 216
        end
        object cxGrid1DBTableView1VENEER_TYPE: TcxGridDBColumn
          Caption = 'VENEER TYPE'
          DataBinding.FieldName = 'VENEER_TYPE'
          HeaderAlignmentHorz = taCenter
          Width = 121
        end
        object cxGrid1DBTableView1VENEER_COLOR: TcxGridDBColumn
          Caption = 'VENEER COLOR'
          DataBinding.FieldName = 'VENEER_COLOR'
          HeaderAlignmentHorz = taCenter
          Width = 119
        end
        object cxGrid1DBTableView1METAL_COLOR: TcxGridDBColumn
          Caption = 'METAL COLOR'
          DataBinding.FieldName = 'METAL_COLOR'
          HeaderAlignmentHorz = taCenter
          Width = 119
        end
        object cxGrid1DBTableView1WOOD_TYPE: TcxGridDBColumn
          Caption = 'WOOD TYPE'
          DataBinding.FieldName = 'WOOD_TYPE'
          HeaderAlignmentHorz = taCenter
          Width = 115
        end
        object cxGrid1DBTableView1LABEL_ON_SEAT: TcxGridDBColumn
          Caption = 'LABEL ON SEAT'
          DataBinding.FieldName = 'LABEL_ON_SEAT'
          HeaderAlignmentHorz = taCenter
          Width = 123
        end
        object cxGrid1DBTableView1LABEL_ON_BACK: TcxGridDBColumn
          Caption = 'LABEL ON BACK'
          DataBinding.FieldName = 'LABEL_ON_BACK'
          HeaderAlignmentHorz = taCenter
          Width = 152
        end
        object cxGrid1DBTableView1EMBROIDERY: TcxGridDBColumn
          DataBinding.FieldName = 'EMBROIDERY'
          HeaderAlignmentHorz = taCenter
          Width = 109
        end
        object cxGrid1DBTableView1PIPING: TcxGridDBColumn
          DataBinding.FieldName = 'PIPING'
          HeaderAlignmentHorz = taCenter
          Width = 67
        end
        object cxGrid1DBTableView1STUDS: TcxGridDBColumn
          DataBinding.FieldName = 'STUDS'
          HeaderAlignmentHorz = taCenter
          Width = 70
        end
        object cxGrid1DBTableView1SPECIAL_PRICE: TcxGridDBColumn
          Caption = 'SPECIAL PRICE'
          DataBinding.FieldName = 'SPECIAL_PRICE'
          HeaderAlignmentHorz = taCenter
          Width = 95
        end
        object cxGrid1DBTableView1LABEL_ON_ARMS: TcxGridDBColumn
          Caption = 'LABEL ON ARMS'
          DataBinding.FieldName = 'LABEL_ON_ARMS'
          HeaderAlignmentHorz = taCenter
          Width = 123
        end
        object cxGrid1DBTableView1TAPACIR: TcxGridDBColumn
          DataBinding.FieldName = 'TAPACIR'
          Visible = False
        end
        object cxGrid1DBTableView1SUR: TcxGridDBColumn
          DataBinding.FieldName = 'SUR'
        end
        object cxGrid1DBTableView1SITE_MAT: TcxGridDBColumn
          DataBinding.FieldName = 'SITE_MAT'
          Width = 300
        end
        object cxGrid1DBTableView1SITE_MAT_MAK: TcxGridDBColumn
          DataBinding.FieldName = 'SITE_MAT_MAK'
          Options.Editing = False
          Width = 300
        end
        object cxGrid1DBTableView1SKU: TcxGridDBColumn
          DataBinding.FieldName = 'SKU'
          Width = 50
        end
      end
      object cxGrid1DBTableView2: TcxGridDBTableView
        Navigator.Buttons.CustomButtons = <>
        ScrollbarAnnotations.CustomAnnotations = <>
        DataController.DataSource = dm.dsOperaciiKamion
        DataController.Options = [dcoAssignGroupingValues, dcoAssignMasterDetailKeys, dcoSaveExpanding, dcoGroupsAlwaysExpanded]
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        OptionsView.GroupFooters = gfAlwaysVisible
        object cxGrid1DBTableView2KAMION: TcxGridDBColumn
          Caption = #1050#1072#1084#1080#1086#1085
          DataBinding.FieldName = 'KAMION'
          Visible = False
          GroupIndex = 0
        end
        object cxGrid1DBTableView2OPERACIJA: TcxGridDBColumn
          Caption = #1054#1087#1077#1088#1072#1094#1080#1112#1072
          DataBinding.FieldName = 'OPERACIJA'
          Width = 227
        end
        object cxGrid1DBTableView2POTREBNO_VREME: TcxGridDBColumn
          Caption = #1055#1086#1090#1088#1077#1073#1085#1086' '#1074#1088#1077#1084#1077
          DataBinding.FieldName = 'POTREBNO_VREME'
          Width = 74
        end
        object cxGrid1DBTableView2MERNA_EDINICA: TcxGridDBColumn
          Caption = #1052#1077#1088#1085#1072' '#1077#1076#1080#1085#1080#1094#1072
          DataBinding.FieldName = 'MERNA_EDINICA'
          Width = 78
        end
      end
      object cxGrid1DBTableView3: TcxGridDBTableView
        Navigator.Buttons.CustomButtons = <>
        ScrollbarAnnotations.CustomAnnotations = <>
        DataController.DataSource = dsOperaciiKamion
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        object cxGrid1DBTableView3OPERACIJA: TcxGridDBColumn
          Caption = #1054#1087#1077#1088#1072#1094#1080#1112#1072
          DataBinding.FieldName = 'OPERACIJA'
          Width = 250
        end
        object cxGrid1DBTableView3POTREBNO_VREME: TcxGridDBColumn
          Caption = #1055#1086#1090#1088#1077#1073#1085#1086' '#1074#1088#1077#1084#1077
          DataBinding.FieldName = 'POTREBNO_VREME'
          Width = 100
        end
        object cxGrid1DBTableView3MERNA_EDINICA: TcxGridDBColumn
          Caption = #1052#1077#1088#1082#1072
          DataBinding.FieldName = 'MERNA_EDINICA'
          Width = 100
        end
      end
      object cxGrid1DBTableView4: TcxGridDBTableView
        Navigator.Buttons.CustomButtons = <>
        ScrollbarAnnotations.CustomAnnotations = <>
        DataController.DataSource = dsTapacir
        DataController.DetailKeyFieldNames = 'ID_PORACKA_STAVKA'
        DataController.KeyFieldNames = 'ID_PORACKA_STAVKA'
        DataController.MasterKeyFieldNames = 'ID_PORACKA_STAVKA'
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        OptionsData.Deleting = False
        OptionsData.Editing = False
        OptionsData.Inserting = False
        OptionsView.GroupByBox = False
        object cxGrid1DBTableView4ID_PORACKA_STAVKA: TcxGridDBColumn
          DataBinding.FieldName = 'ID_PORACKA_STAVKA'
          Visible = False
        end
        object cxGrid1DBTableView4VID_TAPACIR: TcxGridDBColumn
          DataBinding.FieldName = 'VID_TAPACIR'
          Visible = False
        end
        object cxGrid1DBTableView4TAPACIR: TcxGridDBColumn
          DataBinding.FieldName = 'TAPACIR'
          Visible = False
        end
        object cxGrid1DBTableView4NAZIV_TAPACIR: TcxGridDBColumn
          Caption = #1042#1080#1076' '#1085#1072' '#1090#1072#1087#1072#1094#1080#1088
          DataBinding.FieldName = 'NAZIV_TAPACIR'
          Width = 200
        end
        object cxGrid1DBTableView4VID_SUROVINA: TcxGridDBColumn
          Caption = #1042#1080#1076' '#1085#1072' '#1084#1072#1090#1077#1088#1080#1112#1072#1083
          DataBinding.FieldName = 'VID_SUROVINA'
        end
        object cxGrid1DBTableView4SUROVINA: TcxGridDBColumn
          Caption = #1064#1080#1092#1088#1072' '#1085#1072' '#1084#1072#1090#1077#1088#1080#1112#1072#1083
          DataBinding.FieldName = 'SUROVINA'
        end
        object cxGrid1DBTableView4NAZIV_SUROVINA: TcxGridDBColumn
          Caption = #1052#1072#1090#1077#1088#1080#1112#1072#1083
          DataBinding.FieldName = 'NAZIV_SUROVINA'
          Width = 200
        end
        object cxGrid1DBTableView4KOLICINA: TcxGridDBColumn
          Caption = #1050#1086#1083#1080#1095#1080#1085#1072
          DataBinding.FieldName = 'KOLICINA'
        end
        object cxGrid1DBTableView4MERKA: TcxGridDBColumn
          Caption = #1052#1077#1088#1082#1072
          DataBinding.FieldName = 'MERKA'
        end
        object cxGrid1DBTableView4PALETA: TcxGridDBColumn
          Caption = #1055#1072#1083#1077#1090#1072
          DataBinding.FieldName = 'PALETA'
          Width = 100
        end
      end
      object cxGrid1Level1: TcxGridLevel
        Caption = #1058#1086#1074#1072#1088#1077#1085' '#1051#1080#1089#1090'  '
        GridView = cxGrid1DBTableView1
        object cxGrid1Level4: TcxGridLevel
          GridView = cxGrid1DBTableView4
          Visible = False
        end
      end
      object cxGrid1Level2: TcxGridLevel
        Caption = #1054#1087#1077#1088#1072#1094#1080#1080' '#1087#1086' '#1082#1072#1084#1080#1086#1085'  '
        GridView = cxGrid1DBTableView2
      end
      object cxGrid1Level3: TcxGridLevel
        Caption = #1054#1087#1077#1088#1072#1094#1080#1080' '#1079#1072' '#1082#1072#1084#1080#1086#1085
        GridView = cxGrid1DBTableView3
        Visible = False
      end
    end
  end
  object dxSpreadSheet1: TdxSpreadSheet
    Left = 8
    Top = 248
    Width = 841
    Height = 337
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    OptionsBehavior.IterativeCalculationMaxCount = 0
    ParentFont = False
    Styles.Content = dmRes.cxStyle225
    Styles.Header = dmRes.cxStyle110
    Visible = False
    Data = {
      8E02000044585353763242461000000042465320000000000000000001000101
      010100000000000001004246532000000000424653200100000001000000200B
      00000007000000430061006C0069006200720069000000000000002000000020
      0000000020000000000020000000000020000000000020000007000000470045
      004E004500520041004C00000000000002000000000000000001424653200100
      0000424653201700000054006400780053007000720065006100640053006800
      6500650074005400610062006C00650056006900650077000600000053006800
      650065007400310001FFFFFFFFFFFFFFFF640000000200000002000000020000
      0055000000140000000200000002000000000200000002000000000000010000
      0000000101000042465320550000000000000042465320000000004246532014
      0000000000000042465320000000000000000000000000010000000000000000
      0000000000000000000000424653200000000002020000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000000000000000000000000000064000000000000000000000000000000
      0000000000000000000000000000000000000000000000000000000000000000
      0000000200020200020000000000000000000000000000000000020000000000
      0000000000000000000000000000000000000000000000000000000000000202
      0000000000000000424653200000000000000000000000000000000000000000
      07000000620069006C006A0061006E0061000000000000000000000000000000
      0000C818CFC1F824E6400000000000000000}
  end
  object cxGridPopupMenu1: TcxGridPopupMenu
    Grid = cxGrid1
    PopupMenus = <>
    Left = 48
    Top = 597
  end
  object PopupMenu1: TPopupMenu
    Left = 144
    Top = 629
    object N1: TMenuItem
      Caption = #1057#1085#1080#1084#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      ShortCut = 49235
      OnClick = SaveToIniFileExecute
    end
    object Excel1: TMenuItem
      Caption = #1057#1085#1080#1084#1080' '#1075#1086' '#1074#1086' Excel'
      ShortCut = 49221
    end
  end
  object dxBarManager1: TdxBarManager
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Segoe UI'
    Font.Style = []
    Categories.Strings = (
      'Default')
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    ImageOptions.Images = dmRes.cxSmallImages
    ImageOptions.LargeImages = dmRes.cxLargeImages
    PopupMenuLinks = <>
    UseSystemFont = True
    Left = 144
    Top = 597
    PixelsPerInch = 96
    object dxBarManager1Bar2: TdxBar
      Caption = #1040#1082#1094#1080#1080
      CaptionButtons = <>
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 789
      FloatTop = 120
      FloatClientWidth = 51
      FloatClientHeight = 120
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxbrlrgbtn2'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton10'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton5'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarLargeButton18'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarSubItem1'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxbrlrgbtn1'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton21'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton20'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton22'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar3: TdxBar
      Caption = #1048#1079#1083#1077#1079
      CaptionButtons = <>
      DockedLeft = 952
      DockedTop = 0
      FloatLeft = 718
      FloatTop = 62
      FloatClientWidth = 51
      FloatClientHeight = 80
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton7'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton8'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar4: TdxBar
      Caption = #1055#1077#1095#1072#1090#1077#1114#1077
      CaptionButtons = <>
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 676
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton12'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton15'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton9'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarLargeButton13'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1BarIzgledGrid: TdxBar
      Caption = #1058#1072#1073#1077#1083#1072
      CaptionButtons = <>
      DockedLeft = 363
      DockedTop = 0
      FloatLeft = 808
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLBtnSnimiIzgled'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarLBtnBrisiIzgled'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarLargeButton2: TdxBarLargeButton
      Action = aAzuriraj
      Category = 0
    end
    object dxBarLargeButton3: TdxBarLargeButton
      Action = aBrisi
      Category = 0
    end
    object dxBarLargeButton4: TdxBarLargeButton
      Action = aSnimiIzgled
      Category = 0
      ScreenTip = tipSnimiIzgled
    end
    object dxBarLargeButton5: TdxBarLargeButton
      Action = aZacuvajExcel
      Category = 0
      ScreenTip = tipZacuvajExcel
    end
    object dxBarLargeButton6: TdxBarLargeButton
      Action = aPecatiTabela
      Caption = #1055#1077#1095#1072#1090#1080' '#1090#1072#1073#1077#1083#1072
      Category = 0
    end
    object dxBarLargeButton7: TdxBarLargeButton
      Action = aHelp
      Category = 0
    end
    object dxBarLargeButton8: TdxBarLargeButton
      Action = aIzlez
      Category = 0
    end
    object dxBarLargeButton1: TdxBarLargeButton
      Action = aNov
      Category = 0
    end
    object dxBarLargeButton9: TdxBarLargeButton
      Action = aSnimiPecatenje
      Category = 0
      ScreenTip = tipSnimiKonf
    end
    object dxBarLargeButton11: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object ribbonFilterReset: TdxBarLargeButton
      Caption = #1056#1077#1089#1077#1090#1080#1088#1072#1112' '#1092#1080#1083#1090#1077#1088
      Category = 0
      Hint = #1056#1077#1089#1077#1090#1080#1088#1072#1112' '#1092#1080#1083#1090#1077#1088
      Visible = ivAlways
    end
    object dxBarLargeButton12: TdxBarLargeButton
      Action = aPodesuvanjePecatenje
      Category = 0
      ScreenTip = tipDizajnReport
    end
    object dxBarLargeButton13: TdxBarLargeButton
      Action = aBrisiPodesuvanjePecatenje
      Category = 0
      ScreenTip = tipBrisiKonf
    end
    object dxBarLargeButton14: TdxBarLargeButton
      Caption = #1048#1079#1073#1088#1080#1096#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      Category = 0
      Visible = ivAlways
      LargeImageIndex = 40
    end
    object dxBarLargeButton15: TdxBarLargeButton
      Action = aPageSetup
      Category = 0
      ScreenTip = tipSetiranjeStrana
    end
    object dxBarLargeButton16: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object dxBarLargeButton17: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object cxBarEditItem1: TcxBarEditItem
      Caption = 'New Item'
      Category = 0
      Hint = 'New Item'
      Visible = ivAlways
      PropertiesClassName = 'TcxCheckBoxProperties'
    end
    object dxBarSubItem1: TdxBarSubItem
      Caption = #1055#1077#1095#1072#1090#1077#1114#1077
      Category = 0
      Visible = ivAlways
      ImageIndex = 30
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton6'
        end>
    end
    object dxBarLBtnSnimiIzgled: TdxBarLargeButton
      Action = aSnimiIzgled
      Category = 0
    end
    object dxBarLBtnBrisiIzgled: TdxBarLargeButton
      Action = aBrisiIzgled
      Category = 0
    end
    object dxBarLargeButton10: TdxBarLargeButton
      Action = aSpustiSoberi
      Category = 0
    end
    object dxbrlrgbtn1: TdxBarLargeButton
      Action = aTovarenList
      Caption = #1055#1083#1072#1085' '#1079#1072' '#1090#1086#1074#1072#1088#1077#1085' '#1083#1080#1089#1090' - '#1058#1072#1087#1072#1094#1077#1088#1089#1082#1086
      Category = 0
      Visible = ivNever
    end
    object dxBarLargeButton18: TdxBarLargeButton
      Action = aZacuvajExcelMS
      Caption = #1047#1072#1095#1091#1074#1072#1112' '#1074#1086' '#1045#1082#1089#1077#1083' '#1079#1072' '#1082#1083#1080#1077#1085#1090
      Category = 0
    end
    object dxbrlrgbtn2: TdxBarLargeButton
      Action = aGenerirajEtiketi
      Category = 0
    end
    object dxBarLargeButton19: TdxBarLargeButton
      Action = aTovarenListMashinsko
      Caption = #1055#1083#1072#1085' '#1090#1086#1074#1072#1088#1077#1085' '#1083#1080#1089#1090' - '#1052#1072#1096#1080#1085#1089#1082#1086
      Category = 0
    end
    object dxBarLargeButton20: TdxBarLargeButton
      Action = aTovarenListFarbara
      Caption = #1055#1083#1072#1085' '#1079#1072' '#1090#1086#1074#1072#1088#1077#1085' '#1083#1080#1089#1090' - '#1052#1072#1096#1080#1085#1089#1082#1086'/'#1060#1072#1088#1073#1072#1088#1072
      Category = 0
      Visible = ivNever
    end
    object dxBarButton1: TdxBarButton
      Action = aDizajnTovarenListNovo
      Category = 0
    end
    object dxBarLargeButton21: TdxBarLargeButton
      Action = aTovaenListNovo
      Caption = #1055#1083#1072#1085' '#1079#1072' '#1090#1086#1074#1072#1088#1077#1085' '#1083#1080#1089#1090' - '#1058#1072#1087#1072#1094#1077#1088#1089#1082#1086
      Category = 0
    end
    object dxBarLargeButton22: TdxBarLargeButton
      Action = aTovarenListMFNovo
      Caption = #1055#1083#1072#1085' '#1079#1072' '#1090#1086#1074#1072#1088#1077#1085' '#1083#1080#1089#1090' - '#1052#1072#1096#1080#1085#1089#1082#1086'/'#1060#1072#1088#1073#1072#1088#1072
      Category = 0
    end
  end
  object ActionList1: TActionList
    Images = dmRes.cxSmallImages
    Left = 16
    Top = 597
    object aNov: TAction
      Caption = #1053#1086#1074
      ImageIndex = 10
      ShortCut = 116
      OnExecute = aNovExecute
    end
    object aAzuriraj: TAction
      Caption = #1040#1078#1091#1088#1080#1088#1072#1112
      ImageIndex = 12
      ShortCut = 117
      OnExecute = aAzurirajExecute
    end
    object aBrisi: TAction
      Caption = #1041#1088#1080#1096#1080
      ImageIndex = 11
      ShortCut = 119
      OnExecute = aBrisiExecute
    end
    object aSnimiIzgled: TAction
      Caption = #1057#1085#1080#1084#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      ImageIndex = 9
      ShortCut = 24659
      OnExecute = aSnimiIzgledExecute
    end
    object aZacuvajExcel: TAction
      Caption = #1047#1072#1095#1091#1074#1072#1112' '#1074#1086' Excel'
      ImageIndex = 9
      ShortCut = 24645
      OnExecute = aZacuvajExcelExecute
    end
    object aPecatiTabela: TAction
      Caption = #1055#1077#1095#1072#1090#1080
      ImageIndex = 30
      OnExecute = aPecatiTabelaExecute
    end
    object aHelp: TAction
      Caption = #1055#1086#1084#1086#1096
      ImageIndex = 24
    end
    object aIzlez: TAction
      Caption = #1048#1079#1083#1077#1079
      ImageIndex = 25
      OnExecute = aIzlezExecute
    end
    object aZapisi: TAction
      Caption = #1047#1072#1087#1080#1096#1080
      ImageIndex = 7
      ShortCut = 120
      OnExecute = aZapisiExecute
    end
    object aOtkazi: TAction
      Caption = #1054#1090#1082#1072#1078#1080
      ImageIndex = 21
      ShortCut = 27
      OnExecute = aOtkaziExecute
    end
    object aRefresh: TAction
      Caption = 'aRefresh'
      ImageIndex = 18
      ShortCut = 118
      OnExecute = aRefreshExecute
    end
    object aSnimiPecatenje: TAction
      Caption = #1057#1085#1080#1084#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      ImageIndex = 39
      OnExecute = aSnimiPecatenjeExecute
    end
    object aPodesuvanjePecatenje: TAction
      Caption = #1044#1080#1079#1072#1112#1085' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090
      ImageIndex = 38
      OnExecute = aPodesuvanjePecatenjeExecute
    end
    object aBrisiPodesuvanjePecatenje: TAction
      Caption = #1041#1088#1080#1096#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      ImageIndex = 37
      OnExecute = aBrisiPodesuvanjePecatenjeExecute
    end
    object aPageSetup: TAction
      Caption = #1057#1077#1090#1080#1088#1072#1114#1077' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072
      ImageIndex = 38
      OnExecute = aPageSetupExecute
    end
    object aFormConfig: TAction
      Caption = 'aFormConfig'
      ShortCut = 24699
      OnExecute = aFormConfigExecute
    end
    object aBrisiIzgled: TAction
      Caption = #1041#1088#1080#1096#1080' '#1075#1086' '#1079#1072#1095#1091#1074#1072#1085#1080#1086#1090' '#1080#1079#1075#1083#1077#1076
      ImageIndex = 56
      OnExecute = aBrisiIzgledExecute
    end
    object aSpustiSoberi: TAction
      Caption = #1057#1087#1091#1096#1090#1080'/'#1057#1086#1073#1077#1088#1080' '#1058#1072#1073#1077#1083#1072
      ImageIndex = 65
      OnExecute = aSpustiSoberiExecute
    end
    object aTovarenList: TAction
      Caption = #1055#1083#1072#1085' '#1079#1072' '#1058#1086#1074#1072#1088#1077#1085' '#1051#1080#1089#1090
      ImageIndex = 30
      ShortCut = 121
      OnExecute = aTovarenListExecute
    end
    object aDizajnerTovarenList: TAction
      Caption = 'aDizajnerTovarenList'
      ShortCut = 24697
      OnExecute = aDizajnerTovarenListExecute
    end
    object aZacuvajExcelMS: TAction
      Caption = #1047#1072#1095#1091#1074#1072#1112' '#1074#1086' '#1045#1082#1089#1077#1083' 2'
      ImageIndex = 9
      OnExecute = aZacuvajExcelMSExecute
    end
    object aGenerirajEtiketi: TAction
      Caption = #1043#1077#1085#1077#1088#1080#1088#1072#1112' '#1045#1090#1080#1082#1077#1090#1080
      ImageIndex = 54
      OnExecute = aGenerirajEtiketiExecute
    end
    object aTovarenListMashinsko: TAction
      Caption = #1058#1086#1074#1072#1088#1077#1085' '#1051#1080#1089#1090' '#1052#1072#1096#1080#1085#1089#1082#1086
      ImageIndex = 30
      OnExecute = aTovarenListMashinskoExecute
    end
    object aDizajnerTovarenListM: TAction
      Caption = 'aDizajnerTovarenListM'
      ShortCut = 24653
      OnExecute = aDizajnerTovarenListMExecute
    end
    object aTovarenListFarbara: TAction
      Caption = #1055#1083#1072#1085' '#1079#1072' '#1090#1086#1074#1072#1088#1077#1085' '#1083#1080#1089#1090' - '#1060#1072#1088#1073#1072#1088#1072
      ImageIndex = 30
      OnExecute = aTovarenListFarbaraExecute
    end
    object aDizajnerTovarenListF: TAction
      Caption = 'aDizajnerTovarenListF'
      ShortCut = 24646
      OnExecute = aDizajnerTovarenListFExecute
    end
    object aDizajnTovarenListNovo: TAction
      Caption = 'aDizajnTovarenListNovo'
      ShortCut = 16468
      OnExecute = aDizajnTovarenListNovoExecute
    end
    object aTovaenListNovo: TAction
      Caption = #1055#1083#1072#1085' '#1079#1072' '#1090#1086#1074#1072#1088#1077#1085' '#1083#1080#1089#1090' - '#1058#1072#1087#1072#1094#1077#1088#1089#1082#1086' ('#1053#1086#1074#1086')'
      ImageIndex = 30
      OnExecute = aTovaenListNovoExecute
    end
    object aDizajnerTLMFNovo: TAction
      Caption = 'aDizajnerTLMFNovo'
      ShortCut = 16461
      OnExecute = aDizajnerTLMFNovoExecute
    end
    object aTovarenListMFNovo: TAction
      Caption = 'aTovarenListMFNovo'
      ImageIndex = 30
      OnExecute = aTovarenListMFNovoExecute
    end
  end
  object dxComponentPrinter1: TdxComponentPrinter
    CurrentLink = dxComponentPrinter1Link1
    Version = 0
    Left = 48
    Top = 629
    PixelsPerInch = 96
    object dxComponentPrinter1Link1: TdxGridReportLink
      Component = cxGrid1
      DateFormat = 0
      PageNumberFormat = pnfNumeral
      PrinterPage.DMPaper = 9
      PrinterPage.Footer = 6350
      PrinterPage.GrayShading = True
      PrinterPage.Header = 6350
      PrinterPage.Margins.Bottom = 12700
      PrinterPage.Margins.Left = 12700
      PrinterPage.Margins.Right = 12700
      PrinterPage.Margins.Top = 12700
      PrinterPage.Orientation = poLandscape
      PrinterPage.PageFooter.RightTitle.Strings = (
        #1057#1090#1088'.[Page #] '#1086#1076' [Total Pages]')
      PrinterPage.PageSize.X = 210000
      PrinterPage.PageSize.Y = 297000
      PrinterPage._dxMeasurementUnits_ = 0
      PrinterPage._dxLastMU_ = 2
      ReportDocument.CreationDate = 40336.425359629630000000
      TimeFormat = 0
      OptionsFormatting.UseNativeStyles = True
      OptionsOnEveryPage.Caption = False
      OptionsOnEveryPage.FilterBar = False
      OptionsView.Caption = False
      OptionsView.FilterBar = False
      StyleRepository = dmRes.cxStyleRepositoryPrinting
      Styles.StyleSheet = dmRes.dxGridReportLinkStyleSheet
      PixelsPerInch = 96
      BuiltInReportLink = True
    end
  end
  object dxBarScreenTipRepository1: TdxBarScreenTipRepository
    Left = 16
    Top = 629
    PixelsPerInch = 96
    object tipSnimiIzgled: TdxBarScreenTip
      Header.Text = #1057#1085#1080#1084#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      Description.Text = #1047#1072#1095#1091#1074#1072#1112' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090' '#1085#1072' '#1092#1086#1088#1084#1072#1090#1072' '#1080' '#1085#1072' '#1090#1072#1073#1077#1083#1072#1090#1072
    end
    object tipZacuvajExcel: TdxBarScreenTip
      Header.Text = #1047#1072#1095#1091#1074#1072#1112' '#1074#1086' Excel'
      Description.Text = #1057#1085#1080#1084#1080' '#1075#1080' '#1087#1086#1076#1072#1090#1086#1094#1080#1090#1077' '#1086#1076' '#1090#1072#1073#1077#1083#1072#1090#1072' '#1074#1086' Excel '#1092#1086#1088#1084#1072#1090
    end
    object tipDizajnReport: TdxBarScreenTip
      Header.Text = #1044#1080#1079#1072#1112#1085' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090
      Description.Text = #1055#1086#1076#1077#1089#1091#1074#1072#1114#1072' '#1085#1072' '#1080#1079#1075#1083#1077#1076#1086#1090' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090' '#1085#1072' '#1090#1072#1073#1077#1083#1072#1090#1072
    end
    object tipSetiranjeStrana: TdxBarScreenTip
      Header.Text = #1057#1077#1090#1080#1088#1072#1114#1077' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072
      Description.Text = #1055#1086#1076#1077#1089#1091#1074#1072#1114#1077' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072' '#1085#1072' '#1082#1086#1112#1072' '#1116#1077' '#1089#1077' '#1087#1077#1095#1072#1090#1080' '#1090#1072#1073#1077#1083#1072#1090#1072
    end
    object tipSnimiKonf: TdxBarScreenTip
      Header.Text = #1057#1085#1080#1084#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      Description.Text = #1047#1072#1095#1091#1074#1072#1112' '#1075#1080' '#1087#1086#1076#1077#1089#1091#1074#1072#1114#1072#1090#1072' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090' '#1080' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072' '#1079#1072' '#1087#1077#1095#1072#1090#1077#1114#1077
    end
    object tipBrisiKonf: TdxBarScreenTip
      Header.Text = #1041#1088#1080#1096#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      Description.Text = #1048#1079#1073#1088#1080#1096#1080' '#1075#1080' '#1087#1086#1076#1077#1089#1091#1074#1072#1114#1072#1090#1072' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090' '#1080' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072' '#1079#1072' '#1087#1077#1095#1072#1090#1077#1114#1077
    end
  end
  object tblOperaciiKamion: TpFIBDataSet
    RefreshSQL.Strings = (
      'select'
      '    t.naziv kamion,'
      '    a.naziv operacija,'
      '    sum( o.potrebno_vreme*pst.kolicina) potrebno_vreme,'
      '    o.merna_edinica'
      'from man_normativ_operacija o'
      'inner join man_normativ n on n.id = o.id_normativ and n.flag = 1'
      
        'inner join mtr_artikal ma on ma.artvid = n.vid_artikal and ma.id' +
        ' = n.artikal'
      
        'inner join man_poracka_stavka ps on ps.vid_artikal = ma.artvid a' +
        'nd ps.artikal = ma.id  and ps.status <> 3'
      
        'inner join man_por_s_trailer pst on pst.id_poracka_stavka = ps.i' +
        'd'
      
        'inner join man_trailer t on t.id = pst.id_trailer and t.status =' +
        ' 0'
      'inner join man_pr_operacija po on po.id = o.id_pr_operacija'
      'inner join man_aktivnost a on a.id = po.id_aktivnost'
      'where --o.id_normativ = :MAS_ID'
      '--t.id = :trailer'
      '--and'
      'a.aktiven = 1'
      'group by t.naziv, a.naziv,o.merna_edinica')
    SelectSQL.Strings = (
      'select'
      '    T.NAZIV KAMION,'
      '    A.NAZIV OPERACIJA,'
      '    sum(O.POTREBNO_VREME * PST.KOLICINA) POTREBNO_VREME,'
      '    O.MERNA_EDINICA'
      'from MAN_NORMATIV_OPERACIJA O'
      'inner join MAN_NORMATIV N on N.ID = O.ID_NORMATIV and'
      '      N.FLAG = 1'
      'inner join MTR_ARTIKAL MA on MA.ARTVID = N.VID_ARTIKAL and'
      '      MA.ID = N.ARTIKAL'
      
        'inner join MAN_PORACKA_STAVKA PS on PS.VID_ARTIKAL = MA.ARTVID a' +
        'nd'
      '      PS.ARTIKAL = MA.ID and'
      '      PS.STATUS <> 3'
      
        'inner join MAN_POR_S_TRAILER PST on PST.ID_PORACKA_STAVKA = PS.I' +
        'D'
      'inner join MAN_TRAILER T on T.ID = PST.ID_TRAILER and'
      '      T.STATUS = 0'
      'inner join MAN_PR_OPERACIJA PO on PO.ID = O.ID_PR_OPERACIJA'
      'inner join MAN_AKTIVNOST A on A.ID = PO.ID_AKTIVNOST'
      'where T.ID = :TRAILER'
      '      and A.AKTIVEN = 1'
      'group by T.NAZIV, A.NAZIV, O.MERNA_EDINICA  ')
    AutoUpdateOptions.UpdateTableName = 'MAN_PORACKA_STAVKA'
    AutoUpdateOptions.KeyFields = 'ID'
    AutoUpdateOptions.GeneratorName = 'GEN_MAN_PORACKA_STAVKA_ID'
    AutoUpdateOptions.WhenGetGenID = wgBeforePost
    AutoUpdateOptions.AutoParamsToFields = True
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 112
    Top = 629
    object tblOperaciiKamionKAMION: TFIBStringField
      FieldName = 'KAMION'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblOperaciiKamionOPERACIJA: TFIBStringField
      FieldName = 'OPERACIJA'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object ptbl1POTREBNO_VREME: TFIBFloatField
      FieldName = 'POTREBNO_VREME'
      DisplayFormat = '#,##0.000'
    end
    object tblOperaciiKamionMERNA_EDINICA: TFIBStringField
      FieldName = 'MERNA_EDINICA'
      Size = 10
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object dsOperaciiKamion: TDataSource
    DataSet = tblOperaciiKamion
    Left = 80
    Top = 597
  end
  object tblTapacir: TpFIBDataSet
    RefreshSQL.Strings = (
      
        'select PE.ID_PORACKA_STAVKA, PE.VID_TAPACIR, PE.TAPACIR, T.NAZIV' +
        ' NAZIV_TAPACIR, PE.VID_SUROVINA, PE.SUROVINA,'
      '       S.NAZIV NAZIV_SUROVINA, PE.KOLICINA, PE.MERKA, PE.PALETA'
      'from MAN_PORACKA_ELEMENT PE'
      'inner join MTR_ARTIKAL T on T.ARTVID = PE.VID_TAPACIR and'
      '      T.ID = PE.TAPACIR'
      'inner join MTR_ARTIKAL S on S.ARTVID = PE.VID_SUROVINA and'
      '      S.ID = PE.SUROVINA'
      '--where PE.ID_PORACKA_STAVKA = :ID_PORACKA_STAVKA')
    SelectSQL.Strings = (
      
        'select PE.ID_PORACKA_STAVKA, PE.VID_TAPACIR, PE.TAPACIR, T.NAZIV' +
        ' NAZIV_TAPACIR, PE.VID_SUROVINA, PE.SUROVINA,'
      '       S.NAZIV NAZIV_SUROVINA, PE.KOLICINA, PE.MERKA, PE.PALETA'
      'from MAN_PORACKA_ELEMENT PE'
      'inner join MTR_ARTIKAL T on T.ARTVID = PE.VID_TAPACIR and'
      '      T.ID = PE.TAPACIR'
      'inner join MTR_ARTIKAL S on S.ARTVID = PE.VID_SUROVINA and'
      '      S.ID = PE.SUROVINA'
      '--where PE.ID_PORACKA_STAVKA = :ID_PORACKA_STAVKA')
    AutoUpdateOptions.GeneratorName = 'GEN__ID'
    AutoUpdateOptions.WhenGetGenID = wgBeforePost
    AutoUpdateOptions.AutoParamsToFields = True
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 80
    Top = 629
    object tblTapacirID_PORACKA_STAVKA: TFIBBCDField
      FieldName = 'ID_PORACKA_STAVKA'
      Size = 0
    end
    object tblTapacirVID_TAPACIR: TFIBIntegerField
      FieldName = 'VID_TAPACIR'
    end
    object tblTapacirTAPACIR: TFIBIntegerField
      FieldName = 'TAPACIR'
    end
    object tblTapacirNAZIV_TAPACIR: TFIBStringField
      FieldName = 'NAZIV_TAPACIR'
      Size = 500
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblTapacirVID_SUROVINA: TFIBIntegerField
      FieldName = 'VID_SUROVINA'
    end
    object tblTapacirSUROVINA: TFIBIntegerField
      FieldName = 'SUROVINA'
    end
    object tblTapacirNAZIV_SUROVINA: TFIBStringField
      FieldName = 'NAZIV_SUROVINA'
      Size = 500
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblTapacirKOLICINA: TFIBFloatField
      FieldName = 'KOLICINA'
    end
    object tblTapacirMERKA: TFIBStringField
      FieldName = 'MERKA'
      Size = 5
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblTapacirPALETA: TFIBStringField
      FieldName = 'PALETA'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object dsTapacir: TDataSource
    DataSet = tblTapacir
    Left = 112
    Top = 597
  end
  object qZabTrailer: TpFIBQuery
    Transaction = dmKon.tProc
    Database = dmKon.fibBaza
    SQL.Strings = (
      'update man_trailer'
      'set zabeleska = :zab_trailer'
      'where id = :id_trailer')
    Left = 280
    Top = 609
    qoAutoCommit = True
    qoStartTransaction = True
  end
end
