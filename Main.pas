unit Main;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxGraphics, cxControls, cxLookAndFeels, cxLookAndFeelPainters,
  dxSkinsCore, dxSkinBlack, dxSkinBlue, dxSkinCaramel, dxSkinCoffee,
  dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy, dxSkinGlassOceans,
  dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky, dxSkinLondonLiquidSky,
  dxSkinMcSkin, dxSkinMoneyTwins, dxSkinOffice2007Black, dxSkinOffice2007Blue,
  dxSkinOffice2007Green, dxSkinOffice2007Pink, dxSkinOffice2007Silver,
  dxSkinOffice2010Black, dxSkinOffice2010Blue, dxSkinOffice2010Silver,
  dxSkinPumpkin, dxSkinSeven, dxSkinSharp, dxSkinSilver, dxSkinSpringTime,
  dxSkinStardust, dxSkinSummer2008, dxSkinsDefaultPainters, dxSkinValentine,
  dxSkinXmas2008Blue, dxSkinsdxRibbonPainter, dxStatusBar, dxRibbonStatusBar,
  cxClasses, dxRibbon, dxSkinsdxBarPainter, dxBar, jpeg, ExtCtrls,
  cxDropDownEdit, cxBarEditItem, ActnList, ImgList, cxContainer, cxEdit, cxLabel,
  dxRibbonSkins, Normativ, RabotniEdinici, dxSkinBlueprint,
  dxSkinDevExpressDarkStyle, dxSkinDevExpressStyle, dxSkinHighContrast,
  dxSkinSevenClassic, dxSkinSharpPlus, dxSkinTheAsphaltWorld, dxSkinVS2010,
  dxSkinWhiteprint, dxSkinsdxNavBarPainter, cxCustomData, cxStyles,
  dxSkinscxPCPainter, cxGridLevel, cxGridCustomView, cxGridChartView, cxGrid,
  cxCustomPivotGrid, cxDBPivotGrid, dxNavBarCollns, dxNavBarGroupItems,
  dxNavBarBase, dxNavBar, Vcl.DBCtrls, cxImage, cxDBEdit, cxCheckBox, cxDBLabel,
  Vcl.Menus, Vcl.StdCtrls, cxButtons, cxLookupEdit, cxDBLookupEdit,
  cxDBLookupComboBox, cxTextEdit, cxMaskEdit, cxCalendar, cxGroupBox, Data.DB,
  Xml.xmldom, Xml.XMLIntf, Xml.Win.msxmldom, Xml.XMLDoc, dxNavBarStyles,
  dxSkinOffice2013White, System.Actions, dxSkinMetropolis, dxSkinMetropolisDark,
  dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray, dxRibbonCustomizationForm,
  dxSkinOffice2016Colorful, dxSkinOffice2016Dark, dxSkinVisualStudio2013Blue,
  dxSkinVisualStudio2013Dark, dxSkinVisualStudio2013Light,
  dxSkinsdxNavBarAccordionViewPainter, System.ImageList, cxImageList,
  dxGDIPlusClasses;

type
  TfrmMain = class(TForm)
    rbMaticniPodatoci: TdxRibbonTab;
    dxRibbon1: TdxRibbon;
    dxRibbonStatusBar1: TdxRibbonStatusBar;
    dxBarManager1: TdxBarManager;
    PanelLogo: TPanel;
    Image1: TImage;
    PanelDole: TPanel;
    Image3: TImage;
    dxRibbon1TabPodesuvanja: TdxRibbonTab;
    dxBarManager1Bar1: TdxBar;
    dxBarEdit1: TdxBarEdit;
    cxBarSkin: TcxBarEditItem;
    dxBarButton1: TdxBarButton;
    ActionList1: TActionList;
    cxMainSmall: TcxImageList;
    cxMainLarge: TcxImageList;
    aSaveSkin: TAction;
    dxBarManager1BarIzlez: TdxBar;
    dxBarLargeButton1: TdxBarLargeButton;
    dxBarLargeButton2: TdxBarLargeButton;
    dxBarLargeButton3: TdxBarLargeButton;
    aHelp: TAction;
    aZabeleski: TAction;
    aIzlez: TAction;
    aAbout: TAction;
    dxBarLargeButton4: TdxBarLargeButton;
    dxBarManager1Bar2: TdxBar;
    aFormConfig: TAction;
    dxBarManager1Bar3: TdxBar;
    dxBarLargeButton5: TdxBarLargeButton;
    aPromeniLozinka: TAction;
    aArtVid: TAction;
    dxBarLargeButton6: TdxBarLargeButton;
    aArtikal: TAction;
    dxBarLargeButton7: TdxBarLargeButton;
    aNormativ: TAction;
    dxBarManager1Bar4: TdxBar;
    dxBarLargeButton8: TdxBarLargeButton;
    aRaboniEdinici: TAction;
    dxBarSubItem1: TdxBarSubItem;
    dxBarLargeButton9: TdxBarLargeButton;
    rtPlan: TdxRibbonTab;
    PanelMain: TPanel;
    aProizvodstvenResurs: TAction;
    dxBarLargeButton10: TdxBarLargeButton;
    aTipNormativ: TAction;
    dxBarLargeButton11: TdxBarLargeButton;
    aArtGrupa: TAction;
    dxBarLargeButton12: TdxBarLargeButton;
    aArtKategorija: TAction;
    dxBarLargeButton13: TdxBarLargeButton;
    dxBarManager1Bar7: TdxBar;
    dxBarLargeButton14: TdxBarLargeButton;
    dxBarManager1Bar8: TdxBar;
    dxBarLargeButton15: TdxBarLargeButton;
    aOperacijaPR: TAction;
    dxBarLargeButton16: TdxBarLargeButton;
    aMerka: TAction;
    dxBarManager1Bar5: TdxBar;
    dxBarLargeButton17: TdxBarLargeButton;
    dxBarLargeButton18: TdxBarLargeButton;
    aOperacija: TAction;
    dxBarManager1Bar6: TdxBar;
    dxBarLargeButton19: TdxBarLargeButton;
    dxBarLargeButton20: TdxBarLargeButton;
    aStatus: TAction;
    aGrupaStatus: TAction;
    aRabotenNalog: TAction;
    dxBarManager1Bar9: TdxBar;
    dxBarLargeButton21: TdxBarLargeButton;
    aCustomFields: TAction;
    dxBarLargeButton22: TdxBarLargeButton;
    Image2: TImage;
    Image4: TImage;
    aTipPR: TAction;
    dxBarLargeButton23: TdxBarLargeButton;
    lblDatumVreme: TcxLabel;
    dxBarLargeButton24: TdxBarLargeButton;
    aPrioritet: TAction;
    aTipPartner: TAction;
    dxBarLargeButton25: TdxBarLargeButton;
    dxBarLargeButton26: TdxBarLargeButton;
    aPartner: TAction;
    dxBarLargeButton27: TdxBarLargeButton;
    aObrabotkaRN: TAction;
    lblFirma: TcxDBLabel;
    dxBarManager1Bar10: TdxBar;
    aTekovenProces: TAction;
    dxBarLargeButton28: TdxBarLargeButton;
    dxBarLargeButton29: TdxBarLargeButton;
    aKontrolnaTocka: TAction;
    dxBarLargeButton30: TdxBarLargeButton;
    aKontrolnaT: TAction;
    aOperacijaKT: TAction;
    dxBarLargeButton31: TdxBarLargeButton;
    dxBarLargeButton32: TdxBarLargeButton;
    aPosledovatelnostKT: TAction;
    dxBarLargeButton33: TdxBarLargeButton;
    aKorisnikKT: TAction;
    aKTVlez: TAction;
    dxBarLargeButton34: TdxBarLargeButton;
    dxBarLargeButton35: TdxBarLargeButton;
    aPoracka: TAction;
    dxBarLargeButton36: TdxBarLargeButton;
    dxNavBar1: TdxNavBar;
    dxNavBar1Group1: TdxNavBarGroup;
    dxNavBar1Item1: TdxNavBarItem;
    dxNavBar1Item2: TdxNavBarItem;
    dxNavBar1Separator1: TdxNavBarSeparator;
    dxBarLargeButton37: TdxBarLargeButton;
    dxNavBar1Item3: TdxNavBarItem;
    dxNavBar1StyleItem1: TdxNavBarStyleItem;
    dxNavBar1Item4: TdxNavBarItem;
    aZavrsiRN: TAction;
    dxBarLargeButton38: TdxBarLargeButton;
    dxNavBar1Item5: TdxNavBarItem;
    aPotrosniMaterijali: TAction;
    btPotroseni: TdxBarLargeButton;
    btRasturKaloKrs: TdxBarLargeButton;
    aRasturKrsKalo: TAction;
    dxBarManager1Bar11: TdxBar;
    aReMagacin: TAction;
    aUserMagacinPravo: TAction;
    dxBarLargeButton39: TdxBarLargeButton;
    cxBarEditItem1: TcxBarEditItem;
    cbGodina: TcxBarEditItem;
    dxNavBar1Item6: TdxNavBarItem;
    aLagerLista: TAction;
    dxBarLargeButton40: TdxBarLargeButton;
    dxBarLargeButton41: TdxBarLargeButton;
    aArtKateg: TAction;
    dxBarButton2: TdxBarButton;
    dxBarButton3: TdxBarButton;
    dxBarManager1Bar12: TdxBar;
    dxBarButton4: TdxBarButton;
    aLinija: TAction;
    dxBarLargeButton42: TdxBarLargeButton;
    dxBarManager1Bar13: TdxBar;
    aPotrosenMaterijal: TAction;
    rbPregledi: TdxRibbonTab;
    dxBarManager1Bar14: TdxBar;
    dxBarLargeButton43: TdxBarLargeButton;
    aUpDownStream: TAction;
    dxNavBar1Separator2: TdxNavBarSeparator;
    aRealiziraniRN: TAction;
    dxBarLargeButton44: TdxBarLargeButton;
    dxBarLargeButton45: TdxBarLargeButton;
    aValuta: TAction;
    aTipPakuvanje: TAction;
    btTipPakuvanje: TdxBarButton;
    dxBarButton5: TdxBarButton;
    aPackingList: TAction;
    dxBarManager1Bar15: TdxBar;
    dxBarLargeButton46: TdxBarLargeButton;
    aListaEtiketi: TAction;
    aPartnerEtiketa: TAction;
    dxBarButton6: TdxBarButton;
    aCeniPoracki: TAction;
    dxBarLargeButton47: TdxBarLargeButton;
    aGenPlanKam: TAction;
    btTipPakuvanje1: TdxBarButton;
    dxbrlrgbtn1: TdxBarLargeButton;
    aRaspredelba: TAction;
    btTipPakuvanje2: TdxBarButton;
    dxbrlrgbtn2: TdxBarLargeButton;
    dxbrlrgbtn3: TdxBarLargeButton;
    aTekovenProcesKamion: TAction;
    dxbrlrgbtn4: TdxBarLargeButton;
    aBarKodVleIzlez: TAction;
    dxRibbon1TabPreglediReporter: TdxRibbonTab;
    btTipPakuvanje3: TdxBarButton;
    dxbrlrgbtn5: TdxBarLargeButton;
    aLagerKT: TAction;
    dxBarSubItem2: TdxBarSubItem;
    aKartEvidencija: TAction;
    dxBarButton7: TdxBarButton;
    dxBarLargeButton48: TdxBarLargeButton;
    dxBarLargeButton49: TdxBarLargeButton;
    dxBarLargeButton50: TdxBarLargeButton;
    aSostDel: TAction;
    aKrojnaLista: TAction;
    dxBarButton8: TdxBarButton;
    dxBarButton9: TdxBarButton;
    dxBarButton10: TdxBarButton;
    dxBarLargeButton51: TdxBarLargeButton;
    dxBarLargeButton52: TdxBarLargeButton;
    dxBarLargeButton53: TdxBarLargeButton;
    aOsveziPodatoci: TAction;
    dxBarLargeButton54: TdxBarLargeButton;
    procedure FormCreate(Sender: TObject);
    procedure OtvoriTabeli;
    procedure cxBarSkinPropertiesChange(Sender: TObject);
    procedure aSaveSkinExecute(Sender: TObject);
    procedure aAboutExecute(Sender: TObject);
    procedure aIzlezExecute(Sender: TObject);
    procedure aHelpExecute(Sender: TObject);
    procedure aZabeleskiExecute(Sender: TObject);
    procedure aFormConfigExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure aPromeniLozinkaExecute(Sender: TObject);
    procedure aArtVidExecute(Sender: TObject);
    procedure aArtikalExecute(Sender: TObject);
    procedure aNormativExecute(Sender: TObject);
    procedure aRaboniEdiniciExecute(Sender: TObject);
    procedure aProizvodstvenResursExecute(Sender: TObject);
    procedure aTipNormativExecute(Sender: TObject);
    procedure aArtGrupaExecute(Sender: TObject);
    procedure aArtKategorijaExecute(Sender: TObject);
    procedure aOperacijaPRExecute(Sender: TObject);
    procedure aMerkaExecute(Sender: TObject);
    procedure aOperacijaExecute(Sender: TObject);
    procedure aStatusExecute(Sender: TObject);
    procedure aGrupaStatusExecute(Sender: TObject);
    procedure aRabotenNalogExecute(Sender: TObject);
    procedure aCustomFieldsExecute(Sender: TObject);
    procedure aTipPRExecute(Sender: TObject);
    procedure aPrioritetExecute(Sender: TObject);
    procedure aTipPartnerExecute(Sender: TObject);
    procedure aPartnerExecute(Sender: TObject);
    procedure aObrabotkaRNExecute(Sender: TObject);
    procedure cxDBTextEditAllEnter(Sender: TObject);
    procedure cxDBTextEditAllExit(Sender: TObject);
    procedure EnterKakoTab(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure SetirajLukap(Sender: TObject; tabela: TDataSet; tip: TcxTextEdit; sifra: TcxTextEdit;
      lukap: TcxLookupComboBox);
    procedure aTekovenProcesExecute(Sender: TObject);
    procedure aKontrolnaTockaExecute(Sender: TObject);
    procedure aKontrolnaTExecute(Sender: TObject);
    procedure aOperacijaKTExecute(Sender: TObject);
    procedure aPosledovatelnostKTExecute(Sender: TObject);
    procedure aKorisnikKTExecute(Sender: TObject);
    procedure aKTVlezExecute(Sender: TObject);
    procedure aPorackaExecute(Sender: TObject);
    procedure aZavrsiRNExecute(Sender: TObject);
    procedure aPotrosniMaterijaliExecute(Sender: TObject);
    procedure aRasturKrsKaloExecute(Sender: TObject);
    procedure dxRibbon1TabChanging(Sender: TdxCustomRibbon;
      ANewTab: TdxRibbonTab; var Allow: Boolean);
    procedure aUserMagacinPravoExecute(Sender: TObject);
    procedure cbGodinaChange(Sender: TObject);
    procedure aLagerListaExecute(Sender: TObject);
    procedure aArtKategExecute(Sender: TObject);
    procedure aLinijaExecute(Sender: TObject);
    procedure aPotrosenMaterijalExecute(Sender: TObject);
    procedure aUpDownStreamExecute(Sender: TObject);
    procedure aRealiziraniRNExecute(Sender: TObject);
    procedure aValutaExecute(Sender: TObject);
    procedure aTipPakuvanjeExecute(Sender: TObject);
    procedure aPackingListExecute(Sender: TObject);
    procedure aListaEtiketiExecute(Sender: TObject);
    procedure aPartnerEtiketaExecute(Sender: TObject);
    procedure aCeniPorackiExecute(Sender: TObject);
    procedure aGenPlanKamExecute(Sender: TObject);
    procedure aRaspredelbaExecute(Sender: TObject);
    procedure aTekovenProcesKamionExecute(Sender: TObject);
    procedure aBarKodVleIzlezExecute(Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure aLagerKTExecute(Sender: TObject);
    procedure aKartEvidencijaExecute(Sender: TObject);
    procedure aSostDelExecute(Sender: TObject);
    procedure aKrojnaListaExecute(Sender: TObject);
    procedure aOsveziPodatociExecute(Sender: TObject);
  private
    { Private declarations }
  public

    { Public declarations }
  end;


  var
  frmMain: TfrmMain;


implementation

uses AboutBox, dmKonekcija, dmMaticni, dmResources, dmSystem, Utils,
  Zabeleskakontakt, FormConfig, PromeniLozinka, mk, artvid, artikal , dmUnit,
  ProizvodstvenResurs, cxConstantsMak, TipNormativ,ArtKategorija,// ArtKategorija,
  ArtGrupa,
  OperacijaPR, Merka, Operacija, Status, StatusGrupa, ManufacturingOrder, CustomFields,
  TipProizvodstvenResurs, Prioritet,Partner, TipPartner, ObrabotkaRN,
  TekovenProces, KontrolnaTocka, KontrolnaT, KTOperacija, Posledovatelnost,
  KorisnikKT, KontrolnaTocka_Vlez, Poracka, ZavrsiRN,
  EvidencijaPotroseniMaterijali, UserMagacinPravo, LagerArtikli, Evidencija, ArtKategorijaArtikal,
  ProiLinija, UpDownStream, RealiziraniRN, Valuta, TipPakuvanje, PakuvackaLista,
  ListaEtiketi, dmUnitBK, PartnerEtiketa, CeniPoracki, GenPlanKam, Raspredelba,
  TekovenProcesKamioni, BarKodIzlezVlez, LagerKT, KartEvidencija, SosDel, KrojnaLista;

{$R *.dfm}

procedure TfrmMain.aAboutExecute(Sender: TObject);
begin
  frmAboutBox := TfrmAboutBox.Create(nil);
  frmAboutBox.ShowModal;
  frmAboutBox.Free;
end;

procedure TfrmMain.aArtGrupaExecute(Sender: TObject);
begin
  frmArtGrupa := TfrmArtGrupa.Create(Application);
  frmArtGrupa.ShowModal;
  frmArtGrupa.Free;
end;

procedure TfrmMain.aArtikalExecute(Sender: TObject);
begin
   frmArtikal:=TfrmArtikal.Create(Application);
   frmArtikal.WindowState := wsMaximized;
   frmArtikal.ShowModal;
   frmArtikal.Free;
end;

procedure TfrmMain.aArtKategExecute(Sender: TObject);
begin
   frmArtKategorijaArtikal := TfrmArtKategorijaArtikal.Create(Application);
   frmArtKategorijaArtikal.ShowModal;
   frmArtKategorijaArtikal.Free;
end;

procedure TfrmMain.aArtKategorijaExecute(Sender: TObject);
begin
   frmArtKategorija := tfrmartkategorija.Create(Application);
   frmArtKategorija.ShowModal;
   frmArtKategorija.Free;
end;

procedure TfrmMain.aArtVidExecute(Sender: TObject);
begin
   frmArtVid := TfrmArtVid.Create(Application);
   frmArtVid.ShowModal;
   frmArtVid.Free;
end;

procedure TfrmMain.aBarKodVleIzlezExecute(Sender: TObject);
begin
  frmIzlezVlez := TfrmIzlezVlez.Create(self);
  frmIzlezVlez.ShowModal;
  frmIzlezVlez.Free;
end;

procedure TfrmMain.aCeniPorackiExecute(Sender: TObject);
begin
  frmCeniPoracki := TfrmCeniPoracki.Create(self);
  frmCeniPoracki.ShowModal;
  frmCeniPoracki.Free;
end;

procedure TfrmMain.aKrojnaListaExecute(Sender: TObject);
begin
  frmKrojnaLista := TfrmKrojnaLista.Create(Application);
  frmKrojnaLista.ShowModal;
  frmKrojnaLista.Free;
end;

procedure TfrmMain.aCustomFieldsExecute(Sender: TObject);
begin
  frmCustomFields := TfrmCustomFields.Create(Application);
  frmCustomFields.ShowModal;
  frmCustomFields.Free;
end;

procedure TfrmMain.aFormConfigExecute(Sender: TObject);
begin
  frmFormConfig := TfrmFormConfig.Create(Application);
  frmFormConfig.formPtr := Addr(Self);
  frmFormConfig.ShowModal;
  frmFormConfig.Free;
end;

procedure TfrmMain.aGenPlanKamExecute(Sender: TObject);
begin
   frmGenPlanKam := TfrmGenPlanKam.Create(self);
   frmGenPlanKam.ShowModal;
   frmGenPlanKam.Free;
end;

procedure TfrmMain.aGrupaStatusExecute(Sender: TObject);
begin
  frmStatusGrupa := TfrmStatusGrupa.Create(Application);
  frmStatusGrupa.ShowModal;
  frmStatusGrupa.Free;
end;

procedure TfrmMain.aHelpExecute(Sender: TObject);
begin
  //Application.HelpContext(100);
end;

procedure TfrmMain.aIzlezExecute(Sender: TObject);
begin
  Close;
end;

procedure TfrmMain.aKartEvidencijaExecute(Sender: TObject);
begin
   frmKartEvidencija := TfrmKartEvidencija.Create(self);
   frmKartEvidencija.ShowModal;
   frmKartEvidencija.Free;
end;

procedure TfrmMain.aKontrolnaTExecute(Sender: TObject);
begin
  frmKontrolnaT := TfrmKontrolnaT.Create(Application);
  frmKontrolnaT.ShowModal;
  frmKontrolnaT.Free;
end;

procedure TfrmMain.aKontrolnaTockaExecute(Sender: TObject);
begin
  frmKontrolnaTocka := TfrmKontrolnaTocka.Create(Application);
  frmKontrolnaTocka.ShowModal;
  frmKontrolnaTocka.Free;
end;

procedure TfrmMain.aKorisnikKTExecute(Sender: TObject);
begin
  frmKorisnikKT := TfrmKorisnikKT.Create(Application);
  frmKorisnikKT.ShowModal;
  frmKorisnikKT.Free;
end;

procedure TfrmMain.aKTVlezExecute(Sender: TObject);
begin
  frmKontrolnaTockaVlez := TfrmKontrolnaTockaVlez.Create(Application);
  frmKontrolnaTockaVlez.ShowModal;
  frmKontrolnaTockaVlez.Free;
end;

procedure TfrmMain.aLagerKTExecute(Sender: TObject);
begin
    frmLagerKT := TfrmLagerKT.Create(self);
    frmLagerKT.ShowModal;
    frmLagerKT.Free;
end;

procedure TfrmMain.aLagerListaExecute(Sender: TObject);
begin
    frmLagerArtikli := TfrmLagerArtikli.Create(Application);
    frmLagerArtikli.ShowModal;
    frmLagerArtikli.Free;
end;

procedure TfrmMain.aLinijaExecute(Sender: TObject);
begin
   frmProLinija := TfrmProLinija.Create(Self);
   frmProLinija.ShowModal;
   frmProLinija.Free;
end;

procedure TfrmMain.aListaEtiketiExecute(Sender: TObject);
begin
  frmListaEtiketi := TfrmListaEtiketi.Create(Self);
  frmListaEtiketi.ShowModal;
  frmListaEtiketi.Free;
end;

procedure TfrmMain.aMerkaExecute(Sender: TObject);
begin
  frmMerka := TfrmMerka.Create(Application);
  frmMerka.ShowModal;
  frmMerka.Free;
end;

procedure TfrmMain.aPackingListExecute(Sender: TObject);
begin
   frmPackingList := TfrmPackingList.Create(Self);
   frmPackingList.ShowModal;
   frmPackingList.Free;
end;

procedure TfrmMain.aPartnerEtiketaExecute(Sender: TObject);
begin
   frmPartnerEtiketa := TfrmPartnerEtiketa.Create(self);
   frmPartnerEtiketa.ShowModal;
   frmPartnerEtiketa.Free;
end;

procedure TfrmMain.aPartnerExecute(Sender: TObject);
begin
  frmPartner := TfrmPartner.Create(Application);
  frmPartner.ShowModal;
  frmPartner.Free;
end;

procedure TfrmMain.aPorackaExecute(Sender: TObject);
begin
  frmPoracka := tfrmPoracka.Create(Application);
  frmPoracka.ShowModal;
  frmPoracka.Free;
end;

procedure TfrmMain.aPosledovatelnostKTExecute(Sender: TObject);
begin
   frmPosledovatelnost := TfrmPosledovatelnost.Create(Application);
   frmPosledovatelnost.ShowModal;
   frmPosledovatelnost.Free;
end;

procedure TfrmMain.aPotrosenMaterijalExecute(Sender: TObject);
begin
//if not (dm.tblRabotenNalog.State in [dsEdit,dsInsert]) and (dm.tblRNStavka.State = dsBrowse) then
// begin
  dmRes.Spremi('MAN', 3);
  dmKon.tblSqlReport.ParamByName('id').AsInteger :=dm.tblRabotenNalogID.Value;;
  dmKon.tblSqlReport.Open;
  dmRes.frxReport1.ShowReport();
// end;

end;

procedure TfrmMain.aPotrosniMaterijaliExecute(Sender: TObject);
begin
  frmEvidencija := TfrmEvidencija.Create(self, false);
  frmEvidencija.mod_rabota := 0;
  frmEvidencija.ShowModal;
  frmEvidencija.Free;
end;

procedure TfrmMain.aPrioritetExecute(Sender: TObject);
begin
   frmPrioritet := TfrmPrioritet.Create(Application);
   frmPrioritet.ShowModal;
   frmPrioritet.Free;
end;

procedure TfrmMain.aProizvodstvenResursExecute(Sender: TObject);
begin
  frmProizvodstvenResurs:=TfrmProizvodstvenResurs.Create(Application);
  frmProizvodstvenResurs.ShowModal;
  frmProizvodstvenResurs.Free;
end;

procedure TfrmMain.aPromeniLozinkaExecute(Sender: TObject);
begin
  frmPromeniLozinka:=TfrmPromeniLozinka.Create(nil);
  frmPromeniLozinka.ShowModal;
  frmPromeniLozinka.Free;
end;

procedure TfrmMain.aRaboniEdiniciExecute(Sender: TObject);
begin
   frmRE := TfrmRE.Create(Application);
   frmRE.ShowModal;
   frmRE.Free;
end;

procedure TfrmMain.aRabotenNalogExecute(Sender: TObject);
var  status: TStatusWindowHandle;
begin
   status := cxCreateStatusWindow();
      try
          frmManufacturingOrder := TfrmManufacturingOrder.Create(Self);
          frmManufacturingOrder.ShowModal;
          frmManufacturingOrder.Free;
        //���� �� �������� ������� �� ��� � �������� ����� �����
        finally
     	cxRemoveStatusWindow(status);
      end;
   
end;

procedure TfrmMain.aRaspredelbaExecute(Sender: TObject);
begin
  frmRaspredelba := TfrmRaspredelba.Create(Self);
  frmRaspredelba.ShowModal;
  frmRaspredelba.Free;
end;

procedure TfrmMain.aRasturKrsKaloExecute(Sender: TObject);
begin
  frmEvidencija := TfrmEvidencija.Create(self, false);
  frmEvidencija.mod_rabota := 1;
  frmEvidencija.dxBarManager1Bar6.Visible := false;
  frmEvidencija.ShowModal;
  frmEvidencija.Free;
end;

procedure TfrmMain.aRealiziraniRNExecute(Sender: TObject);
begin
  frmRealiziraniRN := TfrmRealiziraniRN.Create(Self);
  frmRealiziraniRN.ShowModal;
  frmRealiziraniRN.Free;

end;

procedure TfrmMain.aSaveSkinExecute(Sender: TObject);
begin
  dmRes.ZacuvajSkinVoIni;
end;

procedure TfrmMain.aSostDelExecute(Sender: TObject);
begin
   frmSosDel := TfrmSosDel.Create(self);
   frmSosDel.ShowModal;
   frmSosDel.Free;
end;

procedure TfrmMain.aStatusExecute(Sender: TObject);
begin
   frmStatus := TfrmStatus.Create(Application);
   frmStatus.ShowModal;
   frmStatus.Free;
end;

procedure TfrmMain.aTekovenProcesExecute(Sender: TObject);
begin
  frmTekovenProces := TfrmTekovenProces.Create(Application);
  frmTekovenProces.ShowModal;
  frmTekovenProces.Free;
end;

procedure TfrmMain.aTekovenProcesKamionExecute(Sender: TObject);
begin
  frmTekovenProcesKamioni := TfrmTekovenProcesKamioni.Create(self);
  frmTekovenProcesKamioni.ShowModal;
  frmTekovenProcesKamioni.Free;
end;

procedure TfrmMain.aTipNormativExecute(Sender: TObject);
begin
  frmTipNormativ := TfrmTipNormativ.Create(Application);
  frmTipNormativ.ShowModal;
  frmTipNormativ.Free;
end;

procedure TfrmMain.aTipPakuvanjeExecute(Sender: TObject);
begin
 // frmTipPakuvanje := TfrmTipPakuvanje.Create(Self);
//  frmTipPakuvanje.ShowModal;
//  frmTipPakuvanje.Free;
end;

procedure TfrmMain.aTipPartnerExecute(Sender: TObject);
begin
  frmTipPartner := TfrmTipPartner.Create(Application);
  frmTipPartner.ShowModal;
  frmTipPartner.Free;
end;

procedure TfrmMain.aTipPRExecute(Sender: TObject);
begin
  frmTipPR := TfrmTipPR.Create(Application);
  frmTipPR.ShowModal;
  frmTipPR.Free;
end;

procedure TfrmMain.aUpDownStreamExecute(Sender: TObject);
begin
  frmUpDownStream := TfrmUpDownStream.Create(Self);
  frmUpDownStream.ShowModal;
  frmUpDownStream.Free;
end;

procedure TfrmMain.aUserMagacinPravoExecute(Sender: TObject);
begin
   frmUserMagacinPravo := TfrmUserMagacinPravo.Create(Self, 'MAN');
   frmUserMagacinPravo.ShowModal;
   frmUserMagacinPravo.Free;
end;

procedure TfrmMain.aValutaExecute(Sender: TObject);
begin
  frmValuta := TfrmValuta.Create(Self);
  frmValuta.ShowModal;
  frmValuta.free;
end;

procedure TfrmMain.aZabeleskiExecute(Sender: TObject);
begin
  // ������� ��������� �� Codex
  frmZabeleskaKontakt := TfrmZabeleskaKontakt.Create(nil);
  frmZabeleskaKontakt.ShowModal;
  frmZabeleskaKontakt.Free;
end;

procedure TfrmMain.aZavrsiRNExecute(Sender: TObject);
begin
   frmZavrsiRN := TfrmZavrsiRN.Create(Application);
   frmZavrsiRN.ShowModal;
   frmZavrsiRN.Free;
end;

procedure TfrmMain.cbGodinaChange(Sender: TObject);
begin
  godina := cbGodina.EditValue;
end;

procedure TfrmMain.cxBarSkinPropertiesChange(Sender: TObject);
begin
  dmRes.SkinPromeni(Sender);
  dxRibbon1.ColorSchemeName := dmRes.skin_name;
end;

procedure TfrmMain.cxDBTextEditAllEnter(Sender: TObject);
begin
  TEdit(Sender).Color:=clSkyBlue;
end;

procedure TfrmMain.cxDBTextEditAllExit(Sender: TObject);
begin
//   if ((Sender as TWinControl)= cbP) then
//        begin
//            if (cbP.Text <>'') then
//            begin
//              txtTP.Text := cbP.EditValue[0]; // dm.tblKooperantiTIP_PARTNER.AsString; //cbPartner.EditValue[0];
//              txtP.Text := cbP.EditValue[1]; // dm.tblKooperantiID.AsString;
//            //  txtMesto.Text :=  dm.tblKooperantiNAZIV_MESTO.Value;  //cbKoop.EditValue[3];
//           //   txtAdresa.Text := dm.tblKooperantiADRESA.Value;  //cbKoop.EditValue[4];
//            end
//            else
//            begin
//              txtTP.Clear;
//              txtP.Clear;
//           //   txtMesto.Clear;
//            //  txtAdresa.Clear;
//            end;
//         end;
//         if ((Sender as TWinControl)= txtP) or ((Sender as TWinControl)= txtTP) then
//         begin
//          if (txtTP.Text <>'') and (txtP.Text<>'')  then
//          begin
//            if(dmMat.tblPartner.Locate('TIP_PARTNER;ID',VarArrayOf([txtTP.text,txtP.text]),[])) then
//            begin
//               cbP.Text:=dmMat.tblPartnerNAZIV.Value;
//             //  txtMesto.Text := dm.tblKooperantiNAZIV_MESTO.Value;
//             //  txtAdresa.Text := dm.tblKooperantiADRESA.Value;
//            end
//           end
//            else
//            begin
//               cbP.Clear;
//               //txtMesto.Clear;
//               //txtAdresa.Clear;
//             end;
//
//         end;
//
//   // if (dm.tblRNStavka.State) in [dsInsert, dsEdit] then
//   // begin
//   if ((Sender as TWinControl)= cbNazivArtikal) then
//        begin
//            if (cbNazivArtikal.Text <>'') then
//            begin
//              txtVidArtikal.Text := dm.tblArtikalRNARTVID.AsString;
//              txtArtikal.Text := dm.tblArtikalRNID.AsString;
//              //dm.tblNormativO.Close;
//              //dm.tblNormativO.ParamByName('mas_id').va
//            end
//            else
//            begin
//              dm.tblRNStavkaVID_ARTIKAL.Clear;
//              dm.tblRNStavkaARTIKAL.Clear;
//            end;
//         end;
//         if ((Sender as TWinControl)= txtArtikal) or ((Sender as TWinControl)= txtVidArtikal) then
//         begin
//             SetirajLukap(sender,dm.tblArtikalRN,txtVidArtikal,txtArtikal, cbNazivArtikal);
//         end;
//  //  end;

  TEdit(Sender).Color:=clWhite;
end;

procedure TfrmMain.dxRibbon1TabChanging(Sender: TdxCustomRibbon;
  ANewTab: TdxRibbonTab; var Allow: Boolean);
begin
    if Self.Active then

   if ANewTab = dxRibbon1TabPodesuvanja then
   begin
   //  lgodina.Visible:=True;
     cbgodina.Visible:=ivAlways;
   end
   else
   begin
   //  lgodina.Visible:=False;
     cbgodina.Visible:=ivNever;
   end;
end;

procedure TfrmMain.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  if (dmKon.reporter_dll) then
  begin
    dmRes.FreeDLL;
  end;
end;

procedure TfrmMain.FormCreate(Sender: TObject);
begin
  dxRibbon1.ColorSchemeName := dmRes.skin_name; // ������� �� ������ �� �������� �����
  dmRes.SkinLista(cxBarSkin); // ������ �� ������� ������� �� combo-��
  OtvoriTabeli;

  dxRibbonStatusBar1.Panels[0].Text := dxRibbonStatusBar1.Panels[0].Text + dmKon.imeprezime; // ������� �� ���������� ��������
  lblDatumVreme.Caption := lblDatumVreme.Caption + DateToStr(now); // ������ �� �������

end;

procedure TfrmMain.FormShow(Sender: TObject);
begin
  //  ������� �� ������������ ������������/���������� �� ���������� �� ���� �����
  SpremiForma(self);
 // cxSetEureka('biljana.trpkoska@codex.mk');
  //���� �����, �� ��� �� ������ ����������� �� ��
  frmMK:=TfrmMK.Create(Application);
  frmMK.ShowModal;
  frmMK.free;

  cbGodina.EditValue:=dm.tblGodinaGODINA.AsString;
  godina := dm.tblGodinaGODINA.AsString;
  lblFirma.Caption := dmKon.viewFirmiPARTNERNAZIV.Value+' - '+dmKon.viewFirmiMESTONAZIV.Value;

   if (dmKon.reporter_dll) then
  	     begin
		dmRes.ReporterInit;
		dmRes.GenerateReportsRibbon(dxRibbon1,dxRibbon1TabPreglediReporter,dxBarManager1);
     end;

end;

procedure TfrmMain.aNormativExecute(Sender: TObject);
begin
  frmNormativ := TfrmNormativ.Create(self,true);
  frmNormativ.WindowState := wsMaximized;
  frmNormativ.ShowModal;
  frmNormativ.free;
end;

procedure TfrmMain.aObrabotkaRNExecute(Sender: TObject);
begin
  frmObrabotkaRN := TfrmObrabotkaRN.Create(Application);
  frmObrabotkaRN.ShowModal;
  frmObrabotkaRN.Free;
end;

procedure TfrmMain.aOperacijaExecute(Sender: TObject);
begin
  frmOperacija:=TfrmOperacija.Create(Application);
  frmOperacija.ShowModal;
  frmOperacija.Free;
end;

procedure TfrmMain.aOperacijaKTExecute(Sender: TObject);
begin
  frmOperacijaKT := TfrmOperacijakt.Create(Application);
  frmOperacijaKT.ShowModal;
  frmOperacijaKT.Free;
end;

procedure TfrmMain.aOperacijaPRExecute(Sender: TObject);
begin
  frmOperacijaPR := TfrmOperacijaPR.Create(Application);
  frmOperacijaPR.ShowModal;
  frmOperacijaPR.Free;
end;

procedure TfrmMain.aOsveziPodatociExecute(Sender: TObject);
var  status: TStatusWindowHandle;
begin
   status := cxCreateStatusWindow();
      try
         OtvoriTabeli ;
      finally
     	cxRemoveStatusWindow(status);
      end;
end;


procedure TfrmMain.OtvoriTabeli;
begin
     //  ������� �� ������������ ������������/���������� �� ���������� �� ���� �����
   SpremiForma(self);
   dm.tblGodina.Open;
  //cbGodina.EditValue:=dm.tblGodinaGODINA.AsString;
   // godina  �������� ��������� �� ��������� ������ (������� �� �������� �����)
   //godina := dm.tblGodinaGODINA.AsString;

  // ������ �� ���������� dataset-���
   dm.qSetupM.ExecQuery;
   masinsko := dm.qSetupM.FldByName['v1'].asinteger;
   dm.qSetupK.ExecQuery;
   kompaniski_broj := dm.qSetupK.FldByName['v1'].AsString;
   dm.qSetupMGP.ExecQuery;
   MGP := dm.qSetupMGP.FldByName['v1'].AsInteger;
   dm.qSetupTap.ExecQuery;
   vid_tap := dm.qSetupTap.FldByName['v1'].AsInteger;
   dm.qSetupL.ExecQuery;
   kakov_lot := dm.qSetupl.FldByName['v1'].AsInteger;
   dm.qSetupV.ExecQuery;
   Val := dm.qSetupV.FldByName['v1'].AsString;
   kurs := dm.qSetupV.FldByName['v2'].AsFloat;

   dm.qSetupPartner.ExecQuery;
   skraten_naziv := dm.qSetupPartner.FldByName['v1'].AsString;

   dmMat.tblTipArtikal.close;
   dmMat.tblTipArtikal.Open;
   dmMat.tblArtVid.Close;
   dmMat.tblArtVid.Open;
   dmMat.tblArtikal.Close;
   dmMat.tblArtikal.ParamByName('a').Value := 1;
   dmMat.tblArtikal.Open;

   dmmat.tblSysApp.Close;
   dmmat.tblSysApp.Open;
 //  dm.tblNormativ.close;
 //  dm.tblNormativ.open;
  // dm.tblRE.ParamByName('id').AsInteger:=dmKon.re;
   dm.tblRE.Close;
   dm.tblRE.Open;
   dm.tblMagacin.close;
   dm.tblMagacin.ParamByName('id').AsInteger:=dmKon.re;
   dm.tblMagacin.Open;

   dmMat.tblRE.Close;
   dmMat.tblRE.Open;
   dmMat.tblValuta.close;
   dmMat.tblValuta.Open;
//   dmMat.tblReMagacin.Open;
   dmMat.tblPartner.close;
   dmMat.tblPartner.Open;
   dm.tblArtikal.Close;
   dm.tblArtikal.Open;
   dmMat.tblMerka.Close;
   dmMat.tblMerka.Open;
   dm.tblTipNormativ.Close;
   dm.tblTipNormativ.open;
   dmMat.tblArtKategorija.Close;
   dmMat.tblArtKategorija.Open;
   dmMat.tblArtGrupa.close;
   dmMat.tblArtGrupa.Open;
   dm.tblOperacijaPR.Close;
   dm.tblOperacijaPR.Open;
   dm.tblPR.Close;
   dm.tblPR.Open;
   dm.tblOperacija.Close;
   dm.tblOperacija.Open;
   dmMat.tblStatus.Close;
   dmMat.tblStatus.ParamByName('APP').AsString := 'MAN';
   dmMat.tblStatus.Open;
   dmMat.tblStatusGrupa.Close;
   dmMat.tblStatusGrupa.ParamByName('APP').AsString := 'MAN';
   dmMat.tblStatusGrupa.Open;
   dmMat.tblCustomFields.Close;
   dmMat.tblCustomFields.Open;
   dm.pNormativOrg.Close;
   dm.pNormativOrg.Open;
   dm.tblArtikalRN.Open;
   dm.tblArtikalRN.Open;
   dm.tblArtikalNormativ.Close;
   dm.tblArtikalNormativ.Open;
   dm.tblPrioritet.Close;
   dm.tblPrioritet.Open;
   dm.tblGenPlanKam.Close;
   dm.tblGenPlanKam.ParamByName('status').AsString := '%';
   dm.tblGenPlanKam.ParamByName('godina').AsString := '%';
   dm.tblGenPlanKam.Open;
//   dm.tblMatStatus.ParamByName('app').AsString := dmKon.aplikacija;
//   dm.tblMatStatus.Open;
   dmMat.tblTipPartner.Close;
   dmMat.tblTipPartner.Open;
//   dm.tblRabotenNalog.Close;
//   dm.tblRabotenNalog.ParamByName('status').AsString := '%';
//   dm.tblRabotenNalog.Open;
   dm.tblKT.Close;
   dm.tblKT.ParamByName('id_re').AsString := '%';
   dm.tblKT.Open;
   dmMat.tblNurko.Close;
   dmMat.tblNurko.Open;
   dmMat.tblReMagacin.Close;
   dmMat.tblReMagacin.Open;

   dm.tblLinija.Close;
   dm.tblLinija.Open;
   dm.tblPartnerEtiketa.Close;
   dm.tblPartnerEtiketa.Open;
   dm.tblSysReport.Close;
   dm.tblSysReport.Open;
 //  dm.tblTipPakuvanje.Open;
  // dm.tblPackingList.Open;
   //dmKon.tbluserMagacinpravo.Open;
   //dm.tblEvidencijaKT.Open;
end;

procedure TfrmMain.EnterKakoTab(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
    case Key of
        VK_DOWN:
        begin
          PostMessage(Handle,WM_NextDlgCtl,0,0);
          key:=0;
        end;
        VK_UP:
        begin
          PostMessage(Handle,WM_NextDlgCtl,1,0);
          key:=0;
        end;
        VK_RETURN:
        begin
          PostMessage(Handle,WM_NextDlgCtl,0,0);
        end;
    end;
end;

procedure TfrmMain.SetirajLukap(sender:TObject; tabela:TDataSet; tip:TcxTextEdit; sifra:TcxTextEdit; lukap:TcxLookupComboBox);
begin
         if (tip.Text <>'') and (sifra.Text<>'')  then
         begin
          if(tabela.Locate('ARTVID;ID',VarArrayOf([tip.text,sifra.text]),[])) then
               lukap.Text:=tabela.FieldByName('NAZIV').Value;
         end
         else
         begin
            lukap.Clear;
         end;
end;


end.
