object frmPoracka: TfrmPoracka
  Left = 128
  Top = 212
  BorderIcons = [biSystemMenu, biMaximize]
  Caption = #1055#1086#1088#1072#1095#1082#1072
  ClientHeight = 837
  ClientWidth = 1130
  Color = clBtnFace
  Constraints.MinHeight = 500
  Constraints.MinWidth = 610
  Font.Charset = RUSSIAN_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  Position = poScreenCenter
  ShowHint = True
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  TextHeight = 13
  object dxRibbon1: TdxRibbon
    Left = 0
    Top = 0
    Width = 1130
    Height = 126
    BarManager = dxBarManager1
    ColorSchemeName = 'Blue'
    Fonts.AssignedFonts = [afGroup, afGroupHeader]
    Fonts.Group.Charset = DEFAULT_CHARSET
    Fonts.Group.Color = 9126421
    Fonts.Group.Height = -12
    Fonts.Group.Name = 'Segoe UI'
    Fonts.Group.Style = []
    Fonts.GroupHeader.Charset = DEFAULT_CHARSET
    Fonts.GroupHeader.Color = 11168318
    Fonts.GroupHeader.Height = -12
    Fonts.GroupHeader.Name = 'Segoe UI'
    Fonts.GroupHeader.Style = []
    Contexts = <>
    TabOrder = 0
    TabStop = False
    OnTabChanging = dxRibbon1TabChanging
    object dxRibbon1Tab1: TdxRibbonTab
      Active = True
      Caption = #1052#1077#1085#1080
      Groups = <
        item
          ToolbarName = 'dxBarManager1Bar6'
        end
        item
          ToolbarName = 'dxBarManager1Bar1'
        end
        item
          ToolbarName = 'dxBarManager1Bar5'
        end
        item
          ToolbarName = 'dxBarManager1Bar7'
        end
        item
          ToolbarName = 'dxBarManager1Bar8'
        end
        item
          ToolbarName = 'dxBarManager1Bar2'
        end
        item
          ToolbarName = 'dxBarManager1Bar3'
        end>
      Index = 0
    end
    object dxRibbon1Tab2: TdxRibbonTab
      Caption = #1055#1086#1076#1077#1089#1091#1074#1072#1114#1072
      Groups = <
        item
          ToolbarName = 'dxBarManager1Bar4'
        end
        item
          ToolbarName = 'dxBarManager1BarIzgledGrid'
        end>
      Index = 1
    end
  end
  object StatusBar1: TdxRibbonStatusBar
    Left = 0
    Top = 814
    Width = 1130
    Height = 23
    Panels = <
      item
        PanelStyleClassName = 'TdxStatusBarTextPanelStyle'
        Text = 
          'F5 - '#1053#1086#1074', F6 - '#1040#1078#1091#1088#1080#1088#1072#1112', F7 - '#1054#1089#1074#1077#1078#1080', F8 - '#1041#1088#1080#1096#1080', F9 - '#1047#1072#1087#1080#1096#1080', E' +
          'sc - '#1054#1090#1082#1072#1078#1080' / '#1048#1079#1083#1077#1079
      end>
    Ribbon = dxRibbon1
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clDefault
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
  end
  object cxPageControl1: TcxPageControl
    Left = 0
    Top = 126
    Width = 1130
    Height = 688
    Align = alClient
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 2
    Properties.ActivePage = tsElement
    Properties.CustomButtons.Buttons = <>
    Properties.Images = dmRes.cxSmallImages
    Properties.NavigatorPosition = npLeftTop
    Properties.Rotate = True
    Properties.TabCaptionAlignment = taLeftJustify
    Properties.TabPosition = tpLeft
    LookAndFeel.Kind = lfUltraFlat
    LookAndFeel.NativeStyle = False
    OnChange = cxPageControl1Change
    ClientRectBottom = 688
    ClientRectLeft = 174
    ClientRectRight = 1130
    ClientRectTop = 0
    object tsListaPoracki: TcxTabSheet
      Caption = '   '#1051#1080#1089#1090#1072' '#1085#1072' '#1055#1086#1088#1072#1095#1082#1080'   '
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ImageIndex = 66
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      object lPanel: TPanel
        Left = 0
        Top = 0
        Width = 956
        Height = 688
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object cxGrid1: TcxGrid
          Left = 0
          Top = 0
          Width = 956
          Height = 458
          Align = alClient
          Font.Charset = RUSSIAN_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          TabOrder = 0
          object cxGrid1DBTableView1: TcxGridDBTableView
            OnDblClick = cxGrid1DBTableView1DblClick
            OnKeyDown = cxGrid1DBTableView1KeyDown
            Navigator.Buttons.CustomButtons = <>
            ScrollbarAnnotations.CustomAnnotations = <>
            OnFocusedRecordChanged = cxGrid1DBTableView1FocusedRecordChanged
            DataController.DataSource = dm.dsPoracka
            DataController.KeyFieldNames = 'ID'
            DataController.Options = [dcoAnsiSort, dcoAssignGroupingValues, dcoAssignMasterDetailKeys, dcoSaveExpanding]
            DataController.Summary.DefaultGroupSummaryItems = <>
            DataController.Summary.FooterSummaryItems = <
              item
                Kind = skCount
                OnGetText = cxGrid1DBTableView1TcxGridDBDataControllerTcxDataSummaryFooterSummaryItems0GetText
                FieldName = 'NAZIV_STATUS'
                Column = cxGrid1DBTableView1NAZIV_STATUS
              end>
            DataController.Summary.SummaryGroups = <>
            FilterRow.InfoText = #1050#1083#1080#1082#1085#1077#1090#1077' '#1090#1091#1082#1072' '#1079#1072' '#1076#1072' '#1076#1077#1092#1080#1085#1080#1088#1072#1090#1077' '#1092#1080#1083#1090#1077#1088
            FilterRow.Visible = True
            OptionsBehavior.CellHints = True
            OptionsBehavior.IncSearch = True
            OptionsBehavior.ImmediateEditor = False
            OptionsCustomize.ColumnsQuickCustomization = True
            OptionsData.Deleting = False
            OptionsData.Editing = False
            OptionsData.Inserting = False
            OptionsSelection.HideFocusRectOnExit = False
            OptionsView.Footer = True
            OptionsView.HeaderAutoHeight = True
            OptionsView.Indicator = True
            object cxGrid1DBTableView1BOJA: TcxGridDBColumn
              DataBinding.FieldName = 'BOJA'
              PropertiesClassName = 'TcxColorComboBoxProperties'
              Properties.CustomColors = <>
              Properties.ShowDescriptions = False
              Width = 24
              IsCaptionAssigned = True
            end
            object cxGrid1DBTableView1ID: TcxGridDBColumn
              Caption = #1064#1080#1092#1088#1072
              DataBinding.FieldName = 'ID'
              Visible = False
            end
            object cxGrid1DBTableView1NAZIV_STATUS: TcxGridDBColumn
              Caption = #1057#1090#1072#1090#1091#1089
              DataBinding.FieldName = 'NAZIV_STATUS'
              Width = 56
            end
            object cxGrid1DBTableView1REF_NO: TcxGridDBColumn
              Caption = #1056#1077#1092#1077#1088#1077#1085#1090#1077#1085' '#1073#1088#1086#1112
              DataBinding.FieldName = 'REF_NO'
              Width = 114
            end
            object cxGrid1DBTableView1ID_RE: TcxGridDBColumn
              Caption = #1064#1080#1092#1088#1072' '#1085#1072' '#1092#1080#1088#1084#1072
              DataBinding.FieldName = 'ID_RE'
              Visible = False
            end
            object cxGrid1DBTableView1NAZIV_FIRMA: TcxGridDBColumn
              Caption = #1060#1080#1088#1084#1072
              DataBinding.FieldName = 'NAZIV_FIRMA'
              Visible = False
            end
            object cxGrid1DBTableView1BROJ: TcxGridDBColumn
              Caption = #1041#1088#1086#1112
              DataBinding.FieldName = 'BROJ'
            end
            object cxGrid1DBTableView1TIP_PARTNER: TcxGridDBColumn
              Caption = #1058#1080#1087' '#1085#1072' '#1087#1072#1088#1090#1085#1077#1088
              DataBinding.FieldName = 'TIP_PARTNER'
            end
            object cxGrid1DBTableView1PARTNER: TcxGridDBColumn
              Caption = #1064#1080#1092#1088#1072' '#1085#1072' '#1055#1072#1088#1090#1085#1077#1088
              DataBinding.FieldName = 'PARTNER'
            end
            object cxGrid1DBTableView1NAZIV_PARTNER: TcxGridDBColumn
              Caption = #1055#1072#1088#1090#1085#1077#1088
              DataBinding.FieldName = 'NAZIV_PARTNER'
              Width = 246
            end
            object cxGrid1DBTableView1DATUM: TcxGridDBColumn
              Caption = #1044#1072#1090#1091#1084
              DataBinding.FieldName = 'DATUM'
            end
            object cxGrid1DBTableView1GODINA: TcxGridDBColumn
              Caption = #1043#1086#1076#1080#1085#1072
              DataBinding.FieldName = 'GODINA'
              Visible = False
            end
            object cxGrid1DBTableView1DATUM_OD_ISPORAKA: TcxGridDBColumn
              Caption = #1054#1076' '#1076#1072#1090#1091#1084' '#1085#1072' '#1080#1089#1087#1086#1088#1072#1082#1072
              DataBinding.FieldName = 'DATUM_OD_ISPORAKA'
              Visible = False
            end
            object cxGrid1DBTableView1DATUM_DO_ISPORAKA: TcxGridDBColumn
              Caption = #1044#1086' '#1076#1072#1090#1091#1084' '#1085#1072' '#1080#1089#1087#1086#1088#1072#1082#1072
              DataBinding.FieldName = 'DATUM_DO_ISPORAKA'
              Visible = False
            end
            object cxGrid1DBTableView1STATUS: TcxGridDBColumn
              Caption = #1064#1080#1092#1088#1072' '#1089#1090#1072#1090#1091#1089
              DataBinding.FieldName = 'STATUS'
              Width = 87
            end
          end
          object cxGrid1DBTableView2: TcxGridDBTableView
            Navigator.Buttons.CustomButtons = <>
            ScrollbarAnnotations.CustomAnnotations = <>
            DataController.DataSource = dsPorackaStavka
            DataController.DetailKeyFieldNames = 'ID_PORACKA'
            DataController.KeyFieldNames = 'ID'
            DataController.MasterKeyFieldNames = 'ID'
            DataController.Summary.DefaultGroupSummaryItems = <>
            DataController.Summary.FooterSummaryItems = <>
            DataController.Summary.SummaryGroups = <>
            Images = dmRes.cxImageGrid
            OptionsBehavior.IncSearch = True
            OptionsCustomize.ColumnsQuickCustomization = True
            OptionsData.Deleting = False
            OptionsData.Inserting = False
            OptionsView.ColumnAutoWidth = True
            OptionsView.GroupByBox = False
            object cxGrid1DBTableView2VID_ARTIKAL: TcxGridDBColumn
              Caption = #1042#1080#1076' '#1085#1072' '#1072#1088#1090#1080#1082#1072#1083
              DataBinding.FieldName = 'VID_ARTIKAL'
              Options.Editing = False
              Width = 20
            end
            object cxGrid1DBTableView2ARTIKAL: TcxGridDBColumn
              Caption = #1064#1080#1092#1088#1072' '#1085#1072' '#1072#1088#1090#1080#1082#1072#1083
              DataBinding.FieldName = 'ARTIKAL'
              Options.Editing = False
              Width = 20
            end
            object cxGrid1DBTableView2NAZIV_ARTIKAL: TcxGridDBColumn
              Caption = #1040#1088#1090#1080#1082#1072#1083
              DataBinding.FieldName = 'NAZIV_ARTIKAL'
              Options.Editing = False
              Width = 49
            end
            object cxGrid1DBTableView2NAZIV2: TcxGridDBColumn
              Caption = #1044#1088#1091#1075' '#1085#1072#1079#1080#1074' '#1072#1088#1090#1080#1082#1072#1083
              DataBinding.FieldName = 'NAZIV2'
              Options.Editing = False
              Width = 49
            end
            object cxGrid1DBTableView2KOLICINA: TcxGridDBColumn
              Caption = #1050#1086#1083#1080#1095#1080#1085#1072
              DataBinding.FieldName = 'KOLICINA'
              Options.Editing = False
              SortIndex = 0
              SortOrder = soAscending
              Width = 20
            end
            object cxGrid1DBTableView2ZABELESKA: TcxGridDBColumn
              Caption = #1047#1072#1073#1077#1083#1077#1096#1082#1072
              DataBinding.FieldName = 'ZABELESKA'
              PropertiesClassName = 'TcxBlobEditProperties'
              Properties.BlobEditKind = bekMemo
              Properties.BlobPaintStyle = bpsText
              Properties.ReadOnly = True
              HeaderGlyphAlignmentHorz = taRightJustify
              HeaderImageIndex = 6
              Width = 47
            end
            object cxGrid1DBTableView2ISPORAKA_OD_DATUM: TcxGridDBColumn
              Caption = #1054#1076' '#1076#1072#1090#1091#1084' '#1085#1072' '#1080#1089#1087#1086#1088#1072#1082#1072
              DataBinding.FieldName = 'ISPORAKA_OD_DATUM'
              Options.Editing = False
              Width = 47
            end
            object cxGrid1DBTableView2ISPORAKA_DO_DATUM: TcxGridDBColumn
              Caption = #1044#1086' '#1076#1072#1090#1091#1084' '#1085#1072' '#1080#1089#1087#1086#1088#1072#1082#1072
              DataBinding.FieldName = 'ISPORAKA_DO_DATUM'
              Options.Editing = False
              Width = 142
            end
            object cxGrid1DBTableView2TAPACIR_SEDISTE: TcxGridDBColumn
              Caption = #1058#1072#1087#1072#1094#1080#1088' '#1057#1077#1076#1080#1096#1090#1077
              DataBinding.FieldName = 'TAPACIR_SEDISTE'
              Options.Editing = False
              Width = 123
            end
            object cxGrid1DBTableView2TAPACIR_NAZAD: TcxGridDBColumn
              Caption = #1058#1072#1087#1072#1094#1080#1088' '#1085#1072#1079#1072#1076
              DataBinding.FieldName = 'TAPACIR_NAZAD'
              Options.Editing = False
              Width = 78
            end
            object cxGrid1DBTableView2BOJA_NOGARKI: TcxGridDBColumn
              Caption = #1041#1086#1112#1072' '#1085#1072' '#1085#1086#1075#1072#1088#1082#1080
              DataBinding.FieldName = 'BOJA_NOGARKI'
              Options.Editing = False
              Width = 77
            end
            object cxGrid1DBTableView2SUNGER: TcxGridDBColumn
              Caption = #1057#1091#1085#1107#1077#1088
              DataBinding.FieldName = 'SUNGER'
              Visible = False
              Options.Editing = False
            end
            object cxGrid1DBTableView2LABELS: TcxGridDBColumn
              DataBinding.FieldName = 'LABELS'
              Visible = False
              Options.Editing = False
            end
            object cxGrid1DBTableView2GLIDERS: TcxGridDBColumn
              DataBinding.FieldName = 'GLIDERS'
              Visible = False
              Options.Editing = False
            end
          end
          object cxGrid1Level1: TcxGridLevel
            GridView = cxGrid1DBTableView1
            object cxGrid1Level2: TcxGridLevel
              GridView = cxGrid1DBTableView2
            end
          end
        end
        object SplitterPoracka: TcxSplitter
          AlignWithMargins = True
          Left = 3
          Top = 461
          Width = 950
          Height = 8
          HotZoneClassName = 'TcxMediaPlayer9Style'
          AlignSplitter = salBottom
          AllowHotZoneDrag = False
          Control = gbPoracka
        end
        object gbPoracka: TcxGroupBox
          Left = 0
          Top = 472
          Align = alBottom
          Caption = '  '#1055#1054#1056#1040#1063#1050#1040'  '
          Enabled = False
          TabOrder = 2
          Height = 216
          Width = 956
          object cxLabel15: TcxLabel
            Left = 27
            Top = 364
            Caption = #1042#1082#1091#1087#1077#1085' '#1073#1088#1086#1112' '#1085#1072' '#1094#1080#1082#1083#1091#1089#1080
            Style.TextColor = clNavy
            Properties.Alignment.Horz = taRightJustify
            Properties.WordWrap = True
            Transparent = True
            Visible = False
            Width = 82
            AnchorX = 109
          end
          object txtBrCiklusi: TcxDBTextEdit
            Left = 156
            Top = 364
            DataBinding.DataField = 'BR_CIKLUSI'
            DataBinding.DataSource = dm.dsRabotenNalog
            TabOrder = 12
            Visible = False
            OnEnter = cxDBTextEditAllEnter
            OnExit = cxDBTextEditAllExit
            OnKeyDown = EnterKakoTab
            Width = 61
          end
          object lblBroj: TcxLabel
            Left = 35
            Top = 37
            AutoSize = False
            Caption = #1041#1088#1086#1112
            Style.TextColor = clRed
            Properties.Alignment.Horz = taRightJustify
            Transparent = True
            Height = 17
            Width = 100
            AnchorX = 135
          end
          object txtBroj: TcxDBTextEdit
            Tag = 1
            Left = 141
            Top = 36
            Hint = #1041#1088#1086#1112' '#1085#1072' '#1055#1086#1088#1072#1095#1082#1072
            DataBinding.DataField = 'BROJ'
            DataBinding.DataSource = dm.dsPoracka
            TabOrder = 0
            OnEnter = cxDBTextEditAllEnter
            OnExit = cxDBTextEditAllExit
            OnKeyDown = EnterKakoTab
            Width = 140
          end
          object cxLabel2: TcxLabel
            Left = 727
            Top = 36
            AutoSize = False
            Caption = #1056#1072#1073#1086#1090#1085#1072' '#1077#1076#1080#1085#1080#1094#1072
            Style.TextColor = clRed
            Properties.Alignment.Horz = taRightJustify
            Transparent = True
            Visible = False
            Height = 17
            Width = 100
            AnchorX = 827
          end
          object txtRE: TcxDBTextEdit
            Left = 765
            Top = 59
            DataBinding.DataField = 'ID_RE'
            DataBinding.DataSource = dm.dsRabotenNalog
            TabOrder = 13
            Visible = False
            OnEnter = cxDBTextEditAllEnter
            OnExit = cxDBTextEditAllExit
            OnKeyDown = EnterKakoTab
            Width = 63
          end
          object cxLabel12: TcxLabel
            Left = 51
            Top = 368
            AutoSize = False
            Caption = #1055#1088#1080#1086#1080#1088#1080#1090#1077#1090
            Style.TextColor = clRed
            Properties.Alignment.Horz = taRightJustify
            Transparent = True
            Visible = False
            Height = 17
            Width = 100
            AnchorX = 151
          end
          object txtPrioritet: TcxDBTextEdit
            Left = 161
            Top = 368
            DataBinding.DataField = 'ID_PRIORITET'
            DataBinding.DataSource = dm.dsRabotenNalog
            TabOrder = 10
            Visible = False
            OnEnter = cxDBTextEditAllEnter
            OnExit = cxDBTextEditAllExit
            OnKeyDown = EnterKakoTab
            Width = 63
          end
          object ZapisiButton: TcxButton
            Left = 608
            Top = 172
            Width = 75
            Height = 25
            Action = aZapisi
            TabOrder = 8
          end
          object cxButton2: TcxButton
            Left = 689
            Top = 172
            Width = 75
            Height = 25
            Action = aOtkazi
            TabOrder = 9
          end
          object cbRE: TcxDBLookupComboBox
            Left = 608
            Top = 86
            DataBinding.DataField = 'ID_RE'
            DataBinding.DataSource = dm.dsRabotenNalog
            Properties.DropDownAutoSize = True
            Properties.DropDownSizeable = True
            Properties.KeyFieldNames = 'ID'
            Properties.ListColumns = <
              item
                FieldName = 'ID'
              end
              item
                FieldName = 'naziv'
              end>
            Properties.ListFieldIndex = 1
            Properties.ListSource = dmMat.dsRE
            TabOrder = 14
            Visible = False
            OnEnter = cxDBTextEditAllEnter
            OnExit = cxDBTextEditAllExit
            OnKeyDown = EnterKakoTab
            Width = 240
          end
          object cbPrioritet: TcxDBLookupComboBox
            Left = 223
            Top = 368
            DataBinding.DataField = 'ID_PRIORITET'
            DataBinding.DataSource = dm.dsRabotenNalog
            Properties.DropDownAutoSize = True
            Properties.DropDownSizeable = True
            Properties.KeyFieldNames = 'ID'
            Properties.ListColumns = <
              item
                FieldName = 'ID'
              end
              item
                FieldName = 'naziv'
              end>
            Properties.ListFieldIndex = 1
            Properties.ListSource = dm.dsPrioritet
            TabOrder = 11
            Visible = False
            OnEnter = cxDBTextEditAllEnter
            OnExit = cxDBTextEditAllExit
            OnKeyDown = EnterKakoTab
            Width = 240
          end
          object cxDBImageComboBox1: TcxDBImageComboBox
            Left = 141
            Top = 15
            DataBinding.DataField = 'STATUS'
            DataBinding.DataSource = dm.dsRabotenNalog
            Properties.Images = dmRes.cxImageGrid
            Properties.Items = <
              item
                Description = #1055#1051#1040#1053
                ImageIndex = 4
                Value = 1
              end>
            Properties.ReadOnly = True
            Style.BorderStyle = ebsNone
            Style.Color = clBtnFace
            Style.LookAndFeel.NativeStyle = False
            Style.TransparentBorder = True
            Style.ButtonStyle = btsDefault
            Style.ButtonTransparency = ebtHideInactive
            StyleDisabled.LookAndFeel.NativeStyle = False
            StyleFocused.LookAndFeel.NativeStyle = False
            StyleHot.LookAndFeel.NativeStyle = False
            TabOrder = 19
            Visible = False
            Width = 140
          end
          object cxLabel27: TcxLabel
            Left = 727
            Top = 13
            AutoSize = False
            Caption = #1057#1090#1072#1090#1091#1089
            Style.TextColor = clRed
            Properties.Alignment.Horz = taRightJustify
            Transparent = True
            Visible = False
            Height = 17
            Width = 100
            AnchorX = 827
          end
          object cxLabel36: TcxLabel
            Left = 70
            Top = 87
            Caption = #1047#1072' '#1055#1072#1088#1090#1085#1077#1088' '
            ParentFont = False
            Style.Font.Charset = RUSSIAN_CHARSET
            Style.Font.Color = clWindowText
            Style.Font.Height = -11
            Style.Font.Name = 'Tahoma'
            Style.Font.Style = []
            Style.TextColor = clRed
            Style.IsFontAssigned = True
            Properties.Alignment.Horz = taRightJustify
            Properties.WordWrap = True
            Transparent = True
            Width = 65
            AnchorX = 135
          end
          object txtPartner: TcxDBTextEdit
            Tag = 1
            Left = 200
            Top = 86
            Hint = #1064#1080#1092#1088#1072' '#1085#1072' '#1055#1072#1088#1090#1085#1077#1088
            DataBinding.DataField = 'PARTNER'
            DataBinding.DataSource = dm.dsPoracka
            ParentFont = False
            TabOrder = 3
            OnEnter = cxDBTextEditAllEnter
            OnExit = cxDBTextEditAllExit
            OnKeyDown = EnterKakoTab
            Width = 58
          end
          object cbNazivPartner: TcxLookupComboBox
            Left = 258
            Top = 86
            Hint = #1053#1072#1079#1080#1074' '#1085#1072' '#1055#1072#1088#1090#1085#1077#1088
            Properties.ClearKey = 46
            Properties.DropDownAutoSize = True
            Properties.DropDownSizeable = True
            Properties.KeyFieldNames = 'tip_partner;id'
            Properties.ListColumns = <
              item
                Width = 40
                FieldName = 'TIP_PARTNER'
              end
              item
                Width = 60
                FieldName = 'id'
              end
              item
                Width = 170
                FieldName = 'naziv'
              end>
            Properties.ListFieldIndex = 2
            Properties.ListOptions.SyncMode = True
            Properties.ListSource = dmMat.dsPartner
            TabOrder = 4
            OnEnter = cxDBTextEditAllEnter
            OnExit = cxDBTextEditAllExit
            OnKeyDown = EnterKakoTab
            Width = 293
          end
          object txtTipPartner: TcxDBTextEdit
            Tag = 1
            Left = 141
            Top = 86
            Hint = #1058#1080#1087' '#1085#1072' '#1055#1072#1088#1090#1085#1077#1088
            DataBinding.DataField = 'TIP_PARTNER'
            DataBinding.DataSource = dm.dsPoracka
            ParentFont = False
            TabOrder = 2
            OnEnter = cxDBTextEditAllEnter
            OnExit = cxDBTextEditAllExit
            OnKeyDown = EnterKakoTab
            Width = 58
          end
          object cxGroupBox4: TcxGroupBox
            Left = 515
            Top = 107
            Caption = '  '#1044#1072#1090#1091#1084' '#1085#1072' '#1080#1089#1087#1086#1088#1072#1082#1072'  '
            Style.TextColor = clBlue
            TabOrder = 22
            Visible = False
            Height = 59
            Width = 330
            object cxDBDateEdit1: TcxDBDateEdit
              Left = 42
              Top = 24
              DataBinding.DataField = 'DATUM_OD_ISPORAKA'
              DataBinding.DataSource = dm.dsPoracka
              TabOrder = 0
              OnEnter = cxDBTextEditAllEnter
              OnExit = cxDBTextEditAllExit
              OnKeyDown = EnterKakoTab
              Width = 126
            end
            object cxDBDateEdit2: TcxDBDateEdit
              Left = 203
              Top = 24
              DataBinding.DataField = 'DATUM_DO_ISPORAKA'
              DataBinding.DataSource = dm.dsPoracka
              TabOrder = 1
              OnEnter = cxDBTextEditAllEnter
              OnExit = cxDBTextEditAllExit
              OnKeyDown = EnterKakoTab
              Width = 110
            end
            object cxLabel1: TcxLabel
              Left = 19
              Top = 26
              Caption = #1054#1076
              Style.TextColor = clBlue
            end
            object cxLabel5: TcxLabel
              Left = 179
              Top = 26
              Caption = #1044#1086
              Style.TextColor = clBlue
            end
          end
          object cxLabel10: TcxLabel
            Left = 35
            Top = 62
            AutoSize = False
            Caption = #1056#1077#1092#1077#1088#1077#1085#1090#1077#1085' '#1073#1088#1086#1112
            Style.TextColor = clBlue
            Properties.Alignment.Horz = taRightJustify
            Transparent = True
            Height = 17
            Width = 100
            AnchorX = 135
          end
          object txtRefNo: TcxDBTextEdit
            Left = 141
            Top = 61
            Hint = 
              #1056#1077#1092#1077#1088#1077#1085#1090#1077#1085' '#1073#1088#1086#1112' '#1086#1076' '#1082#1091#1087#1091#1074#1072#1095#1086#1090'. '#1040#1082#1086' '#1080#1084#1072' '#1087#1086#1074#1077#1116#1077', '#1076#1072' '#1089#1077' '#1074#1085#1077#1089#1072#1090' '#1088#1072#1079#1076#1077 +
              #1083#1077#1085#1080' '#1089#1086' '#1087#1088#1072#1079#1085#1086' '#1084#1077#1089#1090#1086
            DataBinding.DataField = 'REF_NO'
            DataBinding.DataSource = dm.dsPoracka
            TabOrder = 1
            OnEnter = cxDBTextEditAllEnter
            OnExit = cxDBTextEditAllExit
            OnKeyDown = EnterKakoTab
            Width = 410
          end
          object txtDatum: TcxDBDateEdit
            Left = 141
            Top = 138
            Hint = #1044#1072#1090#1091#1084' '#1085#1072' '#1055#1086#1088#1072#1095#1082#1072
            DataBinding.DataField = 'DATUM'
            DataBinding.DataSource = dm.dsPoracka
            Properties.Kind = ckDateTime
            TabOrder = 7
            OnEnter = cxDBTextEditAllEnter
            OnExit = cxDBTextEditAllExit
            OnKeyDown = EnterKakoTab
            Width = 230
          end
          object cxLabel3: TcxLabel
            Left = 89
            Top = 113
            AutoSize = False
            Caption = #1057#1090#1072#1090#1091#1089
            Style.TextColor = clRed
            Properties.Alignment.Horz = taRightJustify
            Transparent = True
            Height = 17
            Width = 46
            AnchorX = 135
          end
          object txtStatusP: TcxDBTextEdit
            Tag = 1
            Left = 141
            Top = 112
            Hint = #1064#1080#1092#1088#1072' '#1085#1072' '#1089#1090#1072#1090#1091#1089
            DataBinding.DataField = 'STATUS'
            DataBinding.DataSource = dm.dsPoracka
            TabOrder = 5
            OnEnter = cxDBTextEditAllEnter
            OnExit = cxDBTextEditAllExit
            OnKeyDown = EnterKakoTab
            Width = 59
          end
          object cbStatusP: TcxDBLookupComboBox
            Left = 200
            Top = 112
            Hint = #1053#1072#1079#1080#1074' '#1085#1072' '#1089#1090#1072#1090#1091#1089
            DataBinding.DataField = 'STATUS'
            DataBinding.DataSource = dm.dsPoracka
            Properties.DropDownAutoSize = True
            Properties.DropDownSizeable = True
            Properties.KeyFieldNames = 'VREDNOST'
            Properties.ListColumns = <
              item
                Width = 20
                FieldName = 'ID'
              end
              item
                Width = 150
                FieldName = 'naziv'
              end>
            Properties.ListFieldIndex = 1
            Properties.ListSource = dm.dsMatStatus
            TabOrder = 6
            OnEnter = cxDBTextEditAllEnter
            OnExit = cxDBTextEditAllExit
            OnKeyDown = EnterKakoTab
            Width = 171
          end
          object cxLabel6: TcxLabel
            Left = 89
            Top = 139
            AutoSize = False
            Caption = #1044#1072#1090#1091#1084
            Style.TextColor = clRed
            Properties.Alignment.Horz = taRightJustify
            Transparent = True
            Height = 21
            Width = 46
            AnchorX = 135
          end
        end
      end
    end
    object tsStavki: TcxTabSheet
      Caption = '  '#1057#1090#1072#1074#1082#1080' '#1085#1072' '#1055#1086#1088#1072#1095#1082#1072' '
      ImageIndex = 59
      object dPanelO: TPanel
        Left = 0
        Top = 0
        Width = 956
        Height = 390
        Align = alClient
        TabOrder = 0
        object cxGrid3: TcxGrid
          Left = 1
          Top = 1
          Width = 954
          Height = 388
          Align = alClient
          TabOrder = 0
          LookAndFeel.NativeStyle = False
          RootLevelOptions.DetailTabsPosition = dtpTop
          object cxGrid3DBTableView1: TcxGridDBTableView
            OnDblClick = cxGrid3DBTableView1DblClick
            OnKeyDown = cxGrid3DBTableView1KeyDown
            Navigator.Buttons.CustomButtons = <>
            ScrollbarAnnotations.CustomAnnotations = <>
            OnFocusedRecordChanged = cxGrid3DBTableView1FocusedRecordChanged
            DataController.DataSource = dm.dsPorackaStavka
            DataController.Summary.DefaultGroupSummaryItems = <>
            DataController.Summary.FooterSummaryItems = <
              item
              end>
            DataController.Summary.SummaryGroups = <>
            FilterRow.InfoText = #1050#1083#1080#1082#1085#1077#1090#1077' '#1090#1091#1082#1072' '#1079#1072' '#1076#1072' '#1076#1077#1092#1080#1085#1080#1088#1072#1090#1077' '#1092#1080#1083#1090#1077#1088
            FilterRow.Visible = True
            OptionsBehavior.IncSearch = True
            OptionsCustomize.ColumnsQuickCustomization = True
            OptionsData.Deleting = False
            OptionsData.Inserting = False
            OptionsView.Footer = True
            object cxGrid3DBTableView1BOJA: TcxGridDBColumn
              DataBinding.FieldName = 'BOJA'
              PropertiesClassName = 'TcxColorComboBoxProperties'
              Properties.CustomColors = <>
              Properties.DropDownSizeable = True
              Properties.ShowDescriptions = False
              Options.Editing = False
              Width = 34
              IsCaptionAssigned = True
            end
            object cxGrid3DBTableView1NAZIV_STATUS: TcxGridDBColumn
              Caption = #1057#1090#1072#1090#1091#1089
              DataBinding.FieldName = 'NAZIV_STATUS'
              Options.Editing = False
              Width = 113
            end
            object cxGrid3DBTableView1ID: TcxGridDBColumn
              Caption = #1064#1080#1092#1088#1072
              DataBinding.FieldName = 'ID'
              Visible = False
              Options.Editing = False
            end
            object cxGrid3DBTableView1BROJ: TcxGridDBColumn
              Caption = #1041#1088#1086#1112
              DataBinding.FieldName = 'BROJ'
              Options.Editing = False
              Width = 37
            end
            object cxGrid3DBTableView1ID_PORACKA: TcxGridDBColumn
              Caption = #1064#1080#1092#1088#1072' '#1085#1072' '#1087#1086#1088#1072#1095#1082#1072
              DataBinding.FieldName = 'ID_PORACKA'
              Visible = False
              Options.Editing = False
            end
            object cxGrid3DBTableView1VID_ARTIKAL: TcxGridDBColumn
              Caption = #1042#1080#1076' '#1085#1072' '#1072#1088#1090#1080#1082#1072#1083
              DataBinding.FieldName = 'VID_ARTIKAL'
              Options.Editing = False
            end
            object cxGrid3DBTableView1ARTIKAL: TcxGridDBColumn
              Caption = #1064#1080#1092#1088#1072' '#1085#1072' '#1072#1088#1090#1080#1082#1072#1083
              DataBinding.FieldName = 'ARTIKAL'
              Options.Editing = False
              Width = 40
            end
            object cxGrid3DBTableView1NAZIV_ARTIKAL: TcxGridDBColumn
              Caption = #1040#1088#1090#1080#1082#1072#1083
              DataBinding.FieldName = 'NAZIV_ARTIKAL'
              Options.Editing = False
              Width = 163
            end
            object cxGrid3DBTableView1KOLICINA: TcxGridDBColumn
              Caption = #1050#1086#1083#1080#1095#1080#1085#1072
              DataBinding.FieldName = 'KOLICINA'
              Options.Editing = False
            end
            object cxGrid3DBTableView1TAPACIR_SEDISTE: TcxGridDBColumn
              Caption = #1058#1072#1087#1072#1094#1080#1088' '#1085#1072' '#1089#1077#1076#1080#1096#1090#1077
              DataBinding.FieldName = 'TAPACIR_SEDISTE'
              Visible = False
              Options.Editing = False
              Width = 170
            end
            object cxGrid3DBTableView1TAPACIR_NAZAD: TcxGridDBColumn
              Caption = #1058#1072#1087#1072#1094#1080#1088' '#1085#1072#1079#1072#1076
              DataBinding.FieldName = 'TAPACIR_NAZAD'
              Visible = False
              Options.Editing = False
              Width = 226
            end
            object cxGrid3DBTableView1SUNGER: TcxGridDBColumn
              Caption = #1057#1091#1085#1107#1077#1088
              DataBinding.FieldName = 'SUNGER'
              Visible = False
              Options.Editing = False
              Width = 143
            end
            object cxGrid3DBTableView1BOJA_NOGARKI: TcxGridDBColumn
              Caption = #1041#1086#1112#1072' '#1085#1072' '#1085#1086#1075#1072#1088#1082#1080
              DataBinding.FieldName = 'BOJA_NOGARKI'
              Visible = False
              Options.Editing = False
              Width = 284
            end
            object cxGrid3DBTableView1LABELS: TcxGridDBColumn
              DataBinding.FieldName = 'LABELS'
              Visible = False
              Options.Editing = False
            end
            object cxGrid3DBTableView1GLIDERS: TcxGridDBColumn
              DataBinding.FieldName = 'GLIDERS'
              Visible = False
              Options.Editing = False
              Width = 142
            end
            object cxGrid3DBTableView1SLIKA: TcxGridDBColumn
              Caption = #1057#1083#1080#1082#1072
              DataBinding.FieldName = 'SLIKA'
              Options.Editing = False
            end
            object cxGrid3DBTableView1SERISKI_BROJ: TcxGridDBColumn
              Caption = #1089#1077#1088#1080#1089#1082#1080' '#1073#1088#1086#1112
              DataBinding.FieldName = 'SERISKI_BROJ'
              Visible = False
              Options.Editing = False
              Width = 182
            end
            object cxGrid3DBTableView1ZABELESKA: TcxGridDBColumn
              Caption = #1047#1072#1073#1077#1083#1077#1096#1082#1072
              DataBinding.FieldName = 'ZABELESKA'
              PropertiesClassName = 'TcxBlobEditProperties'
              Properties.BlobEditKind = bekMemo
              Properties.BlobPaintStyle = bpsText
              Properties.ReadOnly = True
              Width = 134
            end
            object cxGrid3DBTableView1ISPORAKA_OD_DATUM: TcxGridDBColumn
              Caption = #1048#1089#1087#1086#1088#1072#1082#1072' '#1086#1076' '#1076#1072#1090#1091#1084' '
              DataBinding.FieldName = 'ISPORAKA_OD_DATUM'
              Options.Editing = False
              Width = 72
            end
            object cxGrid3DBTableView1ISPORAKA_DO_DATUM: TcxGridDBColumn
              Caption = #1048#1089#1087#1086#1088#1072#1082#1072' '#1076#1086' '#1076#1072#1090#1091#1084
              DataBinding.FieldName = 'ISPORAKA_DO_DATUM'
              Options.Editing = False
              Width = 68
            end
            object cxGrid3DBTableView1STATUS: TcxGridDBColumn
              Caption = #1057#1090#1072#1090#1091#1089
              DataBinding.FieldName = 'STATUS'
              Visible = False
              Options.Editing = False
            end
            object cxGrid3DBTableView1CUSTOMER_NAME: TcxGridDBColumn
              DataBinding.FieldName = 'CUSTOMER_NAME'
              Width = 150
            end
            object cxGrid3DBTableView1PALETA_BR1: TcxGridDBColumn
              Caption = #1055#1072#1083#1077#1090#1072' '#1073#1088'.1'
              DataBinding.FieldName = 'PALETA_BR1'
              Width = 70
            end
            object cxGrid3DBTableView1PALETA_BR2: TcxGridDBColumn
              Caption = #1055#1072#1083#1077#1090#1072' '#1073#1088'.2'
              DataBinding.FieldName = 'PALETA_BR2'
              Width = 71
            end
            object cxGrid3DBTableView1PALETA_BR3: TcxGridDBColumn
              Caption = #1055#1072#1083#1077#1090#1072' '#1073#1088'.3'
              DataBinding.FieldName = 'PALETA_BR3'
              Width = 68
            end
          end
          object cxGrid3Level1: TcxGridLevel
            Caption = #1055#1086#1088#1072#1095#1082#1080
            GridView = cxGrid3DBTableView1
          end
        end
      end
      object lPanelO: TPanel
        Left = 0
        Top = 390
        Width = 956
        Height = 298
        Align = alBottom
        TabOrder = 1
        object cxPageControl2: TcxPageControl
          Left = 1
          Top = 1
          Width = 954
          Height = 296
          Align = alClient
          Font.Charset = RUSSIAN_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          TabOrder = 0
          Properties.ActivePage = tsStavkiPoracka
          Properties.CustomButtons.Buttons = <>
          OnChange = cxPageControl2Change
          ClientRectBottom = 296
          ClientRectRight = 954
          ClientRectTop = 27
          object tsStavkiPoracka: TcxTabSheet
            Caption = #1057#1090#1072#1074#1082#1080' '#1085#1072' '#1055#1086#1088#1072#1095#1082#1072
            ImageIndex = 0
            object lPanelO1: TPanel
              Left = 0
              Top = 0
              Width = 954
              Height = 269
              Align = alClient
              TabOrder = 0
              object gbRNStavki: TcxGroupBox
                Left = 1
                Top = 1
                Align = alClient
                Enabled = False
                TabOrder = 0
                Height = 267
                Width = 952
                object Label1: TLabel
                  Left = 530
                  Top = 9
                  Width = 56
                  Height = 13
                  Alignment = taRightJustify
                  Caption = #1047#1072#1073#1077#1083#1077#1096#1082#1072
                  Font.Charset = RUSSIAN_CHARSET
                  Font.Color = clBlue
                  Font.Height = -11
                  Font.Name = 'Tahoma'
                  Font.Style = []
                  ParentFont = False
                end
                object Label5: TLabel
                  Left = 296
                  Top = 50
                  Width = 31
                  Height = 13
                  Alignment = taRightJustify
                  Caption = #1057#1083#1080#1082#1072
                  Font.Charset = RUSSIAN_CHARSET
                  Font.Color = clBlue
                  Font.Height = -11
                  Font.Name = 'Tahoma'
                  Font.Style = []
                  ParentFont = False
                end
                object cxLabel8: TcxLabel
                  Left = 16
                  Top = 16
                  Caption = #1040#1088#1090#1080#1082#1072#1083' / '#1055#1088#1086#1080#1079#1074#1086#1076
                  Style.TextColor = clRed
                  Properties.Alignment.Horz = taRightJustify
                  Properties.WordWrap = True
                  Transparent = True
                  Width = 65
                  AnchorX = 81
                end
                object txtVidArtikal: TcxDBTextEdit
                  Tag = 1
                  Left = 94
                  Top = 22
                  DataBinding.DataField = 'VID_ARTIKAL'
                  DataBinding.DataSource = dm.dsPorackaStavka
                  ParentFont = False
                  Style.Font.Charset = DEFAULT_CHARSET
                  Style.Font.Color = clWindowText
                  Style.Font.Height = -13
                  Style.Font.Name = 'Tahoma'
                  Style.Font.Style = []
                  Style.IsFontAssigned = True
                  TabOrder = 0
                  OnEnter = cxDBTextEditAllEnter
                  OnExit = cxDBTextEditAllExit
                  OnKeyDown = EnterKakoTab
                  Width = 61
                end
                object txtArtikal: TcxDBTextEdit
                  Tag = 1
                  Left = 155
                  Top = 23
                  DataBinding.DataField = 'ARTIKAL'
                  DataBinding.DataSource = dm.dsPorackaStavka
                  ParentFont = False
                  Style.Font.Charset = DEFAULT_CHARSET
                  Style.Font.Color = clWindowText
                  Style.Font.Height = -13
                  Style.Font.Name = 'Tahoma'
                  Style.Font.Style = []
                  Style.IsFontAssigned = True
                  TabOrder = 1
                  OnEnter = cxDBTextEditAllEnter
                  OnExit = cxDBTextEditAllExit
                  OnKeyDown = EnterKakoTab
                  Width = 63
                end
                object cxLabel9: TcxLabel
                  Left = 20
                  Top = 77
                  Caption = #1050#1086#1083#1080#1095#1080#1085#1072
                  Style.TextColor = clRed
                  Properties.Alignment.Horz = taRightJustify
                  Transparent = True
                  AnchorX = 80
                end
                object txtKolicinaArtikal: TcxDBTextEdit
                  Tag = 1
                  Left = 94
                  Top = 77
                  DataBinding.DataField = 'KOLICINA'
                  DataBinding.DataSource = dm.dsPorackaStavka
                  ParentFont = False
                  Style.BorderStyle = ebsUltraFlat
                  Style.Font.Charset = RUSSIAN_CHARSET
                  Style.Font.Color = clWindowText
                  Style.Font.Height = -13
                  Style.Font.Name = 'Tahoma'
                  Style.Font.Style = []
                  Style.LookAndFeel.NativeStyle = False
                  Style.LookAndFeel.SkinName = ''
                  Style.TextStyle = []
                  Style.TransparentBorder = False
                  Style.IsFontAssigned = True
                  StyleDisabled.LookAndFeel.NativeStyle = False
                  StyleDisabled.LookAndFeel.SkinName = ''
                  StyleFocused.LookAndFeel.NativeStyle = False
                  StyleFocused.LookAndFeel.SkinName = ''
                  StyleHot.LookAndFeel.NativeStyle = False
                  StyleHot.LookAndFeel.SkinName = ''
                  TabOrder = 4
                  OnEnter = cxDBTextEditAllEnter
                  OnExit = cxDBTextEditAllExit
                  OnKeyDown = EnterKakoTab
                  Width = 124
                end
                object gbIsporaka: TcxGroupBox
                  Left = 90
                  Top = 137
                  Caption = '  '#1044#1072#1090#1091#1084' '#1085#1072' '#1080#1089#1087#1086#1088#1072#1082#1072'  '
                  Style.TextColor = clBlue
                  TabOrder = 8
                  Height = 59
                  Width = 395
                  object txtIspDatumOd: TcxDBDateEdit
                    Left = 42
                    Top = 24
                    DataBinding.DataField = 'ISPORAKA_OD_DATUM'
                    DataBinding.DataSource = dm.dsPorackaStavka
                    TabOrder = 0
                    OnEnter = cxDBTextEditAllEnter
                    OnExit = cxDBTextEditAllExit
                    OnKeyDown = EnterKakoTab
                    Width = 111
                  end
                  object txtIspDatumDo: TcxDBDateEdit
                    Left = 210
                    Top = 24
                    DataBinding.DataField = 'ISPORAKA_DO_DATUM'
                    DataBinding.DataSource = dm.dsPorackaStavka
                    TabOrder = 1
                    OnEnter = cxDBTextEditAllEnter
                    OnExit = cxDBTextEditAllExit
                    OnKeyDown = EnterKakoTab
                    Width = 110
                  end
                  object cxLabel20: TcxLabel
                    Left = 19
                    Top = 26
                    Caption = #1054#1076
                    Style.TextColor = clBlue
                    Transparent = True
                  end
                  object cxLabel21: TcxLabel
                    Left = 186
                    Top = 26
                    Caption = #1044#1086
                    Style.TextColor = clBlue
                    Transparent = True
                  end
                end
                object ImgGolema: TcxDBImage
                  Left = 336
                  Top = 50
                  Hint = #1057#1083#1080#1082#1072' '#1085#1072' '#1072#1088#1090#1080#1082#1083#1086#1090'/'#1087#1088#1086#1080#1079#1074#1086#1076#1086#1090', '#1096#1090#1086' '#1084#1086#1078#1077' '#1076#1072' '#1112#1072' '#1087#1088#1072#1090#1080' '#1082#1091#1087#1091#1074#1072#1095#1086#1090
                  DataBinding.DataField = 'SLIKA'
                  DataBinding.DataSource = dm.dsPorackaStavka
                  Properties.ClearKey = 46
                  Properties.FitMode = ifmProportionalStretch
                  TabOrder = 22
                  OnKeyDown = EnterKakoTab
                  Height = 48
                  Width = 110
                end
                object Button1: TButton
                  Left = 452
                  Top = 50
                  Width = 37
                  Height = 18
                  Hint = #1048#1079#1073#1077#1088#1080' '#1089#1083#1080#1082#1072
                  Caption = '...'
                  TabOrder = 28
                  TabStop = False
                  OnClick = Button1Click
                  OnEnter = cxDBTextEditAllEnter
                  OnExit = cxDBTextEditAllExit
                  OnKeyDown = EnterKakoTab
                end
                object cxLabel22: TcxLabel
                  Left = 149
                  Top = 361
                  Caption = #1055#1086#1088#1072#1095#1082#1072
                  ParentFont = False
                  Style.Font.Charset = RUSSIAN_CHARSET
                  Style.Font.Color = clRed
                  Style.Font.Height = -11
                  Style.Font.Name = 'Tahoma'
                  Style.Font.Style = []
                  Style.TextColor = clBlue
                  Style.IsFontAssigned = True
                  Properties.Alignment.Horz = taRightJustify
                  Transparent = True
                  Visible = False
                  AnchorX = 196
                end
                object txtPorackaS: TcxDBTextEdit
                  Left = 218
                  Top = 357
                  DataBinding.DataField = 'ID_PORACKA'
                  DataBinding.DataSource = dm.dsRNStavka
                  ParentFont = False
                  Style.BorderStyle = ebsUltraFlat
                  Style.Font.Charset = RUSSIAN_CHARSET
                  Style.Font.Color = clWindowText
                  Style.Font.Height = -11
                  Style.Font.Name = 'Tahoma'
                  Style.Font.Style = []
                  Style.LookAndFeel.NativeStyle = False
                  Style.LookAndFeel.SkinName = ''
                  Style.TextStyle = []
                  Style.TransparentBorder = False
                  Style.IsFontAssigned = True
                  StyleDisabled.LookAndFeel.NativeStyle = False
                  StyleDisabled.LookAndFeel.SkinName = ''
                  StyleDisabled.TextColor = clWindowText
                  StyleFocused.LookAndFeel.NativeStyle = False
                  StyleFocused.LookAndFeel.SkinName = ''
                  StyleHot.LookAndFeel.NativeStyle = False
                  StyleHot.LookAndFeel.SkinName = ''
                  TabOrder = 12
                  Visible = False
                  OnEnter = cxDBTextEditAllEnter
                  OnExit = cxDBTextEditAllExit
                  OnKeyDown = EnterKakoTab
                  Width = 56
                end
                object cbPorackaS: TcxDBLookupComboBox
                  Left = 276
                  Top = 356
                  RepositoryItem = dm.erLookupComboBox
                  AutoSize = False
                  DataBinding.DataField = 'ID_PORACKA'
                  DataBinding.DataSource = dm.dsRNStavka
                  Properties.KeyFieldNames = 'ID'
                  Properties.ListColumns = <
                    item
                      FieldName = 'NAZIV'
                    end>
                  TabOrder = 13
                  Visible = False
                  OnEnter = cxDBTextEditAllEnter
                  OnExit = cxDBTextEditAllExit
                  OnKeyDown = EnterKakoTab
                  Height = 21
                  Width = 134
                end
                object txtKraenDatum: TcxDBDateEdit
                  Left = 573
                  Top = 319
                  DataBinding.DataField = 'KRAEN_DATUM_PREVOZ'
                  DataBinding.DataSource = dm.dsRNStavka
                  TabOrder = 16
                  Visible = False
                  OnEnter = cxDBTextEditAllEnter
                  OnExit = cxDBTextEditAllExit
                  OnKeyDown = EnterKakoTab
                  Width = 111
                end
                object cxLabel23: TcxLabel
                  Left = 443
                  Top = 320
                  AutoSize = False
                  Caption = #1050#1088#1072#1077#1085' '#1076#1072#1090#1091#1084' '#1085#1072' '#1087#1088#1077#1074#1086#1079
                  Style.TextColor = clBlue
                  StyleFocused.TextColor = clWindowText
                  StyleHot.TextColor = clWindowText
                  Properties.WordWrap = True
                  Transparent = True
                  Visible = False
                  Height = 20
                  Width = 124
                end
                object txtBrCiklusiS: TcxDBTextEdit
                  Left = 239
                  Top = 382
                  DataBinding.DataField = 'BROJ_CIKLUSI'
                  DataBinding.DataSource = dm.dsRNStavka
                  TabOrder = 18
                  Visible = False
                  OnEnter = cxDBTextEditAllEnter
                  OnExit = cxDBTextEditAllExit
                  OnKeyDown = EnterKakoTab
                  Width = 131
                end
                object cxLabel24: TcxLabel
                  Left = 149
                  Top = 382
                  Caption = #1041#1088#1086#1112' '#1085#1072' '#1094#1080#1082#1083#1091#1089#1080
                  Style.TextColor = clBlue
                  Properties.Alignment.Horz = taRightJustify
                  Properties.WordWrap = True
                  Transparent = True
                  Visible = False
                  Width = 50
                  AnchorX = 199
                end
                object cxLabel25: TcxLabel
                  Left = 30
                  Top = 354
                  Caption = #1041#1088#1086#1112' '#1085#1072' '#1095#1072#1089#1086#1074#1080
                  Style.TextColor = clBlue
                  Properties.Alignment.Horz = taRightJustify
                  Properties.WordWrap = True
                  Transparent = True
                  Visible = False
                  Width = 45
                  AnchorX = 75
                end
                object txtBrCasoviS: TcxDBTextEdit
                  Left = 30
                  Top = 377
                  DataBinding.DataField = 'BROJ_CASOVI'
                  DataBinding.DataSource = dm.dsRNStavka
                  TabOrder = 17
                  Visible = False
                  OnEnter = cxDBTextEditAllEnter
                  OnExit = cxDBTextEditAllExit
                  OnKeyDown = EnterKakoTab
                  Width = 131
                end
                object cxLabel28: TcxLabel
                  Left = 475
                  Top = 106
                  AutoSize = False
                  Caption = #1057#1090#1072#1090#1091#1089
                  Style.TextColor = clRed
                  Properties.Alignment.Horz = taRightJustify
                  Transparent = True
                  Height = 17
                  Width = 46
                  AnchorX = 521
                end
                object txtStatusS: TcxDBTextEdit
                  Left = 527
                  Top = 105
                  DataBinding.DataField = 'STATUS'
                  DataBinding.DataSource = dm.dsPorackaStavka
                  StyleDisabled.BorderColor = clWindow
                  StyleDisabled.TextColor = clWindowText
                  TabOrder = 29
                  OnEnter = cxDBTextEditAllEnter
                  OnExit = cxDBTextEditAllExit
                  OnKeyDown = EnterKakoTab
                  Width = 59
                end
                object cbStatusS: TcxDBLookupComboBox
                  Left = 586
                  Top = 105
                  DataBinding.DataField = 'STATUS'
                  DataBinding.DataSource = dm.dsPorackaStavka
                  Properties.DropDownAutoSize = True
                  Properties.DropDownSizeable = True
                  Properties.KeyFieldNames = 'VREDNOST'
                  Properties.ListColumns = <
                    item
                      Width = 20
                      FieldName = 'ID'
                    end
                    item
                      Width = 150
                      FieldName = 'naziv'
                    end>
                  Properties.ListFieldIndex = 1
                  Properties.ListSource = dm.dsMatStatus
                  StyleDisabled.BorderColor = clWindow
                  StyleDisabled.TextColor = clWindowText
                  TabOrder = 30
                  OnEnter = cxDBTextEditAllEnter
                  OnExit = cxDBTextEditAllExit
                  OnKeyDown = EnterKakoTab
                  Width = 215
                end
                object cxButton1: TcxButton
                  Left = 642
                  Top = 166
                  Width = 75
                  Height = 25
                  Action = aZapisi
                  TabOrder = 10
                end
                object cxButton3: TcxButton
                  Left = 723
                  Top = 166
                  Width = 75
                  Height = 25
                  Action = aOtkazi
                  TabOrder = 11
                end
                object txtZabeleska: TcxDBMemo
                  Left = 527
                  Top = 23
                  Hint = '*'#1050#1083#1080#1082#1085#1077#1090#1077' '#1076#1074#1072#1087#1072#1090#1080', '#1079#1072' '#1086#1090#1074#1086#1088#1072#1114#1077' '#1085#1072' Notepad!  '
                  DataBinding.DataField = 'ZABELESKA'
                  DataBinding.DataSource = dm.dsPorackaStavka
                  TabOrder = 9
                  OnDblClick = aUrediTextOpisExecute
                  OnEnter = cxDBTextEditAllEnter
                  OnExit = cxDBTextEditAllExit
                  OnKeyDown = EnterKakoTab
                  Height = 75
                  Width = 274
                end
                object cxLabel37: TcxLabel
                  Left = 91
                  Top = 284
                  AutoSize = False
                  Caption = #1054#1076#1086#1073#1088#1080#1083
                  ParentFont = False
                  Style.Font.Charset = RUSSIAN_CHARSET
                  Style.Font.Color = clWindowText
                  Style.Font.Height = -11
                  Style.Font.Name = 'Tahoma'
                  Style.Font.Style = []
                  Style.TextColor = clBlue
                  Style.IsFontAssigned = True
                  Properties.Alignment.Horz = taRightJustify
                  Properties.WordWrap = True
                  Transparent = True
                  Visible = False
                  Height = 18
                  Width = 52
                  AnchorX = 143
                end
                object txtTipOdobril: TcxDBTextEdit
                  Left = 91
                  Top = 304
                  Hint = #1058#1080#1087' '#1085#1072' '#1055#1072#1088#1090#1085#1077#1088
                  DataBinding.DataField = 'TIP_ODOBRIL'
                  DataBinding.DataSource = dm.dsRNStavka
                  ParentFont = False
                  TabOrder = 19
                  Visible = False
                  OnEnter = cxDBTextEditAllEnter
                  OnExit = cxDBTextEditAllExit
                  OnKeyDown = EnterKakoTab
                  Width = 46
                end
                object txtOdobril: TcxDBTextEdit
                  Left = 137
                  Top = 304
                  Hint = #1064#1080#1092#1088#1072' '#1085#1072' '#1055#1072#1088#1090#1085#1077#1088
                  DataBinding.DataField = 'ODOBRIL'
                  DataBinding.DataSource = dm.dsRNStavka
                  ParentFont = False
                  TabOrder = 20
                  Visible = False
                  OnEnter = cxDBTextEditAllEnter
                  OnExit = cxDBTextEditAllExit
                  OnKeyDown = EnterKakoTab
                  Width = 58
                end
                object cbOdobril: TcxLookupComboBox
                  Left = 195
                  Top = 304
                  Hint = #1053#1072#1079#1080#1074' '#1085#1072' '#1055#1072#1088#1090#1085#1077#1088
                  Properties.ClearKey = 46
                  Properties.DropDownAutoSize = True
                  Properties.DropDownSizeable = True
                  Properties.KeyFieldNames = 'tip_partner;id'
                  Properties.ListColumns = <
                    item
                      Width = 40
                      FieldName = 'TIP_PARTNER'
                    end
                    item
                      Width = 60
                      FieldName = 'id'
                    end
                    item
                      Width = 170
                      FieldName = 'naziv'
                    end>
                  Properties.ListFieldIndex = 2
                  Properties.ListOptions.SyncMode = True
                  Properties.ListSource = dmMat.dsPartner
                  TabOrder = 21
                  Visible = False
                  OnEnter = cxDBTextEditAllEnter
                  OnExit = cxDBTextEditAllExit
                  OnKeyDown = EnterKakoTab
                  Width = 226
                end
                object cxLabel4: TcxLabel
                  Left = 12
                  Top = 52
                  AutoSize = False
                  Caption = #1041#1088#1086#1112
                  Style.TextColor = clBlue
                  Properties.Alignment.Horz = taRightJustify
                  Transparent = True
                  Height = 22
                  Width = 69
                  AnchorX = 81
                end
                object txtBrojS: TcxDBTextEdit
                  Left = 94
                  Top = 49
                  DataBinding.DataField = 'BROJ'
                  DataBinding.DataSource = dm.dsPorackaStavka
                  TabOrder = 3
                  OnEnter = cxDBTextEditAllEnter
                  OnExit = cxDBTextEditAllExit
                  OnKeyDown = EnterKakoTab
                  Width = 192
                end
                object cbNazivArtikal: TcxExtLookupComboBox
                  Left = 219
                  Top = 23
                  RepositoryItem = dmRes.cxNurkoEditRepositoryExtLookupComboBox
                  Properties.ClearKey = 46
                  Properties.DropDownAutoSize = True
                  Properties.DropDownSizeable = True
                  Properties.OnChange = cxExtLookupComboBox1PropertiesEditValueChanged
                  TabOrder = 2
                  OnEnter = cxDBTextEditAllEnter
                  OnExit = cxDBTextEditAllExit
                  OnKeyDown = FormKeyDown
                  Width = 270
                end
                object cxLabel14: TcxLabel
                  Left = 34
                  Top = 105
                  AutoSize = False
                  Caption = #1062#1077#1085#1072
                  Style.TextColor = clBlue
                  Properties.Alignment.Horz = taRightJustify
                  Transparent = True
                  Visible = False
                  Height = 17
                  Width = 34
                  AnchorX = 68
                end
                object txtCena: TcxDBTextEdit
                  Left = 78
                  Top = 105
                  DataBinding.DataField = 'CENA'
                  DataBinding.DataSource = dm.dsPorackaStavka
                  TabOrder = 6
                  Visible = False
                  OnEnter = cxDBTextEditAllEnter
                  OnExit = cxDBTextEditAllExit
                  OnKeyDown = EnterKakoTab
                  Width = 124
                end
                object cbValuta: TcxDBLookupComboBox
                  Left = 203
                  Top = 105
                  DataBinding.DataField = 'VALUTA'
                  DataBinding.DataSource = dm.dsPorackaStavka
                  Properties.DropDownAutoSize = True
                  Properties.DropDownSizeable = True
                  Properties.KeyFieldNames = 'ID'
                  Properties.ListColumns = <
                    item
                      Width = 20
                      FieldName = 'ID'
                    end
                    item
                      Width = 150
                      FieldName = 'naziv'
                    end>
                  Properties.ListSource = dmMat.dsValuta
                  TabOrder = 34
                  Visible = False
                  OnEnter = cxDBTextEditAllEnter
                  OnExit = cxDBTextEditAllExit
                  OnKeyDown = EnterKakoTab
                  Width = 67
                end
                object txtKurs: TcxDBTextEdit
                  Left = 400
                  Top = 134
                  DataBinding.DataField = 'KURS'
                  DataBinding.DataSource = dm.dsPorackaStavka
                  TabOrder = 7
                  Visible = False
                  OnEnter = cxDBTextEditAllEnter
                  OnExit = cxDBTextEditAllExit
                  OnKeyDown = EnterKakoTab
                  Width = 149
                end
                object cxLabel16: TcxLabel
                  Left = 277
                  Top = 105
                  AutoSize = False
                  Caption = #1050#1091#1088#1089
                  Style.TextColor = clBlue
                  Properties.Alignment.Horz = taRightJustify
                  Transparent = True
                  Visible = False
                  Height = 17
                  Width = 34
                  AnchorX = 311
                end
                object cxlbl5: TcxLabel
                  Left = 11
                  Top = 102
                  AutoSize = False
                  Caption = 'Customer name'
                  Style.TextColor = clBlue
                  Properties.Alignment.Horz = taRightJustify
                  Properties.WordWrap = True
                  Transparent = True
                  Height = 38
                  Width = 69
                  AnchorX = 80
                end
                object txtCustomerName: TcxDBTextEdit
                  Left = 94
                  Top = 103
                  Hint = #1053#1072#1079#1080#1074' '#1085#1072' '#1087#1088#1086#1080#1079#1074#1086#1076#1086#1090' '#1089#1087#1086#1088#1077#1076' '#1082#1091#1087#1091#1074#1072#1095#1086#1090
                  DataBinding.DataField = 'CUSTOMER_NAME'
                  DataBinding.DataSource = dm.dsPorackaStavka
                  TabOrder = 5
                  OnEnter = cxDBTextEditAllEnter
                  OnExit = cxDBTextEditAllExit
                  OnKeyDown = EnterKakoTab
                  Width = 352
                end
              end
            end
          end
          object tsElementiPoracka: TcxTabSheet
            Caption = #1045#1083#1077#1084#1077#1085#1090#1080' '#1085#1072' '#1055#1086#1088#1072#1095#1082#1072
            ImageIndex = 0
            object Panel1: TPanel
              Left = 0
              Top = 0
              Width = 954
              Height = 269
              Align = alClient
              DoubleBuffered = True
              Enabled = False
              ParentBackground = False
              ParentDoubleBuffered = False
              TabOrder = 0
              object lblCustom1: TLabel
                Left = 51
                Top = 92
                Width = 33
                Height = 16
                Alignment = taRightJustify
                Caption = 'Keper'
                Font.Charset = RUSSIAN_CHARSET
                Font.Color = clBlue
                Font.Height = -13
                Font.Name = 'Tahoma'
                Font.Style = []
                ParentFont = False
              end
              object lblCustom2: TLabel
                Left = 15
                Top = 116
                Width = 69
                Height = 16
                Alignment = taRightJustify
                Caption = 'Veneer type'
                Font.Charset = RUSSIAN_CHARSET
                Font.Color = clBlue
                Font.Height = -13
                Font.Name = 'Tahoma'
                Font.Style = []
                ParentFont = False
              end
              object lblCustom3: TLabel
                Left = 41
                Top = 17
                Width = 43
                Height = 16
                Alignment = taRightJustify
                Caption = 'Sponge'
                Font.Charset = RUSSIAN_CHARSET
                Font.Color = clBlue
                Font.Height = -13
                Font.Name = 'Tahoma'
                Font.Style = []
                ParentFont = False
              end
              object Label2: TLabel
                Left = 9
                Top = 218
                Width = 72
                Height = 16
                Alignment = taRightJustify
                Caption = 'Wood colour'
                Font.Charset = RUSSIAN_CHARSET
                Font.Color = clBlue
                Font.Height = -13
                Font.Name = 'Tahoma'
                Font.Style = []
                ParentFont = False
              end
              object Label3: TLabel
                Left = 46
                Top = 43
                Width = 36
                Height = 16
                Alignment = taRightJustify
                Caption = 'Labels'
                Font.Charset = RUSSIAN_CHARSET
                Font.Color = clBlue
                Font.Height = -13
                Font.Name = 'Tahoma'
                Font.Style = []
                ParentFont = False
              end
              object Label4: TLabel
                Left = 43
                Top = 68
                Width = 39
                Height = 16
                Alignment = taRightJustify
                Caption = 'Gliders'
                Font.Charset = RUSSIAN_CHARSET
                Font.Color = clBlue
                Font.Height = -13
                Font.Name = 'Tahoma'
                Font.Style = []
                ParentFont = False
              end
              object Label10: TLabel
                Left = 4
                Top = 141
                Width = 80
                Height = 16
                Alignment = taRightJustify
                Caption = 'Veneer colour'
                Font.Charset = RUSSIAN_CHARSET
                Font.Color = clBlue
                Font.Height = -13
                Font.Name = 'Tahoma'
                Font.Style = []
                ParentFont = False
              end
              object Label11: TLabel
                Left = 471
                Top = 19
                Width = 78
                Height = 16
                Alignment = taRightJustify
                Caption = 'Laber on seat'
                Font.Charset = RUSSIAN_CHARSET
                Font.Color = clBlue
                Font.Height = -13
                Font.Name = 'Tahoma'
                Font.Style = []
                ParentFont = False
              end
              object Label12: TLabel
                Left = 471
                Top = 44
                Width = 78
                Height = 16
                Alignment = taRightJustify
                Caption = 'Label on back'
                Font.Charset = RUSSIAN_CHARSET
                Font.Color = clBlue
                Font.Height = -13
                Font.Name = 'Tahoma'
                Font.Style = []
                ParentFont = False
              end
              object Label13: TLabel
                Left = 468
                Top = 70
                Width = 81
                Height = 16
                Alignment = taRightJustify
                Caption = 'Labal on arms'
                Font.Charset = RUSSIAN_CHARSET
                Font.Color = clBlue
                Font.Height = -13
                Font.Name = 'Tahoma'
                Font.Style = []
                ParentFont = False
              end
              object Label14: TLabel
                Left = 483
                Top = 93
                Width = 65
                Height = 16
                Alignment = taRightJustify
                Caption = 'Embroidery'
                Font.Charset = RUSSIAN_CHARSET
                Font.Color = clBlue
                Font.Height = -13
                Font.Name = 'Tahoma'
                Font.Style = []
                ParentFont = False
              end
              object Label15: TLabel
                Left = 515
                Top = 118
                Width = 34
                Height = 16
                Alignment = taRightJustify
                Caption = 'Piping'
                Font.Charset = RUSSIAN_CHARSET
                Font.Color = clBlue
                Font.Height = -13
                Font.Name = 'Tahoma'
                Font.Style = []
                ParentFont = False
              end
              object Label16: TLabel
                Left = 516
                Top = 143
                Width = 32
                Height = 16
                Alignment = taRightJustify
                Caption = 'Studs'
                Font.Charset = RUSSIAN_CHARSET
                Font.Color = clBlue
                Font.Height = -13
                Font.Name = 'Tahoma'
                Font.Style = []
                ParentFont = False
              end
              object Label17: TLabel
                Left = 14
                Top = 168
                Width = 70
                Height = 16
                Alignment = taRightJustify
                Caption = 'Metal colour'
                Font.Charset = RUSSIAN_CHARSET
                Font.Color = clBlue
                Font.Height = -13
                Font.Name = 'Tahoma'
                Font.Style = []
                ParentFont = False
              end
              object Label18: TLabel
                Left = 23
                Top = 193
                Width = 61
                Height = 16
                Alignment = taRightJustify
                Caption = 'Wood type'
                Font.Charset = RUSSIAN_CHARSET
                Font.Color = clBlue
                Font.Height = -13
                Font.Name = 'Tahoma'
                Font.Style = []
                ParentFont = False
              end
              object Label19: TLabel
                Left = 475
                Top = 169
                Width = 73
                Height = 16
                Alignment = taRightJustify
                Caption = 'Special price'
                Font.Charset = RUSSIAN_CHARSET
                Font.Color = clBlue
                Font.Height = -13
                Font.Name = 'Tahoma'
                Font.Style = []
                ParentFont = False
              end
              object Label6: TLabel
                Left = 525
                Top = 194
                Width = 23
                Height = 16
                Alignment = taRightJustify
                Caption = 'SKU'
                Font.Charset = RUSSIAN_CHARSET
                Font.Color = clBlue
                Font.Height = -13
                Font.Name = 'Tahoma'
                Font.Style = []
                ParentFont = False
              end
              object txtKeper: TcxDBTextEdit
                Left = 91
                Top = 89
                BeepOnEnter = False
                DataBinding.DataField = 'KEPER'
                DataBinding.DataSource = dm.dsPorackaStavka
                Properties.BeepOnError = True
                TabOrder = 3
                OnEnter = cxDBTextEditAllEnter
                OnExit = cxDBTextEditAllExit
                OnKeyDown = EnterKakoTab
                Width = 366
              end
              object txtVeneerType: TcxDBTextEdit
                Left = 91
                Top = 114
                BeepOnEnter = False
                DataBinding.DataField = 'VENEER_TYPE'
                DataBinding.DataSource = dm.dsPorackaStavka
                Properties.BeepOnError = True
                TabOrder = 4
                OnEnter = cxDBTextEditAllEnter
                OnExit = cxDBTextEditAllExit
                OnKeyDown = EnterKakoTab
                Width = 366
              end
              object txtSunger: TcxDBTextEdit
                Left = 91
                Top = 15
                BeepOnEnter = False
                DataBinding.DataField = 'SUNGER'
                DataBinding.DataSource = dm.dsPorackaStavka
                Properties.BeepOnError = True
                TabOrder = 0
                OnEnter = cxDBTextEditAllEnter
                OnExit = cxDBTextEditAllExit
                OnKeyDown = EnterKakoTab
                Width = 366
              end
              object txtBojaNogarki: TcxDBTextEdit
                Left = 90
                Top = 215
                BeepOnEnter = False
                DataBinding.DataField = 'BOJA_NOGARKI'
                DataBinding.DataSource = dm.dsPorackaStavka
                Properties.BeepOnError = True
                TabOrder = 8
                OnEnter = cxDBTextEditAllEnter
                OnExit = cxDBTextEditAllExit
                OnKeyDown = EnterKakoTab
                Width = 366
              end
              object txtLabels: TcxDBTextEdit
                Left = 91
                Top = 40
                BeepOnEnter = False
                DataBinding.DataField = 'LABELS'
                DataBinding.DataSource = dm.dsPorackaStavka
                Properties.BeepOnError = True
                TabOrder = 1
                OnEnter = cxDBTextEditAllEnter
                OnExit = cxDBTextEditAllExit
                OnKeyDown = EnterKakoTab
                Width = 366
              end
              object txtGliders: TcxDBTextEdit
                Left = 91
                Top = 65
                BeepOnEnter = False
                DataBinding.DataField = 'GLIDERS'
                DataBinding.DataSource = dm.dsPorackaStavka
                Properties.BeepOnError = True
                TabOrder = 2
                OnEnter = cxDBTextEditAllEnter
                OnExit = cxDBTextEditAllExit
                OnKeyDown = EnterKakoTab
                Width = 366
              end
              object cxButton6: TcxButton
                Left = 693
                Top = 222
                Width = 104
                Height = 25
                Action = aZapisi
                TabOrder = 17
              end
              object cxButton7: TcxButton
                Left = 821
                Top = 222
                Width = 101
                Height = 25
                Action = aOtkazi
                TabOrder = 18
              end
              object txtVeneerColor: TcxDBTextEdit
                Left = 91
                Top = 140
                BeepOnEnter = False
                DataBinding.DataField = 'VENEER_COLOR'
                DataBinding.DataSource = dm.dsPorackaStavka
                Properties.BeepOnError = True
                TabOrder = 5
                OnEnter = cxDBTextEditAllEnter
                OnExit = cxDBTextEditAllExit
                OnKeyDown = EnterKakoTab
                Width = 366
              end
              object txtLabelOnSeat: TcxDBTextEdit
                Left = 556
                Top = 16
                BeepOnEnter = False
                DataBinding.DataField = 'LABEL_ON_SEAT'
                DataBinding.DataSource = dm.dsPorackaStavka
                Properties.BeepOnError = True
                TabOrder = 9
                OnEnter = cxDBTextEditAllEnter
                OnExit = cxDBTextEditAllExit
                OnKeyDown = EnterKakoTab
                Width = 366
              end
              object txtLabelOnBack: TcxDBTextEdit
                Left = 556
                Top = 41
                BeepOnEnter = False
                DataBinding.DataField = 'LABEL_ON_BACK'
                DataBinding.DataSource = dm.dsPorackaStavka
                Properties.BeepOnError = True
                TabOrder = 10
                OnEnter = cxDBTextEditAllEnter
                OnExit = cxDBTextEditAllExit
                OnKeyDown = EnterKakoTab
                Width = 366
              end
              object txtLabelOnArms: TcxDBTextEdit
                Left = 556
                Top = 66
                BeepOnEnter = False
                DataBinding.DataField = 'LABEL_ON_ARMS'
                DataBinding.DataSource = dm.dsPorackaStavka
                Properties.BeepOnError = True
                TabOrder = 11
                OnEnter = cxDBTextEditAllEnter
                OnExit = cxDBTextEditAllExit
                OnKeyDown = EnterKakoTab
                Width = 366
              end
              object txtEmboidery: TcxDBTextEdit
                Left = 556
                Top = 90
                BeepOnEnter = False
                DataBinding.DataField = 'EMBROIDERY'
                DataBinding.DataSource = dm.dsPorackaStavka
                Properties.BeepOnError = True
                TabOrder = 12
                OnEnter = cxDBTextEditAllEnter
                OnExit = cxDBTextEditAllExit
                OnKeyDown = EnterKakoTab
                Width = 366
              end
              object txtPiping: TcxDBTextEdit
                Left = 556
                Top = 115
                BeepOnEnter = False
                DataBinding.DataField = 'PIPING'
                DataBinding.DataSource = dm.dsPorackaStavka
                Properties.BeepOnError = True
                TabOrder = 13
                OnEnter = cxDBTextEditAllEnter
                OnExit = cxDBTextEditAllExit
                OnKeyDown = EnterKakoTab
                Width = 366
              end
              object txtStuds: TcxDBTextEdit
                Left = 556
                Top = 140
                BeepOnEnter = False
                DataBinding.DataField = 'PALETA_BR'
                DataBinding.DataSource = dm.dsPorackaStavka
                Properties.BeepOnError = True
                TabOrder = 14
                OnEnter = cxDBTextEditAllEnter
                OnExit = cxDBTextEditAllExit
                OnKeyDown = EnterKakoTab
                Width = 366
              end
              object txtMetalColor: TcxDBTextEdit
                Left = 91
                Top = 165
                BeepOnEnter = False
                DataBinding.DataField = 'METAL_COLOR'
                DataBinding.DataSource = dm.dsPorackaStavka
                Properties.BeepOnError = True
                TabOrder = 6
                OnEnter = cxDBTextEditAllEnter
                OnExit = cxDBTextEditAllExit
                OnKeyDown = EnterKakoTab
                Width = 366
              end
              object txtWoodType: TcxDBTextEdit
                Left = 91
                Top = 190
                BeepOnEnter = False
                DataBinding.DataField = 'WOOD_TYPE'
                DataBinding.DataSource = dm.dsPorackaStavka
                Properties.BeepOnError = True
                TabOrder = 7
                OnEnter = cxDBTextEditAllEnter
                OnExit = cxDBTextEditAllExit
                OnKeyDown = EnterKakoTab
                Width = 366
              end
              object txtSpecialPrice: TcxDBTextEdit
                Left = 556
                Top = 166
                BeepOnEnter = False
                DataBinding.DataField = 'SPECIAL_PRICE'
                DataBinding.DataSource = dm.dsPorackaStavka
                Properties.BeepOnError = True
                TabOrder = 15
                OnEnter = cxDBTextEditAllEnter
                OnExit = cxDBTextEditAllExit
                OnKeyDown = EnterKakoTab
                Width = 366
              end
              object txtSKU: TcxDBTextEdit
                Left = 556
                Top = 191
                BeepOnEnter = False
                DataBinding.DataField = 'SKU'
                DataBinding.DataSource = dm.dsPorackaStavka
                Properties.BeepOnError = True
                TabOrder = 16
                OnEnter = cxDBTextEditAllEnter
                OnExit = cxDBTextEditAllExit
                OnKeyDown = EnterKakoTab
                Width = 366
              end
            end
          end
        end
      end
    end
    object tsElement: TcxTabSheet
      Caption = '  '#1057#1091#1088#1086#1074#1080#1085#1080'  '#1085#1072' '#1055#1086#1088#1072#1095#1082#1072
      ImageIndex = 33
      object lPanelP: TPanel
        Left = 0
        Top = 0
        Width = 956
        Height = 251
        Align = alTop
        Enabled = False
        TabOrder = 0
        object gbElementi: TcxGroupBox
          Left = 1
          Top = 1
          Align = alClient
          TabOrder = 0
          Height = 249
          Width = 954
          object Label8: TLabel
            Left = 622
            Top = 103
            Width = 65
            Height = 16
            Alignment = taRightJustify
            Caption = #1047#1072#1073#1077#1083#1077#1096#1082#1072
            Font.Charset = RUSSIAN_CHARSET
            Font.Color = clBlue
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
          end
          object cxLabel7: TcxLabel
            Left = 8
            Top = 102
            AutoSize = False
            Caption = #1040#1088#1090#1080#1082#1072#1083' / '#1057#1091#1088#1086#1074#1080#1085#1072
            Style.TextColor = clRed
            Properties.Alignment.Horz = taRightJustify
            Properties.WordWrap = True
            Transparent = True
            Height = 36
            Width = 98
            AnchorX = 106
          end
          object txtVidElement: TcxDBTextEdit
            Tag = 1
            Left = 117
            Top = 107
            DataBinding.DataField = 'VID_SUROVINA'
            DataBinding.DataSource = dm.dsPorackaElement
            ParentFont = False
            Style.Font.Charset = DEFAULT_CHARSET
            Style.Font.Color = clWindowText
            Style.Font.Height = -13
            Style.Font.Name = 'Tahoma'
            Style.Font.Style = []
            Style.IsFontAssigned = True
            TabOrder = 2
            OnEnter = cxDBTextEditAllEnter
            OnExit = cxDBTextEditAllExit
            OnKeyDown = EnterKakoTab
            Width = 61
          end
          object txtElement: TcxDBTextEdit
            Tag = 1
            Left = 178
            Top = 107
            DataBinding.DataField = 'SUROVINA'
            DataBinding.DataSource = dm.dsPorackaElement
            ParentFont = False
            Style.Font.Charset = DEFAULT_CHARSET
            Style.Font.Color = clWindowText
            Style.Font.Height = -13
            Style.Font.Name = 'Tahoma'
            Style.Font.Style = []
            Style.IsFontAssigned = True
            TabOrder = 3
            OnEnter = cxDBTextEditAllEnter
            OnExit = cxDBTextEditAllExit
            OnKeyDown = EnterKakoTab
            Width = 63
          end
          object cxLabel11: TcxLabel
            Left = 304
            Top = 163
            AutoSize = False
            Caption = #1042#1082'.'#1059#1090#1088#1086#1096#1086#1082
            Style.TextColor = clBlue
            Properties.Alignment.Horz = taRightJustify
            Transparent = True
            Height = 20
            Width = 81
            AnchorX = 385
          end
          object txtKolicinaElement: TcxDBTextEdit
            Left = 393
            Top = 162
            DataBinding.DataField = 'KOLICINA'
            DataBinding.DataSource = dm.dsPorackaElement
            ParentFont = False
            Properties.ReadOnly = True
            Style.BorderStyle = ebsUltraFlat
            Style.Font.Charset = RUSSIAN_CHARSET
            Style.Font.Color = clWindowText
            Style.Font.Height = -13
            Style.Font.Name = 'Tahoma'
            Style.Font.Style = []
            Style.LookAndFeel.NativeStyle = False
            Style.LookAndFeel.SkinName = ''
            Style.TextStyle = []
            Style.TransparentBorder = False
            Style.IsFontAssigned = True
            StyleDisabled.LookAndFeel.NativeStyle = False
            StyleDisabled.LookAndFeel.SkinName = ''
            StyleFocused.LookAndFeel.NativeStyle = False
            StyleFocused.LookAndFeel.SkinName = ''
            StyleHot.LookAndFeel.NativeStyle = False
            StyleHot.LookAndFeel.SkinName = ''
            TabOrder = 22
            OnEnter = cxDBTextEditAllEnter
            OnExit = cxDBTextEditAllExit
            OnKeyDown = EnterKakoTab
            Width = 124
          end
          object cxButton4: TcxButton
            Left = 632
            Top = 210
            Width = 117
            Height = 25
            Action = aZapisi
            TabOrder = 9
          end
          object cxButton5: TcxButton
            Left = 768
            Top = 210
            Width = 112
            Height = 25
            Action = aOtkazi
            TabOrder = 10
          end
          object txtZabeleskaElement: TcxDBMemo
            Left = 622
            Top = 128
            DataBinding.DataField = 'ZABELESKA'
            DataBinding.DataSource = dm.dsPorackaElement
            TabOrder = 13
            Height = 66
            Width = 258
          end
          object cbMerkaArtikal: TcxDBLookupComboBox
            Left = 178
            Top = 135
            Hint = #1045#1076#1080#1085#1080#1094#1072' '#1084#1077#1088#1082#1072
            RepositoryItem = dm.erLookupComboBox
            AutoSize = False
            DataBinding.DataField = 'MERKA'
            DataBinding.DataSource = dm.dsPorackaElement
            Properties.KeyFieldNames = 'ID'
            Properties.ListColumns = <
              item
                FieldName = 'NAZIV'
              end>
            Properties.ListSource = dmMat.dsMerka
            TabOrder = 6
            OnEnter = cxDBTextEditAllEnter
            OnExit = cxDBTextEditAllExit
            OnKeyDown = EnterKakoTab
            Height = 24
            Width = 224
          end
          object txtMerkaArtikal: TcxDBTextEdit
            Left = 117
            Top = 136
            Hint = #1045#1076#1080#1085#1080#1094#1072' '#1084#1077#1088#1082#1072
            DataBinding.DataField = 'MERKA'
            DataBinding.DataSource = dm.dsPorackaElement
            ParentFont = False
            Style.BorderStyle = ebsUltraFlat
            Style.Font.Charset = RUSSIAN_CHARSET
            Style.Font.Color = clWindowText
            Style.Font.Height = -13
            Style.Font.Name = 'Tahoma'
            Style.Font.Style = []
            Style.LookAndFeel.NativeStyle = False
            Style.LookAndFeel.SkinName = ''
            Style.TextStyle = []
            Style.TransparentBorder = False
            Style.IsFontAssigned = True
            StyleDisabled.LookAndFeel.NativeStyle = False
            StyleDisabled.LookAndFeel.SkinName = ''
            StyleDisabled.TextColor = clWindowText
            StyleFocused.LookAndFeel.NativeStyle = False
            StyleFocused.LookAndFeel.SkinName = ''
            StyleHot.LookAndFeel.NativeStyle = False
            StyleHot.LookAndFeel.SkinName = ''
            TabOrder = 5
            OnEnter = cxDBTextEditAllEnter
            OnExit = cxDBTextEditAllExit
            OnKeyDown = EnterKakoTab
            Width = 61
          end
          object cxLabel13: TcxLabel
            Left = 41
            Top = 138
            AutoSize = False
            Caption = #1052#1077#1088#1082#1072
            Style.TextColor = clBlue
            Properties.Alignment.Horz = taRightJustify
            Transparent = True
            Height = 20
            Width = 65
            AnchorX = 106
          end
          object cxDBLabel2: TcxDBLabel
            Left = 521
            Top = 164
            AutoSize = True
            DataBinding.DataField = 'MERKA'
            DataBinding.DataSource = dm.dsPorackaElement
            Properties.Alignment.Horz = taLeftJustify
            Transparent = True
          end
          object cbElement: TcxExtLookupComboBox
            Left = 241
            Top = 107
            RepositoryItem = dmRes.cxNurkoEditRepositoryExtLookupComboBox
            Properties.ClearKey = 46
            Properties.DropDownAutoSize = True
            Properties.DropDownSizeable = True
            Properties.OnChange = cxExtLookupComboBox1PropertiesEditValueChanged
            TabOrder = 4
            OnEnter = cxDBTextEditAllEnter
            OnExit = cxDBTextEditAllExit
            OnKeyDown = FormKeyDown
            Width = 329
          end
          object cxLabel33: TcxLabel
            Left = 36
            Top = 46
            Caption = #1042#1080#1076' '#1085#1072' '#1090#1072#1087#1072#1094#1080#1088#1072#1114#1077
            Style.TextColor = clBlue
            Properties.Alignment.Horz = taRightJustify
            Properties.WordWrap = True
            Transparent = True
            Width = 77
            AnchorX = 113
          end
          object txtTapacir: TcxDBTextEdit
            Tag = 1
            Left = 119
            Top = 53
            DataBinding.DataField = 'TAPACIR'
            DataBinding.DataSource = dm.dsPorackaElement
            ParentFont = False
            Style.Font.Charset = DEFAULT_CHARSET
            Style.Font.Color = clWindowText
            Style.Font.Height = -13
            Style.Font.Name = 'Tahoma'
            Style.Font.Style = []
            Style.IsFontAssigned = True
            TabOrder = 0
            OnEnter = cxDBTextEditAllEnter
            OnExit = cxDBTextEditAllExit
            OnKeyDown = EnterKakoTab
            Width = 59
          end
          object cbTapacir: TcxDBLookupComboBox
            Left = 178
            Top = 53
            DataBinding.DataField = 'TAPACIR'
            DataBinding.DataSource = dm.dsPorackaElement
            ParentFont = False
            Properties.KeyFieldNames = 'ID'
            Properties.ListColumns = <
              item
                Caption = #1064#1080#1092#1088#1072
                FieldName = 'ID'
              end
              item
                Caption = #1053#1072#1079#1080#1074
                FieldName = 'NAZIV'
              end>
            Properties.ListFieldIndex = 1
            Properties.ListSource = dsArtikalTapacir
            Style.Font.Charset = RUSSIAN_CHARSET
            Style.Font.Color = clWindowText
            Style.Font.Height = -13
            Style.Font.Name = 'Tahoma'
            Style.Font.Style = []
            Style.IsFontAssigned = True
            TabOrder = 1
            OnEnter = cxDBTextEditAllEnter
            OnExit = cxDBTextEditAllExit
            OnKeyDown = EnterKakoTab
            Width = 273
          end
          object txtPaleta: TcxDBTextEdit
            Left = 117
            Top = 190
            DataBinding.DataField = 'PALETA'
            DataBinding.DataSource = dm.dsPorackaElement
            ParentFont = False
            Style.BorderStyle = ebsUltraFlat
            Style.Font.Charset = RUSSIAN_CHARSET
            Style.Font.Color = clWindowText
            Style.Font.Height = -13
            Style.Font.Name = 'Tahoma'
            Style.Font.Style = []
            Style.LookAndFeel.NativeStyle = False
            Style.LookAndFeel.SkinName = ''
            Style.TextStyle = []
            Style.TransparentBorder = False
            Style.IsFontAssigned = True
            StyleDisabled.LookAndFeel.NativeStyle = False
            StyleDisabled.LookAndFeel.SkinName = ''
            StyleFocused.LookAndFeel.NativeStyle = False
            StyleFocused.LookAndFeel.SkinName = ''
            StyleHot.LookAndFeel.NativeStyle = False
            StyleHot.LookAndFeel.SkinName = ''
            TabOrder = 8
            OnEnter = cxDBTextEditAllEnter
            OnExit = cxDBTextEditAllExit
            OnKeyDown = EnterKakoTab
            Width = 124
          end
          object cxLabel34: TcxLabel
            Left = 41
            Top = 192
            AutoSize = False
            Caption = #1055#1072#1083#1077#1090#1072
            Style.TextColor = clBlue
            Properties.Alignment.Horz = taRightJustify
            Transparent = True
            Height = 20
            Width = 65
            AnchorX = 106
          end
          object cxLabel38: TcxLabel
            Left = 2
            Top = 21
            Align = alTop
            AutoSize = False
            Caption = '        '#1040#1082#1086' '#1074#1085#1077#1089#1091#1074#1072#1090#1077' '#1084#1072#1090#1077#1088#1080#1112#1072#1083', '#1080#1079#1073#1077#1088#1077#1090#1077' '#1042#1080#1076' '#1085#1072' '#1090#1072#1087#1072#1094#1080#1088#1072#1114#1077
            ParentFont = False
            Style.Font.Charset = RUSSIAN_CHARSET
            Style.Font.Color = clWindowText
            Style.Font.Height = -13
            Style.Font.Name = 'Tahoma'
            Style.Font.Style = []
            Style.TextColor = clMaroon
            Style.TextStyle = []
            Style.IsFontAssigned = True
            Properties.Alignment.Horz = taLeftJustify
            Properties.Alignment.Vert = taVCenter
            Transparent = True
            Height = 20
            Width = 950
            AnchorY = 31
          end
          object cxLabel35: TcxLabel
            Left = 0
            Top = 83
            AutoSize = False
            Caption = 
              '----------------------------------------------------------------' +
              '----------------------------------------------------------------' +
              '----------------------------------------------------------------' +
              '-------------------------------------------'
            ParentFont = False
            Style.Font.Charset = RUSSIAN_CHARSET
            Style.Font.Color = clMaroon
            Style.Font.Height = -11
            Style.Font.Name = 'Tahoma'
            Style.Font.Style = []
            Style.TextStyle = [fsUnderline]
            Style.IsFontAssigned = True
            Transparent = True
            Height = 15
            Width = 939
          end
          object cxLabel17: TcxLabel
            Left = 8
            Top = 165
            AutoSize = False
            Caption = #1045#1076#1080#1085'.'#1059#1090#1088#1086#1096#1086#1082
            Style.TextColor = clBlue
            Properties.Alignment.Horz = taRightJustify
            Transparent = True
            Height = 20
            Width = 98
            AnchorX = 106
          end
          object txtEdKol: TcxDBTextEdit
            Left = 117
            Top = 163
            DataBinding.DataField = 'ED_KOL'
            DataBinding.DataSource = dm.dsPorackaElement
            ParentFont = False
            Style.BorderStyle = ebsUltraFlat
            Style.Font.Charset = RUSSIAN_CHARSET
            Style.Font.Color = clWindowText
            Style.Font.Height = -13
            Style.Font.Name = 'Tahoma'
            Style.Font.Style = []
            Style.LookAndFeel.NativeStyle = False
            Style.LookAndFeel.SkinName = ''
            Style.TextStyle = []
            Style.TransparentBorder = False
            Style.IsFontAssigned = True
            StyleDisabled.LookAndFeel.NativeStyle = False
            StyleDisabled.LookAndFeel.SkinName = ''
            StyleFocused.LookAndFeel.NativeStyle = False
            StyleFocused.LookAndFeel.SkinName = ''
            StyleHot.LookAndFeel.NativeStyle = False
            StyleHot.LookAndFeel.SkinName = ''
            TabOrder = 7
            OnEnter = cxDBTextEditAllEnter
            OnExit = cxDBTextEditAllExit
            OnKeyDown = EnterKakoTab
            Width = 124
          end
          object cxDBLabel1: TcxDBLabel
            Left = 245
            Top = 165
            AutoSize = True
            DataBinding.DataField = 'MERKA'
            DataBinding.DataSource = dm.dsPorackaElement
            Properties.Alignment.Horz = taLeftJustify
            Transparent = True
          end
        end
      end
      object dPanelP: TPanel
        Left = 0
        Top = 251
        Width = 956
        Height = 437
        Align = alClient
        TabOrder = 1
        object cxGrid4: TcxGrid
          Left = 1
          Top = 1
          Width = 954
          Height = 435
          Align = alClient
          TabOrder = 0
          LookAndFeel.NativeStyle = False
          object cxGridDBTableView1: TcxGridDBTableView
            Navigator.Buttons.CustomButtons = <>
            ScrollbarAnnotations.CustomAnnotations = <>
            OnFocusedRecordChanged = cxGridDBTableView1FocusedRecordChanged
            DataController.DataSource = dm.dsPorackaElement
            DataController.Summary.DefaultGroupSummaryItems = <>
            DataController.Summary.FooterSummaryItems = <>
            DataController.Summary.SummaryGroups = <>
            FilterRow.InfoText = #1050#1083#1080#1082#1085#1077#1090#1077' '#1090#1091#1082#1072' '#1079#1072' '#1076#1072' '#1076#1077#1092#1080#1085#1080#1088#1072#1090#1077' '#1092#1080#1083#1090#1077#1088
            FilterRow.Visible = True
            OptionsBehavior.IncSearch = True
            OptionsCustomize.ColumnsQuickCustomization = True
            OptionsData.Deleting = False
            OptionsData.Editing = False
            OptionsData.Inserting = False
            OptionsView.Footer = True
            object cxGridDBTableView1ID: TcxGridDBColumn
              Caption = #1064#1080#1092#1088#1072
              DataBinding.FieldName = 'ID'
              Visible = False
            end
            object cxGridDBTableView1ID_PORACKA_STAVKA: TcxGridDBColumn
              Caption = #1064#1080#1092#1088#1072' '#1085#1072' '#1089#1090#1072#1074#1082#1072
              DataBinding.FieldName = 'ID_PORACKA_STAVKA'
              Visible = False
            end
            object cxGridDBTableView1VID_SUROVINA: TcxGridDBColumn
              Caption = #1042#1080#1076' '#1085#1072' '#1089#1091#1088#1086#1074#1080#1085#1072
              DataBinding.FieldName = 'VID_SUROVINA'
            end
            object cxGridDBTableView1SUROVINA: TcxGridDBColumn
              Caption = #1064#1080#1092#1088#1072' '#1085#1072' '#1057#1091#1088#1086#1074#1080#1085#1072
              DataBinding.FieldName = 'SUROVINA'
              Width = 45
            end
            object cxGridDBTableView1NAZIV_ELEMENT: TcxGridDBColumn
              Caption = #1057#1091#1088#1086#1074#1080#1085#1072
              DataBinding.FieldName = 'NAZIV_ELEMENT'
              Width = 246
            end
            object cxGridDBTableView1MERKA: TcxGridDBColumn
              Caption = #1064#1080#1092#1088#1072' '#1085#1072' '#1084#1077#1088#1082#1072
              DataBinding.FieldName = 'MERKA'
              Visible = False
            end
            object cxGridDBTableView1NAZIV_MERKA: TcxGridDBColumn
              Caption = #1052#1077#1088#1082#1072
              DataBinding.FieldName = 'NAZIV_MERKA'
              Width = 180
            end
            object cxGridDBTableView1ED_KOL: TcxGridDBColumn
              Caption = #1045#1076'.'#1059#1090#1088#1086#1096#1086#1082
              DataBinding.FieldName = 'ED_KOL'
            end
            object cxGridDBTableView1KOLICINA: TcxGridDBColumn
              Caption = #1042#1082'.'#1059#1090#1088#1086#1096#1086#1082
              DataBinding.FieldName = 'KOLICINA'
            end
            object cxGridDBTableView1ZABELESKA: TcxGridDBColumn
              Caption = #1047#1072#1073#1077#1083#1077#1096#1082#1072
              DataBinding.FieldName = 'ZABELESKA'
              Width = 221
            end
            object cxGridDBTableView1VID_TAPACIR: TcxGridDBColumn
              DataBinding.FieldName = 'VID_TAPACIR'
              Visible = False
            end
            object cxGridDBTableView1TAPACIR: TcxGridDBColumn
              Caption = #1064#1080#1092#1088#1072' '#1085#1072' '#1090#1072#1087#1072#1094#1080#1088
              DataBinding.FieldName = 'TAPACIR'
              Width = 52
            end
            object cxGridDBTableView1NAZIV_TAPACIR: TcxGridDBColumn
              Caption = #1058#1072#1087#1072#1094#1080#1088
              DataBinding.FieldName = 'NAZIV_TAPACIR'
              Width = 100
            end
            object cxGridDBTableView1PALETA: TcxGridDBColumn
              Caption = #1055#1072#1083#1077#1090#1072
              DataBinding.FieldName = 'PALETA'
              Width = 50
            end
          end
          object cxGridLevel1: TcxGridLevel
            GridView = cxGridDBTableView1
          end
        end
      end
    end
    object tsTrailer: TcxTabSheet
      Caption = #1050#1072#1084#1080#1086#1085' (TRAILER)'
      ImageIndex = 50
      TabVisible = False
      object dPanelT: TPanel
        Left = 0
        Top = 177
        Width = 956
        Height = 511
        Align = alClient
        TabOrder = 0
        object cxGrid2: TcxGrid
          Left = 1
          Top = 1
          Width = 954
          Height = 509
          Align = alClient
          TabOrder = 0
          LookAndFeel.NativeStyle = False
          object cxGridDBTableView2: TcxGridDBTableView
            Navigator.Buttons.CustomButtons = <>
            ScrollbarAnnotations.CustomAnnotations = <>
            OnFocusedRecordChanged = cxGridDBTableView1FocusedRecordChanged
            DataController.DataSource = dm.dsPSTrailer
            DataController.Summary.DefaultGroupSummaryItems = <>
            DataController.Summary.FooterSummaryItems = <>
            DataController.Summary.SummaryGroups = <>
            FilterRow.InfoText = #1050#1083#1080#1082#1085#1077#1090#1077' '#1090#1091#1082#1072' '#1079#1072' '#1076#1072' '#1076#1077#1092#1080#1085#1080#1088#1072#1090#1077' '#1092#1080#1083#1090#1077#1088
            FilterRow.Visible = True
            OptionsBehavior.IncSearch = True
            OptionsCustomize.ColumnsQuickCustomization = True
            OptionsData.Deleting = False
            OptionsData.Editing = False
            OptionsData.Inserting = False
            OptionsView.Footer = True
            object cxGridDBTableView2ID: TcxGridDBColumn
              Caption = #1064#1080#1092#1088#1072
              DataBinding.FieldName = 'ID'
              Visible = False
            end
            object cxGridDBTableView2ID_PORACKA_STAVKA: TcxGridDBColumn
              Caption = #1064#1080#1092#1088#1072' '#1085#1072' '#1089#1090#1072#1074#1082#1072
              DataBinding.FieldName = 'ID_PORACKA_STAVKA'
              Visible = False
            end
            object cxGridDBTableView2VID_ARTIKAL: TcxGridDBColumn
              Caption = #1042#1080#1076' '#1085#1072' '#1087#1088#1086#1080#1079#1074#1086#1076
              DataBinding.FieldName = 'VID_ARTIKAL'
            end
            object cxGridDBTableView2ARTIKAL: TcxGridDBColumn
              Caption = #1064#1080#1092#1088#1072' '#1085#1072' '#1087#1088#1086#1080#1079#1074#1086#1076
              DataBinding.FieldName = 'ARTIKAL'
            end
            object cxGridDBTableView2NAZIV_ARTIKAL: TcxGridDBColumn
              Caption = #1055#1088#1086#1080#1079#1074#1086#1076
              DataBinding.FieldName = 'NAZIV_ARTIKAL'
              Width = 242
            end
            object cxGridDBTableView2ID_TRAILER: TcxGridDBColumn
              Caption = #1064#1080#1092#1088#1072' '#1082#1072#1084#1080#1086#1085
              DataBinding.FieldName = 'ID_TRAILER'
            end
            object cxGridDBTableView2NAZIV_TRAILER: TcxGridDBColumn
              Caption = #1050#1072#1084#1080#1086#1085
              DataBinding.FieldName = 'NAZIV_TRAILER'
              Width = 100
            end
            object cxGridDBTableView2DATUM_TRANSPORT: TcxGridDBColumn
              Caption = #1044#1072#1090#1091#1084' '#1085#1072' '#1090#1088#1072#1085#1089#1087#1086#1088#1090
              DataBinding.FieldName = 'DATUM_TRANSPORT'
              Width = 89
            end
            object cxGridDBTableView2VK_KOLICINA: TcxGridDBColumn
              Caption = #1042#1082'. '#1082#1086#1083#1080#1095#1080#1085#1072
              DataBinding.FieldName = 'VK_KOLICINA'
              Width = 72
            end
            object cxGridDBTableView2KOL_TRAILER: TcxGridDBColumn
              Caption = #1050#1086#1083#1080#1095#1080#1085#1072' '#1074#1086' '#1050#1072#1084#1080#1086#1085
              DataBinding.FieldName = 'KOL_TRAILER'
              Width = 103
            end
          end
          object cxGridLevel2: TcxGridLevel
            GridView = cxGridDBTableView2
          end
        end
      end
      object lPanelT: TPanel
        Left = 0
        Top = 0
        Width = 956
        Height = 177
        Align = alTop
        Enabled = False
        TabOrder = 1
        object gbTrailer: TcxGroupBox
          Left = 1
          Top = 1
          Align = alClient
          TabOrder = 0
          Height = 175
          Width = 954
          object cxlbl1: TcxLabel
            Left = 345
            Top = 105
            Caption = #1055#1088#1086#1080#1079#1074#1086#1076
            Style.TextColor = clRed
            Properties.Alignment.Horz = taRightJustify
            Properties.WordWrap = True
            Transparent = True
            Visible = False
            Width = 60
            AnchorX = 405
          end
          object txtVidArtikalT: TcxDBTextEdit
            Left = 403
            Top = 105
            DataBinding.DataField = 'VID_ARTIKAL'
            DataBinding.DataSource = dm.dsPSTrailer
            ParentFont = False
            TabOrder = 8
            Visible = False
            OnEnter = cxDBTextEditAllEnter
            OnExit = cxDBTextEditAllExit
            OnKeyDown = EnterKakoTab
            Width = 61
          end
          object txtArtikalT: TcxDBTextEdit
            Left = 464
            Top = 105
            DataBinding.DataField = 'ARTIKAL'
            DataBinding.DataSource = dm.dsPSTrailer
            ParentFont = False
            TabOrder = 7
            Visible = False
            OnEnter = cxDBTextEditAllEnter
            OnExit = cxDBTextEditAllExit
            OnKeyDown = EnterKakoTab
            Width = 63
          end
          object cxlbl2: TcxLabel
            Left = 44
            Top = 42
            Caption = #1042#1082#1091#1087#1085#1072' '#1050#1086#1083#1080#1095#1080#1085#1072
            Style.TextColor = clBlue
            Properties.Alignment.Horz = taRightJustify
            Transparent = True
            AnchorX = 148
          end
          object txtVkKolicina: TcxDBTextEdit
            Left = 153
            Top = 43
            Hint = #1042#1082#1091#1087#1085#1072' '#1050#1086#1083#1080#1095#1080#1085#1072' '#1086#1076' '#1055#1086#1088#1072#1095#1082#1072#1090#1072
            DataBinding.DataField = 'VK_KOLICINA'
            DataBinding.DataSource = dm.dsPSTrailer
            ParentFont = False
            Properties.ReadOnly = True
            Style.BorderStyle = ebsUltraFlat
            Style.Font.Charset = RUSSIAN_CHARSET
            Style.Font.Color = clWindowText
            Style.Font.Height = -11
            Style.Font.Name = 'Tahoma'
            Style.Font.Style = []
            Style.LookAndFeel.NativeStyle = False
            Style.LookAndFeel.SkinName = ''
            Style.TextStyle = []
            Style.TransparentBorder = False
            Style.IsFontAssigned = True
            StyleDisabled.LookAndFeel.NativeStyle = False
            StyleDisabled.LookAndFeel.SkinName = ''
            StyleFocused.LookAndFeel.NativeStyle = False
            StyleFocused.LookAndFeel.SkinName = ''
            StyleHot.LookAndFeel.NativeStyle = False
            StyleHot.LookAndFeel.SkinName = ''
            TabOrder = 6
            OnEnter = cxDBTextEditAllEnter
            OnExit = cxDBTextEditAllExit
            OnKeyDown = EnterKakoTab
            Width = 124
          end
          object btnZapisi: TcxButton
            Left = 648
            Top = 128
            Width = 75
            Height = 25
            Action = aZapisi
            TabOrder = 3
          end
          object btnOtkazi: TcxButton
            Left = 729
            Top = 128
            Width = 75
            Height = 25
            Action = aOtkazi
            TabOrder = 4
          end
          object cbKamion: TcxDBLookupComboBox
            Left = 212
            Top = 72
            Hint = #1050#1072#1084#1080#1086#1085' (TRAILER)'
            AutoSize = False
            DataBinding.DataField = 'ID_TRAILER'
            DataBinding.DataSource = dm.dsPSTrailer
            Properties.KeyFieldNames = 'ID'
            Properties.ListColumns = <
              item
                Caption = #1064#1080#1092#1088#1072
                FieldName = 'id'
              end
              item
                Caption = #1053#1072#1079#1080#1074
                FieldName = 'NAZIV'
              end>
            Properties.ListFieldIndex = 1
            Properties.ListSource = dm.dsGenPlanKam
            TabOrder = 1
            OnEnter = cxDBTextEditAllEnter
            OnExit = cxDBTextEditAllExit
            OnKeyDown = EnterKakoTab
            Height = 21
            Width = 312
          end
          object txtKamion: TcxDBTextEdit
            Tag = 1
            Left = 150
            Top = 73
            Hint = #1045#1076#1080#1085#1080#1094#1072' '#1084#1077#1088#1082#1072
            DataBinding.DataField = 'ID_TRAILER'
            DataBinding.DataSource = dm.dsPSTrailer
            ParentFont = False
            Style.BorderStyle = ebsUltraFlat
            Style.Font.Charset = RUSSIAN_CHARSET
            Style.Font.Color = clWindowText
            Style.Font.Height = -11
            Style.Font.Name = 'Tahoma'
            Style.Font.Style = []
            Style.LookAndFeel.NativeStyle = False
            Style.LookAndFeel.SkinName = ''
            Style.TextStyle = []
            Style.TransparentBorder = False
            Style.IsFontAssigned = True
            StyleDisabled.LookAndFeel.NativeStyle = False
            StyleDisabled.LookAndFeel.SkinName = ''
            StyleDisabled.TextColor = clWindowText
            StyleFocused.LookAndFeel.NativeStyle = False
            StyleFocused.LookAndFeel.SkinName = ''
            StyleHot.LookAndFeel.NativeStyle = False
            StyleHot.LookAndFeel.SkinName = ''
            TabOrder = 0
            OnEnter = cxDBTextEditAllEnter
            OnExit = cxDBTextEditAllExit
            OnKeyDown = EnterKakoTab
            Width = 61
          end
          object cxlbl3: TcxLabel
            Left = 38
            Top = 72
            Caption = #1050#1072#1084#1080#1086#1085' (TRAILER)'
            Style.TextColor = clRed
            Properties.Alignment.Horz = taRightJustify
            Transparent = True
            AnchorX = 148
          end
          object cbArtikalT: TcxExtLookupComboBox
            Left = 526
            Top = 105
            RepositoryItem = dmRes.cxNurkoEditRepositoryExtLookupComboBox
            Properties.ClearKey = 46
            Properties.DropDownAutoSize = True
            Properties.DropDownSizeable = True
            Properties.OnChange = cxExtLookupComboBox1PropertiesEditValueChanged
            TabOrder = 9
            Visible = False
            OnEnter = cxDBTextEditAllEnter
            OnExit = cxDBTextEditAllExit
            OnKeyDown = FormKeyDown
            Width = 303
          end
          object cxlbl4: TcxLabel
            Left = 23
            Top = 101
            Caption = #1050#1086#1083#1080#1095#1080#1085#1072' '#1074#1086' '#1050#1072#1084#1080#1086#1085
            Style.TextColor = clBlue
            Properties.Alignment.Horz = taRightJustify
            Transparent = True
            AnchorX = 148
          end
          object txtKolTrailer: TcxDBTextEdit
            Left = 153
            Top = 102
            DataBinding.DataField = 'KOL_TRAILER'
            DataBinding.DataSource = dm.dsPSTrailer
            ParentFont = False
            Style.BorderStyle = ebsUltraFlat
            Style.Font.Charset = RUSSIAN_CHARSET
            Style.Font.Color = clWindowText
            Style.Font.Height = -11
            Style.Font.Name = 'Tahoma'
            Style.Font.Style = []
            Style.LookAndFeel.NativeStyle = False
            Style.LookAndFeel.SkinName = ''
            Style.TextStyle = []
            Style.TransparentBorder = False
            Style.IsFontAssigned = True
            StyleDisabled.LookAndFeel.NativeStyle = False
            StyleDisabled.LookAndFeel.SkinName = ''
            StyleFocused.LookAndFeel.NativeStyle = False
            StyleFocused.LookAndFeel.SkinName = ''
            StyleHot.LookAndFeel.NativeStyle = False
            StyleHot.LookAndFeel.SkinName = ''
            TabOrder = 2
            OnEnter = cxDBTextEditAllEnter
            OnExit = cxDBTextEditAllExit
            OnKeyDown = EnterKakoTab
            Width = 124
          end
        end
      end
    end
  end
  object cxGridPopupMenu1: TcxGridPopupMenu
    Grid = cxGrid1
    PopupMenus = <>
    Left = 1049
    Top = 16
  end
  object PopupMenu1: TPopupMenu
    Left = 1017
    Top = 144
    object N1: TMenuItem
      Caption = #1057#1085#1080#1084#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      ShortCut = 49235
      OnClick = SaveToIniFileExecute
    end
    object Excel1: TMenuItem
      Caption = #1057#1085#1080#1084#1080' '#1075#1086' '#1074#1086' Excel'
      ShortCut = 49221
    end
  end
  object dxBarManager1: TdxBarManager
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Segoe UI'
    Font.Style = []
    Categories.Strings = (
      'Default')
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    ImageOptions.Images = dmRes.cxSmallImages
    ImageOptions.LargeImages = dmRes.cxLargeImages
    PopupMenuLinks = <>
    UseSystemFont = True
    Left = 953
    Top = 80
    PixelsPerInch = 96
    object dxBarManager1Bar1: TdxBar
      Caption = #1055#1054#1056#1040#1063#1050#1040
      CaptionButtons = <>
      DockedLeft = 135
      DockedTop = 0
      FloatLeft = 715
      FloatTop = 157
      FloatClientWidth = 51
      FloatClientHeight = 120
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton1'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton23'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton3'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxbrlrgbtn2'
        end
        item
          Visible = True
          ItemName = 'dxbrlrgbtn3'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarSubItem2'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar2: TdxBar
      Caption = #1040#1082#1094#1080#1080
      CaptionButtons = <>
      DockedLeft = 940
      DockedTop = 0
      FloatLeft = 789
      FloatTop = 120
      FloatClientWidth = 51
      FloatClientHeight = 120
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxbrlrgbtn1'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton5'
        end
        item
          Visible = True
          ItemName = 'dxBarSubItem1'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar3: TdxBar
      Caption = #1048#1079#1083#1077#1079
      CaptionButtons = <>
      DockedLeft = 986
      DockedTop = 0
      FloatLeft = 718
      FloatTop = 62
      FloatClientWidth = 51
      FloatClientHeight = 80
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton7'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton8'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar4: TdxBar
      Caption = #1055#1077#1095#1072#1090#1077#1114#1077
      CaptionButtons = <>
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 676
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton12'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton15'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton9'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarLargeButton13'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1BarIzgledGrid: TdxBar
      Caption = #1058#1072#1073#1077#1083#1072
      CaptionButtons = <>
      DockedLeft = 363
      DockedTop = 0
      FloatLeft = 808
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLBtnSnimiIzgled'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarLBtnBrisiIzgled'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar5: TdxBar
      Caption = #1057#1058#1040#1042#1050#1040
      CaptionButtons = <>
      DockedLeft = 502
      DockedTop = 0
      FloatLeft = 1034
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton28'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton18'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton19'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'blbElementi'
        end
        item
          Visible = True
          ItemName = 'btTipPakuvanje4'
        end
        item
          Visible = True
          ItemName = 'btTipPakuvanje5'
        end
        item
          Visible = True
          ItemName = 'dxBarButton15'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar6: TdxBar
      Caption = '                                          '
      CaptionButtons = <>
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 1066
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          UserDefine = [udWidth]
          UserWidth = 65
          Visible = True
          ItemName = 'cbGodina'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar7: TdxBar
      Caption = #1057#1059#1056#1054#1042#1048#1053#1040
      CaptionButtons = <>
      DockedLeft = 762
      DockedTop = 0
      FloatLeft = 1058
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarButton12'
        end
        item
          Visible = True
          ItemName = 'dxBarButton14'
        end
        item
          Visible = True
          ItemName = 'dxBarButton11'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar8: TdxBar
      Caption = #1050#1040#1052#1048#1054#1053
      CaptionButtons = <>
      DockedLeft = 851
      DockedTop = 0
      FloatLeft = 1034
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'btTipPakuvanje1'
        end
        item
          Visible = True
          ItemName = 'btTipPakuvanje2'
        end
        item
          Visible = True
          ItemName = 'btTipPakuvanje3'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarLargeButton2: TdxBarLargeButton
      Action = aAzurirajj
      Category = 0
    end
    object dxBarLargeButton3: TdxBarLargeButton
      Action = aBrisii
      Category = 0
    end
    object dxBarLargeButton4: TdxBarLargeButton
      Action = aSnimiIzgled
      Category = 0
      ScreenTip = tipSnimiIzgled
    end
    object dxBarLargeButton5: TdxBarLargeButton
      Action = aZacuvajExcel
      Category = 0
      ScreenTip = tipZacuvajExcel
    end
    object dxBarLargeButton6: TdxBarLargeButton
      Action = aPecatiTabela
      Caption = #1055#1077#1095#1072#1090#1080' '#1090#1072#1073#1077#1083#1072
      Category = 0
    end
    object dxBarLargeButton7: TdxBarLargeButton
      Action = aHelp
      Category = 0
    end
    object dxBarLargeButton8: TdxBarLargeButton
      Action = aIzlez
      Category = 0
    end
    object dxBarLargeButton1: TdxBarLargeButton
      Action = aNovv
      Category = 0
      Hint = #1053#1086#1074#1072' '#1055#1086#1088#1072#1095#1082#1072
    end
    object dxBarLargeButton9: TdxBarLargeButton
      Action = aSnimiPecatenje
      Category = 0
      ScreenTip = tipSnimiKonf
    end
    object dxBarLargeButton11: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object ribbonFilterReset: TdxBarLargeButton
      Caption = #1056#1077#1089#1077#1090#1080#1088#1072#1112' '#1092#1080#1083#1090#1077#1088
      Category = 0
      Hint = #1056#1077#1089#1077#1090#1080#1088#1072#1112' '#1092#1080#1083#1090#1077#1088
      Visible = ivAlways
    end
    object dxBarLargeButton12: TdxBarLargeButton
      Action = aPodesuvanjePecatenje
      Category = 0
      ScreenTip = tipDizajnReport
    end
    object dxBarLargeButton13: TdxBarLargeButton
      Action = aBrisiPodesuvanjePecatenje
      Category = 0
      ScreenTip = tipBrisiKonf
    end
    object dxBarLargeButton14: TdxBarLargeButton
      Caption = #1048#1079#1073#1088#1080#1096#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      Category = 0
      Visible = ivAlways
      LargeImageIndex = 40
    end
    object dxBarLargeButton15: TdxBarLargeButton
      Action = aPageSetup
      Category = 0
      ScreenTip = tipSetiranjeStrana
    end
    object dxBarLargeButton16: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object dxBarLargeButton17: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object cxBarEditItem1: TcxBarEditItem
      Caption = 'New Item'
      Category = 0
      Hint = 'New Item'
      Visible = ivAlways
      PropertiesClassName = 'TcxCheckBoxProperties'
    end
    object dxBarSubItem1: TdxBarSubItem
      Caption = #1055#1077#1095#1072#1090#1077#1114#1077
      Category = 0
      Visible = ivAlways
      ImageIndex = 30
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton6'
        end>
    end
    object dxBarLBtnSnimiIzgled: TdxBarLargeButton
      Action = aSnimiIzgled
      Category = 0
    end
    object dxBarLBtnBrisiIzgled: TdxBarLargeButton
      Action = aBrisiIzgled
      Category = 0
    end
    object dxBarLargeButton10: TdxBarLargeButton
      Action = aNova
      Category = 0
    end
    object dxBarLargeButton18: TdxBarLargeButton
      Action = aAzurirajS
      Caption = #1040#1078#1091#1088#1080#1088#1072#1112
      Category = 0
    end
    object dxBarLargeButton19: TdxBarLargeButton
      Action = aBrisiS
      Category = 0
    end
    object dxBarLargeButton20: TdxBarLargeButton
      Action = aOtvoreniRN
      Caption = #1054#1090#1074#1086#1088#1077#1085#1080
      Category = 0
      LargeImageIndex = 66
      SyncImageIndex = False
      ImageIndex = 62
    end
    object dxBarLargeButton21: TdxBarLargeButton
      Action = aSiteRN
      Caption = #1057#1080#1090#1077
      Category = 0
      SyncImageIndex = False
      ImageIndex = 33
    end
    object dxBarDateCombo1: TdxBarDateCombo
      Caption = #1043#1086#1076#1080#1085#1072
      Category = 0
      Hint = #1043#1086#1076#1080#1085#1072
      Visible = ivAlways
    end
    object cbGodina: TcxBarEditItem
      Caption = #1043#1086#1076#1080#1085#1072
      Category = 0
      Hint = #1043#1086#1076#1080#1085#1072
      Visible = ivAlways
      OnChange = cbGodinaChange
      PropertiesClassName = 'TcxComboBoxProperties'
      Properties.DropDownSizeable = True
    end
    object cxBarEditItem2: TcxBarEditItem
      Caption = #1053#1086#1074' '#1056#1053' '#1079#1072' : '
      Category = 0
      Hint = #1053#1086#1074' '#1056#1053' '#1079#1072' : '
      Visible = ivAlways
      PropertiesClassName = 'TcxComboBoxProperties'
      Properties.Items.Strings = (
        #1052#1040#1064#1048#1053#1057#1050#1054' '#1054#1044#1044#1045#1051#1045#1053#1048#1045)
    end
    object cbNovRE: TdxBarLookupCombo
      Caption = #1053#1086#1074' '#1056#1053' '#1079#1072' : '
      Category = 0
      Hint = #1053#1086#1074' '#1056#1053' '#1079#1072' : '
      Visible = ivAlways
      Glyph.SourceDPI = 96
      Glyph.Data = {
        424D360400000000000036000000280000001000000010000000010020000000
        000000000000C40E0000C40E00000000000000000000FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00000000FF000000FF0000
        00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
        00FF000000FF000000FF000000FF000000FFFF00FF00000000FFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFF00FF00000000FFFFFFFFFF8080
        80FF808080FFFFFFFFFF000000FFFFFFFFFF808080FF808080FF808080FF8080
        80FF808080FF808080FFFFFFFFFF000000FFFF00FF00000000FFFF0000FFFF00
        00FFFF0000FFFF0000FF000000FFFF0000FFFF0000FFFF0000FFFF0000FFFF00
        00FFFF0000FFFF0000FFFF0000FF000000FFFF00FF00000000FFFF0000FF8080
        80FF808080FFFF0000FF000000FFFF0000FF808080FF808080FF808080FF8080
        80FF808080FF808080FFFF0000FF000000FFFF00FF00000000FFFF0000FFFF00
        00FFFF0000FFFF0000FF000000FFFF0000FFFF0000FFFF0000FFFF0000FFFF00
        00FFFF0000FFFF0000FFFF0000FF000000FFFF00FF00000000FFFFFFFFFF8080
        80FF808080FFFFFFFFFF000000FFFFFFFFFF808080FF808080FF808080FF8080
        80FF808080FF808080FFFFFFFFFF000000FFFF00FF00000000FFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFF00FF00000000FFFFFFFFFF8080
        80FF808080FFFFFFFFFF000000FFFFFFFFFF808080FF808080FF808080FF8080
        80FF808080FF808080FFFFFFFFFF000000FFFF00FF00000000FFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFF00FF00000000FF000000FF0000
        00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
        00FF000000FF000000FF000000FF000000FFFF00FF00000000FFFFFFFFFFFFFF
        FFFFFFFFFFFFFF0000FFFF0000FFFF0000FFFF0000FFFFFFFFFFFFFFFFFFFFFF
        FFFF000000FFFF00FF00FF00FF00FF00FF00FF00FF00000000FFFFFFFFFF8080
        80FF808080FF808080FF808080FF808080FF808080FFFFFFFFFFFFFFFFFFFFFF
        FFFF000000FFFF00FF00FF00FF00FF00FF00FF00FF00000000FFFFFFFFFFFFFF
        FFFFFFFFFFFFFF0000FFFF0000FFFF0000FFFF0000FFFFFFFFFFFFFFFFFFFFFF
        FFFF000000FFFF00FF00FF00FF00FF00FF00FF00FF00000000FF000000FF0000
        00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
        00FF000000FFFF00FF00FF00FF00FF00FF00FF00FF00}
      KeyField = 'ID'
      ListField = 'ID;naziv'
      ListFieldIndex = 1
      ListSource = dm.dsRE
      RowCount = 7
      OnKeyValueChange = cbNovREKeyValueChange
    end
    object cxBarEditItem3: TcxBarEditItem
      Caption = 'New Item'
      Category = 0
      Hint = 'New Item'
      Visible = ivAlways
      OnExit = cxDBTextEditAllExit
      PropertiesClassName = 'TcxLookupComboBoxProperties'
      Properties.KeyFieldNames = 'ID'
      Properties.ListColumns = <
        item
          FieldName = 'ID'
        end
        item
          FieldName = 'naziv'
        end>
      Properties.ListFieldIndex = 1
      Properties.ListSource = dmMat.dsRE
    end
    object dxBarLargeButton22: TdxBarLargeButton
      Action = aZatvoriRN
      Caption = #1047#1072#1090#1074#1086#1088#1080
      Category = 0
      Visible = ivNever
    end
    object dxBarSubItem2: TdxBarSubItem
      Caption = #1057#1090#1072#1090#1091#1089#1080' '#1085#1072' '#1055#1086#1088#1072#1095#1082#1080
      Category = 0
      Visible = ivAlways
      LargeImageIndex = 66
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton20'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton21'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton22'
        end>
    end
    object dxBarLargeButton23: TdxBarLargeButton
      Action = aAzurirajj
      Category = 0
    end
    object dxBarButton1: TdxBarButton
      Action = aNovaS
      Category = 0
    end
    object dxBarButton2: TdxBarButton
      Action = aAzurirajEE
      Category = 0
    end
    object dxBarButton3: TdxBarButton
      Action = aBrisiEE
      Category = 0
    end
    object blbElementi: TdxBarLargeButton
      Action = aElementiPoracka
      Caption = #1045#1083#1077#1084#1077#1085#1090
      Category = 0
      Hint = #1045#1083#1077#1084#1077#1085#1090#1080' '#1085#1072' '#1087#1086#1088#1072#1095#1082#1072' '#1086#1076' '#1085#1086#1088#1084#1072#1090#1080#1074
    end
    object dxBarButton4: TdxBarButton
      Action = aNovaS
      Category = 0
    end
    object dxBarLargeButton24: TdxBarLargeButton
      Action = aAzurirajS
      Category = 0
    end
    object dxBarLargeButton25: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object dxBarButton5: TdxBarButton
      Action = aBrisiS
      Category = 0
    end
    object dxBarButton6: TdxBarButton
      Action = aNovaS
      Category = 0
    end
    object dxBarButton7: TdxBarButton
      Action = aAzurirajS
      Category = 0
    end
    object dxBarButton8: TdxBarButton
      Action = aAzurirajS
      Category = 0
    end
    object dxBarButton9: TdxBarButton
      Action = aAzurirajS
      Category = 0
    end
    object dxBarLargeButton26: TdxBarLargeButton
      Action = aAzurirajS
      Category = 0
    end
    object dxBarButton10: TdxBarButton
      Action = aAzurirajS
      Category = 0
    end
    object dxBarButton11: TdxBarButton
      Action = aBrisiEE
      Category = 0
    end
    object dxBarButton12: TdxBarButton
      Action = aNova
      Category = 0
    end
    object dxBarButton13: TdxBarButton
      Action = aAzurirajS
      Category = 0
    end
    object dxBarLargeButton27: TdxBarLargeButton
      Action = aAzurirajS
      Category = 0
    end
    object dxBarButton14: TdxBarButton
      Action = aAzurirajEE
      Category = 0
    end
    object dxBarLargeButton28: TdxBarLargeButton
      Action = aNovaS
      Category = 0
    end
    object dxbrlrgbtn1: TdxBarLargeButton
      Action = aSpustiSoberi
      Category = 0
    end
    object btTipPakuvanje1: TdxBarButton
      Action = aNovaK
      Category = 0
    end
    object btTipPakuvanje2: TdxBarButton
      Action = aAzurirajK
      Category = 0
    end
    object btTipPakuvanje3: TdxBarButton
      Action = aBrisiK
      Category = 0
    end
    object dxbrlrgbtn2: TdxBarLargeButton
      Action = aStorniraj
      Category = 0
    end
    object dxbrlrgbtn3: TdxBarLargeButton
      Action = aVratiStornoOtvorena
      Category = 0
    end
    object btTipPakuvanje4: TdxBarButton
      Action = aStornirajPS
      Category = 0
    end
    object btTipPakuvanje5: TdxBarButton
      Action = aVratiStornoS
      Category = 0
    end
    object dxBarButton15: TdxBarButton
      Action = aAnaliticki
      Category = 0
    end
  end
  object ActionList1: TActionList
    Images = dmRes.cxSmallImages
    Left = 953
    Top = 16
    object aNov: TAction
      Caption = #1053#1086#1074
      ImageIndex = 10
      ShortCut = 116
      OnExecute = aNovExecute
    end
    object aPodesuvanjePecatenje: TAction
      Caption = #1044#1080#1079#1072#1112#1085' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090
      ImageIndex = 38
      OnExecute = aPodesuvanjePecatenjeExecute
    end
    object aAzuriraj: TAction
      Caption = #1040#1078#1091#1088#1080#1088#1072#1112
      ImageIndex = 12
      ShortCut = 117
      OnExecute = aAzurirajExecute
    end
    object aBrisi: TAction
      Caption = #1041#1088#1080#1096#1080
      ImageIndex = 11
      ShortCut = 119
      OnExecute = aBrisiExecute
    end
    object aSnimiIzgled: TAction
      Caption = #1057#1085#1080#1084#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      ImageIndex = 9
      ShortCut = 24659
      OnExecute = aSnimiIzgledExecute
    end
    object aZacuvajExcel: TAction
      Caption = #1047#1072#1095#1091#1074#1072#1112' '#1074#1086' Excel'
      ImageIndex = 9
      ShortCut = 24645
      OnExecute = aZacuvajExcelExecute
    end
    object aPecatiTabela: TAction
      Caption = #1055#1077#1095#1072#1090#1080
      ImageIndex = 30
      OnExecute = aPecatiTabelaExecute
    end
    object aHelp: TAction
      Caption = #1055#1086#1084#1086#1096
      ImageIndex = 24
    end
    object aIzlez: TAction
      Caption = #1048#1079#1083#1077#1079
      ImageIndex = 25
      OnExecute = aIzlezExecute
    end
    object aZapisi: TAction
      Caption = #1047#1072#1087#1080#1096#1080
      ImageIndex = 7
      ShortCut = 120
      OnExecute = aZapisiExecute
    end
    object aOtkazi: TAction
      Caption = #1054#1090#1082#1072#1078#1080
      ImageIndex = 21
      ShortCut = 27
      OnExecute = aOtkaziExecute
    end
    object aRefresh: TAction
      Caption = 'aRefresh'
      ImageIndex = 18
      ShortCut = 118
      OnExecute = aRefreshExecute
    end
    object aSnimiPecatenje: TAction
      Caption = #1057#1085#1080#1084#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      ImageIndex = 39
      OnExecute = aSnimiPecatenjeExecute
    end
    object aBrisiPodesuvanjePecatenje: TAction
      Caption = #1041#1088#1080#1096#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      ImageIndex = 37
      OnExecute = aBrisiPodesuvanjePecatenjeExecute
    end
    object aPageSetup: TAction
      Caption = #1057#1077#1090#1080#1088#1072#1114#1077' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072
      ImageIndex = 38
      OnExecute = aPageSetupExecute
    end
    object aFormConfig: TAction
      Caption = 'aFormConfig'
      ShortCut = 24699
      OnExecute = aFormConfigExecute
    end
    object aBrisiIzgled: TAction
      Caption = #1041#1088#1080#1096#1080' '#1075#1086' '#1079#1072#1095#1091#1074#1072#1085#1080#1086#1090' '#1080#1079#1075#1083#1077#1076
      ImageIndex = 56
      OnExecute = aBrisiIzgledExecute
    end
    object aListaRN: TAction
      Caption = #1051#1080#1089#1090#1072' '#1085#1072' '#1056#1053
      ImageIndex = 8
      OnExecute = aListaRNExecute
    end
    object aNova: TAction
      Caption = #1053#1086#1074#1072
      ImageIndex = 10
      OnExecute = aNovaExecute
    end
    object aNovv: TAction
      Caption = #1053#1086#1074
      ImageIndex = 10
      OnExecute = aNovvExecute
    end
    object aAzurirajS: TAction
      Caption = #1040#1078#1091
      ImageIndex = 12
      OnExecute = aAzurirajSExecute
    end
    object aAzurirajj: TAction
      Caption = #1040#1078#1091#1088#1080#1088#1072#1112
      ImageIndex = 12
      OnExecute = aAzurirajjExecute
    end
    object aBrisiS: TAction
      Caption = #1041#1088#1080#1096#1080
      ImageIndex = 11
      OnExecute = aBrisiSExecute
    end
    object aBrisii: TAction
      Caption = #1041#1088#1080#1096#1080
      ImageIndex = 11
      OnExecute = aBrisiiExecute
    end
    object aPogledRN: TAction
      Caption = #1055#1086#1075#1083#1077#1076' '#1085#1072' '#1056#1053
      OnExecute = aPogledRNExecute
    end
    object aSiteRN: TAction
      Caption = #1057#1080#1090#1077' '#1056#1053
      ImageIndex = 1
      OnExecute = aSiteRNExecute
    end
    object aOtvoreniRN: TAction
      Caption = #1054#1090#1074#1086#1088#1077#1085#1080' '#1056#1053
      ImageIndex = 2
      OnExecute = aOtvoreniRNExecute
    end
    object aSmeniStatus: TAction
      Caption = 'aSmeniStatus'
      OnExecute = aSmeniStatusExecute
    end
    object aRNMasinsko: TAction
      Caption = #1056#1053' '#1048#1085#1090#1077#1088#1077#1085
    end
    object aZatvoriRN: TAction
      Caption = #1047#1072#1090#1074#1086#1088#1080' '#1056#1053
      ImageIndex = 6
      OnExecute = aZatvoriRNExecute
    end
    object aZatvoriRNButton: TAction
      Caption = #1047#1072#1090#1074#1086#1088#1080
    end
    object aNovaS: TAction
      Caption = #1053#1086#1074#1072
      ImageIndex = 10
      OnExecute = aNovaSExecute
    end
    object aElementiPoracka: TAction
      Caption = #1045#1083#1077#1084#1077#1085#1090#1080' '#1085#1072' '#1087#1086#1088#1072#1095#1082#1072#1090#1072' '#1086#1076' '#1085#1086#1088#1084#1072#1090#1080#1074
      ImageIndex = 33
      OnExecute = aElementiPorackaExecute
    end
    object aAzurirajEE: TAction
      Caption = #1040#1078#1091#1088#1080#1088#1072#1112
      ImageIndex = 12
      OnExecute = aAzurirajEExecute
    end
    object aBrisiEE: TAction
      Caption = #1041#1088#1080#1096#1080
      ImageIndex = 11
      OnExecute = aBrisiEExecute
    end
    object aPogledE: TAction
      Caption = 'aPogledE'
      OnExecute = aPogledEExecute
    end
    object aSpustiSoberi: TAction
      Caption = #1057#1087#1091#1096#1090#1080' '#1089#1086#1073#1088#1080' '#1058#1072#1073#1077#1083#1072
      ImageIndex = 65
      OnExecute = aSpustiSoberiExecute
    end
    object aNovaK: TAction
      Caption = #1053#1086#1074#1072
      ImageIndex = 10
      OnExecute = aNovaKExecute
    end
    object aAzurirajK: TAction
      Caption = #1040#1078#1091#1088#1080#1088#1072#1112
      ImageIndex = 12
      OnExecute = aAzurirajKExecute
    end
    object aBrisiK: TAction
      Caption = #1041#1088#1080#1096#1080
      ImageIndex = 11
      OnExecute = aBrisiKExecute
    end
    object aStorniraj: TAction
      Caption = #1057#1090#1086#1088#1085#1080#1088#1072#1112
      ImageIndex = 63
      OnExecute = aStornirajExecute
    end
    object aVratiStornoOtvorena: TAction
      Caption = #1042#1088#1072#1090#1080' '#1086#1076' '#1089#1090#1086#1088#1085#1086' '#1074#1086' '#1086#1090#1074#1086#1088#1077#1085#1072
      ImageIndex = 18
      OnExecute = aVratiStornoOtvorenaExecute
    end
    object aStornirajPS: TAction
      Caption = #1057#1090#1088#1086#1085#1080#1088#1072#1112
      ImageIndex = 63
      OnExecute = aStornirajPSExecute
    end
    object aVratiStornoS: TAction
      Caption = #1042#1088#1072#1090#1080' '#1089#1090#1086#1088#1085#1086
      ImageIndex = 18
      OnExecute = aVratiStornoSExecute
    end
    object aAnaliticki: TAction
      Caption = #1040#1085#1072#1083#1080#1090#1080#1095#1082#1080
      OnExecute = aAnalitickiExecute
    end
    object aTapacir: TAction
      Caption = 'aTapacir'
      OnExecute = aTapacirExecute
    end
  end
  object dxComponentPrinter1: TdxComponentPrinter
    CurrentLink = dxComponentPrinter1Link1
    Version = 0
    Left = 1017
    Top = 80
    PixelsPerInch = 96
    object dxComponentPrinter1Link1: TdxGridReportLink
      Component = cxGrid1
      DateFormat = 0
      PageNumberFormat = pnfNumeral
      PrinterPage.DMPaper = 9
      PrinterPage.Footer = 6350
      PrinterPage.GrayShading = True
      PrinterPage.Header = 6350
      PrinterPage.Margins.Bottom = 12700
      PrinterPage.Margins.Left = 12700
      PrinterPage.Margins.Right = 12700
      PrinterPage.Margins.Top = 12700
      PrinterPage.Orientation = poLandscape
      PrinterPage.PageFooter.RightTitle.Strings = (
        #1057#1090#1088'.[Page #] '#1086#1076' [Total Pages]')
      PrinterPage.PageSize.X = 210000
      PrinterPage.PageSize.Y = 297000
      PrinterPage._dxMeasurementUnits_ = 0
      PrinterPage._dxLastMU_ = 2
      ReportDocument.CreationDate = 40336.425359629630000000
      TimeFormat = 0
      OptionsFormatting.UseNativeStyles = True
      OptionsOnEveryPage.Caption = False
      OptionsOnEveryPage.FilterBar = False
      OptionsView.Caption = False
      OptionsView.FilterBar = False
      StyleRepository = dmRes.cxStyleRepositoryPrinting
      Styles.StyleSheet = dmRes.dxGridReportLinkStyleSheet
      PixelsPerInch = 96
      BuiltInReportLink = True
    end
  end
  object dxBarScreenTipRepository1: TdxBarScreenTipRepository
    Left = 985
    Top = 80
    PixelsPerInch = 96
    object tipSnimiIzgled: TdxBarScreenTip
      Header.Text = #1057#1085#1080#1084#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      Description.Text = #1047#1072#1095#1091#1074#1072#1112' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090' '#1085#1072' '#1092#1086#1088#1084#1072#1090#1072' '#1080' '#1085#1072' '#1090#1072#1073#1077#1083#1072#1090#1072
    end
    object tipZacuvajExcel: TdxBarScreenTip
      Header.Text = #1047#1072#1095#1091#1074#1072#1112' '#1074#1086' Excel'
      Description.Text = #1057#1085#1080#1084#1080' '#1075#1080' '#1087#1086#1076#1072#1090#1086#1094#1080#1090#1077' '#1086#1076' '#1090#1072#1073#1077#1083#1072#1090#1072' '#1074#1086' Excel '#1092#1086#1088#1084#1072#1090
    end
    object tipDizajnReport: TdxBarScreenTip
      Header.Text = #1044#1080#1079#1072#1112#1085' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090
      Description.Text = #1055#1086#1076#1077#1089#1091#1074#1072#1114#1072' '#1085#1072' '#1080#1079#1075#1083#1077#1076#1086#1090' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090' '#1085#1072' '#1090#1072#1073#1077#1083#1072#1090#1072
    end
    object tipSetiranjeStrana: TdxBarScreenTip
      Header.Text = #1057#1077#1090#1080#1088#1072#1114#1077' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072
      Description.Text = #1055#1086#1076#1077#1089#1091#1074#1072#1114#1077' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072' '#1085#1072' '#1082#1086#1112#1072' '#1116#1077' '#1089#1077' '#1087#1077#1095#1072#1090#1080' '#1090#1072#1073#1077#1083#1072#1090#1072
    end
    object tipSnimiKonf: TdxBarScreenTip
      Header.Text = #1057#1085#1080#1084#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      Description.Text = #1047#1072#1095#1091#1074#1072#1112' '#1075#1080' '#1087#1086#1076#1077#1089#1091#1074#1072#1114#1072#1090#1072' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090' '#1080' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072' '#1079#1072' '#1087#1077#1095#1072#1090#1077#1114#1077
    end
    object tipBrisiKonf: TdxBarScreenTip
      Header.Text = #1041#1088#1080#1096#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      Description.Text = #1048#1079#1073#1088#1080#1096#1080' '#1075#1080' '#1087#1086#1076#1077#1089#1091#1074#1072#1114#1072#1090#1072' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090' '#1080' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072' '#1079#1072' '#1087#1077#1095#1072#1090#1077#1114#1077
    end
  end
  object OpenPictureDialog1: TOpenPictureDialog
    Left = 1049
    Top = 80
  end
  object cxGridPopupMenu2: TcxGridPopupMenu
    PopupMenus = <>
    Left = 985
    Top = 16
  end
  object cxGridPopupMenu3: TcxGridPopupMenu
    Grid = cxGrid3
    PopupMenus = <>
    Left = 1017
    Top = 16
  end
  object cxPropertiesStore1: TcxPropertiesStore
    Components = <>
    StorageName = 'cxPropertiesStore1'
    Left = 1081
    Top = 16
  end
  object tblIzberiGenPK: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE MAN_TRAILER'
      'SET '
      '    NAZIV = :NAZIV,'
      '    DATUM = :DATUM,'
      '    TS_INS = :TS_INS,'
      '    TS_UPD = :TS_UPD,'
      '    USR_INS = :USR_INS,'
      '    USR_UPD = :USR_UPD,'
      '    MESEC = :MESEC,'
      '    GODINA = :GODINA,'
      '    TIP_PARTNER = :TIP_PARTNER,'
      '    PARTNER = :PARTNER'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    MAN_TRAILER'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO MAN_TRAILER('
      '    ID,'
      '    NAZIV,'
      '    DATUM,'
      '    TS_INS,'
      '    TS_UPD,'
      '    USR_INS,'
      '    USR_UPD,'
      '    MESEC,'
      '    GODINA,'
      '    TIP_PARTNER,'
      '    PARTNER'
      ')'
      'VALUES('
      '    :ID,'
      '    :NAZIV,'
      '    :DATUM,'
      '    :TS_INS,'
      '    :TS_UPD,'
      '    :USR_INS,'
      '    :USR_UPD,'
      '    :MESEC,'
      '    :GODINA,'
      '    :TIP_PARTNER,'
      '    :PARTNER'
      ')')
    RefreshSQL.Strings = (
      'SELECT'
      '    mt.id,'
      '    mt.naziv,'
      '    mt.datum,'
      '    mt.ts_ins,'
      '    mt.ts_upd,'
      '    mt.usr_ins,'
      '    mt.usr_upd,'
      '    mt.mesec,'
      '    mt.godina,'
      '    mt.tip_partner,'
      '    mt.partner,'
      '    mp.naziv naziv_partner'
      'FROM'
      '    MAN_TRAILER mt'
      
        '    left join mat_partner mp on mp.tip_partner = mt.tip_partner ' +
        'and mp.id = mt.partner'
      '--where mt.datum > current'
      ''
      ' WHERE '
      '        MT.ID = :OLD_ID'
      '    ')
    SelectSQL.Strings = (
      'SELECT'
      '    mt.id,'
      '    mt.naziv,'
      '    mt.datum,'
      '    mt.ts_ins,'
      '    mt.ts_upd,'
      '    mt.usr_ins,'
      '    mt.usr_upd,'
      '    mt.mesec,'
      '    mt.godina,'
      '    mt.tip_partner,'
      '    mt.partner,'
      '    mp.naziv naziv_partner'
      'FROM'
      '    MAN_TRAILER mt'
      
        '    left join mat_partner mp on mp.tip_partner = mt.tip_partner ' +
        'and mp.id = mt.partner'
      '--where mt.datum > current')
    AutoUpdateOptions.UpdateTableName = 'MAN_TRAILER'
    AutoUpdateOptions.KeyFields = 'ID'
    AutoUpdateOptions.GeneratorName = 'GEN_MAN_TRAILER_ID'
    AutoUpdateOptions.WhenGetGenID = wgBeforePost
    AutoUpdateOptions.AutoParamsToFields = True
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 985
    Top = 112
    object tblIzberiGenPKID: TFIBIntegerField
      FieldName = 'ID'
    end
    object fbstrngfldGenPlanKamNAZIV: TFIBStringField
      FieldName = 'NAZIV'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object fbdtfldGenPlanKamDATUM: TFIBDateField
      FieldName = 'DATUM'
    end
    object fbdtmfldGenPlanKamTS_INS: TFIBDateTimeField
      FieldName = 'TS_INS'
    end
    object fbdtmfldGenPlanKamTS_UPD: TFIBDateTimeField
      FieldName = 'TS_UPD'
    end
    object fbstrngfldGenPlanKamUSR_INS: TFIBStringField
      FieldName = 'USR_INS'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object fbstrngfldGenPlanKamUSR_UPD: TFIBStringField
      FieldName = 'USR_UPD'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblIzberiGenPKMESEC: TFIBIntegerField
      FieldName = 'MESEC'
    end
    object tblIzberiGenPKGODINA: TFIBIntegerField
      FieldName = 'GODINA'
    end
    object tblIzberiGenPKTIP_PARTNER: TFIBIntegerField
      FieldName = 'TIP_PARTNER'
    end
    object tblIzberiGenPKPARTNER: TFIBIntegerField
      FieldName = 'PARTNER'
    end
  end
  object dsIzberiGenPK: TDataSource
    DataSet = tblIzberiGenPK
    Left = 953
    Top = 48
  end
  object tblPorackaStavka: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE MAN_PORACKA_STAVKA'
      'SET '
      '    ID = :ID,'
      '    BROJ = :BROJ,'
      '    ID_PORACKA = :MAS_ID,'
      '    VID_ARTIKAL = :VID_ARTIKAL,'
      '    ARTIKAL = :ARTIKAL,'
      '    SLIKA = :SLIKA,'
      '    TAPACIR_SEDISTE = :TAPACIR_SEDISTE,'
      '    TAPACIR_NAZAD = :TAPACIR_NAZAD,'
      '    SUNGER = :SUNGER,'
      '    BOJA_NOGARKI = :BOJA_NOGARKI,'
      '    LABELS = :LABELS,'
      '    GLIDERS = :GLIDERS,'
      '    KOLICINA = :KOLICINA,'
      '    SERISKI_BROJ = :SERISKI_BROJ,'
      '    ZABELESKA = :ZABELESKA,'
      '    ISPORAKA_OD_DATUM = :ISPORAKA_OD_DATUM,'
      '    ISPORAKA_DO_DATUM = :ISPORAKA_DO_DATUM,'
      '    status = :status,'
      '    cena = :cena,'
      '    valuta = :valuta,'
      '    kurs = :kurs,'
      '    paleta_br1 = :paleta_br1,'
      '    paleta_br2 = :paleta_br2,'
      '    paleta_br3 = :paleta_br3,'
      '    veneer_type = :veneer_type,'
      '    veneer_color = :veneer_color,'
      '    metal_color = :metal_color,'
      '    wood_type = :wood_type,'
      '    label_on_seat = :label_on_seat,'
      '    label_on_back = :label_on_back,'
      '    embroidery = :embroidery,'
      '    piping = :piping,'
      '    studs = :studs,'
      '    special_price = :special_price,'
      '    label_on_arms = :label_on_arms'
      '    '
      '    '
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    MAN_PORACKA_STAVKA'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO MAN_PORACKA_STAVKA('
      '    ID,'
      '    BROJ,'
      '    ID_PORACKA,'
      '    VID_ARTIKAL,'
      '    ARTIKAL,'
      '    SLIKA,'
      '    TAPACIR_SEDISTE,'
      '    TAPACIR_NAZAD,'
      '    SUNGER,'
      '    BOJA_NOGARKI,'
      '    LABELS,'
      '    GLIDERS,'
      '    KOLICINA,'
      '    SERISKI_BROJ,'
      '    ZABELESKA,'
      '    ISPORAKA_OD_DATUM,'
      '    ISPORAKA_DO_DATUM,'
      '    status,'
      '    cena,'
      '    valuta,'
      '    kurs,'
      '    paleta_br1,'
      '    paleta_br2,'
      '    paleta_br3,'
      '    veneer_type,'
      '    veneer_color,'
      '    metal_color,'
      '    wood_type,'
      '    label_on_seat,'
      '    label_on_back,'
      '    embroidery,'
      '    piping,'
      '    studs,'
      '    special_price,'
      '    label_on_arms '
      ')'
      'VALUES('
      '    :ID,'
      '    :BROJ,'
      '    :MAS_ID,'
      '    :VID_ARTIKAL,'
      '    :ARTIKAL,'
      '    :SLIKA,'
      '    :TAPACIR_SEDISTE,'
      '    :TAPACIR_NAZAD,'
      '    :SUNGER,'
      '    :BOJA_NOGARKI,'
      '    :LABELS,'
      '    :GLIDERS,'
      '    :KOLICINA,'
      '    :SERISKI_BROJ,'
      '    :ZABELESKA,'
      '    :ISPORAKA_OD_DATUM,'
      '    :ISPORAKA_DO_DATUM,'
      '    :status,'
      '    :cena,'
      '    :valuta,'
      '    :kurs,'
      '    :paleta_br1,'
      '    :paleta_br2,'
      '    :paleta_br3,'
      '    :veneer_type,'
      '    :ps.veneer_color,'
      '    :ps.metal_color,'
      '    :ps.wood_type,'
      '    :label_on_seat,'
      '    :label_on_back,'
      '    :embroidery,'
      '    :piping,'
      '    :studs,'
      '    :special_price,'
      '    :label_on_arms  )')
    RefreshSQL.Strings = (
      'select'
      '     ps.id,'
      '     ps.broj,'
      '     ps.id_poracka,'
      '     ps.vid_artikal,'
      '     ps.artikal,'
      '     a.naziv naziv_artikal,'
      '     ps.slika, ps.tapacir_sediste,'
      '     ps.tapacir_nazad,'
      '     ps.sunger,'
      '     ps.boja_nogarki,'
      '     ps.labels,'
      '     ps.gliders,'
      '     ps.kolicina,'
      '     ps.seriski_broj,'
      '     ps.zabeleska,'
      '     ps.isporaka_od_datum,'
      '     ps.isporaka_do_datum,'
      '     ps.status,'
      '     ms.naziv naziv_status,'
      '     ms.boja,'
      '     ms.vrednost,'
      '     a.naziv2,'
      '     ps.cena,'
      '     ps.valuta,'
      '     ps.kurs,'
      '     ps.paleta_br1,'
      '     ps.paleta_br2,'
      '     ps.paleta_br3,'
      '     ps.veneer_type,'
      '     ps.veneer_color,'
      '     ps.metal_color,'
      '     ps.wood_type,'
      '     ps.label_on_seat,'
      '     ps.label_on_back,'
      '     ps.embroidery,'
      '     ps.piping,'
      '     ps.studs,'
      '     ps.special_price,'
      '     ps.label_on_arms   '
      'from man_poracka_stavka ps'
      
        '    inner join mtr_artikal a on a.artvid = ps.vid_artikal and a.' +
        'id = ps.artikal'
      '    left outer join mat_status ms on ms.vrednost = ps.status'
      
        '        and ms.grupa = (select sg.id from mat_status_grupa sg wh' +
        'ere (sg.app = '#39'MAN'#39' and sg.id = ms.grupa and sg.tabela = '#39'MAN_PO' +
        'RACKA_STAVKA'#39') )'
      '--where ps.id_poracka = :MAS_ID'
      ''
      ' WHERE '
      '        PS.ID = :OLD_ID'
      '    ')
    SelectSQL.Strings = (
      'select'
      '     ps.id,'
      '     ps.broj,'
      '     ps.id_poracka,'
      '     ps.vid_artikal,'
      '     ps.artikal,'
      '     a.naziv naziv_artikal,'
      '     ps.slika, ps.tapacir_sediste,'
      '     ps.tapacir_nazad,'
      '     ps.sunger,'
      '     ps.boja_nogarki,'
      '     ps.labels,'
      '     ps.gliders,'
      '     ps.kolicina,'
      '     ps.seriski_broj,'
      '     ps.zabeleska,'
      '     ps.isporaka_od_datum,'
      '     ps.isporaka_do_datum,'
      '     ps.status,'
      '     ms.naziv naziv_status,'
      '     ms.boja,'
      '     ms.vrednost,'
      '     a.naziv2,'
      '     ps.cena,'
      '     ps.valuta,'
      '     ps.kurs,'
      '     ps.paleta_br1,'
      '     ps.paleta_br2,'
      '     ps.paleta_br3,'
      '     ps.veneer_type,'
      '     ps.veneer_color,'
      '     ps.metal_color,'
      '     ps.wood_type,'
      '     ps.label_on_seat,'
      '     ps.label_on_back,'
      '     ps.embroidery,'
      '     ps.piping,'
      '     ps.studs,'
      '     ps.special_price,'
      '     ps.label_on_arms '
      'from man_poracka_stavka ps'
      
        '    inner join mtr_artikal a on a.artvid = ps.vid_artikal and a.' +
        'id = ps.artikal'
      '    left outer join mat_status ms on ms.vrednost = ps.status'
      
        '        and ms.grupa = (select sg.id from mat_status_grupa sg wh' +
        'ere (sg.app = '#39'MAN'#39' and sg.id = ms.grupa and sg.tabela = '#39'MAN_PO' +
        'RACKA_STAVKA'#39') )'
      '--where ps.id_poracka = :MAS_ID')
    AutoUpdateOptions.UpdateTableName = 'MAN_PORACKA_STAVKA'
    AutoUpdateOptions.KeyFields = 'ID'
    AutoUpdateOptions.GeneratorName = 'GEN_MAN_PORACKA_STAVKA_ID'
    AutoUpdateOptions.WhenGetGenID = wgBeforePost
    AutoUpdateOptions.AutoParamsToFields = True
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 1017
    Top = 112
    object tblPorackaStavkaID: TFIBBCDField
      FieldName = 'ID'
      Size = 0
    end
    object tblPorackaStavkaBROJ: TFIBStringField
      FieldName = 'BROJ'
      Size = 200
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPorackaStavkaID_PORACKA: TFIBBCDField
      FieldName = 'ID_PORACKA'
      Size = 0
    end
    object tblPorackaStavkaVID_ARTIKAL: TFIBIntegerField
      FieldName = 'VID_ARTIKAL'
    end
    object tblPorackaStavkaARTIKAL: TFIBIntegerField
      FieldName = 'ARTIKAL'
    end
    object tblPorackaStavkaNAZIV_ARTIKAL: TFIBStringField
      FieldName = 'NAZIV_ARTIKAL'
      Size = 500
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPorackaStavkaSLIKA: TFIBBlobField
      FieldName = 'SLIKA'
      Size = 8
    end
    object tblPorackaStavkaTAPACIR_SEDISTE: TFIBStringField
      FieldName = 'TAPACIR_SEDISTE'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPorackaStavkaTAPACIR_NAZAD: TFIBStringField
      FieldName = 'TAPACIR_NAZAD'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPorackaStavkaSUNGER: TFIBStringField
      FieldName = 'SUNGER'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPorackaStavkaBOJA_NOGARKI: TFIBStringField
      FieldName = 'BOJA_NOGARKI'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPorackaStavkaLABELS: TFIBStringField
      FieldName = 'LABELS'
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPorackaStavkaGLIDERS: TFIBStringField
      FieldName = 'GLIDERS'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPorackaStavkaKOLICINA: TFIBFloatField
      FieldName = 'KOLICINA'
    end
    object tblPorackaStavkaSERISKI_BROJ: TFIBStringField
      FieldName = 'SERISKI_BROJ'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPorackaStavkaZABELESKA: TFIBStringField
      FieldName = 'ZABELESKA'
      Size = 500
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPorackaStavkaISPORAKA_OD_DATUM: TFIBDateField
      FieldName = 'ISPORAKA_OD_DATUM'
    end
    object tblPorackaStavkaISPORAKA_DO_DATUM: TFIBDateField
      FieldName = 'ISPORAKA_DO_DATUM'
    end
    object tblPorackaStavkaSTATUS: TFIBIntegerField
      FieldName = 'STATUS'
    end
    object tblPorackaStavkaNAZIV_STATUS: TFIBStringField
      FieldName = 'NAZIV_STATUS'
      Size = 255
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPorackaStavkaBOJA: TFIBIntegerField
      FieldName = 'BOJA'
    end
    object tblPorackaStavkaVREDNOST: TFIBIntegerField
      FieldName = 'VREDNOST'
    end
    object tblPorackaStavkaNAZIV2: TFIBStringField
      FieldName = 'NAZIV2'
      Size = 500
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPorackaStavkaCENA: TFIBBCDField
      FieldName = 'CENA'
      Size = 2
    end
    object tblPorackaStavkaVALUTA: TFIBStringField
      FieldName = 'VALUTA'
      Size = 3
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPorackaStavkaKURS: TFIBFloatField
      FieldName = 'KURS'
    end
    object tblPorackaStavkaPALETA_BR1: TFIBStringField
      FieldName = 'PALETA_BR1'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPorackaStavkaPALETA_BR2: TFIBStringField
      FieldName = 'PALETA_BR2'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPorackaStavkaPALETA_BR3: TFIBStringField
      FieldName = 'PALETA_BR3'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPorackaStavkaVENEER_TYPE: TFIBStringField
      FieldName = 'VENEER_TYPE'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPorackaStavkaVENEER_COLOR: TFIBStringField
      FieldName = 'VENEER_COLOR'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPorackaStavkaMETAL_COLOR: TFIBStringField
      FieldName = 'METAL_COLOR'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPorackaStavkaWOOD_TYPE: TFIBStringField
      FieldName = 'WOOD_TYPE'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPorackaStavkaLABEL_ON_SEAT: TFIBStringField
      FieldName = 'LABEL_ON_SEAT'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPorackaStavkaLABEL_ON_BACK: TFIBStringField
      FieldName = 'LABEL_ON_BACK'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPorackaStavkaEMBROIDERY: TFIBStringField
      FieldName = 'EMBROIDERY'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPorackaStavkaPIPING: TFIBStringField
      FieldName = 'PIPING'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPorackaStavkaSTUDS: TFIBStringField
      FieldName = 'STUDS'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPorackaStavkaSPECIAL_PRICE: TFIBStringField
      FieldName = 'SPECIAL_PRICE'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPorackaStavkaLABEL_ON_ARMS: TFIBStringField
      FieldName = 'LABEL_ON_ARMS'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object dsPorackaStavka: TDataSource
    DataSet = tblPorackaStavka
    Left = 1049
    Top = 48
  end
  object qVratiStornoStavki: TpFIBQuery
    Transaction = dmKon.qTransaction
    Database = dmKon.fibBaza
    SQL.Strings = (
      'update man_poracka_stavka ps'
      'set ps.status = 1'
      'where ps.id_poracka = :id')
    Left = 1081
    Top = 112
    qoAutoCommit = True
    qoStartTransaction = True
  end
  object qVratiSS: TpFIBQuery
    Transaction = dmKon.qTransaction
    Database = dmKon.fibBaza
    SQL.Strings = (
      'update man_poracka_stavka ps'
      'set ps.status = 1'
      'where ps.id = :id')
    Left = 953
    Top = 152
    qoAutoCommit = True
    qoStartTransaction = True
  end
  object qInsertSt: TpFIBQuery
    Transaction = dmKon.qTransaction
    Database = dmKon.fibBaza
    SQL.Strings = (
      'INSERT INTO MAN_PORACKA_STAVKA('
      '   -- ID,'
      '    BROJ,'
      '    ID_PORACKA,'
      '    VID_ARTIKAL,'
      '    ARTIKAL,'
      '    SLIKA,'
      '    TAPACIR_SEDISTE,'
      '    TAPACIR_NAZAD,'
      '    SUNGER,'
      '    BOJA_NOGARKI,'
      '    LABELS,'
      '    GLIDERS,'
      '    KOLICINA,'
      '    SERISKI_BROJ,'
      '    ZABELESKA,'
      '    ISPORAKA_OD_DATUM,'
      '    ISPORAKA_DO_DATUM,'
      '    status,'
      '    cena,'
      '    valuta,'
      '    kurs,'
      '    customer_name'
      ')'
      'VALUES('
      '  --  :ID,'
      '    :BROJ,'
      '    :ID_PORACKA,'
      '    :VID_ARTIKAL,'
      '    :ARTIKAL,'
      '    :SLIKA,'
      '    :TAPACIR_SEDISTE,'
      '    :TAPACIR_NAZAD,'
      '    :SUNGER,'
      '    :BOJA_NOGARKI,'
      '    :LABELS,'
      '    :GLIDERS,'
      '    :KOLICINA,'
      '    :SERISKI_BROJ,'
      '    :ZABELESKA,'
      '    :ISPORAKA_OD_DATUM,'
      '    :ISPORAKA_DO_DATUM,'
      '    :status,'
      '    :cena,'
      '    :valuta,'
      '    :kurs,'
      '    :customer_name )'
      ''
      ''
      ''
      '')
    Left = 985
    Top = 144
    qoAutoCommit = True
    qoStartTransaction = True
  end
  object tblArtikalCS: TpFIBDataSet
    RefreshSQL.Strings = (
      
        'select N.ID, N.VID_ARTIKAL, N.ARTIKAL, A.NAZIV, N.KOLICINA, N.ME' +
        'RKA'
      'from MAN_NORMATIV N'
      'inner join MTR_ARTIKAL A on A.ARTVID = N.VID_ARTIKAL and'
      '      A.ID = N.ARTIKAL'
      'where(  N.KOREN = (select first 1 NN.ID'
      '                 from MAN_NORMATIV NN'
      '                 where NN.VID_ARTIKAL = :VID_ARTIKAL and'
      '                       NN.ARTIKAL = :ARTIKAL and'
      '                       NN.FLAG = 1 and'
      '                       NN.KOREN is null) and'
      '      N.VID_ARTIKAL = 11 and'
      '      N.ARTIKAL = 1'
      '     ) and (     N.ID = :OLD_ID'
      '     )'
      '       ')
    SelectSQL.Strings = (
      
        'select N.ID, N.VID_ARTIKAL, N.ARTIKAL, A.NAZIV, N.KOLICINA, N.ME' +
        'RKA'
      'from MAN_NORMATIV N'
      'inner join MTR_ARTIKAL A on A.ARTVID = N.VID_ARTIKAL and'
      '      A.ID = N.ARTIKAL'
      'where N.KOREN = (select first 1 NN.ID'
      '                 from MAN_NORMATIV NN'
      '                 where NN.VID_ARTIKAL = :VID_ARTIKAL and'
      '                       NN.ARTIKAL = :ARTIKAL and'
      '                       NN.FLAG = 1 and'
      '                       NN.KOREN is null) and'
      '      N.VID_ARTIKAL = 11 and'
      '      N.ARTIKAL = 1   ')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 953
    Top = 112
    poSQLINT64ToBCD = True
    object tblArtikalCSID: TFIBIntegerField
      FieldName = 'ID'
    end
    object tblArtikalCSVID_ARTIKAL: TFIBIntegerField
      FieldName = 'VID_ARTIKAL'
    end
    object tblArtikalCSARTIKAL: TFIBIntegerField
      FieldName = 'ARTIKAL'
    end
    object tblArtikalCSNAZIV: TFIBStringField
      FieldName = 'NAZIV'
      Size = 500
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblArtikalCSKOLICINA: TFIBFloatField
      FieldName = 'KOLICINA'
    end
    object tblArtikalCSMERKA: TFIBStringField
      FieldName = 'MERKA'
      Size = 5
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object dsArtikalCS: TDataSource
    AutoEdit = False
    DataSet = tblArtikalCS
    Left = 1081
    Top = 48
  end
  object tblArtikalSed: TpFIBDataSet
    RefreshSQL.Strings = (
      
        'select N.ID, N.VID_ARTIKAL, N.ARTIKAL, A.NAZIV, N.KOLICINA, N.ME' +
        'RKA'
      'from MAN_NORMATIV N'
      'inner join MTR_ARTIKAL A on A.ARTVID = N.VID_ARTIKAL and'
      '      A.ID = N.ARTIKAL'
      'where(  N.KOREN = (select first 1 NN.ID'
      '                 from MAN_NORMATIV NN'
      '                 where NN.VID_ARTIKAL = :VID_ARTIKAL and'
      '                       NN.ARTIKAL = :ARTIKAL and'
      '                       NN.FLAG = 1 and'
      '                       NN.KOREN is null) and'
      '      N.VID_ARTIKAL = 11 and'
      '      N.ARTIKAL = 2'
      '     ) and (     N.ID = :OLD_ID'
      '     )'
      '       ')
    SelectSQL.Strings = (
      
        'select N.ID, N.VID_ARTIKAL, N.ARTIKAL, A.NAZIV, N.KOLICINA, N.ME' +
        'RKA'
      'from MAN_NORMATIV N'
      'inner join MTR_ARTIKAL A on A.ARTVID = N.VID_ARTIKAL and'
      '      A.ID = N.ARTIKAL'
      'where N.KOREN = (select first 1 NN.ID'
      '                 from MAN_NORMATIV NN'
      '                 where NN.VID_ARTIKAL = :VID_ARTIKAL and'
      '                       NN.ARTIKAL = :ARTIKAL and'
      '                       NN.FLAG = 1 and'
      '                       NN.KOREN is null) and'
      '      N.VID_ARTIKAL = 11 and'
      '      N.ARTIKAL = 2   ')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 1081
    Top = 80
    poSQLINT64ToBCD = True
    object tblArtikalSedID: TFIBIntegerField
      FieldName = 'ID'
    end
    object tblArtikalSedVID_ARTIKAL: TFIBIntegerField
      FieldName = 'VID_ARTIKAL'
    end
    object tblArtikalSedARTIKAL: TFIBIntegerField
      FieldName = 'ARTIKAL'
    end
    object tblArtikalSedNAZIV: TFIBStringField
      FieldName = 'NAZIV'
      Size = 500
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblArtikalSedKOLICINA: TFIBFloatField
      FieldName = 'KOLICINA'
    end
    object tblArtikalSedMERKA: TFIBStringField
      FieldName = 'MERKA'
      Size = 5
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object dsArtikalSed: TDataSource
    AutoEdit = False
    DataSet = tblArtikalSed
    Left = 985
    Top = 48
  end
  object tblArtikalNas: TpFIBDataSet
    RefreshSQL.Strings = (
      
        'select N.ID, N.VID_ARTIKAL, N.ARTIKAL, A.NAZIV, N.KOLICINA, N.ME' +
        'RKA'
      'from MAN_NORMATIV N'
      'inner join MTR_ARTIKAL A on A.ARTVID = N.VID_ARTIKAL and'
      '      A.ID = N.ARTIKAL'
      'where(  N.KOREN = (select first 1 NN.ID'
      '                 from MAN_NORMATIV NN'
      '                 where NN.VID_ARTIKAL = :VID_ARTIKAL and'
      '                       NN.ARTIKAL = :ARTIKAL and'
      '                       NN.FLAG = 1 and'
      '                       NN.KOREN is null) and'
      '      N.VID_ARTIKAL = 11 and'
      '      N.ARTIKAL = 3'
      '     ) and (     N.ID = :OLD_ID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      
        'select N.ID, N.VID_ARTIKAL, N.ARTIKAL, A.NAZIV, N.KOLICINA, N.ME' +
        'RKA'
      'from MAN_NORMATIV N'
      'inner join MTR_ARTIKAL A on A.ARTVID = N.VID_ARTIKAL and'
      '      A.ID = N.ARTIKAL'
      'where N.KOREN = (select first 1 NN.ID'
      '                 from MAN_NORMATIV NN'
      '                 where NN.VID_ARTIKAL = :VID_ARTIKAL and'
      '                       NN.ARTIKAL = :ARTIKAL and'
      '                       NN.FLAG = 1 and'
      '                       NN.KOREN is null) and'
      '      N.VID_ARTIKAL = 11 and'
      '      N.ARTIKAL = 3')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 1049
    Top = 112
    poSQLINT64ToBCD = True
    object tblArtikalNasID: TFIBIntegerField
      FieldName = 'ID'
    end
    object tblArtikalNasVID_ARTIKAL: TFIBIntegerField
      FieldName = 'VID_ARTIKAL'
    end
    object tblArtikalNasARTIKAL: TFIBIntegerField
      FieldName = 'ARTIKAL'
    end
    object tblArtikalNasNAZIV: TFIBStringField
      FieldName = 'NAZIV'
      Size = 500
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblArtikalNasKOLICINA: TFIBFloatField
      FieldName = 'KOLICINA'
    end
    object tblArtikalNasMERKA: TFIBStringField
      FieldName = 'MERKA'
      Size = 5
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object dsArtikalNas: TDataSource
    AutoEdit = False
    DataSet = tblArtikalNas
    Left = 1017
    Top = 48
  end
  object tblArtikalTapacir: TpFIBDataSet
    RefreshSQL.Strings = (
      'select a.artvid, a.id, a.naziv'
      ' from mtr_artikal a'
      'where(  a.artvid = (select s.v1 from man_setup s'
      '                  where s.opis = '#39'tapacir'#39' )'
      '     ) and (     A.ARTVID = :OLD_ARTVID'
      '    and A.ID = :OLD_ID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      'select a.artvid, a.id, a.naziv'
      ' from mtr_artikal a'
      'where a.artvid = (select s.v1 from man_setup s'
      '                  where s.opis = '#39'tapacir'#39' )')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    Left = 984
    Top = 200
    object tblArtikalTapacirARTVID: TFIBIntegerField
      FieldName = 'ARTVID'
    end
    object tblArtikalTapacirID: TFIBIntegerField
      FieldName = 'ID'
    end
    object tblArtikalTapacirNAZIV: TFIBStringField
      FieldName = 'NAZIV'
      Size = 500
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object dsArtikalTapacir: TDataSource
    DataSet = tblArtikalTapacir
    Left = 928
    Top = 184
  end
  object tblNorTapacir: TpFIBDataSet
    RefreshSQL.Strings = (
      
        'select N.ID, N.VID_ARTIKAL, N.ARTIKAL, A.NAZIV, N.KOLICINA, N.ME' +
        'RKA'
      'from MAN_NORMATIV N'
      'inner join MTR_ARTIKAL A on A.ARTVID = N.VID_ARTIKAL and'
      '      A.ID = N.ARTIKAL'
      'where(  N.KOREN = (select first 1 NN.ID'
      '                 from MAN_NORMATIV NN'
      '                 where NN.VID_ARTIKAL = :VID_ARTIKAL and'
      '                       NN.ARTIKAL = :ARTIKAL and'
      '                       NN.FLAG = 1 and'
      '                       NN.KOREN is null) and'
      '      N.VID_ARTIKAL = (select s.v1 from man_setup s'
      '                  where s.opis = '#39'tapacir'#39' ) and'
      '      N.ARTIKAL = :art_tapacir'
      '     ) and (     N.ID = :OLD_ID'
      '     )'
      '       ')
    SelectSQL.Strings = (
      
        'select N.ID, N.VID_ARTIKAL, N.ARTIKAL, A.NAZIV, N.KOLICINA, N.ME' +
        'RKA'
      'from MAN_NORMATIV N'
      'inner join MTR_ARTIKAL A on A.ARTVID = N.VID_ARTIKAL and'
      '      A.ID = N.ARTIKAL'
      'where N.KOREN = (select first 1 NN.ID'
      '                 from MAN_NORMATIV NN'
      '                 where NN.VID_ARTIKAL = :VID_ARTIKAL and'
      '                       NN.ARTIKAL = :ARTIKAL and'
      '                       NN.FLAG = 1 and'
      '                       NN.KOREN is null) and'
      '      N.VID_ARTIKAL = (select s.v1 from man_setup s'
      '                  where s.opis = '#39'tapacir'#39' ) and'
      '      N.ARTIKAL = :art_tapacir   ')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 925
    Top = 280
    poSQLINT64ToBCD = True
    object tblNorTapacirID: TFIBIntegerField
      FieldName = 'ID'
    end
    object tblNorTapacirVID_ARTIKAL: TFIBIntegerField
      FieldName = 'VID_ARTIKAL'
    end
    object tblNorTapacirARTIKAL: TFIBIntegerField
      FieldName = 'ARTIKAL'
    end
    object tblNorTapacirNAZIV: TFIBStringField
      FieldName = 'NAZIV'
      Size = 500
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblNorTapacirKOLICINA: TFIBFloatField
      FieldName = 'KOLICINA'
    end
    object tblNorTapacirMERKA: TFIBStringField
      FieldName = 'MERKA'
      Size = 5
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object dsNorTapacir: TDataSource
    AutoEdit = False
    DataSet = tblNorTapacir
    Left = 921
    Top = 232
  end
end
