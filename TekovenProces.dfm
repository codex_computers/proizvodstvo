object frmTekovenProces: TfrmTekovenProces
  Left = 128
  Top = 212
  BorderIcons = [biSystemMenu, biMaximize]
  Caption = #1058#1077#1082#1086#1074#1077#1085' '#1087#1088#1086#1094#1077#1089' '#1074#1086' '#1055#1088#1086#1080#1079#1074#1086#1076#1089#1090#1074#1086#1090#1086
  ClientHeight = 710
  ClientWidth = 1362
  Color = clBtnFace
  Constraints.MinHeight = 500
  Constraints.MinWidth = 610
  Font.Charset = RUSSIAN_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  ShowHint = True
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object dxRibbon1: TdxRibbon
    Left = 0
    Top = 0
    Width = 1362
    Height = 126
    BarManager = dxBarManager1
    ColorSchemeName = 'Blue'
    Contexts = <>
    TabOrder = 0
    TabStop = False
    object dxRibbon1Tab1: TdxRibbonTab
      Active = True
      Caption = #1052#1077#1085#1080
      Groups = <
        item
          ToolbarName = 'dxBarManager1Bar3'
        end>
      Index = 0
    end
    object dxRibbon1Tab2: TdxRibbonTab
      Caption = #1055#1086#1076#1077#1089#1091#1074#1072#1114#1072
      Groups = <
        item
          ToolbarName = 'dxBarManager1Bar4'
        end
        item
          ToolbarName = 'dxBarManager1BarIzgledGrid'
        end>
      Index = 1
    end
  end
  object StatusBar1: TdxRibbonStatusBar
    Left = 0
    Top = 687
    Width = 1362
    Height = 23
    Panels = <
      item
        PanelStyleClassName = 'TdxStatusBarTextPanelStyle'
      end>
    Ribbon = dxRibbon1
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clDefault
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
  end
  object PanelMain: TPanel
    Left = 0
    Top = 126
    Width = 1362
    Height = 561
    Align = alClient
    Color = 15790320
    ParentBackground = False
    TabOrder = 6
    object GridPanel1: TGridPanel
      Left = 1
      Top = 153
      Width = 1360
      Height = 407
      Align = alClient
      ColumnCollection = <
        item
          Value = 20.004424455906520000
        end
        item
          Value = 19.998027160467130000
        end
        item
          Value = 19.997720169413850000
        end
        item
          Value = 19.999403817994860000
        end
        item
          Value = 20.000424396217650000
        end>
      ControlCollection = <
        item
          Column = -1
          Row = -1
        end
        item
          Column = 0
          Control = Panel11
          Row = 0
        end
        item
          Column = 1
          Control = Panel1
          Row = 0
        end
        item
          Column = 3
          Control = Panel4
          Row = 0
        end
        item
          Column = 4
          Control = Panel2
          Row = 0
        end
        item
          Column = 2
          Control = Panel3
          Row = 0
        end
        item
          Column = 0
          Control = dPanel1
          Row = 1
        end
        item
          Column = 2
          Control = dPanel2
          Row = 1
        end
        item
          Column = 1
          Control = dPanel3
          Row = 1
        end
        item
          Column = 3
          Control = dPanel4
          Row = 1
        end>
      DoubleBuffered = False
      ParentDoubleBuffered = False
      RowCollection = <
        item
          Value = 50.000000000000000000
        end
        item
          Value = 50.000000000000000000
        end>
      TabOrder = 0
      object Panel11: TPanel
        Left = 1
        Top = 1
        Width = 271
        Height = 202
        Align = alTop
        Anchors = [akLeft, akTop, akRight, akBottom]
        BevelKind = bkFlat
        BevelOuter = bvNone
        Color = 15925247
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentBackground = False
        ParentFont = False
        TabOrder = 0
        object btMasinsko: TcxButton
          Left = 0
          Top = 176
          Width = 267
          Height = 22
          Align = alBottom
          Action = aPoveke
          Caption = #1044#1077#1090#1072#1083#1085#1086' ...'
          TabOrder = 0
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          ExplicitLeft = -2
          ExplicitTop = 174
        end
        object cxLabel2: TcxLabel
          Left = 2
          Top = 41
          AutoSize = False
          Caption = #1042#1082#1091#1087#1077#1085' '#1073#1088#1086#1112' '#1085#1072' '#1087#1088#1086#1080#1079#1074#1086#1076#1080' '#1087#1086' '#1056#1053' :'
          ParentColor = False
          ParentFont = False
          Style.BorderStyle = ebsNone
          Style.Color = clBtnFace
          Style.Font.Charset = DEFAULT_CHARSET
          Style.Font.Color = clWindowText
          Style.Font.Height = -11
          Style.Font.Name = 'Tahoma'
          Style.Font.Style = []
          Style.HotTrack = False
          Style.LookAndFeel.NativeStyle = False
          Style.Shadow = False
          Style.TextStyle = [fsBold, fsUnderline]
          Style.IsFontAssigned = True
          StyleDisabled.LookAndFeel.NativeStyle = False
          StyleFocused.LookAndFeel.NativeStyle = False
          StyleHot.LookAndFeel.NativeStyle = False
          Properties.Alignment.Horz = taRightJustify
          Properties.Alignment.Vert = taVCenter
          Transparent = True
          Height = 25
          Width = 197
          AnchorX = 199
          AnchorY = 54
        end
        object cxLabel9: TcxLabel
          Left = 11
          Top = 64
          AutoSize = False
          Caption = '  - '#1055#1086#1084#1080#1085#1072#1090#1080' '#1085#1072' '#1089#1083#1077#1076#1085#1072' '#1086#1073#1088#1072#1073#1086#1090#1082#1072
          ParentColor = False
          Style.BorderStyle = ebsNone
          Style.Color = clBtnFace
          Style.HotTrack = False
          Style.LookAndFeel.NativeStyle = False
          Style.Shadow = False
          Style.TextStyle = []
          StyleDisabled.LookAndFeel.NativeStyle = False
          StyleFocused.LookAndFeel.NativeStyle = False
          StyleHot.LookAndFeel.NativeStyle = False
          Properties.Alignment.Horz = taRightJustify
          Properties.Alignment.Vert = taVCenter
          Transparent = True
          Height = 25
          Width = 188
          AnchorX = 199
          AnchorY = 77
        end
        object cxLabel10: TcxLabel
          Left = 11
          Top = 83
          AutoSize = False
          Caption = '  - '#1054#1089#1090#1072#1085#1072#1090#1080
          ParentColor = False
          Style.BorderStyle = ebsNone
          Style.Color = clBtnFace
          Style.HotTrack = False
          Style.LookAndFeel.NativeStyle = False
          Style.Shadow = False
          Style.TextStyle = []
          StyleDisabled.LookAndFeel.NativeStyle = False
          StyleFocused.LookAndFeel.NativeStyle = False
          StyleHot.LookAndFeel.NativeStyle = False
          Properties.Alignment.Horz = taRightJustify
          Properties.Alignment.Vert = taVCenter
          Transparent = True
          Height = 25
          Width = 188
          AnchorX = 199
          AnchorY = 96
        end
        object txtVkKolicina1: TcxDBTextEdit
          Left = 198
          Top = 45
          DataBinding.DataField = 'VK_KOLICINA1'
          DataBinding.DataSource = dm.dsTekovenProces
          Enabled = False
          StyleDisabled.BorderColor = clWindowFrame
          StyleDisabled.Color = clWindow
          StyleDisabled.TextColor = clWindowText
          StyleDisabled.TextStyle = []
          TabOrder = 4
          Width = 66
        end
        object txtZavrseni1: TcxDBTextEdit
          Left = 198
          Top = 67
          DataBinding.DataField = 'ZAVRSENI1'
          DataBinding.DataSource = dm.dsTekovenProces
          Enabled = False
          StyleDisabled.BorderColor = clWindowFrame
          StyleDisabled.Color = clWindow
          StyleDisabled.TextColor = clWindowText
          StyleDisabled.TextStyle = []
          TabOrder = 5
          Width = 66
        end
        object txtOstanati1: TcxDBTextEdit
          Left = 198
          Top = 88
          DataBinding.DataField = 'OSTANATI1'
          DataBinding.DataSource = dm.dsTekovenProces
          Enabled = False
          StyleDisabled.BorderColor = clWindowFrame
          StyleDisabled.Color = clWindow
          StyleDisabled.TextColor = clWindowText
          StyleDisabled.TextStyle = []
          TabOrder = 6
          Width = 66
        end
        object cxDBLabel1: TcxDBLabel
          Left = 0
          Top = 0
          Align = alTop
          DataBinding.DataField = 'KT_NAZIV1'
          DataBinding.DataSource = dm.dsTekovenProces
          ParentColor = False
          Properties.Alignment.Horz = taCenter
          Properties.Alignment.Vert = taVCenter
          Style.Color = 12910591
          Style.Shadow = True
          Style.TextStyle = [fsBold]
          Height = 25
          Width = 267
          AnchorX = 134
          AnchorY = 13
        end
      end
      object Panel1: TPanel
        Left = 272
        Top = 1
        Width = 271
        Height = 202
        Align = alTop
        Anchors = [akLeft, akTop, akRight, akBottom]
        BevelKind = bkFlat
        BevelOuter = bvNone
        Color = 15925247
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentBackground = False
        ParentFont = False
        TabOrder = 1
        DesignSize = (
          267
          198)
        object cxButton2: TcxButton
          Left = 40
          Top = 191
          Width = 75
          Height = 19
          Anchors = [akRight, akBottom]
          Caption = #1055#1086#1074#1077#1116#1077' ...'
          TabOrder = 0
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object cxLabel11: TcxLabel
          Left = 12
          Top = 41
          AutoSize = False
          Caption = #1042#1082#1091#1087#1077#1085' '#1073#1088#1086#1112' '#1079#1072' '#1086#1073#1088#1072#1073#1086#1090#1082#1072' :'
          ParentColor = False
          Style.BorderStyle = ebsNone
          Style.Color = clBtnFace
          Style.HotTrack = False
          Style.LookAndFeel.NativeStyle = False
          Style.Shadow = False
          Style.TextStyle = [fsBold, fsUnderline]
          StyleDisabled.LookAndFeel.NativeStyle = False
          StyleFocused.LookAndFeel.NativeStyle = False
          StyleHot.LookAndFeel.NativeStyle = False
          Properties.Alignment.Horz = taRightJustify
          Properties.Alignment.Vert = taVCenter
          Transparent = True
          Height = 25
          Width = 187
          AnchorX = 199
          AnchorY = 54
        end
        object cxLabel12: TcxLabel
          Left = 12
          Top = 85
          AutoSize = False
          Caption = '  - '#1055#1086#1084#1080#1085#1072#1090#1080' '#1085#1072' '#1089#1083#1077#1076#1085#1072' '#1086#1073#1088#1072#1073#1086#1090#1082#1072
          ParentColor = False
          Style.BorderStyle = ebsNone
          Style.Color = clBtnFace
          Style.HotTrack = False
          Style.LookAndFeel.NativeStyle = False
          Style.Shadow = False
          Style.TextStyle = []
          StyleDisabled.LookAndFeel.NativeStyle = False
          StyleFocused.LookAndFeel.NativeStyle = False
          StyleHot.LookAndFeel.NativeStyle = False
          Properties.Alignment.Horz = taRightJustify
          Properties.Alignment.Vert = taVCenter
          Transparent = True
          Height = 25
          Width = 187
          AnchorX = 199
          AnchorY = 98
        end
        object cxLabel13: TcxLabel
          Left = 12
          Top = 106
          AutoSize = False
          Caption = '  - '#1054#1089#1090#1072#1085#1072#1090#1080
          ParentColor = False
          Style.BorderStyle = ebsNone
          Style.Color = clBtnFace
          Style.HotTrack = False
          Style.LookAndFeel.NativeStyle = False
          Style.Shadow = False
          Style.TextStyle = []
          StyleDisabled.LookAndFeel.NativeStyle = False
          StyleFocused.LookAndFeel.NativeStyle = False
          StyleHot.LookAndFeel.NativeStyle = False
          Properties.Alignment.Horz = taRightJustify
          Properties.Alignment.Vert = taVCenter
          Transparent = True
          Height = 25
          Width = 187
          AnchorX = 199
          AnchorY = 119
        end
        object btFarbara: TcxButton
          Left = 0
          Top = 176
          Width = 267
          Height = 22
          Align = alBottom
          Action = aPoveke
          Caption = #1044#1077#1090#1072#1083#1085#1086' ...'
          TabOrder = 4
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object txtVkKolicina2: TcxDBTextEdit
          Left = 198
          Top = 45
          DataBinding.DataField = 'VK_KOLICINA2'
          DataBinding.DataSource = dm.dsTekovenProces
          Enabled = False
          StyleDisabled.BorderColor = clWindowFrame
          StyleDisabled.Color = clWindow
          StyleDisabled.TextColor = clWindowText
          TabOrder = 5
          Width = 66
        end
        object txtZavrseni2: TcxDBTextEdit
          Left = 198
          Top = 88
          DataBinding.DataField = 'ZAVRSENI2'
          DataBinding.DataSource = dm.dsTekovenProces
          Enabled = False
          StyleDisabled.BorderColor = clWindowFrame
          StyleDisabled.Color = clWindow
          StyleDisabled.TextColor = clWindowText
          TabOrder = 6
          Width = 66
        end
        object txtOstanati2: TcxDBTextEdit
          Left = 198
          Top = 109
          DataBinding.DataField = 'OSTANATI2'
          DataBinding.DataSource = dm.dsTekovenProces
          Enabled = False
          StyleDisabled.BorderColor = clWindowFrame
          StyleDisabled.Color = clWindow
          StyleDisabled.TextColor = clWindowText
          TabOrder = 7
          Width = 66
        end
        object cxDBLabel2: TcxDBLabel
          Left = 0
          Top = 0
          Align = alTop
          DataBinding.DataField = 'KT_NAZIV2'
          DataBinding.DataSource = dm.dsTekovenProces
          ParentColor = False
          Properties.Alignment.Horz = taCenter
          Properties.Alignment.Vert = taVCenter
          Style.Color = 11064319
          Style.Shadow = True
          Style.TextStyle = [fsBold]
          Height = 25
          Width = 267
          AnchorX = 134
          AnchorY = 13
        end
        object cxLabel1: TcxLabel
          Left = 12
          Top = 64
          AutoSize = False
          Caption = '  - '#1055#1088#1080#1084#1077#1085#1080' '#1086#1076' '#1087#1088#1077#1090#1093#1086#1076#1085#1072' '#1050#1058
          ParentColor = False
          Style.BorderStyle = ebsNone
          Style.Color = clBtnFace
          Style.HotTrack = False
          Style.LookAndFeel.NativeStyle = False
          Style.Shadow = False
          Style.TextStyle = []
          StyleDisabled.LookAndFeel.NativeStyle = False
          StyleFocused.LookAndFeel.NativeStyle = False
          StyleHot.LookAndFeel.NativeStyle = False
          Properties.Alignment.Horz = taRightJustify
          Properties.Alignment.Vert = taVCenter
          Transparent = True
          Height = 25
          Width = 187
          AnchorX = 199
          AnchorY = 77
        end
        object txtPrimeni2: TcxDBTextEdit
          Left = 198
          Top = 67
          DataBinding.DataField = 'PRIMENI2'
          DataBinding.DataSource = dm.dsTekovenProces
          Enabled = False
          StyleDisabled.BorderColor = clWindowFrame
          StyleDisabled.Color = clWindow
          StyleDisabled.TextColor = clWindowText
          TabOrder = 10
          Width = 66
        end
      end
      object Panel4: TPanel
        Left = 814
        Top = 1
        Width = 271
        Height = 201
        Align = alTop
        Anchors = [akLeft, akTop, akRight, akBottom]
        BevelKind = bkFlat
        BevelOuter = bvNone
        Color = 15925247
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentBackground = False
        ParentFont = False
        TabOrder = 2
        DesignSize = (
          267
          197)
        object cxButton9: TcxButton
          Left = 40
          Top = 549
          Width = 75
          Height = 19
          Anchors = [akRight, akBottom]
          Caption = #1055#1086#1074#1077#1116#1077' ...'
          TabOrder = 0
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object cxLabel20: TcxLabel
          Left = 9
          Top = 41
          AutoSize = False
          Caption = #1042#1082#1091#1087#1077#1085' '#1073#1088#1086#1112' '#1079#1072' '#1086#1073#1088#1072#1073#1086#1090#1082#1072':'
          ParentColor = False
          Style.BorderStyle = ebsNone
          Style.Color = clBtnFace
          Style.HotTrack = False
          Style.LookAndFeel.NativeStyle = False
          Style.Shadow = False
          Style.TextStyle = [fsBold, fsUnderline]
          StyleDisabled.LookAndFeel.NativeStyle = False
          StyleFocused.LookAndFeel.NativeStyle = False
          StyleHot.LookAndFeel.NativeStyle = False
          Properties.Alignment.Horz = taRightJustify
          Properties.Alignment.Vert = taVCenter
          Transparent = True
          Height = 25
          Width = 188
          AnchorX = 197
          AnchorY = 54
        end
        object cxLabel21: TcxLabel
          Left = 9
          Top = 85
          AutoSize = False
          Caption = '  - '#1047#1072' '#1090#1088#1072#1085#1089#1087#1086#1088#1090
          ParentColor = False
          Style.BorderStyle = ebsNone
          Style.Color = clBtnFace
          Style.HotTrack = False
          Style.LookAndFeel.NativeStyle = False
          Style.Shadow = False
          Style.TextStyle = []
          StyleDisabled.LookAndFeel.NativeStyle = False
          StyleFocused.LookAndFeel.NativeStyle = False
          StyleHot.LookAndFeel.NativeStyle = False
          Properties.Alignment.Horz = taRightJustify
          Properties.Alignment.Vert = taVCenter
          Transparent = True
          Height = 25
          Width = 188
          AnchorX = 197
          AnchorY = 98
        end
        object cxLabel22: TcxLabel
          Left = 9
          Top = 104
          AutoSize = False
          Caption = '  - '#1054#1089#1090#1072#1085#1072#1090#1080
          ParentColor = False
          Style.BorderStyle = ebsNone
          Style.Color = clBtnFace
          Style.HotTrack = False
          Style.LookAndFeel.NativeStyle = False
          Style.Shadow = False
          Style.TextStyle = []
          StyleDisabled.LookAndFeel.NativeStyle = False
          StyleFocused.LookAndFeel.NativeStyle = False
          StyleHot.LookAndFeel.NativeStyle = False
          Properties.Alignment.Horz = taRightJustify
          Properties.Alignment.Vert = taVCenter
          Transparent = True
          Height = 25
          Width = 188
          AnchorX = 197
          AnchorY = 117
        end
        object cxButton10: TcxButton
          Left = 172
          Top = 522
          Width = 75
          Height = 19
          Anchors = [akRight, akBottom]
          Caption = #1055#1086#1074#1077#1116#1077' ...'
          TabOrder = 4
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object cxButton15: TcxButton
          Left = 172
          Top = 286
          Width = 75
          Height = 19
          Anchors = [akRight, akBottom]
          Caption = #1055#1086#1074#1077#1116#1077' ...'
          TabOrder = 5
          Visible = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object txtVkKolicina5: TcxDBTextEdit
          Left = 197
          Top = 45
          DataBinding.DataField = 'VK_KOLICINA5'
          DataBinding.DataSource = dm.dsTekovenProces
          Enabled = False
          StyleDisabled.BorderColor = clWindowFrame
          StyleDisabled.Color = clWindow
          StyleDisabled.TextColor = clWindowText
          TabOrder = 6
          Width = 66
        end
        object txtZavrrseni5: TcxDBTextEdit
          Left = 197
          Top = 88
          DataBinding.DataField = 'ZAVRSENI5'
          DataBinding.DataSource = dm.dsTekovenProces
          Enabled = False
          StyleDisabled.BorderColor = clWindowFrame
          StyleDisabled.Color = clWindow
          StyleDisabled.TextColor = clWindowText
          TabOrder = 7
          Width = 66
        end
        object txtOstanati5: TcxDBTextEdit
          Left = 197
          Top = 109
          DataBinding.DataField = 'OSTANATI5'
          DataBinding.DataSource = dm.dsTekovenProces
          Enabled = False
          StyleDisabled.BorderColor = clWindowFrame
          StyleDisabled.Color = clWindow
          StyleDisabled.TextColor = clWindowText
          TabOrder = 8
          Width = 66
        end
        object cxDBLabel5: TcxDBLabel
          Left = 0
          Top = 0
          Align = alTop
          DataBinding.DataField = 'KT_NAZIV5'
          DataBinding.DataSource = dm.dsTekovenProces
          ParentColor = False
          Properties.Alignment.Horz = taCenter
          Properties.Alignment.Vert = taVCenter
          Style.Color = 16770995
          Style.Shadow = True
          Style.TextStyle = [fsBold]
          Height = 25
          Width = 267
          AnchorX = 134
          AnchorY = 13
        end
        object cxLabel5: TcxLabel
          Left = 9
          Top = 64
          AutoSize = False
          Caption = '  - '#1055#1088#1080#1084#1077#1085#1080' '#1086#1076' '#1087#1088#1077#1090#1093#1086#1076#1085#1072' '#1050#1058
          ParentColor = False
          Style.BorderStyle = ebsNone
          Style.Color = clBtnFace
          Style.HotTrack = False
          Style.LookAndFeel.NativeStyle = False
          Style.Shadow = False
          Style.TextStyle = []
          StyleDisabled.LookAndFeel.NativeStyle = False
          StyleFocused.LookAndFeel.NativeStyle = False
          StyleHot.LookAndFeel.NativeStyle = False
          Properties.Alignment.Horz = taRightJustify
          Properties.Alignment.Vert = taVCenter
          Transparent = True
          Height = 25
          Width = 188
          AnchorX = 197
          AnchorY = 77
        end
        object txtPrimeni5: TcxDBTextEdit
          Left = 197
          Top = 67
          DataBinding.DataField = 'PRIMENI5'
          DataBinding.DataSource = dm.dsTekovenProces
          Enabled = False
          StyleDisabled.BorderColor = clWindowFrame
          StyleDisabled.Color = clWindow
          StyleDisabled.TextColor = clWindowText
          TabOrder = 11
          Width = 66
        end
        object btMagGP: TcxButton
          Left = 0
          Top = 175
          Width = 267
          Height = 22
          Align = alBottom
          Action = aPoveke
          Caption = #1044#1077#1090#1072#1083#1085#1086' ...'
          TabOrder = 12
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
      end
      object Panel2: TPanel
        Left = 1085
        Top = 1
        Width = 274
        Height = 201
        Align = alTop
        Anchors = [akLeft, akTop, akRight, akBottom]
        BevelKind = bkFlat
        BevelOuter = bvNone
        Color = 15925247
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentBackground = False
        ParentFont = False
        TabOrder = 3
        DesignSize = (
          270
          197)
        object cxButton5: TcxButton
          Left = 43
          Top = 430
          Width = 75
          Height = 19
          Anchors = [akRight, akBottom]
          Caption = #1055#1086#1074#1077#1116#1077' ...'
          TabOrder = 0
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object cxLabel14: TcxLabel
          Left = 6
          Top = 42
          AutoSize = False
          Caption = #1042#1082#1091#1087#1077#1085' '#1073#1088#1086#1112' '#1079#1072' '#1086#1073#1088#1072#1073#1086#1090#1082#1072': '
          ParentColor = False
          Style.BorderStyle = ebsNone
          Style.Color = clBtnFace
          Style.HotTrack = False
          Style.LookAndFeel.NativeStyle = False
          Style.Shadow = False
          Style.TextStyle = [fsBold, fsUnderline]
          StyleDisabled.LookAndFeel.NativeStyle = False
          StyleFocused.LookAndFeel.NativeStyle = False
          StyleHot.LookAndFeel.NativeStyle = False
          Properties.Alignment.Horz = taRightJustify
          Properties.Alignment.Vert = taVCenter
          Transparent = True
          Height = 25
          Width = 195
          AnchorX = 201
          AnchorY = 55
        end
        object cxLabel15: TcxLabel
          Left = 6
          Top = 64
          AutoSize = False
          Caption = '  - '#1055#1086#1084#1080#1085#1072#1090#1080' '#1085#1072' '#1089#1083#1077#1076#1085#1072' '#1086#1073#1088#1072#1073#1086#1090#1082#1072
          ParentColor = False
          Style.BorderStyle = ebsNone
          Style.Color = clBtnFace
          Style.HotTrack = False
          Style.LookAndFeel.NativeStyle = False
          Style.Shadow = False
          Style.TextStyle = []
          StyleDisabled.LookAndFeel.NativeStyle = False
          StyleFocused.LookAndFeel.NativeStyle = False
          StyleHot.LookAndFeel.NativeStyle = False
          Properties.Alignment.Horz = taRightJustify
          Properties.Alignment.Vert = taVCenter
          Transparent = True
          Height = 25
          Width = 195
          AnchorX = 201
          AnchorY = 77
        end
        object cxLabel16: TcxLabel
          Left = 6
          Top = 83
          AutoSize = False
          Caption = '  - '#1054#1089#1090#1072#1085#1072#1090#1080
          ParentColor = False
          Style.BorderStyle = ebsNone
          Style.Color = clBtnFace
          Style.HotTrack = False
          Style.LookAndFeel.NativeStyle = False
          Style.Shadow = False
          Style.TextStyle = []
          StyleDisabled.LookAndFeel.NativeStyle = False
          StyleFocused.LookAndFeel.NativeStyle = False
          StyleHot.LookAndFeel.NativeStyle = False
          Properties.Alignment.Horz = taRightJustify
          Properties.Alignment.Vert = taVCenter
          Transparent = True
          Height = 25
          Width = 195
          AnchorX = 201
          AnchorY = 96
        end
        object cxButton6: TcxButton
          Left = 175
          Top = 403
          Width = 75
          Height = 19
          Anchors = [akRight, akBottom]
          Caption = #1055#1086#1074#1077#1116#1077' ...'
          TabOrder = 4
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object cxButton13: TcxButton
          Left = 183
          Top = 291
          Width = 75
          Height = 19
          Anchors = [akRight, akBottom]
          Caption = #1055#1086#1074#1077#1116#1077' ...'
          TabOrder = 5
          Visible = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object txtVkKolicina3: TcxDBTextEdit
          Left = 201
          Top = 45
          DataBinding.DataField = 'VK_KOLICINA3'
          DataBinding.DataSource = dm.dsTekovenProces
          Enabled = False
          StyleDisabled.BorderColor = clWindowFrame
          StyleDisabled.Color = clWindow
          StyleDisabled.TextColor = clWindowText
          TabOrder = 6
          Width = 66
        end
        object txtZavrseni3: TcxDBTextEdit
          Left = 201
          Top = 66
          DataBinding.DataField = 'ZAVRSENI3'
          DataBinding.DataSource = dm.dsTekovenProces
          Enabled = False
          StyleDisabled.BorderColor = clWindowFrame
          StyleDisabled.Color = clWindow
          StyleDisabled.TextColor = clWindowText
          TabOrder = 7
          Width = 66
        end
        object txtOstanati3: TcxDBTextEdit
          Left = 201
          Top = 87
          DataBinding.DataField = 'OSTANATI3'
          DataBinding.DataSource = dm.dsTekovenProces
          Enabled = False
          StyleDisabled.BorderColor = clWindowFrame
          StyleDisabled.Color = clWindow
          StyleDisabled.TextColor = clWindowText
          TabOrder = 8
          Width = 66
        end
        object cxDBLabel3: TcxDBLabel
          Left = 0
          Top = 0
          Align = alTop
          DataBinding.DataField = 'KT_NAZIV3'
          DataBinding.DataSource = dm.dsTekovenProces
          ParentColor = False
          Properties.Alignment.Horz = taCenter
          Properties.Alignment.Vert = taVCenter
          Style.Color = 14796031
          Style.Shadow = True
          Style.TextStyle = [fsBold]
          Height = 25
          Width = 270
          AnchorX = 135
          AnchorY = 13
        end
        object cxLabel3: TcxLabel
          Left = 6
          Top = 124
          AutoSize = False
          Caption = '  - '#1055#1088#1080#1084#1077#1085#1080' '#1086#1076' '#1087#1088#1077#1090#1093#1086#1076#1085#1072' '#1050#1058
          ParentColor = False
          Style.BorderStyle = ebsNone
          Style.Color = clBtnFace
          Style.HotTrack = False
          Style.LookAndFeel.NativeStyle = False
          Style.Shadow = False
          Style.TextStyle = []
          StyleDisabled.LookAndFeel.NativeStyle = False
          StyleFocused.LookAndFeel.NativeStyle = False
          StyleHot.LookAndFeel.NativeStyle = False
          Properties.Alignment.Horz = taRightJustify
          Properties.Alignment.Vert = taVCenter
          Transparent = True
          Visible = False
          Height = 25
          Width = 195
          AnchorX = 201
          AnchorY = 137
        end
        object txtPrimeni31: TcxDBTextEdit
          Left = 201
          Top = 126
          DataBinding.DataField = 'PRIMENI3'
          DataBinding.DataSource = dm.dsTekovenProces
          TabOrder = 11
          Visible = False
          Width = 66
        end
        object btSivara: TcxButton
          Left = 0
          Top = 175
          Width = 270
          Height = 22
          Align = alBottom
          Action = aPoveke
          Caption = #1044#1077#1090#1072#1083#1085#1086' ...'
          TabOrder = 12
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
      end
      object Panel3: TPanel
        Left = 543
        Top = 1
        Width = 271
        Height = 202
        Align = alTop
        Anchors = [akLeft, akTop, akRight, akBottom]
        BevelKind = bkFlat
        BevelOuter = bvNone
        Color = 15925247
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentBackground = False
        ParentFont = False
        TabOrder = 4
        DesignSize = (
          267
          198)
        object cxButton7: TcxButton
          Left = 40
          Top = 1099
          Width = 75
          Height = 19
          Anchors = [akRight, akBottom]
          Caption = #1055#1086#1074#1077#1116#1077' ...'
          TabOrder = 0
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object cxLabel17: TcxLabel
          Left = 9
          Top = 41
          AutoSize = False
          Caption = #1042#1082#1091#1087#1077#1085' '#1073#1088#1086#1112' '#1079#1072' '#1086#1073#1088#1072#1073#1086#1090#1082#1072':'
          ParentColor = False
          Style.BorderStyle = ebsNone
          Style.Color = clBtnFace
          Style.HotTrack = False
          Style.LookAndFeel.NativeStyle = False
          Style.Shadow = False
          Style.TextStyle = [fsBold, fsUnderline]
          StyleDisabled.LookAndFeel.NativeStyle = False
          StyleFocused.LookAndFeel.NativeStyle = False
          StyleHot.LookAndFeel.NativeStyle = False
          Properties.Alignment.Horz = taRightJustify
          Properties.Alignment.Vert = taVCenter
          Transparent = True
          Height = 25
          Width = 190
          AnchorX = 199
          AnchorY = 54
        end
        object cxLabel18: TcxLabel
          Left = 9
          Top = 106
          AutoSize = False
          Caption = '  - '#1055#1086#1084#1080#1085#1072#1090#1080' '#1085#1072' '#1089#1083#1077#1076#1085#1072' '#1086#1073#1088#1072#1073#1086#1090#1082#1072
          ParentColor = False
          Style.BorderStyle = ebsNone
          Style.Color = clBtnFace
          Style.HotTrack = False
          Style.LookAndFeel.NativeStyle = False
          Style.Shadow = False
          Style.TextStyle = []
          StyleDisabled.LookAndFeel.NativeStyle = False
          StyleFocused.LookAndFeel.NativeStyle = False
          StyleHot.LookAndFeel.NativeStyle = False
          Properties.Alignment.Horz = taRightJustify
          Properties.Alignment.Vert = taVCenter
          Transparent = True
          Height = 25
          Width = 190
          AnchorX = 199
          AnchorY = 119
        end
        object cxLabel19: TcxLabel
          Left = 14
          Top = 126
          AutoSize = False
          Caption = '  - '#1054#1089#1090#1072#1085#1072#1090#1080
          ParentColor = False
          Style.BorderStyle = ebsNone
          Style.Color = clBtnFace
          Style.HotTrack = False
          Style.LookAndFeel.NativeStyle = False
          Style.Shadow = False
          Style.TextStyle = []
          StyleDisabled.LookAndFeel.NativeStyle = False
          StyleFocused.LookAndFeel.NativeStyle = False
          StyleHot.LookAndFeel.NativeStyle = False
          Properties.Alignment.Horz = taRightJustify
          Properties.Alignment.Vert = taVCenter
          Transparent = True
          Height = 25
          Width = 190
          AnchorX = 204
          AnchorY = 139
        end
        object cxButton8: TcxButton
          Left = 172
          Top = 1072
          Width = 75
          Height = 19
          Anchors = [akRight, akBottom]
          Caption = #1055#1086#1074#1077#1116#1077' ...'
          TabOrder = 4
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object cxButton14: TcxButton
          Left = 172
          Top = 860
          Width = 75
          Height = 19
          Anchors = [akRight, akBottom]
          Caption = #1055#1086#1074#1077#1116#1077' ...'
          TabOrder = 5
          Visible = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object txtVkKolicina4: TcxDBTextEdit
          Left = 198
          Top = 45
          DataBinding.DataField = 'VK_KOLICINA4'
          DataBinding.DataSource = dm.dsTekovenProces
          Enabled = False
          StyleDisabled.BorderColor = clWindowFrame
          StyleDisabled.Color = clWindow
          StyleDisabled.TextColor = clWindowText
          TabOrder = 6
          Width = 66
        end
        object txtZavrseni4: TcxDBTextEdit
          Left = 198
          Top = 109
          DataBinding.DataField = 'ZAVRSENI4'
          DataBinding.DataSource = dm.dsTekovenProces
          Enabled = False
          StyleDisabled.BorderColor = clWindowFrame
          StyleDisabled.Color = clWindow
          StyleDisabled.TextColor = clWindowText
          TabOrder = 7
          Width = 66
        end
        object txtOstanati4: TcxDBTextEdit
          Left = 198
          Top = 129
          DataBinding.DataField = 'OSTANATI4'
          DataBinding.DataSource = dm.dsTekovenProces
          Enabled = False
          StyleDisabled.BorderColor = clWindowFrame
          StyleDisabled.Color = clWindow
          StyleDisabled.TextColor = clWindowText
          TabOrder = 8
          Width = 66
        end
        object cxDBLabel4: TcxDBLabel
          Left = 0
          Top = 0
          Align = alTop
          DataBinding.DataField = 'KT_NAZIV4'
          DataBinding.DataSource = dm.dsTekovenProces
          ParentColor = False
          Properties.Alignment.Horz = taCenter
          Properties.Alignment.Vert = taVCenter
          Style.Color = 13828036
          Style.Shadow = True
          Style.TextStyle = [fsBold]
          Height = 25
          Width = 267
          AnchorX = 134
          AnchorY = 13
        end
        object cxLabel4: TcxLabel
          Left = 13
          Top = 64
          AutoSize = False
          Caption = '  - '#1055#1088#1080#1084#1077#1085#1080' '#1086#1076' '#1064#1048#1042#1040#1056#1040
          ParentColor = False
          Style.BorderStyle = ebsNone
          Style.Color = clBtnFace
          Style.HotTrack = False
          Style.LookAndFeel.NativeStyle = False
          Style.Shadow = False
          Style.TextStyle = []
          StyleDisabled.LookAndFeel.NativeStyle = False
          StyleFocused.LookAndFeel.NativeStyle = False
          StyleHot.LookAndFeel.NativeStyle = False
          Properties.Alignment.Horz = taRightJustify
          Properties.Alignment.Vert = taVCenter
          Transparent = True
          Height = 25
          Width = 186
          AnchorX = 199
          AnchorY = 77
        end
        object txtPrimeni4: TcxDBTextEdit
          Left = 198
          Top = 67
          DataBinding.DataField = 'PRIMENI4'
          DataBinding.DataSource = dm.dsTekovenProces
          Enabled = False
          StyleDisabled.BorderColor = clWindowFrame
          StyleDisabled.Color = clWindow
          StyleDisabled.TextColor = clWindowText
          TabOrder = 11
          Width = 66
        end
        object cxLabel6: TcxLabel
          Left = 13
          Top = 85
          AutoSize = False
          Caption = '  - '#1055#1088#1080#1084#1077#1085#1080' '#1086#1076' '#1060#1040#1056#1041#1040#1056#1040
          ParentColor = False
          Style.BorderStyle = ebsNone
          Style.Color = clBtnFace
          Style.HotTrack = False
          Style.LookAndFeel.NativeStyle = False
          Style.Shadow = False
          Style.TextStyle = []
          StyleDisabled.LookAndFeel.NativeStyle = False
          StyleFocused.LookAndFeel.NativeStyle = False
          StyleHot.LookAndFeel.NativeStyle = False
          Properties.Alignment.Horz = taRightJustify
          Properties.Alignment.Vert = taVCenter
          Transparent = True
          Height = 25
          Width = 186
          AnchorX = 199
          AnchorY = 98
        end
        object txtPrimeni3: TcxDBTextEdit
          Left = 198
          Top = 88
          DataBinding.DataField = 'PRIMENI3'
          DataBinding.DataSource = dm.dsTekovenProces
          Enabled = False
          StyleDisabled.BorderColor = clWindowFrame
          StyleDisabled.Color = clWindow
          StyleDisabled.TextColor = clWindowText
          TabOrder = 13
          Width = 66
        end
        object btTapacersko: TcxButton
          Left = 0
          Top = 176
          Width = 267
          Height = 22
          Align = alBottom
          Action = aPoveke
          Caption = #1044#1077#1090#1072#1083#1085#1086' ...'
          TabOrder = 14
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
      end
      object dPanel1: TPanel
        Left = 1
        Top = 203
        Width = 271
        Height = 202
        Align = alTop
        Anchors = [akLeft, akTop, akRight, akBottom]
        BevelKind = bkFlat
        BevelOuter = bvNone
        Color = 15925247
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentBackground = False
        ParentFont = False
        TabOrder = 5
        object btMasinskoNapredok: TcxButton
          Left = 0
          Top = 176
          Width = 267
          Height = 22
          Align = alBottom
          Action = aPoveke
          Caption = #1044#1077#1090#1072#1083#1085#1086' ...'
          TabOrder = 1
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object cxlbl1: TcxLabel
          Left = 2
          Top = 41
          AutoSize = False
          Caption = #1042#1082#1091#1087#1077#1085' '#1073#1088#1086#1112' '#1085#1072' '#1087#1088#1086#1080#1079#1074#1086#1076#1080' '#1087#1086' '#1056#1053' :'
          ParentColor = False
          ParentFont = False
          Style.BorderStyle = ebsNone
          Style.Color = clBtnFace
          Style.Font.Charset = DEFAULT_CHARSET
          Style.Font.Color = clWindowText
          Style.Font.Height = -11
          Style.Font.Name = 'Tahoma'
          Style.Font.Style = []
          Style.HotTrack = False
          Style.LookAndFeel.NativeStyle = False
          Style.Shadow = False
          Style.TextStyle = [fsBold, fsUnderline]
          Style.IsFontAssigned = True
          StyleDisabled.LookAndFeel.NativeStyle = False
          StyleFocused.LookAndFeel.NativeStyle = False
          StyleHot.LookAndFeel.NativeStyle = False
          Properties.Alignment.Horz = taRightJustify
          Properties.Alignment.Vert = taVCenter
          Transparent = True
          Height = 25
          Width = 197
          AnchorX = 199
          AnchorY = 54
        end
        object cxlbl2: TcxLabel
          Left = 11
          Top = 64
          AutoSize = False
          Caption = '  - '#1055#1086#1084#1080#1085#1072#1090#1080' '#1085#1072' '#1089#1083#1077#1076#1085#1072' '#1086#1073#1088#1072#1073#1086#1090#1082#1072
          ParentColor = False
          Style.BorderStyle = ebsNone
          Style.Color = clBtnFace
          Style.HotTrack = False
          Style.LookAndFeel.NativeStyle = False
          Style.Shadow = False
          Style.TextStyle = []
          StyleDisabled.LookAndFeel.NativeStyle = False
          StyleFocused.LookAndFeel.NativeStyle = False
          StyleHot.LookAndFeel.NativeStyle = False
          Properties.Alignment.Horz = taRightJustify
          Properties.Alignment.Vert = taVCenter
          Transparent = True
          Height = 25
          Width = 188
          AnchorX = 199
          AnchorY = 77
        end
        object cxlbl3: TcxLabel
          Left = 11
          Top = 83
          AutoSize = False
          Caption = '  - '#1054#1089#1090#1072#1085#1072#1090#1080
          ParentColor = False
          Style.BorderStyle = ebsNone
          Style.Color = clBtnFace
          Style.HotTrack = False
          Style.LookAndFeel.NativeStyle = False
          Style.Shadow = False
          Style.TextStyle = []
          StyleDisabled.LookAndFeel.NativeStyle = False
          StyleFocused.LookAndFeel.NativeStyle = False
          StyleHot.LookAndFeel.NativeStyle = False
          Properties.Alignment.Horz = taRightJustify
          Properties.Alignment.Vert = taVCenter
          Transparent = True
          Height = 25
          Width = 188
          AnchorX = 199
          AnchorY = 96
        end
        object txtVkKolicina6: TcxDBTextEdit
          Left = 198
          Top = 45
          DataBinding.DataField = 'VK_KOLICINA6'
          DataBinding.DataSource = dm.dsTekovenProces
          Enabled = False
          StyleDisabled.BorderColor = clWindowFrame
          StyleDisabled.Color = clWindow
          StyleDisabled.TextColor = clWindowText
          StyleDisabled.TextStyle = []
          TabOrder = 4
          Width = 66
        end
        object txtZavrseni6: TcxDBTextEdit
          Left = 198
          Top = 67
          DataBinding.DataField = 'ZAVRSENI6'
          DataBinding.DataSource = dm.dsTekovenProces
          Enabled = False
          StyleDisabled.BorderColor = clWindowFrame
          StyleDisabled.Color = clWindow
          StyleDisabled.TextColor = clWindowText
          StyleDisabled.TextStyle = []
          TabOrder = 5
          Width = 66
        end
        object txtOstanati6: TcxDBTextEdit
          Left = 198
          Top = 88
          DataBinding.DataField = 'OSTANATI6'
          DataBinding.DataSource = dm.dsTekovenProces
          Enabled = False
          StyleDisabled.BorderColor = clWindowFrame
          StyleDisabled.Color = clWindow
          StyleDisabled.TextColor = clWindowText
          StyleDisabled.TextStyle = []
          TabOrder = 6
          Width = 66
        end
        object cxDBLabel6: TcxDBLabel
          Left = 0
          Top = 0
          Align = alTop
          DataBinding.DataField = 'KT_NAZIV6'
          DataBinding.DataSource = dm.dsTekovenProces
          ParentColor = False
          Properties.Alignment.Horz = taCenter
          Properties.Alignment.Vert = taVCenter
          Style.Color = 12910591
          Style.Shadow = True
          Style.TextStyle = [fsBold]
          Height = 25
          Width = 267
          AnchorX = 134
          AnchorY = 13
        end
      end
      object dPanel2: TPanel
        Left = 543
        Top = 203
        Width = 271
        Height = 202
        Align = alTop
        Anchors = [akLeft, akTop, akRight, akBottom]
        BevelKind = bkFlat
        BevelOuter = bvNone
        Color = 15925247
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentBackground = False
        ParentFont = False
        TabOrder = 6
        DesignSize = (
          267
          198)
        object btn1: TcxButton
          Left = 40
          Top = 352
          Width = 75
          Height = 19
          Anchors = [akRight, akBottom]
          Caption = #1055#1086#1074#1077#1116#1077' ...'
          TabOrder = 0
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object cxlbl4: TcxLabel
          Left = 9
          Top = 41
          AutoSize = False
          Caption = #1042#1082#1091#1087#1077#1085' '#1073#1088#1086#1112' '#1079#1072' '#1086#1073#1088#1072#1073#1086#1090#1082#1072' :'
          ParentColor = False
          Style.BorderStyle = ebsNone
          Style.Color = clBtnFace
          Style.HotTrack = False
          Style.LookAndFeel.NativeStyle = False
          Style.Shadow = False
          Style.TextStyle = [fsBold, fsUnderline]
          StyleDisabled.LookAndFeel.NativeStyle = False
          StyleFocused.LookAndFeel.NativeStyle = False
          StyleHot.LookAndFeel.NativeStyle = False
          Properties.Alignment.Horz = taRightJustify
          Properties.Alignment.Vert = taVCenter
          Transparent = True
          Height = 25
          Width = 190
          AnchorX = 199
          AnchorY = 54
        end
        object cxlbl5: TcxLabel
          Left = 9
          Top = 85
          AutoSize = False
          Caption = '  - '#1055#1086#1084#1080#1085#1072#1090#1080' '#1085#1072' '#1089#1083#1077#1076#1085#1072' '#1086#1073#1088#1072#1073#1086#1090#1082#1072
          ParentColor = False
          Style.BorderStyle = ebsNone
          Style.Color = clBtnFace
          Style.HotTrack = False
          Style.LookAndFeel.NativeStyle = False
          Style.Shadow = False
          Style.TextStyle = []
          StyleDisabled.LookAndFeel.NativeStyle = False
          StyleFocused.LookAndFeel.NativeStyle = False
          StyleHot.LookAndFeel.NativeStyle = False
          Properties.Alignment.Horz = taRightJustify
          Properties.Alignment.Vert = taVCenter
          Transparent = True
          Height = 25
          Width = 190
          AnchorX = 199
          AnchorY = 98
        end
        object cxlbl6: TcxLabel
          Left = 9
          Top = 106
          AutoSize = False
          Caption = '  - '#1054#1089#1090#1072#1085#1072#1090#1080
          ParentColor = False
          Style.BorderStyle = ebsNone
          Style.Color = clBtnFace
          Style.HotTrack = False
          Style.LookAndFeel.NativeStyle = False
          Style.Shadow = False
          Style.TextStyle = []
          StyleDisabled.LookAndFeel.NativeStyle = False
          StyleFocused.LookAndFeel.NativeStyle = False
          StyleHot.LookAndFeel.NativeStyle = False
          Properties.Alignment.Horz = taRightJustify
          Properties.Alignment.Vert = taVCenter
          Transparent = True
          Height = 25
          Width = 190
          AnchorX = 199
          AnchorY = 119
        end
        object btMebelStip: TcxButton
          Left = 0
          Top = 176
          Width = 267
          Height = 22
          Align = alBottom
          Action = aPoveke
          Caption = #1044#1077#1090#1072#1083#1085#1086' ...'
          TabOrder = 4
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object txtVkKolicina8: TcxDBTextEdit
          Left = 198
          Top = 45
          DataBinding.DataField = 'VK_KOLICINA7'
          DataBinding.DataSource = dm.dsTekovenProces
          Enabled = False
          StyleDisabled.BorderColor = clWindowFrame
          StyleDisabled.Color = clWindow
          StyleDisabled.TextColor = clWindowText
          TabOrder = 5
          Width = 66
        end
        object txtZavrseni8: TcxDBTextEdit
          Left = 198
          Top = 88
          DataBinding.DataField = 'ZAVRSENI7'
          DataBinding.DataSource = dm.dsTekovenProces
          Enabled = False
          StyleDisabled.BorderColor = clWindowFrame
          StyleDisabled.Color = clWindow
          StyleDisabled.TextColor = clWindowText
          TabOrder = 6
          Width = 66
        end
        object txtOstanati8: TcxDBTextEdit
          Left = 198
          Top = 109
          DataBinding.DataField = 'OSTANATI7'
          DataBinding.DataSource = dm.dsTekovenProces
          Enabled = False
          StyleDisabled.BorderColor = clWindowFrame
          StyleDisabled.Color = clWindow
          StyleDisabled.TextColor = clWindowText
          TabOrder = 7
          Width = 66
        end
        object cxDBLabel8: TcxDBLabel
          Left = 0
          Top = 0
          Align = alTop
          DataBinding.DataField = 'KT_NAZIV7'
          DataBinding.DataSource = dm.dsTekovenProces
          ParentColor = False
          Properties.Alignment.Horz = taCenter
          Properties.Alignment.Vert = taVCenter
          Style.Color = 11064319
          Style.Shadow = True
          Style.TextStyle = [fsBold]
          Height = 25
          Width = 267
          AnchorX = 134
          AnchorY = 13
        end
        object cxlbl7: TcxLabel
          Left = 9
          Top = 64
          AutoSize = False
          Caption = '  - '#1055#1088#1080#1084#1077#1085#1080' '#1086#1076' '#1087#1088#1077#1090#1093#1086#1076#1085#1072' '#1050#1058
          ParentColor = False
          Style.BorderStyle = ebsNone
          Style.Color = clBtnFace
          Style.HotTrack = False
          Style.LookAndFeel.NativeStyle = False
          Style.Shadow = False
          Style.TextStyle = []
          StyleDisabled.LookAndFeel.NativeStyle = False
          StyleFocused.LookAndFeel.NativeStyle = False
          StyleHot.LookAndFeel.NativeStyle = False
          Properties.Alignment.Horz = taRightJustify
          Properties.Alignment.Vert = taVCenter
          Transparent = True
          Height = 25
          Width = 190
          AnchorX = 199
          AnchorY = 77
        end
        object txtPrimeni8: TcxDBTextEdit
          Left = 198
          Top = 67
          DataBinding.DataField = 'PRIMENI7'
          DataBinding.DataSource = dm.dsTekovenProces
          Enabled = False
          StyleDisabled.BorderColor = clWindowFrame
          StyleDisabled.Color = clWindow
          StyleDisabled.TextColor = clWindowText
          TabOrder = 10
          Width = 66
        end
      end
      object dPanel3: TPanel
        Left = 272
        Top = 203
        Width = 271
        Height = 202
        Align = alTop
        Anchors = [akLeft, akTop, akRight, akBottom]
        BevelKind = bkFlat
        BevelOuter = bvNone
        Color = 15925247
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentBackground = False
        ParentFont = False
        TabOrder = 7
        DesignSize = (
          267
          198)
        object btn2: TcxButton
          Left = 39
          Top = 352
          Width = 75
          Height = 19
          Anchors = [akRight, akBottom]
          Caption = #1055#1086#1074#1077#1116#1077' ...'
          TabOrder = 0
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object cxlbl8: TcxLabel
          Left = 9
          Top = 41
          AutoSize = False
          Caption = #1042#1082#1091#1087#1077#1085' '#1073#1088#1086#1112' '#1079#1072' '#1086#1073#1088#1072#1073#1086#1090#1082#1072' :'
          ParentColor = False
          Style.BorderStyle = ebsNone
          Style.Color = clBtnFace
          Style.HotTrack = False
          Style.LookAndFeel.NativeStyle = False
          Style.Shadow = False
          Style.TextStyle = [fsBold, fsUnderline]
          StyleDisabled.LookAndFeel.NativeStyle = False
          StyleFocused.LookAndFeel.NativeStyle = False
          StyleHot.LookAndFeel.NativeStyle = False
          Properties.Alignment.Horz = taRightJustify
          Properties.Alignment.Vert = taVCenter
          Transparent = True
          Height = 25
          Width = 190
          AnchorX = 199
          AnchorY = 54
        end
        object cxlbl9: TcxLabel
          Left = 9
          Top = 85
          AutoSize = False
          Caption = '  - '#1055#1086#1084#1080#1085#1072#1090#1080' '#1085#1072' '#1089#1083#1077#1076#1085#1072' '#1086#1073#1088#1072#1073#1086#1090#1082#1072
          ParentColor = False
          Style.BorderStyle = ebsNone
          Style.Color = clBtnFace
          Style.HotTrack = False
          Style.LookAndFeel.NativeStyle = False
          Style.Shadow = False
          Style.TextStyle = []
          StyleDisabled.LookAndFeel.NativeStyle = False
          StyleFocused.LookAndFeel.NativeStyle = False
          StyleHot.LookAndFeel.NativeStyle = False
          Properties.Alignment.Horz = taRightJustify
          Properties.Alignment.Vert = taVCenter
          Transparent = True
          Height = 25
          Width = 190
          AnchorX = 199
          AnchorY = 98
        end
        object cxlbl10: TcxLabel
          Left = 9
          Top = 106
          AutoSize = False
          Caption = '  - '#1054#1089#1090#1072#1085#1072#1090#1080
          ParentColor = False
          Style.BorderStyle = ebsNone
          Style.Color = clBtnFace
          Style.HotTrack = False
          Style.LookAndFeel.NativeStyle = False
          Style.Shadow = False
          Style.TextStyle = []
          StyleDisabled.LookAndFeel.NativeStyle = False
          StyleFocused.LookAndFeel.NativeStyle = False
          StyleHot.LookAndFeel.NativeStyle = False
          Properties.Alignment.Horz = taRightJustify
          Properties.Alignment.Vert = taVCenter
          Transparent = True
          Height = 25
          Width = 190
          AnchorX = 199
          AnchorY = 119
        end
        object btDorabotkaStip: TcxButton
          Left = 0
          Top = 176
          Width = 267
          Height = 22
          Align = alBottom
          Action = aPoveke
          Caption = #1044#1077#1090#1072#1083#1085#1086' ...'
          TabOrder = 4
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object txtVkKolicina7: TcxDBTextEdit
          Left = 198
          Top = 45
          DataBinding.DataField = 'VK_KOLICINA9'
          DataBinding.DataSource = dm.dsTekovenProces
          Enabled = False
          StyleDisabled.BorderColor = clWindowFrame
          StyleDisabled.Color = clWindow
          StyleDisabled.TextColor = clWindowText
          TabOrder = 5
          Width = 66
        end
        object txtZavrseni7: TcxDBTextEdit
          Left = 198
          Top = 88
          DataBinding.DataField = 'ZAVRSENI9'
          DataBinding.DataSource = dm.dsTekovenProces
          Enabled = False
          StyleDisabled.BorderColor = clWindowFrame
          StyleDisabled.Color = clWindow
          StyleDisabled.TextColor = clWindowText
          TabOrder = 6
          Width = 66
        end
        object txtOstanati7: TcxDBTextEdit
          Left = 198
          Top = 109
          DataBinding.DataField = 'OSTANATI9'
          DataBinding.DataSource = dm.dsTekovenProces
          Enabled = False
          StyleDisabled.BorderColor = clWindowFrame
          StyleDisabled.Color = clWindow
          StyleDisabled.TextColor = clWindowText
          TabOrder = 7
          Width = 66
        end
        object cxDBLabel7: TcxDBLabel
          Left = 0
          Top = 0
          Align = alTop
          DataBinding.DataField = 'KT_NAZIV9'
          DataBinding.DataSource = dm.dsTekovenProces
          ParentColor = False
          Properties.Alignment.Horz = taCenter
          Properties.Alignment.Vert = taVCenter
          Style.Color = 11064319
          Style.Shadow = True
          Style.TextStyle = [fsBold]
          Height = 25
          Width = 267
          AnchorX = 134
          AnchorY = 13
        end
        object cxlbl11: TcxLabel
          Left = 9
          Top = 64
          AutoSize = False
          Caption = '  - '#1055#1088#1080#1084#1077#1085#1080' '#1086#1076' '#1087#1088#1077#1090#1093#1086#1076#1085#1072' '#1050#1058
          ParentColor = False
          Style.BorderStyle = ebsNone
          Style.Color = clBtnFace
          Style.HotTrack = False
          Style.LookAndFeel.NativeStyle = False
          Style.Shadow = False
          Style.TextStyle = []
          StyleDisabled.LookAndFeel.NativeStyle = False
          StyleFocused.LookAndFeel.NativeStyle = False
          StyleHot.LookAndFeel.NativeStyle = False
          Properties.Alignment.Horz = taRightJustify
          Properties.Alignment.Vert = taVCenter
          Transparent = True
          Height = 25
          Width = 190
          AnchorX = 199
          AnchorY = 77
        end
        object txtPrimeni7: TcxDBTextEdit
          Left = 198
          Top = 67
          DataBinding.DataField = 'PRIMENI9'
          DataBinding.DataSource = dm.dsTekovenProces
          Enabled = False
          StyleDisabled.BorderColor = clWindowFrame
          StyleDisabled.Color = clWindow
          StyleDisabled.TextColor = clWindowText
          TabOrder = 10
          Width = 66
        end
      end
      object dPanel4: TPanel
        Left = 814
        Top = 203
        Width = 271
        Height = 202
        Align = alTop
        Anchors = [akLeft, akTop, akRight, akBottom]
        BevelKind = bkFlat
        BevelOuter = bvNone
        Color = 15925247
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentBackground = False
        ParentFont = False
        TabOrder = 8
        DesignSize = (
          267
          198)
        object btn3: TcxButton
          Left = 40
          Top = 710
          Width = 75
          Height = 19
          Anchors = [akRight, akBottom]
          Caption = #1055#1086#1074#1077#1116#1077' ...'
          TabOrder = 0
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object cxlbl12: TcxLabel
          Left = -1
          Top = 41
          AutoSize = False
          Caption = #1042#1082#1091#1087#1077#1085' '#1073#1088#1086#1112' '#1079#1072' '#1086#1073#1088#1072#1073#1086#1090#1082#1072':'
          ParentColor = False
          Style.BorderStyle = ebsNone
          Style.Color = clBtnFace
          Style.HotTrack = False
          Style.LookAndFeel.NativeStyle = False
          Style.Shadow = False
          Style.TextStyle = [fsBold, fsUnderline]
          StyleDisabled.LookAndFeel.NativeStyle = False
          StyleFocused.LookAndFeel.NativeStyle = False
          StyleHot.LookAndFeel.NativeStyle = False
          Properties.Alignment.Horz = taRightJustify
          Properties.Alignment.Vert = taVCenter
          Transparent = True
          Height = 25
          Width = 198
          AnchorX = 197
          AnchorY = 54
        end
        object cxlbl13: TcxLabel
          Left = -1
          Top = 85
          AutoSize = False
          Caption = '  - '#1047#1072' '#1090#1088#1072#1085#1089#1087#1086#1088#1090
          ParentColor = False
          Style.BorderStyle = ebsNone
          Style.Color = clBtnFace
          Style.HotTrack = False
          Style.LookAndFeel.NativeStyle = False
          Style.Shadow = False
          Style.TextStyle = []
          StyleDisabled.LookAndFeel.NativeStyle = False
          StyleFocused.LookAndFeel.NativeStyle = False
          StyleHot.LookAndFeel.NativeStyle = False
          Properties.Alignment.Horz = taRightJustify
          Properties.Alignment.Vert = taVCenter
          Transparent = True
          Height = 25
          Width = 198
          AnchorX = 197
          AnchorY = 98
        end
        object cxlbl14: TcxLabel
          Left = -1
          Top = 104
          AutoSize = False
          Caption = '  - '#1054#1089#1090#1072#1085#1072#1090#1080
          ParentColor = False
          Style.BorderStyle = ebsNone
          Style.Color = clBtnFace
          Style.HotTrack = False
          Style.LookAndFeel.NativeStyle = False
          Style.Shadow = False
          Style.TextStyle = []
          StyleDisabled.LookAndFeel.NativeStyle = False
          StyleFocused.LookAndFeel.NativeStyle = False
          StyleHot.LookAndFeel.NativeStyle = False
          Properties.Alignment.Horz = taRightJustify
          Properties.Alignment.Vert = taVCenter
          Transparent = True
          Height = 25
          Width = 198
          AnchorX = 197
          AnchorY = 117
        end
        object btn4: TcxButton
          Left = 172
          Top = 683
          Width = 75
          Height = 19
          Anchors = [akRight, akBottom]
          Caption = #1055#1086#1074#1077#1116#1077' ...'
          TabOrder = 4
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object btn5: TcxButton
          Left = 172
          Top = 447
          Width = 75
          Height = 19
          Anchors = [akRight, akBottom]
          Caption = #1055#1086#1074#1077#1116#1077' ...'
          TabOrder = 5
          Visible = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object txtVkKolicina9: TcxDBTextEdit
          Left = 197
          Top = 45
          DataBinding.DataField = 'VK_KOLICINA8'
          DataBinding.DataSource = dm.dsTekovenProces
          Enabled = False
          StyleDisabled.BorderColor = clWindowFrame
          StyleDisabled.Color = clWindow
          StyleDisabled.TextColor = clWindowText
          TabOrder = 6
          Width = 66
        end
        object txtZavrseni9: TcxDBTextEdit
          Left = 197
          Top = 88
          DataBinding.DataField = 'ZAVRSENI8'
          DataBinding.DataSource = dm.dsTekovenProces
          Enabled = False
          StyleDisabled.BorderColor = clWindowFrame
          StyleDisabled.Color = clWindow
          StyleDisabled.TextColor = clWindowText
          TabOrder = 7
          Width = 66
        end
        object txtOstanati9: TcxDBTextEdit
          Left = 197
          Top = 109
          DataBinding.DataField = 'OSTANATI8'
          DataBinding.DataSource = dm.dsTekovenProces
          Enabled = False
          StyleDisabled.BorderColor = clWindowFrame
          StyleDisabled.Color = clWindow
          StyleDisabled.TextColor = clWindowText
          TabOrder = 8
          Width = 66
        end
        object cxDBLabel9: TcxDBLabel
          Left = 0
          Top = 0
          Align = alTop
          DataBinding.DataField = 'KT_NAZIV8'
          DataBinding.DataSource = dm.dsTekovenProces
          ParentColor = False
          Properties.Alignment.Horz = taCenter
          Properties.Alignment.Vert = taVCenter
          Style.Color = 16770995
          Style.Shadow = True
          Style.TextStyle = [fsBold]
          Height = 25
          Width = 267
          AnchorX = 134
          AnchorY = 13
        end
        object cxlbl15: TcxLabel
          Left = -1
          Top = 64
          AutoSize = False
          Caption = '  - '#1055#1088#1080#1084#1077#1085#1080' '#1086#1076' '#1087#1088#1077#1090#1093#1086#1076#1085#1072' '#1050#1058
          ParentColor = False
          Style.BorderStyle = ebsNone
          Style.Color = clBtnFace
          Style.HotTrack = False
          Style.LookAndFeel.NativeStyle = False
          Style.Shadow = False
          Style.TextStyle = []
          StyleDisabled.LookAndFeel.NativeStyle = False
          StyleFocused.LookAndFeel.NativeStyle = False
          StyleHot.LookAndFeel.NativeStyle = False
          Properties.Alignment.Horz = taRightJustify
          Properties.Alignment.Vert = taVCenter
          Transparent = True
          Height = 25
          Width = 198
          AnchorX = 197
          AnchorY = 77
        end
        object txtPrimeni9: TcxDBTextEdit
          Left = 197
          Top = 67
          DataBinding.DataField = 'PRIMENI8'
          DataBinding.DataSource = dm.dsTekovenProces
          Enabled = False
          StyleDisabled.BorderColor = clWindowFrame
          StyleDisabled.Color = clWindow
          StyleDisabled.TextColor = clWindowText
          TabOrder = 11
          Width = 66
        end
        object btTrgovskaStoka: TcxButton
          Left = 0
          Top = 176
          Width = 267
          Height = 22
          Align = alBottom
          Action = aPoveke
          Caption = #1044#1077#1090#1072#1083#1085#1086' ...'
          TabOrder = 12
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
      end
    end
    object gbPrebaruvanje: TcxGroupBox
      Left = 1
      Top = 1
      Align = alTop
      Caption = '  '#1048#1079#1074#1088#1096#1077#1085#1072' '#1088#1072#1073#1086#1090#1072' '#1087#1086' '#1056#1072#1073#1086#1090#1077#1085' '#1085#1072#1083#1086#1075'   '
      ParentFont = False
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clWindowText
      Style.Font.Height = -11
      Style.Font.Name = 'Tahoma'
      Style.Font.Style = []
      Style.IsFontAssigned = True
      TabOrder = 1
      Height = 152
      Width = 1360
      object Label14: TLabel
        Left = 31
        Top = 57
        Width = 70
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1047#1072' '#1082#1091#1087#1091#1074#1072#1095
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        WordWrap = True
      end
      object Label1: TLabel
        Left = 31
        Top = 99
        Width = 70
        Height = 32
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1040#1088#1090#1080#1082#1072#1083' / '#1055#1088#1086#1080#1079#1074#1086#1076
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        WordWrap = True
      end
      object cxLabel8: TcxLabel
        Left = 45
        Top = 31
        Caption = #1041#1088#1086#1112' '#1085#1072' '#1056#1053
        ParentFont = False
        Style.Font.Charset = DEFAULT_CHARSET
        Style.Font.Color = clNavy
        Style.Font.Height = -11
        Style.Font.Name = 'Tahoma'
        Style.Font.Style = []
        Style.TextColor = clRed
        Style.IsFontAssigned = True
        Transparent = True
      end
      object cxLabel7: TcxLabel
        Left = 50
        Top = 78
        Caption = #1047#1072' '#1044#1072#1090#1091#1084
        Style.TextColor = clRed
        Transparent = True
      end
      object txtDatum: TcxDateEdit
        Left = 106
        Top = 78
        Properties.Kind = ckDateTime
        TabOrder = 1
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 190
      end
      object cbRN: TcxLookupComboBox
        Tag = 1
        Left = 107
        Top = 30
        Properties.DropDownAutoSize = True
        Properties.DropDownSizeable = True
        Properties.KeyFieldNames = 'ID'
        Properties.ListColumns = <
          item
            Width = 100
            FieldName = 'BROJ'
          end>
        Properties.ListOptions.SyncMode = True
        Properties.ListSource = dsIzberiRN
        Properties.OnChange = cbRNPropertiesChange
        TabOrder = 0
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 190
      end
      object cxButton3: TcxButton
        Left = 556
        Top = 100
        Width = 162
        Height = 25
        Action = aPogled
        TabOrder = 7
      end
      object txtTP: TcxTextEdit
        Tag = 1
        Left = 106
        Top = 54
        Enabled = False
        Style.LookAndFeel.NativeStyle = False
        StyleDisabled.Color = clWindow
        StyleDisabled.LookAndFeel.NativeStyle = False
        StyleDisabled.TextColor = clWindowText
        StyleFocused.LookAndFeel.NativeStyle = False
        StyleHot.LookAndFeel.NativeStyle = False
        TabOrder = 9
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 52
      end
      object txtP: TcxTextEdit
        Tag = 1
        Left = 158
        Top = 54
        Enabled = False
        StyleDisabled.Color = clWindow
        StyleDisabled.TextColor = clWindowText
        TabOrder = 8
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 57
      end
      object txtVidArtikal: TcxTextEdit
        Tag = 1
        Left = 106
        Top = 102
        Style.LookAndFeel.NativeStyle = False
        StyleDisabled.LookAndFeel.NativeStyle = False
        StyleFocused.LookAndFeel.NativeStyle = False
        StyleHot.LookAndFeel.NativeStyle = False
        TabOrder = 4
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 52
      end
      object txtArtikal: TcxTextEdit
        Tag = 1
        Left = 158
        Top = 102
        TabOrder = 5
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 57
      end
      object cbArtikal: TcxLookupComboBox
        Left = 215
        Top = 102
        ParentShowHint = False
        Properties.DropDownAutoSize = True
        Properties.DropDownSizeable = True
        Properties.ImmediatePost = True
        Properties.KeyFieldNames = 'ARTVID;id'
        Properties.ListColumns = <
          item
            Width = 20
            FieldName = 'ARTVID'
          end
          item
            Width = 20
            FieldName = 'ID'
          end
          item
            Width = 100
            FieldName = 'NAZIV'
          end>
        Properties.ListFieldIndex = 2
        Properties.ListOptions.SyncMode = True
        Properties.ListSource = dsArtikalRN
        ShowHint = True
        TabOrder = 6
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 307
      end
      object cbP: TcxTextEdit
        Left = 215
        Top = 54
        Enabled = False
        StyleDisabled.Color = clWindow
        StyleDisabled.TextColor = clWindowText
        TabOrder = 10
        Width = 307
      end
    end
  end
  object cxGridPopupMenu1: TcxGridPopupMenu
    PopupMenus = <>
    Left = 805
    Top = 16
  end
  object PopupMenu1: TPopupMenu
    Left = 840
    Top = 51
    object N1: TMenuItem
      Caption = #1057#1085#1080#1084#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      ShortCut = 49235
      OnClick = SaveToIniFileExecute
    end
    object Excel1: TMenuItem
      Caption = #1057#1085#1080#1084#1080' '#1075#1086' '#1074#1086' Excel'
      ShortCut = 49221
    end
  end
  object dxBarManager1: TdxBarManager
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Segoe UI'
    Font.Style = []
    Categories.Strings = (
      'Default')
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    ImageOptions.Images = dmRes.cxSmallImages
    ImageOptions.LargeImages = dmRes.cxLargeImages
    PopupMenuLinks = <>
    UseSystemFont = True
    Left = 910
    Top = 16
    DockControlHeights = (
      0
      0
      0
      0)
    object dxBarManager1Bar3: TdxBar
      Caption = #1048#1079#1083#1077#1079
      CaptionButtons = <>
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 718
      FloatTop = 62
      FloatClientWidth = 51
      FloatClientHeight = 80
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton7'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton8'
        end>
      OneOnRow = True
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar4: TdxBar
      Caption = #1055#1077#1095#1072#1090#1077#1114#1077
      CaptionButtons = <>
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 676
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton12'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton15'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton9'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarLargeButton13'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1BarIzgledGrid: TdxBar
      Caption = #1058#1072#1073#1077#1083#1072
      CaptionButtons = <>
      DockedLeft = 363
      DockedTop = 0
      FloatLeft = 808
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLBtnSnimiIzgled'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarLBtnBrisiIzgled'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarLargeButton2: TdxBarLargeButton
      Action = aAzuriraj
      Category = 0
    end
    object dxBarLargeButton3: TdxBarLargeButton
      Action = aBrisi
      Category = 0
    end
    object dxBarLargeButton4: TdxBarLargeButton
      Action = aSnimiIzgled
      Category = 0
      ScreenTip = tipSnimiIzgled
    end
    object dxBarLargeButton5: TdxBarLargeButton
      Action = aZacuvajExcel
      Category = 0
      ScreenTip = tipZacuvajExcel
    end
    object dxBarLargeButton6: TdxBarLargeButton
      Action = aPecatiTabela
      Caption = #1055#1077#1095#1072#1090#1080' '#1090#1072#1073#1077#1083#1072
      Category = 0
    end
    object dxBarLargeButton7: TdxBarLargeButton
      Action = aHelp
      Category = 0
    end
    object dxBarLargeButton8: TdxBarLargeButton
      Action = aIzlez
      Category = 0
    end
    object dxBarLargeButton1: TdxBarLargeButton
      Action = aNov
      Category = 0
    end
    object dxBarLargeButton9: TdxBarLargeButton
      Action = aSnimiPecatenje
      Category = 0
      ScreenTip = tipSnimiKonf
    end
    object dxBarLargeButton11: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object ribbonFilterReset: TdxBarLargeButton
      Caption = #1056#1077#1089#1077#1090#1080#1088#1072#1112' '#1092#1080#1083#1090#1077#1088
      Category = 0
      Hint = #1056#1077#1089#1077#1090#1080#1088#1072#1112' '#1092#1080#1083#1090#1077#1088
      Visible = ivAlways
    end
    object dxBarLargeButton12: TdxBarLargeButton
      Action = aPodesuvanjePecatenje
      Category = 0
      ScreenTip = tipDizajnReport
    end
    object dxBarLargeButton13: TdxBarLargeButton
      Action = aBrisiPodesuvanjePecatenje
      Category = 0
      ScreenTip = tipBrisiKonf
    end
    object dxBarLargeButton14: TdxBarLargeButton
      Caption = #1048#1079#1073#1088#1080#1096#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      Category = 0
      Visible = ivAlways
      LargeImageIndex = 40
    end
    object dxBarLargeButton15: TdxBarLargeButton
      Action = aPageSetup
      Category = 0
      ScreenTip = tipSetiranjeStrana
    end
    object dxBarLargeButton16: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object dxBarLargeButton17: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object cxBarEditItem1: TcxBarEditItem
      Caption = 'New Item'
      Category = 0
      Hint = 'New Item'
      Visible = ivAlways
      PropertiesClassName = 'TcxCheckBoxProperties'
    end
    object dxBarSubItem1: TdxBarSubItem
      Caption = #1055#1077#1095#1072#1090#1077#1114#1077
      Category = 0
      Visible = ivAlways
      ImageIndex = 30
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton6'
        end>
    end
    object dxBarLBtnSnimiIzgled: TdxBarLargeButton
      Action = aSnimiIzgled
      Category = 0
    end
    object dxBarLBtnBrisiIzgled: TdxBarLargeButton
      Action = aBrisiIzgled
      Category = 0
    end
  end
  object ActionList1: TActionList
    Images = dmRes.cxSmallImages
    Left = 770
    Top = 16
    object aNov: TAction
      Caption = #1053#1086#1074
      ImageIndex = 10
      ShortCut = 116
      OnExecute = aNovExecute
    end
    object aAzuriraj: TAction
      Caption = #1040#1078#1091#1088#1080#1088#1072#1112
      ImageIndex = 12
      ShortCut = 117
      OnExecute = aAzurirajExecute
    end
    object aBrisi: TAction
      Caption = #1041#1088#1080#1096#1080
      ImageIndex = 11
      ShortCut = 119
      OnExecute = aBrisiExecute
    end
    object aSnimiIzgled: TAction
      Caption = #1057#1085#1080#1084#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      ImageIndex = 9
      ShortCut = 24659
      OnExecute = aSnimiIzgledExecute
    end
    object aZacuvajExcel: TAction
      Caption = #1047#1072#1095#1091#1074#1072#1112' '#1074#1086' Excel'
      ImageIndex = 9
      ShortCut = 24645
      OnExecute = aZacuvajExcelExecute
    end
    object aPecatiTabela: TAction
      Caption = #1055#1077#1095#1072#1090#1080
      ImageIndex = 30
      OnExecute = aPecatiTabelaExecute
    end
    object aHelp: TAction
      Caption = #1055#1086#1084#1086#1096
      ImageIndex = 24
    end
    object aIzlez: TAction
      Caption = #1048#1079#1083#1077#1079
      ImageIndex = 25
      OnExecute = aIzlezExecute
    end
    object aZapisi: TAction
      Caption = #1047#1072#1087#1080#1096#1080
      ImageIndex = 7
      ShortCut = 120
      OnExecute = aZapisiExecute
    end
    object aOtkazi: TAction
      Caption = #1054#1090#1082#1072#1078#1080
      ImageIndex = 21
      ShortCut = 27
      OnExecute = aOtkaziExecute
    end
    object aRefresh: TAction
      Caption = 'aRefresh'
      ImageIndex = 18
      ShortCut = 118
      OnExecute = aRefreshExecute
    end
    object aSnimiPecatenje: TAction
      Caption = #1057#1085#1080#1084#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      ImageIndex = 39
      OnExecute = aSnimiPecatenjeExecute
    end
    object aPodesuvanjePecatenje: TAction
      Caption = #1044#1080#1079#1072#1112#1085' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090
      ImageIndex = 38
      OnExecute = aPodesuvanjePecatenjeExecute
    end
    object aBrisiPodesuvanjePecatenje: TAction
      Caption = #1041#1088#1080#1096#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      ImageIndex = 37
      OnExecute = aBrisiPodesuvanjePecatenjeExecute
    end
    object aPageSetup: TAction
      Caption = #1057#1077#1090#1080#1088#1072#1114#1077' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072
      ImageIndex = 38
      OnExecute = aPageSetupExecute
    end
    object aFormConfig: TAction
      Caption = 'aFormConfig'
      ShortCut = 24699
      OnExecute = aFormConfigExecute
    end
    object aBrisiIzgled: TAction
      Caption = #1041#1088#1080#1096#1080' '#1075#1086' '#1079#1072#1095#1091#1074#1072#1085#1080#1086#1090' '#1080#1079#1075#1083#1077#1076
      ImageIndex = 56
      OnExecute = aBrisiIzgledExecute
    end
    object aPogled: TAction
      Caption = #1055#1086#1075#1083#1077#1076
      ImageIndex = 47
      OnExecute = aPogledExecute
    end
    object aPoveke: TAction
      Caption = #1044#1077#1090#1072#1083#1085#1086
      OnExecute = aPovekeExecute
    end
  end
  object dxComponentPrinter1: TdxComponentPrinter
    CurrentLink = dxComponentPrinter1Link1
    Version = 0
    Left = 980
    Top = 16
    object dxComponentPrinter1Link1: TdxGridReportLink
      PageNumberFormat = pnfNumeral
      PrinterPage.DMPaper = 9
      PrinterPage.Footer = 6350
      PrinterPage.GrayShading = True
      PrinterPage.Header = 6350
      PrinterPage.Margins.Bottom = 12700
      PrinterPage.Margins.Left = 12700
      PrinterPage.Margins.Right = 12700
      PrinterPage.Margins.Top = 12700
      PrinterPage.Orientation = poLandscape
      PrinterPage.PageFooter.RightTitle.Strings = (
        #1057#1090#1088'.[Page #] '#1086#1076' [Total Pages]')
      PrinterPage.PageSize.X = 210000
      PrinterPage.PageSize.Y = 297000
      PrinterPage._dxMeasurementUnits_ = 0
      PrinterPage._dxLastMU_ = 2
      ReportDocument.CreationDate = 40336.425359629630000000
      AssignedFormatValues = [fvDate, fvTime, fvPageNumber]
      OptionsFormatting.UseNativeStyles = True
      OptionsOnEveryPage.Caption = False
      OptionsOnEveryPage.FilterBar = False
      OptionsView.Caption = False
      OptionsView.FilterBar = False
      StyleRepository = dmRes.cxStyleRepositoryPrinting
      Styles.StyleSheet = dmRes.dxGridReportLinkStyleSheet
      BuiltInReportLink = True
    end
  end
  object dxBarScreenTipRepository1: TdxBarScreenTipRepository
    Left = 945
    Top = 16
    object tipSnimiIzgled: TdxBarScreenTip
      Header.Text = #1057#1085#1080#1084#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      Description.Text = #1047#1072#1095#1091#1074#1072#1112' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090' '#1085#1072' '#1092#1086#1088#1084#1072#1090#1072' '#1080' '#1085#1072' '#1090#1072#1073#1077#1083#1072#1090#1072
    end
    object tipZacuvajExcel: TdxBarScreenTip
      Header.Text = #1047#1072#1095#1091#1074#1072#1112' '#1074#1086' Excel'
      Description.Text = #1057#1085#1080#1084#1080' '#1075#1080' '#1087#1086#1076#1072#1090#1086#1094#1080#1090#1077' '#1086#1076' '#1090#1072#1073#1077#1083#1072#1090#1072' '#1074#1086' Excel '#1092#1086#1088#1084#1072#1090
    end
    object tipDizajnReport: TdxBarScreenTip
      Header.Text = #1044#1080#1079#1072#1112#1085' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090
      Description.Text = #1055#1086#1076#1077#1089#1091#1074#1072#1114#1072' '#1085#1072' '#1080#1079#1075#1083#1077#1076#1086#1090' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090' '#1085#1072' '#1090#1072#1073#1077#1083#1072#1090#1072
    end
    object tipSetiranjeStrana: TdxBarScreenTip
      Header.Text = #1057#1077#1090#1080#1088#1072#1114#1077' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072
      Description.Text = #1055#1086#1076#1077#1089#1091#1074#1072#1114#1077' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072' '#1085#1072' '#1082#1086#1112#1072' '#1116#1077' '#1089#1077' '#1087#1077#1095#1072#1090#1080' '#1090#1072#1073#1077#1083#1072#1090#1072
    end
    object tipSnimiKonf: TdxBarScreenTip
      Header.Text = #1057#1085#1080#1084#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      Description.Text = #1047#1072#1095#1091#1074#1072#1112' '#1075#1080' '#1087#1086#1076#1077#1089#1091#1074#1072#1114#1072#1090#1072' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090' '#1080' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072' '#1079#1072' '#1087#1077#1095#1072#1090#1077#1114#1077
    end
    object tipBrisiKonf: TdxBarScreenTip
      Header.Text = #1041#1088#1080#1096#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      Description.Text = #1048#1079#1073#1088#1080#1096#1080' '#1075#1080' '#1087#1086#1076#1077#1089#1091#1074#1072#1114#1072#1090#1072' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090' '#1080' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072' '#1079#1072' '#1087#1077#1095#1072#1090#1077#1114#1077
    end
  end
  object tblIzberiRN: TpFIBDataSet
    RefreshSQL.Strings = (
      'select'
      '   rn.id,'
      '   rn.broj||'#39'/'#39'||rn.godina broj,'
      '   rn.status,'
      '   rn.zabeleska,'
      '   rn.tip_partner tp_poracka,'
      '   rn.partner p_poracka,'
      '   mp.naziv naziv_partner_poracka,'
      '   rn.id_re'
      'from man_raboten_nalog rn'
      
        'left outer join mat_partner mp on mp.id = rn.partner and mp.tip_' +
        'partner = rn.tip_partner'
      'where(  rn.status like :status and rn.source_dokument is null'
      '     ) and (     RN.ID = :OLD_ID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      'select'
      '   rn.id,'
      '   rn.broj||'#39'/'#39'||rn.godina broj,'
      '   rn.status,'
      '   rn.zabeleska,'
      '   rn.tip_partner tp_poracka,'
      '   rn.partner p_poracka,'
      '   mp.naziv naziv_partner_poracka,'
      '   rn.id_re'
      'from man_raboten_nalog rn'
      
        'left outer join mat_partner mp on mp.id = rn.partner and mp.tip_' +
        'partner = rn.tip_partner'
      'where rn.status like :status and rn.source_dokument is null')
    AutoUpdateOptions.GeneratorName = 'GEN__ID'
    AutoUpdateOptions.WhenGetGenID = wgBeforePost
    AutoUpdateOptions.AutoParamsToFields = True
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 805
    Top = 51
    object tblIzberiRNSTATUS: TFIBIntegerField
      FieldName = 'STATUS'
    end
    object tblIzberiRNNAZIV_PARTNER_PORACKA: TFIBStringField
      DisplayLabel = #1055#1086#1088#1072#1095#1082#1072' - '#1055#1072#1088#1090#1085#1077#1088
      FieldName = 'NAZIV_PARTNER_PORACKA'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblIzberiRNZABELESKA: TFIBStringField
      FieldName = 'ZABELESKA'
      Size = 200
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblIzberiRNTP_PORACKA: TFIBIntegerField
      FieldName = 'TP_PORACKA'
    end
    object tblIzberiRNP_PORACKA: TFIBIntegerField
      FieldName = 'P_PORACKA'
    end
    object tblIzberiRNID: TFIBBCDField
      FieldName = 'ID'
      Size = 0
    end
    object tblIzberiRNBROJ: TFIBStringField
      DisplayLabel = #1041#1088#1086#1112' '#1085#1072' '#1056#1053
      FieldName = 'BROJ'
      Size = 112
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblIzberiRNID_RE: TFIBIntegerField
      FieldName = 'ID_RE'
    end
  end
  object dsIzberiRN: TDataSource
    DataSet = tblIzberiRN
    Left = 840
    Top = 16
  end
  object tblArtikalRN: TpFIBDataSet
    RefreshSQL.Strings = (
      'select distinct'
      '       A.ARTVID,'
      '       A.ID,'
      '       A.NAZIV/*$$IBEC$$ ,'
      '       A.MERKA,'
      '       a.merka_kolicina,'
      '       rns.zabeleska $$IBEC$$*/'
      'from MTR_ARTIKAL A'
      
        'inner join man_raboten_nalog_stavka rns on rns.vid_artikal = a.a' +
        'rtvid and rns.artikal = a.id'
      'where(  rns.id_raboten_nalog = :id'
      '     ) and (     A.ARTVID = :OLD_ARTVID'
      '    and A.ID = :OLD_ID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      'select distinct'
      '       A.ARTVID,'
      '       A.ID,'
      '       A.NAZIV/*$$IBEC$$ ,'
      '       A.MERKA,'
      '       a.merka_kolicina,'
      '       rns.zabeleska $$IBEC$$*/'
      'from MTR_ARTIKAL A'
      
        'inner join man_raboten_nalog_stavka rns on rns.vid_artikal = a.a' +
        'rtvid and rns.artikal = a.id'
      'where rns.id_raboten_nalog = :id')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 770
    Top = 51
    poSQLINT64ToBCD = True
    object tblArtikalRNARTVID: TFIBIntegerField
      DisplayLabel = #1042#1080#1076' '#1085#1072' '#1072#1088#1090#1080#1082#1072#1083
      FieldName = 'ARTVID'
    end
    object tblArtikalRNID: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072
      FieldName = 'ID'
    end
    object tblArtikalRNNAZIV: TFIBStringField
      DisplayLabel = #1053#1072#1079#1080#1074
      FieldName = 'NAZIV'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object dsArtikalRN: TDataSource
    AutoEdit = False
    DataSet = tblArtikalRN
    Left = 875
    Top = 16
  end
end
