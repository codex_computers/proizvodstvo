object frmPromenaLager: TfrmPromenaLager
  Left = 0
  Top = 0
  Caption = #1055#1088#1086#1084#1077#1085#1072' '#1085#1072' '#1051#1072#1075#1077#1088
  ClientHeight = 219
  ClientWidth = 418
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 418
    Height = 219
    Align = alClient
    TabOrder = 0
    ExplicitWidth = 469
    ExplicitHeight = 231
    object lblNaziv: TcxLabel
      Left = 1
      Top = 1
      Align = alTop
      AutoSize = False
      ParentFont = False
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clWindowText
      Style.Font.Height = -13
      Style.Font.Name = 'Tahoma'
      Style.Font.Style = []
      Style.IsFontAssigned = True
      Properties.Alignment.Horz = taCenter
      Properties.Alignment.Vert = taVCenter
      Transparent = True
      ExplicitWidth = 467
      Height = 40
      Width = 416
      AnchorX = 209
      AnchorY = 21
    end
    object txtVlez: TcxTextEdit
      Left = 75
      Top = 100
      AutoSize = False
      TabOrder = 1
      Height = 27
      Width = 121
    end
    object txtIzlez: TcxTextEdit
      Left = 214
      Top = 100
      AutoSize = False
      TabOrder = 2
      Height = 27
      Width = 121
    end
    object cxLabel1: TcxLabel
      Left = 75
      Top = 76
      AutoSize = False
      Caption = #1042#1083#1077#1079
      ParentFont = False
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clWindowText
      Style.Font.Height = -16
      Style.Font.Name = 'Tahoma'
      Style.Font.Style = []
      Style.IsFontAssigned = True
      Transparent = True
      Height = 25
      Width = 49
    end
    object cxLabel2: TcxLabel
      Left = 214
      Top = 76
      AutoSize = False
      Caption = #1048#1079#1083#1077#1079
      ParentFont = False
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clWindowText
      Style.Font.Height = -16
      Style.Font.Name = 'Tahoma'
      Style.Font.Style = []
      Style.IsFontAssigned = True
      Transparent = True
      Height = 25
      Width = 73
    end
    object cxButton1: TcxButton
      Left = 166
      Top = 160
      Width = 75
      Height = 25
      Action = aZapisi
      TabOrder = 5
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object cxButton2: TcxButton
      Left = 260
      Top = 160
      Width = 75
      Height = 25
      Action = aOtkazi
      TabOrder = 6
    end
  end
  object ActionList1: TActionList
    Images = dmRes.cxIcoDBSmall
    Left = 24
    Top = 40
    object aZapisi: TAction
      Caption = #1047#1072#1087#1080#1096#1080
      ShortCut = 120
      OnExecute = aZapisiExecute
    end
    object aOtkazi: TAction
      Caption = #1054#1090#1082#1072#1078#1080
      ShortCut = 27
      OnExecute = aOtkaziExecute
    end
  end
end
