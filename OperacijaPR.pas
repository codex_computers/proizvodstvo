unit OperacijaPR;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Master, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxStyles, dxSkinsCore, dxSkinBlack,
  dxSkinBlue, dxSkinBlueprint, dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom,
  dxSkinDarkSide, dxSkinDevExpressDarkStyle, dxSkinDevExpressStyle, dxSkinFoggy,
  dxSkinGlassOceans, dxSkinHighContrast, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven,
  dxSkinSevenClassic, dxSkinSharp, dxSkinSharpPlus, dxSkinSilver,
  dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008, dxSkinTheAsphaltWorld,
  dxSkinsDefaultPainters, dxSkinValentine, dxSkinVS2010, dxSkinWhiteprint,
  dxSkinXmas2008Blue, dxSkinscxPCPainter, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxEdit, Data.DB, cxDBData, cxContainer, Vcl.Menus,
  dxRibbonSkins, dxSkinsdxRibbonPainter, dxSkinsdxBarPainter, cxDropDownEdit,
  dxPSGlbl, dxPSUtl, dxPSEngn, dxPrnPg, dxBkgnd, dxWrap, dxPrnDev,
  dxPSCompsProvider, dxPSFillPatterns, dxPSEdgePatterns, dxPSPDFExportCore,
  dxPSPDFExport, cxDrawTextUtils, dxPSPrVwStd, dxPSPrVwAdv, dxPSPrVwRibbon,
  dxPScxPageControlProducer, dxPScxGridLnk, dxPScxGridLayoutViewLnk,
  dxPScxEditorProducers, dxPScxExtEditorProducers, dxScreenTip, dxBar, dxPSCore,
  dxPScxCommon, Vcl.ActnList, cxBarEditItem, cxGridCustomPopupMenu,
  cxGridPopupMenu, dxStatusBar, dxRibbonStatusBar, cxClasses, dxRibbon,
  Vcl.StdCtrls, cxButtons, cxTextEdit, cxDBEdit, cxGridLevel, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid,
  Vcl.ExtCtrls, cxMaskEdit, cxLookupEdit, cxDBLookupEdit, cxDBLookupComboBox,
  cxLabel, dxSkinMetropolis, dxSkinMetropolisDark, dxSkinOffice2013DarkGray,
  dxSkinOffice2013LightGray, dxSkinOffice2013White, cxNavigator,
  dxRibbonCustomizationForm, System.Actions;

type
  TfrmOperacijaPR = class(TfrmMaster)
    cxLabel6: TcxLabel;
    txtOperacija: TcxDBTextEdit;
    Label7: TLabel;
    txtRE: TcxDBTextEdit;
    cbRE: TcxDBLookupComboBox;
    cxGrid1DBTableView1ID: TcxGridDBColumn;
    cxGrid1DBTableView1ID_PROIZVODSTVEN_RESURS: TcxGridDBColumn;
    cxGrid1DBTableView1PROIZVODSTVEN_RESURS: TcxGridDBColumn;
    cxGrid1DBTableView1NAZIV_OPERACIJA: TcxGridDBColumn;
    cxGrid1DBTableView1SEKVENCA: TcxGridDBColumn;
    cxGrid1DBTableView1BROJ_CASOVI: TcxGridDBColumn;
    cxGrid1DBTableView1BROJ_CIKLUSI: TcxGridDBColumn;
    Label2: TLabel;
    txtSekvenca: TcxDBTextEdit;
    Label3: TLabel;
    Label4: TLabel;
    txtBrojCiklusi: TcxDBTextEdit;
    cbOperacija: TcxDBLookupComboBox;
    cxGrid1DBTableView1ID_OPERACIJA: TcxGridDBColumn;
    Label5: TLabel;
    txtVremenskaME: TcxDBTextEdit;
    cbVremenskaME: TcxDBLookupComboBox;
    txtPotrebnoVreme: TcxDBTextEdit;
    procedure cxDBTextEditAllExit(Sender: TObject);
    procedure SetirajLukap(sender:TObject; tabela:TDataSet; tip:TcxDBTextEdit; sifra:TcxDBTextEdit; lukap:TcxLookupComboBox);
    procedure cxGrid1DBTableView1FocusedRecordChanged(
      Sender: TcxCustomGridTableView; APrevFocusedRecord,
      AFocusedRecord: TcxCustomGridRecord;
      ANewItemRecordFocusingChanged: Boolean);
    procedure cxDBTextEditAllEnter(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure FormCreate(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmOperacijaPR: TfrmOperacijaPR;

implementation

{$R *.dfm}

uses dmKonekcija, dmMaticni, dmResources, dmSystem, dmUnit;

procedure TfrmOperacijaPR.cxDBTextEditAllEnter(Sender: TObject);
begin
  inherited;
//  if ((Sender is TcxDBTextEdit)) then
// begin
//  dm.tblHint.Close;
//  dm.tblHint.ParamByName('tabela').Value:='MAN_PR_OPERACIJA';
//  dm.tblHint.ParamByName('pole').Value:=(Sender as TcxDBTextEdit).DataBinding.DataField;
//  dm.tblHint.Open;
//
//  (Sender as TcxDBTextEdit).Hint:=dm.tblHintRDBDESCRIPTION.AsString;
// end;
end;

procedure TfrmOperacijaPR.cxDBTextEditAllExit(Sender: TObject);
begin
  inherited;
    if (dm.tblOperacijaPR.State) in [dsInsert, dsEdit] then
    begin
//    if ((Sender as TWinControl)= cbNazivArtikal) then
//        begin
//            if (cbNazivArtikal.Text <>'') then
//            begin
//              dm.tblOperacijaPRVID_OPERACIJA.Value := dm.tblArtikalARTVID.Value; //cbPartner.EditValue[0];
//              dm.tblOperacijaPROPERACIJA.Value := dm.tblArtikalID.Value; //cbPartner.EditValue[1];
//            end
//            else
//            begin
//              dm.tblOperacijaPRVID_OPERACIJA.Clear;
//              dm.tblOperacijaPROPERACIJA.Clear;
//            end;
//         end;
//         if ((Sender as TWinControl)= txtArtikal) or ((Sender as TWinControl)= txtVidArtikal) then
//         begin
//             SetirajLukap(sender,dm.tblArtikal,txtVidArtikal,txtArtikal, cbNazivArtikal);
//         end;
     end;
end;

procedure TfrmOperacijaPR.cxGrid1DBTableView1FocusedRecordChanged(
  Sender: TcxCustomGridTableView; APrevFocusedRecord,
  AFocusedRecord: TcxCustomGridRecord; ANewItemRecordFocusingChanged: Boolean);
begin
  inherited;
//  if not (dm.tblOperacijaPR.State in [dsEdit, dsInsert]) then
//  begin
//    cbNazivArtikal.Text := dm.tblOperacijaPRNAZIV_OPERACIJA.Value;
//  end
//  else
//    cbNazivArtikal.Clear;
end;

procedure TfrmOperacijaPR.FormCreate(Sender: TObject);
begin
  inherited;
  dm.tblPR.CloseOpen(True);
  dm.tblOperacija.CloseOpen(True);
end;

procedure TfrmOperacijaPR.FormShow(Sender: TObject);
begin
  inherited;
 // cbNazivArtikal.Text := dm.tblOperacijaPRNAZIV_OPERACIJA.Value;
end;

procedure TfrmOperacijaPR.SetirajLukap(sender:TObject; tabela:TDataSet; tip:TcxDBTextEdit; sifra:TcxDBTextEdit; lukap:TcxLookupComboBox);
begin
         if (tip.Text <>'') and (sifra.Text<>'')  then
         begin
          if(tabela.Locate('ARTVID;ID',VarArrayOf([tip.text,sifra.text]),[])) then
               lukap.Text:=tabela.FieldByName('NAZIV').Value;
         end
         else
         begin
            lukap.Clear;
         end;
end;
end.
