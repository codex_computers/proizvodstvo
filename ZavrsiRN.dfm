object frmZavrsiRN: TfrmZavrsiRN
  Left = 128
  Top = 212
  BorderIcons = [biSystemMenu, biMaximize]
  Caption = #1056#1072#1073#1086#1090#1077#1085' '#1085#1072#1083#1086#1075' '#1079#1072' '#1087#1072#1082#1091#1074#1072#1114#1077
  ClientHeight = 710
  ClientWidth = 1172
  Color = clBtnFace
  Constraints.MinHeight = 500
  Constraints.MinWidth = 610
  Font.Charset = RUSSIAN_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  ShowHint = True
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object dxRibbon1: TdxRibbon
    Left = 0
    Top = 0
    Width = 1172
    Height = 126
    BarManager = dxBarManager1
    ColorSchemeName = 'Blue'
    Contexts = <>
    TabOrder = 0
    TabStop = False
    object dxRibbon1Tab1: TdxRibbonTab
      Active = True
      Caption = #1052#1077#1085#1080
      Groups = <
        item
          ToolbarName = 'dxBarManager1Bar6'
        end
        item
          ToolbarName = 'dxBarManager1Bar1'
        end
        item
          ToolbarName = 'dxBarManager1Bar5'
        end
        item
          ToolbarName = 'dxBarManager1Bar2'
        end
        item
          ToolbarName = 'dxBarManager1Bar3'
        end>
      Index = 0
    end
    object dxRibbon1Tab2: TdxRibbonTab
      Caption = #1055#1086#1076#1077#1089#1091#1074#1072#1114#1072
      Groups = <
        item
          ToolbarName = 'dxBarManager1Bar4'
        end
        item
          ToolbarName = 'dxBarManager1BarIzgledGrid'
        end>
      Index = 1
    end
  end
  object StatusBar1: TdxRibbonStatusBar
    Left = 0
    Top = 687
    Width = 1172
    Height = 23
    Panels = <
      item
        PanelStyleClassName = 'TdxStatusBarTextPanelStyle'
        Text = 
          'F5 - '#1042#1085#1077#1089#1080' '#1079#1072#1074#1088#1096#1077#1085#1072' '#1088#1072#1073#1086#1090#1072' '#1087#1086'  '#1056#1053',   F8 - '#1041#1088#1080#1096#1080' '#1056#1053'/'#1057#1090#1072#1074#1082#1072',   Esc' +
          ' - '#1054#1090#1082#1072#1078#1080' / '#1048#1079#1083#1077#1079
      end>
    Ribbon = dxRibbon1
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clDefault
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
  end
  object Panel1: TPanel
    Left = 0
    Top = 387
    Width = 1172
    Height = 177
    Align = alTop
    Enabled = False
    TabOrder = 2
    object gbRNStavki: TcxGroupBox
      Left = 1
      Top = 1
      Align = alClient
      Caption = '  '#1057#1090#1072#1074#1082#1080' '#1085#1072' '#1056#1053'  '
      TabOrder = 0
      Height = 175
      Width = 1170
      object lblCustom1: TLabel
        Left = 33
        Top = 225
        Width = 42
        Height = 13
        Alignment = taRightJustify
        Caption = 'Custom1'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clBlue
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Visible = False
      end
      object lblCustom2: TLabel
        Left = 33
        Top = 248
        Width = 42
        Height = 13
        Alignment = taRightJustify
        Caption = 'Custom2'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clBlue
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Visible = False
      end
      object lblCustom3: TLabel
        Left = 33
        Top = 270
        Width = 42
        Height = 13
        Alignment = taRightJustify
        Caption = 'Custom3'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clBlue
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Visible = False
      end
      object Label2: TLabel
        Left = 611
        Top = 48
        Width = 56
        Height = 13
        Alignment = taRightJustify
        Caption = #1047#1072#1073#1077#1083#1077#1096#1082#1072
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clBlue
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object cxLabel1: TcxLabel
        Left = 39
        Top = 20
        AutoSize = False
        Caption = #1040#1088#1090#1080#1082#1072#1083' / '#1055#1088#1086#1080#1079#1074#1086#1076
        Style.TextColor = clRed
        Properties.Alignment.Horz = taRightJustify
        Properties.WordWrap = True
        Transparent = True
        Height = 30
        Width = 82
        AnchorX = 121
      end
      object txtVidArtikalS: TcxDBTextEdit
        Tag = 1
        Left = 126
        Top = 24
        Hint = #1042#1080#1076' '#1085#1072' '#1072#1088#1090#1080#1082#1072#1083
        DataBinding.DataField = 'VID_ARTIKAL'
        DataBinding.DataSource = dm.dsRNZavrsiStavka
        ParentFont = False
        TabOrder = 0
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 56
      end
      object txtArtikalS: TcxDBTextEdit
        Tag = 1
        Left = 183
        Top = 24
        Hint = #1064#1080#1092#1088#1072' '#1085#1072' '#1072#1088#1090#1080#1082#1072#1083
        DataBinding.DataField = 'ARTIKAL'
        DataBinding.DataSource = dm.dsRNZavrsiStavka
        ParentFont = False
        TabOrder = 1
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 56
      end
      object cxLabel9: TcxLabel
        Left = 9
        Top = 104
        AutoSize = False
        Caption = #1041#1088#1086#1112' '#1085#1072'  '#1087#1072#1082#1091#1074#1072#1114#1072'/ '#1082#1086#1083#1080#1095#1080#1085#1072
        Style.TextColor = clRed
        Properties.Alignment.Horz = taRightJustify
        Properties.WordWrap = True
        Transparent = True
        OnClick = cxLabel9Click
        Height = 30
        Width = 112
        AnchorX = 121
      end
      object txtKolicinaArtikal: TcxDBTextEdit
        Tag = 1
        Left = 126
        Top = 109
        Hint = #1050#1086#1083#1080#1095#1080#1085#1072' '#1082#1086#1112#1072' '#1090#1088#1077#1073#1072' '#1076#1072' '#1089#1077' '#1085#1072#1087#1090#1072#1074#1080' '#1087#1086' '#1056#1053
        DataBinding.DataField = 'KOLICINA_PAKUVANJE'
        DataBinding.DataSource = dm.dsRNZavrsiStavka
        ParentFont = False
        Style.BorderStyle = ebsUltraFlat
        Style.Font.Charset = RUSSIAN_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -11
        Style.Font.Name = 'Tahoma'
        Style.Font.Style = []
        Style.LookAndFeel.NativeStyle = False
        Style.LookAndFeel.SkinName = ''
        Style.TextStyle = []
        Style.TransparentBorder = False
        Style.IsFontAssigned = True
        StyleDisabled.LookAndFeel.NativeStyle = False
        StyleDisabled.LookAndFeel.SkinName = ''
        StyleFocused.LookAndFeel.NativeStyle = False
        StyleFocused.LookAndFeel.SkinName = ''
        StyleHot.LookAndFeel.NativeStyle = False
        StyleHot.LookAndFeel.SkinName = ''
        TabOrder = 8
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 92
      end
      object cxLabel11: TcxLabel
        Left = 978
        Top = 34
        Caption = #1052#1077#1088#1082#1072
        Style.TextColor = clBlue
        Properties.Alignment.Horz = taRightJustify
        Transparent = True
        Visible = False
        AnchorX = 1014
      end
      object txtMerkaArtikal: TcxDBTextEdit
        Left = 1023
        Top = 34
        Hint = #1045#1076#1080#1085#1080#1094#1072' '#1084#1077#1088#1082#1072
        DataBinding.DataField = 'MERKA'
        ParentFont = False
        Style.BorderStyle = ebsUltraFlat
        Style.Font.Charset = RUSSIAN_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -11
        Style.Font.Name = 'Tahoma'
        Style.Font.Style = []
        Style.LookAndFeel.NativeStyle = False
        Style.LookAndFeel.SkinName = ''
        Style.TextStyle = []
        Style.TransparentBorder = False
        Style.IsFontAssigned = True
        StyleDisabled.LookAndFeel.NativeStyle = False
        StyleDisabled.LookAndFeel.SkinName = ''
        StyleDisabled.TextColor = clWindowText
        StyleFocused.LookAndFeel.NativeStyle = False
        StyleFocused.LookAndFeel.SkinName = ''
        StyleHot.LookAndFeel.NativeStyle = False
        StyleHot.LookAndFeel.SkinName = ''
        TabOrder = 16
        Visible = False
        Width = 56
      end
      object cbMerkaArtikal: TcxDBLookupComboBox
        Left = 1081
        Top = 33
        Hint = #1045#1080#1085#1080#1094#1072' '#1084#1077#1088#1082#1072
        AutoSize = False
        DataBinding.DataField = 'MERKA'
        Properties.KeyFieldNames = 'ID'
        Properties.ListColumns = <
          item
            FieldName = 'NAZIV'
          end>
        Properties.ListSource = dmMat.dsMerka
        TabOrder = 17
        Visible = False
        Height = 21
        Width = 134
      end
      object txtCustom1: TcxDBTextEdit
        Left = 81
        Top = 222
        BeepOnEnter = False
        DataBinding.DataField = 'CUSTOM1'
        Properties.BeepOnError = True
        TabOrder = 22
        Visible = False
        Width = 367
      end
      object txtCustom2: TcxDBTextEdit
        Left = 81
        Top = 245
        BeepOnEnter = False
        DataBinding.DataField = 'CUSTOM2'
        Properties.BeepOnError = True
        TabOrder = 23
        Visible = False
        Width = 367
      end
      object txtCustom3: TcxDBTextEdit
        Left = 81
        Top = 268
        BeepOnEnter = False
        DataBinding.DataField = 'CUSTOM3'
        Properties.BeepOnError = True
        TabOrder = 24
        Visible = False
        Width = 367
      end
      object cxGroupBox5: TcxGroupBox
        Left = 137
        Top = 226
        Caption = '  '#1044#1072#1090#1091#1084' '#1085#1072' '#1088#1077#1072#1083#1080#1079#1072#1094#1080#1112#1072'  '
        Style.TextColor = clBlue
        TabOrder = 36
        Visible = False
        Height = 63
        Width = 329
        object txtReaDatumOd: TcxDBDateEdit
          Left = 42
          Top = 24
          DataBinding.DataField = 'OD_DATUM_REALIZACIJA'
          Properties.Kind = ckDateTime
          TabOrder = 0
          Width = 110
        end
        object txtReaDatumDo: TcxDBDateEdit
          Left = 190
          Top = 24
          DataBinding.DataField = 'DO_DATUM_REALIZACIJA'
          TabOrder = 1
          Width = 110
        end
        object cxLabel18: TcxLabel
          Left = 19
          Top = 26
          Caption = #1054#1076
          Style.TextColor = clBlue
        end
        object cxLabel19: TcxLabel
          Left = 166
          Top = 27
          Caption = #1044#1086
          Style.TextColor = clBlue
        end
      end
      object cxGroupBox6: TcxGroupBox
        Left = 333
        Top = 240
        Caption = '  '#1044#1072#1090#1091#1084' '#1085#1072' '#1080#1089#1087#1086#1088#1072#1082#1072'  '
        Style.TextColor = clBlue
        TabOrder = 41
        Visible = False
        Height = 59
        Width = 330
        object txtIspDatumOd: TcxDBDateEdit
          Left = 42
          Top = 24
          DataBinding.DataField = 'DATUM_OD_ISPORAKA'
          TabOrder = 0
          Width = 126
        end
        object txtIspDatumDo: TcxDBDateEdit
          Left = 203
          Top = 24
          DataBinding.DataField = 'DATUM_DO_ISPORAKA'
          TabOrder = 1
          Width = 110
        end
        object cxLabel20: TcxLabel
          Left = 19
          Top = 26
          Caption = #1054#1076
          Style.TextColor = clBlue
        end
        object cxLabel21: TcxLabel
          Left = 179
          Top = 26
          Caption = #1044#1086
          Style.TextColor = clBlue
        end
      end
      object cxGroupBox1: TcxGroupBox
        Left = 472
        Top = 215
        Caption = '  '#1044#1072#1090#1091#1084'  '
        Style.TextColor = clBlue
        TabOrder = 35
        Visible = False
        Height = 65
        Width = 329
        object txtPlDatumOd: TcxDBDateEdit
          Left = 42
          Top = 24
          DataBinding.DataField = 'OD_DATUM_PLANIRAN'
          Properties.Kind = ckDateTime
          TabOrder = 0
          Width = 110
        end
        object txtPlDatumDo: TcxDBDateEdit
          Left = 190
          Top = 24
          DataBinding.DataField = 'DO_DATUM_PLANIRAN'
          TabOrder = 1
          Width = 110
        end
        object cxLabel2: TcxLabel
          Left = 166
          Top = 26
          Caption = #1044#1086
          Style.TextColor = clBlue
        end
        object cxLabel4: TcxLabel
          Left = 19
          Top = 26
          Caption = #1054#1076
          Style.TextColor = clBlue
        end
      end
      object ImgGolema: TcxDBImage
        Left = 500
        Top = 257
        Hint = #1057#1083#1080#1082#1072' '#1085#1072' '#1072#1088#1090#1080#1082#1083#1086#1090
        DataBinding.DataField = 'SLIKA'
        Properties.ClearKey = 46
        Properties.FitMode = ifmProportionalStretch
        Properties.GraphicClassName = 'TdxSmartImage'
        Properties.GraphicTransparency = gtTransparent
        TabOrder = 27
        Visible = False
        Height = 58
        Width = 77
      end
      object Button1: TButton
        Left = 59
        Top = 295
        Width = 125
        Height = 25
        Hint = 'Bitmap  image |*.bmp;*.dib'
        Caption = '...'
        TabOrder = 28
        TabStop = False
        Visible = False
      end
      object cxLabel22: TcxLabel
        Left = 149
        Top = 361
        Caption = #1055#1086#1088#1072#1095#1082#1072
        ParentFont = False
        Style.Font.Charset = RUSSIAN_CHARSET
        Style.Font.Color = clRed
        Style.Font.Height = -11
        Style.Font.Name = 'Tahoma'
        Style.Font.Style = []
        Style.TextColor = clBlue
        Style.IsFontAssigned = True
        Properties.Alignment.Horz = taRightJustify
        Transparent = True
        Visible = False
        AnchorX = 196
      end
      object txtPorackaS: TcxDBTextEdit
        Left = 218
        Top = 357
        DataBinding.DataField = 'ID_PORACKA'
        ParentFont = False
        Style.BorderStyle = ebsUltraFlat
        Style.Font.Charset = RUSSIAN_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -11
        Style.Font.Name = 'Tahoma'
        Style.Font.Style = []
        Style.LookAndFeel.NativeStyle = False
        Style.LookAndFeel.SkinName = ''
        Style.TextStyle = []
        Style.TransparentBorder = False
        Style.IsFontAssigned = True
        StyleDisabled.LookAndFeel.NativeStyle = False
        StyleDisabled.LookAndFeel.SkinName = ''
        StyleDisabled.TextColor = clWindowText
        StyleFocused.LookAndFeel.NativeStyle = False
        StyleFocused.LookAndFeel.SkinName = ''
        StyleHot.LookAndFeel.NativeStyle = False
        StyleHot.LookAndFeel.SkinName = ''
        TabOrder = 25
        Visible = False
        Width = 56
      end
      object cbPorackaS: TcxDBLookupComboBox
        Left = 276
        Top = 356
        AutoSize = False
        DataBinding.DataField = 'ID_PORACKA'
        Properties.KeyFieldNames = 'ID'
        Properties.ListColumns = <
          item
            FieldName = 'NAZIV'
          end>
        TabOrder = 26
        Visible = False
        Height = 21
        Width = 134
      end
      object txtKraenDatum: TcxDBDateEdit
        Left = 201
        Top = 321
        DataBinding.DataField = 'KRAEN_DATUM_PREVOZ'
        TabOrder = 32
        Visible = False
        Width = 111
      end
      object cxLabel23: TcxLabel
        Left = 71
        Top = 322
        AutoSize = False
        Caption = #1050#1088#1072#1077#1085' '#1076#1072#1090#1091#1084' '#1085#1072' '#1087#1088#1077#1074#1086#1079
        Style.TextColor = clBlue
        StyleFocused.TextColor = clWindowText
        StyleHot.TextColor = clWindowText
        Properties.WordWrap = True
        Transparent = True
        Visible = False
        Height = 20
        Width = 124
      end
      object txtBrCiklusiS: TcxDBTextEdit
        Left = 239
        Top = 382
        DataBinding.DataField = 'BROJ_CIKLUSI'
        TabOrder = 34
        Visible = False
        Width = 131
      end
      object cxLabel24: TcxLabel
        Left = 149
        Top = 382
        Caption = #1041#1088#1086#1112' '#1085#1072' '#1094#1080#1082#1083#1091#1089#1080
        Style.TextColor = clBlue
        Properties.Alignment.Horz = taRightJustify
        Properties.WordWrap = True
        Transparent = True
        Visible = False
        Width = 45
        AnchorX = 194
      end
      object cxLabel25: TcxLabel
        Left = 30
        Top = 354
        Caption = #1041#1088#1086#1112' '#1085#1072' '#1095#1072#1089#1086#1074#1080
        Style.TextColor = clBlue
        Properties.Alignment.Horz = taRightJustify
        Properties.WordWrap = True
        Transparent = True
        Visible = False
        Width = 43
        AnchorX = 73
      end
      object txtBrCasoviS: TcxDBTextEdit
        Left = 30
        Top = 377
        DataBinding.DataField = 'BROJ_CASOVI'
        TabOrder = 33
        Visible = False
        Width = 131
      end
      object cxLabel28: TcxLabel
        Left = 995
        Top = 104
        AutoSize = False
        Caption = #1057#1090#1072#1090#1091#1089
        Style.TextColor = clRed
        Properties.Alignment.Horz = taRightJustify
        Transparent = True
        Visible = False
        Height = 17
        Width = 46
        AnchorX = 1041
      end
      object txtStatusS: TcxDBTextEdit
        Left = 1047
        Top = 103
        DataBinding.DataField = 'STATUS'
        TabOrder = 18
        Visible = False
        Width = 56
      end
      object cbStatusS: TcxDBLookupComboBox
        Left = 1105
        Top = 103
        DataBinding.DataField = 'STATUS'
        Properties.DropDownAutoSize = True
        Properties.DropDownSizeable = True
        Properties.KeyFieldNames = 'VREDNOST'
        Properties.ListColumns = <
          item
            FieldName = 'vrednost'
          end
          item
            FieldName = 'naziv'
          end>
        Properties.ListFieldIndex = 1
        TabOrder = 19
        Visible = False
        Width = 136
      end
      object cxButton1: TcxButton
        Left = 859
        Top = 143
        Width = 75
        Height = 25
        Action = aZapisi
        TabOrder = 20
      end
      object cxButton2: TcxButton
        Left = 940
        Top = 143
        Width = 75
        Height = 25
        Action = aOtkazi
        TabOrder = 21
      end
      object cxLabel37: TcxLabel
        Left = 615
        Top = 120
        AutoSize = False
        Caption = #1054#1076#1086#1073#1088#1080#1083
        ParentFont = False
        Style.Font.Charset = RUSSIAN_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -11
        Style.Font.Name = 'Tahoma'
        Style.Font.Style = []
        Style.TextColor = clBlue
        Style.IsFontAssigned = True
        Properties.Alignment.Horz = taRightJustify
        Properties.WordWrap = True
        Transparent = True
        Height = 20
        Width = 52
        AnchorX = 667
      end
      object txtTipOdobril: TcxDBTextEdit
        Left = 676
        Top = 118
        Hint = #1058#1080#1087' '#1085#1072' '#1055#1072#1088#1090#1085#1077#1088
        DataBinding.DataField = 'TIP_ODOBRIL'
        DataBinding.DataSource = dm.dsRNZavrsiStavka
        ParentFont = False
        TabOrder = 13
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 56
      end
      object txtOdobril: TcxDBTextEdit
        Left = 735
        Top = 118
        Hint = #1064#1080#1092#1088#1072' '#1085#1072' '#1055#1072#1088#1090#1085#1077#1088
        DataBinding.DataField = 'ODOBRIL'
        DataBinding.DataSource = dm.dsRNZavrsiStavka
        ParentFont = False
        TabOrder = 14
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 56
      end
      object cbOdobril: TcxLookupComboBox
        Left = 792
        Top = 118
        Hint = #1053#1072#1079#1080#1074' '#1085#1072' '#1055#1072#1088#1090#1085#1077#1088
        Properties.ClearKey = 46
        Properties.DropDownAutoSize = True
        Properties.DropDownSizeable = True
        Properties.KeyFieldNames = 'tip_partner;id'
        Properties.ListColumns = <
          item
            Width = 40
            FieldName = 'TIP_PARTNER'
          end
          item
            Width = 60
            FieldName = 'id'
          end
          item
            Width = 170
            FieldName = 'naziv'
          end>
        Properties.ListFieldIndex = 2
        Properties.ListOptions.SyncMode = True
        Properties.ListSource = dmMat.dsPartner
        TabOrder = 15
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 223
      end
      object cxLabel39: TcxLabel
        Left = 39
        Top = 81
        AutoSize = False
        Caption = #1055#1072#1082#1091#1074#1072#1114#1077
        Style.TextColor = clBlue
        Properties.Alignment.Horz = taRightJustify
        Transparent = True
        Height = 17
        Width = 82
        AnchorX = 121
      end
      object txtPakuvanje: TcxDBTextEdit
        Left = 126
        Top = 81
        Hint = #1055#1072#1082#1091#1074#1072#1114#1077' '#1085#1072' '#1075#1086#1090#1086#1074#1080#1086#1090' '#1087#1088#1086#1080#1079#1074#1086#1076
        DataBinding.DataField = 'ID_PAKUVANJE'
        DataBinding.DataSource = dm.dsRNZavrsiStavka
        ParentFont = False
        Style.BorderStyle = ebsUltraFlat
        Style.Font.Charset = RUSSIAN_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -11
        Style.Font.Name = 'Tahoma'
        Style.Font.Style = []
        Style.LookAndFeel.NativeStyle = False
        Style.LookAndFeel.SkinName = ''
        Style.TextStyle = []
        Style.TransparentBorder = False
        Style.IsFontAssigned = True
        StyleDisabled.LookAndFeel.NativeStyle = False
        StyleDisabled.LookAndFeel.SkinName = ''
        StyleDisabled.TextColor = clWindowText
        StyleFocused.LookAndFeel.NativeStyle = False
        StyleFocused.LookAndFeel.SkinName = ''
        StyleHot.LookAndFeel.NativeStyle = False
        StyleHot.LookAndFeel.SkinName = ''
        TabOrder = 6
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 56
      end
      object cxLabel42: TcxLabel
        Left = 39
        Top = 55
        AutoSize = False
        Caption = #1040#1084#1073#1072#1083#1072#1078#1072
        Style.TextColor = clBlue
        Properties.Alignment.Horz = taRightJustify
        Properties.WordWrap = True
        Transparent = True
        Height = 17
        Width = 82
        AnchorX = 121
      end
      object txtVidAmbalaza: TcxDBTextEdit
        Left = 126
        Top = 54
        Hint = #1042#1080#1076' '#1085#1072' '#1072#1084#1073#1072#1083#1072#1078#1072
        DataBinding.DataField = 'VID_AMBALAZA'
        DataBinding.DataSource = dm.dsRNZavrsiStavka
        ParentFont = False
        TabOrder = 3
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 56
      end
      object txtAmbalaza: TcxDBTextEdit
        Left = 183
        Top = 54
        Hint = #1064#1080#1092#1088#1072' '#1085#1072' '#1072#1084#1073#1072#1083#1072#1078#1072
        DataBinding.DataField = 'AMBALAZA'
        DataBinding.DataSource = dm.dsRNZavrsiStavka
        ParentFont = False
        TabOrder = 4
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 56
      end
      object cbArtikalS: TcxExtLookupComboBox
        Left = 239
        Top = 24
        RepositoryItem = dmRes.cxNurkoEditRepositoryExtLookupComboBox
        Properties.ClearKey = 46
        Properties.DropDownAutoSize = True
        Properties.DropDownSizeable = True
        TabOrder = 2
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 286
      end
      object cbAmbalaza: TcxExtLookupComboBox
        Left = 239
        Top = 54
        RepositoryItem = dmRes.cxNurkoEditRepositoryExtLookupComboBox
        Properties.ClearKey = 46
        Properties.DropDownAutoSize = True
        Properties.DropDownSizeable = True
        TabOrder = 5
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 286
      end
      object cbPakuvanje: TcxDBLookupComboBox
        Left = 183
        Top = 80
        DataBinding.DataField = 'ID_PAKUVANJE'
        DataBinding.DataSource = dm.dsRNZavrsiStavka
        Properties.DropDownAutoSize = True
        Properties.DropDownSizeable = True
        Properties.KeyFieldNames = 'ID'
        Properties.ListColumns = <
          item
            Caption = #1064#1080#1092#1088#1072
            Width = 20
            FieldName = 'ID'
          end
          item
            Caption = #1048#1079#1074#1077#1076#1077#1085#1072' '#1045#1052
            Width = 20
            FieldName = 'izvedena_em'
          end
          item
            Caption = #1053#1072#1079#1080#1074
            Width = 100
            FieldName = 'naziv'
          end>
        Properties.ListFieldIndex = 2
        Properties.ListSource = dm.dsIzvedenaEM
        TabOrder = 7
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 160
      end
      object txtZabeleska: TcxDBMemo
        Left = 675
        Top = 45
        DataBinding.DataField = 'ZABELESKA'
        DataBinding.DataSource = dm.dsRNZavrsiStavka
        TabOrder = 12
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Height = 63
        Width = 340
      end
      object txtSerija: TcxDBTextEdit
        Left = 675
        Top = 19
        Hint = #1057#1077#1088#1080#1089#1082#1080' '#1085#1072' '#1087#1088#1086#1080#1079#1074#1086#1076#1086#1090
        DataBinding.DataField = 'SERISKI_BROJ'
        DataBinding.DataSource = dm.dsRNZavrsiStavka
        ParentFont = False
        Style.BorderStyle = ebsUltraFlat
        Style.Font.Charset = RUSSIAN_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -11
        Style.Font.Name = 'Tahoma'
        Style.Font.Style = []
        Style.LookAndFeel.NativeStyle = False
        Style.LookAndFeel.SkinName = ''
        Style.TextStyle = []
        Style.TransparentBorder = False
        Style.IsFontAssigned = True
        StyleDisabled.LookAndFeel.NativeStyle = False
        StyleDisabled.LookAndFeel.SkinName = ''
        StyleFocused.LookAndFeel.NativeStyle = False
        StyleFocused.LookAndFeel.SkinName = ''
        StyleHot.LookAndFeel.NativeStyle = False
        StyleHot.LookAndFeel.SkinName = ''
        TabOrder = 11
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 115
      end
      object cxLabel43: TcxLabel
        Left = 629
        Top = 19
        Caption = #1057#1077#1088#1080#1112#1072
        Style.TextColor = clBlue
        Properties.Alignment.Horz = taRightJustify
        Transparent = True
        AnchorX = 667
      end
      object txtKolicinaEdMerka: TcxDBTextEdit
        Tag = 1
        Left = 126
        Top = 134
        Hint = #1050#1086#1083#1080#1095#1080#1085#1072' '#1082#1086#1112#1072' '#1090#1088#1077#1073#1072' '#1076#1072' '#1089#1077' '#1085#1072#1087#1090#1072#1074#1080' '#1087#1086' '#1056#1053
        DataBinding.DataField = 'KOLICINA'
        DataBinding.DataSource = dm.dsRNZavrsiStavka
        ParentFont = False
        Properties.ReadOnly = False
        Style.BorderStyle = ebsUltraFlat
        Style.Font.Charset = RUSSIAN_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -11
        Style.Font.Name = 'Tahoma'
        Style.Font.Style = []
        Style.LookAndFeel.NativeStyle = False
        Style.LookAndFeel.SkinName = ''
        Style.TextStyle = []
        Style.TransparentBorder = False
        Style.IsFontAssigned = True
        StyleDisabled.LookAndFeel.NativeStyle = False
        StyleDisabled.LookAndFeel.SkinName = ''
        StyleFocused.LookAndFeel.NativeStyle = False
        StyleFocused.LookAndFeel.SkinName = ''
        StyleHot.LookAndFeel.NativeStyle = False
        StyleHot.LookAndFeel.SkinName = ''
        TabOrder = 9
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 92
      end
      object cxLabel5: TcxLabel
        Left = 9
        Top = 134
        Hint = #1053#1077#1090#1086' '#1082#1086#1083#1080#1095#1080#1085#1072' '#1086#1076' '#1072#1088#1090#1080#1082#1083#1086#1090
        AutoSize = False
        Caption = #1050#1086#1083#1080#1095#1080#1085#1072'/ '#1085#1077#1090#1086
        Style.TextColor = clRed
        Properties.Alignment.Horz = taRightJustify
        Properties.WordWrap = True
        Transparent = True
        Height = 17
        Width = 112
        AnchorX = 121
      end
      object lblMerkaK: TcxDBLabel
        Left = 218
        Top = 134
        AutoSize = True
        DataBinding.DataField = 'MERKA'
        DataBinding.DataSource = dm.dsRNZavrsiStavka
        Properties.Alignment.Horz = taLeftJustify
        Transparent = True
      end
      object lblMerkaP: TcxDBLabel
        Left = 218
        Top = 110
        AutoSize = True
        DataBinding.DataField = 'MERKA_KOLICINA'
        DataBinding.DataSource = dm.dsRNZavrsiStavka
        Properties.Alignment.Horz = taLeftJustify
        Transparent = True
      end
      object cxLabel3: TcxLabel
        Left = 932
        Top = 56
        Caption = #1056#1086#1082' '#1085#1072' '#1091#1087#1086#1090#1088#1077#1073#1072
        Style.TextColor = clBlue
        Properties.Alignment.Horz = taRightJustify
        Properties.WordWrap = True
        Transparent = True
        Visible = False
        Width = 52
        AnchorX = 984
      end
      object txtRokUpotreba: TcxDBTextEdit
        Left = 995
        Top = 60
        Hint = #1056#1086#1082' '#1085#1072' '#1091#1087#1086#1090#1088#1077#1073#1072' '#1085#1072' '#1087#1088#1086#1080#1079#1074#1086#1076#1086#1090' '#1074#1086' '#1084#1077#1089#1077#1094#1080
        DataBinding.DataField = 'ROK_UPOTREBA'
        DataBinding.DataSource = dm.dsRNZavrsiStavka
        ParentFont = False
        Style.BorderStyle = ebsUltraFlat
        Style.Font.Charset = RUSSIAN_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -11
        Style.Font.Name = 'Tahoma'
        Style.Font.Style = []
        Style.LookAndFeel.NativeStyle = False
        Style.LookAndFeel.SkinName = ''
        Style.TextStyle = []
        Style.TransparentBorder = False
        Style.IsFontAssigned = True
        StyleDisabled.LookAndFeel.NativeStyle = False
        StyleDisabled.LookAndFeel.SkinName = ''
        StyleFocused.LookAndFeel.NativeStyle = False
        StyleFocused.LookAndFeel.SkinName = ''
        StyleHot.LookAndFeel.NativeStyle = False
        StyleHot.LookAndFeel.SkinName = ''
        TabOrder = 10
        Visible = False
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 92
      end
      object cxLabel6: TcxLabel
        Left = 1090
        Top = 61
        Caption = #1084#1077#1089#1077#1094#1080
        Transparent = True
        Visible = False
      end
      object cxLabel10: TcxLabel
        Left = 340
        Top = 132
        Caption = #1041#1088#1086#1112' '#1085#1072' '#1087#1072#1088#1095#1080#1114#1072
        Style.TextColor = clRed
        Properties.Alignment.Horz = taRightJustify
        Properties.WordWrap = True
        Transparent = True
        Width = 88
        AnchorX = 428
      end
      object txtKolicinaBroj: TcxDBTextEdit
        Tag = 1
        Left = 434
        Top = 132
        Hint = #1041#1088#1086#1112' '#1085#1072' '#1087#1072#1088#1095#1080#1114#1072'/ '#1096#1080#1096#1080#1114#1072', '
        DataBinding.DataField = 'KOLICINA_BROJ'
        DataBinding.DataSource = dm.dsRNZavrsiStavka
        ParentFont = False
        Properties.ReadOnly = True
        Style.BorderStyle = ebsUltraFlat
        Style.Font.Charset = RUSSIAN_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -11
        Style.Font.Name = 'Tahoma'
        Style.Font.Style = []
        Style.LookAndFeel.NativeStyle = False
        Style.LookAndFeel.SkinName = ''
        Style.TextStyle = []
        Style.TransparentBorder = False
        Style.IsFontAssigned = True
        StyleDisabled.LookAndFeel.NativeStyle = False
        StyleDisabled.LookAndFeel.SkinName = ''
        StyleFocused.LookAndFeel.NativeStyle = False
        StyleFocused.LookAndFeel.SkinName = ''
        StyleHot.LookAndFeel.NativeStyle = False
        StyleHot.LookAndFeel.SkinName = ''
        TabOrder = 53
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 91
      end
    end
  end
  object dPanel: TPanel
    Left = 0
    Top = 572
    Width = 1172
    Height = 115
    Align = alClient
    TabOrder = 4
    object cxGrid2: TcxGrid
      Left = 1
      Top = 1
      Width = 1170
      Height = 113
      Align = alClient
      TabOrder = 0
      RootLevelOptions.DetailTabsPosition = dtpTop
      object cxGrid2DBTableView1: TcxGridDBTableView
        Navigator.Buttons.CustomButtons = <>
        OnFocusedRecordChanged = cxGrid2DBTableView1FocusedRecordChanged
        DataController.DataSource = dm.dsRNZavrsiStavka
        DataController.Options = [dcoAnsiSort, dcoAssignGroupingValues, dcoAssignMasterDetailKeys, dcoSaveExpanding]
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        FilterRow.InfoText = #1050#1083#1080#1082#1085#1077#1090#1077' '#1090#1091#1082#1072' '#1079#1072' '#1076#1072' '#1076#1077#1092#1080#1085#1080#1088#1072#1090#1077' '#1092#1080#1083#1090#1077#1088
        OptionsBehavior.IncSearch = True
        OptionsCustomize.ColumnsQuickCustomization = True
        OptionsData.Deleting = False
        OptionsData.Editing = False
        OptionsData.Inserting = False
        OptionsView.NoDataToDisplayInfoText = '<'#1053#1077#1084#1072' '#1087#1086#1076#1072#1090#1086#1094#1080'>'
        OptionsView.GroupByBox = False
        object cxGrid2DBTableView1BROJ: TcxGridDBColumn
          Caption = #1041#1088#1086#1112' '#1056#1053
          DataBinding.FieldName = 'BROJ'
          Visible = False
          Width = 70
        end
        object cxGrid2DBTableView1NAZIV_ARTIKAL: TcxGridDBColumn
          Caption = #1055#1088#1086#1080#1079#1074#1086#1076
          DataBinding.FieldName = 'NAZIV_ARTIKAL'
          Width = 209
        end
        object cxGrid2DBTableView1VID_AMBALAZA: TcxGridDBColumn
          Caption = #1042#1080#1076' '#1085#1072' '#1072#1084#1073#1072#1083#1072#1078#1072
          DataBinding.FieldName = 'VID_AMBALAZA'
        end
        object cxGrid2DBTableView1AMBALAZA: TcxGridDBColumn
          Caption = #1064#1080#1092#1088#1072' '#1085#1072' '#1040#1084#1073#1072#1083#1072#1078#1072
          DataBinding.FieldName = 'AMBALAZA'
        end
        object cxGrid2DBTableView1NAZIV_AMBALAZA: TcxGridDBColumn
          Caption = #1040#1084#1073#1072#1083#1072#1078#1072
          DataBinding.FieldName = 'NAZIV_AMBALAZA'
          Width = 136
        end
        object cxGrid2DBTableView1KOLICINA: TcxGridDBColumn
          Caption = #1050#1086#1083#1080#1095#1080#1085#1072
          DataBinding.FieldName = 'KOLICINA'
          Width = 57
        end
        object cxGrid2DBTableView1IZVEDENA_EM: TcxGridDBColumn
          Caption = #1052#1077#1088#1082#1072
          DataBinding.FieldName = 'IZVEDENA_EM'
        end
        object cxGrid2DBTableView1KOLICINA_REALNA: TcxGridDBColumn
          Caption = #1050#1086#1083#1080#1095#1080#1085#1072' '#1074#1086' '#1045#1076'.'#1052#1077#1088#1082#1072' '#1085#1072' '#1087#1088#1086#1080#1079#1074#1086#1076#1086#1090
          DataBinding.FieldName = 'KOLICINA_REALNA'
          Width = 54
        end
        object cxGrid2DBTableView1MERKA: TcxGridDBColumn
          Caption = #1052#1077#1088#1082#1072
          DataBinding.FieldName = 'MERKA'
          Width = 60
        end
        object cxGrid2DBTableView1SERISKI_BROJ: TcxGridDBColumn
          Caption = #1057#1077#1088#1080#1089#1082#1080'/'#1083#1086#1090' '#1073#1088#1086#1112
          DataBinding.FieldName = 'SERISKI_BROJ'
          Width = 96
        end
        object cxGrid2DBTableView1ZABELESKA: TcxGridDBColumn
          Caption = #1047#1072#1073#1077#1083#1077#1096#1082#1072
          DataBinding.FieldName = 'ZABELESKA'
          Width = 150
        end
        object cxGrid2DBTableView1OD_DATUM_PLANIRAN: TcxGridDBColumn
          Caption = #1044#1072#1090#1091#1084
          DataBinding.FieldName = 'OD_DATUM_PLANIRAN'
          Visible = False
          Width = 131
        end
        object cxGrid2DBTableView1VID_ARTIKAL: TcxGridDBColumn
          DataBinding.FieldName = 'VID_ARTIKAL'
          Visible = False
        end
        object cxGrid2DBTableView1ARTIKAL: TcxGridDBColumn
          DataBinding.FieldName = 'ARTIKAL'
          Visible = False
        end
        object cxGrid2DBTableView1ID_RN_OLD: TcxGridDBColumn
          DataBinding.FieldName = 'ID_RN_OLD'
          Visible = False
        end
        object cxGrid2DBTableView1STATUS: TcxGridDBColumn
          DataBinding.FieldName = 'STATUS'
          Visible = False
        end
        object cxGrid2DBTableView1TIP_ODOBRIL: TcxGridDBColumn
          Caption = #1058#1080#1087' '#1086#1076#1086#1073#1088#1080#1083
          DataBinding.FieldName = 'TIP_ODOBRIL'
        end
        object cxGrid2DBTableView1ODOBRIL: TcxGridDBColumn
          Caption = #1064#1080#1092#1088#1072' '#1086#1076#1086#1073#1088#1080#1083
          DataBinding.FieldName = 'ODOBRIL'
        end
        object cxGrid2DBTableView1NAIV_ODOBRIL: TcxGridDBColumn
          Caption = #1054#1076#1086#1073#1088#1080#1083
          DataBinding.FieldName = 'NAIV_ODOBRIL'
          Width = 139
        end
        object cxGrid2DBTableView1ID_PAKUVANJE: TcxGridDBColumn
          Caption = #1064#1080#1092#1088#1072' '#1085#1072' '#1087#1072#1082#1091#1074#1072#1114#1077
          DataBinding.FieldName = 'ID_PAKUVANJE'
          Visible = False
        end
      end
      object cxGrid2Level1: TcxGridLevel
        Caption = #1057#1090#1072#1074#1082#1080' '#1085#1072' '#1056#1072#1073#1086#1090#1085#1080#1086#1090' '#1085#1072#1083#1086#1075
        GridView = cxGrid2DBTableView1
      end
    end
  end
  object cxSplitter2: TcxSplitter
    Left = 0
    Top = 564
    Width = 1172
    Height = 8
    HotZoneClassName = 'TcxMediaPlayer9Style'
    AlignSplitter = salBottom
    AllowHotZoneDrag = False
    Control = Panel1
    ExplicitWidth = 8
  end
  object Panel2: TPanel
    Left = 0
    Top = 126
    Width = 1172
    Height = 261
    Align = alTop
    TabOrder = 5
    object gbPrebaruvanje: TcxGroupBox
      Left = 1
      Top = 1
      Align = alClient
      ParentFont = False
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clWindowText
      Style.Font.Height = -11
      Style.Font.Name = 'Tahoma'
      Style.Font.Style = []
      Style.IsFontAssigned = True
      TabOrder = 0
      OnClick = gbPrebaruvanjeClick
      Height = 259
      Width = 1170
      object Label1: TLabel
        Left = 34
        Top = 125
        Width = 70
        Height = 32
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1040#1088#1090#1080#1082#1072#1083' / '#1055#1088#1086#1080#1079#1074#1086#1076
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        Visible = False
        WordWrap = True
      end
      object cxLabel8: TcxLabel
        Left = 45
        Top = 31
        Caption = #1041#1088#1086#1112' '#1085#1072' '#1056#1053
        ParentFont = False
        Style.Font.Charset = DEFAULT_CHARSET
        Style.Font.Color = clNavy
        Style.Font.Height = -11
        Style.Font.Name = 'Tahoma'
        Style.Font.Style = []
        Style.TextColor = clRed
        Style.IsFontAssigned = True
        Transparent = True
      end
      object cxLabel7: TcxLabel
        Left = 49
        Top = 58
        Caption = #1047#1072' '#1044#1072#1090#1091#1084
        Style.TextColor = clRed
        Transparent = True
      end
      object txtDatum: TcxDateEdit
        Left = 106
        Top = 57
        Properties.Kind = ckDateTime
        TabOrder = 1
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 190
      end
      object cbRN: TcxLookupComboBox
        Tag = 1
        Left = 107
        Top = 30
        Properties.DropDownAutoSize = True
        Properties.DropDownSizeable = True
        Properties.KeyFieldNames = 'ID'
        Properties.ListColumns = <
          item
            Width = 100
            FieldName = 'BROJ'
          end>
        Properties.ListOptions.SyncMode = True
        TabOrder = 0
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 190
      end
      object cxButton3: TcxButton
        Left = 110
        Top = 94
        Width = 162
        Height = 25
        Action = aPonatamu
        TabOrder = 7
      end
      object txtVidArtikal: TcxTextEdit
        Tag = 1
        Left = 109
        Top = 128
        Style.LookAndFeel.NativeStyle = False
        StyleDisabled.LookAndFeel.NativeStyle = False
        StyleFocused.LookAndFeel.NativeStyle = False
        StyleHot.LookAndFeel.NativeStyle = False
        TabOrder = 4
        Visible = False
        Width = 52
      end
      object txtArtikal: TcxTextEdit
        Tag = 1
        Left = 161
        Top = 128
        TabOrder = 5
        Visible = False
        Width = 57
      end
      object cbArtikal: TcxLookupComboBox
        Left = 218
        Top = 128
        Properties.DropDownAutoSize = True
        Properties.DropDownSizeable = True
        Properties.ImmediatePost = True
        Properties.KeyFieldNames = 'ARTVID;id'
        Properties.ListColumns = <
          item
            Caption = #1042#1080#1076' '#1085#1072' '#1072#1088#1090#1080#1082#1072#1083
            Width = 20
            FieldName = 'ARTVID'
          end
          item
            Caption = #1064#1080#1092#1088#1072' '#1085#1072' '#1072#1088#1090#1080#1082#1072#1083
            Width = 20
            FieldName = 'ID'
          end
          item
            Caption = #1040#1088#1090#1080#1082#1072#1083
            Width = 100
            FieldName = 'NAZIV'
          end>
        Properties.ListFieldIndex = 2
        Properties.ListOptions.SyncMode = True
        TabOrder = 6
        Visible = False
        Width = 230
      end
      object cxGrid3: TcxGrid
        Left = 2
        Top = 18
        Width = 1166
        Height = 132
        Align = alTop
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clWindowText
        Font.Height = -15
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 8
        RootLevelOptions.DetailTabsPosition = dtpTop
        object cxGridDBTableView1: TcxGridDBTableView
          Navigator.Buttons.CustomButtons = <>
          OnEditKeyDown = cxGridDBTableView1EditKeyDown
          OnFocusedRecordChanged = cxGridDBTableView1FocusedRecordChanged
          DataController.DataSource = dm.dsRNZavrsi
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          FilterRow.InfoText = #1050#1083#1080#1082#1085#1077#1090#1077' '#1090#1091#1082#1072' '#1079#1072' '#1076#1072' '#1076#1077#1092#1080#1085#1080#1088#1072#1090#1077' '#1092#1080#1083#1090#1077#1088
          OptionsBehavior.IncSearch = True
          OptionsCustomize.ColumnsQuickCustomization = True
          OptionsData.Deleting = False
          OptionsData.Inserting = False
          OptionsSelection.MultiSelect = True
          OptionsSelection.HideFocusRectOnExit = False
          OptionsSelection.HideSelection = True
          OptionsSelection.UnselectFocusedRecordOnExit = False
          OptionsView.NoDataToDisplayInfoText = '<'#1053#1077#1084#1072' '#1087#1086#1076#1072#1090#1086#1094#1080'>'
          OptionsView.GroupByBox = False
          OptionsView.GroupFooters = gfAlwaysVisible
          Preview.Visible = True
          object cxGridDBTableView1STATUS_BOJA: TcxGridDBColumn
            DataBinding.FieldName = 'STATUS_BOJA'
            PropertiesClassName = 'TcxColorComboBoxProperties'
            Properties.Alignment.Horz = taLeftJustify
            Properties.Alignment.Vert = taBottomJustify
            Properties.CustomColors = <>
            Properties.DropDownAutoWidth = False
            Properties.ShowDescriptions = False
            Visible = False
            Options.Editing = False
            Width = 48
            IsCaptionAssigned = True
          end
          object cxGridDBTableView1NAZIV_STATUS: TcxGridDBColumn
            Caption = #1057#1090#1072#1090#1091#1089
            DataBinding.FieldName = 'NAZIV_STATUS'
            Visible = False
            Options.Editing = False
            Width = 99
          end
          object cxGridDBColumn2: TcxGridDBColumn
            Caption = #1044#1072#1090#1091#1084
            DataBinding.FieldName = 'DATUM_PLANIRAN_POCETOK'
            Options.Editing = False
            Width = 139
          end
          object cxGridDBTableView1VID_ARTIKAL: TcxGridDBColumn
            Caption = #1042#1080#1076' '#1085#1072' '#1072#1088#1090#1080#1082#1072#1083
            DataBinding.FieldName = 'VID_ARTIKAL'
            Options.Editing = False
            Width = 35
          end
          object cxGridDBColumn1: TcxGridDBColumn
            Caption = #1041#1088#1086#1112
            DataBinding.FieldName = 'BROJ'
            Options.Editing = False
            Width = 80
          end
          object cxGridDBTableView1ARTIKAL: TcxGridDBColumn
            Caption = #1064#1080#1092#1088#1072' '#1085#1072' '#1072#1088#1090#1080#1082#1072#1083
            DataBinding.FieldName = 'ARTIKAL'
            Options.Editing = False
            Width = 57
          end
          object cxGridDBTableView1NAZIV_ARTIKAL: TcxGridDBColumn
            Caption = #1040#1088#1090#1080#1082#1072#1083' ('#1087#1088#1086#1076#1091#1082#1090')'
            DataBinding.FieldName = 'NAZIV_ARTIKAL'
            Options.Editing = False
            Width = 359
          end
          object cxGridDBTableView1ZABELESKA: TcxGridDBColumn
            Caption = #1047#1072#1073#1077#1083#1077#1096#1082#1072
            DataBinding.FieldName = 'ZABELESKA'
            Width = 330
          end
          object cxGridDBTableView1STATUS_VREDNOST: TcxGridDBColumn
            Caption = #1064#1080#1092#1088#1072' '#1057#1090#1072#1090#1091#1089
            DataBinding.FieldName = 'STATUS_VREDNOST'
            Visible = False
            Options.Editing = False
            Width = 122
          end
          object cxGridDBTableView1DATUM_KRAJ: TcxGridDBColumn
            Caption = #1044#1072#1090#1091#1084' '#1085#1072' '#1079#1072#1090#1074#1086#1088#1072#1114#1077
            DataBinding.FieldName = 'DATUM_KRAJ'
            Options.Editing = False
          end
        end
        object cxGridDBLayoutView1: TcxGridDBLayoutView
          Navigator.Buttons.CustomButtons = <>
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          OptionsBehavior.IncSearch = True
          OptionsData.Deleting = False
          OptionsData.Editing = False
          OptionsData.Inserting = False
          OptionsView.NoDataToDisplayInfoText = '<'#1053#1077#1084#1072' '#1087#1086#1076#1072#1090#1086#1094#1080'>'
          OptionsView.RecordCaption.DisplayMask = '[RecordIndex] '#1086#1076' [RecordCount]'
          OptionsView.RecordCaption.TextAlignmentHorz = taCenter
          OptionsView.SingleRecordStretch = srsHorizontal
          object cxGridDBLayoutViewItem1: TcxGridDBLayoutViewItem
            Caption = #1041#1088#1086#1112' '#1085#1072' '#1056#1053
            DataBinding.FieldName = 'BROJ'
            LayoutItem = cxGridLayoutItem2
          end
          object cxGridDBLayoutViewItem2: TcxGridDBLayoutViewItem
            Caption = #1044#1072#1090#1091#1084
            DataBinding.FieldName = 'DATUM_PLANIRAN_POCETOK'
            LayoutItem = cxGridLayoutItem1
          end
          object cxGridDBLayoutViewItem3: TcxGridDBLayoutViewItem
            Caption = #1047#1072#1073#1077#1083#1077#1096#1082#1072
            DataBinding.FieldName = 'ZABELESKA'
            LayoutItem = cxGridLayoutItem3
          end
          object dxLayoutGroup1: TdxLayoutGroup
            AlignHorz = ahClient
            AlignVert = avTop
            ButtonOptions.Buttons = <>
            Hidden = True
            ShowBorder = False
            Index = -1
          end
          object cxGridLayoutItem1: TcxGridLayoutItem
            Parent = dxLayoutGroup1
            Index = 0
          end
          object cxGridLayoutItem2: TcxGridLayoutItem
            Parent = dxLayoutGroup1
            Index = 1
          end
          object cxGridLayoutItem3: TcxGridLayoutItem
            Parent = dxLayoutGroup1
            Index = 2
          end
        end
        object cxGridLevel1: TcxGridLevel
          Caption = '  '#1056#1072#1073#1086#1090#1085#1080' '#1085#1072#1083#1086#1079#1080' '#1074#1086' '#1055#1088#1086#1080#1079#1074#1086#1076#1089#1090#1074#1086'  '
          GridView = cxGridDBTableView1
        end
      end
      object cxGrid1: TcxGrid
        Left = 2
        Top = 150
        Width = 1166
        Height = 107
        Align = alClient
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 9
        RootLevelOptions.DetailTabsPosition = dtpTop
        object cxGrid1DBTableView1: TcxGridDBTableView
          Navigator.Buttons.CustomButtons = <>
          OnEditKeyDown = cxGridDBTableView1EditKeyDown
          DataController.DataSource = dm.dsZavrsiRn
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          FilterRow.InfoText = #1050#1083#1080#1082#1085#1077#1090#1077' '#1090#1091#1082#1072' '#1079#1072' '#1076#1072' '#1076#1077#1092#1080#1085#1080#1088#1072#1090#1077' '#1092#1080#1083#1090#1077#1088
          OptionsBehavior.IncSearch = True
          OptionsCustomize.ColumnsQuickCustomization = True
          OptionsData.Deleting = False
          OptionsData.Inserting = False
          OptionsSelection.MultiSelect = True
          OptionsSelection.HideFocusRectOnExit = False
          OptionsSelection.HideSelection = True
          OptionsView.NoDataToDisplayInfoText = '<'#1053#1077#1084#1072' '#1087#1086#1076#1072#1090#1086#1094#1080'>'
          OptionsView.GroupByBox = False
          OptionsView.GroupFooters = gfAlwaysVisible
          OptionsView.Indicator = True
          object cxGrid1DBTableView1STATUS_BOJA: TcxGridDBColumn
            DataBinding.FieldName = 'STATUS_BOJA'
            PropertiesClassName = 'TcxColorComboBoxProperties'
            Properties.CustomColors = <>
            Properties.DropDownAutoWidth = False
            Properties.DropDownSizeable = True
            Properties.ShowDescriptions = False
            Options.Editing = False
            Width = 45
            IsCaptionAssigned = True
          end
          object cxGrid1DBTableView1NAZIV_STATUS: TcxGridDBColumn
            Caption = #1057#1090#1072#1090#1091#1089
            DataBinding.FieldName = 'NAZIV_STATUS'
            Options.Editing = False
            Width = 96
          end
          object cxGrid1DBTableView1DATUM_PLANIRAN_POCETOK: TcxGridDBColumn
            Caption = #1044#1072#1090#1091#1084
            DataBinding.FieldName = 'DATUM_PLANIRAN_POCETOK'
            Options.Editing = False
            Width = 140
          end
          object cxGrid1DBTableView1BROJ: TcxGridDBColumn
            Caption = #1041#1088#1086#1112
            DataBinding.FieldName = 'BROJ'
            Options.Editing = False
            Width = 131
          end
          object cxGrid1DBTableView1SOURCE_DOKUMENT: TcxGridDBColumn
            DataBinding.FieldName = 'SOURCE_DOKUMENT'
            Visible = False
            Options.Editing = False
            Width = 74
          end
          object cxGrid1DBTableView1ZABELESKA: TcxGridDBColumn
            Caption = #1047#1072#1073#1077#1083#1077#1096#1082#1072
            DataBinding.FieldName = 'ZABELESKA'
            Width = 333
          end
          object cxGrid1DBTableView1STATUS: TcxGridDBColumn
            Caption = #1057#1090#1072#1090#1091#1089
            DataBinding.FieldName = 'STATUS'
            Visible = False
          end
          object cxGrid1DBTableView1DATUM_KRAJ: TcxGridDBColumn
            Caption = #1044#1072#1090#1091#1084' '#1085#1072' '#1079#1072#1090#1074#1086#1088#1072#1114#1077
            DataBinding.FieldName = 'DATUM_KRAJ'
          end
        end
        object cxGrid1DBLayoutView1: TcxGridDBLayoutView
          Navigator.Buttons.CustomButtons = <>
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          OptionsBehavior.IncSearch = True
          OptionsData.Deleting = False
          OptionsData.Editing = False
          OptionsData.Inserting = False
          OptionsView.NoDataToDisplayInfoText = '<'#1053#1077#1084#1072' '#1087#1086#1076#1072#1090#1086#1094#1080'>'
          OptionsView.RecordCaption.DisplayMask = '[RecordIndex] '#1086#1076' [RecordCount]'
          OptionsView.RecordCaption.TextAlignmentHorz = taCenter
          OptionsView.SingleRecordStretch = srsHorizontal
          object cxGrid1DBLayoutView1BROJ: TcxGridDBLayoutViewItem
            Caption = #1041#1088#1086#1112' '#1085#1072' '#1056#1053
            DataBinding.FieldName = 'BROJ'
            LayoutItem = cxGridLayoutItem4
          end
          object cxGrid1DBLayoutView1DATUM_PLANIRAN_POCETOK: TcxGridDBLayoutViewItem
            Caption = #1044#1072#1090#1091#1084
            DataBinding.FieldName = 'DATUM_PLANIRAN_POCETOK'
            LayoutItem = cxGrid1DBLayoutView1LayoutItem8
          end
          object cxGrid1DBLayoutView1ZABELESKA: TcxGridDBLayoutViewItem
            Caption = #1047#1072#1073#1077#1083#1077#1096#1082#1072
            DataBinding.FieldName = 'ZABELESKA'
            LayoutItem = cxGrid1DBLayoutView1LayoutItem3
          end
          object dxLayoutGroup2: TdxLayoutGroup
            AlignHorz = ahClient
            AlignVert = avTop
            ButtonOptions.Buttons = <>
            Hidden = True
            ShowBorder = False
            Index = -1
          end
          object cxGrid1DBLayoutView1LayoutItem8: TcxGridLayoutItem
            Parent = dxLayoutGroup2
            Index = 0
          end
          object cxGridLayoutItem4: TcxGridLayoutItem
            Parent = dxLayoutGroup2
            Index = 1
          end
          object cxGrid1DBLayoutView1LayoutItem3: TcxGridLayoutItem
            Parent = dxLayoutGroup2
            Index = 2
          end
        end
        object cxGrid1Level1: TcxGridLevel
          Caption = ' '#1056#1072#1073#1086#1090#1077#1085' '#1085#1072#1083#1086#1075' '#1079#1072' '#1055#1072#1082#1091#1074#1072#1114#1077' '
          GridView = cxGrid1DBTableView1
        end
      end
    end
  end
  object cxGridPopupMenu1: TcxGridPopupMenu
    Grid = cxGrid3
    PopupMenus = <>
    Left = 86
    Top = 16
  end
  object PopupMenu1: TPopupMenu
    Left = 121
    Top = 51
    object N1: TMenuItem
      Action = aRN
    end
  end
  object dxBarManager1: TdxBarManager
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Segoe UI'
    Font.Style = []
    Categories.Strings = (
      'Default')
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    ImageOptions.Images = dmRes.cxSmallImages
    ImageOptions.LargeImages = dmRes.cxLargeImages
    PopupMenuLinks = <>
    UseSystemFont = True
    Left = 191
    Top = 16
    PixelsPerInch = 96
    object dxBarManager1Bar1: TdxBar
      Caption = 'Custom 1'
      CaptionButtons = <>
      DockedLeft = 106
      DockedTop = 0
      FloatLeft = 715
      FloatTop = 157
      FloatClientWidth = 51
      FloatClientHeight = 120
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton1'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarLargeButton22'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarLargeButton23'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar2: TdxBar
      Caption = #1040#1082#1094#1080#1080
      CaptionButtons = <>
      DockedLeft = 473
      DockedTop = 0
      FloatLeft = 789
      FloatTop = 120
      FloatClientWidth = 51
      FloatClientHeight = 120
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton10'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton5'
        end
        item
          Visible = True
          ItemName = 'dxBarSubItem1'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar3: TdxBar
      Caption = #1048#1079#1083#1077#1079
      CaptionButtons = <>
      DockedLeft = 718
      DockedTop = 0
      FloatLeft = 718
      FloatTop = 62
      FloatClientWidth = 51
      FloatClientHeight = 80
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton7'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton8'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar4: TdxBar
      Caption = #1055#1077#1095#1072#1090#1077#1114#1077
      CaptionButtons = <>
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 676
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton12'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton15'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton9'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarLargeButton13'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1BarIzgledGrid: TdxBar
      Caption = #1058#1072#1073#1077#1083#1072
      CaptionButtons = <>
      DockedLeft = 363
      DockedTop = 0
      FloatLeft = 808
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLBtnSnimiIzgled'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarLBtnBrisiIzgled'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar5: TdxBar
      Caption = #1057#1090#1072#1074#1082#1072
      CaptionButtons = <>
      DockedLeft = 296
      DockedTop = 0
      FloatLeft = 1212
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton21'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton19'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton20'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar6: TdxBar
      Caption = #1055#1088#1086#1080#1079#1074#1086#1076#1089#1090#1074#1086' '#1056#1053
      CaptionButtons = <>
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 1206
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton18'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarLargeButton2: TdxBarLargeButton
      Action = aAzuriraj
      Category = 0
    end
    object dxBarLargeButton3: TdxBarLargeButton
      Action = aBrisi
      Category = 0
    end
    object dxBarLargeButton4: TdxBarLargeButton
      Action = aSnimiIzgled
      Category = 0
      ScreenTip = tipSnimiIzgled
    end
    object dxBarLargeButton5: TdxBarLargeButton
      Action = aZacuvajExcel
      Category = 0
      ScreenTip = tipZacuvajExcel
    end
    object dxBarLargeButton6: TdxBarLargeButton
      Action = aPecatiTabela
      Caption = #1055#1077#1095#1072#1090#1080' '#1090#1072#1073#1077#1083#1072
      Category = 0
    end
    object dxBarLargeButton7: TdxBarLargeButton
      Action = aHelp
      Category = 0
    end
    object dxBarLargeButton8: TdxBarLargeButton
      Action = aIzlez
      Category = 0
    end
    object dxBarLargeButton1: TdxBarLargeButton
      Action = aRN
      Caption = #1042#1085#1077#1089#1080
      Category = 0
      Hint = #1042#1085#1077#1089#1080' '#1079#1072#1074#1088#1096#1077#1085#1072' '#1088#1072#1073#1086#1090#1072', '#1074#1086' '#1082#1072#1082#1074#1086' '#1087#1072#1082#1091#1074#1072#1114#1077' '#1077
      LargeImageIndex = 50
      SyncImageIndex = False
      ImageIndex = 50
    end
    object dxBarLargeButton9: TdxBarLargeButton
      Action = aSnimiPecatenje
      Category = 0
      ScreenTip = tipSnimiKonf
    end
    object dxBarLargeButton11: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object ribbonFilterReset: TdxBarLargeButton
      Caption = #1056#1077#1089#1077#1090#1080#1088#1072#1112' '#1092#1080#1083#1090#1077#1088
      Category = 0
      Hint = #1056#1077#1089#1077#1090#1080#1088#1072#1112' '#1092#1080#1083#1090#1077#1088
      Visible = ivAlways
    end
    object dxBarLargeButton12: TdxBarLargeButton
      Action = aPodesuvanjePecatenje
      Category = 0
      ScreenTip = tipDizajnReport
    end
    object dxBarLargeButton13: TdxBarLargeButton
      Action = aBrisiPodesuvanjePecatenje
      Category = 0
      ScreenTip = tipBrisiKonf
    end
    object dxBarLargeButton14: TdxBarLargeButton
      Caption = #1048#1079#1073#1088#1080#1096#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      Category = 0
      Visible = ivAlways
      LargeImageIndex = 40
    end
    object dxBarLargeButton15: TdxBarLargeButton
      Action = aPageSetup
      Category = 0
      ScreenTip = tipSetiranjeStrana
    end
    object dxBarLargeButton16: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object dxBarLargeButton17: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object dxBarSubItem1: TdxBarSubItem
      Caption = #1055#1077#1095#1072#1090#1077#1114#1077
      Category = 0
      Visible = ivAlways
      ImageIndex = 30
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton6'
        end>
    end
    object dxBarLBtnSnimiIzgled: TdxBarLargeButton
      Action = aSnimiIzgled
      Category = 0
    end
    object dxBarLBtnBrisiIzgled: TdxBarLargeButton
      Action = aBrisiIzgled
      Category = 0
    end
    object dxBarLargeButton10: TdxBarLargeButton
      Action = aSpustiSoberi
      Category = 0
    end
    object cbSite: TcxBarEditItem
      Action = aSiteStavki
      Caption = #1054#1079#1085#1072#1095#1080' '#1075#1080' '#1089#1080#1090#1077' '#1055#1086#1088#1072#1095#1082#1080
      Category = 0
      ShowCaption = True
      Width = 0
      PropertiesClassName = 'TcxCheckBoxProperties'
      Properties.ImmediatePost = True
      Properties.NullStyle = nssUnchecked
      Properties.OnChange = cbSitePropertiesChange
    end
    object dxBarLargeButton18: TdxBarLargeButton
      Action = aZatvoriRN
      Caption = #1047#1072#1090#1074#1086#1088#1080
      Category = 0
      LargeImageIndex = 88
    end
    object dxBarLargeButton19: TdxBarLargeButton
      Action = aAzuriraj
      Category = 0
    end
    object dxBarLargeButton20: TdxBarLargeButton
      Action = aBrisi
      Category = 0
    end
    object dxBarLargeButton21: TdxBarLargeButton
      Action = aNov
      Caption = #1053#1086#1074#1072
      Category = 0
    end
    object dxBarLargeButton22: TdxBarLargeButton
      Action = aBrisiRN
      Category = 0
    end
    object dxBarLargeButton23: TdxBarLargeButton
      Action = aZatvoriRN
      Category = 0
      LargeImageIndex = 88
      SyncImageIndex = False
      ImageIndex = 88
    end
  end
  object ActionList1: TActionList
    Images = dmRes.cxSmallImages
    Left = 16
    Top = 16
    object aNov: TAction
      Caption = #1053#1086#1074
      ImageIndex = 10
      OnExecute = aNovExecute
    end
    object aAzuriraj: TAction
      Caption = #1040#1078#1091#1088#1080#1088#1072#1112
      ImageIndex = 12
      ShortCut = 117
      OnExecute = aAzurirajExecute
    end
    object aBrisi: TAction
      Caption = #1041#1088#1080#1096#1080
      ImageIndex = 11
      OnExecute = aBrisiExecute
    end
    object aSnimiIzgled: TAction
      Caption = #1057#1085#1080#1084#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      ImageIndex = 9
      ShortCut = 24659
      OnExecute = aSnimiIzgledExecute
    end
    object aZacuvajExcel: TAction
      Caption = #1047#1072#1095#1091#1074#1072#1112' '#1074#1086' Excel'
      ImageIndex = 9
      ShortCut = 24645
      OnExecute = aZacuvajExcelExecute
    end
    object aPecatiTabela: TAction
      Caption = #1055#1077#1095#1072#1090#1080
      ImageIndex = 30
      OnExecute = aPecatiTabelaExecute
    end
    object aHelp: TAction
      Caption = #1055#1086#1084#1086#1096
      ImageIndex = 24
    end
    object aIzlez: TAction
      Caption = #1048#1079#1083#1077#1079
      ImageIndex = 25
      OnExecute = aIzlezExecute
    end
    object aZapisi: TAction
      Caption = #1047#1072#1087#1080#1096#1080
      ImageIndex = 7
      ShortCut = 120
      OnExecute = aZapisiExecute
    end
    object aOtkazi: TAction
      Caption = #1054#1090#1082#1072#1078#1080
      ImageIndex = 21
      ShortCut = 27
      OnExecute = aOtkaziExecute
    end
    object aRefresh: TAction
      Caption = 'aRefresh'
      ImageIndex = 18
      ShortCut = 118
      OnExecute = aRefreshExecute
    end
    object aSnimiPecatenje: TAction
      Caption = #1057#1085#1080#1084#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      ImageIndex = 39
      OnExecute = aSnimiPecatenjeExecute
    end
    object aPodesuvanjePecatenje: TAction
      Caption = #1044#1080#1079#1072#1112#1085' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090
      ImageIndex = 38
      OnExecute = aPodesuvanjePecatenjeExecute
    end
    object aBrisiPodesuvanjePecatenje: TAction
      Caption = #1041#1088#1080#1096#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      ImageIndex = 37
      OnExecute = aBrisiPodesuvanjePecatenjeExecute
    end
    object aPageSetup: TAction
      Caption = #1057#1077#1090#1080#1088#1072#1114#1077' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072
      ImageIndex = 38
      OnExecute = aPageSetupExecute
    end
    object aFormConfig: TAction
      Caption = 'aFormConfig'
      ShortCut = 24699
      OnExecute = aFormConfigExecute
    end
    object aBrisiIzgled: TAction
      Caption = #1041#1088#1080#1096#1080' '#1075#1086' '#1079#1072#1095#1091#1074#1072#1085#1080#1086#1090' '#1080#1079#1075#1083#1077#1076
      ImageIndex = 56
      OnExecute = aBrisiIzgledExecute
    end
    object aSpustiSoberi: TAction
      Caption = #1057#1087#1091#1096#1090#1080'/'#1057#1086#1073#1077#1088#1080' '#1058#1072#1073#1077#1083#1072
      ImageIndex = 65
      OnExecute = aSpustiSoberiExecute
    end
    object aKreirajRN: TAction
      Caption = #1050#1088#1077#1080#1088#1072#1112' '#1056#1072#1073#1086#1090#1077#1085' '#1085#1072#1083#1086#1075
      ImageIndex = 10
      ShortCut = 116
      OnExecute = aKreirajRNExecute
    end
    object aSiteStavki: TAction
      Caption = 'aSiteStavki'
      OnExecute = aSteStavkiExecute
    end
    object aPonatamu: TAction
      Caption = #1055#1086#1085#1072#1090#1072#1084#1091
      OnExecute = aPonatamuExecute
    end
    object aRN: TAction
      Caption = #1042#1085#1077#1089#1080' '#1079#1072#1074#1088#1096#1077#1085#1072' '#1088#1072#1073#1086#1090#1072
      ShortCut = 116
      OnExecute = aRNExecute
    end
    object aZatvoriRN: TAction
      Caption = #1047#1072#1090#1074#1086#1088#1080' '#1056#1053
      OnExecute = aZatvoriRNExecute
    end
    object aPromeni: TAction
      Caption = 'aPromeni'
    end
    object aBrisiRN: TAction
      Caption = #1041#1088#1080#1096#1080' '#1056#1053
      ImageIndex = 13
      OnExecute = aBrisiRNExecute
    end
    object aZatvori: TAction
      Caption = #1047#1072#1090#1074#1086#1088#1080' '#1056#1053
      ImageIndex = 88
      OnExecute = aZatvoriExecute
    end
    object aB: TAction
      Caption = #1041#1088#1080#1096#1080
      ShortCut = 119
      OnExecute = aBExecute
    end
  end
  object dxComponentPrinter1: TdxComponentPrinter
    CurrentLink = dxComponentPrinter1Link1
    Version = 0
    Left = 16
    Top = 51
    object dxComponentPrinter1Link1: TdxGridReportLink
      Component = cxGrid1
      PrinterPage.DMPaper = 9
      PrinterPage.Footer = 6350
      PrinterPage.GrayShading = True
      PrinterPage.Header = 6350
      PrinterPage.Margins.Bottom = 12700
      PrinterPage.Margins.Left = 12700
      PrinterPage.Margins.Right = 12700
      PrinterPage.Margins.Top = 12700
      PrinterPage.Orientation = poLandscape
      PrinterPage.PageFooter.RightTitle.Strings = (
        #1057#1090#1088'.[Page #] '#1086#1076' [Total Pages]')
      PrinterPage.PageSize.X = 210000
      PrinterPage.PageSize.Y = 297000
      PrinterPage._dxMeasurementUnits_ = 0
      PrinterPage._dxLastMU_ = 2
      ReportDocument.CreationDate = 40336.425359629630000000
      OptionsFormatting.UseNativeStyles = True
      OptionsOnEveryPage.Caption = False
      OptionsOnEveryPage.FilterBar = False
      OptionsView.Caption = False
      OptionsView.FilterBar = False
      StyleRepository = dmRes.cxStyleRepositoryPrinting
      Styles.StyleSheet = dmRes.dxGridReportLinkStyleSheet
      BuiltInReportLink = True
    end
  end
  object dxBarScreenTipRepository1: TdxBarScreenTipRepository
    Left = 226
    Top = 16
    PixelsPerInch = 96
    object tipSnimiIzgled: TdxBarScreenTip
      Header.Text = #1057#1085#1080#1084#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      Description.Text = #1047#1072#1095#1091#1074#1072#1112' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090' '#1085#1072' '#1092#1086#1088#1084#1072#1090#1072' '#1080' '#1085#1072' '#1090#1072#1073#1077#1083#1072#1090#1072
    end
    object tipZacuvajExcel: TdxBarScreenTip
      Header.Text = #1047#1072#1095#1091#1074#1072#1112' '#1074#1086' Excel'
      Description.Text = #1057#1085#1080#1084#1080' '#1075#1080' '#1087#1086#1076#1072#1090#1086#1094#1080#1090#1077' '#1086#1076' '#1090#1072#1073#1077#1083#1072#1090#1072' '#1074#1086' Excel '#1092#1086#1088#1084#1072#1090
    end
    object tipDizajnReport: TdxBarScreenTip
      Header.Text = #1044#1080#1079#1072#1112#1085' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090
      Description.Text = #1055#1086#1076#1077#1089#1091#1074#1072#1114#1072' '#1085#1072' '#1080#1079#1075#1083#1077#1076#1086#1090' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090' '#1085#1072' '#1090#1072#1073#1077#1083#1072#1090#1072
    end
    object tipSetiranjeStrana: TdxBarScreenTip
      Header.Text = #1057#1077#1090#1080#1088#1072#1114#1077' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072
      Description.Text = #1055#1086#1076#1077#1089#1091#1074#1072#1114#1077' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072' '#1085#1072' '#1082#1086#1112#1072' '#1116#1077' '#1089#1077' '#1087#1077#1095#1072#1090#1080' '#1090#1072#1073#1077#1083#1072#1090#1072
    end
    object tipSnimiKonf: TdxBarScreenTip
      Header.Text = #1057#1085#1080#1084#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      Description.Text = #1047#1072#1095#1091#1074#1072#1112' '#1075#1080' '#1087#1086#1076#1077#1089#1091#1074#1072#1114#1072#1090#1072' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090' '#1080' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072' '#1079#1072' '#1087#1077#1095#1072#1090#1077#1114#1077
    end
    object tipBrisiKonf: TdxBarScreenTip
      Header.Text = #1041#1088#1080#1096#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      Description.Text = #1048#1079#1073#1088#1080#1096#1080' '#1075#1080' '#1087#1086#1076#1077#1089#1091#1074#1072#1114#1072#1090#1072' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090' '#1080' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072' '#1079#1072' '#1087#1077#1095#1072#1090#1077#1114#1077
    end
  end
  object tblIzberiRN: TpFIBDataSet
    RefreshSQL.Strings = (
      'select'
      '   rn.id,'
      
        '   rn.broj||'#39'/'#39'||rn.godina||coalesce('#39' '#1079#1072' '#1087#1072#1088#1090#1085#1077#1088#39'||mp.naziv,'#39#39')' +
        ' broj ,'
      '   rn.status,'
      '   rn.zabeleska,'
      '   rn.tip_partner tp_poracka,'
      '   rn.partner p_poracka,'
      '   mp.naziv naziv_partner_poracka,'
      '   rn.datum_planiran_pocetok'
      'from man_raboten_nalog rn'
      
        'left outer join mat_partner mp on mp.id = rn.partner and mp.tip_' +
        'partner = rn.tip_partner'
      
        'where(  rn.status like :status and rn.godina like :godina and rn' +
        '.id_re like :id_re'
      'and rn.source_dokument is null'
      '     ) and (     RN.ID = :OLD_ID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      'select'
      '   rn.id,'
      
        '   rn.broj||'#39'/'#39'||rn.godina||coalesce('#39' '#1079#1072' '#1087#1072#1088#1090#1085#1077#1088#39'||mp.naziv,'#39#39')' +
        ' broj ,'
      '   rn.status,'
      '   rn.zabeleska,'
      '   rn.tip_partner tp_poracka,'
      '   rn.partner p_poracka,'
      '   mp.naziv naziv_partner_poracka,'
      '   rn.datum_planiran_pocetok'
      'from man_raboten_nalog rn'
      
        'left outer join mat_partner mp on mp.id = rn.partner and mp.tip_' +
        'partner = rn.tip_partner'
      
        'where rn.status like :status and rn.godina like :godina and rn.i' +
        'd_re like :id_re'
      'and rn.source_dokument is null')
    AutoUpdateOptions.GeneratorName = 'GEN__ID'
    AutoUpdateOptions.WhenGetGenID = wgBeforePost
    AutoUpdateOptions.AutoParamsToFields = True
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 51
    Top = 51
    object tblIzberiRNBROJ: TFIBStringField
      DisplayLabel = #1041#1088#1086#1112' '#1085#1072' '#1056#1053
      FieldName = 'BROJ'
      Size = 223
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblIzberiRNSTATUS: TFIBIntegerField
      FieldName = 'STATUS'
    end
    object tblIzberiRNNAZIV_PARTNER_PORACKA: TFIBStringField
      DisplayLabel = #1055#1086#1088#1072#1095#1082#1072' - '#1055#1072#1088#1090#1085#1077#1088
      FieldName = 'NAZIV_PARTNER_PORACKA'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblIzberiRNZABELESKA: TFIBStringField
      FieldName = 'ZABELESKA'
      Size = 200
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblIzberiRNTP_PORACKA: TFIBIntegerField
      FieldName = 'TP_PORACKA'
    end
    object tblIzberiRNP_PORACKA: TFIBIntegerField
      FieldName = 'P_PORACKA'
    end
    object tblIzberiRNID: TFIBBCDField
      FieldName = 'ID'
      Size = 0
    end
    object tblIzberiRNDATUM_PLANIRAN_POCETOK: TFIBDateTimeField
      FieldName = 'DATUM_PLANIRAN_POCETOK'
    end
  end
  object dsIzberiRN: TDataSource
    DataSet = tblIzberiRN
    Left = 156
    Top = 16
  end
  object qKojBrojRN: TpFIBQuery
    Transaction = dmKon.qTransaction
    Database = dmKon.fibBaza
    SQL.Strings = (
      'select count(*)+1 broj'
      'from man_raboten_nalog rn'
      'where rn.source_dokument = :id')
    Left = 86
    Top = 51
    qoStartTransaction = True
  end
  object cxGridPopupMenu2: TcxGridPopupMenu
    Grid = cxGrid1
    PopupMenus = <>
    Left = 51
    Top = 16
  end
  object cxGridPopupMenu3: TcxGridPopupMenu
    Grid = cxGrid2
    PopupMenus = <>
    Left = 121
    Top = 16
  end
end
