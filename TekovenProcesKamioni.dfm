object frmTekovenProcesKamioni: TfrmTekovenProcesKamioni
  Left = 293
  Top = -55
  BorderIcons = [biSystemMenu, biMaximize]
  Caption = #1058#1077#1082#1086#1074#1077#1085' '#1087#1088#1086#1094#1077#1089' '#1074#1086' '#1055#1088#1086#1080#1079#1074#1086#1076#1089#1090#1074#1086#1090#1086' ('#1050#1040#1052#1048#1054#1053#1048')'
  ClientHeight = 717
  ClientWidth = 1362
  Color = clBtnFace
  Constraints.MinHeight = 500
  Constraints.MinWidth = 610
  Font.Charset = RUSSIAN_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poDesigned
  ShowHint = True
  WindowState = wsMaximized
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object dxRibbon1: TdxRibbon
    Left = 0
    Top = 0
    Width = 1362
    Height = 126
    BarManager = dxBarManager1
    ColorSchemeName = 'Blue'
    Contexts = <>
    TabOrder = 0
    TabStop = False
    object dxRibbon1Tab1: TdxRibbonTab
      Active = True
      Caption = #1052#1077#1085#1080
      Groups = <
        item
          ToolbarName = 'dxBarManager1Bar3'
        end>
      Index = 0
    end
    object dxRibbon1Tab2: TdxRibbonTab
      Caption = #1055#1086#1076#1077#1089#1091#1074#1072#1114#1072
      Groups = <
        item
          ToolbarName = 'dxBarManager1Bar4'
        end
        item
          ToolbarName = 'dxBarManager1BarIzgledGrid'
        end>
      Index = 1
    end
  end
  object StatusBar1: TdxRibbonStatusBar
    Left = 0
    Top = 694
    Width = 1362
    Height = 23
    Panels = <
      item
        PanelStyleClassName = 'TdxStatusBarTextPanelStyle'
      end>
    Ribbon = dxRibbon1
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clDefault
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
  end
  object PanelMain: TPanel
    Left = 0
    Top = 126
    Width = 1362
    Height = 568
    Align = alClient
    Color = 15790320
    ParentBackground = False
    TabOrder = 2
    object gbPrebaruvanje: TcxGroupBox
      Left = 1
      Top = 1
      Align = alTop
      Caption = '  '#1055#1083#1072#1085' '#1079#1072' '#1050#1072#1084#1080#1086#1085#1080'   '
      ParentFont = False
      Style.Font.Charset = DEFAULT_CHARSET
      Style.Font.Color = clWindowText
      Style.Font.Height = -11
      Style.Font.Name = 'Tahoma'
      Style.Font.Style = []
      Style.IsFontAssigned = True
      TabOrder = 0
      OnClick = gbPrebaruvanjeClick
      Height = 143
      Width = 1360
      object Label14: TLabel
        Left = 31
        Top = 57
        Width = 70
        Height = 17
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1047#1072' '#1082#1091#1087#1091#1074#1072#1095
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        WordWrap = True
      end
      object Label1: TLabel
        Left = 31
        Top = 99
        Width = 70
        Height = 32
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1040#1088#1090#1080#1082#1072#1083' / '#1055#1088#1086#1080#1079#1074#1086#1076
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        WordWrap = True
      end
      object cxLabel8: TcxLabel
        Left = 45
        Top = 31
        Caption = #1050#1072#1084#1080#1086#1085
        ParentFont = False
        Style.Font.Charset = DEFAULT_CHARSET
        Style.Font.Color = clNavy
        Style.Font.Height = -11
        Style.Font.Name = 'Tahoma'
        Style.Font.Style = []
        Style.TextColor = clRed
        Style.IsFontAssigned = True
        Transparent = True
      end
      object cxLabel7: TcxLabel
        Left = 50
        Top = 78
        Caption = #1047#1072' '#1044#1072#1090#1091#1084
        Style.TextColor = clRed
        Transparent = True
      end
      object txtDatum: TcxDateEdit
        Left = 106
        Top = 78
        Properties.Kind = ckDateTime
        TabOrder = 1
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 190
      end
      object cbKamion: TcxLookupComboBox
        Tag = 1
        Left = 107
        Top = 27
        Properties.DropDownSizeable = True
        Properties.KeyFieldNames = 'ID_TRAILER'
        Properties.ListColumns = <
          item
            FieldName = 'NAZIV_TRAILER'
          end>
        Properties.ListOptions.SyncMode = True
        Properties.ListSource = dsIzberiKamioni
        Properties.OnChange = cbRNPropertiesChange
        TabOrder = 0
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 190
      end
      object cxButton3: TcxButton
        Left = 556
        Top = 100
        Width = 162
        Height = 25
        Action = aPogled
        TabOrder = 7
      end
      object txtTP: TcxTextEdit
        Tag = 1
        Left = 106
        Top = 54
        Enabled = False
        Style.LookAndFeel.NativeStyle = False
        StyleDisabled.Color = clWindow
        StyleDisabled.LookAndFeel.NativeStyle = False
        StyleDisabled.TextColor = clWindowText
        StyleFocused.LookAndFeel.NativeStyle = False
        StyleHot.LookAndFeel.NativeStyle = False
        TabOrder = 9
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 52
      end
      object txtP: TcxTextEdit
        Tag = 1
        Left = 158
        Top = 54
        Enabled = False
        StyleDisabled.Color = clWindow
        StyleDisabled.TextColor = clWindowText
        TabOrder = 8
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 57
      end
      object txtVidArtikal: TcxTextEdit
        Tag = 1
        Left = 106
        Top = 102
        Style.LookAndFeel.NativeStyle = False
        StyleDisabled.LookAndFeel.NativeStyle = False
        StyleFocused.LookAndFeel.NativeStyle = False
        StyleHot.LookAndFeel.NativeStyle = False
        TabOrder = 4
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 52
      end
      object txtArtikal: TcxTextEdit
        Tag = 1
        Left = 158
        Top = 102
        TabOrder = 5
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 57
      end
      object cbArtikal: TcxLookupComboBox
        Left = 215
        Top = 102
        ParentShowHint = False
        Properties.DropDownAutoSize = True
        Properties.DropDownSizeable = True
        Properties.ImmediatePost = True
        Properties.KeyFieldNames = 'ARTVID;id'
        Properties.ListColumns = <
          item
            Width = 20
            FieldName = 'ARTVID'
          end
          item
            Width = 20
            FieldName = 'ID'
          end
          item
            Width = 100
            FieldName = 'NAZIV'
          end>
        Properties.ListFieldIndex = 2
        Properties.ListOptions.SyncMode = True
        Properties.ListSource = dsArtikalRN
        ShowHint = True
        TabOrder = 6
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 307
      end
      object cbP: TcxTextEdit
        Left = 215
        Top = 54
        Enabled = False
        StyleDisabled.Color = clWindow
        StyleDisabled.TextColor = clWindowText
        TabOrder = 10
        Width = 307
      end
    end
    object GridPanel1: TGridPanel
      Left = 1
      Top = 144
      Width = 1360
      Height = 423
      Align = alClient
      ColumnCollection = <
        item
          Value = 20.004424455906520000
        end
        item
          Value = 19.998027160467130000
        end
        item
          Value = 19.997720169413850000
        end
        item
          Value = 19.999403817994860000
        end
        item
          Value = 20.000424396217650000
        end>
      ControlCollection = <
        item
          Column = -1
          Row = -1
        end
        item
          Column = 0
          Control = dPanel5
          Row = 0
        end
        item
          Column = 1
          Control = dPanel6
          Row = 0
        end
        item
          Column = 3
          Control = dPanel7
          Row = 0
        end
        item
          Column = 4
          Control = dPanel8
          Row = 0
        end
        item
          Column = 2
          Control = dPanel9
          Row = 0
        end
        item
          Column = 0
          Control = dPanel1
          Row = 1
        end
        item
          Column = 2
          Control = dPanel2
          Row = 1
        end
        item
          Column = 1
          Control = dPanel3
          Row = 1
        end
        item
          Column = 3
          Control = dPanel4
          Row = 1
        end>
      DoubleBuffered = False
      ParentDoubleBuffered = False
      RowCollection = <
        item
          Value = 50.000000000000000000
        end
        item
          Value = 50.000000000000000000
        end>
      TabOrder = 1
      object dPanel5: TPanel
        Left = 1
        Top = 1
        Width = 271
        Height = 200
        Align = alTop
        Anchors = [akLeft, akTop, akRight, akBottom]
        BevelKind = bkFlat
        BevelOuter = bvNone
        Color = 15925247
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentBackground = False
        ParentFont = False
        TabOrder = 0
        object btMasinsko: TcxButton
          Left = 0
          Top = 174
          Width = 267
          Height = 22
          Align = alBottom
          Action = aPoveke
          TabOrder = 0
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object cxlbl16: TcxLabel
          Left = 2
          Top = 41
          AutoSize = False
          Caption = #1042#1082'. '#1073#1088'. '#1085#1072' '#1087#1088#1086#1080#1079#1074#1086#1076#1080' '#1087#1086' '#1050#1072#1084#1080#1086#1085' :'
          ParentColor = False
          ParentFont = False
          Style.BorderStyle = ebsNone
          Style.Color = clBtnFace
          Style.Font.Charset = DEFAULT_CHARSET
          Style.Font.Color = clWindowText
          Style.Font.Height = -11
          Style.Font.Name = 'Tahoma'
          Style.Font.Style = []
          Style.HotTrack = False
          Style.LookAndFeel.NativeStyle = False
          Style.Shadow = False
          Style.TextStyle = [fsBold, fsUnderline]
          Style.IsFontAssigned = True
          StyleDisabled.LookAndFeel.NativeStyle = False
          StyleFocused.LookAndFeel.NativeStyle = False
          StyleHot.LookAndFeel.NativeStyle = False
          Properties.Alignment.Horz = taRightJustify
          Properties.Alignment.Vert = taVCenter
          Transparent = True
          OnClick = cxlbl16Click
          Height = 25
          Width = 197
          AnchorX = 199
          AnchorY = 54
        end
        object cxlbl17: TcxLabel
          Left = 11
          Top = 64
          AutoSize = False
          Caption = '  - '#1055#1086#1084#1080#1085#1072#1090#1080' '#1085#1072' '#1089#1083#1077#1076#1085#1072' '#1086#1073#1088#1072#1073#1086#1090#1082#1072
          ParentColor = False
          Style.BorderStyle = ebsNone
          Style.Color = clBtnFace
          Style.HotTrack = False
          Style.LookAndFeel.NativeStyle = False
          Style.Shadow = False
          Style.TextStyle = []
          StyleDisabled.LookAndFeel.NativeStyle = False
          StyleFocused.LookAndFeel.NativeStyle = False
          StyleHot.LookAndFeel.NativeStyle = False
          Properties.Alignment.Horz = taRightJustify
          Properties.Alignment.Vert = taVCenter
          Transparent = True
          Height = 25
          Width = 188
          AnchorX = 199
          AnchorY = 77
        end
        object cxlbl18: TcxLabel
          Left = 11
          Top = 83
          AutoSize = False
          Caption = '  - '#1054#1089#1090#1072#1085#1072#1090#1080
          ParentColor = False
          Style.BorderStyle = ebsNone
          Style.Color = clBtnFace
          Style.HotTrack = False
          Style.LookAndFeel.NativeStyle = False
          Style.Shadow = False
          Style.TextStyle = []
          StyleDisabled.LookAndFeel.NativeStyle = False
          StyleFocused.LookAndFeel.NativeStyle = False
          StyleHot.LookAndFeel.NativeStyle = False
          Properties.Alignment.Horz = taRightJustify
          Properties.Alignment.Vert = taVCenter
          Transparent = True
          Height = 25
          Width = 188
          AnchorX = 199
          AnchorY = 96
        end
        object txtVkKolicina1: TcxDBTextEdit
          Left = 198
          Top = 45
          DataBinding.DataField = 'VK_KOLICINA1'
          DataBinding.DataSource = dm.dsTekovenProcesKamion
          Enabled = False
          StyleDisabled.BorderColor = clWindowFrame
          StyleDisabled.Color = clWindow
          StyleDisabled.TextColor = clWindowText
          StyleDisabled.TextStyle = []
          TabOrder = 4
          Width = 66
        end
        object txtZavrseni1: TcxDBTextEdit
          Left = 198
          Top = 67
          DataBinding.DataField = 'ZAVRSENI1'
          DataBinding.DataSource = dm.dsTekovenProcesKamion
          Enabled = False
          StyleDisabled.BorderColor = clWindowFrame
          StyleDisabled.Color = clWindow
          StyleDisabled.TextColor = clWindowText
          StyleDisabled.TextStyle = []
          TabOrder = 5
          Width = 66
        end
        object txtOstanati1: TcxDBTextEdit
          Left = 198
          Top = 88
          DataBinding.DataField = 'OSTANATI1'
          DataBinding.DataSource = dm.dsTekovenProcesKamion
          Enabled = False
          StyleDisabled.BorderColor = clWindowFrame
          StyleDisabled.Color = clWindow
          StyleDisabled.TextColor = clWindowText
          StyleDisabled.TextStyle = []
          TabOrder = 6
          Width = 66
        end
        object cxDBLabel1: TcxDBLabel
          Left = 0
          Top = 0
          Align = alTop
          DataBinding.DataField = 'KT_NAZIV1'
          DataBinding.DataSource = dm.dsTekovenProcesKamion
          ParentColor = False
          Properties.Alignment.Horz = taCenter
          Properties.Alignment.Vert = taVCenter
          Style.Color = 12910591
          Style.Shadow = True
          Style.TextStyle = [fsBold]
          Height = 25
          Width = 267
          AnchorX = 134
          AnchorY = 13
        end
      end
      object dPanel6: TPanel
        Left = 272
        Top = 1
        Width = 271
        Height = 200
        Align = alTop
        Anchors = [akLeft, akTop, akRight, akBottom]
        BevelKind = bkFlat
        BevelOuter = bvNone
        Color = 15925247
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentBackground = False
        ParentFont = False
        TabOrder = 1
        object cxlbl19: TcxLabel
          Left = 12
          Top = 41
          AutoSize = False
          Caption = #1042#1082#1091#1087#1077#1085' '#1073#1088#1086#1112' '#1079#1072' '#1086#1073#1088#1072#1073#1086#1090#1082#1072' :'
          ParentColor = False
          Style.BorderStyle = ebsNone
          Style.Color = clBtnFace
          Style.HotTrack = False
          Style.LookAndFeel.NativeStyle = False
          Style.Shadow = False
          Style.TextStyle = [fsBold, fsUnderline]
          StyleDisabled.LookAndFeel.NativeStyle = False
          StyleFocused.LookAndFeel.NativeStyle = False
          StyleHot.LookAndFeel.NativeStyle = False
          Properties.Alignment.Horz = taRightJustify
          Properties.Alignment.Vert = taVCenter
          Transparent = True
          Height = 25
          Width = 187
          AnchorX = 199
          AnchorY = 54
        end
        object cxlbl20: TcxLabel
          Left = 12
          Top = 85
          AutoSize = False
          Caption = '  - '#1055#1086#1084#1080#1085#1072#1090#1080' '#1085#1072' '#1089#1083#1077#1076#1085#1072' '#1086#1073#1088#1072#1073#1086#1090#1082#1072
          ParentColor = False
          Style.BorderStyle = ebsNone
          Style.Color = clBtnFace
          Style.HotTrack = False
          Style.LookAndFeel.NativeStyle = False
          Style.Shadow = False
          Style.TextStyle = []
          StyleDisabled.LookAndFeel.NativeStyle = False
          StyleFocused.LookAndFeel.NativeStyle = False
          StyleHot.LookAndFeel.NativeStyle = False
          Properties.Alignment.Horz = taRightJustify
          Properties.Alignment.Vert = taVCenter
          Transparent = True
          Height = 25
          Width = 187
          AnchorX = 199
          AnchorY = 98
        end
        object cxlbl21: TcxLabel
          Left = 12
          Top = 106
          AutoSize = False
          Caption = '  - '#1054#1089#1090#1072#1085#1072#1090#1080
          ParentColor = False
          Style.BorderStyle = ebsNone
          Style.Color = clBtnFace
          Style.HotTrack = False
          Style.LookAndFeel.NativeStyle = False
          Style.Shadow = False
          Style.TextStyle = []
          StyleDisabled.LookAndFeel.NativeStyle = False
          StyleFocused.LookAndFeel.NativeStyle = False
          StyleHot.LookAndFeel.NativeStyle = False
          Properties.Alignment.Horz = taRightJustify
          Properties.Alignment.Vert = taVCenter
          Transparent = True
          Height = 25
          Width = 187
          AnchorX = 199
          AnchorY = 119
        end
        object btFarbara: TcxButton
          Left = 0
          Top = 174
          Width = 267
          Height = 22
          Align = alBottom
          Action = aPoveke
          TabOrder = 3
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object txtVkKolicina2: TcxDBTextEdit
          Left = 198
          Top = 45
          DataBinding.DataField = 'VK_KOLICINA2'
          DataBinding.DataSource = dm.dsTekovenProcesKamion
          Enabled = False
          StyleDisabled.BorderColor = clWindowFrame
          StyleDisabled.Color = clWindow
          StyleDisabled.TextColor = clWindowText
          TabOrder = 4
          Width = 66
        end
        object txtZavrseni2: TcxDBTextEdit
          Left = 198
          Top = 88
          DataBinding.DataField = 'ZAVRSENI2'
          DataBinding.DataSource = dm.dsTekovenProcesKamion
          Enabled = False
          StyleDisabled.BorderColor = clWindowFrame
          StyleDisabled.Color = clWindow
          StyleDisabled.TextColor = clWindowText
          TabOrder = 5
          Width = 66
        end
        object txtOstanati2: TcxDBTextEdit
          Left = 198
          Top = 109
          DataBinding.DataField = 'OSTANATI2'
          DataBinding.DataSource = dm.dsTekovenProcesKamion
          Enabled = False
          StyleDisabled.BorderColor = clWindowFrame
          StyleDisabled.Color = clWindow
          StyleDisabled.TextColor = clWindowText
          TabOrder = 6
          Width = 66
        end
        object cxDBLabel2: TcxDBLabel
          Left = 0
          Top = 0
          Align = alTop
          DataBinding.DataField = 'KT_NAZIV2'
          DataBinding.DataSource = dm.dsTekovenProcesKamion
          ParentColor = False
          Properties.Alignment.Horz = taCenter
          Properties.Alignment.Vert = taVCenter
          Style.Color = 11064319
          Style.Shadow = True
          Style.TextStyle = [fsBold]
          Height = 25
          Width = 267
          AnchorX = 134
          AnchorY = 13
        end
        object cxlbl22: TcxLabel
          Left = 12
          Top = 64
          AutoSize = False
          Caption = '  - '#1055#1088#1080#1084#1077#1085#1080' '#1086#1076' '#1087#1088#1077#1090#1093#1086#1076#1085#1072' '#1050#1058
          ParentColor = False
          Style.BorderStyle = ebsNone
          Style.Color = clBtnFace
          Style.HotTrack = False
          Style.LookAndFeel.NativeStyle = False
          Style.Shadow = False
          Style.TextStyle = []
          StyleDisabled.LookAndFeel.NativeStyle = False
          StyleFocused.LookAndFeel.NativeStyle = False
          StyleHot.LookAndFeel.NativeStyle = False
          Properties.Alignment.Horz = taRightJustify
          Properties.Alignment.Vert = taVCenter
          Transparent = True
          Height = 25
          Width = 187
          AnchorX = 199
          AnchorY = 77
        end
        object txtPrimeni2: TcxDBTextEdit
          Left = 198
          Top = 67
          DataBinding.DataField = 'PRIMENI2'
          DataBinding.DataSource = dm.dsTekovenProcesKamion
          Enabled = False
          StyleDisabled.BorderColor = clWindowFrame
          StyleDisabled.Color = clWindow
          StyleDisabled.TextColor = clWindowText
          TabOrder = 9
          Width = 66
        end
      end
      object dPanel7: TPanel
        Left = 814
        Top = 1
        Width = 271
        Height = 199
        Align = alTop
        Anchors = [akLeft, akTop, akRight, akBottom]
        BevelKind = bkFlat
        BevelOuter = bvNone
        Color = 15925247
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentBackground = False
        ParentFont = False
        TabOrder = 2
        DesignSize = (
          267
          195)
        object cxButton9: TcxButton
          Left = 35
          Top = 523
          Width = 75
          Height = 19
          Anchors = [akRight, akBottom]
          Caption = #1055#1086#1074#1077#1116#1077' ...'
          TabOrder = 0
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object cxlbl23: TcxLabel
          Left = 9
          Top = 41
          AutoSize = False
          Caption = #1042#1082#1091#1087#1077#1085' '#1073#1088#1086#1112' '#1079#1072' '#1086#1073#1088#1072#1073#1086#1090#1082#1072':'
          ParentColor = False
          Style.BorderStyle = ebsNone
          Style.Color = clBtnFace
          Style.HotTrack = False
          Style.LookAndFeel.NativeStyle = False
          Style.Shadow = False
          Style.TextStyle = [fsBold, fsUnderline]
          StyleDisabled.LookAndFeel.NativeStyle = False
          StyleFocused.LookAndFeel.NativeStyle = False
          StyleHot.LookAndFeel.NativeStyle = False
          Properties.Alignment.Horz = taRightJustify
          Properties.Alignment.Vert = taVCenter
          Transparent = True
          Height = 25
          Width = 188
          AnchorX = 197
          AnchorY = 54
        end
        object cxlbl24: TcxLabel
          Left = 9
          Top = 85
          AutoSize = False
          Caption = '  - '#1047#1072' '#1090#1088#1072#1085#1089#1087#1086#1088#1090
          ParentColor = False
          Style.BorderStyle = ebsNone
          Style.Color = clBtnFace
          Style.HotTrack = False
          Style.LookAndFeel.NativeStyle = False
          Style.Shadow = False
          Style.TextStyle = []
          StyleDisabled.LookAndFeel.NativeStyle = False
          StyleFocused.LookAndFeel.NativeStyle = False
          StyleHot.LookAndFeel.NativeStyle = False
          Properties.Alignment.Horz = taRightJustify
          Properties.Alignment.Vert = taVCenter
          Transparent = True
          Height = 25
          Width = 188
          AnchorX = 197
          AnchorY = 98
        end
        object cxlbl25: TcxLabel
          Left = 9
          Top = 104
          AutoSize = False
          Caption = '  - '#1054#1089#1090#1072#1085#1072#1090#1080
          ParentColor = False
          Style.BorderStyle = ebsNone
          Style.Color = clBtnFace
          Style.HotTrack = False
          Style.LookAndFeel.NativeStyle = False
          Style.Shadow = False
          Style.TextStyle = []
          StyleDisabled.LookAndFeel.NativeStyle = False
          StyleFocused.LookAndFeel.NativeStyle = False
          StyleHot.LookAndFeel.NativeStyle = False
          Properties.Alignment.Horz = taRightJustify
          Properties.Alignment.Vert = taVCenter
          Transparent = True
          Height = 25
          Width = 188
          AnchorX = 197
          AnchorY = 117
        end
        object cxButton10: TcxButton
          Left = 191
          Top = 520
          Width = 75
          Height = 19
          Anchors = [akRight, akBottom]
          Caption = #1055#1086#1074#1077#1116#1077' ...'
          TabOrder = 4
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object cxButton15: TcxButton
          Left = 191
          Top = 284
          Width = 75
          Height = 19
          Anchors = [akRight, akBottom]
          Caption = #1055#1086#1074#1077#1116#1077' ...'
          TabOrder = 5
          Visible = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object txtVkKolicina5: TcxDBTextEdit
          Left = 197
          Top = 45
          DataBinding.DataField = 'VK_KOLICINA5'
          DataBinding.DataSource = dm.dsTekovenProcesKamion
          Enabled = False
          StyleDisabled.BorderColor = clWindowFrame
          StyleDisabled.Color = clWindow
          StyleDisabled.TextColor = clWindowText
          TabOrder = 6
          Width = 66
        end
        object txtZavrrseni5: TcxDBTextEdit
          Left = 197
          Top = 88
          DataBinding.DataField = 'ZAVRSENI5'
          DataBinding.DataSource = dm.dsTekovenProcesKamion
          Enabled = False
          StyleDisabled.BorderColor = clWindowFrame
          StyleDisabled.Color = clWindow
          StyleDisabled.TextColor = clWindowText
          TabOrder = 7
          Width = 66
        end
        object txtOstanati5: TcxDBTextEdit
          Left = 197
          Top = 109
          DataBinding.DataField = 'OSTANATI5'
          DataBinding.DataSource = dm.dsTekovenProcesKamion
          Enabled = False
          StyleDisabled.BorderColor = clWindowFrame
          StyleDisabled.Color = clWindow
          StyleDisabled.TextColor = clWindowText
          TabOrder = 8
          Width = 66
        end
        object cxDBLabel5: TcxDBLabel
          Left = 0
          Top = 0
          Align = alTop
          DataBinding.DataField = 'KT_NAZIV5'
          DataBinding.DataSource = dm.dsTekovenProcesKamion
          ParentColor = False
          Properties.Alignment.Horz = taCenter
          Properties.Alignment.Vert = taVCenter
          Style.Color = 16770995
          Style.Shadow = True
          Style.TextStyle = [fsBold]
          Height = 25
          Width = 267
          AnchorX = 134
          AnchorY = 13
        end
        object cxlbl26: TcxLabel
          Left = 9
          Top = 64
          AutoSize = False
          Caption = '  - '#1055#1088#1080#1084#1077#1085#1080' '#1086#1076' '#1087#1088#1077#1090#1093#1086#1076#1085#1072' '#1050#1058
          ParentColor = False
          Style.BorderStyle = ebsNone
          Style.Color = clBtnFace
          Style.HotTrack = False
          Style.LookAndFeel.NativeStyle = False
          Style.Shadow = False
          Style.TextStyle = []
          StyleDisabled.LookAndFeel.NativeStyle = False
          StyleFocused.LookAndFeel.NativeStyle = False
          StyleHot.LookAndFeel.NativeStyle = False
          Properties.Alignment.Horz = taRightJustify
          Properties.Alignment.Vert = taVCenter
          Transparent = True
          Height = 25
          Width = 188
          AnchorX = 197
          AnchorY = 77
        end
        object txtPrimeni5: TcxDBTextEdit
          Left = 197
          Top = 67
          DataBinding.DataField = 'PRIMENI5'
          DataBinding.DataSource = dm.dsTekovenProcesKamion
          Enabled = False
          StyleDisabled.BorderColor = clWindowFrame
          StyleDisabled.Color = clWindow
          StyleDisabled.TextColor = clWindowText
          TabOrder = 11
          Width = 66
        end
        object btMagGP: TcxButton
          Left = 0
          Top = 173
          Width = 267
          Height = 22
          Align = alBottom
          Action = aPoveke
          TabOrder = 12
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
      end
      object dPanel8: TPanel
        Left = 1085
        Top = 1
        Width = 274
        Height = 199
        Align = alTop
        Anchors = [akLeft, akTop, akRight, akBottom]
        BevelKind = bkFlat
        BevelOuter = bvNone
        Color = 15925247
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentBackground = False
        ParentFont = False
        TabOrder = 3
        DesignSize = (
          270
          195)
        object cxButton5: TcxButton
          Left = 40
          Top = 404
          Width = 75
          Height = 19
          Anchors = [akRight, akBottom]
          Caption = #1055#1086#1074#1077#1116#1077' ...'
          TabOrder = 0
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object cxlbl27: TcxLabel
          Left = 6
          Top = 42
          AutoSize = False
          Caption = #1042#1082#1091#1087#1077#1085' '#1073#1088#1086#1112' '#1079#1072' '#1086#1073#1088#1072#1073#1086#1090#1082#1072': '
          ParentColor = False
          Style.BorderStyle = ebsNone
          Style.Color = clBtnFace
          Style.HotTrack = False
          Style.LookAndFeel.NativeStyle = False
          Style.Shadow = False
          Style.TextStyle = [fsBold, fsUnderline]
          StyleDisabled.LookAndFeel.NativeStyle = False
          StyleFocused.LookAndFeel.NativeStyle = False
          StyleHot.LookAndFeel.NativeStyle = False
          Properties.Alignment.Horz = taRightJustify
          Properties.Alignment.Vert = taVCenter
          Transparent = True
          Height = 25
          Width = 195
          AnchorX = 201
          AnchorY = 55
        end
        object cxlbl28: TcxLabel
          Left = 6
          Top = 64
          AutoSize = False
          Caption = '  - '#1055#1086#1084#1080#1085#1072#1090#1080' '#1085#1072' '#1089#1083#1077#1076#1085#1072' '#1086#1073#1088#1072#1073#1086#1090#1082#1072
          ParentColor = False
          Style.BorderStyle = ebsNone
          Style.Color = clBtnFace
          Style.HotTrack = False
          Style.LookAndFeel.NativeStyle = False
          Style.Shadow = False
          Style.TextStyle = []
          StyleDisabled.LookAndFeel.NativeStyle = False
          StyleFocused.LookAndFeel.NativeStyle = False
          StyleHot.LookAndFeel.NativeStyle = False
          Properties.Alignment.Horz = taRightJustify
          Properties.Alignment.Vert = taVCenter
          Transparent = True
          Height = 25
          Width = 195
          AnchorX = 201
          AnchorY = 77
        end
        object cxlbl29: TcxLabel
          Left = 6
          Top = 83
          AutoSize = False
          Caption = '  - '#1054#1089#1090#1072#1085#1072#1090#1080
          ParentColor = False
          Style.BorderStyle = ebsNone
          Style.Color = clBtnFace
          Style.HotTrack = False
          Style.LookAndFeel.NativeStyle = False
          Style.Shadow = False
          Style.TextStyle = []
          StyleDisabled.LookAndFeel.NativeStyle = False
          StyleFocused.LookAndFeel.NativeStyle = False
          StyleHot.LookAndFeel.NativeStyle = False
          Properties.Alignment.Horz = taRightJustify
          Properties.Alignment.Vert = taVCenter
          Transparent = True
          Height = 25
          Width = 195
          AnchorX = 201
          AnchorY = 96
        end
        object cxButton6: TcxButton
          Left = 196
          Top = 401
          Width = 75
          Height = 19
          Anchors = [akRight, akBottom]
          Caption = #1055#1086#1074#1077#1116#1077' ...'
          TabOrder = 4
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object cxButton13: TcxButton
          Left = 204
          Top = 289
          Width = 75
          Height = 19
          Anchors = [akRight, akBottom]
          Caption = #1055#1086#1074#1077#1116#1077' ...'
          TabOrder = 5
          Visible = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object txtVkKolicina3: TcxDBTextEdit
          Left = 201
          Top = 45
          DataBinding.DataField = 'VK_KOLICINA3'
          DataBinding.DataSource = dm.dsTekovenProcesKamion
          Enabled = False
          StyleDisabled.BorderColor = clWindowFrame
          StyleDisabled.Color = clWindow
          StyleDisabled.TextColor = clWindowText
          TabOrder = 6
          Width = 66
        end
        object txtZavrseni3: TcxDBTextEdit
          Left = 201
          Top = 66
          DataBinding.DataField = 'ZAVRSENI3'
          DataBinding.DataSource = dm.dsTekovenProcesKamion
          Enabled = False
          StyleDisabled.BorderColor = clWindowFrame
          StyleDisabled.Color = clWindow
          StyleDisabled.TextColor = clWindowText
          TabOrder = 7
          Width = 66
        end
        object txtOstanati3: TcxDBTextEdit
          Left = 201
          Top = 87
          DataBinding.DataField = 'OSTANATI3'
          DataBinding.DataSource = dm.dsTekovenProcesKamion
          Enabled = False
          StyleDisabled.BorderColor = clWindowFrame
          StyleDisabled.Color = clWindow
          StyleDisabled.TextColor = clWindowText
          TabOrder = 8
          Width = 66
        end
        object cxDBLabel3: TcxDBLabel
          Left = 0
          Top = 0
          Align = alTop
          DataBinding.DataField = 'KT_NAZIV3'
          DataBinding.DataSource = dm.dsTekovenProcesKamion
          ParentColor = False
          Properties.Alignment.Horz = taCenter
          Properties.Alignment.Vert = taVCenter
          Style.Color = 14796031
          Style.Shadow = True
          Style.TextStyle = [fsBold]
          Height = 25
          Width = 270
          AnchorX = 135
          AnchorY = 13
        end
        object cxlbl30: TcxLabel
          Left = 6
          Top = 124
          AutoSize = False
          Caption = '  - '#1055#1088#1080#1084#1077#1085#1080' '#1086#1076' '#1087#1088#1077#1090#1093#1086#1076#1085#1072' '#1050#1058
          ParentColor = False
          Style.BorderStyle = ebsNone
          Style.Color = clBtnFace
          Style.HotTrack = False
          Style.LookAndFeel.NativeStyle = False
          Style.Shadow = False
          Style.TextStyle = []
          StyleDisabled.LookAndFeel.NativeStyle = False
          StyleFocused.LookAndFeel.NativeStyle = False
          StyleHot.LookAndFeel.NativeStyle = False
          Properties.Alignment.Horz = taRightJustify
          Properties.Alignment.Vert = taVCenter
          Transparent = True
          Visible = False
          Height = 25
          Width = 195
          AnchorX = 201
          AnchorY = 137
        end
        object txtPrimeni31: TcxDBTextEdit
          Left = 201
          Top = 126
          DataBinding.DataField = 'PRIMENI3'
          DataBinding.DataSource = dm.dsTekovenProcesKamion
          TabOrder = 11
          Visible = False
          Width = 66
        end
        object btSivara: TcxButton
          Left = 0
          Top = 173
          Width = 270
          Height = 22
          Align = alBottom
          Action = aPoveke
          TabOrder = 12
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
      end
      object dPanel9: TPanel
        Left = 543
        Top = 1
        Width = 271
        Height = 200
        Align = alTop
        Anchors = [akLeft, akTop, akRight, akBottom]
        BevelKind = bkFlat
        BevelOuter = bvNone
        Color = 15925247
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentBackground = False
        ParentFont = False
        TabOrder = 4
        DesignSize = (
          267
          196)
        object cxButton7: TcxButton
          Left = 35
          Top = 1073
          Width = 75
          Height = 19
          Anchors = [akRight, akBottom]
          Caption = #1055#1086#1074#1077#1116#1077' ...'
          TabOrder = 0
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object cxlbl31: TcxLabel
          Left = 9
          Top = 41
          AutoSize = False
          Caption = #1042#1082#1091#1087#1077#1085' '#1073#1088#1086#1112' '#1079#1072' '#1086#1073#1088#1072#1073#1086#1090#1082#1072':'
          ParentColor = False
          Style.BorderStyle = ebsNone
          Style.Color = clBtnFace
          Style.HotTrack = False
          Style.LookAndFeel.NativeStyle = False
          Style.Shadow = False
          Style.TextStyle = [fsBold, fsUnderline]
          StyleDisabled.LookAndFeel.NativeStyle = False
          StyleFocused.LookAndFeel.NativeStyle = False
          StyleHot.LookAndFeel.NativeStyle = False
          Properties.Alignment.Horz = taRightJustify
          Properties.Alignment.Vert = taVCenter
          Transparent = True
          Height = 25
          Width = 190
          AnchorX = 199
          AnchorY = 54
        end
        object cxlbl32: TcxLabel
          Left = 9
          Top = 106
          AutoSize = False
          Caption = '  - '#1055#1086#1084#1080#1085#1072#1090#1080' '#1085#1072' '#1089#1083#1077#1076#1085#1072' '#1086#1073#1088#1072#1073#1086#1090#1082#1072
          ParentColor = False
          Style.BorderStyle = ebsNone
          Style.Color = clBtnFace
          Style.HotTrack = False
          Style.LookAndFeel.NativeStyle = False
          Style.Shadow = False
          Style.TextStyle = []
          StyleDisabled.LookAndFeel.NativeStyle = False
          StyleFocused.LookAndFeel.NativeStyle = False
          StyleHot.LookAndFeel.NativeStyle = False
          Properties.Alignment.Horz = taRightJustify
          Properties.Alignment.Vert = taVCenter
          Transparent = True
          Height = 25
          Width = 190
          AnchorX = 199
          AnchorY = 119
        end
        object cxlbl33: TcxLabel
          Left = 14
          Top = 126
          AutoSize = False
          Caption = '  - '#1054#1089#1090#1072#1085#1072#1090#1080
          ParentColor = False
          Style.BorderStyle = ebsNone
          Style.Color = clBtnFace
          Style.HotTrack = False
          Style.LookAndFeel.NativeStyle = False
          Style.Shadow = False
          Style.TextStyle = []
          StyleDisabled.LookAndFeel.NativeStyle = False
          StyleFocused.LookAndFeel.NativeStyle = False
          StyleHot.LookAndFeel.NativeStyle = False
          Properties.Alignment.Horz = taRightJustify
          Properties.Alignment.Vert = taVCenter
          Transparent = True
          Height = 25
          Width = 190
          AnchorX = 204
          AnchorY = 139
        end
        object cxButton8: TcxButton
          Left = 191
          Top = 1070
          Width = 75
          Height = 19
          Anchors = [akRight, akBottom]
          Caption = #1055#1086#1074#1077#1116#1077' ...'
          TabOrder = 4
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object cxButton14: TcxButton
          Left = 191
          Top = 858
          Width = 75
          Height = 19
          Anchors = [akRight, akBottom]
          Caption = #1055#1086#1074#1077#1116#1077' ...'
          TabOrder = 5
          Visible = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object txtVkKolicina4: TcxDBTextEdit
          Left = 198
          Top = 45
          DataBinding.DataField = 'VK_KOLICINA4'
          DataBinding.DataSource = dm.dsTekovenProcesKamion
          Enabled = False
          StyleDisabled.BorderColor = clWindowFrame
          StyleDisabled.Color = clWindow
          StyleDisabled.TextColor = clWindowText
          TabOrder = 6
          Width = 66
        end
        object txtZavrseni4: TcxDBTextEdit
          Left = 198
          Top = 109
          DataBinding.DataField = 'ZAVRSENI4'
          DataBinding.DataSource = dm.dsTekovenProcesKamion
          Enabled = False
          StyleDisabled.BorderColor = clWindowFrame
          StyleDisabled.Color = clWindow
          StyleDisabled.TextColor = clWindowText
          TabOrder = 7
          Width = 66
        end
        object txtOstanati4: TcxDBTextEdit
          Left = 198
          Top = 129
          DataBinding.DataField = 'OSTANATI4'
          DataBinding.DataSource = dm.dsTekovenProcesKamion
          Enabled = False
          StyleDisabled.BorderColor = clWindowFrame
          StyleDisabled.Color = clWindow
          StyleDisabled.TextColor = clWindowText
          TabOrder = 8
          Width = 66
        end
        object cxDBLabel4: TcxDBLabel
          Left = 0
          Top = 0
          Align = alTop
          DataBinding.DataField = 'KT_NAZIV4'
          DataBinding.DataSource = dm.dsTekovenProcesKamion
          ParentColor = False
          Properties.Alignment.Horz = taCenter
          Properties.Alignment.Vert = taVCenter
          Style.Color = 13828036
          Style.Shadow = True
          Style.TextStyle = [fsBold]
          Height = 25
          Width = 267
          AnchorX = 134
          AnchorY = 13
        end
        object cxlbl34: TcxLabel
          Left = 13
          Top = 64
          AutoSize = False
          Caption = '  - '#1055#1088#1080#1084#1077#1085#1080' '#1086#1076' '#1064#1048#1042#1040#1056#1040
          ParentColor = False
          Style.BorderStyle = ebsNone
          Style.Color = clBtnFace
          Style.HotTrack = False
          Style.LookAndFeel.NativeStyle = False
          Style.Shadow = False
          Style.TextStyle = []
          StyleDisabled.LookAndFeel.NativeStyle = False
          StyleFocused.LookAndFeel.NativeStyle = False
          StyleHot.LookAndFeel.NativeStyle = False
          Properties.Alignment.Horz = taRightJustify
          Properties.Alignment.Vert = taVCenter
          Transparent = True
          Height = 25
          Width = 186
          AnchorX = 199
          AnchorY = 77
        end
        object txtPrimeni4: TcxDBTextEdit
          Left = 198
          Top = 67
          DataBinding.DataField = 'PRIMENI4'
          DataBinding.DataSource = dm.dsTekovenProcesKamion
          Enabled = False
          StyleDisabled.BorderColor = clWindowFrame
          StyleDisabled.Color = clWindow
          StyleDisabled.TextColor = clWindowText
          TabOrder = 11
          Width = 66
        end
        object cxlbl35: TcxLabel
          Left = 13
          Top = 85
          AutoSize = False
          Caption = '  - '#1055#1088#1080#1084#1077#1085#1080' '#1086#1076' '#1060#1040#1056#1041#1040#1056#1040
          ParentColor = False
          Style.BorderStyle = ebsNone
          Style.Color = clBtnFace
          Style.HotTrack = False
          Style.LookAndFeel.NativeStyle = False
          Style.Shadow = False
          Style.TextStyle = []
          StyleDisabled.LookAndFeel.NativeStyle = False
          StyleFocused.LookAndFeel.NativeStyle = False
          StyleHot.LookAndFeel.NativeStyle = False
          Properties.Alignment.Horz = taRightJustify
          Properties.Alignment.Vert = taVCenter
          Transparent = True
          Height = 25
          Width = 186
          AnchorX = 199
          AnchorY = 98
        end
        object txtPrimeni3: TcxDBTextEdit
          Left = 198
          Top = 88
          DataBinding.DataField = 'PRIMENI3'
          DataBinding.DataSource = dm.dsTekovenProcesKamion
          Enabled = False
          StyleDisabled.BorderColor = clWindowFrame
          StyleDisabled.Color = clWindow
          StyleDisabled.TextColor = clWindowText
          TabOrder = 13
          Width = 66
        end
        object btTapacersko: TcxButton
          Left = 0
          Top = 174
          Width = 267
          Height = 22
          Align = alBottom
          Action = aPoveke
          TabOrder = 14
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
      end
      object dPanel1: TPanel
        Left = 1
        Top = 211
        Width = 271
        Height = 201
        Align = alTop
        Anchors = [akLeft, akTop, akRight, akBottom]
        BevelKind = bkFlat
        BevelOuter = bvNone
        Color = 15925247
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentBackground = False
        ParentFont = False
        TabOrder = 5
        object btMasinskoNapredok: TcxButton
          Left = 0
          Top = 175
          Width = 267
          Height = 22
          Align = alBottom
          Action = aPoveke
          TabOrder = 0
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object cxlbl1: TcxLabel
          Left = 2
          Top = 41
          AutoSize = False
          Caption = #1042#1082'. '#1073#1088'. '#1085#1072' '#1087#1088#1086#1080#1079#1074#1086#1076#1080' '#1087#1086' '#1050#1072#1084#1080#1086#1085' :'
          ParentColor = False
          ParentFont = False
          Style.BorderStyle = ebsNone
          Style.Color = clBtnFace
          Style.Font.Charset = DEFAULT_CHARSET
          Style.Font.Color = clWindowText
          Style.Font.Height = -11
          Style.Font.Name = 'Tahoma'
          Style.Font.Style = []
          Style.HotTrack = False
          Style.LookAndFeel.NativeStyle = False
          Style.Shadow = False
          Style.TextStyle = [fsBold, fsUnderline]
          Style.IsFontAssigned = True
          StyleDisabled.LookAndFeel.NativeStyle = False
          StyleFocused.LookAndFeel.NativeStyle = False
          StyleHot.LookAndFeel.NativeStyle = False
          Properties.Alignment.Horz = taRightJustify
          Properties.Alignment.Vert = taVCenter
          Transparent = True
          Height = 25
          Width = 197
          AnchorX = 199
          AnchorY = 54
        end
        object cxlbl2: TcxLabel
          Left = 11
          Top = 64
          AutoSize = False
          Caption = '  - '#1055#1086#1084#1080#1085#1072#1090#1080' '#1085#1072' '#1089#1083#1077#1076#1085#1072' '#1086#1073#1088#1072#1073#1086#1090#1082#1072
          ParentColor = False
          Style.BorderStyle = ebsNone
          Style.Color = clBtnFace
          Style.HotTrack = False
          Style.LookAndFeel.NativeStyle = False
          Style.Shadow = False
          Style.TextStyle = []
          StyleDisabled.LookAndFeel.NativeStyle = False
          StyleFocused.LookAndFeel.NativeStyle = False
          StyleHot.LookAndFeel.NativeStyle = False
          Properties.Alignment.Horz = taRightJustify
          Properties.Alignment.Vert = taVCenter
          Transparent = True
          Height = 25
          Width = 188
          AnchorX = 199
          AnchorY = 77
        end
        object cxlbl3: TcxLabel
          Left = 11
          Top = 83
          AutoSize = False
          Caption = '  - '#1054#1089#1090#1072#1085#1072#1090#1080
          ParentColor = False
          Style.BorderStyle = ebsNone
          Style.Color = clBtnFace
          Style.HotTrack = False
          Style.LookAndFeel.NativeStyle = False
          Style.Shadow = False
          Style.TextStyle = []
          StyleDisabled.LookAndFeel.NativeStyle = False
          StyleFocused.LookAndFeel.NativeStyle = False
          StyleHot.LookAndFeel.NativeStyle = False
          Properties.Alignment.Horz = taRightJustify
          Properties.Alignment.Vert = taVCenter
          Transparent = True
          Height = 25
          Width = 188
          AnchorX = 199
          AnchorY = 96
        end
        object txtVkKolicina6: TcxDBTextEdit
          Left = 198
          Top = 45
          DataBinding.DataField = 'VK_KOLICINA6'
          DataBinding.DataSource = dm.dsTekovenProcesKamion
          Enabled = False
          StyleDisabled.BorderColor = clWindowFrame
          StyleDisabled.Color = clWindow
          StyleDisabled.TextColor = clWindowText
          StyleDisabled.TextStyle = []
          TabOrder = 4
          Width = 66
        end
        object txtZavrseni6: TcxDBTextEdit
          Left = 198
          Top = 67
          DataBinding.DataField = 'ZAVRSENI6'
          DataBinding.DataSource = dm.dsTekovenProcesKamion
          Enabled = False
          StyleDisabled.BorderColor = clWindowFrame
          StyleDisabled.Color = clWindow
          StyleDisabled.TextColor = clWindowText
          StyleDisabled.TextStyle = []
          TabOrder = 5
          Width = 66
        end
        object txtOstanati6: TcxDBTextEdit
          Left = 198
          Top = 88
          DataBinding.DataField = 'OSTANATI6'
          DataBinding.DataSource = dm.dsTekovenProcesKamion
          Enabled = False
          StyleDisabled.BorderColor = clWindowFrame
          StyleDisabled.Color = clWindow
          StyleDisabled.TextColor = clWindowText
          StyleDisabled.TextStyle = []
          TabOrder = 6
          Width = 66
        end
        object cxDBLabel6: TcxDBLabel
          Left = 0
          Top = 0
          Align = alTop
          DataBinding.DataField = 'KT_NAZIV6'
          DataBinding.DataSource = dm.dsTekovenProcesKamion
          ParentColor = False
          Properties.Alignment.Horz = taCenter
          Properties.Alignment.Vert = taVCenter
          Style.Color = 12910591
          Style.Shadow = True
          Style.TextStyle = [fsBold]
          Height = 25
          Width = 267
          AnchorX = 134
          AnchorY = 13
        end
      end
      object dPanel2: TPanel
        Left = 543
        Top = 211
        Width = 271
        Height = 201
        Align = alTop
        Anchors = [akLeft, akTop, akRight, akBottom]
        BevelKind = bkFlat
        BevelOuter = bvNone
        Color = 15925247
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentBackground = False
        ParentFont = False
        TabOrder = 6
        DesignSize = (
          267
          197)
        object btn1: TcxButton
          Left = 35
          Top = 327
          Width = 75
          Height = 19
          Anchors = [akRight, akBottom]
          Caption = #1055#1086#1074#1077#1116#1077' ...'
          TabOrder = 0
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object cxlbl4: TcxLabel
          Left = 9
          Top = 41
          AutoSize = False
          Caption = #1042#1082#1091#1087#1077#1085' '#1073#1088#1086#1112' '#1079#1072' '#1086#1073#1088#1072#1073#1086#1090#1082#1072' :'
          ParentColor = False
          Style.BorderStyle = ebsNone
          Style.Color = clBtnFace
          Style.HotTrack = False
          Style.LookAndFeel.NativeStyle = False
          Style.Shadow = False
          Style.TextStyle = [fsBold, fsUnderline]
          StyleDisabled.LookAndFeel.NativeStyle = False
          StyleFocused.LookAndFeel.NativeStyle = False
          StyleHot.LookAndFeel.NativeStyle = False
          Properties.Alignment.Horz = taRightJustify
          Properties.Alignment.Vert = taVCenter
          Transparent = True
          Height = 25
          Width = 190
          AnchorX = 199
          AnchorY = 54
        end
        object cxlbl5: TcxLabel
          Left = 9
          Top = 85
          AutoSize = False
          Caption = '  - '#1055#1086#1084#1080#1085#1072#1090#1080' '#1085#1072' '#1089#1083#1077#1076#1085#1072' '#1086#1073#1088#1072#1073#1086#1090#1082#1072
          ParentColor = False
          Style.BorderStyle = ebsNone
          Style.Color = clBtnFace
          Style.HotTrack = False
          Style.LookAndFeel.NativeStyle = False
          Style.Shadow = False
          Style.TextStyle = []
          StyleDisabled.LookAndFeel.NativeStyle = False
          StyleFocused.LookAndFeel.NativeStyle = False
          StyleHot.LookAndFeel.NativeStyle = False
          Properties.Alignment.Horz = taRightJustify
          Properties.Alignment.Vert = taVCenter
          Transparent = True
          Height = 25
          Width = 190
          AnchorX = 199
          AnchorY = 98
        end
        object cxlbl6: TcxLabel
          Left = 9
          Top = 106
          AutoSize = False
          Caption = '  - '#1054#1089#1090#1072#1085#1072#1090#1080
          ParentColor = False
          Style.BorderStyle = ebsNone
          Style.Color = clBtnFace
          Style.HotTrack = False
          Style.LookAndFeel.NativeStyle = False
          Style.Shadow = False
          Style.TextStyle = []
          StyleDisabled.LookAndFeel.NativeStyle = False
          StyleFocused.LookAndFeel.NativeStyle = False
          StyleHot.LookAndFeel.NativeStyle = False
          Properties.Alignment.Horz = taRightJustify
          Properties.Alignment.Vert = taVCenter
          Transparent = True
          Height = 25
          Width = 190
          AnchorX = 199
          AnchorY = 119
        end
        object btMebelStip: TcxButton
          Left = 0
          Top = 175
          Width = 267
          Height = 22
          Align = alBottom
          Action = aPoveke
          TabOrder = 4
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object txtVkKolicina8: TcxDBTextEdit
          Left = 198
          Top = 45
          DataBinding.DataField = 'VK_KOLICINA7'
          DataBinding.DataSource = dm.dsTekovenProcesKamion
          Enabled = False
          StyleDisabled.BorderColor = clWindowFrame
          StyleDisabled.Color = clWindow
          StyleDisabled.TextColor = clWindowText
          TabOrder = 5
          Width = 66
        end
        object txtZavrseni8: TcxDBTextEdit
          Left = 198
          Top = 88
          DataBinding.DataField = 'ZAVRSENI7'
          DataBinding.DataSource = dm.dsTekovenProcesKamion
          Enabled = False
          StyleDisabled.BorderColor = clWindowFrame
          StyleDisabled.Color = clWindow
          StyleDisabled.TextColor = clWindowText
          TabOrder = 6
          Width = 66
        end
        object txtOstanati8: TcxDBTextEdit
          Left = 198
          Top = 109
          DataBinding.DataField = 'OSTANATI7'
          DataBinding.DataSource = dm.dsTekovenProcesKamion
          Enabled = False
          StyleDisabled.BorderColor = clWindowFrame
          StyleDisabled.Color = clWindow
          StyleDisabled.TextColor = clWindowText
          TabOrder = 7
          Width = 66
        end
        object cxDBLabel8: TcxDBLabel
          Left = 0
          Top = 0
          Align = alTop
          DataBinding.DataField = 'KT_NAZIV7'
          DataBinding.DataSource = dm.dsTekovenProcesKamion
          ParentColor = False
          Properties.Alignment.Horz = taCenter
          Properties.Alignment.Vert = taVCenter
          Style.Color = 11064319
          Style.Shadow = True
          Style.TextStyle = [fsBold]
          Height = 25
          Width = 267
          AnchorX = 134
          AnchorY = 13
        end
        object cxlbl7: TcxLabel
          Left = 9
          Top = 64
          AutoSize = False
          Caption = '  - '#1055#1088#1080#1084#1077#1085#1080' '#1086#1076' '#1087#1088#1077#1090#1093#1086#1076#1085#1072' '#1050#1058
          ParentColor = False
          Style.BorderStyle = ebsNone
          Style.Color = clBtnFace
          Style.HotTrack = False
          Style.LookAndFeel.NativeStyle = False
          Style.Shadow = False
          Style.TextStyle = []
          StyleDisabled.LookAndFeel.NativeStyle = False
          StyleFocused.LookAndFeel.NativeStyle = False
          StyleHot.LookAndFeel.NativeStyle = False
          Properties.Alignment.Horz = taRightJustify
          Properties.Alignment.Vert = taVCenter
          Transparent = True
          Height = 25
          Width = 190
          AnchorX = 199
          AnchorY = 77
        end
        object txtPrimeni8: TcxDBTextEdit
          Left = 198
          Top = 67
          DataBinding.DataField = 'PRIMENI7'
          DataBinding.DataSource = dm.dsTekovenProcesKamion
          Enabled = False
          StyleDisabled.BorderColor = clWindowFrame
          StyleDisabled.Color = clWindow
          StyleDisabled.TextColor = clWindowText
          TabOrder = 10
          Width = 66
        end
      end
      object dPanel3: TPanel
        Left = 272
        Top = 211
        Width = 271
        Height = 201
        Align = alTop
        Anchors = [akLeft, akTop, akRight, akBottom]
        BevelKind = bkFlat
        BevelOuter = bvNone
        Color = 15925247
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentBackground = False
        ParentFont = False
        TabOrder = 7
        DesignSize = (
          267
          197)
        object btn2: TcxButton
          Left = 34
          Top = 327
          Width = 75
          Height = 19
          Anchors = [akRight, akBottom]
          Caption = #1055#1086#1074#1077#1116#1077' ...'
          TabOrder = 0
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object cxlbl8: TcxLabel
          Left = 9
          Top = 41
          AutoSize = False
          Caption = #1042#1082#1091#1087#1077#1085' '#1073#1088#1086#1112' '#1079#1072' '#1086#1073#1088#1072#1073#1086#1090#1082#1072' :'
          ParentColor = False
          Style.BorderStyle = ebsNone
          Style.Color = clBtnFace
          Style.HotTrack = False
          Style.LookAndFeel.NativeStyle = False
          Style.Shadow = False
          Style.TextStyle = [fsBold, fsUnderline]
          StyleDisabled.LookAndFeel.NativeStyle = False
          StyleFocused.LookAndFeel.NativeStyle = False
          StyleHot.LookAndFeel.NativeStyle = False
          Properties.Alignment.Horz = taRightJustify
          Properties.Alignment.Vert = taVCenter
          Transparent = True
          Height = 25
          Width = 190
          AnchorX = 199
          AnchorY = 54
        end
        object cxlbl9: TcxLabel
          Left = 9
          Top = 85
          AutoSize = False
          Caption = '  - '#1055#1086#1084#1080#1085#1072#1090#1080' '#1085#1072' '#1089#1083#1077#1076#1085#1072' '#1086#1073#1088#1072#1073#1086#1090#1082#1072
          ParentColor = False
          Style.BorderStyle = ebsNone
          Style.Color = clBtnFace
          Style.HotTrack = False
          Style.LookAndFeel.NativeStyle = False
          Style.Shadow = False
          Style.TextStyle = []
          StyleDisabled.LookAndFeel.NativeStyle = False
          StyleFocused.LookAndFeel.NativeStyle = False
          StyleHot.LookAndFeel.NativeStyle = False
          Properties.Alignment.Horz = taRightJustify
          Properties.Alignment.Vert = taVCenter
          Transparent = True
          Height = 25
          Width = 190
          AnchorX = 199
          AnchorY = 98
        end
        object cxlbl10: TcxLabel
          Left = 9
          Top = 106
          AutoSize = False
          Caption = '  - '#1054#1089#1090#1072#1085#1072#1090#1080
          ParentColor = False
          Style.BorderStyle = ebsNone
          Style.Color = clBtnFace
          Style.HotTrack = False
          Style.LookAndFeel.NativeStyle = False
          Style.Shadow = False
          Style.TextStyle = []
          StyleDisabled.LookAndFeel.NativeStyle = False
          StyleFocused.LookAndFeel.NativeStyle = False
          StyleHot.LookAndFeel.NativeStyle = False
          Properties.Alignment.Horz = taRightJustify
          Properties.Alignment.Vert = taVCenter
          Transparent = True
          Height = 25
          Width = 190
          AnchorX = 199
          AnchorY = 119
        end
        object btDorabotkaStip: TcxButton
          Left = 0
          Top = 175
          Width = 267
          Height = 22
          Align = alBottom
          Action = aPoveke
          TabOrder = 4
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object txtVkKolicina7: TcxDBTextEdit
          Left = 198
          Top = 45
          DataBinding.DataField = 'VK_KOLICINA9'
          DataBinding.DataSource = dm.dsTekovenProcesKamion
          Enabled = False
          StyleDisabled.BorderColor = clWindowFrame
          StyleDisabled.Color = clWindow
          StyleDisabled.TextColor = clWindowText
          TabOrder = 5
          Width = 66
        end
        object txtZavrseni7: TcxDBTextEdit
          Left = 198
          Top = 88
          DataBinding.DataField = 'ZAVRSENI9'
          DataBinding.DataSource = dm.dsTekovenProcesKamion
          Enabled = False
          StyleDisabled.BorderColor = clWindowFrame
          StyleDisabled.Color = clWindow
          StyleDisabled.TextColor = clWindowText
          TabOrder = 6
          Width = 66
        end
        object txtOstanati7: TcxDBTextEdit
          Left = 198
          Top = 109
          DataBinding.DataField = 'OSTANATI9'
          DataBinding.DataSource = dm.dsTekovenProcesKamion
          Enabled = False
          StyleDisabled.BorderColor = clWindowFrame
          StyleDisabled.Color = clWindow
          StyleDisabled.TextColor = clWindowText
          TabOrder = 7
          Width = 66
        end
        object cxDBLabel7: TcxDBLabel
          Left = 0
          Top = 0
          Align = alTop
          DataBinding.DataField = 'KT_NAZIV9'
          DataBinding.DataSource = dm.dsTekovenProcesKamion
          ParentColor = False
          Properties.Alignment.Horz = taCenter
          Properties.Alignment.Vert = taVCenter
          Style.Color = 11064319
          Style.Shadow = True
          Style.TextStyle = [fsBold]
          Height = 25
          Width = 267
          AnchorX = 134
          AnchorY = 13
        end
        object cxlbl11: TcxLabel
          Left = 9
          Top = 64
          AutoSize = False
          Caption = '  - '#1055#1088#1080#1084#1077#1085#1080' '#1086#1076' '#1087#1088#1077#1090#1093#1086#1076#1085#1072' '#1050#1058
          ParentColor = False
          Style.BorderStyle = ebsNone
          Style.Color = clBtnFace
          Style.HotTrack = False
          Style.LookAndFeel.NativeStyle = False
          Style.Shadow = False
          Style.TextStyle = []
          StyleDisabled.LookAndFeel.NativeStyle = False
          StyleFocused.LookAndFeel.NativeStyle = False
          StyleHot.LookAndFeel.NativeStyle = False
          Properties.Alignment.Horz = taRightJustify
          Properties.Alignment.Vert = taVCenter
          Transparent = True
          Height = 25
          Width = 190
          AnchorX = 199
          AnchorY = 77
        end
        object txtPrimeni7: TcxDBTextEdit
          Left = 198
          Top = 67
          DataBinding.DataField = 'PRIMENI9'
          DataBinding.DataSource = dm.dsTekovenProcesKamion
          Enabled = False
          StyleDisabled.BorderColor = clWindowFrame
          StyleDisabled.Color = clWindow
          StyleDisabled.TextColor = clWindowText
          TabOrder = 10
          Width = 66
        end
      end
      object dPanel4: TPanel
        Left = 814
        Top = 211
        Width = 271
        Height = 201
        Align = alTop
        Anchors = [akLeft, akTop, akRight, akBottom]
        BevelKind = bkFlat
        BevelOuter = bvNone
        Color = 15925247
        Font.Charset = DEFAULT_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentBackground = False
        ParentFont = False
        TabOrder = 8
        DesignSize = (
          267
          197)
        object btn3: TcxButton
          Left = 35
          Top = 685
          Width = 75
          Height = 19
          Anchors = [akRight, akBottom]
          Caption = #1055#1086#1074#1077#1116#1077' ...'
          TabOrder = 0
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object cxlbl12: TcxLabel
          Left = -1
          Top = 41
          AutoSize = False
          Caption = #1042#1082#1091#1087#1077#1085' '#1073#1088#1086#1112' '#1079#1072' '#1086#1073#1088#1072#1073#1086#1090#1082#1072':'
          ParentColor = False
          Style.BorderStyle = ebsNone
          Style.Color = clBtnFace
          Style.HotTrack = False
          Style.LookAndFeel.NativeStyle = False
          Style.Shadow = False
          Style.TextStyle = [fsBold, fsUnderline]
          StyleDisabled.LookAndFeel.NativeStyle = False
          StyleFocused.LookAndFeel.NativeStyle = False
          StyleHot.LookAndFeel.NativeStyle = False
          Properties.Alignment.Horz = taRightJustify
          Properties.Alignment.Vert = taVCenter
          Transparent = True
          Height = 25
          Width = 198
          AnchorX = 197
          AnchorY = 54
        end
        object cxlbl13: TcxLabel
          Left = -1
          Top = 85
          AutoSize = False
          Caption = '  - '#1047#1072' '#1090#1088#1072#1085#1089#1087#1086#1088#1090
          ParentColor = False
          Style.BorderStyle = ebsNone
          Style.Color = clBtnFace
          Style.HotTrack = False
          Style.LookAndFeel.NativeStyle = False
          Style.Shadow = False
          Style.TextStyle = []
          StyleDisabled.LookAndFeel.NativeStyle = False
          StyleFocused.LookAndFeel.NativeStyle = False
          StyleHot.LookAndFeel.NativeStyle = False
          Properties.Alignment.Horz = taRightJustify
          Properties.Alignment.Vert = taVCenter
          Transparent = True
          Height = 25
          Width = 198
          AnchorX = 197
          AnchorY = 98
        end
        object cxlbl14: TcxLabel
          Left = -1
          Top = 104
          AutoSize = False
          Caption = '  - '#1054#1089#1090#1072#1085#1072#1090#1080
          ParentColor = False
          Style.BorderStyle = ebsNone
          Style.Color = clBtnFace
          Style.HotTrack = False
          Style.LookAndFeel.NativeStyle = False
          Style.Shadow = False
          Style.TextStyle = []
          StyleDisabled.LookAndFeel.NativeStyle = False
          StyleFocused.LookAndFeel.NativeStyle = False
          StyleHot.LookAndFeel.NativeStyle = False
          Properties.Alignment.Horz = taRightJustify
          Properties.Alignment.Vert = taVCenter
          Transparent = True
          Height = 25
          Width = 198
          AnchorX = 197
          AnchorY = 117
        end
        object btn4: TcxButton
          Left = 191
          Top = 682
          Width = 75
          Height = 19
          Anchors = [akRight, akBottom]
          Caption = #1055#1086#1074#1077#1116#1077' ...'
          TabOrder = 4
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object btn5: TcxButton
          Left = 191
          Top = 446
          Width = 75
          Height = 19
          Anchors = [akRight, akBottom]
          Caption = #1055#1086#1074#1077#1116#1077' ...'
          TabOrder = 5
          Visible = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object txtVkKolicina9: TcxDBTextEdit
          Left = 197
          Top = 45
          DataBinding.DataField = 'VK_KOLICINA8'
          DataBinding.DataSource = dm.dsTekovenProcesKamion
          Enabled = False
          StyleDisabled.BorderColor = clWindowFrame
          StyleDisabled.Color = clWindow
          StyleDisabled.TextColor = clWindowText
          TabOrder = 6
          Width = 66
        end
        object txtZavrseni9: TcxDBTextEdit
          Left = 197
          Top = 88
          DataBinding.DataField = 'ZAVRSENI8'
          DataBinding.DataSource = dm.dsTekovenProcesKamion
          Enabled = False
          StyleDisabled.BorderColor = clWindowFrame
          StyleDisabled.Color = clWindow
          StyleDisabled.TextColor = clWindowText
          TabOrder = 7
          Width = 66
        end
        object txtOstanati9: TcxDBTextEdit
          Left = 197
          Top = 109
          DataBinding.DataField = 'OSTANATI8'
          DataBinding.DataSource = dm.dsTekovenProcesKamion
          Enabled = False
          StyleDisabled.BorderColor = clWindowFrame
          StyleDisabled.Color = clWindow
          StyleDisabled.TextColor = clWindowText
          TabOrder = 8
          Width = 66
        end
        object cxDBLabel9: TcxDBLabel
          Left = 0
          Top = 0
          Align = alTop
          DataBinding.DataField = 'KT_NAZIV8'
          DataBinding.DataSource = dm.dsTekovenProcesKamion
          ParentColor = False
          Properties.Alignment.Horz = taCenter
          Properties.Alignment.Vert = taVCenter
          Style.Color = 16770995
          Style.Shadow = True
          Style.TextStyle = [fsBold]
          Height = 25
          Width = 267
          AnchorX = 134
          AnchorY = 13
        end
        object cxlbl15: TcxLabel
          Left = -1
          Top = 64
          AutoSize = False
          Caption = '  - '#1055#1088#1080#1084#1077#1085#1080' '#1086#1076' '#1087#1088#1077#1090#1093#1086#1076#1085#1072' '#1050#1058
          ParentColor = False
          Style.BorderStyle = ebsNone
          Style.Color = clBtnFace
          Style.HotTrack = False
          Style.LookAndFeel.NativeStyle = False
          Style.Shadow = False
          Style.TextStyle = []
          StyleDisabled.LookAndFeel.NativeStyle = False
          StyleFocused.LookAndFeel.NativeStyle = False
          StyleHot.LookAndFeel.NativeStyle = False
          Properties.Alignment.Horz = taRightJustify
          Properties.Alignment.Vert = taVCenter
          Transparent = True
          Height = 25
          Width = 198
          AnchorX = 197
          AnchorY = 77
        end
        object txtPrimeni9: TcxDBTextEdit
          Left = 197
          Top = 67
          DataBinding.DataField = 'PRIMENI8'
          DataBinding.DataSource = dm.dsTekovenProcesKamion
          Enabled = False
          StyleDisabled.BorderColor = clWindowFrame
          StyleDisabled.Color = clWindow
          StyleDisabled.TextColor = clWindowText
          TabOrder = 11
          Width = 66
        end
        object btTrgovskaStoka: TcxButton
          Left = 0
          Top = 175
          Width = 267
          Height = 22
          Align = alBottom
          Action = aPoveke
          TabOrder = 12
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
      end
    end
  end
  object cxGridPopupMenu1: TcxGridPopupMenu
    PopupMenus = <>
    Left = 1046
    Top = 16
  end
  object PopupMenu1: TPopupMenu
    Left = 1081
    Top = 51
    object N1: TMenuItem
      Caption = #1057#1085#1080#1084#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      ShortCut = 49235
      OnClick = SaveToIniFileExecute
    end
    object Excel1: TMenuItem
      Caption = #1057#1085#1080#1084#1080' '#1075#1086' '#1074#1086' Excel'
      ShortCut = 49221
    end
  end
  object dxBarManager1: TdxBarManager
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Segoe UI'
    Font.Style = []
    Categories.Strings = (
      'Default')
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    ImageOptions.Images = dmRes.cxSmallImages
    ImageOptions.LargeImages = dmRes.cxLargeImages
    PopupMenuLinks = <>
    UseSystemFont = True
    Left = 1151
    Top = 16
    object dxBarManager1Bar3: TdxBar
      Caption = #1048#1079#1083#1077#1079
      CaptionButtons = <>
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 718
      FloatTop = 62
      FloatClientWidth = 51
      FloatClientHeight = 80
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton7'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton8'
        end>
      OneOnRow = True
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar4: TdxBar
      Caption = #1055#1077#1095#1072#1090#1077#1114#1077
      CaptionButtons = <>
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 676
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton12'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton15'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton9'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarLargeButton13'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1BarIzgledGrid: TdxBar
      Caption = #1058#1072#1073#1077#1083#1072
      CaptionButtons = <>
      DockedLeft = 363
      DockedTop = 0
      FloatLeft = 808
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLBtnSnimiIzgled'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarLBtnBrisiIzgled'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarLargeButton2: TdxBarLargeButton
      Action = aAzuriraj
      Category = 0
    end
    object dxBarLargeButton3: TdxBarLargeButton
      Action = aBrisi
      Category = 0
    end
    object dxBarLargeButton4: TdxBarLargeButton
      Action = aSnimiIzgled
      Category = 0
      ScreenTip = tipSnimiIzgled
    end
    object dxBarLargeButton5: TdxBarLargeButton
      Action = aZacuvajExcel
      Category = 0
      ScreenTip = tipZacuvajExcel
    end
    object dxBarLargeButton6: TdxBarLargeButton
      Action = aPecatiTabela
      Caption = #1055#1077#1095#1072#1090#1080' '#1090#1072#1073#1077#1083#1072
      Category = 0
    end
    object dxBarLargeButton7: TdxBarLargeButton
      Action = aHelp
      Category = 0
    end
    object dxBarLargeButton8: TdxBarLargeButton
      Action = aIzlez
      Category = 0
    end
    object dxBarLargeButton1: TdxBarLargeButton
      Action = aNov
      Category = 0
    end
    object dxBarLargeButton9: TdxBarLargeButton
      Action = aSnimiPecatenje
      Category = 0
      ScreenTip = tipSnimiKonf
    end
    object dxBarLargeButton11: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object ribbonFilterReset: TdxBarLargeButton
      Caption = #1056#1077#1089#1077#1090#1080#1088#1072#1112' '#1092#1080#1083#1090#1077#1088
      Category = 0
      Hint = #1056#1077#1089#1077#1090#1080#1088#1072#1112' '#1092#1080#1083#1090#1077#1088
      Visible = ivAlways
    end
    object dxBarLargeButton12: TdxBarLargeButton
      Action = aPodesuvanjePecatenje
      Category = 0
      ScreenTip = tipDizajnReport
    end
    object dxBarLargeButton13: TdxBarLargeButton
      Action = aBrisiPodesuvanjePecatenje
      Category = 0
      ScreenTip = tipBrisiKonf
    end
    object dxBarLargeButton14: TdxBarLargeButton
      Caption = #1048#1079#1073#1088#1080#1096#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      Category = 0
      Visible = ivAlways
      LargeImageIndex = 40
    end
    object dxBarLargeButton15: TdxBarLargeButton
      Action = aPageSetup
      Category = 0
      ScreenTip = tipSetiranjeStrana
    end
    object dxBarLargeButton16: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object dxBarLargeButton17: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object cxBarEditItem1: TcxBarEditItem
      Caption = 'New Item'
      Category = 0
      Hint = 'New Item'
      Visible = ivAlways
      PropertiesClassName = 'TcxCheckBoxProperties'
    end
    object dxBarSubItem1: TdxBarSubItem
      Caption = #1055#1077#1095#1072#1090#1077#1114#1077
      Category = 0
      Visible = ivAlways
      ImageIndex = 30
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton6'
        end>
    end
    object dxBarLBtnSnimiIzgled: TdxBarLargeButton
      Action = aSnimiIzgled
      Category = 0
    end
    object dxBarLBtnBrisiIzgled: TdxBarLargeButton
      Action = aBrisiIzgled
      Category = 0
    end
  end
  object ActionList1: TActionList
    Images = dmRes.cxSmallImages
    Left = 1011
    Top = 16
    object aNov: TAction
      Caption = #1053#1086#1074
      ImageIndex = 10
      ShortCut = 116
      OnExecute = aNovExecute
    end
    object aAzuriraj: TAction
      Caption = #1040#1078#1091#1088#1080#1088#1072#1112
      ImageIndex = 12
      ShortCut = 117
      OnExecute = aAzurirajExecute
    end
    object aBrisi: TAction
      Caption = #1041#1088#1080#1096#1080
      ImageIndex = 11
      ShortCut = 119
      OnExecute = aBrisiExecute
    end
    object aSnimiIzgled: TAction
      Caption = #1057#1085#1080#1084#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      ImageIndex = 9
      ShortCut = 24659
      OnExecute = aSnimiIzgledExecute
    end
    object aZacuvajExcel: TAction
      Caption = #1047#1072#1095#1091#1074#1072#1112' '#1074#1086' Excel'
      ImageIndex = 9
      ShortCut = 24645
      OnExecute = aZacuvajExcelExecute
    end
    object aPecatiTabela: TAction
      Caption = #1055#1077#1095#1072#1090#1080
      ImageIndex = 30
      OnExecute = aPecatiTabelaExecute
    end
    object aHelp: TAction
      Caption = #1055#1086#1084#1086#1096
      ImageIndex = 24
    end
    object aIzlez: TAction
      Caption = #1048#1079#1083#1077#1079
      ImageIndex = 25
      OnExecute = aIzlezExecute
    end
    object aZapisi: TAction
      Caption = #1047#1072#1087#1080#1096#1080
      ImageIndex = 7
      ShortCut = 120
      OnExecute = aZapisiExecute
    end
    object aOtkazi: TAction
      Caption = #1054#1090#1082#1072#1078#1080
      ImageIndex = 21
      ShortCut = 27
      OnExecute = aOtkaziExecute
    end
    object aRefresh: TAction
      Caption = 'aRefresh'
      ImageIndex = 18
      ShortCut = 118
      OnExecute = aRefreshExecute
    end
    object aSnimiPecatenje: TAction
      Caption = #1057#1085#1080#1084#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      ImageIndex = 39
      OnExecute = aSnimiPecatenjeExecute
    end
    object aPodesuvanjePecatenje: TAction
      Caption = #1044#1080#1079#1072#1112#1085' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090
      ImageIndex = 38
      OnExecute = aPodesuvanjePecatenjeExecute
    end
    object aBrisiPodesuvanjePecatenje: TAction
      Caption = #1041#1088#1080#1096#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      ImageIndex = 37
      OnExecute = aBrisiPodesuvanjePecatenjeExecute
    end
    object aPageSetup: TAction
      Caption = #1057#1077#1090#1080#1088#1072#1114#1077' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072
      ImageIndex = 38
      OnExecute = aPageSetupExecute
    end
    object aFormConfig: TAction
      Caption = 'aFormConfig'
      ShortCut = 24699
      OnExecute = aFormConfigExecute
    end
    object aBrisiIzgled: TAction
      Caption = #1041#1088#1080#1096#1080' '#1075#1086' '#1079#1072#1095#1091#1074#1072#1085#1080#1086#1090' '#1080#1079#1075#1083#1077#1076
      ImageIndex = 56
      OnExecute = aBrisiIzgledExecute
    end
    object aPogled: TAction
      Caption = #1055#1086#1075#1083#1077#1076
      ImageIndex = 47
      OnExecute = aPogledExecute
    end
    object aPoveke: TAction
      Caption = #1044#1077#1090#1072#1083#1085#1086
      OnExecute = aPovekeExecute
    end
  end
  object dxComponentPrinter1: TdxComponentPrinter
    CurrentLink = dxComponentPrinter1Link1
    Version = 0
    Left = 1221
    Top = 16
    object dxComponentPrinter1Link1: TdxGridReportLink
      DateFormat = 0
      PageNumberFormat = pnfNumeral
      PrinterPage.DMPaper = 9
      PrinterPage.Footer = 6350
      PrinterPage.GrayShading = True
      PrinterPage.Header = 6350
      PrinterPage.Margins.Bottom = 12700
      PrinterPage.Margins.Left = 12700
      PrinterPage.Margins.Right = 12700
      PrinterPage.Margins.Top = 12700
      PrinterPage.Orientation = poLandscape
      PrinterPage.PageFooter.RightTitle.Strings = (
        #1057#1090#1088'.[Page #] '#1086#1076' [Total Pages]')
      PrinterPage.PageSize.X = 210000
      PrinterPage.PageSize.Y = 297000
      PrinterPage._dxMeasurementUnits_ = 0
      PrinterPage._dxLastMU_ = 2
      ReportDocument.CreationDate = 40336.425359629630000000
      TimeFormat = 0
      OptionsFormatting.UseNativeStyles = True
      OptionsOnEveryPage.Caption = False
      OptionsOnEveryPage.FilterBar = False
      OptionsView.Caption = False
      OptionsView.FilterBar = False
      StyleRepository = dmRes.cxStyleRepositoryPrinting
      Styles.StyleSheet = dmRes.dxGridReportLinkStyleSheet
      BuiltInReportLink = True
    end
  end
  object dxBarScreenTipRepository1: TdxBarScreenTipRepository
    Left = 1186
    Top = 16
    object tipSnimiIzgled: TdxBarScreenTip
      Header.Text = #1057#1085#1080#1084#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      Description.Text = #1047#1072#1095#1091#1074#1072#1112' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090' '#1085#1072' '#1092#1086#1088#1084#1072#1090#1072' '#1080' '#1085#1072' '#1090#1072#1073#1077#1083#1072#1090#1072
    end
    object tipZacuvajExcel: TdxBarScreenTip
      Header.Text = #1047#1072#1095#1091#1074#1072#1112' '#1074#1086' Excel'
      Description.Text = #1057#1085#1080#1084#1080' '#1075#1080' '#1087#1086#1076#1072#1090#1086#1094#1080#1090#1077' '#1086#1076' '#1090#1072#1073#1077#1083#1072#1090#1072' '#1074#1086' Excel '#1092#1086#1088#1084#1072#1090
    end
    object tipDizajnReport: TdxBarScreenTip
      Header.Text = #1044#1080#1079#1072#1112#1085' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090
      Description.Text = #1055#1086#1076#1077#1089#1091#1074#1072#1114#1072' '#1085#1072' '#1080#1079#1075#1083#1077#1076#1086#1090' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090' '#1085#1072' '#1090#1072#1073#1077#1083#1072#1090#1072
    end
    object tipSetiranjeStrana: TdxBarScreenTip
      Header.Text = #1057#1077#1090#1080#1088#1072#1114#1077' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072
      Description.Text = #1055#1086#1076#1077#1089#1091#1074#1072#1114#1077' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072' '#1085#1072' '#1082#1086#1112#1072' '#1116#1077' '#1089#1077' '#1087#1077#1095#1072#1090#1080' '#1090#1072#1073#1077#1083#1072#1090#1072
    end
    object tipSnimiKonf: TdxBarScreenTip
      Header.Text = #1057#1085#1080#1084#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      Description.Text = #1047#1072#1095#1091#1074#1072#1112' '#1075#1080' '#1087#1086#1076#1077#1089#1091#1074#1072#1114#1072#1090#1072' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090' '#1080' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072' '#1079#1072' '#1087#1077#1095#1072#1090#1077#1114#1077
    end
    object tipBrisiKonf: TdxBarScreenTip
      Header.Text = #1041#1088#1080#1096#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      Description.Text = #1048#1079#1073#1088#1080#1096#1080' '#1075#1080' '#1087#1086#1076#1077#1089#1091#1074#1072#1114#1072#1090#1072' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090' '#1080' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072' '#1079#1072' '#1087#1077#1095#1072#1090#1077#1114#1077
    end
  end
  object tblIzberiKamioni: TpFIBDataSet
    RefreshSQL.Strings = (
      'select distinct'
      '  --  pst.id,'
      '    t.id,'
      '    t.naziv naziv_trailer,'
      '    t.datum datum_transport--,'
      '/*$$IBEC$$     pst.id_poracka_stavka,'
      '    pst.id_trailer,'
      '    pst.kolicina kol_trailer,'
      '    ma.naziv naziv_artikal,'
      '    ps.kolicina vk_kolicina'
      ' $$IBEC$$*/'
      ' from man_por_s_trailer pst'
      'inner join man_trailer t on t.id = pst.id_trailer'
      
        '--inner join man_poracka_stavka ps on ps.id = pst.id_poracka_sta' +
        'vka'
      '--inner join man_poracka p on p.id = ps.id_poracka'
      
        '--inner join mtr_artikal ma on ma.artvid = ps.vid_artikal and ma' +
        '.id = ps.artikal'
      'where t.status like :status'
      '')
    SelectSQL.Strings = (
      'select distinct'
      '  --  pst.id,'
      '    t.id id_trailer,'
      '    t.naziv naziv_trailer,'
      '    t.datum datum_transport--,'
      '/*$$IBEC$$     pst.id_poracka_stavka,'
      '    pst.id_trailer,'
      '    pst.kolicina kol_trailer,'
      '    ma.naziv naziv_artikal,'
      '    ps.kolicina vk_kolicina'
      ' $$IBEC$$*/'
      ' from man_por_s_trailer pst'
      'inner join man_trailer t on t.id = pst.id_trailer'
      
        '--inner join man_poracka_stavka ps on ps.id = pst.id_poracka_sta' +
        'vka'
      '--inner join man_poracka p on p.id = ps.id_poracka'
      
        '--inner join mtr_artikal ma on ma.artvid = ps.vid_artikal and ma' +
        '.id = ps.artikal'
      'where t.status like :status'
      '')
    AutoUpdateOptions.GeneratorName = 'GEN__ID'
    AutoUpdateOptions.WhenGetGenID = wgBeforePost
    AutoUpdateOptions.AutoParamsToFields = True
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 1046
    Top = 51
    object fbstrngfldIzberiKamioniNAZIV_TRAILER: TFIBStringField
      FieldName = 'NAZIV_TRAILER'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object fbdtfldIzberiKamioniDATUM_TRANSPORT: TFIBDateField
      FieldName = 'DATUM_TRANSPORT'
    end
    object tblIzberiKamioniID_TRAILER: TFIBIntegerField
      FieldName = 'ID_TRAILER'
    end
  end
  object dsIzberiKamioni: TDataSource
    DataSet = tblIzberiKamioni
    Left = 1081
    Top = 16
  end
  object tblArtikalRN: TpFIBDataSet
    RefreshSQL.Strings = (
      'select distinct'
      '       A.ARTVID,'
      '       A.ID,'
      '       A.NAZIV'
      'from MTR_ARTIKAL A'
      
        'inner join man_poracka_stavka ps on ps.vid_artikal = a.artvid an' +
        'd ps.artikal = a.id and ps.status <> 3'
      
        'inner join man_por_s_trailer pst on pst.id_poracka_stavka = ps.i' +
        'd'
      'inner join man_trailer t on t.id = pst.id_trailer'
      'where(  t.id = :id'
      '     ) and (     A.ARTVID = :OLD_ARTVID'
      '    and A.ID = :OLD_ID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      'select distinct'
      '       A.ARTVID,'
      '       A.ID,'
      '       A.NAZIV'
      'from MTR_ARTIKAL A'
      
        'inner join man_poracka_stavka ps on ps.vid_artikal = a.artvid an' +
        'd ps.artikal = a.id and ps.status <> 3'
      
        'inner join man_por_s_trailer pst on pst.id_poracka_stavka = ps.i' +
        'd'
      'inner join man_trailer t on t.id = pst.id_trailer'
      'where t.id = :id')
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 1011
    Top = 51
    poSQLINT64ToBCD = True
    object tblArtikalRNARTVID: TFIBIntegerField
      DisplayLabel = #1042#1080#1076' '#1085#1072' '#1072#1088#1090#1080#1082#1072#1083
      FieldName = 'ARTVID'
    end
    object tblArtikalRNID: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072
      FieldName = 'ID'
    end
    object tblArtikalRNNAZIV: TFIBStringField
      DisplayLabel = #1053#1072#1079#1080#1074
      FieldName = 'NAZIV'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object dsArtikalRN: TDataSource
    AutoEdit = False
    DataSet = tblArtikalRN
    Left = 1116
    Top = 16
  end
end
