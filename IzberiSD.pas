unit IzberiSD;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxStyles, dxSkinsCore, dxSkinBlack, dxSkinBlue,
  dxSkinBlueprint, dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide,
  dxSkinDevExpressDarkStyle, dxSkinDevExpressStyle, dxSkinFoggy,
  dxSkinGlassOceans, dxSkinHighContrast, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMetropolis,
  dxSkinMetropolisDark, dxSkinMoneyTwins, dxSkinOffice2007Black,
  dxSkinOffice2007Blue, dxSkinOffice2007Green, dxSkinOffice2007Pink,
  dxSkinOffice2007Silver, dxSkinOffice2010Black, dxSkinOffice2010Blue,
  dxSkinOffice2010Silver, dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray,
  dxSkinOffice2013White, dxSkinOffice2016Colorful, dxSkinOffice2016Dark,
  dxSkinPumpkin, dxSkinSeven, dxSkinSevenClassic, dxSkinSharp, dxSkinSharpPlus,
  dxSkinSilver, dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008,
  dxSkinTheAsphaltWorld, dxSkinsDefaultPainters, dxSkinValentine,
  dxSkinVisualStudio2013Blue, dxSkinVisualStudio2013Dark,
  dxSkinVisualStudio2013Light, dxSkinVS2010, dxSkinWhiteprint,
  dxSkinXmas2008Blue, dxSkinscxPCPainter, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxEdit, cxNavigator, Data.DB, cxDBData, cxGridLevel, cxClasses,
  cxGridCustomView, cxGridCustomTableView, cxGridTableView, cxGridDBTableView,
  cxGrid, Vcl.ExtCtrls, FIBDataSet, pFIBDataSet, System.Actions, Vcl.ActnList,
  Vcl.Menus, Vcl.StdCtrls, cxButtons, DaNe, Vcl.ComCtrls, cxGridBandedTableView,
  cxGridDBBandedTableView;

type
  TfrmIzberiSD = class(TForm)
    Panel1: TPanel;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    tblIzberiSD: TpFIBDataSet;
    dsIzberiSD: TDataSource;
    tblIzberiSDARTVID: TFIBIntegerField;
    tblIzberiSDID: TFIBIntegerField;
    tblIzberiSDNAZIV: TFIBStringField;
    cxGrid1DBTableView1ARTVID: TcxGridDBColumn;
    cxGrid1DBTableView1ID: TcxGridDBColumn;
    cxGrid1DBTableView1NAZIV: TcxGridDBColumn;
    ActionList1: TActionList;
    aZapisi: TAction;
    aOtkazi: TAction;
    aGenerirajEtiketi: TAction;
    aGenerirajEtiketiStavki: TAction;
    aPresmetajSD: TAction;
    Panel3: TPanel;
    Panel2: TPanel;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    StatusBar1: TStatusBar;
    cxGrid2: TcxGrid;
    cxGrid2DBTableView1: TcxGridDBTableView;
    cxGrid2Level1: TcxGridLevel;
    tblSosDelLot: TpFIBDataSet;
    dsSosDelLot: TDataSource;
    tblSosDelLotID: TFIBIntegerField;
    tblSosDelLotVID_SOS_DEL: TFIBIntegerField;
    tblSosDelLotSOS_DEL: TFIBIntegerField;
    tblSosDelLotNAZIV: TFIBStringField;
    tblSosDelLotLOT: TFIBStringField;
    cxGrid2DBBandedTableView1ID: TcxGridDBBandedColumn;
    cxGrid2DBBandedTableView1VID_SOS_DEL: TcxGridDBBandedColumn;
    cxGrid2DBBandedTableView1SOS_DEL: TcxGridDBBandedColumn;
    cxGrid2DBBandedTableView1NAZIV: TcxGridDBBandedColumn;
    cxGrid2DBBandedTableView1LOT: TcxGridDBBandedColumn;
    tblSosDelLotBARKOD: TFIBStringField;
    cxGrid2DBBandedTableView1BARKOD: TcxGridDBBandedColumn;
    cxGrid2DBBandedTableView1: TcxGridDBBandedTableView;
  // cxGrid2DBBandedTableView1ID: TcxGridDBBandedColumn;
  //  cxGrid2DBBandedTableView1VID_SOS_DEL: TcxGridDBBandedColumn;
  //  cxGrid2DBBandedTableView1SOS_DEL: TcxGridDBBandedColumn;
  //  cxGrid2DBBandedTableView1NAZIV: TcxGridDBBandedColumn;
  //  cxGrid2DBBandedTableView1LOT: TcxGridDBBandedColumn;
  //  cxGrid2DBBandedTableView1BARKOD: TcxGridDBBandedColumn;
    cxGrid1DBBandedTableView1: TcxGridDBBandedTableView;
    cxGrid1DBBandedTableView1ARTVID: TcxGridDBBandedColumn;
    cxGrid1DBBandedTableView1ID: TcxGridDBBandedColumn;
    cxGrid1DBBandedTableView1NAZIV: TcxGridDBBandedColumn;
    aBrisiSD: TAction;
    aGE: TAction;
    procedure aZapisiExecute(Sender: TObject);
    procedure aOtkaziExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure aGenerirajEtiketiExecute(Sender: TObject);
    procedure aGenerirajEtiketiStavkiExecute(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure aBrisiSDExecute(Sender: TObject);
    procedure cxGrid2DBBandedTableView1DblClick(Sender: TObject);
    procedure aGEExecute(Sender: TObject);
    procedure tblSosDelLotBeforeDelete(DataSet: TDataSet);
  private
    { Private declarations }
  public
     lot, sifra_sd, kolicina_broj, bar_art : string;
     rn, id_rn_stavka : LongInt;
     imenitel, vid_artikal, artikal : Integer;
    { Public declarations }
  end;

var
  frmIzberiSD: TfrmIzberiSD;

implementation
uses
   dmKonekcija, dmUnit, dmMaticni, dmResources, dmUnitBK, KolicinaBroj;

{$R *.dfm}

procedure TfrmIzberiSD.aBrisiSDExecute(Sender: TObject);
begin
    tblSosDelLot.Delete;
    tblIzberiSD.FullRefresh;
end;

procedure TfrmIzberiSD.aGEExecute(Sender: TObject);
var i : Integer;
begin
      imenitel := tblSosDelLot.RecordCount;

      frmDaNe := TfrmDaNe.Create(self, '�������', '���� �� ���������� ������� �� ���������� ������?', 1);
         if (frmDaNe.ShowModal <> mrYes) then
              Abort
         else
         begin
             frmKolicinaBroj := TfrmKolicinaBroj.Create(self);
             frmKolicinaBroj.ShowModal;
             kolicina_broj := frmKolicinaBroj.txtKolicinaBroj.Text;
             frmKolicinaBroj.Free;
             aGenerirajEtiketi.Execute;

               //generiraj etiketa
               tblSosDelLot.First;
               while not tblSosDelLot.eof do
                 begin
                      sifra_sd :=  tblSosDelLotSOS_DEL.AsString;
                      aGenerirajEtiketiStavki.Execute;
                      tblSosDelLot.Next;
                  end;

               end;

           // tblSosDelLot.Close;
           // tblSosDelLot.ParamByName('lot').Value := lot;
           // tblSosDelLot.Open;

            tblSosDelLot.FullRefresh;
            tblIzberiSD.FullRefresh;

//            tblIzberiSD.Close;
//            tblIzberiSD.ParamByName('lot').Value := lot;
//            tblIzberiSD.Open;
end;

procedure TfrmIzberiSD.aGenerirajEtiketiExecute(Sender: TObject);
var broj : string;
i : Integer;
begin
      dmBK.MaxEtiketa.Close;
      dmBK.MaxEtiketa.ExecQuery;

      dmBK.tblEtiketa.Open;
      dmbk.tblEtiketa.Insert;
      dmBK.tblEtiketaID_RABOTEN_NALOG.Value := rn;

      for I := 1 to (7-(Length(inttostr(dmBK.MaxEtiketa.FldByName['maks'].AsInteger+1)))) do
      begin
       broj := broj+'0';
      end;
       broj := kompaniski_broj + broj + inttostr(dmBK.MaxEtiketa.FldByName['maks'].AsInteger+1);
       dmBK.Kontrolen_broj(broj);
       dmBK.tblEtiketaSSCC.Value := '(00)' + broj + inttostr(kb);
       dmbk.tblEtiketa.Post;

end;

procedure TfrmIzberiSD.aGenerirajEtiketiStavkiExecute(Sender: TObject);
var i : Integer;  broj, imen_str:string; def:AnsiString;
begin
      dmBK.tblEtiketaStavka.Open;

      dmBK.tblEtiketaStavka.Insert;
      dmBK.tblEtiketaStavkaVID_ARTIKAL.Value := vid_artikal;
      dmbk.tblEtiketaStavkaARTIKAL.Value := artikal;
      dmbk.tblEtiketaStavkaLOT.Value := lot;
      dmbk.tblEtiketaStavkaID_RNS.Value := id_rn_stavka;
      dmBK.tblEtiketaStavkaID_SOS_DEL.AsString := sifra_sd;
      dmBK.tblEtiketaStavkaGTIN.Value := bar_art;
      dmBK.tblEtiketaStavkaKOLICINA_BROJ.AsString := kolicina_broj;
      dmBK.tblEtiketaStavkaIMENITEL.Value := imenitel;

      dmBK.MaxEtiketaStavka.Close;
      dmBK.MaxEtiketaStavka.ExecQuery;

       for I := 1 to (7-(Length(inttostr(dmBK.MaxEtiketaStavka.FldByName['maks'].AsInteger+1)))) do
       begin
        broj := broj+'0';
       end;
        broj := kompaniski_broj + broj + inttostr(dmBK.MaxEtiketaStavka.FldByName['maks'].AsInteger+1);
        dmBK.Kontrolen_broj(broj);
        dmBK.tblEtiketaStavkaSSCC.Value := '(00)' + broj + inttostr(kb);
        if Length(IntToStr(Imenitel))=1 then  imen_str := '0'+ IntToStr(Imenitel);
        if Length(sifra_sd)=1 then  sifra_sd := '0'+ sifra_sd;
        dmBK.tblEtiketaStavkaBARKOD.Value := '(01)' + bar_art +
                                             '(10)' + lot +
                                             '(37)' + kolicina_broj +
                                             '(8026)' + sifra_sd + imen_str;

       dmBK.tblEtiketaStavka.Post;

end;

procedure TfrmIzberiSD.aOtkaziExecute(Sender: TObject);
begin
   dm.tblSosDel.Cancel;
   Close;
end;

procedure TfrmIzberiSD.aZapisiExecute(Sender: TObject);
var i : integer;
begin
  frmDaNe := TfrmDaNe.Create(self, '�������', '���� �������� �������� ������ �� ��������?', 1);
    if (frmDaNe.ShowModal <> mrYes) then
        Abort
    else
    begin
      imenitel := cxGrid1DBBandedTableView1.DataController.GetSelectedCount;
      frmDaNe := TfrmDaNe.Create(self, '�������', '���� �� ���������� ������� �� ���������� ������?', 1);
         if (frmDaNe.ShowModal <> mrYes) then
         begin
            for i := 0 to cxGrid1DBBandedTableView1.DataController.GetSelectedCount-1 do
               begin
                      dm.tblSosDel.Insert;
                      dm.tblSosDelSOS_DEL.Value :=  (cxGrid1DBBandedTableView1.Controller.SelectedRows[i].Values[cxGrid1DBBandedTableView1.GetColumnByFieldName('ID').Index]); //(Values[i,cxGrid1DBTableView1ID.Index]);  //tblIzberiSDID.Value;
                      dm.tblSosDelLOT.Value := lot;
                      dm.tblSosDel.Post;
               end;
                ShowMessage('�������� �� ���������� ������!!!');
               // Close;
         end
         else
         begin
             frmKolicinaBroj := TfrmKolicinaBroj.Create(self);
             frmKolicinaBroj.ShowModal;
             kolicina_broj := frmKolicinaBroj.txtKolicinaBroj.Text;
             frmKolicinaBroj.Free;
             aGenerirajEtiketi.Execute;
             for i := 0 to cxGrid1DBBandedTableView1.DataController.GetSelectedCount-1 do
               begin
                //insertiraj sostaven del i napravi etiketa
                      dm.tblSosDel.Insert;
                      dm.tblSosDelSOS_DEL.Value :=  (cxGrid1DBBandedTableView1.Controller.SelectedRows[i].Values[cxGrid1DBBandedTableView1.GetColumnByFieldName('ID').Index]); //(Values[i,cxGrid1DBBandedTableView1ID.Index]);  //tblIzberiSDID.Value;
                      dm.tblSosDelLOT.Value := lot;
                      dm.tblSosDel.Post;
               //generiraj etiketa
                      sifra_sd := (cxGrid1DBBandedTableView1.Controller.SelectedRows[i].Values[cxGrid1DBBandedTableView1.GetColumnByFieldName('ID').Index]);
                      aGenerirajEtiketiStavki.Execute;

               end;


         end;
          //  tblSosDelLot.Close;
          //  tblSosDelLot.ParamByName('lot').Value := lot;
          //  tblSosDelLot.Open;
            tblSosDelLot.FullRefresh;

//            tblIzberiSD.Close;
//            tblIzberiSD.ParamByName('lot').Value := lot;
//            tblIzberiSD.Open;
           tblIzberiSD.FullRefresh;
    end;
 end;


procedure TfrmIzberiSD.cxGrid2DBBandedTableView1DblClick(Sender: TObject);
begin
   aBrisiSD.Execute;
end;

//end;

procedure TfrmIzberiSD.FormCreate(Sender: TObject);
begin
    dm.tblSosDel.Close;
    dm.tblSosDel.ParamByName('lot').AsString := '%';
    dm.tblSosDel.Open;

end;

procedure TfrmIzberiSD.FormShow(Sender: TObject);
begin
            tblSosDelLot.Close;
            tblSosDelLot.ParamByName('lot').Value := lot;
            tblSosDelLot.Open;
end;

procedure TfrmIzberiSD.tblSosDelLotBeforeDelete(DataSet: TDataSet);
begin
    frmDaNe := TfrmDaNe.Create(self, '������ �� �����', '���� ��������� ������ �� �� ��������� �������?', 1);
    if (frmDaNe.ShowModal <> mrYes) then
        Abort;
end;

end.
