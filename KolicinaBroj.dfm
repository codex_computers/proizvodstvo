object frmKolicinaBroj: TfrmKolicinaBroj
  Left = 0
  Top = 0
  BorderIcons = []
  BorderStyle = bsNone
  Caption = 'frmKolicinaBroj'
  ClientHeight = 104
  ClientWidth = 267
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poDesktopCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel5: TPanel
    Left = 0
    Top = 0
    Width = 267
    Height = 104
    Align = alClient
    BevelKind = bkFlat
    BevelOuter = bvNone
    Color = clWindow
    ParentBackground = False
    TabOrder = 0
    object txtKolicinaBroj: TcxTextEdit
      Left = 32
      Top = 35
      AutoSize = False
      ParentFont = False
      Style.BorderStyle = ebsUltraFlat
      Style.Font.Charset = RUSSIAN_CHARSET
      Style.Font.Color = clWindowText
      Style.Font.Height = -11
      Style.Font.Name = 'Tahoma'
      Style.Font.Style = []
      Style.IsFontAssigned = True
      TabOrder = 0
      OnKeyDown = txtKolicinaBrojKeyDown
      Height = 33
      Width = 185
    end
    object cxLabel21: TcxLabel
      AlignWithMargins = True
      Left = 3
      Top = 3
      Align = alTop
      AutoSize = False
      Caption = #1050#1086#1083#1080#1095#1080#1085#1072' '#1074#1086' '#1082#1091#1090#1080#1112#1072
      ParentColor = False
      Style.BorderStyle = ebsNone
      Style.Color = clHotLight
      Style.LookAndFeel.Kind = lfUltraFlat
      Style.LookAndFeel.NativeStyle = False
      Style.Shadow = False
      Style.TextColor = clWindow
      Style.TextStyle = [fsBold]
      StyleDisabled.LookAndFeel.Kind = lfUltraFlat
      StyleDisabled.LookAndFeel.NativeStyle = False
      StyleFocused.LookAndFeel.Kind = lfUltraFlat
      StyleFocused.LookAndFeel.NativeStyle = False
      StyleHot.LookAndFeel.Kind = lfUltraFlat
      StyleHot.LookAndFeel.NativeStyle = False
      Properties.Alignment.Horz = taCenter
      Properties.Alignment.Vert = taVCenter
      ExplicitLeft = -2
      Height = 26
      Width = 257
      AnchorX = 132
      AnchorY = 16
    end
  end
  object qUpdateSetup: TpFIBQuery
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    SQL.Strings = (
      'update sys_setup s'
      'set s.v1=:v1'
      'where p1=:p1 and p2=:p2 '
      ' ')
    Left = 32
    Top = 8
    qoAutoCommit = True
    qoStartTransaction = True
  end
  object qSiteId: TpFIBQuery
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    SQL.Strings = (
      'select site_id from repli_site_setup ')
    Left = 8
    Top = 8
    qoAutoCommit = True
    qoStartTransaction = True
  end
  object ActionList1: TActionList
    Left = 8
    Top = 56
    object aExit: TAction
      Caption = 'aExit'
      ShortCut = 27
      OnExecute = aExitExecute
    end
  end
end
