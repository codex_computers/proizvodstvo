object frmEvidencijaPotroseniMaterijali: TfrmEvidencijaPotroseniMaterijali
  Left = 128
  Top = 212
  BorderIcons = [biSystemMenu, biMaximize]
  Caption = #1052#1072#1090#1077#1088#1080#1112#1072#1083#1080' '#1074#1086' '#1077#1074#1080#1076#1077#1085#1094#1080#1112#1072' : '
  ClientHeight = 690
  ClientWidth = 1000
  Color = 15790320
  Constraints.MinHeight = 500
  Constraints.MinWidth = 610
  Font.Charset = RUSSIAN_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  ShowHint = True
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object dxRibbon1: TdxRibbon
    Left = 0
    Top = 0
    Width = 1000
    Height = 126
    BarManager = dxBarManager1
    ColorSchemeName = 'Blue'
    Contexts = <>
    TabOrder = 0
    TabStop = False
    object dxRibbon1Tab1: TdxRibbonTab
      Active = True
      Caption = #1052#1077#1085#1080
      Groups = <
        item
          Caption = #1052#1072#1090#1077#1088#1080#1112#1072#1083
          ToolbarName = 'dxBarManager1Bar1'
        end
        item
          ToolbarName = 'dxBarManager1Bar2'
        end
        item
          ToolbarName = 'dxBarManager1Bar3'
        end>
      Index = 0
    end
    object dxRibbon1Tab2: TdxRibbonTab
      Caption = #1055#1086#1076#1077#1089#1091#1074#1072#1114#1072
      Groups = <
        item
          ToolbarName = 'dxBarManager1Bar4'
        end
        item
          ToolbarName = 'dxBarManager1BarSnimiIzgled'
        end
        item
          Caption = 'Custom'
          ToolbarName = 'dxBarManager1Bar5'
        end>
      Index = 1
    end
  end
  object StatusBar1: TdxRibbonStatusBar
    Left = 0
    Top = 667
    Width = 1000
    Height = 23
    Panels = <
      item
        PanelStyleClassName = 'TdxStatusBarTextPanelStyle'
        Text = 
          ' F5 - '#1053#1086#1074', F6 - '#1040#1078#1091#1088#1080#1088#1072#1112', F7 - '#1054#1089#1074#1077#1078#1080', F8 - '#1041#1088#1080#1096#1080', F9 - '#1047#1072#1087#1080#1096#1080', ' +
          'Esc - '#1054#1090#1082#1072#1078#1080' / '#1048#1079#1083#1077#1079
      end>
    Ribbon = dxRibbon1
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clDefault
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
  end
  object lPanel: TPanel
    Left = 0
    Top = 126
    Width = 1000
    Height = 234
    Align = alClient
    BevelInner = bvLowered
    BevelOuter = bvSpace
    TabOrder = 2
  end
  object cxPageControlMenu: TcxPageControl
    Left = 0
    Top = 484
    Width = 1000
    Height = 183
    Align = alBottom
    TabOrder = 3
    TabStop = False
    Properties.ActivePage = cxTabSheetKolicinski
    Properties.CustomButtons.Buttons = <>
    Properties.OwnerDraw = True
    Properties.TabWidth = 80
    LookAndFeel.NativeStyle = False
    OnChange = cxPageControlMenuChange
    ClientRectBottom = 183
    ClientRectRight = 1000
    ClientRectTop = 24
    object cxTabSheetKolicinski: TcxTabSheet
      Caption = #1055#1086#1076#1072#1090#1086#1094#1080
      ImageIndex = 0
      object dPanelKol: TPanel
        Left = 0
        Top = 0
        Width = 1000
        Height = 159
        Align = alClient
        BevelOuter = bvSpace
        Color = 15790320
        Enabled = False
        ParentBackground = False
        PopupMenu = PopupMenu1
        TabOrder = 0
        object Label1: TLabel
          Left = 17
          Top = 14
          Width = 55
          Height = 13
          Alignment = taRightJustify
          Caption = #1040#1088#1090#1080#1082#1072#1083' :'
          Font.Charset = RUSSIAN_CHARSET
          Font.Color = clRed
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object Label2: TLabel
          Left = 39
          Top = 41
          Width = 33
          Height = 13
          Alignment = taRightJustify
          Caption = #1054#1087#1080#1089' :'
          Font.Charset = RUSSIAN_CHARSET
          Font.Color = clBlue
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object OtkaziButton: TcxButton
          Left = 839
          Top = 115
          Width = 75
          Height = 25
          Action = aOtkazi
          Colors.Pressed = clGradientActiveCaption
          TabOrder = 6
        end
        object ArtVid: TcxDBTextEdit
          Tag = 1
          Left = 78
          Top = 11
          Hint = #1042#1080#1076' '#1085#1072' '#1072#1088#1090#1080#1082#1072#1083'/'#1087#1088#1086#1080#1079#1074#1086#1076
          BeepOnEnter = False
          DataBinding.DataField = 'ART_VID'
          DataBinding.DataSource = dsOutStavka
          ParentFont = False
          Properties.BeepOnError = True
          Style.Shadow = False
          TabOrder = 0
          OnKeyDown = EnterKakoTab
          Width = 55
        end
        object ZapisiButtonKol: TcxButton
          Left = 758
          Top = 115
          Width = 75
          Height = 25
          Action = aZapisi
          Colors.Pressed = clGradientActiveCaption
          TabOrder = 5
        end
        object ArtSifra: TcxDBTextEdit
          Tag = 1
          Left = 134
          Top = 11
          Hint = #1064#1080#1092#1088#1072' '#1085#1072' '#1072#1088#1090#1080#1082#1072#1083'/'#1087#1088#1086#1080#1079#1074#1086#1076
          BeepOnEnter = False
          DataBinding.DataField = 'ART_SIF'
          DataBinding.DataSource = dsOutStavka
          ParentFont = False
          Properties.BeepOnError = True
          Properties.OnChange = ArtSifraPropertiesChange
          Properties.OnEditValueChanged = ArtSifraPropertiesEditValueChanged
          Style.Shadow = False
          TabOrder = 1
          OnKeyDown = EnterKakoTab
          Width = 71
        end
        object cxExtArtikal: TcxExtLookupComboBox
          Left = 205
          Top = 11
          Hint = #1053#1072#1079#1080#1074' '#1085#1072' '#1072#1088#1090#1080#1082#1072#1083'/'#1087#1088#1086#1080#1079#1074#1086#1076
          TabOrder = 2
          OnExit = cxExtArtikalExit
          OnKeyDown = EnterKakoTab
          Width = 444
        end
        object gbMerkaKolicina: TcxGroupBox
          Left = 17
          Top = 65
          Caption = #1052#1077#1088#1082#1072'/'#1050#1086#1083#1080#1095#1080#1085#1072
          TabOrder = 4
          Height = 75
          Width = 656
          object Label9: TLabel
            Left = 18
            Top = 21
            Width = 83
            Height = 13
            Alignment = taRightJustify
            Caption = #1048#1079#1074#1077#1076#1077#1085#1072' '#1045#1052' :'
            Font.Charset = RUSSIAN_CHARSET
            Font.Color = clBlue
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object Label10: TLabel
            Left = 329
            Top = 21
            Width = 48
            Height = 13
            Alignment = taRightJustify
            Caption = #1055#1072#1082#1077#1090#1080' :'
            Font.Charset = RUSSIAN_CHARSET
            Font.Color = clBlue
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object Label11: TLabel
            Left = 395
            Top = 21
            Width = 78
            Height = 13
            Alignment = taRightJustify
            Caption = #1041#1088'. '#1074#1086' '#1087#1072#1082#1077#1090' :'
            Font.Charset = RUSSIAN_CHARSET
            Font.Color = clBlue
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object Label8: TLabel
            Left = 486
            Top = 21
            Width = 60
            Height = 13
            Alignment = taRightJustify
            Caption = #1050#1086#1083#1080#1095#1080#1085#1072' :'
            Font.Charset = RUSSIAN_CHARSET
            Font.Color = clRed
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object Paketi: TcxDBTextEdit
            Left = 329
            Top = 34
            Hint = #1041#1088#1086#1112' '#1085#1072' '#1087#1072#1082#1077#1090#1080
            BeepOnEnter = False
            DataBinding.DataField = 'PAKETI'
            DataBinding.DataSource = dsOutStavka
            ParentFont = False
            Properties.BeepOnError = True
            Style.Shadow = False
            TabOrder = 1
            OnExit = PresmetajNaIzlez
            OnKeyDown = EnterKakoTab
            Width = 60
          end
          object BrPaket: TcxDBTextEdit
            Left = 395
            Top = 34
            Hint = #1041#1088#1086#1112' '#1074#1086' '#1087#1072#1082#1077#1090
            BeepOnEnter = False
            DataBinding.DataField = 'BR_PAKET'
            DataBinding.DataSource = dsOutStavka
            ParentFont = False
            Properties.BeepOnError = True
            Style.Shadow = False
            TabOrder = 2
            OnExit = PresmetajNaIzlez
            OnKeyDown = EnterKakoTab
            Width = 85
          end
          object lcbIzvedena: TcxDBLookupComboBox
            Left = 18
            Top = 34
            Hint = #1053#1072#1079#1080#1074' '#1085#1072' '#1080#1079#1074#1077#1076#1077#1085#1072' '#1084#1077#1088#1085#1072' '#1077#1076#1080#1085#1080#1094#1072
            DataBinding.DataField = 'IZV_EM'
            DataBinding.DataSource = dsOutStavka
            Properties.ListColumns = <
              item
                Width = 50
                FieldName = 'ID'
              end
              item
                Width = 250
                FieldName = 'MERKA_NAZIV'
              end
              item
                FieldName = 'IZVEDENA_EM'
              end
              item
                FieldName = 'KOEFICIENT'
              end>
            Properties.ListFieldIndex = 1
            Properties.ListOptions.SyncMode = True
            Properties.ListSource = dm.dsIzvedenaEM
            TabOrder = 0
            OnEnter = lcbIzvedenaEnter
            OnExit = lcbIzvedenaExit
            OnKeyDown = EnterKakoTab
            Width = 305
          end
          object Kolicina: TcxDBTextEdit
            Tag = 1
            Left = 486
            Top = 34
            Hint = #1055#1086#1090#1088#1086#1096#1077#1085#1072' '#1082#1086#1083#1080#1095#1080#1085#1072
            BeepOnEnter = False
            DataBinding.DataField = 'KOLICINA'
            DataBinding.DataSource = dsOutStavka
            ParentFont = False
            Properties.BeepOnError = True
            Style.Shadow = False
            TabOrder = 3
            OnKeyDown = EnterKakoTab
            Width = 91
          end
          object lblMerka: TcxLabel
            Left = 583
            Top = 35
            Caption = #1052#1077#1088#1082#1072
            ParentFont = False
            Style.Font.Charset = RUSSIAN_CHARSET
            Style.Font.Color = clWindowText
            Style.Font.Height = -11
            Style.Font.Name = 'Tahoma'
            Style.Font.Style = [fsBold]
            Style.IsFontAssigned = True
            Transparent = True
          end
        end
        object Opis: TcxDBTextEdit
          Left = 78
          Top = 38
          Hint = #1054#1087#1080#1089' '
          BeepOnEnter = False
          DataBinding.DataField = 'OPIS'
          DataBinding.DataSource = dsOutStavka
          ParentFont = False
          Properties.BeepOnError = True
          Properties.OnChange = ArtSifraPropertiesChange
          Style.Shadow = False
          TabOrder = 3
          OnKeyDown = EnterKakoTab
          Width = 571
        end
        object cxGroupBox1: TcxGroupBox
          Left = 724
          Top = 11
          Caption = #1048#1085#1092#1086' '#1072#1088#1090#1080#1082#1072#1083
          TabOrder = 7
          Height = 70
          Width = 190
          object lbl1: TLabel
            Left = 16
            Top = 33
            Width = 41
            Height = 13
            Alignment = taRightJustify
            Caption = #1051#1072#1075#1077#1088' :'
            Font.Charset = RUSSIAN_CHARSET
            Font.Color = clBlack
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = [fsBold]
            ParentFont = False
          end
          object Lager: TcxDBTextEdit
            Left = 63
            Top = 30
            TabStop = False
            BeepOnEnter = False
            DataBinding.DataField = 'O_LAGER'
            DataBinding.DataSource = dsLagerArtikal
            ParentFont = False
            Properties.BeepOnError = True
            Properties.ReadOnly = True
            Style.Shadow = False
            TabOrder = 0
            Width = 85
          end
          object cxButton1: TcxButton
            Left = 154
            Top = 28
            Width = 22
            Height = 25
            Action = aZemiArtikalInfo
            Colors.Pressed = clGradientActiveCaption
            OptionsImage.Images = dmRes.cxSmallImages
            PaintStyle = bpsGlyph
            TabOrder = 1
          end
        end
      end
    end
    object cxTabSheetCustom: TcxTabSheet
      Caption = 'Custom'
      ImageIndex = 4
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object dPanelCustom: TPanel
        Left = 0
        Top = 0
        Width = 1000
        Height = 159
        Align = alClient
        Color = 15790320
        ParentBackground = False
        TabOrder = 0
        DesignSize = (
          1000
          159)
        object lblCustom1: TLabel
          Left = 15
          Top = 9
          Width = 56
          Height = 13
          Caption = 'Custom1 :'
          Font.Charset = RUSSIAN_CHARSET
          Font.Color = clBlue
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
          Visible = False
        end
        object lblCustom2: TLabel
          Left = 15
          Top = 51
          Width = 56
          Height = 13
          Caption = 'Custom2 :'
          Font.Charset = RUSSIAN_CHARSET
          Font.Color = clBlue
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
          Visible = False
        end
        object lblCustom3: TLabel
          Left = 15
          Top = 93
          Width = 56
          Height = 13
          Caption = 'Custom3 :'
          Font.Charset = RUSSIAN_CHARSET
          Font.Color = clBlue
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
          Visible = False
        end
        object txtCustom1: TcxDBTextEdit
          Left = 15
          Top = 24
          BeepOnEnter = False
          DataBinding.DataField = 'CUSTOM1'
          Properties.BeepOnError = True
          TabOrder = 0
          Visible = False
          OnKeyDown = EnterKakoTab
          Width = 458
        end
        object txtCustom2: TcxDBTextEdit
          Left = 15
          Top = 66
          BeepOnEnter = False
          DataBinding.DataField = 'CUSTOM2'
          Properties.BeepOnError = True
          TabOrder = 1
          Visible = False
          OnKeyDown = EnterKakoTab
          Width = 458
        end
        object txtCustom3: TcxDBTextEdit
          Left = 15
          Top = 108
          BeepOnEnter = False
          DataBinding.DataField = 'CUSTOM3'
          Properties.BeepOnError = True
          TabOrder = 2
          Visible = False
          OnKeyDown = EnterKakoTab
          Width = 458
        end
        object ZapisiButtonCustom: TcxButton
          Left = 824
          Top = 115
          Width = 75
          Height = 25
          Action = aZapisi
          Anchors = [akRight, akBottom]
          Colors.Pressed = clGradientActiveCaption
          TabOrder = 3
        end
        object cxButton4: TcxButton
          Left = 905
          Top = 115
          Width = 75
          Height = 25
          Action = aOtkazi
          Anchors = [akRight, akBottom]
          Colors.Pressed = clGradientActiveCaption
          TabOrder = 4
        end
      end
    end
  end
  object Panel1: TPanel
    Left = 0
    Top = 360
    Width = 1000
    Height = 124
    Align = alBottom
    TabOrder = 4
    object cxGrid2: TcxGrid
      Left = 1
      Top = 1
      Width = 998
      Height = 122
      Align = alClient
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      PopupMenu = PopupMenu1
      TabOrder = 0
      RootLevelOptions.DetailTabsPosition = dtpTop
      object cxGridDBTableView1: TcxGridDBTableView
        Navigator.Buttons.CustomButtons = <>
        DataController.DataSource = dm.dsPogledNormativ
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        FilterRow.InfoText = #1050#1083#1080#1082#1085#1077#1090#1077' '#1090#1091#1082#1072' '#1079#1072' '#1076#1072' '#1076#1077#1092#1080#1085#1080#1088#1072#1090#1077' '#1092#1080#1083#1090#1077#1088
        OptionsBehavior.IncSearch = True
        OptionsCustomize.ColumnsQuickCustomization = True
        OptionsData.Deleting = False
        OptionsData.Inserting = False
        OptionsSelection.MultiSelect = True
        OptionsSelection.HideFocusRectOnExit = False
        OptionsSelection.HideSelection = True
        OptionsView.NoDataToDisplayInfoText = '<'#1053#1077#1084#1072' '#1087#1086#1076#1072#1090#1086#1094#1080'>'
        OptionsView.GroupByBox = False
        object cxGrid1DBTableView1VID_ARTIKAL_N: TcxGridDBColumn
          Caption = #1042#1080#1076' '#1085#1072' '#1089#1091#1088#1086#1074#1080#1085#1072
          DataBinding.FieldName = 'VID_ARTIKAL_N'
          Options.Editing = False
          Width = 26
        end
        object cxGrid1DBTableView1ARTIKAL_N: TcxGridDBColumn
          Caption = #1064#1080#1092#1088#1072' '#1085#1072' '#1089#1091#1088#1086#1074#1080#1085#1072
          DataBinding.FieldName = 'ARTIKAL_N'
          Options.Editing = False
          Width = 42
        end
        object cxGrid1DBTableView1NAZIV_SUROVINA_N: TcxGridDBColumn
          Caption = #1057#1091#1088#1086#1074#1080#1085#1072
          DataBinding.FieldName = 'NAZIV_SUROVINA_N'
          Options.Editing = False
          Width = 423
        end
        object cxGrid1DBTableView1KOLICINA_N: TcxGridDBColumn
          Caption = #1050#1086#1083#1080#1095#1080#1085#1072
          DataBinding.FieldName = 'KOLICINA_N'
          Options.Editing = False
          Width = 74
        end
        object cxGridDBTableView1RASTUR: TcxGridDBColumn
          Caption = #1050#1086#1083#1080#1095#1080#1085#1072
          DataBinding.FieldName = 'RASTUR'
        end
        object cxGrid1DBTableView1MERKA_N: TcxGridDBColumn
          Caption = #1052#1077#1088#1082#1072
          DataBinding.FieldName = 'MERKA_N'
          Options.Editing = False
          Width = 113
        end
        object cxGridDBTableView1VNESI: TcxGridDBColumn
          Caption = #1042#1085#1077#1089#1080' '#1089#1091#1088#1086#1074#1080#1085#1072
          DataBinding.ValueType = 'String'
          PropertiesClassName = 'TcxButtonEditProperties'
          Properties.Alignment.Horz = taCenter
          Properties.Buttons = <
            item
              Action = aVnesiOdNor
              Caption = '+'
              Kind = bkText
            end>
          Properties.Images = dmRes.cxIcoDBSmall
          Properties.ReadOnly = True
          Properties.ViewStyle = vsButtonsAutoWidth
          HeaderHint = #1042#1085#1077#1089#1080' '#1089#1091#1088#1086#1074#1080#1085#1072' '#1086#1076' '#1085#1086#1088#1084#1072#1090#1080#1074
          Options.ShowEditButtons = isebAlways
          Styles.Content = dmRes.cxStyle208
          Width = 103
        end
      end
      object cxGrid1DBTableView2: TcxGridDBTableView
        Navigator.Buttons.CustomButtons = <>
        DataController.DataSource = dm.dsIzberiPStavki
        DataController.DetailKeyFieldNames = #1064#1080#1092#1088#1072
        DataController.KeyFieldNames = 'ID_PORACKA'
        DataController.MasterKeyFieldNames = 'ID'
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        FilterRow.InfoText = #1050#1083#1080#1082#1085#1090#1077#1090#1077' '#1090#1091#1082#1072' '#1079#1072' '#1076#1072' '#1076#1077#1092#1080#1085#1080#1088#1072#1090#1077' '#1092#1080#1083#1090#1077#1088
        FilterRow.Visible = True
        OptionsBehavior.IncSearch = True
        OptionsCustomize.ColumnsQuickCustomization = True
        OptionsData.Deleting = False
        OptionsData.Inserting = False
        OptionsView.GroupByBox = False
        object cxGrid1DBTableView2DBColumn: TcxGridDBColumn
          DataBinding.FieldName = #1064#1080#1092#1088#1072
          Options.Editing = False
          Width = 74
        end
        object cxGrid1DBTableView2DBColumn1: TcxGridDBColumn
          DataBinding.FieldName = #1041#1088#1086#1112
          Options.Editing = False
          Width = 48
        end
        object cxGrid1DBTableView2ID_PORACKA: TcxGridDBColumn
          DataBinding.FieldName = 'ID_PORACKA'
          Visible = False
          Options.Editing = False
          Width = 74
        end
        object cxGrid1DBTableView2DBColumn2: TcxGridDBColumn
          DataBinding.FieldName = #1042#1080#1076' '#1085#1072' '#1087#1088#1086#1080#1079#1074#1086#1076
          Options.Editing = False
          Width = 31
        end
        object cxGrid1DBTableView2DBColumn3: TcxGridDBColumn
          DataBinding.FieldName = #1064#1080#1092#1088#1072' '#1085#1072' '#1087#1088#1086#1080#1079#1074#1086#1076
          Options.Editing = False
          Width = 43
        end
        object cxGrid1DBTableView2DBColumn4: TcxGridDBColumn
          DataBinding.FieldName = #1055#1088#1086#1080#1079#1074#1086#1076
          Options.Editing = False
          Width = 481
        end
        object cxGrid1DBTableView2DBColumn5: TcxGridDBColumn
          DataBinding.FieldName = #1057#1083#1080#1082#1072
          Options.Editing = False
          Width = 74
        end
        object cxGrid1DBTableView2DBColumn6: TcxGridDBColumn
          DataBinding.FieldName = #1058#1072#1087#1072#1094#1080#1088' '#1089#1077#1076#1080#1096#1090#1077
          Options.Editing = False
          Width = 172
        end
        object cxGrid1DBTableView2DBColumn7: TcxGridDBColumn
          DataBinding.FieldName = #1058#1072#1087#1072#1094#1080#1088' '#1085#1072#1079#1072#1076
          Options.Editing = False
          Width = 182
        end
        object cxGrid1DBTableView2DBColumn8: TcxGridDBColumn
          DataBinding.FieldName = #1057#1091#1085#1107#1077#1088
          Options.Editing = False
          Width = 134
        end
        object cxGrid1DBTableView2DBColumn9: TcxGridDBColumn
          DataBinding.FieldName = #1041#1088#1086#1112' '#1085#1072' '#1085#1086#1075#1072#1088#1082#1080
          Options.Editing = False
          Width = 207
        end
        object cxGrid1DBTableView2LABELS: TcxGridDBColumn
          DataBinding.FieldName = 'LABELS'
          Options.Editing = False
          Width = 144
        end
        object cxGrid1DBTableView2GLIDERS: TcxGridDBColumn
          DataBinding.FieldName = 'GLIDERS'
          Options.Editing = False
          Width = 150
        end
        object cxGrid1DBTableView2DBColumn10: TcxGridDBColumn
          DataBinding.FieldName = #1050#1086#1083#1080#1095#1080#1085#1072
          Options.Editing = False
          Width = 74
        end
        object cxGrid1DBTableView2DBColumn11: TcxGridDBColumn
          DataBinding.FieldName = #1047#1072#1073#1077#1083#1077#1096#1082#1072
          Options.Editing = False
          Width = 188
        end
        object cxGrid1DBTableView2DBColumn12: TcxGridDBColumn
          DataBinding.FieldName = #1054#1076' '#1076#1072#1090#1091#1084' '#1085#1072' '#1080#1089#1087#1086#1088#1072#1082#1072
          Options.Editing = False
          Width = 74
        end
        object cxGrid1DBTableView2DBColumn13: TcxGridDBColumn
          DataBinding.FieldName = #1044#1086' '#1076#1072#1090#1091#1084' '#1085#1072' '#1080#1089#1087#1086#1088#1072#1082#1072
          Options.Editing = False
          Width = 74
        end
        object cxGrid1DBTableView2STATUS: TcxGridDBColumn
          DataBinding.FieldName = 'STATUS'
          Visible = False
          Options.Editing = False
          Width = 74
        end
        object cxGrid1DBTableView2Column1: TcxGridDBColumn
          Caption = #1048#1079#1073#1077#1088#1080
          DataBinding.ValueType = 'String'
          PropertiesClassName = 'TcxCheckBoxProperties'
          Properties.ValueChecked = '1'
          Properties.ValueGrayed = '2'
          Properties.ValueUnchecked = '0'
        end
      end
      object cxGridLevel1: TcxGridLevel
        Caption = 
          #1057#1091#1088#1086#1074#1080#1085#1080', '#1089#1087#1086#1088#1077#1076' '#1085#1086#1088#1084#1072#1090#1080#1074#1086#1090' '#1085#1072' '#1087#1088#1086#1080#1079#1074#1086#1076#1086#1090' , '#1082#1086#1080' '#1085#1077' '#1089#1077' '#1074#1085#1077#1089#1077#1085#1080' '#1074#1086 +
          ' '#1087#1086#1090#1088#1086#1096#1077#1085#1080' '#1084#1072#1090#1077#1088#1080#1112#1072#1083#1080
        GridView = cxGridDBTableView1
      end
    end
  end
  object cxGrid1: TcxGrid
    Left = 0
    Top = 126
    Width = 1000
    Height = 234
    Align = alClient
    TabOrder = 5
    object cxGrid1DBTableView1: TcxGridDBTableView
      Navigator.Buttons.CustomButtons = <>
      DataController.DataSource = dsOutStavka
      DataController.Options = [dcoAnsiSort, dcoAssignGroupingValues, dcoAssignMasterDetailKeys, dcoSaveExpanding, dcoImmediatePost]
      DataController.Summary.DefaultGroupSummaryItems = <>
      DataController.Summary.FooterSummaryItems = <
        item
          Kind = skCount
        end
        item
          Kind = skCount
          Column = cxGrid1DBTableView1ART_NAZIV
        end>
      DataController.Summary.SummaryGroups = <>
      Filtering.ColumnFilteredItemsList = True
      FilterRow.InfoText = #1050#1083#1080#1082#1085#1080' '#1090#1091#1082#1072' '#1079#1072' '#1076#1072' '#1076#1077#1092#1080#1085#1080#1088#1072#1096' '#1092#1080#1083#1090#1077#1088
      FilterRow.Visible = True
      FilterRow.ApplyChanges = fracImmediately
      OptionsBehavior.IncSearch = True
      OptionsCustomize.ColumnsQuickCustomization = True
      OptionsData.Deleting = False
      OptionsData.Editing = False
      OptionsData.Inserting = False
      OptionsSelection.HideFocusRectOnExit = False
      OptionsView.NoDataToDisplayInfoText = '<'#1053#1077#1084#1072' '#1084#1072#1090#1077#1088#1080#1112#1072#1083#1080' '#1074#1086' '#1077#1074#1080#1076#1077#1085#1094#1080#1112#1072#1090#1072'>'
      OptionsView.Footer = True
      OptionsView.GroupByBox = False
      object cxGrid1DBTableView1ID: TcxGridDBColumn
        DataBinding.FieldName = 'ID'
        Visible = False
      end
      object cxGrid1DBTableView1SLEDLIVOST_KOLICINA: TcxGridDBColumn
        Caption = #1057#1083#1077#1076#1083#1080#1074#1086#1089#1090' '#1082#1086#1083'.'
        DataBinding.FieldName = 'SLEDLIVOST_KOLICINA'
        PropertiesClassName = 'TcxImageComboBoxProperties'
        Properties.Images = dmRes.cxImageGrid
        Properties.Items = <
          item
            Description = #1085#1077' '#1077' '#1074#1086' '#1088#1077#1076
            ImageIndex = 3
            Value = 0
          end
          item
            Description = #1074#1086' '#1088#1077#1076
            ImageIndex = 2
            Value = 1
          end>
        Width = 90
      end
      object cxGrid1DBTableView1MTR_OUT_ID: TcxGridDBColumn
        DataBinding.FieldName = 'MTR_OUT_ID'
        Visible = False
      end
      object cxGrid1DBTableView1ART_VID: TcxGridDBColumn
        DataBinding.FieldName = 'ART_VID'
        Visible = False
      end
      object cxGrid1DBTableView1ART_SIF: TcxGridDBColumn
        DataBinding.FieldName = 'ART_SIF'
        Visible = False
      end
      object cxGrid1DBTableView1ART_NAZIV: TcxGridDBColumn
        DataBinding.FieldName = 'ART_NAZIV'
        Width = 300
      end
      object cxGrid1DBTableView1KOLICINA: TcxGridDBColumn
        DataBinding.FieldName = 'KOLICINA'
      end
      object cxGrid1DBTableView1IZV_EM: TcxGridDBColumn
        DataBinding.FieldName = 'IZV_EM'
        Width = 93
      end
      object cxGrid1DBTableView1IZVEDENA_EM: TcxGridDBColumn
        DataBinding.FieldName = 'IZVEDENA_EM'
      end
      object cxGrid1DBTableView1PAKETI: TcxGridDBColumn
        DataBinding.FieldName = 'PAKETI'
      end
      object cxGrid1DBTableView1BR_PAKET: TcxGridDBColumn
        DataBinding.FieldName = 'BR_PAKET'
        Width = 84
      end
      object cxGrid1DBTableView1CENA_SO_DDV: TcxGridDBColumn
        Tag = 1
        DataBinding.FieldName = 'CENA_SO_DDV'
        Visible = False
        VisibleForCustomization = False
        Width = 100
      end
      object cxGrid1DBTableView1CENA_BEZ_DDV: TcxGridDBColumn
        Tag = 1
        DataBinding.FieldName = 'CENA_BEZ_DDV'
        Visible = False
        VisibleForCustomization = False
        Width = 100
      end
      object cxGrid1DBTableView1DANOK_PR: TcxGridDBColumn
        Tag = 1
        DataBinding.FieldName = 'DANOK_PR'
        Visible = False
        VisibleForCustomization = False
        Width = 87
      end
      object cxGrid1DBTableView1RABAT_PR: TcxGridDBColumn
        Tag = 1
        DataBinding.FieldName = 'RABAT_PR'
        Visible = False
        VisibleForCustomization = False
        Width = 90
      end
      object cxGrid1DBTableView1DANOK_IZNOS: TcxGridDBColumn
        Tag = 1
        DataBinding.FieldName = 'DANOK_IZNOS'
        Visible = False
        VisibleForCustomization = False
        Width = 100
      end
      object cxGrid1DBTableView1RABAT_IZNOS: TcxGridDBColumn
        Tag = 1
        DataBinding.FieldName = 'RABAT_IZNOS'
        Visible = False
        VisibleForCustomization = False
        Width = 100
      end
      object cxGrid1DBTableView1IZNOS_SO_DDV: TcxGridDBColumn
        Tag = 1
        DataBinding.FieldName = 'IZNOS_SO_DDV'
        Visible = False
        VisibleForCustomization = False
        Width = 100
      end
      object cxGrid1DBTableView1IZNOS_BEZ_DDV: TcxGridDBColumn
        Tag = 1
        DataBinding.FieldName = 'IZNOS_BEZ_DDV'
        Visible = False
        VisibleForCustomization = False
        Width = 100
      end
      object cxGrid1DBTableView1KNIG_CENA: TcxGridDBColumn
        Tag = 2
        DataBinding.FieldName = 'KNIG_CENA'
        Visible = False
        VisibleForCustomization = False
        Width = 100
      end
      object cxGrid1DBTableView1KNIG_IZNOS: TcxGridDBColumn
        Tag = 2
        DataBinding.FieldName = 'KNIG_IZNOS'
        Visible = False
        VisibleForCustomization = False
        Width = 100
      end
      object cxGrid1DBTableView1LOT_NO: TcxGridDBColumn
        DataBinding.FieldName = 'LOT_NO'
        Visible = False
      end
      object cxGrid1DBTableView1VAZI_DO: TcxGridDBColumn
        DataBinding.FieldName = 'VAZI_DO'
        Width = 85
      end
      object cxGrid1DBTableView1TS_INS: TcxGridDBColumn
        DataBinding.FieldName = 'TS_INS'
        Visible = False
      end
      object cxGrid1DBTableView1TS_UPD: TcxGridDBColumn
        DataBinding.FieldName = 'TS_UPD'
        Visible = False
        Width = 100
      end
      object cxGrid1DBTableView1USR_INS: TcxGridDBColumn
        DataBinding.FieldName = 'USR_INS'
        Visible = False
        Width = 100
      end
      object cxGrid1DBTableView1USR_UPD: TcxGridDBColumn
        DataBinding.FieldName = 'USR_UPD'
        Visible = False
        Width = 100
      end
      object cxGrid1DBTableView1CUSTOM1: TcxGridDBColumn
        DataBinding.FieldName = 'CUSTOM1'
        Visible = False
        Width = 150
      end
      object cxGrid1DBTableView1CUSTOM2: TcxGridDBColumn
        DataBinding.FieldName = 'CUSTOM2'
        Visible = False
        Width = 150
      end
      object cxGrid1DBTableView1CUSTOM3: TcxGridDBColumn
        DataBinding.FieldName = 'CUSTOM3'
        Visible = False
        Width = 150
      end
      object cxGrid1DBTableView1REF_ID: TcxGridDBColumn
        DataBinding.FieldName = 'REF_ID'
        Visible = False
      end
      object cxGrid1DBTableView1OPIS: TcxGridDBColumn
        DataBinding.FieldName = 'OPIS'
        Width = 250
      end
      object cxGrid1DBTableView1EBROJ: TcxGridDBColumn
        DataBinding.FieldName = 'EBROJ'
        Visible = False
      end
      object cxGrid1DBTableView1EGOD: TcxGridDBColumn
        DataBinding.FieldName = 'EGOD'
        Visible = False
      end
      object cxGrid1DBTableView1RN_BROJ_GOD: TcxGridDBColumn
        DataBinding.FieldName = 'RN_BROJ_GOD'
        Visible = False
        Width = 100
      end
    end
    object cxGrid1Level1: TcxGridLevel
      GridView = cxGrid1DBTableView1
    end
  end
  object cxGridPopupMenu1: TcxGridPopupMenu
    PopupMenus = <>
    Left = 152
    Top = 184
  end
  object PopupMenu1: TPopupMenu
    Left = 160
    Top = 288
    object N1: TMenuItem
      Action = aVnesiOdNor
    end
  end
  object dxBarManager1: TdxBarManager
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Segoe UI'
    Font.Style = []
    Categories.Strings = (
      'Default')
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    ImageOptions.Images = dmRes.cxSmallImages
    ImageOptions.LargeImages = dmRes.cxLargeImages
    PopupMenuLinks = <>
    UseSystemFont = True
    Left = 104
    Top = 240
    PixelsPerInch = 96
    object dxBarManager1Bar1: TdxBar
      CaptionButtons = <>
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 715
      FloatTop = 157
      FloatClientWidth = 51
      FloatClientHeight = 120
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton1'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton2'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton24'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarLargeButton3'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar2: TdxBar
      Caption = #1040#1082#1094#1080#1080
      CaptionButtons = <>
      DockedLeft = 239
      DockedTop = 0
      FloatLeft = 789
      FloatTop = 120
      FloatClientWidth = 51
      FloatClientHeight = 120
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton26'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton25'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarLargeButton5'
        end
        item
          Visible = True
          ItemName = 'dxBarSubItem1'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar3: TdxBar
      Caption = #1048#1079#1083#1077#1079
      CaptionButtons = <>
      DockedLeft = 544
      DockedTop = 0
      FloatLeft = 718
      FloatTop = 62
      FloatClientWidth = 51
      FloatClientHeight = 80
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton7'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton8'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar4: TdxBar
      Caption = #1055#1077#1095#1072#1090#1077#1114#1077
      CaptionButtons = <>
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 676
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton12'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton15'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton9'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarLargeButton13'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1BarSnimiIzgled: TdxBar
      Caption = #1058#1072#1073#1077#1083#1072
      CaptionButtons = <>
      DockedLeft = 363
      DockedTop = 0
      FloatLeft = 868
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLBtnSnimiIzgled'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarLBtnBrisiIzgled'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar5: TdxBar
      Caption = 'Custom 1'
      CaptionButtons = <>
      DockedLeft = 566
      DockedTop = 0
      FloatLeft = 868
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton18'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarLargeButton2: TdxBarLargeButton
      Action = aAzuriraj
      Category = 0
      Hint = #1040#1078#1091#1088#1080#1088#1072#1112'  '#1072#1088#1090#1080#1082#1072#1083'/'#1087#1088#1086#1080#1079#1074#1086#1076
    end
    object dxBarLargeButton3: TdxBarLargeButton
      Action = aBrisi
      Category = 0
      Hint = #1041#1088#1080#1096#1080'  '#1072#1088#1090#1080#1082#1072#1083'/'#1087#1088#1086#1080#1079#1074#1086#1076
    end
    object dxBarLargeButton4: TdxBarLargeButton
      Action = aSnimiIzgled
      Category = 0
      ScreenTip = tipSnimiIzgled
    end
    object dxBarLargeButton5: TdxBarLargeButton
      Action = aZacuvajExcel
      Category = 0
      ScreenTip = tipZacuvajExcel
    end
    object dxBarLargeButton6: TdxBarLargeButton
      Action = aPecatiTabela
      Caption = #1055#1077#1095#1072#1090#1080' '#1090#1072#1073#1077#1083#1072
      Category = 0
    end
    object dxBarLargeButton7: TdxBarLargeButton
      Action = aHelp
      Category = 0
    end
    object dxBarLargeButton8: TdxBarLargeButton
      Action = aIzlez
      Category = 0
    end
    object dxBarLargeButton1: TdxBarLargeButton
      Action = aNov
      Category = 0
      Hint = #1053#1086#1074' '#1084#1072#1090#1077#1088#1080#1112#1072#1083'/'#1089#1091#1088#1086#1074#1080#1085#1072
    end
    object dxBarLargeButton9: TdxBarLargeButton
      Action = aSnimiPecatenje
      Category = 0
      ScreenTip = tipSnimiKonf
    end
    object dxBarLargeButton11: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object ribbonFilterReset: TdxBarLargeButton
      Caption = #1056#1077#1089#1077#1090#1080#1088#1072#1112' '#1092#1080#1083#1090#1077#1088
      Category = 0
      Hint = #1056#1077#1089#1077#1090#1080#1088#1072#1112' '#1092#1080#1083#1090#1077#1088
      Visible = ivAlways
    end
    object dxBarLargeButton12: TdxBarLargeButton
      Action = aPodesuvanjePecatenje
      Category = 0
      ScreenTip = tipDizajnReport
    end
    object dxBarLargeButton13: TdxBarLargeButton
      Action = aBrisiPodesuvanjePecatenje
      Category = 0
      ScreenTip = tipBrisiKonf
    end
    object dxBarLargeButton14: TdxBarLargeButton
      Caption = #1048#1079#1073#1088#1080#1096#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      Category = 0
      Visible = ivAlways
      LargeImageIndex = 40
    end
    object dxBarLargeButton15: TdxBarLargeButton
      Action = aPageSetup
      Category = 0
      ScreenTip = tipSetiranjeStrana
    end
    object dxBarLargeButton16: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object dxBarLargeButton17: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object cxBarEditItem1: TcxBarEditItem
      Caption = 'New Item'
      Category = 0
      Hint = 'New Item'
      Visible = ivAlways
      PropertiesClassName = 'TcxCheckBoxProperties'
    end
    object dxBarSubItem1: TdxBarSubItem
      Caption = #1055#1077#1095#1072#1090#1077#1114#1077
      Category = 0
      Visible = ivAlways
      ImageIndex = 30
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton6'
        end>
    end
    object dxBarLBtnSnimiIzgled: TdxBarLargeButton
      Action = aSnimiIzgled
      Category = 0
    end
    object dxBarLBtnBrisiIzgled: TdxBarLargeButton
      Action = aBrisiIzgled
      Category = 0
    end
    object dxBarLargeButton10: TdxBarLargeButton
      Action = aCustomFieldsForm
      Category = 0
    end
    object dxBarLargeButton18: TdxBarLargeButton
      Action = aCustomFieldsForm
      Category = 0
    end
    object dxBarLargeButton19: TdxBarLargeButton
      Action = aBarajEvidencija
      Category = 0
    end
    object dxBarLargeButton20: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object dxBarLargeButton21: TdxBarLargeButton
      Action = aBrisiEvidencija
      Category = 0
    end
    object dxBarLargeButton22: TdxBarLargeButton
      Action = aNovaEvidencija
      Category = 0
    end
    object dxBarLargeButton23: TdxBarLargeButton
      Action = aMagacin
      Category = 0
    end
    object dxBarLargeButton24: TdxBarLargeButton
      Action = aOtvoriSerii
      Caption = 'LOT '#1073#1088#1086#1077#1074#1080
      Category = 0
      Hint = #1055#1088#1077#1075#1083#1077#1076' '#1085#1072' '#1089#1077#1088#1080#1080' '#1079#1072'  '#1072#1088#1090#1080#1082#1072#1083'/'#1087#1088#1086#1080#1079#1074#1086#1076
    end
    object dxBarLargeButton25: TdxBarLargeButton
      Action = aUpstream
      Category = 0
      Visible = ivNever
    end
    object dxBarLargeButton26: TdxBarLargeButton
      Action = aSurovinaOdN
      Category = 0
      Hint = #1055#1088#1077#1079#1077#1084#1080' '#1089#1091#1088#1086#1074#1080#1085#1080' '#1086#1076' '#1085#1086#1088#1084#1072#1090#1080#1074
    end
  end
  object ActionList1: TActionList
    Images = dmRes.cxSmallImages
    Left = 56
    Top = 192
    object aNov: TAction
      Caption = #1053#1086#1074
      ImageIndex = 90
      ShortCut = 116
      OnExecute = aNovExecute
    end
    object aAzuriraj: TAction
      Caption = #1040#1078#1091#1088#1080#1088#1072#1112
      ImageIndex = 92
      ShortCut = 117
      OnExecute = aAzurirajExecute
    end
    object aBrisi: TAction
      Caption = #1041#1088#1080#1096#1080
      ImageIndex = 91
      ShortCut = 119
      OnExecute = aBrisiExecute
    end
    object aSnimiIzgled: TAction
      Caption = #1057#1085#1080#1084#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      ImageIndex = 9
      ShortCut = 24659
      OnExecute = aSnimiIzgledExecute
    end
    object aZacuvajExcel: TAction
      Caption = #1047#1072#1095#1091#1074#1072#1112' '#1074#1086' Excel'
      ImageIndex = 9
      ShortCut = 24645
      OnExecute = aZacuvajExcelExecute
    end
    object aPecatiTabela: TAction
      Caption = #1055#1077#1095#1072#1090#1080
      ImageIndex = 30
      OnExecute = aPecatiTabelaExecute
    end
    object aHelp: TAction
      Caption = #1055#1086#1084#1086#1096
      ImageIndex = 24
    end
    object aIzlez: TAction
      Caption = #1048#1079#1083#1077#1079
      ImageIndex = 25
      OnExecute = aIzlezExecute
    end
    object aZapisi: TAction
      Caption = #1047#1072#1087#1080#1096#1080
      ImageIndex = 7
      ShortCut = 120
      OnExecute = aZapisiExecute
    end
    object aOtkazi: TAction
      Caption = #1054#1090#1082#1072#1078#1080
      ImageIndex = 21
      ShortCut = 27
      OnExecute = aOtkaziExecute
    end
    object aRefresh: TAction
      Caption = 'aRefresh'
      ImageIndex = 18
      ShortCut = 118
      OnExecute = aRefreshExecute
    end
    object aSnimiPecatenje: TAction
      Caption = #1057#1085#1080#1084#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      ImageIndex = 39
      OnExecute = aSnimiPecatenjeExecute
    end
    object aPodesuvanjePecatenje: TAction
      Caption = #1044#1080#1079#1072#1112#1085' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090
      ImageIndex = 38
      OnExecute = aPodesuvanjePecatenjeExecute
    end
    object aBrisiPodesuvanjePecatenje: TAction
      Caption = #1041#1088#1080#1096#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      ImageIndex = 37
      OnExecute = aBrisiPodesuvanjePecatenjeExecute
    end
    object aPageSetup: TAction
      Caption = #1057#1077#1090#1080#1088#1072#1114#1077' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072
      ImageIndex = 38
      OnExecute = aPageSetupExecute
    end
    object aFormConfig: TAction
      Caption = 'aFormConfig'
      ShortCut = 24699
      OnExecute = aFormConfigExecute
    end
    object aBrisiIzgled: TAction
      Caption = #1041#1088#1080#1096#1080' '#1075#1086' '#1079#1072#1095#1091#1074#1072#1085#1080#1086#1090' '#1080#1079#1075#1083#1077#1076
      ImageIndex = 56
      OnExecute = aBrisiIzgledExecute
    end
    object aHideFields: TAction
      Caption = 'aHideFields'
      OnExecute = aHideFieldsExecute
    end
    object aCustomFieldsForm: TAction
      Caption = 'Custom Fields'
      ImageIndex = 59
      OnExecute = aCustomFieldsFormExecute
    end
    object aResetIzvedenaEM: TAction
      Caption = 'aResetIzvedenaEM'
      OnExecute = aResetIzvedenaEMExecute
    end
    object aNovaEvidencija: TAction
      Caption = #1053#1086#1074#1072
      ImageIndex = 10
      ShortCut = 16500
    end
    object aBrisiEvidencija: TAction
      Caption = #1041#1088#1080#1096#1080
      ImageIndex = 11
      ShortCut = 16503
    end
    object aBarajEvidencija: TAction
      Caption = #1041#1072#1088#1072#1112
      ImageIndex = 15
      ShortCut = 115
    end
    object aMagacin: TAction
      Caption = #1052#1072#1075#1072#1094#1080#1085
      Hint = #1055#1088#1086#1084#1077#1085#1080' '#1075#1086' '#1084#1086#1084#1077#1085#1090#1072#1083#1085#1080#1086#1090' '#1084#1072#1075#1072#1094#1080#1085
      ImageIndex = 53
      ShortCut = 113
      OnExecute = aMagacinExecute
    end
    object aResetNurkovci: TAction
      Caption = 'aResetNurkovci'
      OnExecute = aResetNurkovciExecute
    end
    object aKreiraj: TAction
      Caption = #1050#1088#1077#1080#1088#1072#1112
      ImageIndex = 7
    end
    object aOtvoriSerii: TAction
      Caption = #1055#1088#1077#1075#1083#1077#1076' '#1085#1072' '#1089#1077#1088#1080#1080
      ImageIndex = 35
      OnExecute = aOtvoriSeriiExecute
    end
    object aZemiArtikalInfo: TAction
      Caption = 'aZemiArtikalInfo'
      ImageIndex = 42
      OnExecute = aZemiArtikalInfoExecute
    end
    object aUpstream: TAction
      Caption = 'aUpstream'
      OnExecute = aUpstreamExecute
    end
    object aDesigner: TAction
      Caption = 'aDesigner'
      ShortCut = 24697
      OnExecute = aDesignerExecute
    end
    object aSurovinaOdN: TAction
      Caption = #1057#1091#1088#1086#1074#1080#1085#1072' '#1086#1076' '#1085#1086#1088#1084#1072#1090#1080#1074
      ImageIndex = 17
      OnExecute = aSurovinaOdNExecute
    end
    object aVnesiOdNor: TAction
      Caption = #1042#1085#1077#1089#1080' '#1086#1076' '#1085#1086#1088#1084#1072#1090#1080#1074
      OnExecute = aVnesiOdNorExecute
    end
    object Action1: TAction
      Caption = 'LOT '#1073#1088#1086#1077#1074#1080
    end
    object aLikvidiranaEvidencija: TAction
      Caption = 'aLikvidiranaEvidencija'
      OnExecute = aLikvidiranaEvidencijaExecute
    end
    object aZemiMerka: TAction
      Caption = 'aZemiMerka'
      OnExecute = aZemiMerkaExecute
    end
  end
  object dxBarScreenTipRepository1: TdxBarScreenTipRepository
    Left = 112
    Top = 320
    PixelsPerInch = 96
    object tipSnimiIzgled: TdxBarScreenTip
      Header.Text = #1057#1085#1080#1084#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      Description.Text = #1047#1072#1095#1091#1074#1072#1112' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090' '#1085#1072' '#1092#1086#1088#1084#1072#1090#1072' '#1080' '#1085#1072' '#1090#1072#1073#1077#1083#1072#1090#1072
    end
    object tipZacuvajExcel: TdxBarScreenTip
      Header.Text = #1047#1072#1095#1091#1074#1072#1112' '#1074#1086' Excel'
      Description.Text = #1057#1085#1080#1084#1080' '#1075#1080' '#1087#1086#1076#1072#1090#1086#1094#1080#1090#1077' '#1086#1076' '#1090#1072#1073#1077#1083#1072#1090#1072' '#1074#1086' Excel '#1092#1086#1088#1084#1072#1090
    end
    object tipDizajnReport: TdxBarScreenTip
      Header.Text = #1044#1080#1079#1072#1112#1085' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090
      Description.Text = #1055#1086#1076#1077#1089#1091#1074#1072#1114#1072' '#1085#1072' '#1080#1079#1075#1083#1077#1076#1086#1090' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090' '#1085#1072' '#1090#1072#1073#1077#1083#1072#1090#1072
    end
    object tipSetiranjeStrana: TdxBarScreenTip
      Header.Text = #1057#1077#1090#1080#1088#1072#1114#1077' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072
      Description.Text = #1055#1086#1076#1077#1089#1091#1074#1072#1114#1077' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072' '#1085#1072' '#1082#1086#1112#1072' '#1116#1077' '#1089#1077' '#1087#1077#1095#1072#1090#1080' '#1090#1072#1073#1077#1083#1072#1090#1072
    end
    object tipSnimiKonf: TdxBarScreenTip
      Header.Text = #1057#1085#1080#1084#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      Description.Text = #1047#1072#1095#1091#1074#1072#1112' '#1075#1080' '#1087#1086#1076#1077#1089#1091#1074#1072#1114#1072#1090#1072' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090' '#1080' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072' '#1079#1072' '#1087#1077#1095#1072#1090#1077#1114#1077
    end
    object tipBrisiKonf: TdxBarScreenTip
      Header.Text = #1041#1088#1080#1096#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      Description.Text = #1048#1079#1073#1088#1080#1096#1080' '#1075#1080' '#1087#1086#1076#1077#1089#1091#1074#1072#1114#1072#1090#1072' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090' '#1080' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072' '#1079#1072' '#1087#1077#1095#1072#1090#1077#1114#1077
    end
  end
  object dxComponentPrinter1: TdxComponentPrinter
    CurrentLink = dxComponentPrinter1Link1
    Version = 0
    Left = 48
    Top = 280
    object dxComponentPrinter1Link1: TdxGridReportLink
      DateFormat = 0
      PageNumberFormat = pnfNumeral
      PrinterPage.DMPaper = 9
      PrinterPage.Footer = 6350
      PrinterPage.GrayShading = True
      PrinterPage.Header = 6350
      PrinterPage.Margins.Bottom = 12700
      PrinterPage.Margins.Left = 12700
      PrinterPage.Margins.Right = 12700
      PrinterPage.Margins.Top = 18000
      PrinterPage.Orientation = poLandscape
      PrinterPage.PageFooter.RightTitle.Strings = (
        #1057#1090#1088'.[Page #] '#1086#1076' [Total Pages]')
      PrinterPage.PageHeader.RightTitle.Strings = (
        #1044#1072#1090#1091#1084' : [Date & Time Printed]')
      PrinterPage.PageSize.X = 210000
      PrinterPage.PageSize.Y = 297000
      PrinterPage._dxMeasurementUnits_ = 0
      PrinterPage._dxLastMU_ = 2
      ReportDocument.CreationDate = 40336.425359629630000000
      TimeFormat = 0
      OptionsFormatting.UseNativeStyles = True
      OptionsOnEveryPage.Caption = False
      OptionsOnEveryPage.FilterBar = False
      OptionsView.Caption = False
      OptionsView.FilterBar = False
      StyleRepository = dmRes.cxStyleRepositoryPrinting
      Styles.StyleSheet = dmRes.dxGridReportLinkStyleSheet
      BuiltInReportLink = True
    end
  end
  object qryArtikalInfo: TpFIBQuery
    Transaction = dmKon.qTransaction
    Database = dmKon.fibBaza
    SQL.Strings = (
      'select coalesce(A.MERKA, '#39#39') as MERKA,'
      '       coalesce(A.MERKA_KOLICINA, '#39#39') as MERKA_KOLICINA'
      'from MTR_ARTIKAL A'
      'where A.ARTVID = :ARTVID'
      '      and A.ID = :ARTSIF ')
    Left = 264
    Top = 208
    qoAutoCommit = True
    qoStartTransaction = True
  end
  object dsIzvedenaEM: TDataSource
    AutoEdit = False
    DataSet = tblIzvedenaEM
    Left = 256
    Top = 328
  end
  object tblIzvedenaEM: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE MTR_IZVEDENA_EM'
      'SET '
      '    ID = :ID,'
      '    ARTVID = :ARTVID,'
      '    ARTSIF = :ARTSIF,'
      '    IZVEDENA_EM = :IZVEDENA_EM,'
      '    KOEFICIENT = :KOEFICIENT,'
      '    DEFAULT_IZVEDENA_EM = :DEFAULT_IZVEDENA_EM'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    MTR_IZVEDENA_EM'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO MTR_IZVEDENA_EM('
      '    ID,'
      '    ARTVID,'
      '    ARTSIF,'
      '    IZVEDENA_EM,'
      '    KOEFICIENT,'
      '    DEFAULT_IZVEDENA_EM'
      ')'
      'VALUES('
      '    :ID,'
      '    :ARTVID,'
      '    :ARTSIF,'
      '    :IZVEDENA_EM,'
      '    :KOEFICIENT,'
      '    :DEFAULT_IZVEDENA_EM'
      ')')
    RefreshSQL.Strings = (
      'select IM.ID,'
      '       IM.ARTVID,'
      '       IM.ARTSIF,'
      '       IM.IZVEDENA_EM,'
      '       M.NAZIV as MERKA_NAZIV,'
      '       IM.KOEFICIENT,'
      '       IM.DEFAULT_IZVEDENA_EM'
      'from MTR_IZVEDENA_EM IM'
      'join MAT_MERKA M on M.ID = IM.IZVEDENA_EM'
      'where(  (IM.ARTVID = :ARTVID'
      
        '      and IM.ARTSIF = :ARTSIF) or ((cast(:ARTVID as integer) is ' +
        'null)'
      '      and (cast(:ARTSIF as integer) is null))'
      '     ) and (     IM.ID = :OLD_ID'
      '     )'
      '       ')
    SelectSQL.Strings = (
      'select IM.ID,'
      '       IM.ARTVID,'
      '       IM.ARTSIF,'
      '       IM.IZVEDENA_EM,'
      '       M.NAZIV as MERKA_NAZIV,'
      '       IM.KOEFICIENT,'
      '       IM.DEFAULT_IZVEDENA_EM'
      'from MTR_IZVEDENA_EM IM'
      'join MAT_MERKA M on M.ID = IM.IZVEDENA_EM'
      'where (IM.ARTVID = :ARTVID'
      
        '      and IM.ARTSIF = :ARTSIF) or ((cast(:ARTVID as integer) is ' +
        'null)'
      '      and (cast(:ARTSIF as integer) is null))   ')
    BeforeDelete = TabelaBeforeDelete
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 216
    Top = 328
    poSQLINT64ToBCD = True
    object tblIzvedenaEMID: TFIBIntegerField
      DisplayLabel = #1064#1080#1092'.'
      FieldName = 'ID'
    end
    object tblIzvedenaEMARTVID: TFIBIntegerField
      DisplayLabel = #1040#1088#1090'. '#1074#1080#1076
      FieldName = 'ARTVID'
    end
    object tblIzvedenaEMARTSIF: TFIBIntegerField
      DisplayLabel = #1040#1088#1090'. '#1096#1080#1092'.'
      FieldName = 'ARTSIF'
    end
    object tblIzvedenaEMIZVEDENA_EM: TFIBStringField
      DisplayLabel = #1048#1079#1074#1077#1076#1077#1085#1072' '#1077'.'#1084'.'
      FieldName = 'IZVEDENA_EM'
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblIzvedenaEMMERKA_NAZIV: TFIBStringField
      DisplayLabel = #1048#1079#1074#1077#1076#1077#1085#1072' '#1077'.'#1084'.'
      FieldName = 'MERKA_NAZIV'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblIzvedenaEMKOEFICIENT: TFIBBCDField
      DisplayLabel = #1050#1086#1077#1092#1080#1094#1080#1077#1085#1090
      FieldName = 'KOEFICIENT'
      Size = 8
    end
    object tblIzvedenaEMDEFAULT_IZVEDENA_EM: TFIBIntegerField
      DisplayLabel = #1054#1089#1085#1086#1074#1085#1072
      FieldName = 'DEFAULT_IZVEDENA_EM'
    end
  end
  object dsOutStavka: TDataSource
    DataSet = tblOutStavka
    Left = 680
    Top = 184
  end
  object tblOutStavka: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE MTR_OUT_S'
      'SET '
      '    ID = :ID,'
      '    MTR_OUT_ID = :MTR_OUT_ID,'
      '    ART_VID = :ART_VID,'
      '    ART_SIF = :ART_SIF,'
      '    KOLICINA = :KOLICINA,'
      '    PAKETI = :PAKETI,'
      '    IZV_EM = :IZV_EM,'
      '    BR_PAKET = :BR_PAKET,'
      '    CENA_SO_DDV = :CENA_SO_DDV,'
      '    CENA_BEZ_DDV = :CENA_BEZ_DDV,'
      '    DANOK_PR = :DANOK_PR,'
      '    RABAT_PR = :RABAT_PR,'
      '    DANOK_IZNOS = :DANOK_IZNOS,'
      '    RABAT_IZNOS = :RABAT_IZNOS,'
      '    IZNOS_SO_DDV = :IZNOS_SO_DDV,'
      '    IZNOS_BEZ_DDV = :IZNOS_BEZ_DDV,'
      '    KNIG_CENA = :KNIG_CENA,'
      '    KNIG_IZNOS = :KNIG_IZNOS,'
      '    VAZI_DO = :VAZI_DO,'
      '    LOT_NO = :LOT_NO,'
      '    REF_ID = :REF_ID,'
      '    OPIS = :OPIS,'
      '    SLEDLIVOST_KOLICINA = :SLEDLIVOST_KOLICINA,'
      '    CUSTOM1 = :CUSTOM1,'
      '    CUSTOM2 = :CUSTOM2,'
      '    CUSTOM3 = :CUSTOM3,'
      '    TS_INS = :TS_INS,'
      '    TS_UPD = :TS_UPD,'
      '    USR_INS = :USR_INS,'
      '    USR_UPD = :USR_UPD'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    MTR_OUT_S'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO MTR_OUT_S('
      '    ID,'
      '    MTR_OUT_ID,'
      '    ART_VID,'
      '    ART_SIF,'
      '    KOLICINA,'
      '    PAKETI,'
      '    IZV_EM,'
      '    BR_PAKET,'
      '    CENA_SO_DDV,'
      '    CENA_BEZ_DDV,'
      '    DANOK_PR,'
      '    RABAT_PR,'
      '    DANOK_IZNOS,'
      '    RABAT_IZNOS,'
      '    IZNOS_SO_DDV,'
      '    IZNOS_BEZ_DDV,'
      '    KNIG_CENA,'
      '    KNIG_IZNOS,'
      '    VAZI_DO,'
      '    LOT_NO,'
      '    REF_ID,'
      '    OPIS,'
      '    SLEDLIVOST_KOLICINA,'
      '    CUSTOM1,'
      '    CUSTOM2,'
      '    CUSTOM3,'
      '    TS_INS,'
      '    TS_UPD,'
      '    USR_INS,'
      '    USR_UPD'
      ')'
      'VALUES('
      '    :ID,'
      '    :MTR_OUT_ID,'
      '    :ART_VID,'
      '    :ART_SIF,'
      '    :KOLICINA,'
      '    :PAKETI,'
      '    :IZV_EM,'
      '    :BR_PAKET,'
      '    :CENA_SO_DDV,'
      '    :CENA_BEZ_DDV,'
      '    :DANOK_PR,'
      '    :RABAT_PR,'
      '    :DANOK_IZNOS,'
      '    :RABAT_IZNOS,'
      '    :IZNOS_SO_DDV,'
      '    :IZNOS_BEZ_DDV,'
      '    :KNIG_CENA,'
      '    :KNIG_IZNOS,'
      '    :VAZI_DO,'
      '    :LOT_NO,'
      '    :REF_ID,'
      '    :OPIS,'
      '    :SLEDLIVOST_KOLICINA,'
      '    :CUSTOM1,'
      '    :CUSTOM2,'
      '    :CUSTOM3,'
      '    :TS_INS,'
      '    :TS_UPD,'
      '    :USR_INS,'
      '    :USR_UPD'
      ')')
    RefreshSQL.Strings = (
      'select S.ID,'
      '       S.MTR_OUT_ID,'
      '       O.BROJ as EBROJ,'
      '       O.GODINA as EGOD,'
      '       RN.BROJ || '#39'/'#39' || RN.GODINA as RN_BROJ_GOD,'
      '       S.ART_VID,'
      '       S.ART_SIF,'
      '       A.NAZIV as ART_NAZIV,'
      '       A.MERKA as ART_MERKA,'
      '       A.MERKA_KOLICINA as ART_MERKA_KOL,'
      '       S.KOLICINA,'
      '       S.PAKETI,'
      '       S.IZV_EM,'
      '       IE.IZVEDENA_EM,'
      '       S.BR_PAKET,'
      '       S.CENA_SO_DDV,'
      '       S.CENA_BEZ_DDV,'
      '       S.DANOK_PR,'
      '       S.RABAT_PR,'
      '       S.DANOK_IZNOS,'
      '       S.RABAT_IZNOS,'
      '       S.IZNOS_SO_DDV,'
      '       S.IZNOS_BEZ_DDV,'
      '       S.KNIG_CENA,'
      '       S.KNIG_IZNOS,'
      '       S.VAZI_DO,'
      '       S.LOT_NO,'
      '       S.REF_ID,'
      '       S.OPIS,'
      '       S.SLEDLIVOST_KOLICINA,'
      '       S.CUSTOM1,'
      '       S.CUSTOM2,'
      '       S.CUSTOM3,'
      '       S.TS_INS,'
      '       S.TS_UPD,'
      '       S.USR_INS,'
      '       S.USR_UPD,'
      '       rns.lot lot_rns,'
      '       p.naziv naziv_proizvod'
      'from MTR_OUT_S S'
      'join MTR_OUT O on O.ID = S.MTR_OUT_ID'
      'join MTR_ARTIKAL A on A.ARTVID = S.ART_VID and A.ID = S.ART_SIF'
      'left join MAN_RABOTEN_NALOG RN on RN.ID = O.WO_REF_ID'
      
        'left join man_raboten_nalog_stavka rns on rns.id_raboten_nalog =' +
        ' rn.id'
      
        'left join MTR_ARTIKAL p on p.ARTVID = rns.vid_artikal and p.id =' +
        ' rns.artikal'
      ''
      'left join MTR_IZVEDENA_EM IE on IE.ID = S.IZV_EM'
      'where(  S.MTR_OUT_ID = :EVIDENCIJA_ID'
      '     ) and (     S.ID = :OLD_ID'
      '     )'
      '       ')
    SelectSQL.Strings = (
      'select S.ID,'
      '       S.MTR_OUT_ID,'
      '       O.BROJ as EBROJ,'
      '       O.GODINA as EGOD,'
      '       RN.BROJ || '#39'/'#39' || RN.GODINA as RN_BROJ_GOD,'
      '       S.ART_VID,'
      '       S.ART_SIF,'
      '       A.NAZIV as ART_NAZIV,'
      '       A.MERKA as ART_MERKA,'
      '       A.MERKA_KOLICINA as ART_MERKA_KOL,'
      '       S.KOLICINA,'
      '       S.PAKETI,'
      '       S.IZV_EM,'
      '       IE.IZVEDENA_EM,'
      '       S.BR_PAKET,'
      '       S.CENA_SO_DDV,'
      '       S.CENA_BEZ_DDV,'
      '       S.DANOK_PR,'
      '       S.RABAT_PR,'
      '       S.DANOK_IZNOS,'
      '       S.RABAT_IZNOS,'
      '       S.IZNOS_SO_DDV,'
      '       S.IZNOS_BEZ_DDV,'
      '       S.KNIG_CENA,'
      '       S.KNIG_IZNOS,'
      '       S.VAZI_DO,'
      '       S.LOT_NO,'
      '       S.REF_ID,'
      '       S.OPIS,'
      '       S.SLEDLIVOST_KOLICINA,'
      '       S.CUSTOM1,'
      '       S.CUSTOM2,'
      '       S.CUSTOM3,'
      '       S.TS_INS,'
      '       S.TS_UPD,'
      '       S.USR_INS,'
      '       S.USR_UPD,'
      '       rns.lot lot_rns,'
      '       p.naziv naziv_proizvod'
      'from MTR_OUT_S S'
      'join MTR_OUT O on O.ID = S.MTR_OUT_ID'
      'join MTR_ARTIKAL A on A.ARTVID = S.ART_VID and A.ID = S.ART_SIF'
      'left join MAN_RABOTEN_NALOG RN on RN.ID = O.WO_REF_ID'
      
        'left join man_raboten_nalog_stavka rns on rns.id_raboten_nalog =' +
        ' rn.id'
      
        'left join MTR_ARTIKAL p on p.ARTVID = rns.vid_artikal and p.id =' +
        ' rns.artikal'
      ''
      'left join MTR_IZVEDENA_EM IE on IE.ID = S.IZV_EM'
      'where S.MTR_OUT_ID = :EVIDENCIJA_ID   ')
    AutoUpdateOptions.UpdateTableName = 'MTR_OUT_S'
    AutoUpdateOptions.KeyFields = 'ID'
    AutoUpdateOptions.GeneratorName = 'GEN_MTR_OUT_S_ID'
    AutoUpdateOptions.WhenGetGenID = wgBeforePost
    BeforeDelete = TabelaBeforeDelete
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 768
    Top = 184
    poSQLINT64ToBCD = True
    object tblOutStavkaID: TFIBBCDField
      DisplayLabel = #1064#1080#1092'.'
      FieldName = 'ID'
      Size = 0
    end
    object tblOutStavkaMTR_OUT_ID: TFIBBCDField
      DisplayLabel = #1048#1089#1087'. '#1096#1080#1092'.'
      FieldName = 'MTR_OUT_ID'
      Size = 0
    end
    object tblOutStavkaART_VID: TFIBIntegerField
      DisplayLabel = #1040#1088#1090'. '#1074#1080#1076
      FieldName = 'ART_VID'
    end
    object tblOutStavkaART_SIF: TFIBIntegerField
      DisplayLabel = #1040#1088#1090'. '#1096#1080#1092'.'
      FieldName = 'ART_SIF'
    end
    object tblOutStavkaART_NAZIV: TFIBStringField
      DisplayLabel = #1040#1088#1090'. '#1085#1072#1079#1080#1074
      FieldName = 'ART_NAZIV'
      Size = 500
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblOutStavkaKOLICINA: TFIBFloatField
      DisplayLabel = #1050#1086#1083#1080#1095#1080#1085#1072
      FieldName = 'KOLICINA'
    end
    object tblOutStavkaPAKETI: TFIBIntegerField
      DisplayLabel = #1055#1072#1082#1077#1090#1080
      FieldName = 'PAKETI'
    end
    object tblOutStavkaIZV_EM: TFIBIntegerField
      DisplayLabel = #1048'.'#1077#1076'.'#1084#1077#1088#1082#1072' '#1096#1080#1092'.'
      FieldName = 'IZV_EM'
    end
    object tblOutStavkaIZVEDENA_EM: TFIBStringField
      DisplayLabel = #1048#1079#1074#1077#1076#1077#1085#1072' '#1077#1076'. '#1084#1077#1088#1082#1072
      FieldName = 'IZVEDENA_EM'
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblOutStavkaBR_PAKET: TFIBBCDField
      DisplayLabel = #1041#1088'. '#1074#1086' '#1087#1072#1082#1077#1090
      FieldName = 'BR_PAKET'
      Size = 8
    end
    object tblOutStavkaCENA_SO_DDV: TFIBFloatField
      DisplayLabel = #1062#1077#1085#1072' '#1089#1086' '#1044#1044#1042
      FieldName = 'CENA_SO_DDV'
    end
    object tblOutStavkaCENA_BEZ_DDV: TFIBFloatField
      DisplayLabel = #1062#1077#1085#1072' '#1073#1077#1079' '#1044#1044#1042
      FieldName = 'CENA_BEZ_DDV'
    end
    object tblOutStavkaDANOK_PR: TFIBBCDField
      DisplayLabel = #1055#1088#1086#1094'. '#1085#1072' '#1076#1072#1085#1086#1082
      FieldName = 'DANOK_PR'
      Size = 2
    end
    object tblOutStavkaRABAT_PR: TFIBBCDField
      DisplayLabel = #1055#1088#1086#1094'. '#1084#1072' '#1088#1072#1073#1072#1090
      FieldName = 'RABAT_PR'
      Size = 2
    end
    object tblOutStavkaKNIG_CENA: TFIBFloatField
      DisplayLabel = #1050#1085#1080#1075'. '#1094#1077#1085#1072
      FieldName = 'KNIG_CENA'
    end
    object tblOutStavkaKNIG_IZNOS: TFIBBCDField
      DisplayLabel = #1050#1085#1080#1075'. '#1080#1079#1085#1086#1089
      FieldName = 'KNIG_IZNOS'
      Size = 2
    end
    object tblOutStavkaIZNOS_BEZ_DDV: TFIBBCDField
      DisplayLabel = #1048#1079#1085#1086#1089' '#1073#1077#1079' '#1044#1044#1042
      FieldName = 'IZNOS_BEZ_DDV'
      Size = 2
    end
    object tblOutStavkaIZNOS_SO_DDV: TFIBBCDField
      DisplayLabel = #1048#1079#1085#1086#1089' '#1089#1086' '#1044#1044#1042
      FieldName = 'IZNOS_SO_DDV'
      Size = 2
    end
    object tblOutStavkaRABAT_IZNOS: TFIBBCDField
      DisplayLabel = #1048#1079#1085#1086#1089' '#1085#1072' '#1088#1072#1073#1072#1090
      FieldName = 'RABAT_IZNOS'
      Size = 2
    end
    object tblOutStavkaDANOK_IZNOS: TFIBBCDField
      DisplayLabel = #1048#1079#1085#1086#1089' '#1085#1072' '#1044#1044#1042
      FieldName = 'DANOK_IZNOS'
      Size = 2
    end
    object tblOutStavkaLOT_NO: TFIBStringField
      DisplayLabel = 'LOT '#1073#1088'.'
      FieldName = 'LOT_NO'
      Size = 30
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblOutStavkaVAZI_DO: TFIBDateField
      DisplayLabel = #1042#1072#1078#1080' '#1076#1086
      FieldName = 'VAZI_DO'
    end
    object tblOutStavkaTS_INS: TFIBDateTimeField
      FieldName = 'TS_INS'
    end
    object tblOutStavkaTS_UPD: TFIBDateTimeField
      FieldName = 'TS_UPD'
    end
    object tblOutStavkaUSR_INS: TFIBStringField
      FieldName = 'USR_INS'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblOutStavkaUSR_UPD: TFIBStringField
      FieldName = 'USR_UPD'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblOutStavkaCUSTOM1: TFIBStringField
      FieldName = 'CUSTOM1'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblOutStavkaCUSTOM2: TFIBStringField
      FieldName = 'CUSTOM2'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblOutStavkaCUSTOM3: TFIBStringField
      FieldName = 'CUSTOM3'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblOutStavkaREF_ID: TFIBBCDField
      DisplayLabel = #1056#1077#1092'. ID'
      FieldName = 'REF_ID'
      Size = 0
    end
    object tblOutStavkaOPIS: TFIBStringField
      DisplayLabel = #1054#1087#1080#1089
      FieldName = 'OPIS'
      Size = 200
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblOutStavkaART_MERKA: TFIBStringField
      DisplayLabel = #1040#1088#1090'. '#1084#1077#1088#1082#1072
      FieldName = 'ART_MERKA'
      Size = 5
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblOutStavkaART_MERKA_KOL: TFIBStringField
      DisplayLabel = #1040#1088#1090'. '#1084#1077#1088#1082#1072' '#1082#1086#1083'.'
      FieldName = 'ART_MERKA_KOL'
      Size = 5
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblOutStavkaEBROJ: TFIBIntegerField
      DisplayLabel = #1045#1074#1080#1076'. '#1073#1088'.'
      FieldName = 'EBROJ'
    end
    object tblOutStavkaEGOD: TFIBIntegerField
      DisplayLabel = #1045#1074#1080#1076'. '#1075#1086#1076'.'
      FieldName = 'EGOD'
    end
    object tblOutStavkaRN_BROJ_GOD: TFIBStringField
      DisplayLabel = #1056#1072#1073'. '#1085#1072#1083#1086#1075'.'
      FieldName = 'RN_BROJ_GOD'
      Size = 112
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblOutStavkaSLEDLIVOST_KOLICINA: TFIBSmallIntField
      DisplayLabel = #1057#1083#1077#1076#1083#1080#1074#1086#1089#1090
      FieldName = 'SLEDLIVOST_KOLICINA'
    end
    object tblOutStavkaLOT_RNS: TFIBStringField
      FieldName = 'LOT_RNS'
      Size = 30
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblOutStavkaNAZIV_PROIZVOD: TFIBStringField
      FieldName = 'NAZIV_PROIZVOD'
      Size = 500
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object dsLagerArtikal: TDataSource
    DataSet = tblLagerArtikal
    Left = 384
    Top = 296
  end
  object tblLagerArtikal: TpFIBDataSet
    RefreshSQL.Strings = (
      'select SP.O_VLEZ,'
      '       SP.O_VLEZ_NEKONTROLIRAN,'
      '       SP.O_IZLEZ,'
      '       SP.O_LAGER,'
      '       SP.O_IZNOS_VLEZ,'
      '       SP.O_IZNOS_IZLEZ,'
      '       SP.O_IZNOS_DDV_VLEZ,'
      '       SP.O_IZNOS_DDV_IZLEZ,'
      '       A.TARIFA as ART_DDV,'
      '       A.MERKA as ART_MERKA,'
      '       A.ZALIHA as ART_ZALIHA'
      'from PROC_MTR_LAGER_ARTIKAL(:RE, :ARTVID, :ARTSIF, :DATUM) SP'
      'join MTR_ARTIKAL A on A.ARTVID = :ARTVID and A.ID = :ARTSIF  ')
    SelectSQL.Strings = (
      'select SP.O_VLEZ,'
      '       SP.O_VLEZ_NEKONTROLIRAN,'
      '       SP.O_IZLEZ,'
      '       SP.O_LAGER,'
      '       SP.O_IZNOS_VLEZ,'
      '       SP.O_IZNOS_IZLEZ,'
      '       SP.O_IZNOS_DDV_VLEZ,'
      '       SP.O_IZNOS_DDV_IZLEZ,'
      '       A.TARIFA as ART_DDV,'
      '       A.MERKA as ART_MERKA,'
      '       A.ZALIHA as ART_ZALIHA'
      'from PROC_MTR_LAGER_ARTIKAL(:RE, :ARTVID, :ARTSIF, :DATUM) SP'
      'join MTR_ARTIKAL A on A.ARTVID = :ARTVID and A.ID = :ARTSIF  ')
    BeforeDelete = TabelaBeforeDelete
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    DefaultFormats.DateTimeDisplayFormat = 'dd.mm.yyyy hh:mm'
    DefaultFormats.DisplayFormatTime = 'hh:mm'
    Left = 448
    Top = 296
    poSQLINT64ToBCD = True
    object tblLagerArtikalO_IZNOS_VLEZ: TFIBBCDField
      DisplayLabel = #1048#1079#1085#1086#1089' '#1074#1083#1077#1079
      FieldName = 'O_IZNOS_VLEZ'
      Size = 2
    end
    object tblLagerArtikalO_IZNOS_IZLEZ: TFIBBCDField
      DisplayLabel = #1048#1079#1085#1086#1089' '#1080#1079#1083#1077#1079
      FieldName = 'O_IZNOS_IZLEZ'
      Size = 2
    end
    object tblLagerArtikalO_IZNOS_DDV_VLEZ: TFIBBCDField
      DisplayLabel = #1044#1072#1085#1086#1082' '#1080#1079#1085#1086#1089' '#1074#1083#1077#1079
      FieldName = 'O_IZNOS_DDV_VLEZ'
      Size = 2
    end
    object tblLagerArtikalO_IZNOS_DDV_IZLEZ: TFIBBCDField
      DisplayLabel = #1044#1072#1085#1086#1082' '#1080#1079#1085#1086#1089' '#1080#1079#1083#1077#1079
      FieldName = 'O_IZNOS_DDV_IZLEZ'
      Size = 2
    end
    object tblLagerArtikalO_VLEZ: TFIBFloatField
      DisplayLabel = #1042#1083#1077#1079
      FieldName = 'O_VLEZ'
      DisplayFormat = '###,##0.00'
    end
    object tblLagerArtikalO_VLEZ_NEKONTROLIRAN: TFIBFloatField
      DisplayLabel = #1042#1083#1077#1079' '#1085#1077#1082#1086#1085#1090'.'
      FieldName = 'O_VLEZ_NEKONTROLIRAN'
      DisplayFormat = '###,##0.00'
    end
    object tblLagerArtikalO_IZLEZ: TFIBFloatField
      DisplayLabel = #1048#1079#1083#1077#1079
      FieldName = 'O_IZLEZ'
      DisplayFormat = '###,##0.00'
    end
    object tblLagerArtikalO_LAGER: TFIBFloatField
      DisplayLabel = #1051#1072#1075#1077#1088
      FieldName = 'O_LAGER'
      DisplayFormat = '###,###0.000'
      EditFormat = '0.000'
    end
    object tblLagerArtikalART_DDV: TFIBFloatField
      FieldName = 'ART_DDV'
    end
    object tblLagerArtikalART_MERKA: TFIBStringField
      FieldName = 'ART_MERKA'
      Size = 5
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblLagerArtikalART_ZALIHA: TFIBIntegerField
      FieldName = 'ART_ZALIHA'
    end
  end
  object Action2: TAction
    Caption = 'Action2'
  end
end
