inherited frmKontrolnaT: TfrmKontrolnaT
  Caption = #1050#1086#1085#1090#1088#1086#1083#1085#1072' '#1090#1086#1095#1082#1072
  ClientHeight = 558
  ExplicitWidth = 749
  ExplicitHeight = 597
  PixelsPerInch = 96
  TextHeight = 13
  inherited lPanel: TPanel
    Height = 255
    ExplicitHeight = 255
    inherited cxGrid1: TcxGrid
      Height = 251
      ExplicitHeight = 251
      inherited cxGrid1DBTableView1: TcxGridDBTableView
        DataController.DataSource = dm.dsKT
        object cxGrid1DBTableView1ID: TcxGridDBColumn
          DataBinding.FieldName = 'ID'
        end
        object cxGrid1DBTableView1NAZIV: TcxGridDBColumn
          Caption = #1053#1072#1079#1080#1074' '#1085#1072' '#1050#1058
          DataBinding.FieldName = 'NAZIV'
        end
        object cxGrid1DBTableView1ID_RE: TcxGridDBColumn
          DataBinding.FieldName = 'ID_RE'
        end
        object cxGrid1DBTableView1NAZIV_RE: TcxGridDBColumn
          Caption = #1053#1072#1079#1080#1074' '#1085#1072' '#1056#1045
          DataBinding.FieldName = 'NAZIV_RE'
          Width = 238
        end
        object cxGrid1DBTableView1AKTIVNA: TcxGridDBColumn
          Caption = #1040#1082#1090#1080#1074#1085#1072
          DataBinding.FieldName = 'AKTIVNA'
          PropertiesClassName = 'TcxCheckBoxProperties'
          Properties.NullStyle = nssInactive
          Properties.ValueChecked = 1
          Properties.ValueUnchecked = 0
        end
        object cxGrid1DBTableView1KT_KI: TcxGridDBColumn
          Caption = #1050#1086#1083#1080#1095#1080#1085#1072' '#1074#1083#1077#1075#1091#1074#1072' '#1074#1086' '#1080#1079#1083#1077#1079
          DataBinding.FieldName = 'KT_KI'
        end
      end
    end
  end
  inherited dPanel: TPanel
    Top = 381
    ExplicitTop = 381
    inherited Label1: TLabel
      Left = 79
      Visible = False
      ExplicitLeft = 79
    end
    object Label2: TLabel [1]
      Left = 79
      Top = 49
      Width = 50
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1053#1072#1079#1080#1074' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label3: TLabel [2]
      Left = 13
      Top = 76
      Width = 116
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1056#1072#1073#1086#1090#1085#1072' '#1077#1076#1080#1085#1080#1094#1072' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label4: TLabel [3]
      Left = 64
      Top = 100
      Width = 65
      Height = 29
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1059#1095#1077#1089#1090#1074#1091#1074#1072' '#1074#1086' '#1080#1079#1083#1077#1079' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      WordWrap = True
    end
    inherited Sifra: TcxDBTextEdit
      Tag = 0
      Left = 135
      DataBinding.DataField = 'ID'
      DataBinding.DataSource = dm.dsKT
      TabOrder = 7
      Visible = False
      ExplicitLeft = 135
    end
    inherited OtkaziButton: TcxButton
      TabOrder = 6
    end
    inherited ZapisiButton: TcxButton
      TabOrder = 5
    end
    object txtNaziv: TcxDBTextEdit
      Tag = 1
      Left = 135
      Top = 46
      BeepOnEnter = False
      DataBinding.DataField = 'NAZIV'
      DataBinding.DataSource = dm.dsKT
      ParentFont = False
      Properties.BeepOnError = True
      Properties.CharCase = ecUpperCase
      Style.Shadow = False
      TabOrder = 0
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 285
    end
    object txtRE: TcxDBTextEdit
      Left = 135
      Top = 73
      BeepOnEnter = False
      DataBinding.DataField = 'ID_RE'
      DataBinding.DataSource = dm.dsKT
      ParentFont = False
      Properties.BeepOnError = True
      Properties.CharCase = ecUpperCase
      Style.Shadow = False
      TabOrder = 1
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 48
    end
    object cbRE: TcxDBLookupComboBox
      Left = 183
      Top = 73
      DataBinding.DataField = 'ID_RE'
      DataBinding.DataSource = dm.dsKT
      Properties.KeyFieldNames = 'ID'
      Properties.ListColumns = <
        item
          FieldName = 'ID'
        end
        item
          FieldName = 'naziv'
        end>
      Properties.ListFieldIndex = 1
      Properties.ListSource = dm.dsRE
      TabOrder = 2
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 314
    end
    object cbAktivna: TcxDBCheckBox
      Left = 407
      Top = 100
      Caption = ' '#1040#1082#1090#1080#1074#1085#1072' :'
      DataBinding.DataField = 'AKTIVNA'
      DataBinding.DataSource = dm.dsKT
      ParentBackground = False
      ParentColor = False
      ParentFont = False
      Properties.Alignment = taRightJustify
      Properties.NullStyle = nssUnchecked
      Properties.ValueChecked = 1
      Properties.ValueUnchecked = 0
      Style.TextColor = clRed
      Style.TextStyle = [fsBold]
      TabOrder = 4
      Transparent = True
    end
    object cbIzlez: TcxDBImageComboBox
      Tag = 1
      Left = 135
      Top = 100
      Hint = #1044#1072#1083#1080' '#1074#1083#1077#1079#1086#1090' '#1074#1083#1077#1075#1091#1074#1072' '#1074#1086' '#1080#1079#1083#1077#1079#1086#1090
      DataBinding.DataField = 'KT_KI'
      DataBinding.DataSource = dm.dsKT
      Properties.Items = <
        item
          Description = #1044#1040
          ImageIndex = 0
          Value = 1
        end
        item
          Description = #1053#1045
          Value = 0
        end>
      TabOrder = 3
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 80
    end
  end
  inherited dxRibbon1: TdxRibbon
    inherited dxRibbon1Tab1: TdxRibbonTab
      Index = 0
    end
    inherited dxRibbon1Tab2: TdxRibbonTab
      Index = 1
    end
  end
  inherited StatusBar1: TdxRibbonStatusBar
    Top = 535
    ExplicitTop = 535
  end
  inherited dxBarManager1: TdxBarManager
    inherited dxBarManager1Bar1: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 69
      FloatClientHeight = 167
    end
    inherited dxBarManager1Bar2: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 149
      FloatClientHeight = 188
    end
    inherited dxBarManager1Bar3: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 59
      FloatClientHeight = 108
    end
    inherited dxBarManager1Bar4: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 141
      FloatClientHeight = 221
    end
    inherited dxBarManager1BarTabela: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 174
      FloatClientHeight = 113
    end
  end
  inherited dxComponentPrinter1: TdxComponentPrinter
    inherited dxComponentPrinter1Link1: TdxGridReportLink
      ReportDocument.CreationDate = 41179.525014247690000000
      BuiltInReportLink = True
    end
  end
end
