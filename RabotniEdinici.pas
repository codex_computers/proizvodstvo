unit RabotniEdinici;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData, cxEdit,
  DB, cxDBData, cxGridCustomPopupMenu, cxGridPopupMenu,
  ComCtrls, cxContainer, cxTextEdit, cxDBEdit, StdCtrls, cxGridLevel,
  cxClasses, cxControls, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGrid, ExtCtrls, Menus, ActnList,
  cxGridExportLink, cxExport, cxLookAndFeelPainters, cxDataStorage,
  cxButtons, dxStatusBar, dxRibbonStatusBar, dxRibbon, dxBar, dxBarExtItems,
  dxPSGlbl, dxPSUtl, dxPSEngn, dxPrnPg, dxBkgnd, dxWrap, dxPrnDev,
  dxPSCompsProvider, dxPSFillPatterns, dxPSEdgePatterns, dxPSCore, dxPScxCommon,
  cxPC, cxMemo, cxLocalization, cxLookAndFeels,
  dxPSPDFExportCore, dxPSPDFExport, cxDrawTextUtils, dxPSPrVwStd, dxPSPrVwAdv,
  dxPSPrVwRibbon, dxPScxEditorProducers, dxPScxExtEditorProducers,
  dxPScxPageControlProducer, dxPgsDlg, dxtree, dxdbtree, cxCheckBox, cxMaskEdit,
  cxDropDownEdit, cxLookupEdit, cxDBLookupEdit, cxDBLookupComboBox, dxPSTVLnk,
  dxPSdxDBTVLnk, cxDBExtLookupComboBox, dxSkinsCore, dxSkinBlack, dxSkinBlue,
  dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy,
  dxSkinGlassOceans, dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky,
  dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins, dxSkinOffice2007Black,
  dxSkinOffice2007Blue, dxSkinOffice2007Green, dxSkinOffice2007Pink,
  dxSkinOffice2007Silver, dxSkinPumpkin, dxSkinSeven, dxSkinSharp, dxSkinSilver,
  dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008, dxSkinsDefaultPainters,
  dxSkinValentine, dxSkinXmas2008Blue, dxSkinsdxRibbonPainter,
  dxSkinsdxBarPainter, dxSkinscxPCPainter, dxBarSkinnedCustForm,
  dxSkinOffice2010Black, dxSkinOffice2010Blue, dxSkinOffice2010Silver,
  dxRibbonSkins, dxPScxGridLnk, dxPScxGridLayoutViewLnk, dmUnit,
  dxSkinBlueprint, dxSkinDevExpressDarkStyle, dxSkinDevExpressStyle,
  dxSkinHighContrast, dxSkinSevenClassic, dxSkinSharpPlus,
  dxSkinTheAsphaltWorld, dxSkinVS2010, dxSkinWhiteprint, cxPCdxBarPopupMenu,
  cxLabel, cxDBLabel, cxGroupBox;

type
//  niza = Array[1..5] of Variant;

  TfrmRE = class(TForm)
    cxGridPopupMenu1: TcxGridPopupMenu;
    PopupMenu1: TPopupMenu;
    N1: TMenuItem;
    Excel1: TMenuItem;
    dxRibbon1Tab1: TdxRibbonTab;
    dxRibbon1: TdxRibbon;
    StatusBar1: TdxRibbonStatusBar;
    dxBarManager1: TdxBarManager;
    dxBarManager1Bar1: TdxBar;
    dxBarManager1Bar2: TdxBar;
    dxBarManager1Bar3: TdxBar;
    dxBarLargeButton2: TdxBarLargeButton;
    dxBarLargeButton3: TdxBarLargeButton;
    dxBarLargeButton4: TdxBarLargeButton;
    dxBarLargeButton5: TdxBarLargeButton;
    dxBarLargeButton6: TdxBarLargeButton;
    dxBarLargeButton7: TdxBarLargeButton;
    dxBarLargeButton8: TdxBarLargeButton;
    dxBarLargeButton1: TdxBarLargeButton;
    ActionList1: TActionList;
    aNov: TAction;
    aAzuriraj: TAction;
    aBrisi: TAction;
    aPecati: TAction;
    aHelp: TAction;
    aIzlez: TAction;
    aZapisi: TAction;
    aOtkazi: TAction;
    aRefresh: TAction;
    dxRibbon1Tab2: TdxRibbonTab;
    dxBarManager1Bar4: TdxBar;
    dxBarLargeButton9: TdxBarLargeButton;
    aSnimiPecatenje: TAction;
    dxComponentPrinter1: TdxComponentPrinter;
    dxBarLargeButton11: TdxBarLargeButton;
    ribbonFilterReset: TdxBarLargeButton;
    aPodesuvanjePecatenje: TAction;
    dxBarLargeButton12: TdxBarLargeButton;
    aBrisiPodesuvanjePecatenje: TAction;
    dxBarLargeButton13: TdxBarLargeButton;
    dxBarLargeButton14: TdxBarLargeButton;
    aPageSetup: TAction;
    dxBarLargeButton15: TdxBarLargeButton;
    dxBarManager1Bar5: TdxBar;
    dxBarLargeButton10: TdxBarLargeButton;
    dxBarLargeButton16: TdxBarLargeButton;
    aSnimiLayout: TAction;
    aBrisiLayout: TAction;
    aDizajnLayout: TAction;
    dxBarLargeButton17: TdxBarLargeButton;
    lPanel: TPanel;
    dxTree: TdxDBTreeView;
    dxComponentPrinter1Link1: TdxDBTreeViewReportLink;
    dxBarSubItem1: TdxBarSubItem;
    cxGridViewRepository1: TcxGridViewRepository;
    cxGridViewRepository1DBTableView1: TcxGridDBTableView;
    cxGridViewRepository1DBTableView1TIP_PARTNER: TcxGridDBColumn;
    cxGridViewRepository1DBTableView1ID: TcxGridDBColumn;
    cxGridViewRepository1DBTableView1NAZIV: TcxGridDBColumn;
    cxGridViewRepository1DBTableView1MESTO_NAZIV: TcxGridDBColumn;
    pcRabotniEdinici: TcxPageControl;
    tsRE: TcxTabSheet;
    tsPR: TcxTabSheet;
    dPanel: TPanel;
    Label1: TLabel;
    Label15: TLabel;
    Label5: TLabel;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Label14: TLabel;
    Sifra: TcxDBTextEdit;
    Naziv: TcxDBTextEdit;
    Koren: TcxDBTextEdit;
    Rakovoditel: TcxDBTextEdit;
    Poteklo: TcxDBTextEdit;
    Spisok: TcxDBTextEdit;
    TipPartner: TcxDBTextEdit;
    Partner: TcxDBTextEdit;
    ZapisiButton: TcxButton;
    OtkaziButton: TcxButton;
    cBoxExtPartner: TcxExtLookupComboBox;
    Panel1: TPanel;
    Label6: TLabel;
    cxDBTextEdit1: TcxDBTextEdit;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    gbRE: TcxGroupBox;
    Label7: TLabel;
    Label8: TLabel;
    Label9: TLabel;
    Label10: TLabel;
    txtRE: TcxDBTextEdit;
    cbRE: TcxDBLookupComboBox;
    cbAktiven: TcxDBCheckBox;
    txtOpis: TcxDBTextEdit;
    txtRabotnoVreme: TcxDBTextEdit;
    cxGroupBox2: TcxGroupBox;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    Label16: TLabel;
    Label18: TLabel;
    Label19: TLabel;
    Label20: TLabel;
    txtVremenskaME: TcxDBTextEdit;
    txtVremePocetok: TcxDBTextEdit;
    txtVremeKraj: TcxDBTextEdit;
    lblMernaEdinicaK: TcxDBLabel;
    txtVremeCiklus: TcxDBTextEdit;
    lblMernaEdinica2: TcxDBLabel;
    txtCena: TcxDBTextEdit;
    txtCenaCiklus: TcxDBTextEdit;
    txtFaktorCiklus: TcxDBTextEdit;
    txtKapacitetCiklus: TcxDBTextEdit;
    cbVremenskaME: TcxDBLookupComboBox;
    cxLabel2: TcxLabel;
    cxLabel1: TcxLabel;
    lblMernaEdinica: TcxDBLabel;
    cxButton3: TcxButton;
    cxButton4: TcxButton;
    Label17: TLabel;

    procedure cxDBTextEditAllEnter(Sender: TObject);
    procedure cxDBTextEditAllExit(Sender: TObject);
    procedure EnterKakoTab(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure PrvPosledenTab(panel:TPanel;var posledna,prva:TWinControl);
    procedure SaveToIniFileExecute(Sender: TObject);
    procedure ExportToExcelExecute(Sender: TObject);
    procedure cxGrid1DBTableView1KeyPress(Sender: TObject; var Key: Char);
    procedure aZapisiExecute(Sender: TObject);
    procedure aOtkaziExecute(Sender: TObject);
    procedure aNovExecute(Sender: TObject);
    procedure aAzurirajExecute(Sender: TObject);
    procedure aBrisiExecute(Sender: TObject);
    procedure aZacuvajExcelExecute(Sender: TObject);
    procedure aRefreshExecute(Sender: TObject);
    procedure aSnimiIzgledExecute(Sender: TObject);
    procedure aIzlezExecute(Sender: TObject);
    procedure aPecatiExecute(Sender: TObject);
    procedure aSnimiPecatenjeExecute(Sender: TObject);

    procedure zacuvajPrintVoIni(ime:AnsiString);
    procedure procitajPrintOdIni(ime:AnsiString);

//    procedure brisiPrintOdBaza(formName: string; ime:AnsiString);
//    procedure zacuvajPrintVoBaza(formName: string; ime:AnsiString);
//    procedure procitajPrintOdBaza(formName: string; ime:AnsiString);

    procedure ribbonFilterResetClick(Sender: TObject);
    procedure aPodesuvanjePecatenjeExecute(Sender: TObject);
    procedure aBrisiPodesuvanjePecatenjeExecute(Sender: TObject);
    procedure aPageSetupExecute(Sender: TObject);
    procedure cxGrid1DBTableView1FilterCustomization(
      Sender: TcxCustomGridTableView; var ADone: Boolean);
    procedure aSnimiLayoutExecute(Sender: TObject);
    procedure aBrisiLayoutExecute(Sender: TObject);
    procedure aDizajnLayoutExecute(Sender: TObject);
    procedure PartnerPropertiesEditValueChanged(Sender: TObject);
    procedure cBoxExtPartnerPropertiesEditValueChanged(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure dxTreeDblClick(Sender: TObject);
    procedure dxTreeKeyPress(Sender: TObject; var Key: Char);
    procedure pcRabotniEdiniciChange(Sender: TObject);
    procedure dxTreeChanging(Sender: TObject; Node: TTreeNode;
      var AllowChange: Boolean);
  private
    { Private declarations }

    _sifra_kluc :array[1..10] of string;  // �������� ���������� �� ������ �� ������� �� ������������� ���


  protected
    list : TList;
    inserting : boolean;
    prva, posledna :TWinControl;
    procedure prefrli;

  public
    { Public declarations }
    constructor Create(Owner : TComponent; insert : boolean = true);reintroduce; overload;

    ///////////////////////////////////////////////////////////
    procedure SetSifra(br: integer; s : string);
    function GetSifra(br: integer) : string;

    property sifra_kluc[br :integer]: string read GetSifra write SetSifra;  // ������� �� ���������� ������������ _sifra_kluc
    ///////////////////////////////////////////////////////////
  end;

var
  frmRE: TfrmRE;

implementation

uses DaNe, dmKonekcija, Utils, dmResources, dmMaticni;

{$R *.dfm}
//------------------------------------------------------------------------------

constructor TfrmRE.Create(Owner : TComponent; insert : boolean = true);
begin
inherited Create(Owner);
    inserting := insert;
end;
//------------------------------------------------------------------------------

procedure TfrmRE.aNovExecute(Sender: TObject);
begin
if pcRabotniEdinici.ActivePage = tsRE then
  begin
    dPanel.Enabled := true;
    prva.SetFocus;
    lPanel.Enabled := false;
    dxTree.DataSource.DataSet.Insert;
    dm.tblRETIP_PARTNER.Value := dmKon.firmaTP;
    dm.tblREPARTNER.Value := dmKon.firmaP;
  end
  else
  begin
    dPanel.Enabled:=False;
    //pcRabotniEdinici.ActivePage:=tsPR;
    Panel1.Enabled := true;
    txtOpis.SetFocus;
    dm.tblProizvodstvenResurs.Insert;
  end;
end;

procedure TfrmRE.aAzurirajExecute(Sender: TObject);
begin
if pcRabotniEdinici.ActivePage = tsRE then
  begin
    dPanel.Enabled := true;
    prva.SetFocus;
    lPanel.Enabled := false;
    dxTree.DataSource.DataSet.Edit;
  end
  else
  begin
    Panel1.Enabled := true;
    txtOpis.SetFocus;
    lPanel.Enabled := false;
    dm.tblProizvodstvenResurs.Edit;
  end;
end;

procedure TfrmRE.aBrisiExecute(Sender: TObject);
begin
 if pcRabotniEdinici.ActivePage = tsRE then
    dxTree.DataSource.DataSet.Delete()
 else
    dm.tblProizvodstvenResurs.Delete;
end;

procedure TfrmRE.aBrisiLayoutExecute(Sender: TObject);
begin
  //brisiLayoutOdBaza(Name,LayoutControl);
end;

procedure TfrmRE.aBrisiPodesuvanjePecatenjeExecute(Sender: TObject);
begin
  //brisiPrintOdBaza(Name,cxGrid1DBTableView1.Name, dxComponentPrinter1Link1);
end;

procedure TfrmRE.aDizajnLayoutExecute(Sender: TObject);
begin
  //LayoutControl.Customization := true;
end;

procedure TfrmRE.aRefreshExecute(Sender: TObject);
begin
  dxTree.DataSource.DataSet.Refresh;
end;

procedure TfrmRE.aIzlezExecute(Sender: TObject);
begin
   close; // proveri dali se zapisani podatocite
end;

procedure TfrmRE.aSnimiIzgledExecute(Sender: TObject);
begin
  //zacuvajGridVoBaza(Name,cxGrid1DBTableView1);
end;

procedure TfrmRE.aSnimiLayoutExecute(Sender: TObject);
begin
  //zacuvajLayoutVoBaza(Name,LayoutControl.Name,LayoutControl);
end;

procedure TfrmRE.aSnimiPecatenjeExecute(Sender: TObject);
begin
  //zacuvajPrintVoBaza(Name,cxGrid1DBTableView1.Name,dxComponentPrinter1Link1);
end;

procedure TfrmRE.aZacuvajExcelExecute(Sender: TObject);
begin
  //zacuvajVoExcel(cxGrid1, Caption);
end;

procedure TfrmRE.EnterKakoTab(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  st : TDataSetState;
  kom : TWinControl;
begin    kom := Sender as TWinControl;
    case Key of
        VK_DOWN:
        begin
            PostMessage(Handle,WM_NextDlgCtl,0,0);
        end;
        VK_UP:
        begin
            PostMessage(Handle,WM_NextDlgCtl,1,0);
        end;
        VK_RETURN:
        begin
          PostMessage(Handle, WM_NEXTDLGCTL,0,0);
        end
    end;
end;
//------------------------------------------------------------------------------

procedure TfrmRE.cxDBTextEditAllEnter(Sender: TObject);
begin
    TEdit(Sender).Color:=clSkyBlue;
//if pcRabotniEdinici.ActivePage = tsRE then
//begin
// if ((Sender is TcxDBTextEdit)) then
//  begin
//    dm.tblHint.Close;
//    dm.tblHint.ParamByName('tabela').Value:='MAT_RE';
//    dm.tblHint.ParamByName('pole').Value:=(Sender as TcxDBTextEdit).DataBinding.DataField;
//    dm.tblHint.Open;
//
//    (Sender as TcxDBTextEdit).Hint:=dm.tblHintRDBDESCRIPTION.AsString;
//  end;
// end
// else
// begin
//    if ((Sender is TcxDBTextEdit)) then
//     begin
//      dm.tblHint.Close;
//      dm.tblHint.ParamByName('tabela').Value:='MAN_PROIZVODSTVEN_RESURS';
//      dm.tblHint.ParamByName('pole').Value:=(Sender as TcxDBTextEdit).DataBinding.DataField;
//      dm.tblHint.Open;
//
//      (Sender as TcxDBTextEdit).Hint:=dm.tblHintRDBDESCRIPTION.AsString;
//     end;
 //end;
end;
//------------------------------------------------------------------------------

procedure TfrmRE.cxDBTextEditAllExit(Sender: TObject);
begin
    TEdit(Sender).Color:=clWhite;
end;

procedure TfrmRE.cxGrid1DBTableView1FilterCustomization(
  Sender: TcxCustomGridTableView; var ADone: Boolean);
begin

end;

//------------------------------------------------------------------------------
procedure TfrmRE.SetSifra(br: integer; s : string);
begin
  _sifra_kluc[br] := s;
end;

function TfrmRE.GetSifra(br: integer) : string;
begin
  Result := _sifra_kluc[br];
end;

procedure TfrmRE.PartnerPropertiesEditValueChanged(Sender: TObject);
begin
  if (TipPartner.Text <> '') and (Partner.Text <> '') then
  begin
    dmMat.tblPartner.Locate('TIP_PARTNER; ID', VarArrayOf([StrToInt(TipPartner.Text), StrToInt(Partner.Text)]) , []);
    cBoxExtPartner.Text := dmMat.tblPartnerNAZIV.Value;
  end;
end;

procedure TfrmRE.pcRabotniEdiniciChange(Sender: TObject);
begin
  if pcRabotniEdinici.ActivePage = tsRE then
     tsRE.Caption := '������� ������� ' +dm.tblRENAZIV.Value;
end;

procedure TfrmRE.prefrli;
begin
  if(not inserting) then
  begin
      SetSifra(0,IntToStr(dm.tblReID.Value));
      ModalResult := mrOk;
  end;
end;

//------------------------------------------------------------------------------
procedure TfrmRE.PrvPosledenTab(panel : TPanel; var posledna , prva : TWinControl);
var n:Smallint;
begin
    list := TList.Create;
    panel.GetTabOrderList(list);
    n:=List.Count;
    prva := TWinControl(list.First);
    posledna := TWinControl(list.Items[n-3]);
end;
//------------------------------------------------------------------------------
procedure TfrmRE.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    if (dxTree.DataSource.State = dsEdit) or (dxTree.DataSource.State = dsInsert) then
    begin
        frmDaNe := TfrmDaNe.Create(self, '���������� ��������', '���������� �� �� ��������. ���� ������ �� �� �������?', 1);
        if (frmDaNe.ShowModal <> mrYes) then
        begin
            dxTree.DataSource.DataSet.Cancel;
            Action := caFree;
        end
        else
        begin
          if (Validacija(dPanel) = false) then
          begin
            dxTree.DataSource.DataSet.Post;
            Action := caFree;
          end
          else Action := caNone;
        end;
    end;
end;

procedure TfrmRE.FormCreate(Sender: TObject);
begin
  ProcitajFormaIzgled(self);
  dxRibbon1.ColorSchemeName := dmRes.skin_name;
  dm.tblProizvodstvenResurs.Open;
end;

//------------------------------------------------------------------------------
procedure TfrmRE.FormShow(Sender: TObject);
begin
    PrvPosledenTab(dPanel,posledna,prva);
    dxBarManager1Bar1.Caption := Caption;

    PartnerPropertiesEditValueChanged(self);
    pcRabotniEdinici.ActivePage := tsRE;
    tsRE.Caption := '������� ������� ' +dm.tblRENAZIV.Value;
end;

//------------------------------------------------------------------------------
procedure TfrmRE.SaveToIniFileExecute(Sender: TObject);
begin

end;

//------------------------------------------------------------------------------
procedure TfrmRE.ExportToExcelExecute(Sender: TObject);
begin
    dmRes.SaveToExcelDialog.FileName := Caption + '-' + dmKon.user;
    if(dmRes.SaveToExcelDialog.Execute)then
//    ExportGridToExcel(dmRes.SaveToExcelDialog.FileName,cxGrid1,true, true, false);
end;
//------------------------------------------------------------------------------

procedure TfrmRE.cxGrid1DBTableView1KeyPress(Sender: TObject;
  var Key: Char);
begin
     //if(Ord(Key) <> VK_RETURN) then cxSortiraj(cxGrid1DBTableView1);
end;

procedure TfrmRE.dxTreeChanging(Sender: TObject; Node: TTreeNode;
  var AllowChange: Boolean);
begin
   tsRE.Caption := '������� ������� ' +dm.tblRENAZIV.Value;
end;

procedure TfrmRE.dxTreeDblClick(Sender: TObject);
begin
  prefrli;
end;

procedure TfrmRE.dxTreeKeyPress(Sender: TObject; var Key: Char);
begin
  if(Ord(Key) = VK_RETURN) then prefrli;
end;

procedure TfrmRE.ribbonFilterResetClick(Sender: TObject);
begin
  //
  //cxGrid1DBTableView1.DataController.Filter.Root.Clear;
end;

procedure TfrmRE.aZapisiExecute(Sender: TObject);
var
  st: TDataSetState;
begin

  st := dxTree.DataSource.State;
  if st in [dsEdit,dsInsert] then
  begin
    if (Validacija(dPanel) = false) then
    begin
      if ((st = dsInsert) //and inserting
      ) then
      begin
            dxTree.DataSource.DataSet.Post;
         // ��� ������������� ������
            dPanel.Enabled:=False;
            pcRabotniEdinici.ActivePage:=tsPR;
            Panel1.Enabled := true;
            txtopis.SetFocus;
            dm.tblProizvodstvenResurs.Insert;
      end;

//      if ((st = dsInsert) and (not inserting)) then
//      begin
//        dxTree.DataSource.DataSet.Post;
//        dPanel.Enabled:=false;
//        lPanel.Enabled:=true;
//        Panel1.Enabled := true;
//        txtRe.SetFocus;
//        dPanel.Enabled := false;
//        dm.tblProizvodstvenResurs.Edit;
//        //dxTree.SetFocus;
//
//      end;

      if (st = dsEdit) then
      begin
        dxTree.DataSource.DataSet.Post;
        dPanel.Enabled:=false;
        lPanel.Enabled:=true;
        dxTree.SetFocus;
      end;
      end;

   // end;

    end
    else
    if dm.tblProizvodstvenResurs.State in [dsEdit,dsInsert] then
    begin
       if (Validacija(Panel1) = false) then
    begin
      if ((dm.tblProizvodstvenResurs.State = dsInsert) //and inserting
      ) then
      begin
            dm.tblProizvodstvenResurs.Post;
            Panel1.Enabled := false;
            //txtRe.SetFocus;
            lPanel.Enabled := true;
            dxTree.SetFocus;
      end;

//      if ((st = dsInsert) and (not inserting)) then
//      begin
//        dxTree.DataSource.DataSet.Post;
//        dPanel.Enabled:=false;
//        lPanel.Enabled:=true;
//        Panel1.Enabled := true;
//        txtRe.SetFocus;
//        dPanel.Enabled := false;
//        dm.tblProizvodstvenResurs.Edit;
//        dxTree.SetFocus;
//
//      end;

      if (dm.tblProizvodstvenResurs.State = dsEdit) then
      begin
        dm.tblProizvodstvenResurs.Post;
        Panel1.Enabled:=false;
        lPanel.Enabled:=true;
        dxTree.SetFocus;
      end;
    end;
    end;

end;

procedure TfrmRE.cBoxExtPartnerPropertiesEditValueChanged(Sender: TObject);
begin
  if (dPanel.Enabled = true) then
  begin
    dm.tblReTIP_PARTNER.Value := dmmat.tblPartnerTIP_PARTNER.Value;
    dm.tblRePARTNER.Value := dmmat.tblPartnerID.Value;
  end;
end;

procedure TfrmRE.aOtkaziExecute(Sender: TObject);
begin
 if pcRabotniEdinici.ActivePage = tsRE then
 begin
    if (dxTree.DataSource.State = dsBrowse) then
    begin
        ModalResult := mrCancel;
        Close();
    end
    else
    begin
        dxTree.DataSource.DataSet.Cancel;
        RestoreControls(dPanel);
        dPanel.Enabled := false;
        lPanel.Enabled := true;
        dxTree.SetFocus;
    end;
 end
 else
 begin
    if (dm.tblProizvodstvenResurs.State = dsBrowse) then
    begin
        ModalResult := mrCancel;
        Close();
    end
    else
    begin
        dm.tblProizvodstvenResurs.Cancel;
        RestoreControls(Panel1);
        Panel1.Enabled := false;
        lPanel.Enabled := true;
        dxTree.SetFocus;
    end;
 end;
end;

procedure TfrmRE.aPageSetupExecute(Sender: TObject);
begin
  dxComponentPrinter1Link1.PageSetup;
end;

procedure TfrmRE.aPecatiExecute(Sender: TObject);
begin
  dxComponentPrinter1Link1.ReportTitle.Text := Caption;
  dxComponentPrinter1.Preview(true, dxComponentPrinter1Link1);
end;

procedure TfrmRE.aPodesuvanjePecatenjeExecute(Sender: TObject);
begin
  dxComponentPrinter1Link1.DesignReport();
end;

//-------------------------------------------------------------------------------------------
// ��������� �� ���������� �� ��������� � �������� �� Print �������� �� .ini ��������

procedure TfrmRE.zacuvajPrintVoIni(ime:AnsiString);
var
  AStream: TFileStream;
begin
  AStream := TFileStream.Create(dmKon.RabotenDir + ime + 'Print.ini', fmCreate);
  try
    AStream.WriteComponent(dxComponentPrinter1Link1);
  finally
    AStream.Free;
  end;

end;

//-------------------------------------------------------------------------------------------
// ��������� �� ������ �� ��������� � �������� �� Print �������� �������� �� .ini ��������

procedure TfrmRE.procitajPrintOdIni(ime:AnsiString);
var
  AStream: TFileStream;
begin
  AStream := TFileStream.Create(dmKon.RabotenDir + ime + 'Print.ini', fmOpenRead);
  try
    dxComponentPrinter1Link1.RestoreDefaults;
    AStream.ReadComponent(dxComponentPrinter1Link1);
  finally
    AStream.Free;
  end;
end;


end.
