unit ProizvodstvenResurs;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData, cxEdit,
  DB, cxDBData, cxGridCustomPopupMenu, cxGridPopupMenu,
  ComCtrls, cxContainer, cxTextEdit, cxDBEdit, StdCtrls, cxGridLevel,
  cxClasses, cxControls, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGrid, ExtCtrls, Menus, ActnList,
  cxGridExportLink, cxExport, cxLookAndFeelPainters, cxDataStorage,
  cxButtons, dxStatusBar, dxRibbonStatusBar, dxRibbon, dxBar, dxBarExtItems,
  dxPSGlbl, dxPSUtl, dxPSEngn, dxPrnPg, dxBkgnd, dxWrap, dxPrnDev,
  dxPSCompsProvider, dxPSFillPatterns, dxPSEdgePatterns, dxPSCore, dxPScxCommon,
  cxPC, cxMemo, cxLocalization, cxLookAndFeels,
  dxPSPDFExportCore, dxPSPDFExport, cxDrawTextUtils, dxPSPrVwStd, dxPSPrVwAdv,
  dxPSPrVwRibbon, dxPScxEditorProducers, dxPScxExtEditorProducers,
  dxPScxPageControlProducer, dxPgsDlg, dxtree, dxdbtree, cxCheckBox, cxMaskEdit,
  cxDropDownEdit, cxLookupEdit, cxDBLookupEdit, cxDBLookupComboBox, dxPSTVLnk,
  dxPSdxDBTVLnk, cxDBExtLookupComboBox, dxSkinsCore, dxSkinBlack, dxSkinBlue,
  dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy,
  dxSkinGlassOceans, dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky,
  dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins, dxSkinOffice2007Black,
  dxSkinOffice2007Blue, dxSkinOffice2007Green, dxSkinOffice2007Pink,
  dxSkinOffice2007Silver, dxSkinPumpkin, dxSkinSeven, dxSkinSharp, dxSkinSilver,
  dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008, dxSkinsDefaultPainters,
  dxSkinValentine, dxSkinXmas2008Blue, dxSkinsdxRibbonPainter,
  dxSkinsdxBarPainter, dxSkinscxPCPainter, dxBarSkinnedCustForm,
  dxSkinOffice2010Black, dxSkinOffice2010Blue, dxSkinOffice2010Silver,
  dxRibbonSkins, dxPScxGridLnk, dxPScxGridLayoutViewLnk, dmUnit,
  dxSkinBlueprint, dxSkinDevExpressDarkStyle, dxSkinDevExpressStyle,
  dxSkinHighContrast, dxSkinSevenClassic, dxSkinSharpPlus,
  dxSkinTheAsphaltWorld, dxSkinVS2010, dxSkinWhiteprint, cxGroupBox, cxLabel,
  cxDBLabel, Merka, dxScreenTip, dxCustomHint, cxHint, dxSkinMetropolis,
  dxSkinMetropolisDark, dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray,
  dxSkinOffice2013White, dxSkinOffice2016Colorful, dxSkinOffice2016Dark,
  dxSkinVisualStudio2013Blue, dxSkinVisualStudio2013Dark,
  dxSkinVisualStudio2013Light, dxRibbonCustomizationForm, cxNavigator,
  System.Actions;

type
//  niza = Array[1..5] of Variant;

  TfrmProizvodstvenResurs = class(TForm)
    cxGridPopupMenu1: TcxGridPopupMenu;
    PopupMenu1: TPopupMenu;
    N1: TMenuItem;
    Excel1: TMenuItem;
    dxRibbon1Tab1: TdxRibbonTab;
    dxRibbon1: TdxRibbon;
    StatusBar1: TdxRibbonStatusBar;
    dxBarManager1: TdxBarManager;
    dxBarManager1Bar1: TdxBar;
    dxBarManager1Bar2: TdxBar;
    dxBarManager1Bar3: TdxBar;
    dxBarLargeButton2: TdxBarLargeButton;
    dxBarLargeButton3: TdxBarLargeButton;
    dxBarLargeButton4: TdxBarLargeButton;
    dxBarLargeButton5: TdxBarLargeButton;
    dxBarLargeButton6: TdxBarLargeButton;
    dxBarLargeButton7: TdxBarLargeButton;
    dxBarLargeButton8: TdxBarLargeButton;
    dxBarLargeButton1: TdxBarLargeButton;
    ActionList1: TActionList;
    aNov: TAction;
    aAzuriraj: TAction;
    aBrisi: TAction;
    aPecati: TAction;
    aHelp: TAction;
    aIzlez: TAction;
    aZapisi: TAction;
    aOtkazi: TAction;
    aRefresh: TAction;
    dxRibbon1Tab2: TdxRibbonTab;
    dxBarManager1Bar4: TdxBar;
    dxBarLargeButton9: TdxBarLargeButton;
    aSnimiPecatenje: TAction;
    dxComponentPrinter1: TdxComponentPrinter;
    dxBarLargeButton11: TdxBarLargeButton;
    ribbonFilterReset: TdxBarLargeButton;
    aPodesuvanjePecatenje: TAction;
    dxBarLargeButton12: TdxBarLargeButton;
    aBrisiPodesuvanjePecatenje: TAction;
    dxBarLargeButton13: TdxBarLargeButton;
    dxBarLargeButton14: TdxBarLargeButton;
    aPageSetup: TAction;
    dxBarLargeButton15: TdxBarLargeButton;
    dxBarManager1Bar5: TdxBar;
    dxBarLargeButton10: TdxBarLargeButton;
    dxBarLargeButton16: TdxBarLargeButton;
    aSnimiLayout: TAction;
    aBrisiLayout: TAction;
    aDizajnLayout: TAction;
    dxBarLargeButton17: TdxBarLargeButton;
    lPanel: TPanel;
    dPanel: TPanel;
    Label1: TLabel;
    Sifra: TcxDBTextEdit;
    ZapisiButton: TcxButton;
    OtkaziButton: TcxButton;
    dxBarSubItem1: TdxBarSubItem;
    cxGridViewRepository1: TcxGridViewRepository;
    cxGridViewRepository1DBTableView1: TcxGridDBTableView;
    cxGridViewRepository1DBTableView1TIP_PARTNER: TcxGridDBColumn;
    cxGridViewRepository1DBTableView1ID: TcxGridDBColumn;
    cxGridViewRepository1DBTableView1NAZIV: TcxGridDBColumn;
    cxGridViewRepository1DBTableView1MESTO_NAZIV: TcxGridDBColumn;
    gbRE: TcxGroupBox;
    Label15: TLabel;
    txtRE: TcxDBTextEdit;
    cbRE: TcxDBLookupComboBox;
    cbAktiven: TcxDBCheckBox;
    cxGroupBox1: TcxGroupBox;
    cxGroupBox2: TcxGroupBox;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    Label2: TLabel;
    txtOpis: TcxDBTextEdit;
    Label3: TLabel;
    txtRabotnoVreme: TcxDBTextEdit;
    Label4: TLabel;
    Label5: TLabel;
    txtVremenskaME: TcxDBTextEdit;
    cbVremenskaME: TcxDBLookupComboBox;
    Label6: TLabel;
    txtVremePocetok: TcxDBTextEdit;
    Label7: TLabel;
    txtVremeKraj: TcxDBTextEdit;
    lblMernaEdinicaK: TcxDBLabel;
    Label8: TLabel;
    txtVremeCiklus: TcxDBTextEdit;
    lblMernaEdinica2: TcxDBLabel;
    txtCena: TcxDBTextEdit;
    Label10: TLabel;
    txtCenaCiklus: TcxDBTextEdit;
    Label11: TLabel;
    txtFaktorCiklus: TcxDBTextEdit;
    Label12: TLabel;
    txtKapacitetCiklus: TcxDBTextEdit;
    cxGrid1DBTableView1ID: TcxGridDBColumn;
    cxGrid1DBTableView1OPIS: TcxGridDBColumn;
    cxGrid1DBTableView1AKTIVEN: TcxGridDBColumn;
    cxGrid1DBTableView1MERNA_EDINICA: TcxGridDBColumn;
    cxGrid1DBTableView1VREME_ZA_POCETOK: TcxGridDBColumn;
    cxGrid1DBTableView1VREME_PO_ZAVRSUVANJE: TcxGridDBColumn;
    cxGrid1DBTableView1CENA_CAS: TcxGridDBColumn;
    cxGrid1DBTableView1VREME_CIKLUS: TcxGridDBColumn;
    cxGrid1DBTableView1CENA_CIKLUS: TcxGridDBColumn;
    cxGrid1DBTableView1NEDELNO_RABOTNO_VREME: TcxGridDBColumn;
    cxGrid1DBTableView1FAKTOR_CIKLUS: TcxGridDBColumn;
    cxGrid1DBTableView1KAPACITET_CIKLUS: TcxGridDBColumn;
    cxLabel1: TcxLabel;
    cxLabel2: TcxLabel;
    cxGrid1DBTableView1NAZIV: TcxGridDBColumn;
    dxComponentPrinter1Link1: TdxGridReportLink;
    cxHintStyleController1: TcxHintStyleController;
    Label13: TLabel;
    txtNaziv: TcxDBTextEdit;
    Label14: TLabel;
    txtTipPR: TcxDBTextEdit;
    cbTipPR: TcxDBLookupComboBox;
    aTipPR: TAction;
    lblMernaEdinica: TcxDBLabel;
    Label9: TLabel;

    procedure cxDBTextEditAllEnter(Sender: TObject);
    procedure cxDBTextEditAllExit(Sender: TObject);
    procedure EnterKakoTab(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    procedure PrvPosledenTab(panel:TPanel;var posledna,prva:TWinControl);
    procedure SaveToIniFileExecute(Sender: TObject);
    procedure ExportToExcelExecute(Sender: TObject);
    procedure cxGrid1DBTableView1KeyPress(Sender: TObject; var Key: Char);
    procedure aZapisiExecute(Sender: TObject);
    procedure aOtkaziExecute(Sender: TObject);
    procedure aNovExecute(Sender: TObject);
    procedure aAzurirajExecute(Sender: TObject);
    procedure aBrisiExecute(Sender: TObject);
    procedure aZacuvajExcelExecute(Sender: TObject);
    procedure aRefreshExecute(Sender: TObject);
    procedure aSnimiIzgledExecute(Sender: TObject);
    procedure aIzlezExecute(Sender: TObject);
    procedure aPecatiExecute(Sender: TObject);
    procedure aSnimiPecatenjeExecute(Sender: TObject);

    procedure zacuvajPrintVoIni(ime:AnsiString);
    procedure procitajPrintOdIni(ime:AnsiString);

//    procedure brisiPrintOdBaza(formName: string; ime:AnsiString);
//    procedure zacuvajPrintVoBaza(formName: string; ime:AnsiString);
//    procedure procitajPrintOdBaza(formName: string; ime:AnsiString);

    procedure ribbonFilterResetClick(Sender: TObject);
    procedure aPodesuvanjePecatenjeExecute(Sender: TObject);
    procedure aBrisiPodesuvanjePecatenjeExecute(Sender: TObject);
    procedure aPageSetupExecute(Sender: TObject);
    procedure cxGrid1DBTableView1FilterCustomization(
      Sender: TcxCustomGridTableView; var ADone: Boolean);
    procedure aSnimiLayoutExecute(Sender: TObject);
    procedure aBrisiLayoutExecute(Sender: TObject);
    procedure aDizajnLayoutExecute(Sender: TObject);
    procedure PartnerPropertiesEditValueChanged(Sender: TObject);
    procedure cBoxExtPartnerPropertiesEditValueChanged(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure dxTreeDblClick(Sender: TObject);
    procedure dxTreeKeyPress(Sender: TObject; var Key: Char);
    procedure cbVremenskaMEDblClick(Sender: TObject);
    procedure gbREClick(Sender: TObject);
    procedure aTipPRExecute(Sender: TObject);
  private
    { Private declarations }

    _sifra_kluc :array[1..10] of string;  // �������� ���������� �� ������ �� ������� �� ������������� ���


  protected
    list : TList;
    inserting : boolean;
    prva, posledna :TWinControl;
    procedure prefrli;

  public
    { Public declarations }
    constructor Create(Owner : TComponent; insert : boolean = true);reintroduce; overload;

    ///////////////////////////////////////////////////////////
    procedure SetSifra(br: integer; s : string);
    function GetSifra(br: integer) : string;

    property sifra_kluc[br :integer]: string read GetSifra write SetSifra;  // ������� �� ���������� ������������ _sifra_kluc
    ///////////////////////////////////////////////////////////
  end;

var
  frmProizvodstvenResurs: TfrmProizvodstvenResurs;

implementation

uses DaNe, dmKonekcija, Utils, dmResources, dmMaticni, TipProizvodstvenResurs;

{$R *.dfm}
//------------------------------------------------------------------------------

constructor TfrmProizvodstvenResurs.Create(Owner : TComponent; insert : boolean = true);
begin
inherited Create(Owner);
    inserting := insert;
end;
//------------------------------------------------------------------------------

procedure TfrmProizvodstvenResurs.aNovExecute(Sender: TObject);
begin
    dPanel.Enabled := true;
    txtTipPR.SetFocus;
    lPanel.Enabled := false;
    cxGrid1DBTableView1.DataController.DataSet.Insert;
  //  dmMat.tblRETIP_PARTNER.Value := dmKon.firmaTP;
   // dmMat.tblREPARTNER.Value := dmKon.firmaP;
end;

procedure TfrmProizvodstvenResurs.aAzurirajExecute(Sender: TObject);
begin
    dPanel.Enabled := true;
    txtTipPR.SetFocus;
    lPanel.Enabled := false;
    cxGrid1DBTableView1.DataController.DataSet.Edit;
end;

procedure TfrmProizvodstvenResurs.aBrisiExecute(Sender: TObject);
begin
  cxGrid1DBTableView1.DataController.DataSet.Delete();
end;

procedure TfrmProizvodstvenResurs.aBrisiLayoutExecute(Sender: TObject);
begin
  //brisiLayoutOdBaza(Name,LayoutControl);
end;

procedure TfrmProizvodstvenResurs.aBrisiPodesuvanjePecatenjeExecute(Sender: TObject);
begin
  //brisiPrintOdBaza(Name,cxGrid1DBTableView1.Name, dxComponentPrinter1Link1);
end;

procedure TfrmProizvodstvenResurs.aDizajnLayoutExecute(Sender: TObject);
begin
  //LayoutControl.Customization := true;
end;

procedure TfrmProizvodstvenResurs.aRefreshExecute(Sender: TObject);
begin
  cxGrid1DBTableView1.DataController.DataSet.Refresh;
end;

procedure TfrmProizvodstvenResurs.aIzlezExecute(Sender: TObject);
begin
   close; // proveri dali se zapisani podatocite
end;

procedure TfrmProizvodstvenResurs.aSnimiIzgledExecute(Sender: TObject);
begin
  zacuvajGridVoBaza(Name,cxGrid1DBTableView1);
end;

procedure TfrmProizvodstvenResurs.aSnimiLayoutExecute(Sender: TObject);
begin
  //zacuvajLayoutVoBaza(Name,LayoutControl.Name,LayoutControl);
end;

procedure TfrmProizvodstvenResurs.aSnimiPecatenjeExecute(Sender: TObject);
begin
  zacuvajPrintVoBaza(Name,cxGrid1DBTableView1.Name,dxComponentPrinter1Link1);
end;

procedure TfrmProizvodstvenResurs.aTipPRExecute(Sender: TObject);
begin
  if cbTipPR.Focused then
      begin
        frmTipPR := TfrmTipPR.Create(self, false);
        frmTipPR.ShowModal;
        if frmTipPR.ModalResult = mrOk then
           begin
             dm.tblProizvodstvenResursID_TIP_PROIZVODSTVEN_RESURS.Value := strtoint(frmTipPR.GetSifra(0));
             //dm.tblNormativARTIKAL.Value := strtoint(frmTipPR.GetSifra(1));
           end;
        frmTipPR.Free;
      end;

     if cbVremenskaME.Focused   then
      begin
        frmmerka := TfrmMerka.Create(self, false);
        frmMerka.ShowModal;
        if frmMerka.ModalResult = mrOk then
           begin
             dm.tblProizvodstvenResursMERNA_EDINICA.Value := frmMerka.GetSifra(0);
           end;
        frmMerka.Free;
      end
end;

procedure TfrmProizvodstvenResurs.aZacuvajExcelExecute(Sender: TObject);
begin
  //zacuvajVoExcel(cxGrid1, Caption);
end;

procedure TfrmProizvodstvenResurs.EnterKakoTab(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  st : TDataSetState;
  kom : TWinControl;
begin    kom := Sender as TWinControl;
    case Key of
        VK_DOWN:
        begin
            PostMessage(Handle,WM_NextDlgCtl,0,0);
        end;
        VK_UP:
        begin
            PostMessage(Handle,WM_NextDlgCtl,1,0);
        end;
        VK_RETURN:
        begin
          PostMessage(Handle, WM_NEXTDLGCTL,0,0);
        end
    end;
end;
//------------------------------------------------------------------------------

procedure TfrmProizvodstvenResurs.cxDBTextEditAllEnter(Sender: TObject);
begin
// if ((Sender is TcxDBTextEdit)) then
// begin
//  dm.tblHint.Close;
//  dm.tblHint.ParamByName('tabela').Value:='MAN_PROIZVODSTVEN_RESURS';
//  dm.tblHint.ParamByName('pole').Value:=(Sender as TcxDBTextEdit).DataBinding.DataField;
//  dm.tblHint.Open;
//
//  (Sender as TcxDBTextEdit).Hint:=dm.tblHintRDBDESCRIPTION.AsString;
// end;

    TEdit(Sender).Color:=clSkyBlue;
end;
//------------------------------------------------------------------------------

procedure TfrmProizvodstvenResurs.cxDBTextEditAllExit(Sender: TObject);
begin
    TEdit(Sender).Color:=clWhite;
end;

procedure TfrmProizvodstvenResurs.cxGrid1DBTableView1FilterCustomization(
  Sender: TcxCustomGridTableView; var ADone: Boolean);
begin

end;

//------------------------------------------------------------------------------
procedure TfrmProizvodstvenResurs.SetSifra(br: integer; s : string);
begin
  _sifra_kluc[br] := s;
end;

function TfrmProizvodstvenResurs.GetSifra(br: integer) : string;
begin
  Result := _sifra_kluc[br];
end;

procedure TfrmProizvodstvenResurs.PartnerPropertiesEditValueChanged(Sender: TObject);
begin
//  if (TipPartner.Text <> '') and (Partner.Text <> '') then
//  begin
//    dmMat.tblPartner.Locate('TIP_PARTNER; ID', VarArrayOf([StrToInt(TipPartner.Text), StrToInt(Partner.Text)]) , []);
//    cBoxExtPartner.Text := dmMat.tblPartnerNAZIV.Value;
//  end;
end;

procedure TfrmProizvodstvenResurs.prefrli;
begin
  if(not inserting) then
  begin
      SetSifra(0,IntToStr(dmMat.tblREID.Value));
      ModalResult := mrOk;
  end;
end;

//------------------------------------------------------------------------------
procedure TfrmProizvodstvenResurs.PrvPosledenTab(panel : TPanel; var posledna , prva : TWinControl);
var n:Smallint;
begin
    list := TList.Create;
    panel.GetTabOrderList(list);
    n:=List.Count;
    prva := TWinControl(list.First);
    posledna := TWinControl(list.Items[n-3]);
end;
//------------------------------------------------------------------------------
procedure TfrmProizvodstvenResurs.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    if (cxGrid1DBTableView1.DataController.DataSource.State = dsEdit) or (cxGrid1DBTableView1.DataController.DataSource.State = dsInsert) then
    begin
        frmDaNe := TfrmDaNe.Create(self, '���������� ��������', '���������� �� �� ��������. ���� ������ �� �� �������?', 1);
        if (frmDaNe.ShowModal <> mrYes) then
        begin
            cxGrid1DBTableView1.DataController.DataSet.Cancel;
            Action := caFree;
        end
        else
        begin
          if (Validacija(dPanel) = false) then
          begin
            cxGrid1DBTableView1.DataController.DataSet.Post;
            Action := caFree;
          end
          else Action := caNone;
        end;
    end;
end;

procedure TfrmProizvodstvenResurs.FormCreate(Sender: TObject);
begin
  ProcitajFormaIzgled(self);
  dxRibbon1.ColorSchemeName := dmRes.skin_name;
  dm.tblTipPR.Close;
  dm.tblTipPR.ParamByName('aktiven').AsInteger := 1;
  dm.tblTipPR.Open;
  dm.tblProizvodstvenResurs.CloseOpen(true);
  dm.tblRE.CloseOpen(true);
end;

//------------------------------------------------------------------------------
procedure TfrmProizvodstvenResurs.FormShow(Sender: TObject);
begin
    PrvPosledenTab(dPanel,posledna,prva);
    dxBarManager1Bar1.Caption := Caption;

   // PartnerPropertiesEditValueChanged(self);
end;

procedure TfrmProizvodstvenResurs.gbREClick(Sender: TObject);
begin

end;

//------------------------------------------------------------------------------
procedure TfrmProizvodstvenResurs.SaveToIniFileExecute(Sender: TObject);
begin

end;

//------------------------------------------------------------------------------
procedure TfrmProizvodstvenResurs.ExportToExcelExecute(Sender: TObject);
begin
    dmRes.SaveToExcelDialog.FileName := Caption + '-' + dmKon.user;
    if(dmRes.SaveToExcelDialog.Execute)then
//    ExportGridToExcel(dmRes.SaveToExcelDialog.FileName,cxGrid1,true, true, false);
end;
//------------------------------------------------------------------------------

procedure TfrmProizvodstvenResurs.cxGrid1DBTableView1KeyPress(Sender: TObject;
  var Key: Char);
begin
     //if(Ord(Key) <> VK_RETURN) then cxSortiraj(cxGrid1DBTableView1);
end;

procedure TfrmProizvodstvenResurs.dxTreeDblClick(Sender: TObject);
begin
  prefrli;
end;

procedure TfrmProizvodstvenResurs.dxTreeKeyPress(Sender: TObject; var Key: Char);
begin
  if(Ord(Key) = VK_RETURN) then prefrli;
end;

procedure TfrmProizvodstvenResurs.ribbonFilterResetClick(Sender: TObject);
begin
  //
  //cxGrid1DBTableView1.DataController.Filter.Root.Clear;
end;

procedure TfrmProizvodstvenResurs.aZapisiExecute(Sender: TObject);
var
  st: TDataSetState;
begin
  st := cxGrid1DBTableView1.DataController.datasource.State;
  if st in [dsEdit,dsInsert] then
  begin
    if (Validacija(dPanel) = false) then
    begin
      if ((st = dsInsert) and inserting) then
      begin
        cxGrid1DBTableView1.DataController.DataSet.Post;
        aNov.Execute;
      end;

      if ((st = dsInsert) and (not inserting)) then
      begin
        cxGrid1DBTableView1.DataController.DataSet.Post;
        dPanel.Enabled:=false;
        lPanel.Enabled:=true;
        cxGrid1.SetFocus;
      end;

      if (st = dsEdit) then
      begin
        cxGrid1DBTableView1.DataController.DataSet.Post;
        dPanel.Enabled:=false;
        lPanel.Enabled:=true;
        cxGrid1.SetFocus;
      end;
    end;
  end;
end;

procedure TfrmProizvodstvenResurs.cBoxExtPartnerPropertiesEditValueChanged(Sender: TObject);
begin
  if (dPanel.Enabled = true) then
  begin
    //dmMat.tblRETIP_PARTNER.Value := dmmat.tblPartnerTIP_PARTNER.Value;
  //  dmMat.tblREPARTNER.Value := dmmat.tblPartnerID.Value;
  end;
end;

procedure TfrmProizvodstvenResurs.cbVremenskaMEDblClick(Sender: TObject);
begin
//      if TcxdbLookupComboBox(Sender)=cbVremenskaME   then
//      begin
//        frmmerka := TfrmMerka.Create(self, false);
//        frmMerka.ShowModal;
//        if frmMerka.ModalResult = mrOk then
//           begin
//             dm.tblProizvodstvenResursMERNA_EDINICA.Value := frmMerka.GetSifra(0);
//           end;
//        frmMerka.Free;
//      end
end;

procedure TfrmProizvodstvenResurs.aOtkaziExecute(Sender: TObject);
begin
    if (cxGrid1DBTableView1.DataController.datasource.State = dsBrowse) then
    begin
        ModalResult := mrCancel;
        Close();
    end
    else
    begin
        cxGrid1DBTableView1.DataController.DataSet.Cancel;
        RestoreControls(dPanel);
        dPanel.Enabled := false;
        lPanel.Enabled := true;
        cxgrid1.SetFocus;
    end;
end;

procedure TfrmProizvodstvenResurs.aPageSetupExecute(Sender: TObject);
begin
  dxComponentPrinter1Link1.PageSetup;
end;

procedure TfrmProizvodstvenResurs.aPecatiExecute(Sender: TObject);
begin
   dxComponentPrinter1Link1.ReportTitle.Text := Caption;

  dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Clear;
  dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Add
    (dmkon.firma_naziv);
  dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Add
    (DateTimeToStr(now));

  dxComponentPrinter1Link1.PrinterPage.PageHeader.LeftTitle.Clear;
 // dxComponentPrinter1Link1.PrinterPage.PageHeader.LeftTitle.Add('������� : ' + cbRekolta.Text);

  dxComponentPrinter1.Preview(true, dxComponentPrinter1Link1);
end;

procedure TfrmProizvodstvenResurs.aPodesuvanjePecatenjeExecute(Sender: TObject);
begin
  dxComponentPrinter1Link1.DesignReport();
end;

//-------------------------------------------------------------------------------------------
// ��������� �� ���������� �� ��������� � �������� �� Print �������� �� .ini ��������

procedure TfrmProizvodstvenResurs.zacuvajPrintVoIni(ime:AnsiString);
var
  AStream: TFileStream;
begin
  AStream := TFileStream.Create(dmKon.RabotenDir + ime + 'Print.ini', fmCreate);
  try
    AStream.WriteComponent(dxComponentPrinter1Link1);
  finally
    AStream.Free;
  end;

end;

//-------------------------------------------------------------------------------------------
// ��������� �� ������ �� ��������� � �������� �� Print �������� �������� �� .ini ��������

procedure TfrmProizvodstvenResurs.procitajPrintOdIni(ime:AnsiString);
var
  AStream: TFileStream;
begin
  AStream := TFileStream.Create(dmKon.RabotenDir + ime + 'Print.ini', fmOpenRead);
  try
    dxComponentPrinter1Link1.RestoreDefaults;
    AStream.ReadComponent(dxComponentPrinter1Link1);
  finally
    AStream.Free;
  end;
end;


end.
