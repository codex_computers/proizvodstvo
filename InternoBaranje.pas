unit InternoBaranje;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData, cxEdit,
  DB, cxDBData, cxGridCustomPopupMenu, cxGridPopupMenu,
  ComCtrls, cxContainer, cxTextEdit, cxDBEdit, StdCtrls, cxGridLevel,
  cxClasses, cxControls, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGrid, ExtCtrls, Menus, ActnList,
  cxGridExportLink, cxExport, cxLookAndFeelPainters, cxDataStorage,
  cxButtons, dxStatusBar, dxRibbonStatusBar, dxRibbon, dxBar, dxBarExtItems,
  dxPSGlbl, dxPSUtl, dxPSEngn, dxPrnPg, dxBkgnd, dxWrap, dxPrnDev,
  dxPSCompsProvider, dxPSFillPatterns, dxPSEdgePatterns, dxPSCore, dxPScxCommon,
  cxPC, cxMemo, cxLocalization, cxLookAndFeels,
  dxPSPDFExportCore, dxPSPDFExport, cxDrawTextUtils, dxPSPrVwStd, dxPSPrVwAdv,
  dxPSPrVwRibbon, dxPScxEditorProducers, dxPScxExtEditorProducers,
  dxPScxPageControlProducer, dxPgsDlg, cxCheckBox, cxBarEditItem, dxSkinsCore,
  dxSkinBlack, dxSkinBlue, dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom,
  dxSkinDarkSide, dxSkinFoggy, dxSkinGlassOceans, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven,
  dxSkinSharp, dxSkinSilver, dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008,
  dxSkinsDefaultPainters, dxSkinValentine, dxSkinXmas2008Blue,
  dxSkinsdxRibbonPainter, dxSkinsdxBarPainter, dxSkinscxPCPainter,
  dxBarSkinnedCustForm, cxDropDownEdit, cxCalendar, cxDBLookupComboBox,
  cxMaskEdit, cxLookupEdit, cxDBLookupEdit, cxDBExtLookupComboBox, cxGroupBox,
  cxRadioGroup, cxInplaceContainer, cxVGrid, cxOI, cxSchedulerStorage,
  cxSchedulerCustomControls, cxSchedulerDateNavigator, cxDateNavigator, cxSSheet,
  dmMaticni, dmSystem, cxLabel, dateutils, cxDBVGrid,
  dxRibbonSkins, cxPCdxBarPopupMenu, dxPScxGridLnk, dxPScxGridLayoutViewLnk,
  dxScreenTip, FIBQuery, pFIBQuery, dxSkinBlueprint, dxSkinDevExpressDarkStyle,
  dxSkinDevExpressStyle, dxSkinHighContrast, dxSkinSevenClassic,
  dxSkinSharpPlus, dxSkinTheAsphaltWorld, dxSkinVS2010, dxSkinWhiteprint;

type
//  niza = Array[1..5] of Variant;

  TfrmInternoBaranje = class(TForm)
    cxGridPopupMenu1: TcxGridPopupMenu;
    PopupMenu1: TPopupMenu;
    N1: TMenuItem;
    Excel1: TMenuItem;
    dxRibbon1Tab1: TdxRibbonTab;
    dxRibbon1: TdxRibbon;
    StatusBar1: TdxRibbonStatusBar;
    dxBarManager1: TdxBarManager;
    dxBarManager1Bar1: TdxBar;
    dxBarManager1Bar2: TdxBar;
    dxBarManager1Bar3: TdxBar;
    dxBarLargeButton2: TdxBarLargeButton;
    dxBarLargeButton3: TdxBarLargeButton;
    dxBarLargeButton4: TdxBarLargeButton;
    dxBarLargeButton5: TdxBarLargeButton;
    dxBarLargeButton6: TdxBarLargeButton;
    dxBarLargeButton7: TdxBarLargeButton;
    dxBarLargeButton8: TdxBarLargeButton;
    dxBarLargeButton1: TdxBarLargeButton;
    ActionList1: TActionList;
    aNov: TAction;
    aAzuriraj: TAction;
    aBrisi: TAction;
    aSnimiIzgled: TAction;
    aZacuvajExcel: TAction;
    aPecatiTabela: TAction;
    aHelp: TAction;
    aIzlez: TAction;
    aZapisi: TAction;
    aOtkazi: TAction;
    aRefresh: TAction;
    dxRibbon1Tab2: TdxRibbonTab;
    dxBarManager1Bar4: TdxBar;
    dxBarLargeButton9: TdxBarLargeButton;
    aSnimiPecatenje: TAction;
    dxComponentPrinter1: TdxComponentPrinter;
    dxComponentPrinter1Link1: TdxGridReportLink;
    dxBarLargeButton11: TdxBarLargeButton;
    ribbonFilterReset: TdxBarLargeButton;
    aPodesuvanjePecatenje: TAction;
    dxBarLargeButton12: TdxBarLargeButton;
    aBrisiPodesuvanjePecatenje: TAction;
    dxBarLargeButton13: TdxBarLargeButton;
    dxBarLargeButton14: TdxBarLargeButton;
    aPageSetup: TAction;
    dxBarLargeButton15: TdxBarLargeButton;
    dxBarLargeButton16: TdxBarLargeButton;
    dxBarLargeButton17: TdxBarLargeButton;
    cxBarEditItem1: TcxBarEditItem;
    dxBarSubItem1: TdxBarSubItem;
    aFormConfig: TAction;
    dxBarScreenTipRepository1: TdxBarScreenTipRepository;
    tipSnimiIzgled: TdxBarScreenTip;
    tipZacuvajExcel: TdxBarScreenTip;
    tipDizajnReport: TdxBarScreenTip;
    tipSetiranjeStrana: TdxBarScreenTip;
    tipSnimiKonf: TdxBarScreenTip;
    tipBrisiKonf: TdxBarScreenTip;
    PageControl: TcxPageControl;
    lPanel: TPanel;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    tcInternoBaranje: TcxTabSheet;
    tcStavki: TcxTabSheet;
    Panel1: TPanel;
    Panel2: TPanel;
    dPanel: TPanel;
    Label1: TLabel;
    Label3: TLabel;
    Label8: TLabel;
    Label14: TLabel;
    Sifra: TcxDBTextEdit;
    txtBrojDogovor: TcxDBTextEdit;
    Datum: TcxDBDateEdit;
    cxButton2: TcxButton;
    txtTPTeh: TcxDBTextEdit;
    txtTeh: TcxDBTextEdit;
    cbTeh: TcxLookupComboBox;
    ZapisiButton: TcxButton;
    cxGrid2DBTableView1: TcxGridDBTableView;
    cxGrid2Level1: TcxGridLevel;
    cxGrid2: TcxGrid;
    Label4: TLabel;
    txtBroj: TcxDBTextEdit;
    Label9: TLabel;
    txtMaterijal: TcxDBTextEdit;
    cbMaterijal: TcxDBLookupComboBox;
    Label10: TLabel;
    txtKolicina: TcxDBTextEdit;
    ZapisiSButton: TcxButton;
    cxButton3: TcxButton;
    txtNazivKoop: TcxDBTextEdit;
    cxGridPopupMenu2: TcxGridPopupMenu;
    dxBarManager1Bar6: TdxBar;
    bbZastitaZadrski: TdxBarButton;
    bbVratiZastita: TdxBarButton;
    bbZakluci: TdxBarButton;
    cxGrid1DBTableView1RE: TcxGridDBColumn;
    cxGrid1DBTableView1OD_KADE: TcxGridDBColumn;
    cxGrid1DBTableView1BROJ: TcxGridDBColumn;
    cxGrid1DBTableView1DATUM: TcxGridDBColumn;
    cxGrid1DBTableView1OPIS: TcxGridDBColumn;
    cxGrid1DBTableView1RE2: TcxGridDBColumn;
    cxGrid1DBTableView1KADE: TcxGridDBColumn;
    cxGrid1DBTableView1RBN_RE: TcxGridDBColumn;
    cxGrid1DBTableView1RBN_BROJ: TcxGridDBColumn;
    cxGrid2DBTableView1RE: TcxGridDBColumn;
    cxGrid2DBTableView1OD_KADE: TcxGridDBColumn;
    cxGrid2DBTableView1BROJ: TcxGridDBColumn;
    cxGrid2DBTableView1RBR: TcxGridDBColumn;
    cxGrid2DBTableView1ARTVID: TcxGridDBColumn;
    cxGrid2DBTableView1ARTSIF: TcxGridDBColumn;
    cxGrid2DBTableView1NAZIV: TcxGridDBColumn;
    cxGrid2DBTableView1KOLICINA: TcxGridDBColumn;

    procedure cxDBTextEditAllEnter(Sender: TObject);
    procedure cxDBTextEditAllExit(Sender: TObject);
    procedure EnterKakoTab(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    //procedure PrvPosledenTab(panel:TPanel;var posledna,prva:TWinControl);
    procedure SaveToIniFileExecute(Sender: TObject);
    procedure cxGrid1DBTableView1KeyPress(Sender: TObject; var Key: Char);
    procedure aZapisiExecute(Sender: TObject);
    procedure aOtkaziExecute(Sender: TObject);
    procedure aNovExecute(Sender: TObject);
    procedure aAzurirajExecute(Sender: TObject);
    procedure aBrisiExecute(Sender: TObject);
    procedure aZacuvajExcelExecute(Sender: TObject);
    procedure aRefreshExecute(Sender: TObject);
    procedure aSnimiIzgledExecute(Sender: TObject);
    procedure aIzlezExecute(Sender: TObject);
    procedure aPecatiTabelaExecute(Sender: TObject);
    procedure aSnimiPecatenjeExecute(Sender: TObject);
    procedure zacuvajPrintVoIni(ime:AnsiString);
    procedure procitajPrintOdIni(ime:AnsiString);
    procedure aPodesuvanjePecatenjeExecute(Sender: TObject);
    procedure aBrisiPodesuvanjePecatenjeExecute(Sender: TObject);
    procedure aPageSetupExecute(Sender: TObject);
    procedure aFormConfigExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure PartnerPropertiesChange(Sender: TObject);
    procedure SetirajLukap(sender:TObject; tabela:TDataSet; tip:TcxDBTextEdit; sifra:TcxDBTextEdit; lukap:TcxLookupComboBox);
    procedure cxGrid1DBTableView1FocusedRecordChanged(
      Sender: TcxCustomGridTableView; APrevFocusedRecord,
      AFocusedRecord: TcxCustomGridRecord;
      ANewItemRecordFocusingChanged: Boolean);
    procedure aZapisiStavkaExecute(Sender: TObject);
    procedure cxGrid2DBTableView1FocusedRecordChanged(
      Sender: TcxCustomGridTableView; APrevFocusedRecord,
      AFocusedRecord: TcxCustomGridRecord;
      ANewItemRecordFocusingChanged: Boolean);
    procedure dxRibbon1TabChanging(Sender: TdxCustomRibbon;
      ANewTab: TdxRibbonTab; var Allow: Boolean);
    procedure aZakluciExecute(Sender: TObject);
    procedure aStatusExecute(Sender: TObject);
    procedure aVratiStatusExecute(Sender: TObject);
    procedure FormDblClick(Sender: TObject);
  private
    { Private declarations }

    _sifra_kluc :array[1..10] of string;  // �������� ���������� �� ������ �� ������� �� ������������� ���

  protected
    list : TList;
    inserting : boolean;
    prva, posledna :TWinControl;
    procedure prefrli;

  public
    { Public declarations }
    constructor Create(Owner : TComponent; insert : boolean = true);reintroduce; overload;

    procedure SetSifra(br: integer; s : string);
    function GetSifra(br: integer) : string;

    //������� �� ���������� ������������ _sifra_kluc
    property sifra_kluc[br :integer]: string read GetSifra write SetSifra;
  end;

var
  frmInternoBaranje: TfrmInternoBaranje;

implementation

uses DaNe, dmKonekcija, Utils, dmResources, FormConfig, dmUnit;

{$R *.dfm}
//------------------------------------------------------------------------------

constructor TfrmInternoBaranje.Create(Owner : TComponent; insert : boolean = true);
begin
inherited Create(Owner);
//	�������� ���� ������� � �������� �� ����������� ���������
//	insert = true -> ����� ���� ����� ���������� �� ������� �� Insert Mode
//  insert = false -> ����� ���� ����� ������� �� ����� �� ������. �� ������� ����� ����� �� �� �������� F5
  inserting := insert;
end;

//	����� �� �����������. �� ��������� ������� �� ������, �� �������� ������� �� ���� ��������,
//	�� ������� ������� �� ������ ������ � �� ������� �� Insert Mode
procedure TfrmInternoBaranje.aNovExecute(Sender: TObject);
begin
if PageControl.ActivePage=tcInternoBaranje then
begin
  if(cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) then
  begin

    dPanel.Enabled:=True;
    lPanel.Enabled:=False;
    sifra.SetFocus;
    cxGrid1DBTableView1.DataController.DataSet.Insert;
    dm.tblInternoBaranjeBROJ.Value:=dmMat.zemiMax(dm.pMAXzadrska,'rekolta','id_tip_zadrska',Null,StrToInt(cbRekolta.text),'6',Null,'MAKS');
  end
  else ShowMessage('������ �������� �� ��� �������� �� �� ��������� �������/r/n � ����� ������� �� ������� ��� �����!');
end
else
if PageControl.ActivePage=tcStavki then
begin
  if(cxGrid2DBTableView1.DataController.DataSource.State = dsBrowse) then
  begin
   // PageControl.ActivePage:=tcZadrska;
    Panel1.Enabled:=True;
    Panel2.Enabled:=False;
    txtBroj.SetFocus;
    cxGrid2DBTableView1.DataController.DataSet.Insert;
    dm.tblInternoBaranjeStavkaBROJ.Value:=dmMat.zemiMax(dm.pMaxZadrskaStavka,'id_zadrska',Null,Null,dm.tblZadrskaNaturaID.Value,Null,Null,'MAKS');
   // dm.tblZadrskaNaturaID_TIP_ZADRSKA.Value:=6;
   // dm.tblZadrskaNaturaREKOLTA.AsString:=cbRekolta.Text;
  end
  else ShowMessage('������ �������� �� ��� �������� �� �� ��������� �������/r/n � ����� ������� �� ������� ��� �����!');
end;
end;

//	����� �� ���������. �� ��������� ������� �� ������, �� �������� ������� �� ���� ��������,
//	�� ������� ������� �� ������ ������ � �� ������� �� Edit Mode
procedure TfrmInternoBaranje.aAzurirajExecute(Sender: TObject);
begin
if PageControl.ActivePage=tcInternoBaranje then
begin
  if(cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) then
  begin
   if (dm.tblZadrskaNaturaZAKLUCI.Value=1) then
    begin
      ShowMessage('�� � ��������� ������� �� �������!');
      Abort;
    end
    else
    if (dm.tblZadrskaNaturaSTATUS.Value=1) then
    begin
      ShowMessage('�� � ��������� ������� �� �������!');
      Abort;
    end
    else
    begin
        dPanel.Enabled:=True;
        lPanel.Enabled:=False;
        prva.SetFocus;
        cxGrid1DBTableView1.DataController.DataSet.Edit;
    end;
  end
  else ShowMessage('������ �������� �� ��� �������� �� �� ��������� �������/r/n � ����� ������� �� ������� ��� �����!');
end
else
  if(cxGrid2DBTableView1.DataController.DataSource.State = dsBrowse) then
      if (dm.tblZadrskaNaturaZAKLUCI.Value=1) then
        begin
          ShowMessage('�� � ��������� ������� �� �������!');
          Abort;
        end
        else
        if (dm.tblZadrskaNaturaSTATUS.Value=1) then
            begin
              ShowMessage('�� � ��������� ������� �� �������!');
              Abort;
            end
            else
            begin
                Panel1.Enabled:=True;
                Panel2.Enabled:=False;
                txtBroj.SetFocus;
                cxGrid2DBTableView1.DataController.DataSet.Edit;
            end
      else ShowMessage('������ �������� �� ��� �������� �� �� ��������� �������/r/n � ����� ������� �� ������� ��� �����!');
end;

//	����� �� ������ �� ������������� �����
procedure TfrmInternoBaranje.aBrisiExecute(Sender: TObject);
begin
if PageControl.ActivePage=tcZadrska then
  begin
    if (dm.tblZadrskaNaturaZAKLUCI.Value=1) then
    begin
      ShowMessage('�� � ��������� ������ �� �������!');
      Abort;
    end
    else
    if (dm.tblZadrskaNaturaSTATUS.Value=1) then
    begin
      ShowMessage('�� � ��������� ������ �� �������!');
      Abort;
    end
    else
        if ((cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) and
           (cxGrid1DBTableView1.DataController.RecordCount <> 0)) then
          cxGrid1DBTableView1.DataController.DataSet.Delete();
        end
  else
    if (dm.tblZadrskaNaturaZAKLUCI.Value=1) then
    begin
      ShowMessage('�� � ��������� ������ �� �������!');
      Abort;
    end
    else
    if (dm.tblZadrskaNaturaSTATUS.Value=1) then
    begin
      ShowMessage('�� � ��������� ������ �� �������!');
      Abort;
    end
    else
    begin
        if ((cxGrid2DBTableView1.DataController.DataSource.State = dsBrowse) and
         (cxGrid2DBTableView1.DataController.RecordCount <> 0)) then
        cxGrid2DBTableView1.DataController.DataSet.Delete();
      end;
end;

//	����� �� ���������� �� ����������
procedure TfrmInternoBaranje.aRefreshExecute(Sender: TObject);
begin
  cxGrid1DBTableView1.DataController.Filter.Root.Clear; //���������� �� ��������
  cxGrid1DBTableView1.DataController.DataSet.Refresh;
end;

//	����� �� ����� �� ������� �� ������� �� ������� �����
procedure TfrmInternoBaranje.aIzlezExecute(Sender: TObject);
begin
  Close;
end;

//	����� �� ������� �� �������� �� ������ �� ���� (Utils.pas)
procedure TfrmInternoBaranje.aSnimiIzgledExecute(Sender: TObject);
begin
  zacuvajGridVoBaza(Name,cxGrid1DBTableView1);
  ZacuvajFormaIzgled(self);
end;

//	����� �� ������� �� ������ �� Excel ������ (Utils.pas)
procedure TfrmInternoBaranje.aZacuvajExcelExecute(Sender: TObject);
begin
  zacuvajVoExcel(cxGrid1, Caption);
end;

procedure TfrmInternoBaranje.aZakluciExecute(Sender: TObject);
begin
//     dm.ZakluciZadrski(6,dm.tblZadrskaNatura);
//    if not (dm.tblZadrskaNatura.State in [dsInsert, dsEdit]) then
//    begin
//        frmDaNe := TfrmDaNe.Create(self, '�������', '���� ������� �� ����������� ���������? '+#10#13+' ����� ������� �������� � �� � ���������!', 1);
//        if (frmDaNe.ShowModal = mrYes) then
//        begin
//             //�� �� �������� ���� �������
//              qZakluci.ExecQuery;
//              dm.tblZadrskaNatura.FullRefresh;
//        end
//        else
//          Abort;
//    end;

end;

//  ��������� �� ������ �� ������� �� ���������� �� ����� �� Enter
procedure TfrmInternoBaranje.EnterKakoTab(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
    case Key of
        VK_DOWN:
        begin
          PostMessage(Handle,WM_NextDlgCtl,0,0);
          key:=0;
        end;
        VK_UP:
        begin
          PostMessage(Handle,WM_NextDlgCtl,1,0);
          key:=0;
        end;
        VK_RETURN:
        begin
          PostMessage(Handle,WM_NextDlgCtl,0,0);
        end;
    end;
end;

//	������� �� ��� �� ������ ��� �������� �� �����
procedure TfrmInternoBaranje.cxDBTextEditAllEnter(Sender: TObject);
begin
    TEdit(Sender).Color:=clSkyBlue;
end;

procedure TfrmInternoBaranje.cxDBTextEditAllExit(Sender: TObject);
begin
    if (dm.tblzadrskaNatura.State) in [dsInsert, dsEdit] then
    begin
    if ((Sender as TWinControl)= cbTeh) then
        begin
            if (cbTeh.Text <>'') then
            begin
              dm.tblzadrskaNaturaR_TIP_PARTNER.Value := dm.tblIzberiReonskiTIP_PARTNER.Value; //cbPartner.EditValue[0];
              dm.tblzadrskaNaturaR_PARTNER.Value := dm.tblIzberiReonskiID.Value; //cbPartner.EditValue[1];
            end
            else
            begin
              dm.tblzadrskaNaturaR_TIP_PARTNER.Clear;
              dm.tblzadrskaNaturaR_PARTNER.Clear;
            end;
         end;
         if ((Sender as TWinControl)= txtTeh) or ((Sender as TWinControl)= txtTPTeh) then
         begin
             SetirajLukap(sender,dm.tblIzberiReonski,txtTPTeh,txtTeh, cbTeh);
         end;

    if (Sender as TWinControl) = txtBrojDogovor then
       begin
         dm.tblizberiDogKoop.Close;
         dm.tblizberiDogKoop.ParamByName('rekolta').AsString:=cbRekolta.Text;
         dm.tblizberiDogKoop.Open;
         if txtBrojDogovor.Text='' then
             begin
              if txtBrojDogovor.Text = '' then
              begin
                    frmIzberiDogKoop:= TfrmIzberiDogKoop.Create(Application);
                    frmIzberiDogKoop.ShowModal;
                    if frmIzberiDogKoop.Tag=1 then
                        begin
                            dm.tblZadrskaNaturaTIP_PARTNER.Value:=dm.tblizberiDogKoopTP_KOOP.Value;
                            dm.tblZadrskaNaturaPARTNER.Value:=dm.tblizberiDogKoopP_KOOP.Value;
                            dm.tblZadrskaNaturaID_DOGOVOR.Value:=dm.tblizberiDogKoopID.Value;
                            dm.tblZadrskaNaturaBROJ_DOG.Value:=dm.tblizberiDogKoopBROJ.Value;
                            dm.tblZadrskaNaturaNAZIV_KOOPERANT.Value:=dm.tblizberiDogKoopNAZIV_KOOPERANT.Value;
                        end
                        else
                           begin
                               txtBrojDogovor.SetFocus;
                               Abort;
                           end;
                    frmIzberiDogKoop.Free;
              end;
           end
         else
         begin
             if dm.tblizberiDogKoop.Locate('BROJ',VarArrayOf([txtBrojDogovor.Text]),[]) then
             begin
                dm.tblZadrskaNaturaTIP_PARTNER.Value:=dm.tblizberiDogKoopTP_KOOP.Value;
                dm.tblZadrskaNaturaPARTNER.Value:=dm.tblizberiDogKoopP_KOOP.Value;
                dm.tblZadrskaNaturaID_DOGOVOR.Value:=dm.tblizberiDogKoopID.Value;
                dm.tblZadrskaNaturaNAZIV_KOOPERANT.Value:=dm.tblizberiDogKoopNAZIV_KOOPERANT.Value;
             end;
         end;
       end;
    end;
    TEdit(Sender).Color:=clWhite;
end;

procedure TfrmInternoBaranje.cxGrid1DBTableView1FocusedRecordChanged(
  Sender: TcxCustomGridTableView; APrevFocusedRecord,
  AFocusedRecord: TcxCustomGridRecord; ANewItemRecordFocusingChanged: Boolean);
begin
    SetirajLukap(sender,dm.tblIzberiReonski,txtTPTeh,txtTeh, cbTeh);
    tcZadrska.Caption:='������� ���  '+dm.tblZadrskaNaturaBROJ.AsString+'  �� ���������  '+ dm.tblZadrskaNaturaNAZIV_KOOPERANT.Value;
//    if dm.tblZadrskaNaturaSTATUS.Value=1 then
//    begin
//      aStatus.Enabled:=False;
//      aVratiStatus.Enabled:=True;
//    end
//    else
//    begin
//      aStatus.Enabled:=True;
//      aVratiStatus.Enabled:=False;
//    end;
    if dm.tblZadrskaNaturaZAKLUCI.Value=1 then
    begin
      aStatus.Enabled:=False;
      aVratiStatus.Enabled:=False;
      aZakluci.Enabled:=false;
    end
    else
    if (dm.tblZadrskaNaturaZAKLUCI.Value=0) and (dm.tblZadrskaNaturaSTATUS.Value=1)  then
    begin
      aStatus.Enabled:=False;
      aVratiStatus.Enabled:=True;
      aZakluci.Enabled:=True;
    end
    else
    begin
      aStatus.Enabled:=True;
      aVratiStatus.Enabled:=True;
      aZakluci.Enabled:=True;
    end;
end;

//  ��������� �� ������ �� ��������� �� ���������� ���� �� ��������
//  ��������� ��: br - ����� ��� �� ������ (�� 0 �� 9) �� ��������� �� ����� �� ����� �� ���������� ����
//  s - ��������� �� ������ �� ������ (���������� �� string �� ����� �� integer �� IntToStr ���������)
procedure TfrmInternoBaranje.SetSifra(br: integer; s : string);
begin
  _sifra_kluc[br] := s;
end;

//  ������� �� ������ �� ��������� �� ���������� ���� �� ��������
//  ��������� ��: br - ����� ��� �� ������ (�� 0 �� 9) �� ��������� �� ����� �� ����� �� ���������� ����
function TfrmInternoBaranje.GetSifra(br: integer) : string;
begin
  Result := _sifra_kluc[br];
end;

procedure TfrmInternoBaranje.PartnerPropertiesChange(Sender: TObject);
begin

end;

//  �� ������� �� ������� ��� ����������� �� Master-�� �� �������� �� ��������� �� ������ ����� SetSifra
procedure TfrmInternoBaranje.prefrli;
begin
end;

procedure TfrmInternoBaranje.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    //	��������� ��� ��������� �� �������
    if (cxGrid1DBTableView1.DataController.DataSource.State = dsEdit) or (cxGrid1DBTableView1.DataController.DataSource.State = dsInsert) then
    begin
        frmDaNe := TfrmDaNe.Create(self, '���������� ��������', '���������� �� �� ��������. ���� ������ �� �� �������?', 1);
        if (frmDaNe.ShowModal <> mrYes) then
        begin
            cxGrid1DBTableView1.DataController.DataSet.Cancel;
            Action := caFree;
        end
        else
          if (Validacija(dPanel) = false) then
          begin
            cxGrid1DBTableView1.DataController.DataSet.Post;
            Action := caFree;
          end
          else Action := caNone;
    end;
end;
procedure TfrmInternoBaranje.FormCreate(Sender: TObject);
begin
  cbRekolta.Text:=dm.tblRekoltaGODINA.AsString;
  dm.tblIzberiDogovor.ParamByName('rekolta').AsString:=cbRekolta.Text;
  dm.tblIzberiDogovor.Open;
  if cbRekolta.Text='' then cbRekolta.Text:=inttostr(yearof(now));
  dm.tblZadrskaNatura.Close;
  dm.tblzadrskaNatura.ParamByName('rekolta').AsString := cbRekolta.Text;
  dm.tblzadrskaNatura.open;
  dm.tblZadrskaStavka.Open;
  ProcitajFormaIzgled(self);
  if (dmRes.use_skin = true) then dxRibbon1.ColorSchemeName := dmRes.skin_name;
end;

procedure TfrmInternoBaranje.FormDblClick(Sender: TObject);
begin

end;

//------------------------------------------------------------------------------

procedure TfrmInternoBaranje.FormShow(Sender: TObject);
begin
  //  ������� �� ������������ ������������/���������� �� ���������� �� ���� �����
    SpremiForma(self);
  //	�� ��������� �� ������� ������ ��� ���������� � ���� � ��� �������� �� �������
    PrvPosledenTab(dPanel,posledna,prva);
    dxBarManager1Bar1.Caption := Caption;
    dxComponentPrinter1Link1.ReportTitle.Text := Caption;

  //	������� �� ������������ �� ������ �� ������
    procitajGridOdBaza(Name,cxGrid1DBTableView1,false,false);
  //	������� �� ������������ �� ��������� �� ������
    procitajPrintOdBaza(Name,cxGrid1DBTableView1.Name, dxComponentPrinter1Link1);
    PageControl.ActivePage:=tcZadrska;
    ActiveControl:=cxGrid1;
    SetirajLukap(sender,dm.tblIzberiReonski,txtTPTeh,txtTeh, cbTeh);
    if dm.tblZadrskaNaturaZAKLUCI.Value=1 then
    begin
      aStatus.Enabled:=False;
      aVratiStatus.Enabled:=False;
      aZakluci.Enabled:=false;
    end
    else
    if (dm.tblZadrskaNaturaZAKLUCI.Value=0) and (dm.tblZadrskaNaturaSTATUS.Value=1)  then
    begin
      aStatus.Enabled:=False;
      aVratiStatus.Enabled:=True;
      aZakluci.Enabled:=True;
    end
    else
    begin
      aStatus.Enabled:=True;
      aVratiStatus.Enabled:=True;
      aZakluci.Enabled:=True;
    end;
end;
//------------------------------------------------------------------------------

procedure TfrmInternoBaranje.SaveToIniFileExecute(Sender: TObject);
begin
    cxGrid1DBTableView1.StoreToIniFile(Name,True,[]);
end;
//------------------------------------------------------------------------------

procedure TfrmInternoBaranje.cxGrid1DBTableView1KeyPress(Sender: TObject;
  var Key: Char);
begin
  if(Ord(Key) <> VK_RETURN) then cxSortiraj(cxGrid1DBTableView1);
end;

procedure TfrmInternoBaranje.cxGrid2DBTableView1FocusedRecordChanged(
  Sender: TcxCustomGridTableView; APrevFocusedRecord,
  AFocusedRecord: TcxCustomGridRecord; ANewItemRecordFocusingChanged: Boolean);
begin
//   tcZadrska.Caption:='������� ���  '+dm.tblZadrskaNaturaBROJ.AsString+'  �� ���������  '+ dm.tblZadrskaNaturaNAZIV_KOOPERANT.Value;
end;

procedure TfrmInternoBaranje.dxRibbon1TabChanging(Sender: TdxCustomRibbon;
  ANewTab: TdxRibbonTab; var Allow: Boolean);
begin
 if Self.Active then

   if ANewTab = dxRibbon1Tab2 then
   begin
     lRekolta.Visible:=False;
     cbRekolta.Visible:=False;
   end
   else
   begin
     lRekolta.Visible:=True;
     cbRekolta.Visible:=True;
   end;
end;

//  ����� �� �����
procedure TfrmInternoBaranje.aZapisiExecute(Sender: TObject);
var
  st: TDataSetState;
begin
 if PageControl.ActivePage=tcZadrska then
 begin
  ZapisiButton.SetFocus;

  st := cxGrid1DBTableView1.DataController.DataSet.State;
  if st in [dsEdit,dsInsert] then
  begin
    if (Validacija(dPanel) = false) then
    begin
      if ((st = dsInsert) and inserting) then
      begin
        //Trgnato po baranje na Natka, sami da si go bnesuvat brojot!!!
        //Se vesuva na na nov zapis, so moznost za promena!
       // dm.tblZadrskaNaturaBROJ.Value:=dmMat.zemiMax(dm.pMAXzadrska,'rekolta','id_tip_zadrska',Null,StrToInt(cbRekolta.text),'6',Null,'MAKS');
        cxGrid1DBTableView1.DataController.DataSet.Post;
        dPanel.Enabled:=False;
        lPanel.Enabled:=true;
        PageControl.ActivePage:=tcStavki;
        Panel1.Enabled:=True;
        Panel2.Enabled:=False;
        txtBroj.SetFocus;
        cxGrid2DBTableView1.DataController.DataSet.Insert;
        dm.tblZadrskaStavkaREDEN_BROJ.Value:=dmMat.zemiMax(dm.pMaxZadrskaStavka,'id_zadrska',null,Null,dm.tblZadrskaNaturaID.Value,Null,Null,'MAKS');
      end;

      if ((st = dsInsert) and (not inserting)) then
      begin
        cxGrid1DBTableView1.DataController.DataSet.Post;
        dPanel.Enabled:=false;
        lPanel.Enabled:=true;
        cxGrid1.SetFocus;
      end;

      if (st = dsEdit) then
      begin
        cxGrid1DBTableView1.DataController.DataSet.Post;
        dPanel.Enabled:=false;
        lPanel.Enabled:=true;
        cxGrid1.SetFocus;
      end;
    end;
  end;

 end
 else
if PageControl.ActivePage=tcStavki then
 begin
  ZapisiSButton.SetFocus;

  st := cxGrid2DBTableView1.DataController.DataSet.State;
  if st in [dsEdit,dsInsert] then
  begin
    if (Validacija(Panel1) = false) then
    begin
      //dm.tblZadrskaNaturaTIP_PARTNER.Value:=dm.tblIzberiDogovorTP_KOOP.Value;
     // dm.tblZadrskaNaturaPARTNER.Value:=dm.tblIzberiDogovorP_KOOP.Value;
      dm.tblZadrskaStavkaCENA.Value:=dm.tblUslugaCENA.Value;
      if ((st = dsInsert) and inserting) then
      begin
        cxGrid2DBTableView1.DataController.DataSet.Post;
        aNov.Execute;
      end;

      if ((st = dsInsert) and (not inserting)) then
      begin
        cxGrid2DBTableView1.DataController.DataSet.Post;
        Panel1.Enabled:=false;
        Panel2.Enabled:=true;
        cxGrid2.SetFocus;
      end;

      if (st = dsEdit) then
      begin
        cxGrid2DBTableView1.DataController.DataSet.Post;
        Panel1.Enabled:=false;
        Panel2.Enabled:=true;
        cxGrid2.SetFocus;
      end;
    end;
  end;
 end
end;

procedure TfrmInternoBaranje.aZapisiStavkaExecute(Sender: TObject);
begin

end;

//	����� �� ���������� �� �������
procedure TfrmInternoBaranje.aOtkaziExecute(Sender: TObject);
begin
if PageControl.ActivePage=tcZadrska  then
 begin
  if (cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) then
  begin
      ModalResult := mrCancel;
      Close();
  end
  else
  begin
      cxGrid1DBTableView1.DataController.DataSet.Cancel;
      RestoreControls(dPanel);
      dPanel.Enabled := false;
      lPanel.Enabled := true;
      cxGrid1.SetFocus;
  end;
 end
 else
if PageControl.ActivePage=tcStavki  then
   if (cxGrid2DBTableView1.DataController.DataSource.State = dsBrowse) then
  begin
      ModalResult := mrCancel;
      Close();
  end
  else
  begin
      cxGrid2DBTableView1.DataController.DataSet.Cancel;
      RestoreControls(dPanel);
      Panel1.Enabled := false;
      Panel2.Enabled := true;
      cxGrid2.SetFocus;
  end;

end;

//----------------------------------------------------------------------------------
// ��������� �� ��������, ���������� �� ��������� � �������� �� Print ��������
//----------------------------------------------------------------------------------

//	����� �� �������� �� �������� �� �������
procedure TfrmInternoBaranje.aPageSetupExecute(Sender: TObject);
begin
  dxComponentPrinter1Link1.PageSetup;
end;

//	����� �� ������� �� ������
procedure TfrmInternoBaranje.aPecatiTabelaExecute(Sender: TObject);
begin
  dxComponentPrinter1Link1.ReportTitle.Text := Caption;

  dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Clear;
  dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Add(dmkon.firma_naziv);
  dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Add(DateTimeToStr(Now));

  dxComponentPrinter1Link1.PrinterPage.PageHeader.LeftTitle.Clear;
  dxComponentPrinter1Link1.PrinterPage.PageHeader.LeftTitle.Add('������� : ' + cbRekolta.Text);

  dxComponentPrinter1.Preview(true, dxComponentPrinter1Link1);
end;

//	����� �� ���������� �� ��������� �� �������
procedure TfrmInternoBaranje.aPodesuvanjePecatenjeExecute(Sender: TObject);
begin
  dxComponentPrinter1Link1.DesignReport();
end;

procedure TfrmInternoBaranje.aSnimiPecatenjeExecute(Sender: TObject);
begin
  zacuvajPrintVoBaza(Name,cxGrid1DBTableView1.Name,dxComponentPrinter1Link1);
end;

procedure TfrmInternoBaranje.aStatusExecute(Sender: TObject);
begin
    dm.ZastitiZadrska(6,dm.tblZadrskaNatura);
//    if not (dm.tblZadrskaNatura.State in [dsInsert, dsEdit]) then
//    begin
//     frmDaNe := TfrmDaNe.Create(self, '�������', '���� ������� �� ����������� ���������? '+#10#13+' ����� ������� �������� � �� � ���������!', 1);
//     if (frmDaNe.ShowModal = mrYes) then
//        begin
//            //�� �� �������� ���� �������
//             qUpdateStatus.ExecQuery;
//             dm.tblZadrskaNatura.FullRefresh;
//        end
//        else
//        begin
//          Abort;
//        end;
//    end;
end;

procedure TfrmInternoBaranje.aVratiStatusExecute(Sender: TObject);
begin
      dm.VratiZastita(6,dm.tblZadrskaNatura);
//    if not (dm.tblZadrskaNatura.State in [dsInsert, dsEdit]) then
//    begin
//     frmDaNe := TfrmDaNe.Create(self, '�������', '���� ��� �������? '+#10#13+' ����� ������� �������� �� ���� ���������!', 1);
//     if (frmDaNe.ShowModal = mrYes) then
//        begin
//            //�� �� �������� ���� �������
//             qVratiStatus.ExecQuery;
//             dm.tblZadrskaNatura.FullRefresh;
//        end
//        else
//        begin
//          Abort;
//        end;
//    end;
end;

//	����� �� ������ �� ����������� �������� �� ������� (Utils.pas)
procedure TfrmInternoBaranje.aBrisiPodesuvanjePecatenjeExecute(Sender: TObject);
begin
  brisiPrintOdBaza(Name,cxGrid1DBTableView1.Name, dxComponentPrinter1Link1);
end;

//  ����� �� ����� �� ����� �� ������������� �� �������� � ��������������� ��
//  �������(������ ���������� ��������)
procedure TfrmInternoBaranje.aFormConfigExecute(Sender: TObject);
begin
  frmFormConfig := TfrmFormConfig.Create(Application);
  frmFormConfig.formPtr := Addr(Self);
  frmFormConfig.ShowModal;
  frmFormConfig.Free;
end;

//----------------------------------------------------------------------------------
// ���� -> ��������� �� ��������, ���������� �� ��������� � �������� �� Print ��������
//----------------------------------------------------------------------------------

// ��������� �� ���������� �� ��������� � �������� �� Print �������� �� .ini ��������
procedure TfrmInternoBaranje.zacuvajPrintVoIni(ime:AnsiString);
var
  AStream: TFileStream;
begin
  AStream := TFileStream.Create(dmKon.RabotenDir + ime + 'Print.ini', fmCreate);
  try
    AStream.WriteComponent(dxComponentPrinter1Link1);
  finally
    AStream.Free;
  end;

end;

// ��������� �� ������ �� ��������� � �������� �� Print �������� �������� �� .ini ��������
procedure TfrmInternoBaranje.procitajPrintOdIni(ime:AnsiString);
var
  AStream: TFileStream;
begin
  AStream := TFileStream.Create(dmKon.RabotenDir + ime + 'Print.ini', fmOpenRead);
  try
    dxComponentPrinter1Link1.RestoreDefaults;
    AStream.ReadComponent(dxComponentPrinter1Link1);
  finally
    AStream.Free;
  end;
end;

procedure TfrmInternoBaranje.SetirajLukap(sender:TObject; tabela:TDataSet; tip:TcxDBTextEdit; sifra:TcxDBTextEdit; lukap:TcxLookupComboBox);
begin
  //if tabela.State in [dsEdit, dsInsert] then
  //begin
      //   if (((Sender as TWinControl)= tip) or ((Sender as TWinControl)= sifra)) then
        // begin
         if (tip.Text <>'') and (sifra.Text<>'')  then
         begin
            if(tabela.Locate('TIP_PARTNER;ID',VarArrayOf([tip.text,sifra.text]),[])) then
               lukap.Text:=tabela.FieldByName('NAZIV').Value;
         end
         else
         begin
            lukap.Clear;
         end;
  //end;

end;

end.
