inherited frmTipPR: TfrmTipPR
  Caption = #1058#1080#1087' '#1085#1072' '#1087#1086#1088#1080#1079#1074#1086#1076#1089#1090#1074#1077#1085' '#1088#1077#1089#1091#1088#1089
  ClientWidth = 667
  ExplicitWidth = 683
  ExplicitHeight = 591
  PixelsPerInch = 96
  TextHeight = 13
  inherited lPanel: TPanel
    Width = 667
    ExplicitWidth = 667
    inherited cxGrid1: TcxGrid
      Width = 663
      ExplicitWidth = 663
      inherited cxGrid1DBTableView1: TcxGridDBTableView
        DataController.DataSource = dm.dsTipPR
        object cxGrid1DBTableView1ID: TcxGridDBColumn
          DataBinding.FieldName = 'ID'
        end
        object cxGrid1DBTableView1NAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'NAZIV'
          Width = 424
        end
        object cxGrid1DBTableView1AKTIVEN: TcxGridDBColumn
          Caption = #1040#1082#1090#1080#1074#1077#1085
          DataBinding.FieldName = 'AKTIVEN'
        end
      end
    end
  end
  inherited dPanel: TPanel
    Width = 667
    ExplicitWidth = 667
    inherited Label1: TLabel
      Left = 21
      ExplicitLeft = 21
    end
    object Label2: TLabel [1]
      Left = 21
      Top = 46
      Width = 50
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1053#1072#1079#1080#1074' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    inherited Sifra: TcxDBTextEdit
      Left = 77
      DataBinding.DataField = 'ID'
      DataBinding.DataSource = dm.dsTipPR
      ExplicitLeft = 77
    end
    inherited OtkaziButton: TcxButton
      Left = 576
      TabOrder = 4
      ExplicitLeft = 576
    end
    inherited ZapisiButton: TcxButton
      Left = 495
      TabOrder = 3
      ExplicitLeft = 495
    end
    object txtNaziv: TcxDBTextEdit
      Tag = 1
      Left = 77
      Top = 43
      BeepOnEnter = False
      DataBinding.DataField = 'NAZIV'
      DataBinding.DataSource = dm.dsTipPR
      ParentFont = False
      Properties.BeepOnError = True
      Properties.CharCase = ecUpperCase
      Style.Shadow = False
      TabOrder = 1
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 452
    end
    object cxDBCheckBox1: TcxDBCheckBox
      Left = 11
      Top = 70
      Caption = #1040#1082#1090#1080#1074#1077#1085' :'
      DataBinding.DataField = 'AKTIVEN'
      DataBinding.DataSource = dm.dsTipPR
      ParentFont = False
      Properties.Alignment = taRightJustify
      Properties.ValueChecked = 1
      Properties.ValueUnchecked = 0
      Style.Font.Charset = RUSSIAN_CHARSET
      Style.Font.Color = clRed
      Style.Font.Height = -11
      Style.Font.Name = 'Tahoma'
      Style.Font.Style = [fsBold]
      Style.IsFontAssigned = True
      TabOrder = 2
      Transparent = True
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 84
    end
  end
  inherited dxRibbon1: TdxRibbon
    Width = 667
    ExplicitWidth = 667
    inherited dxRibbon1Tab1: TdxRibbonTab
      Index = 0
    end
    inherited dxRibbon1Tab2: TdxRibbonTab
      Index = 1
    end
  end
  inherited StatusBar1: TdxRibbonStatusBar
    Width = 667
    ExplicitTop = 32000
    ExplicitWidth = 667
  end
  inherited dxBarManager1: TdxBarManager
    DockControlHeights = (
      0
      0
      0
      0)
    inherited dxBarManager1Bar1: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 69
      FloatClientHeight = 167
    end
    inherited dxBarManager1Bar2: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 149
      FloatClientHeight = 130
    end
    inherited dxBarManager1Bar3: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 59
      FloatClientHeight = 108
    end
    inherited dxBarManager1Bar4: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 141
      FloatClientHeight = 221
    end
    inherited dxBarManager1BarTabela: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 174
      FloatClientHeight = 113
    end
  end
  inherited dxComponentPrinter1: TdxComponentPrinter
    inherited dxComponentPrinter1Link1: TdxGridReportLink
      ReportDocument.CreationDate = 41079.567906099540000000
      AssignedFormatValues = [fvDate, fvTime, fvPageNumber]
      Styles.BandHeader = nil
      Styles.Caption = nil
      Styles.CardCaptionRow = nil
      Styles.CardRowCaption = nil
      Styles.Content = nil
      Styles.ContentEven = nil
      Styles.ContentOdd = nil
      Styles.FilterBar = nil
      Styles.Footer = nil
      Styles.Group = nil
      Styles.Header = nil
      Styles.Preview = nil
      Styles.Selection = nil
      BuiltInReportLink = True
    end
  end
end
