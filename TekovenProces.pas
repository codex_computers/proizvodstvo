unit TekovenProces;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData, cxEdit,
  DB, cxDBData, cxGridCustomPopupMenu, cxGridPopupMenu,
  ComCtrls, cxContainer, cxTextEdit, cxDBEdit, StdCtrls, cxGridLevel,
  cxClasses, cxControls, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGrid, ExtCtrls, Menus, ActnList,
  cxGridExportLink, cxExport, cxLookAndFeelPainters, cxDataStorage,
  cxButtons, dxStatusBar, dxRibbonStatusBar, dxRibbon, dxBar, dxBarExtItems,
  dxPSGlbl, dxPSUtl, dxPSEngn, dxPrnPg, dxBkgnd, dxWrap, dxPrnDev,
  dxPSCompsProvider, dxPSFillPatterns, dxPSEdgePatterns, dxPSCore, dxPScxCommon,
  cxPC, cxMemo, cxLocalization, cxLookAndFeels,
  dxPSPDFExportCore, dxPSPDFExport, cxDrawTextUtils, dxPSPrVwStd, dxPSPrVwAdv,
  dxPSPrVwRibbon, dxPScxEditorProducers, dxPScxExtEditorProducers,
  dxPScxPageControlProducer, dxPgsDlg, cxCheckBox, cxBarEditItem, dxSkinsCore,
  dxSkinBlack, dxSkinBlue, dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom,
  dxSkinDarkSide, dxSkinFoggy, dxSkinGlassOceans, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven,
  dxSkinSharp, dxSkinSilver, dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008,
  dxSkinsDefaultPainters, dxSkinValentine, dxSkinXmas2008Blue,
  dxSkinsdxRibbonPainter, dxSkinsdxBarPainter, dxSkinscxPCPainter,
  dxBarSkinnedCustForm, cxDropDownEdit, cxCalendar, cxDBLookupComboBox,
  cxMaskEdit, cxLookupEdit, cxDBLookupEdit, cxDBExtLookupComboBox, cxGroupBox,
  cxRadioGroup, dxRibbonSkins, dxPScxGridLnk, dxPScxGridLayoutViewLnk,
  dxScreenTip, dxSkinBlueprint, dxSkinDevExpressDarkStyle,
  dxSkinDevExpressStyle, dxSkinHighContrast, dxSkinSevenClassic,
  dxSkinSharpPlus, dxSkinTheAsphaltWorld, dxSkinVS2010, dxSkinWhiteprint,
  cxLabel, cxDBLabel, FIBDataSet, pFIBDataSet, dxSkinMetropolis,
  dxSkinMetropolisDark, dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray,
  dxSkinOffice2013White, dxRibbonCustomizationForm, dxCore, cxDateUtils,
  System.Actions, dxSkinOffice2016Colorful, dxSkinOffice2016Dark,
  dxSkinVisualStudio2013Blue, dxSkinVisualStudio2013Dark,
  dxSkinVisualStudio2013Light;

type
//  niza = Array[1..5] of Variant;

  TfrmTekovenProces = class(TForm)
    cxGridPopupMenu1: TcxGridPopupMenu;
    PopupMenu1: TPopupMenu;
    N1: TMenuItem;
    Excel1: TMenuItem;
    dxRibbon1Tab1: TdxRibbonTab;
    dxRibbon1: TdxRibbon;
    StatusBar1: TdxRibbonStatusBar;
    dxBarManager1: TdxBarManager;
    dxBarManager1Bar3: TdxBar;
    dxBarLargeButton2: TdxBarLargeButton;
    dxBarLargeButton3: TdxBarLargeButton;
    dxBarLargeButton4: TdxBarLargeButton;
    dxBarLargeButton5: TdxBarLargeButton;
    dxBarLargeButton6: TdxBarLargeButton;
    dxBarLargeButton7: TdxBarLargeButton;
    dxBarLargeButton8: TdxBarLargeButton;
    dxBarLargeButton1: TdxBarLargeButton;
    ActionList1: TActionList;
    aNov: TAction;
    aAzuriraj: TAction;
    aBrisi: TAction;
    aSnimiIzgled: TAction;
    aZacuvajExcel: TAction;
    aPecatiTabela: TAction;
    aHelp: TAction;
    aIzlez: TAction;
    aZapisi: TAction;
    aOtkazi: TAction;
    aRefresh: TAction;
    dxRibbon1Tab2: TdxRibbonTab;
    dxBarManager1Bar4: TdxBar;
    dxBarLargeButton9: TdxBarLargeButton;
    aSnimiPecatenje: TAction;
    dxComponentPrinter1: TdxComponentPrinter;
    dxComponentPrinter1Link1: TdxGridReportLink;
    dxBarLargeButton11: TdxBarLargeButton;
    ribbonFilterReset: TdxBarLargeButton;
    aPodesuvanjePecatenje: TAction;
    dxBarLargeButton12: TdxBarLargeButton;
    aBrisiPodesuvanjePecatenje: TAction;
    dxBarLargeButton13: TdxBarLargeButton;
    dxBarLargeButton14: TdxBarLargeButton;
    aPageSetup: TAction;
    dxBarLargeButton15: TdxBarLargeButton;
    dxBarLargeButton16: TdxBarLargeButton;
    dxBarLargeButton17: TdxBarLargeButton;
    cxBarEditItem1: TcxBarEditItem;
    dxBarSubItem1: TdxBarSubItem;
    aFormConfig: TAction;
    dxBarScreenTipRepository1: TdxBarScreenTipRepository;
    tipSnimiIzgled: TdxBarScreenTip;
    tipZacuvajExcel: TdxBarScreenTip;
    tipDizajnReport: TdxBarScreenTip;
    tipSetiranjeStrana: TdxBarScreenTip;
    tipSnimiKonf: TdxBarScreenTip;
    tipBrisiKonf: TdxBarScreenTip;
    dxBarManager1BarIzgledGrid: TdxBar;
    dxBarLBtnSnimiIzgled: TdxBarLargeButton;
    dxBarLBtnBrisiIzgled: TdxBarLargeButton;
    aBrisiIzgled: TAction;
    PanelMain: TPanel;
    GridPanel1: TGridPanel;
    Panel11: TPanel;
    btMasinsko: TcxButton;
    cxLabel2: TcxLabel;
    cxLabel9: TcxLabel;
    cxLabel10: TcxLabel;
    txtVkKolicina1: TcxDBTextEdit;
    txtZavrseni1: TcxDBTextEdit;
    txtOstanati1: TcxDBTextEdit;
    Panel1: TPanel;
    cxButton2: TcxButton;
    cxLabel11: TcxLabel;
    cxLabel12: TcxLabel;
    cxLabel13: TcxLabel;
    btFarbara: TcxButton;
    txtVkKolicina2: TcxDBTextEdit;
    txtZavrseni2: TcxDBTextEdit;
    txtOstanati2: TcxDBTextEdit;
    gbPrebaruvanje: TcxGroupBox;
    Label14: TLabel;
    cxLabel8: TcxLabel;
    cxLabel7: TcxLabel;
    txtDatum: TcxDateEdit;
    cbRN: TcxLookupComboBox;
    cxButton3: TcxButton;
    txtTP: TcxTextEdit;
    txtP: TcxTextEdit;
    cxDBLabel1: TcxDBLabel;
    aPogled: TAction;
    Label1: TLabel;
    txtVidArtikal: TcxTextEdit;
    txtArtikal: TcxTextEdit;
    cbArtikal: TcxLookupComboBox;
    cxDBLabel2: TcxDBLabel;
    cbP: TcxTextEdit;
    cxLabel1: TcxLabel;
    txtPrimeni2: TcxDBTextEdit;
    tblIzberiRN: TpFIBDataSet;
    tblIzberiRNSTATUS: TFIBIntegerField;
    tblIzberiRNNAZIV_PARTNER_PORACKA: TFIBStringField;
    tblIzberiRNZABELESKA: TFIBStringField;
    tblIzberiRNTP_PORACKA: TFIBIntegerField;
    tblIzberiRNP_PORACKA: TFIBIntegerField;
    tblIzberiRNID: TFIBBCDField;
    dsIzberiRN: TDataSource;
    tblIzberiRNBROJ: TFIBStringField;
    tblArtikalRN: TpFIBDataSet;
    tblArtikalRNARTVID: TFIBIntegerField;
    tblArtikalRNID: TFIBIntegerField;
    tblArtikalRNNAZIV: TFIBStringField;
    dsArtikalRN: TDataSource;
    Panel4: TPanel;
    cxButton9: TcxButton;
    cxLabel20: TcxLabel;
    cxLabel21: TcxLabel;
    cxLabel22: TcxLabel;
    cxButton10: TcxButton;
    cxButton15: TcxButton;
    txtVkKolicina5: TcxDBTextEdit;
    txtZavrrseni5: TcxDBTextEdit;
    txtOstanati5: TcxDBTextEdit;
    cxDBLabel5: TcxDBLabel;
    cxLabel5: TcxLabel;
    txtPrimeni5: TcxDBTextEdit;
    Panel2: TPanel;
    cxButton5: TcxButton;
    cxLabel14: TcxLabel;
    cxLabel15: TcxLabel;
    cxLabel16: TcxLabel;
    cxButton6: TcxButton;
    cxButton13: TcxButton;
    txtVkKolicina3: TcxDBTextEdit;
    txtZavrseni3: TcxDBTextEdit;
    txtOstanati3: TcxDBTextEdit;
    cxDBLabel3: TcxDBLabel;
    cxLabel3: TcxLabel;
    txtPrimeni31: TcxDBTextEdit;
    Panel3: TPanel;
    cxButton7: TcxButton;
    cxLabel17: TcxLabel;
    cxLabel18: TcxLabel;
    cxLabel19: TcxLabel;
    cxButton8: TcxButton;
    cxButton14: TcxButton;
    txtVkKolicina4: TcxDBTextEdit;
    txtZavrseni4: TcxDBTextEdit;
    txtOstanati4: TcxDBTextEdit;
    cxDBLabel4: TcxDBLabel;
    cxLabel4: TcxLabel;
    txtPrimeni4: TcxDBTextEdit;
    cxLabel6: TcxLabel;
    txtPrimeni3: TcxDBTextEdit;
    btTapacersko: TcxButton;
    btMagGP: TcxButton;
    btSivara: TcxButton;
    aPoveke: TAction;
    tblIzberiRNID_RE: TFIBIntegerField;
    dPanel1: TPanel;
    btMasinskoNapredok: TcxButton;
    cxlbl1: TcxLabel;
    cxlbl2: TcxLabel;
    cxlbl3: TcxLabel;
    txtVkKolicina6: TcxDBTextEdit;
    txtZavrseni6: TcxDBTextEdit;
    txtOstanati6: TcxDBTextEdit;
    cxDBLabel6: TcxDBLabel;
    dPanel2: TPanel;
    btn1: TcxButton;
    cxlbl4: TcxLabel;
    cxlbl5: TcxLabel;
    cxlbl6: TcxLabel;
    btMebelStip: TcxButton;
    txtVkKolicina8: TcxDBTextEdit;
    txtZavrseni8: TcxDBTextEdit;
    txtOstanati8: TcxDBTextEdit;
    cxDBLabel8: TcxDBLabel;
    cxlbl7: TcxLabel;
    txtPrimeni8: TcxDBTextEdit;
    dPanel3: TPanel;
    btn2: TcxButton;
    cxlbl8: TcxLabel;
    cxlbl9: TcxLabel;
    cxlbl10: TcxLabel;
    txtVkKolicina7: TcxDBTextEdit;
    txtZavrseni7: TcxDBTextEdit;
    txtOstanati7: TcxDBTextEdit;
    cxDBLabel7: TcxDBLabel;
    cxlbl11: TcxLabel;
    txtPrimeni7: TcxDBTextEdit;
    dPanel4: TPanel;
    btn3: TcxButton;
    cxlbl12: TcxLabel;
    cxlbl13: TcxLabel;
    cxlbl14: TcxLabel;
    btn4: TcxButton;
    btn5: TcxButton;
    txtVkKolicina9: TcxDBTextEdit;
    txtZavrseni9: TcxDBTextEdit;
    txtOstanati9: TcxDBTextEdit;
    cxDBLabel9: TcxDBLabel;
    cxlbl15: TcxLabel;
    txtPrimeni9: TcxDBTextEdit;
    btTrgovskaStoka: TcxButton;
    btDorabotkaStip: TcxButton;

    procedure cxDBTextEditAllEnter(Sender: TObject);
    procedure cxDBTextEditAllExit(Sender: TObject);
    procedure EnterKakoTab(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    //procedure PrvPosledenTab(panel:TPanel;var posledna,prva:TWinControl);
    procedure SaveToIniFileExecute(Sender: TObject);
    procedure cxGrid1DBTableView1KeyPress(Sender: TObject; var Key: Char);
    procedure aZapisiExecute(Sender: TObject);
    procedure aOtkaziExecute(Sender: TObject);
    procedure aNovExecute(Sender: TObject);
    procedure aAzurirajExecute(Sender: TObject);
    procedure aBrisiExecute(Sender: TObject);
    procedure aZacuvajExcelExecute(Sender: TObject);
    procedure aRefreshExecute(Sender: TObject);
    procedure aSnimiIzgledExecute(Sender: TObject);
    procedure aIzlezExecute(Sender: TObject);
    procedure aPecatiTabelaExecute(Sender: TObject);
    procedure aSnimiPecatenjeExecute(Sender: TObject);
    procedure zacuvajPrintVoIni(ime:AnsiString);
    procedure procitajPrintOdIni(ime:AnsiString);
    procedure aPodesuvanjePecatenjeExecute(Sender: TObject);
    procedure aBrisiPodesuvanjePecatenjeExecute(Sender: TObject);
    procedure aPageSetupExecute(Sender: TObject);
    procedure cxGrid1DBTableView1FilterCustomization(
      Sender: TcxCustomGridTableView; var ADone: Boolean);
    procedure aFormConfigExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure PartnerPropertiesChange(Sender: TObject);
    procedure aBrisiIzgledExecute(Sender: TObject);
    procedure SetirajLukap(sender:TObject; tabela:TDataSet; tip:TcxTextEdit; sifra:TcxTextEdit; lukap:TcxLookupComboBox);
    procedure aPogledExecute(Sender: TObject);
    procedure cbRNPropertiesChange(Sender: TObject);
    procedure cxLabel24Click(Sender: TObject);
    procedure aPovekeExecute(Sender: TObject);
  private
    { Private declarations }

    _sifra_kluc :array[1..10] of string;  // �������� ���������� �� ������ �� ������� �� ������������� ���

  protected
    list : TList;
    inserting : boolean;
    prva, posledna :TWinControl;
    procedure prefrli;

  public
    { Public declarations }
    constructor Create(Owner : TComponent; insert : boolean = true);reintroduce; overload;

    procedure SetSifra(br: integer; s : string);
    function GetSifra(br: integer) : string;

    //������� �� ���������� ������������ _sifra_kluc
    property sifra_kluc[br :integer]: string read GetSifra write SetSifra;
  end;

var
  frmTekovenProces: TfrmTekovenProces;

implementation

uses DaNe, dmKonekcija, Utils, dmResources, FormConfig, dmMaticni, dmSystem,
  dmUnit, KTIzlez;

{$R *.dfm}
//------------------------------------------------------------------------------

constructor TfrmTekovenProces.Create(Owner : TComponent; insert : boolean = true);
begin
inherited Create(Owner);
//	�������� ���� ������� � �������� �� ����������� ���������
//	insert = true -> ����� ���� ����� ���������� �� ������� �� Insert Mode
//  insert = false -> ����� ���� ����� ������� �� ����� �� ������. �� ������� ����� ����� �� �� �������� F5
  inserting := insert;
end;

//	����� �� �����������. �� ��������� ������� �� ������, �� �������� ������� �� ���� ��������,
//	�� ������� ������� �� ������ ������ � �� ������� �� Insert Mode
procedure TfrmTekovenProces.aNovExecute(Sender: TObject);
begin
//  if(cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) then
//  begin
//    dPanel.Enabled:=True;
//    lPanel.Enabled:=False;
//    prva.SetFocus;
//    cxGrid1DBTableView1.DataController.DataSet.Insert;
//  end
//  else ShowMessage('������ �������� �� ��� �������� �� �� ��������� �������/r/n � ����� ������� �� ������� ��� �����!');
end;

//	����� �� ���������. �� ��������� ������� �� ������, �� �������� ������� �� ���� ��������,
//	�� ������� ������� �� ������ ������ � �� ������� �� Edit Mode
procedure TfrmTekovenProces.aAzurirajExecute(Sender: TObject);
begin
//  if(cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) then
//  begin
//    dPanel.Enabled:=True;
//    lPanel.Enabled:=False;
//    prva.SetFocus;
//    cxGrid1DBTableView1.DataController.DataSet.Edit;
//  end
//  else ShowMessage('������ �������� �� ��� �������� �� �� ��������� �������/r/n � ����� ������� �� ������� ��� �����!');
end;

//	����� �� ������ �� ������������� �����
procedure TfrmTekovenProces.aBrisiExecute(Sender: TObject);
begin
//  if ((cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) and
//     (cxGrid1DBTableView1.DataController.RecordCount <> 0)) then
//    cxGrid1DBTableView1.DataController.DataSet.Delete();
end;

procedure TfrmTekovenProces.aBrisiIzgledExecute(Sender: TObject);
begin
  //brisiGridVoBaza(Name,cxGrid1DBTableView1);
  //brisiPivotVoBaza(Name,cxDBPivotGrid1); �� pivot ��� ���
  BrisiFormaIzgled(self);
end;

//	����� �� ���������� �� ����������
procedure TfrmTekovenProces.aRefreshExecute(Sender: TObject);
begin
//  cxGrid1DBTableView1.DataController.Filter.Root.Clear; //���������� �� ��������
//  cxGrid1DBTableView1.DataController.DataSet.Refresh;
end;

//	����� �� ����� �� ������� �� ������� �� ������� �����
procedure TfrmTekovenProces.aIzlezExecute(Sender: TObject);
begin
  Close;
end;

//	����� �� ������� �� �������� �� ������ �� ���� (Utils.pas)
procedure TfrmTekovenProces.aSnimiIzgledExecute(Sender: TObject);
begin
//  zacuvajGridVoBaza(Name,cxGrid1DBTableView1);
  ZacuvajFormaIzgled(self);
end;

//	����� �� ������� �� ������ �� Excel ������ (Utils.pas)
procedure TfrmTekovenProces.aZacuvajExcelExecute(Sender: TObject);
begin
//  zacuvajVoExcel(cxGrid1, Caption);
end;

//  ��������� �� ������ �� ������� �� ���������� �� ����� �� Enter
procedure TfrmTekovenProces.EnterKakoTab(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
    case Key of
        VK_DOWN:
        begin
          PostMessage(Handle,WM_NextDlgCtl,0,0);
          key:=0;
        end;
        VK_UP:
        begin
          PostMessage(Handle,WM_NextDlgCtl,1,0);
          key:=0;
        end;
        VK_RETURN:
        begin
          PostMessage(Handle,WM_NextDlgCtl,0,0);
        end;
    end;
end;

//	������� �� ��� �� ������ ��� �������� �� �����
procedure TfrmTekovenProces.cxDBTextEditAllEnter(Sender: TObject);
begin
    TEdit(Sender).Color:=clSkyBlue;
end;

//	������� �� ��� �� ������ ��� ����� �� �����
procedure TfrmTekovenProces.cxDBTextEditAllExit(Sender: TObject);
begin
   if ((Sender as TWinControl)= cbRN) then
   begin
     txtTP.Text := tblIzberiRNTP_PORACKA.AsString;
     txtP.Text := tblIzberiRNP_PORACKA.AsString;
    // SetirajLukap();
     cbP.Text := tblIzberiRNNAZIV_PARTNER_PORACKA.Value;
     tblArtikalRN.Close;
     tblArtikalRN.ParamByName('id').Value := tblIzberiRNID.Value;
     tblArtikalRN.Open;

   //  txtRN.Text := dm.tblRabotenNalogBROJ.AsString;
   end;


   if ((Sender as TWinControl)= cbP) then
        begin
            if (cbP.Text <>'') then
            begin
              txtTP.Text := cbP.EditValue[0]; // dm.tblKooperantiTIP_PARTNER.AsString; //cbPartner.EditValue[0];
              txtP.Text := cbP.EditValue[1]; // dm.tblKooperantiID.AsString;
            //  txtMesto.Text :=  dm.tblKooperantiNAZIV_MESTO.Value;  //cbKoop.EditValue[3];
           //   txtAdresa.Text := dm.tblKooperantiADRESA.Value;  //cbKoop.EditValue[4];
            end
            else
            begin
              txtTP.Clear;
              txtP.Clear;
           //   txtMesto.Clear;
            //  txtAdresa.Clear;
            end;
         end;
         if ((Sender as TWinControl)= txtP) or ((Sender as TWinControl)= txtTP) then
         begin
          if (txtTP.Text <>'') and (txtP.Text<>'')  then
          begin
            if(dmMat.tblPartner.Locate('TIP_PARTNER;ID',VarArrayOf([txtTP.text,txtP.text]),[])) then
            begin
               cbP.Text:=dmMat.tblPartnerNAZIV.Value;
             //  txtMesto.Text := dm.tblKooperantiNAZIV_MESTO.Value;
             //  txtAdresa.Text := dm.tblKooperantiADRESA.Value;
            end
           end
            else
            begin
               cbP.Clear;
               //txtMesto.Clear;
               //txtAdresa.Clear;
             end;

         end;

   // if (dm.tblRNStavka.State) in [dsInsert, dsEdit] then
   // begin
   if ((Sender as TWinControl)= cbArtikal) then
        begin
            if (cbArtikal.Text <>'') then
            begin
              txtVidArtikal.Text := tblArtikalRNARTVID.AsString;
              txtArtikal.Text := tblArtikalRNID.AsString;
              //dm.tblNormativO.Close;
              //dm.tblNormativO.ParamByName('mas_id').va
            end
            else
            begin
              txtVidArtikal.Clear;
              txtArtikal.Clear;
            end;
           end
           else
            if ((Sender as TWinControl)= txtArtikal) or ((Sender as TWinControl)= txtVidArtikal) then
                SetirajLukap(sender,tblArtikalRN,txtVidArtikal,txtArtikal, cbArtikal);


//         if (not tblIzberiRNID.IsNull) and (not tblArtikalRN.Active) then
//         begin
//                end;

  //  end;
    TEdit(Sender).Color:=clWhite;
end;

procedure TfrmTekovenProces.cxGrid1DBTableView1FilterCustomization(
  Sender: TcxCustomGridTableView; var ADone: Boolean);
begin
end;

//  ��������� �� ������ �� ��������� �� ���������� ���� �� ��������
//  ��������� ��: br - ����� ��� �� ������ (�� 0 �� 9) �� ��������� �� ����� �� ����� �� ���������� ����
//  s - ��������� �� ������ �� ������ (���������� �� string �� ����� �� integer �� IntToStr ���������)
procedure TfrmTekovenProces.SetSifra(br: integer; s : string);
begin
  _sifra_kluc[br] := s;
end;

//  ������� �� ������ �� ��������� �� ���������� ���� �� ��������
//  ��������� ��: br - ����� ��� �� ������ (�� 0 �� 9) �� ��������� �� ����� �� ����� �� ���������� ����
function TfrmTekovenProces.GetSifra(br: integer) : string;
begin
  Result := _sifra_kluc[br];
end;

procedure TfrmTekovenProces.PartnerPropertiesChange(Sender: TObject);
begin

end;

//  �� ������� �� ������� ��� ����������� �� Master-�� �� �������� �� ��������� �� ������ ����� SetSifra
procedure TfrmTekovenProces.prefrli;
begin
end;

procedure TfrmTekovenProces.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    //	��������� ��� ��������� �� �������
//    if (cxGrid1DBTableView1.DataController.DataSource.State = dsEdit) or (cxGrid1DBTableView1.DataController.DataSource.State = dsInsert) then
//    begin
//        frmDaNe := TfrmDaNe.Create(self, '���������� ��������', '���������� �� �� ��������. ���� ������ �� �� �������?', 1);
//        if (frmDaNe.ShowModal <> mrYes) then
//        begin
//            cxGrid1DBTableView1.DataController.DataSet.Cancel;
//            Action := caFree;
//        end
//        else
//          if (Validacija(dPanel) = false) then
//          begin
//            cxGrid1DBTableView1.DataController.DataSet.Post;
            dm.pTekovenProces.Close;
            dm.tblRabotenNalog.Close;
            Action := caFree;
//          end
//          else Action := caNone;
//    end;
end;
procedure TfrmTekovenProces.FormCreate(Sender: TObject);
begin
  ProcitajFormaIzgled(self);
  if (dmRes.use_skin = true) then dxRibbon1.ColorSchemeName := dmRes.skin_name;

  dm.tblRabotenNalog.Close;
  dm.tblRabotenNalog.ParamByName('status').AsString := '1';
  dm.tblRabotenNalog.ParamByName('godina').AsString := '%';
  // ����������� site = 2 ����� �� �� ������� ���� �� ��� �� �� ������� �� ����� �.�. ������ �� ���� ��
  dm.tblRabotenNalog.ParamByName('site').AsString := '2';
if inttostr(dmKon.UserRE) <> ''  then
  dm.tblRabotenNalog.ParamByName('re').AsInteger := dmkon.UserRE
else
  dm.tblRabotenNalog.ParamByName('re').AsString := '%' ;
  dm.tblRabotenNalog.Open;
end;

//------------------------------------------------------------------------------

procedure TfrmTekovenProces.FormShow(Sender: TObject);
begin
  //  ������� �� ������������ ������������/���������� �� ���������� �� ���� �����
    SpremiForma(self);
  //	�� ��������� �� ������� ������ ��� ���������� � ���� � ��� �������� �� �������
//    PrvPosledenTab(dPanel,posledna,prva);
    //dxBarManager1Bar1.Caption := Caption;
//    dxComponentPrinter1Link1.ReportTitle.Text := Caption;

  //	������� �� ������������ �� ������ �� ������
//    procitajGridOdBaza(Name,cxGrid1DBTableView1,false,false);
  //	������� �� ������������ �� ��������� �� ������
//    procitajPrintOdBaza(Name,cxGrid1DBTableView1.Name, dxComponentPrinter1Link1);
      tblIzberiRN.Close;
      tblIzberiRN.ParamByName('status').AsString := '1';
      tblIzberiRN.Open;
      txtDatum.Date := now;
end;
//------------------------------------------------------------------------------

procedure TfrmTekovenProces.SaveToIniFileExecute(Sender: TObject);
begin
//    cxGrid1DBTableView1.StoreToIniFile(Name,True,[]);
end;
//------------------------------------------------------------------------------

procedure TfrmTekovenProces.cxGrid1DBTableView1KeyPress(Sender: TObject;
  var Key: Char);
begin
//  if(Ord(Key) <> VK_RETURN) then cxSortiraj(cxGrid1DBTableView1);
end;

procedure TfrmTekovenProces.cxLabel24Click(Sender: TObject);
begin

end;

//  ����� �� �����
procedure TfrmTekovenProces.aZapisiExecute(Sender: TObject);
var
  st: TDataSetState;
begin
//  ZapisiButton.SetFocus;

//  st := cxGrid1DBTableView1.DataController.DataSet.State;
//  if st in [dsEdit,dsInsert] then
//  begin
//    if (Validacija(dPanel) = false) then
//    begin
//      if ((st = dsInsert) and inserting) then
//      begin
//        cxGrid1DBTableView1.DataController.DataSet.Post;
//        aNov.Execute;
//      end;
//
//      if ((st = dsInsert) and (not inserting)) then
//      begin
//        cxGrid1DBTableView1.DataController.DataSet.Post;
//        dPanel.Enabled:=false;
//        lPanel.Enabled:=true;
//        cxGrid1.SetFocus;
//      end;
//
//      if (st = dsEdit) then
//      begin
//        cxGrid1DBTableView1.DataController.DataSet.Post;
//        dPanel.Enabled:=false;
//        lPanel.Enabled:=true;
//        cxGrid1.SetFocus;
//      end;
//    end;
//  end;
end;

procedure TfrmTekovenProces.cbRNPropertiesChange(Sender: TObject);
begin

end;

//	����� �� ���������� �� �������
procedure TfrmTekovenProces.aOtkaziExecute(Sender: TObject);
begin
//  if (cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) then
//  begin
//      ModalResult := mrCancel;
     Close();
//  end
//  else
//  begin
//      cxGrid1DBTableView1.DataController.DataSet.Cancel;
//      RestoreControls(dPanel);
//      dPanel.Enabled := false;
//      lPanel.Enabled := true;
//      cxGrid1.SetFocus;
//  end;
end;

//----------------------------------------------------------------------------------
// ��������� �� ��������, ���������� �� ��������� � �������� �� Print ��������
//----------------------------------------------------------------------------------

//	����� �� �������� �� �������� �� �������
procedure TfrmTekovenProces.aPageSetupExecute(Sender: TObject);
begin
  dxComponentPrinter1Link1.PageSetup;
end;

//	����� �� ������� �� ������
procedure TfrmTekovenProces.aPecatiTabelaExecute(Sender: TObject);
begin
  dxComponentPrinter1Link1.ReportTitle.Text := Caption;

  dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Clear;
  dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Add(dmkon.firma_naziv);
  dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Add(DateTimeToStr(Now));

  dxComponentPrinter1Link1.PrinterPage.PageHeader.LeftTitle.Clear;
  //dxComponentPrinter1Link1.PrinterPage.PageHeader.LeftTitle.Add('������� : ' + TipSifra.Text + '-' + IdSifra.Text + ' ' + Partner.Text);

  dxComponentPrinter1.Preview(true, dxComponentPrinter1Link1);
end;

//	����� �� ���������� �� ��������� �� �������
procedure TfrmTekovenProces.aPodesuvanjePecatenjeExecute(Sender: TObject);
begin
  dxComponentPrinter1Link1.DesignReport();
end;

procedure TfrmTekovenProces.aPogledExecute(Sender: TObject);
begin
 if (cbRN.Text <> '') and (txtDatum.Text <> '') then
   begin
       dm.pTekovenProces.Close;
       dm.pTekovenProces.ParamByName('datum').AsString := txtDatum.Text;
       dm.pTekovenProces.ParamByName('rn').AsString := tblIzberiRNID.AsString;;
     if txtVidArtikal.Text <> '' then
      begin
       dm.pTekovenProces.ParamByName('vid_Artikal').AsString := txtVidArtikal.Text;
       dm.pTekovenProces.ParamByName('artikal').AsString := txtArtikal.Text;
      end
      else
      begin
       dm.pTekovenProces.ParamByName('vid_Artikal').AsString := '%';
       dm.pTekovenProces.ParamByName('artikal').AsString := '%';
      end;
       dm.pTekovenProces.Open;
   end
   else
   begin
     ShowMessage('������� �� �������������� �����!');
     Abort;
   end;
end;

procedure TfrmTekovenProces.aPovekeExecute(Sender: TObject);
begin
  frmKTIzlez := TfrmKTIzlez.Create(Self);
  frmKTIzlez.id_rn := tblIzberiRNID.Value;
  frmKTIzlez.id_re := tblIzberiRNID_RE.Value;
  if txtVidArtikal.Text <> '' then
  begin
    frmKTIzlez.vid_artikal := txtVidArtikal.Text;
    frmKTIzlez.artikal := txtArtikal.Text;
  end
  else
  begin
    frmKTIzlez.vid_artikal := '%';
    frmKTIzlez.artikal := '%';
  end;
  if btMasinsko.Focused then
  begin
   frmKTIzlez.id_kt := dm.pTekovenProcesID_KT1.Value;
   frmKTIzlez.Caption := '������� ������� '+dm.pTekovenProcesKT_NAZIV1.Value;
   frmKTIzlez.lblKT.Caption := '�� '+ dm.pTekovenProcesKT_NAZIV1.Value+', ��: '+cbRN.Text+', �� �����: '+txtDatum.Text;
   frmKTIzlez.lblKT.Style.Color := $00C4FFFF;
  end
  else
  if btFarbara.Focused then
  begin
   frmKTIzlez.id_kt := dm.pTekovenProcesID_KT2.Value;
   frmKTIzlez.Caption := '������� ������� '+dm.pTekovenProcesKT_NAZIV2.Value;;
   frmKTIzlez.lblKT.Caption := dm.pTekovenProcesKT_NAZIV2.Value;
   frmKTIzlez.lblKT.Caption := '�� '+ dm.pTekovenProcesKT_NAZIV2.Value+', ��: '+cbRN.Text+', �� �����: '+txtDatum.Text;
   frmKTIzlez.lblKT.Style.Color := $00A8D3FF;
  end
  else
  if btTapacersko.Focused then
  begin
   frmKTIzlez.id_kt := dm.pTekovenProcesID_KT4.Value;
   frmKTIzlez.Caption := '������� ������� '+ dm.pTekovenProcesKT_NAZIV4.Value;
   frmKTIzlez.lblKT.Caption := dm.pTekovenProcesKT_NAZIV4.Value;
   frmKTIzlez.lblKT.Caption := '�� '+ dm.pTekovenProcesKT_NAZIV4.Value+', ��: '+cbRN.Text+', �� �����: '+txtDatum.Text;
   frmKTIzlez.lblKT.Style.Color := $00D2FFC4;
  end
  else
  if btMagGP.Focused then
  begin
   frmKTIzlez.id_kt := dm.pTekovenProcesID_KT5.Value;
   frmKTIzlez.Caption := '������� ������� '+ dm.pTekovenProcesKT_NAZIV5.Value;
   frmKTIzlez.lblKT.Caption := dm.pTekovenProcesKT_NAZIV5.Value;
   frmKTIzlez.lblKT.Caption := '�� '+ dm.pTekovenProcesKT_NAZIV5.Value+', ��: '+cbRN.Text+', �� �����: '+txtDatum.Text;
   frmKTIzlez.lblKT.Style.Color := $00FFE7B3;
  end
  else
  if btSivara.Focused then
  begin
   frmKTIzlez.id_kt := dm.pTekovenProcesID_KT3.Value;
   frmKTIzlez.Caption := '������� ������� '+ dm.pTekovenProcesKT_NAZIV3.Value;
   frmKTIzlez.lblKT.Caption := dm.pTekovenProcesKT_NAZIV3.Value;
   frmKTIzlez.lblKT.Caption := '�� '+ dm.pTekovenProcesKT_NAZIV3.Value+', ��: '+cbRN.Text+', �� �����: '+txtDatum.Text;
   frmKTIzlez.lblKT.Style.Color := $00E1C4FF;
  end
  else
  if btMasinskoNapredok.Focused then
  begin
   frmKTIzlez.id_kt := dm.pTekovenProcesID_KT6.Value;
   frmKTIzlez.Caption := '������� ������� '+dm.pTekovenProcesKT_NAZIV6.Value;
   frmKTIzlez.lblKT.Caption := '�� '+ dm.pTekovenProcesKT_NAZIV6.Value+', ��: '+cbRN.Text+', �� �����: '+txtDatum.Text;
   frmKTIzlez.lblKT.Style.Color := $00C4FFFF;
  end
  else
  if btDorabotkaStip.Focused then
  begin
   frmKTIzlez.id_kt := dm.pTekovenProcesID_KT9.Value;
   frmKTIzlez.Caption := '������� ������� '+dm.pTekovenProcesKT_NAZIV9.Value;;
   frmKTIzlez.lblKT.Caption := dm.pTekovenProcesKT_NAZIV9.Value;
   frmKTIzlez.lblKT.Caption := '�� '+ dm.pTekovenProcesKT_NAZIV9.Value+', ��: '+cbRN.Text+', �� �����: '+txtDatum.Text;
   frmKTIzlez.lblKT.Style.Color := $00A8D3FF;
  end
  else
  if btMebelStip.Focused then
  begin
   frmKTIzlez.id_kt := dm.pTekovenProcesID_KT7.Value;
   frmKTIzlez.Caption := '������� ������� '+dm.pTekovenProcesKT_NAZIV7.Value;;
   frmKTIzlez.lblKT.Caption := dm.pTekovenProcesKT_NAZIV7.Value;
   frmKTIzlez.lblKT.Caption := '�� '+ dm.pTekovenProcesKT_NAZIV7.Value+', ��: '+cbRN.Text+', �� �����: '+txtDatum.Text;
   frmKTIzlez.lblKT.Style.Color := $00A8D3FF;
  end
  else
  if btTrgovskaStoka.Focused then
  begin
   frmKTIzlez.id_kt := dm.pTekovenProcesID_KT8.Value;
   frmKTIzlez.Caption := '������� ������� '+dm.pTekovenProcesKT_NAZIV8.Value;;
   frmKTIzlez.lblKT.Caption := dm.pTekovenProcesKT_NAZIV8.Value;
   frmKTIzlez.lblKT.Caption := '�� '+ dm.pTekovenProcesKT_NAZIV8.Value+', ��: '+cbRN.Text+', �� �����: '+txtDatum.Text;
   frmKTIzlez.lblKT.Style.Color := $00A8D3FF;
  end;

  //frmKTIzlez.Caption := '������� ������� '+cxDBLabel1.Caption;
  frmKTIzlez.ShowModal;
  frmKTIzlez.Free;
end;

procedure TfrmTekovenProces.aSnimiPecatenjeExecute(Sender: TObject);
begin
//  zacuvajPrintVoBaza(Name,cxGrid1DBTableView1.Name,dxComponentPrinter1Link1);
end;

//	����� �� ������ �� ����������� �������� �� ������� (Utils.pas)
procedure TfrmTekovenProces.aBrisiPodesuvanjePecatenjeExecute(Sender: TObject);
begin
//  brisiPrintOdBaza(Name,cxGrid1DBTableView1.Name, dxComponentPrinter1Link1);
end;

//  ����� �� ����� �� ����� �� ������������� �� �������� � ��������������� ��
//  �������(������ ���������� ��������)
procedure TfrmTekovenProces.aFormConfigExecute(Sender: TObject);
begin
  frmFormConfig := TfrmFormConfig.Create(Application);
  frmFormConfig.formPtr := Addr(Self);
  frmFormConfig.ShowModal;
  frmFormConfig.Free;
end;

//----------------------------------------------------------------------------------
// ���� -> ��������� �� ��������, ���������� �� ��������� � �������� �� Print ��������
//----------------------------------------------------------------------------------

// ��������� �� ���������� �� ��������� � �������� �� Print �������� �� .ini ��������
procedure TfrmTekovenProces.zacuvajPrintVoIni(ime:AnsiString);
var
  AStream: TFileStream;
begin
  AStream := TFileStream.Create(dmKon.RabotenDir + ime + 'Print.ini', fmCreate);
  try
    AStream.WriteComponent(dxComponentPrinter1Link1);
  finally
    AStream.Free;
  end;

end;

// ��������� �� ������ �� ��������� � �������� �� Print �������� �������� �� .ini ��������
procedure TfrmTekovenProces.procitajPrintOdIni(ime:AnsiString);
var
  AStream: TFileStream;
begin
  AStream := TFileStream.Create(dmKon.RabotenDir + ime + 'Print.ini', fmOpenRead);
  try
    dxComponentPrinter1Link1.RestoreDefaults;
    AStream.ReadComponent(dxComponentPrinter1Link1);
  finally
    AStream.Free;
  end;
end;

procedure TfrmTekovenProces.SetirajLukap(sender:TObject; tabela:TDataSet; tip:TcxTextEdit; sifra:TcxTextEdit; lukap:TcxLookupComboBox);
begin
         if (tip.Text <>'') and (sifra.Text<>'')  then
         begin
          if(tabela.Locate('ARTVID;ID',VarArrayOf([tip.text,sifra.text]),[])) then
               lukap.Text:=tabela.FieldByName('NAZIV').Value;
         end
         else
         begin
            lukap.Clear;
         end;
end;


end.
