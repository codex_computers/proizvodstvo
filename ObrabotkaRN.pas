unit ObrabotkaRN;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData, cxEdit,
  DB, cxDBData, cxGridCustomPopupMenu, cxGridPopupMenu,
  ComCtrls, cxContainer, cxTextEdit, cxDBEdit, StdCtrls, cxGridLevel,
  cxClasses, cxControls, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGrid, ExtCtrls, Menus, ActnList,
  cxGridExportLink, cxExport, cxLookAndFeelPainters, cxDataStorage,
  cxButtons, dxStatusBar, dxRibbonStatusBar, dxRibbon, dxBar, dxBarExtItems,
  dxPSGlbl, dxPSUtl, dxPSEngn, dxPrnPg, dxBkgnd, dxWrap, dxPrnDev,
  dxPSCompsProvider, dxPSFillPatterns, dxPSEdgePatterns, dxPSCore, dxPScxCommon,
  cxPC, cxMemo, cxLocalization, cxLookAndFeels,
  dxPSPDFExportCore, dxPSPDFExport, cxDrawTextUtils, dxPSPrVwStd, dxPSPrVwAdv,
  dxPSPrVwRibbon, dxPScxEditorProducers, dxPScxExtEditorProducers,
  dxPScxPageControlProducer, dxPgsDlg, cxCheckBox, cxBarEditItem, dxSkinsCore,
  dxSkinBlack, dxSkinBlue, dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom,
  dxSkinDarkSide, dxSkinFoggy, dxSkinGlassOceans, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven,
  dxSkinSharp, dxSkinSilver, dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008,
  dxSkinsDefaultPainters, dxSkinValentine, dxSkinXmas2008Blue,
  dxSkinsdxRibbonPainter, dxSkinsdxBarPainter, dxSkinscxPCPainter,
  dxBarSkinnedCustForm, cxDropDownEdit, cxCalendar, cxDBLookupComboBox,
  cxMaskEdit, cxLookupEdit, cxDBLookupEdit, cxDBExtLookupComboBox, cxGroupBox,
  cxRadioGroup, dxRibbonSkins, dxPScxGridLnk, dxPScxGridLayoutViewLnk,
  dxScreenTip, dxSkinBlueprint, dxSkinDevExpressDarkStyle,
  dxSkinDevExpressStyle, dxSkinHighContrast, dxSkinSevenClassic,
  dxSkinSharpPlus, dxSkinTheAsphaltWorld, dxSkinVS2010, dxSkinWhiteprint,
  dxSkinsdxNavBarPainter, cxPCdxBarPopupMenu, dxNavBar, dxNavBarCollns,
  dxNavBarBase, cxLabel, dxSkinsdxStatusBarPainter, cxColorComboBox,
  cxDBColorComboBox, cxDBLabel, cxButtonEdit, Vcl.ExtDlgs, cxImage,
  Vcl.Samples.Gauges, cxScheduler, cxSchedulerStorage,
  cxSchedulerCustomControls, cxSchedulerCustomResourceView, cxSchedulerDayView,
  cxSchedulerDateNavigator, cxSchedulerHolidays, cxSchedulerTimeGridView,
  cxSchedulerUtils, cxSchedulerWeekView, cxSchedulerYearView,
  cxSchedulerGanttView, dxSkinscxSchedulerPainter, cxSchedulercxGridConnection,
  cxSchedulerDBStorage, cxImageComboBox, Vcl.ImgList, dxPScxSchedulerLnk,
  cxPropertiesStore, dxLayoutContainer, cxGridLayoutView, cxGridDBLayoutView,
  cxGridCustomLayoutView, cxGridCardView, cxGridDBCardView,
  cxGridBandedTableView, cxGridDBBandedTableView, FIBDataSet, pFIBDataSet,
  dxLayoutcxEditAdapters, dxLayoutControl;

type
//  niza = Array[1..5] of Variant;

  TfrmObrabotkaRN = class(TForm)
    cxGridPopupMenu1: TcxGridPopupMenu;
    PopupMenu1: TPopupMenu;
    N1: TMenuItem;
    Excel1: TMenuItem;
    dxRibbon1Tab1: TdxRibbonTab;
    dxRibbon1: TdxRibbon;
    StatusBar1: TdxRibbonStatusBar;
    dxBarManager1: TdxBarManager;
    dxBarManager1Bar2: TdxBar;
    dxBarManager1Bar3: TdxBar;
    dxBarLargeButton2: TdxBarLargeButton;
    dxBarLargeButton3: TdxBarLargeButton;
    dxBarLargeButton4: TdxBarLargeButton;
    dxBarLargeButton5: TdxBarLargeButton;
    dxBarLargeButton6: TdxBarLargeButton;
    dxBarLargeButton7: TdxBarLargeButton;
    dxBarLargeButton8: TdxBarLargeButton;
    dxBarLargeButton1: TdxBarLargeButton;
    ActionList1: TActionList;
    aNov: TAction;
    aAzuriraj: TAction;
    aBrisi: TAction;
    aSnimiIzgled: TAction;
    aZacuvajExcel: TAction;
    aPecatiTabela: TAction;
    aHelp: TAction;
    aIzlez: TAction;
    aZapisi: TAction;
    aOtkazi: TAction;
    aRefresh: TAction;
    dxRibbon1Tab2: TdxRibbonTab;
    dxBarManager1Bar4: TdxBar;
    dxBarLargeButton9: TdxBarLargeButton;
    aSnimiPecatenje: TAction;
    dxComponentPrinter1: TdxComponentPrinter;
    dxComponentPrinter1Link1: TdxGridReportLink;
    dxBarLargeButton11: TdxBarLargeButton;
    ribbonFilterReset: TdxBarLargeButton;
    aPodesuvanjePecatenje: TAction;
    dxBarLargeButton12: TdxBarLargeButton;
    aBrisiPodesuvanjePecatenje: TAction;
    dxBarLargeButton13: TdxBarLargeButton;
    dxBarLargeButton14: TdxBarLargeButton;
    aPageSetup: TAction;
    dxBarLargeButton15: TdxBarLargeButton;
    dxBarLargeButton16: TdxBarLargeButton;
    dxBarLargeButton17: TdxBarLargeButton;
    cxBarEditItem1: TcxBarEditItem;
    dxBarSubItem1: TdxBarSubItem;
    aFormConfig: TAction;
    dxBarScreenTipRepository1: TdxBarScreenTipRepository;
    tipSnimiIzgled: TdxBarScreenTip;
    tipZacuvajExcel: TdxBarScreenTip;
    tipDizajnReport: TdxBarScreenTip;
    tipSetiranjeStrana: TdxBarScreenTip;
    tipSnimiKonf: TdxBarScreenTip;
    tipBrisiKonf: TdxBarScreenTip;
    dxBarManager1BarIzgledGrid: TdxBar;
    dxBarLBtnSnimiIzgled: TdxBarLargeButton;
    dxBarLBtnBrisiIzgled: TdxBarLargeButton;
    aBrisiIzgled: TAction;
    cxPageControl1: TcxPageControl;
    tsListaRN: TcxTabSheet;
    lPanel: TPanel;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1ID: TcxGridDBColumn;
    cxGrid1DBTableView1BROJ: TcxGridDBColumn;
    cxGrid1DBTableView1ID_PORACKA: TcxGridDBColumn;
    cxGrid1DBTableView1ID_RE: TcxGridDBColumn;
    cxGrid1DBTableView1DATUM_PLANIRAN_POCETOK: TcxGridDBColumn;
    cxGrid1DBTableView1DATUM_START: TcxGridDBColumn;
    cxGrid1DBTableView1SOURCE_DOKUMENT: TcxGridDBColumn;
    cxGrid1DBTableView1STATUS: TcxGridDBColumn;
    cxGrid1DBTableView1NAZIV_STATUS: TcxGridDBColumn;
    cxGrid1DBTableView1ID_RE_SUROVINI: TcxGridDBColumn;
    cxGrid1DBTableView1NAZIV_MAGACIN_SUROVINI: TcxGridDBColumn;
    cxGrid1DBTableView1ID_RE_GOTOV_PROIZVOD: TcxGridDBColumn;
    cxGrid1DBTableView1NAZIV_MAGACIN_GOTOVI_PROIZVODI: TcxGridDBColumn;
    cxGrid1DBTableView1PRIORITET: TcxGridDBColumn;
    cxGrid1DBTableView1DATUM_PLANIRAN_KRAJ: TcxGridDBColumn;
    cxGrid1DBTableView1DATUM_KRAJ: TcxGridDBColumn;
    cxGrid1DBTableView1BR_CASOVI: TcxGridDBColumn;
    cxGrid1DBTableView1BR_CIKLUSI: TcxGridDBColumn;
    tsNormativ: TcxTabSheet;
    aListaRN: TAction;
    cxGroupBox3: TcxGroupBox;
    cxGrid2: TcxGrid;
    cxGrid2DBTableView1: TcxGridDBTableView;
    cxGrid2Level1: TcxGridLevel;
    cxGrid1DBTableView1GODINA: TcxGridDBColumn;
    cxGrid2DBTableView1VID_ARTIKAL_N: TcxGridDBColumn;
    cxGrid2DBTableView1ARTIKAL_N: TcxGridDBColumn;
    cxGrid2DBTableView1NAZIV_ARTIKAL: TcxGridDBColumn;
    cxGrid2DBTableView1KOLICINA: TcxGridDBColumn;
    cxGrid2DBTableView1MERKA: TcxGridDBColumn;
    cxGrid2DBTableView1AKTIVEN: TcxGridDBColumn;
    OpenPictureDialog1: TOpenPictureDialog;
    aNova: TAction;
    dxBarManager1Bar5: TdxBar;
    dxBarLargeButton10: TdxBarLargeButton;
    aNovv: TAction;
    aAzurirajS: TAction;
    dxBarLargeButton18: TdxBarLargeButton;
    aAzurirajj: TAction;
    dxBarLargeButton19: TdxBarLargeButton;
    aBrisiS: TAction;
    aBrisii: TAction;
    aPogledRN: TAction;
    tsOperacii: TcxTabSheet;
    cxGroupBox2: TcxGroupBox;
    cxGrid5: TcxGrid;
    cxGridDBTableView2: TcxGridDBTableView;
    cxGridLevel2: TcxGridLevel;
    cxGridDBTableView2OPERACIJA_N: TcxGridDBColumn;
    cxGridDBTableView2NAZIV_OPERACIJA: TcxGridDBColumn;
    cxGridDBTableView2POTREBNO_VREME: TcxGridDBColumn;
    cxGridDBTableView2MERKA: TcxGridDBColumn;
    cxGridDBTableView2AKTIVEN: TcxGridDBColumn;
    cxGridDBTableView2SEKVENCA: TcxGridDBColumn;
    cxPropertiesStore1: TcxPropertiesStore;
    aSiteRN: TAction;
    aOtvoreniRN: TAction;
    dxBarLargeButton20: TdxBarLargeButton;
    dxBarLargeButton21: TdxBarLargeButton;
    cxGrid1DBTableView1Column1: TcxGridDBColumn;
    cxGrid1DBTableView1STATUS_STAVKI: TcxGridDBColumn;
    cxGrid1DBTableView1NAZIV_STATUS_STAVKI: TcxGridDBColumn;
    aSmeniStatus: TAction;
    cxGroupBox9: TcxGroupBox;
    cxGrid7: TcxGrid;
    cxGridDBTableView4: TcxGridDBTableView;
    cxGridLevel4: TcxGridLevel;
    PanelSurovini: TPanel;
    cxGridDBTableView4ID: TcxGridDBColumn;
    cxGridDBTableView4ID_RABOTEN_NALOG: TcxGridDBColumn;
    cxGridDBTableView4VID_SUROVINA: TcxGridDBColumn;
    cxGridDBTableView4NAZIV_SUROVINA: TcxGridDBColumn;
    cxGridDBTableView4SUROVINA: TcxGridDBColumn;
    cxGridDBTableView4KOLICINA: TcxGridDBColumn;
    cxGridDBTableView4MERKA: TcxGridDBColumn;
    cxGridDBTableView4NAZIV_MERKA: TcxGridDBColumn;
    cxGridDBTableView4PRODUKCISKI_LOT: TcxGridDBColumn;
    cxLabel32: TcxLabel;
    txtVidSurovina: TcxDBTextEdit;
    txtSurovina: TcxDBTextEdit;
    cbSurovina: TcxLookupComboBox;
    cxLabel33: TcxLabel;
    txtKolicina: TcxDBTextEdit;
    cxLabel34: TcxLabel;
    txtMerka: TcxDBTextEdit;
    cbMerka: TcxDBLookupComboBox;
    cxLabel35: TcxLabel;
    txtMagacin: TcxDBTextEdit;
    cbMagacin: TcxDBLookupComboBox;
    cxGrid1Level2: TcxGridLevel;
    cxGrid1DBTableView2: TcxGridDBTableView;
    cxGrid1DBTableView2ID: TcxGridDBColumn;
    cxGrid1DBTableView2BROJ: TcxGridDBColumn;
    cxGrid1DBTableView2VID_ARTIKAL: TcxGridDBColumn;
    cxGrid1DBTableView2ARTIKAL: TcxGridDBColumn;
    cxGrid1DBTableView2ID_PORACKA: TcxGridDBColumn;
    cxGrid1DBTableView2ZABELESKA: TcxGridDBColumn;
    cxGrid1DBTableView2BROJ_CASOVI: TcxGridDBColumn;
    cxGrid1DBTableView2CUSTOM1: TcxGridDBColumn;
    cxGrid1DBTableView2CUSTOM2: TcxGridDBColumn;
    cxGrid1DBTableView2CUSTOM3: TcxGridDBColumn;
    cxGrid1DBTableView2NAZIV_ARTIKAL: TcxGridDBColumn;
    cxGrid1DBTableView2NAZIV_MERKA: TcxGridDBColumn;
    cxGrid1DBTableView2STATUS: TcxGridDBColumn;
    cxGrid1DBTableView2OD_DATUM_PLANIRAN: TcxGridDBColumn;
    cxGrid1DBTableView2DO_DATUM_PLANIRAN: TcxGridDBColumn;
    cxGrid1DBTableView2OD_DATUM_REALIZACIJA: TcxGridDBColumn;
    cxGrid1DBTableView2DO_DATUM_REALIZACIJA: TcxGridDBColumn;
    cxGrid1DBTableView2DATUM_OD_ISPORAKA: TcxGridDBColumn;
    cxGrid1DBTableView2DATUM_DO_ISPORAKA: TcxGridDBColumn;
    cxGrid1DBTableView2KRAEN_DATUM_PREVOZ: TcxGridDBColumn;
    cxGrid1DBTableView2KOLICINA: TcxGridDBColumn;
    cxGrid1DBTableView2MERKA: TcxGridDBColumn;
    cxGrid1DBTableView2VREDNOST: TcxGridDBColumn;
    cxGrid1DBTableView2BOJA: TcxGridDBColumn;
    cxGrid1DBTableView2NAZIV_STATUS: TcxGridDBColumn;
    cxGrid2DBTableView2: TcxGridDBTableView;
    cxGrid2DBTableView2OPERACIJA_N: TcxGridDBColumn;
    cxGrid2DBTableView2NAZIV_OPERACIJA: TcxGridDBColumn;
    cxGrid2DBTableView2POTREBNO_VREME: TcxGridDBColumn;
    cxGrid2DBTableView2MERKA: TcxGridDBColumn;
    cxGrid2DBTableView2SEKVENCA: TcxGridDBColumn;
    cxGrid2Level2: TcxGridLevel;
    aPromeniStatus: TAction;
    cxGroupBox4: TcxGroupBox;
    cxDBLabel1: TcxDBLabel;
    cxLabel31: TcxLabel;
    cxDBButtonEdit2: TcxDBButtonEdit;
    Action1: TAction;
    cxButton2: TcxButton;
    cxButton4: TcxButton;
    cxGroupBox7: TcxGroupBox;
    cxDBLabel2: TcxDBLabel;
    cxLabel1: TcxLabel;
    cxDBButtonEdit1: TcxDBButtonEdit;
    lblMernaEdinica: TcxDBLabel;
    cxLabel2: TcxLabel;
    txtProdukciskiLot: TcxDBTextEdit;

    procedure cxDBTextEditAllEnter(Sender: TObject);
    procedure cxDBTextEditAllExit(Sender: TObject);
    procedure EnterKakoTab(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    //procedure PrvPosledenTab(panel:TPanel;var posledna,prva:TWinControl);
    procedure SaveToIniFileExecute(Sender: TObject);
    procedure cxGrid1DBTableView1KeyPress(Sender: TObject; var Key: Char);
    procedure aZapisiExecute(Sender: TObject);
    procedure aOtkaziExecute(Sender: TObject);
    procedure aNovExecute(Sender: TObject);
    procedure aAzurirajExecute(Sender: TObject);
    procedure aBrisiExecute(Sender: TObject);
    procedure aZacuvajExcelExecute(Sender: TObject);
    procedure aRefreshExecute(Sender: TObject);
    procedure aSnimiIzgledExecute(Sender: TObject);
    procedure aIzlezExecute(Sender: TObject);
    procedure aPecatiTabelaExecute(Sender: TObject);
    procedure aSnimiPecatenjeExecute(Sender: TObject);
    procedure zacuvajPrintVoIni(ime:AnsiString);
    procedure procitajPrintOdIni(ime:AnsiString);
    procedure aPodesuvanjePecatenjeExecute(Sender: TObject);
    procedure aBrisiPodesuvanjePecatenjeExecute(Sender: TObject);
    procedure aPageSetupExecute(Sender: TObject);
    procedure cxGrid1DBTableView1FilterCustomization(
      Sender: TcxCustomGridTableView; var ADone: Boolean);
    procedure aFormConfigExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure PartnerPropertiesChange(Sender: TObject);
    procedure aBrisiIzgledExecute(Sender: TObject);
    procedure gbRabotenNalogMeasureCaptionHeight(Sender: TcxCustomGroupBox;
      const APainter: TcxCustomLookAndFeelPainter; var ACaptionHeight: Integer);
    procedure SetirajLukap(sender:TObject; tabela:TDataSet; tip:TcxDBTextEdit; sifra:TcxDBTextEdit; lukap:TcxLookupComboBox);
    procedure ActionList1Execute(Action: TBasicAction; var Handled: Boolean);
    procedure aListaRNExecute(Sender: TObject);
    procedure CustomFields();
    procedure XPColorMap1ColorChange(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure StatusBoi;
    procedure aNovaExecute(Sender: TObject);
    procedure aNovvExecute(Sender: TObject);
    procedure cxGrid3DBTableView1FocusedRecordChanged(
      Sender: TcxCustomGridTableView; APrevFocusedRecord,
      AFocusedRecord: TcxCustomGridRecord;
      ANewItemRecordFocusingChanged: Boolean);
    procedure aAzurirajSExecute(Sender: TObject);
    procedure aAzurirajjExecute(Sender: TObject);
    procedure aBrisiSExecute(Sender: TObject);
    procedure aBrisiiExecute(Sender: TObject);
    procedure cxPageControl1Change(Sender: TObject);
    procedure aPogledRNExecute(Sender: TObject);
    procedure cxGrid1DBTableView1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure cxGrid1DBTableView1DblClick(Sender: TObject);
    procedure aSiteRNExecute(Sender: TObject);
    procedure aOtvoreniRNExecute(Sender: TObject);
    procedure cxGrid3LayoutChanged(Sender: TcxCustomGrid;
      AGridView: TcxCustomGridView);
    procedure cxGrid3FocusedViewChanged(Sender: TcxCustomGrid; APrevFocusedView,
      AFocusedView: TcxCustomGridView);
    procedure aSmeniStatusExecute(Sender: TObject);
    procedure cxGrid1DBTableView1DataControllerDetailExpanded(
      ADataController: TcxCustomDataController; ARecordIndex: Integer);
    procedure cxGrid1DBTableView1DataControllerDetailExpanding(
      ADataController: TcxCustomDataController; ARecordIndex: Integer;
      var AAllow: Boolean);
    procedure cxGrid1DBTableView1DataControllerDetailCollapsed(
      ADataController: TcxCustomDataController; ARecordIndex: Integer);
    procedure aPromeniStatusExecute(Sender: TObject);
    procedure cxGridDBTableView4FocusedRecordChanged(
      Sender: TcxCustomGridTableView; APrevFocusedRecord,
      AFocusedRecord: TcxCustomGridRecord;
      ANewItemRecordFocusingChanged: Boolean);
  private
    { Private declarations }

    _sifra_kluc :array[1..10] of string;
    procedure OtvoreniRN;
    procedure OtvoriNormativ;
    procedure OtvoriOperacija;  // �������� ���������� �� ������ �� ������� �� ������������� ���

  protected
    list : TList;
    inserting : boolean;
    prva, posledna :TWinControl;
    custom1,custom2,custom3 : AnsiString;
    procedure prefrli;

  public
    { Public declarations }
    constructor Create(Owner : TComponent; insert : boolean = true);reintroduce; overload;

    procedure SetSifra(br: integer; s : string);
    function GetSifra(br: integer) : string;

    //������� �� ���������� ������������ _sifra_kluc
    property sifra_kluc[br :integer]: string read GetSifra write SetSifra;
  end;

var
  frmObrabotkaRN: TfrmObrabotkaRN;

implementation

uses DaNe, dmKonekcija, Utils, dmResources, FormConfig, dmMaticni, dmSystem,
   Login, dmUnit, CustomFields;

{$R *.dfm}
//------------------------------------------------------------------------------

constructor TfrmObrabotkaRN.Create(Owner : TComponent; insert : boolean = true);
begin
inherited Create(Owner);
//	�������� ���� ������� � �������� �� ����������� ���������
//	insert = true -> ����� ���� ����� ���������� �� ������� �� Insert Mode
//  insert = false -> ����� ���� ����� ������� �� ����� �� ������. �� ������� ����� ����� �� �� �������� F5
  inserting := insert;
end;

//	����� �� �����������. �� ��������� ������� �� ������, �� �������� ������� �� ���� ��������,
//	�� ������� ������� �� ������ ������ � �� ������� �� Insert Mode
procedure TfrmObrabotkaRN.aNovaExecute(Sender: TObject);
begin
  if (cxPageControl1.ActivePageIndex = 2)
  //and (a)
  then
  begin
  if(dm.tblRabotenNalog.State = dsBrowse) then
  begin
    PanelSurovini.Enabled := true;
    //gbRabotenNalog.Enabled := true;
    cxGroupBox9.Enabled:=False;
    lPanel.Enabled:=false;
    txtMagacin.SetFocus;
    dm.tblRNSurovini.Insert;
  end
  end

end;

procedure TfrmObrabotkaRN.aNovExecute(Sender: TObject);
begin
// if cxPageControl1.ActivePageIndex = 1 then
//  begin
//  if(dm.tblRabotenNalog.State = dsBrowse) then
//  begin
//    tsRN.TabVisible := true;
//    //cxPageControl1.ActivePageIndex := 1;
//    dPanel.Enabled := true;
//    gbRabotenNalog.Enabled := true;
//    lPanel.Enabled:=False;
//    Panel1.Enabled:=false;
//    txtBroj.SetFocus;
//    dm.tblRabotenNalog.Insert;
//  end
//  end
//  else
//  if cxPageControl1.ActivePageIndex = 2 then
//  begin
//    if(dm.tblRNStavka.State = dsBrowse) then
//  begin
//    tsStavki.TabVisible := true;
//   // cxPageControl1.ActivePageIndex := 2;
//    lPanelO.Enabled := true;
//    gbRNStavki.Enabled := true;
//    dPanelo.Enabled:=False;
//    txtVidArtikal.SetFocus;
//    dm.tblRNStavka.Insert;
//  end
//  end
//  else ShowMessage('������ �������� �� ��� �������� �� �� ��������� �������/r/n � ����� ������� �� ������� ��� �����!');
end;

procedure TfrmObrabotkaRN.aNovvExecute(Sender: TObject);
begin
  if (dm.tblRabotenNalog.State in [dsBrowse,dsInactive])
   and (dm.tblRNStavka.State in [dsBrowse, dsInactive]) then
   begin
     cxPageControl1.ActivePageIndex := 1;
     aNov.Execute;
   end;
end;

//	����� �� ���������. �� ��������� ������� �� ������, �� �������� ������� �� ���� ��������,
//	�� ������� ������� �� ������ ������ � �� ������� �� Edit Mode
procedure TfrmObrabotkaRN.aAzurirajExecute(Sender: TObject);
begin
//if cxPageControl1.ActivePageIndex = 1 then
// begin
//  if(cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) then
//  begin
//    cxPageControl1.ActivePageIndex := 1;
//    dPanel.Enabled := true;
//    gbRabotenNalog.Enabled := true;
//    lPanel.Enabled:=False;
//    Panel1.Enabled:=false;
//    txtBroj.SetFocus;
//    cxGrid1DBTableView1.DataController.DataSet.Edit;
//  end
//  else ShowMessage('������ �������� �� ��� �������� �� �� ��������� �������/r/n � ����� ������� �� ������� ��� �����!');
// end
// else
// if cxPageControl1.ActivePageIndex = 2 then
// begin
//  if(dm.tblRNStavka.State = dsBrowse) then
//  begin
//    lPanelO.Enabled := true;
//    gbRNStavki.Enabled := true;
//    dPanelO.Enabled:=False;
//    txtVidArtikal.SetFocus;
//    dm.tblRNStavka.Edit;
//  end
//  else ShowMessage('������ �������� �� ��� �������� �� �� ��������� �������/r/n � ����� ������� �� ������� ��� �����!');
// end
end;

//	����� �� ������ �� ������������� �����
procedure TfrmObrabotkaRN.aAzurirajjExecute(Sender: TObject);
begin
 if cxPageControl1.ActivePageIndex = 2 then
 begin
  if(dm.tblRNStavka.State = dsBrowse) then
  begin
    PanelSurovini.Enabled := true;
    PanelSurovini.Enabled := true;
    cxGroupBox9.Enabled:=False;
    txtVidSurovina.SetFocus;
    dm.tblRNSurovini.Edit;
  end
  else ShowMessage('������ �������� �� ��� �������� �� �� ��������� �������/r/n � ����� ������� �� ������� ��� �����!');
 end
end;

procedure TfrmObrabotkaRN.aAzurirajSExecute(Sender: TObject);
begin
//  if (dm.tblRabotenNalog.State in [dsBrowse,dsInactive])
//   and (dm.tblRNStavka.State in [dsBrowse, dsInactive]) then
//   begin
//     cxPageControl1.ActivePageIndex :=2;
//     aAzuriraj.Execute;
//   end;
end;

procedure TfrmObrabotkaRN.aBrisiExecute(Sender: TObject);
begin
if cxPageControl1.ActivePageIndex = 0 then
 begin
 if ((cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) and
     (cxGrid1DBTableView1.DataController.RecordCount <> 0)) then
    cxGrid1DBTableView1.DataController.DataSet.Delete();
 end
 else
 if cxPageControl1.ActivePageIndex = 2 then
 begin
   if ((dm.tblRNStavka.State = dsBrowse) and
     (dm.tblRNStavka.RecordCount <> 0)) then
      dm.tblRNStavka.Delete();
 end;
end;

procedure TfrmObrabotkaRN.aBrisiiExecute(Sender: TObject);
begin
//  if (dm.tblRabotenNalog.State in [dsBrowse,dsInactive])
//   and (dm.tblRNStavka.State in [dsBrowse, dsInactive]) then
//   begin
//     cxPageControl1.ActivePageIndex :=0;
//     aBrisi.Execute;
//   end;
end;

procedure TfrmObrabotkaRN.aBrisiIzgledExecute(Sender: TObject);
begin
  brisiGridVoBaza(Name,cxGrid1DBTableView1);
  BrisiFormaIzgled(self);
end;

//	����� �� ���������� �� ����������
procedure TfrmObrabotkaRN.aRefreshExecute(Sender: TObject);
begin
  cxGrid1DBTableView1.DataController.Filter.Root.Clear; //���������� �� ��������
  cxGrid1DBTableView1.DataController.DataSet.Refresh;
end;

//	����� �� ����� �� ������� �� ������� �� ������� �����
procedure TfrmObrabotkaRN.aIzlezExecute(Sender: TObject);
begin
  Close;
end;

procedure TfrmObrabotkaRN.aListaRNExecute(Sender: TObject);
begin
//    if dm.tblRabotenNalog.State <> dsBrowse then
//       begin
//          dm.tblRabotenNalog.Cancel;
//          dPanel.Enabled := false;
//       end;
//          tsListaRN.TabVisible := true;
//          cxPageControl1.ActivePage := tsListaRN;
end;

//	����� �� ������� �� �������� �� ������ �� ���� (Utils.pas)
procedure TfrmObrabotkaRN.aSiteRNExecute(Sender: TObject);
begin
  dm.tblRabotenNalog.Close;
  dm.tblRabotenNalog.ParamByName('status').AsString := '%';
  dm.tblRabotenNalog.Open;

end;

procedure TfrmObrabotkaRN.aSmeniStatusExecute(Sender: TObject);
begin
  if dm.tblRNSurovini.State = dsBrowse then
  begin
    aPromeniStatus.Execute;
  end;
end;

procedure TfrmObrabotkaRN.aSnimiIzgledExecute(Sender: TObject);
begin
  zacuvajGridVoBaza(Name,cxGrid1DBTableView1);
  ZacuvajFormaIzgled(self);
end;

//	����� �� ������� �� ������ �� Excel ������ (Utils.pas)
procedure TfrmObrabotkaRN.aZacuvajExcelExecute(Sender: TObject);
begin
  zacuvajVoExcel(cxGrid1, Caption);
end;

//  ��������� �� ������ �� ������� �� ���������� �� ����� �� Enter
procedure TfrmObrabotkaRN.EnterKakoTab(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
    case Key of
        VK_DOWN:
        begin
          PostMessage(Handle,WM_NextDlgCtl,0,0);
          key:=0;
        end;
        VK_UP:
        begin
          PostMessage(Handle,WM_NextDlgCtl,1,0);
          key:=0;
        end;
        VK_RETURN:
        begin
          PostMessage(Handle,WM_NextDlgCtl,0,0);
        end;
    end;
end;

//	������� �� ��� �� ������ ��� �������� �� �����
procedure TfrmObrabotkaRN.cxDBTextEditAllEnter(Sender: TObject);
begin
    TEdit(Sender).Color:=clSkyBlue;
end;

//	������� �� ��� �� ������ ��� ����� �� �����
procedure TfrmObrabotkaRN.cxDBTextEditAllExit(Sender: TObject);
begin
    TEdit(Sender).Color:=clWhite;
      if (dm.tblRNSurovini.State) in [dsInsert, dsEdit] then
    begin
   if ((Sender as TWinControl)= cbSurovina) then
        begin
            if (cbSurovina.Text <>'') then
            begin
              txtVidSurovina.Text := dm.tblArtikalRNARTVID.AsString;
              txtSurovina.Text := dm.tblArtikalRNID.AsString;
              //dm.tblNormativO.Close;
              //dm.tblNormativO.ParamByName('mas_id').va
            end
            else
            begin
              dm.tblRNSuroviniVID_SUROVINA.Clear;
              dm.tblRNSuroviniSUROVINA.Clear;
            end;
         end;
         if ((Sender as TWinControl)= txtSurovina) or ((Sender as TWinControl)= txtVidSurovina) then
         begin
             SetirajLukap(sender,dm.tblArtikalRN,txtVidSurovina,txtSurovina, cbSurovina);
         end;
    end;
end;

procedure TfrmObrabotkaRN.cxGrid1DBTableView1DataControllerDetailCollapsed(
  ADataController: TcxCustomDataController; ARecordIndex: Integer);
begin
  ADataController.ClearDetailLinkObject(ARecordIndex, 0);
  dm.tblRNStavka.Open;
end;

procedure TfrmObrabotkaRN.cxGrid1DBTableView1DataControllerDetailExpanded(
  ADataController: TcxCustomDataController; ARecordIndex: Integer);
begin
   ADataController.FocusedRecordIndex := ARecordIndex;
end;

procedure TfrmObrabotkaRN.cxGrid1DBTableView1DataControllerDetailExpanding(
  ADataController: TcxCustomDataController; ARecordIndex: Integer;
  var AAllow: Boolean);
begin
    ADataController.CollapseDetails;
end;

procedure TfrmObrabotkaRN.cxGrid1DBTableView1DblClick(Sender: TObject);
begin
   aPogledRN.Execute;
end;

procedure TfrmObrabotkaRN.cxGrid1DBTableView1FilterCustomization(
  Sender: TcxCustomGridTableView; var ADone: Boolean);
begin
end;

//  ��������� �� ������ �� ��������� �� ���������� ���� �� ��������
//  ��������� ��: br - ����� ��� �� ������ (�� 0 �� 9) �� ��������� �� ����� �� ����� �� ���������� ����
//  s - ��������� �� ������ �� ������ (���������� �� string �� ����� �� integer �� IntToStr ���������)
procedure TfrmObrabotkaRN.SetSifra(br: integer; s : string);
begin
  _sifra_kluc[br] := s;
end;

procedure TfrmObrabotkaRN.XPColorMap1ColorChange(Sender: TObject);
begin

end;

//  ������� �� ������ �� ��������� �� ���������� ���� �� ��������
//  ��������� ��: br - ����� ��� �� ������ (�� 0 �� 9) �� ��������� �� ����� �� ����� �� ���������� ����
function TfrmObrabotkaRN.GetSifra(br: integer) : string;
begin
  Result := _sifra_kluc[br];
end;

procedure TfrmObrabotkaRN.OtvoriOperacija;
begin
    dm.pRNOperacija.Close;
    dm.pRNOperacija.ParamByName('ID_RN').AsString := dm.tblRNStavkaID_RABOTEN_NALOG.AsString;
    dm.pRNOperacija.ParamByName('ID').AsString := dm.tblRNStavkaID.AsString;
    dm.pRNOperacija.Open;
end;

procedure TfrmObrabotkaRN.OtvoriNormativ;
begin
  dm.pRNNormativ.Close;
  dm.pRNNormativ.ParamByName('ID_RN').AsString := dm.tblRNStavkaID_RABOTEN_NALOG.AsString;
  dm.pRNNormativ.ParamByName('ID').AsString := dm.tblRNStavkaID.AsString;
  dm.pRNNormativ.Open;
end;

procedure TfrmObrabotkaRN.OtvoreniRN;
begin
  dm.tblRabotenNalog.Close;
  dm.tblRabotenNalog.ParamByName('status').AsString := '1';
  dm.tblRabotenNalog.Open;
end;

procedure TfrmObrabotkaRN.PartnerPropertiesChange(Sender: TObject);
begin

end;

//  �� ������� �� ������� ��� ����������� �� Master-�� �� �������� �� ��������� �� ������ ����� SetSifra
procedure TfrmObrabotkaRN.prefrli;
begin
end;

procedure TfrmObrabotkaRN.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    //	��������� ��� ��������� �� �������
//    if (cxGrid1DBTableView1.DataController.DataSource.State = dsEdit) or (cxGrid1DBTableView1.DataController.DataSource.State = dsInsert) then
//    begin
//        frmDaNe := TfrmDaNe.Create(self, '���������� ��������', '���������� �� �� ��������. ���� ������ �� �� �������?', 1);
//        if (frmDaNe.ShowModal <> mrYes) then
//        begin
//            cxGrid1DBTableView1.DataController.DataSet.Cancel;
//            Action := caFree;
//        end
//        else
//          if (Validacija(dPanel) = false) then
//          begin
//            cxGrid1DBTableView1.DataController.DataSet.Post;
//            Action := caFree;
//          end
//          else Action := caNone;
//    end;
end;
procedure TfrmObrabotkaRN.FormCreate(Sender: TObject);
begin
  ProcitajFormaIzgled(self);
  if (dmRes.use_skin = true) then dxRibbon1.ColorSchemeName := dmRes.skin_name;
  OtvoreniRN;
  dm.tblRNStavka.Open;
  dm.pRNNormativ.Open;
  dm.tblRNSurovini.Open;
 // cxPageControl1.HideTabs:=True;
 // tsListaRN.TabVisible:=True;
end;

//------------------------------------------------------------------------------

procedure TfrmObrabotkaRN.FormShow(Sender: TObject);
begin
  //  ������� �� ������������ ������������/���������� �� ���������� �� ���� �����
    SpremiForma(self);
  //	�� ��������� �� ������� ������ ��� ���������� � ���� � ��� �������� �� �������
 //  PrvPosledenTab(dPanel,posledna,prva);
    //dxBarManager1Bar1.Caption := Caption;
    dxComponentPrinter1Link1.ReportTitle.Text := Caption;

  //	������� �� ������������ �� ������ �� ������
    procitajGridOdBaza(Name,cxGrid1DBTableView1,false,false);
  //	������� �� ������������ �� ��������� �� ������
    procitajPrintOdBaza(Name,cxGrid1DBTableView1.Name, dxComponentPrinter1Link1);
     CustomFields();
    cxPageControl1.ActivePageIndex := 0;
     // cxDBButtonEdit1.Properties.Buttons[0].Caption := dm.tblRabotenNalogNAZIV_STATUS_STAVKI.AsString;
    cxDBButtonEdit2.Properties.Buttons[0].Caption := dm.tblRabotenNalogNAZIV_STATUS_STAVKI_SLEDEN.AsString;
    cxGrid1DBTableView1.Controller.gotofirst;
    //cxGrid1DBTableView1.DataController.foSetFocus;
      //cxDBButtonEdit1.Style.TextStyle[cxDBButtonEdit1.Properties.Buttons[0].Caption].fsBold;
   //end

end;
//------------------------------------------------------------------------------

procedure TfrmObrabotkaRN.SaveToIniFileExecute(Sender: TObject);
begin
//    cxGrid1DBTableView1.StoreToIniFile(Name,True,[]);
end;
//------------------------------------------------------------------------------

procedure TfrmObrabotkaRN.cxGrid1DBTableView1KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
 if key = VK_RETURN then
    aPogledRN.Execute;
end;

procedure TfrmObrabotkaRN.cxGrid1DBTableView1KeyPress(Sender: TObject;
  var Key: Char);
begin
  if(Ord(Key) <> VK_RETURN) then cxSortiraj(cxGrid1DBTableView1);
end;

procedure TfrmObrabotkaRN.cxGrid3DBTableView1FocusedRecordChanged(
  Sender: TcxCustomGridTableView; APrevFocusedRecord,
  AFocusedRecord: TcxCustomGridRecord; ANewItemRecordFocusingChanged: Boolean);
begin
  // SetirajLukap(sender,dm.tblArtikalRN,txtVidArtikal,txtArtikal, cbNazivArtikal);
 //  cxDBButtonEdit2.Properties.Buttons[0].Caption := dm.tblRabotenNalogNAZIV_STATUS_STAVKI_SLEDEN.AsString;
end;

procedure TfrmObrabotkaRN.cxGrid3FocusedViewChanged(
  Sender: TcxCustomGrid; APrevFocusedView, AFocusedView: TcxCustomGridView);
begin
//   if AFocusedView = cxGrid3DBTableView2 then
//    begin
//      OtvoriNormativ;
//    end
//    else
//    if AFocusedView = cxGrid3DBTableView3 then
//    begin
//      OtvoriOperacija;
//    end;
end;

procedure TfrmObrabotkaRN.cxGrid3LayoutChanged(Sender: TcxCustomGrid;
  AGridView: TcxCustomGridView);
begin
//    if cxGrid3Level2.Active then
//    begin
//      OtvoriNormativ;
//    end;
end;

procedure TfrmObrabotkaRN.cxGridDBTableView4FocusedRecordChanged(
  Sender: TcxCustomGridTableView; APrevFocusedRecord,
  AFocusedRecord: TcxCustomGridRecord; ANewItemRecordFocusingChanged: Boolean);
begin
     SetirajLukap(sender,dm.tblArtikalRN,txtVidSurovina,txtSurovina, cbSurovina);
end;

procedure TfrmObrabotkaRN.cxPageControl1Change(Sender: TObject);
begin
//  if ((dm.tblRNStavka.State in [dsInsert,dsEdit])
//  and (cxPageControl1.ActivePageIndex in [0,1,3])) or
//  ((dm.tblRabotenNalog.State in [dsInsert,dsEdit])
//  and (cxPageControl1.ActivePageIndex in [0,2,3])) then
//     aOtkazi.Execute;
   if cxPageControl1.ActivePageIndex = 2 then
   begin
     dm.pRNNormativ.Close;
     dm.pRNNormativ.ParamByName('ID_RN').AsString := dm.tblRNStavkaID_RABOTEN_NALOG.AsString;
     dm.pRNNormativ.ParamByName('ID').AsString := '%';
     dm.pRNNormativ.Open;

     dm.pRNOperacija.Close;
     dm.pRNOperacija.ParamByName('ID_RN').AsString := dm.tblRNStavkaID_RABOTEN_NALOG.AsString;
     dm.pRNOperacija.ParamByName('ID').AsString := dm.tblRNStavkaID.AsString;
     dm.pRNOperacija.Open;

     dm.tblRNSurovini.Open;
      if dm.tblRabotenNalogSTATUS_STAVKI.Value >= 3 then
       begin
         PanelSurovini.Visible := true;
         cxGroupBox9.Visible := true;
         cxDBButtonEdit1.Properties.Buttons[0].Caption := dm.tblRabotenNalogNAZIV_STATUS_STAVKI_SLEDEN.AsString;
         SetirajLukap(sender,dm.tblArtikalRN,txtVidSurovina,txtSurovina, cbSurovina);
       end;
   end
   else
   if cxPageControl1.ActivePageIndex = 4 then
   begin
     dm.pRNOperacija.Close;
     dm.pRNOperacija.ParamByName('ID_RN').AsString := dm.tblRNStavkaID_RABOTEN_NALOG.AsString;
     dm.pRNOperacija.ParamByName('ID').AsString := dm.tblRNStavkaID.AsString;
     dm.pRNOperacija.Open;
   end
   else
   if cxPageControl1.ActivePageIndex = 0 then
   begin
       aOtvoreniRN.Enabled := true;
       aSiteRN.Enabled := true;
   end
   else
//   if cxPageControl1.ActivePageIndex = 1 then
//   begin
//      cxDBButtonEdit1.Properties.Buttons[0].Caption := dm.tblRabotenNalogNAZIV_STATUS_STAVKI.AsString;
//      cxDBButtonEdit2.Properties.Buttons[0].Caption := dm.tblRabotenNalogNAZIV_STATUS_STAVKI_SLEDEN.AsString;
//      //cxDBButtonEdit1.Style.TextStyle[cxDBButtonEdit1.Properties.Buttons[0].Caption].fsBold;
//   end
//   else
   if cxPageControl1.ActivePageIndex <> 0 then
   begin
       aOtvoreniRN.Enabled := False;
       aSiteRN.Enabled := False;
   end

end;

procedure TfrmObrabotkaRN.gbRabotenNalogMeasureCaptionHeight(
  Sender: TcxCustomGroupBox; const APainter: TcxCustomLookAndFeelPainter;
  var ACaptionHeight: Integer);
begin

end;

//  ����� �� �����
procedure TfrmObrabotkaRN.aZapisiExecute(Sender: TObject);
var
  st: TDataSetState;
begin
//  ZapisiButton.SetFocus;
//if cxPageControl1.ActivePageIndex = 1 then
// begin
//     st := cxGrid1DBTableView1.DataController.DataSet.State;
//      if st in [dsEdit,dsInsert] then
//      begin
//        if (Validacija(dPanel) = false) then
//        begin
//          if ((st = dsInsert) and inserting) then
//          begin
//            dm.tblRabotenNalogSTATUS.AsInteger := 1;
//            cxGrid1DBTableView1.DataController.DataSet.Post;
//            aNova.Execute;
//          end;
//
//          if ((st = dsInsert) and (not inserting)) then
//          begin
//            cxGrid1DBTableView1.DataController.DataSet.Post;
//            dPanel.Enabled:=false;
//            lPanel.Enabled:=true;
//            Panel1.Enabled:=true;
//            //cxPageControl1.ActivePageIndex := 0;
//            cxGrid4.SetFocus;
//          end;
//
//          if (st = dsEdit) then
//          begin
//            cxGrid1DBTableView1.DataController.DataSet.Post;
//            dPanel.Enabled:=false;
//            lPanel.Enabled:=true;
//            Panel1.Enabled:=true;
//           // cxPageControl1.ActivePageIndex := 0;
//            cxGrid4.SetFocus;
//          end;
//        end;
//      end;
// end
// else
 if cxPageControl1.ActivePageIndex = 2 then
 begin
     st := dm.tblRNSurovini.State;
      if st in [dsEdit,dsInsert] then
      begin
        if (Validacija(PanelSurovini) = false) then
        begin
          if ((st = dsInsert) and inserting) then
          begin
            //dm.tblRabotenNalogSTATUS.AsInteger := 1;
            dm.tblRNSurovini.Post;
            aNova.Execute;
          end;

          if ((st = dsInsert) and (not inserting)) then
          begin
            dm.tblRNSurovini.Post;
            PanelSurovini.Enabled:=false;
            cxGroupBox9.Enabled:=true;
            cxGrid7.SetFocus;
          end;

          if (st = dsEdit) then
          begin
            dm.tblRNSurovini.Post;
            PanelSurovini.Enabled:=false;
            cxGroupBox9.Enabled:=true;
            cxGrid7.SetFocus;
          end;
        end;
      end;
 end;
end;

procedure TfrmObrabotkaRN.Button1Click(Sender: TObject);
begin
  OpenPictureDialog1.Filter := 'JPEG Image File |*.jpg;*.jpeg';
  OpenPictureDialog1.Execute();
  if OpenPictureDialog1.FileName <> '' then
    // dm.tblRNStavkaSLIKA.LoadFromFile(OpenPictureDialog1.FileName);
end;

//	����� �� ���������� �� �������
procedure TfrmObrabotkaRN.aOtkaziExecute(Sender: TObject);
begin
if cxPageControl1.ActivePageIndex in [0,1] then
  begin
    if (cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) then
        begin
            ModalResult := mrCancel;
            Close();
        end
        else
        begin
            cxGrid1DBTableView1.DataController.DataSet.Cancel;
          //  RestoreControls(dPanel);
          //  dPanel.Enabled := false;
            lPanel.Enabled := true;
          //  Panel1.Enabled:=true;
            //cxPageControl1.ActivePageIndex := 0;
          //  cxGrid4.SetFocus;
        end;
  end
  else
  //begin
   if cxPageControl1.ActivePageIndex = 2 then
     begin
        if (dm.tblRNSurovini.State = dsBrowse) then
        begin
            ModalResult := mrCancel;
            Close();
        end
        else
        begin
            dm.tblRNSurovini.Cancel;
            RestoreControls(PanelSurovini);
            PanelSurovini.Enabled:=false;
            cxGroupBox9.Enabled:=true;
            cxGrid7.SetFocus;
        end;
     end
     else
       //begin
   if cxPageControl1.ActivePageIndex = 3 then
     begin
            ModalResult := mrCancel;
            Close();
        end
 //end;
end;

procedure TfrmObrabotkaRN.aOtvoreniRNExecute(Sender: TObject);
begin
   OtvoreniRN;
end;

//----------------------------------------------------------------------------------
// ��������� �� ��������, ���������� �� ��������� � �������� �� Print ��������
//----------------------------------------------------------------------------------

//	����� �� �������� �� �������� �� �������
procedure TfrmObrabotkaRN.aPageSetupExecute(Sender: TObject);
begin
  dxComponentPrinter1Link1.PageSetup;
end;

//	����� �� ������� �� ������
procedure TfrmObrabotkaRN.aPecatiTabelaExecute(Sender: TObject);
begin
  dxComponentPrinter1Link1.ReportTitle.Text := Caption;

  dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Clear;
  dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Add(dmkon.firma_naziv);
  dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Add(DateTimeToStr(Now));

  dxComponentPrinter1Link1.PrinterPage.PageHeader.LeftTitle.Clear;
  //dxComponentPrinter1Link1.PrinterPage.PageHeader.LeftTitle.Add('������� : ' + TipSifra.Text + '-' + IdSifra.Text + ' ' + Partner.Text);

  dxComponentPrinter1.Preview(true, dxComponentPrinter1Link1);
end;

//	����� �� ���������� �� ��������� �� �������
procedure TfrmObrabotkaRN.aPodesuvanjePecatenjeExecute(Sender: TObject);
begin
  dxComponentPrinter1Link1.DesignReport();
end;

procedure TfrmObrabotkaRN.aPogledRNExecute(Sender: TObject);
begin
  cxPageControl1.ActivePageIndex := 2;
  //cxGrid4.SetFocus;
end;

procedure TfrmObrabotkaRN.aPromeniStatusExecute(Sender: TObject);
begin
if dm.tblRNSurovini.State = dsBrowse then
    begin
     case dm.tblRabotenNalogSTATUS_STAVKI.Value of
      1:
      begin
       dm.tblRNStavka.First;
       while not dm.tblRNStavka.Eof do
         begin
           dm.tblRNStavka.Edit;
           dm.tblRNStavkaSTATUS.Value := 2;
           dm.tblRNStavka.Post;
           dm.tblRNStavka.Next;
         end;
      end;
      2:
      begin

         // �������� � �� �� ����� �������� �� ������� �� ��������
         //�� �� ������� ��������� -- ��������!!!
         //������ �� MAN_RN_SUROVINA
         //�� ��������� �� �� ������� �������� �� ���������� �� ��������� �� �������� � ����
         //�������� �� ����� �� �����������
         //�� �� ������� �������� �� ����������, ��� � �� ���������� (������� ��� �������)
         //�� �� ������� ���� ���������, ��� �� �� ����������

          dm.pNormativSurovina.Close;
          dm.pNormativSurovina.ParamByName('id_raboten_nalog').Asinteger := dm.tblRabotenNalogID.AsInteger;
          dm.pNormativSurovina.ParamByName('id_re').Asinteger := dm.tblRabotenNalogID_RE_SUROVINI.AsInteger;
          dm.pNormativSurovina.ExecProc;

          dm.tblRabotenNalog.RecNo;
          dm.tblRabotenNalog.Refresh;
          // CloseOpen(true);
          dm.tblRNStavka.RecNo;
          dm.tblRNStavka.Refresh;
          dm.tblRNSurovini.RecNo;
          dm.tblRNSurovini.Refresh;

          PanelSurovini.Visible := true;
          cxGroupBox9.Visible := true;
      end;
      3:
      begin

      end;
     end;
//      dm.tblRNStavka.Edit;
//      dm.tblRNStavkaSTATUS.Value:=dm.tblRabotenNalogSTATUS_STAVKI.Value+1;
//      dm.tblRNStavka.Post;
    end;
end;

procedure TfrmObrabotkaRN.aSnimiPecatenjeExecute(Sender: TObject);
begin
  zacuvajPrintVoBaza(Name,cxGrid1DBTableView1.Name,dxComponentPrinter1Link1);
end;

//	����� �� ������ �� ����������� �������� �� ������� (Utils.pas)
procedure TfrmObrabotkaRN.aBrisiPodesuvanjePecatenjeExecute(Sender: TObject);
begin
  brisiPrintOdBaza(Name,cxGrid1DBTableView1.Name, dxComponentPrinter1Link1);
end;

procedure TfrmObrabotkaRN.aBrisiSExecute(Sender: TObject);
begin
if (dm.tblRNSurovini.State in [dsBrowse, dsInactive]) then
//   begin
   if ((dm.tblRNSurovini.State = dsBrowse) and
     (dm.tblRNSurovini.RecordCount <> 0)) then
      dm.tblRNSurovini.Delete();
//   end;
end;

procedure TfrmObrabotkaRN.ActionList1Execute(Action: TBasicAction;
  var Handled: Boolean);
begin
end;

//  ����� �� ����� �� ����� �� ������������� �� �������� � ��������������� ��
//  �������(������ ���������� ��������)
procedure TfrmObrabotkaRN.aFormConfigExecute(Sender: TObject);
begin
  frmFormConfig := TfrmFormConfig.Create(Application);
  frmFormConfig.formPtr := Addr(Self);
  frmFormConfig.ShowModal;
  frmFormConfig.Free;
end;

//----------------------------------------------------------------------------------
// ���� -> ��������� �� ��������, ���������� �� ��������� � �������� �� Print ��������
//----------------------------------------------------------------------------------

// ��������� �� ���������� �� ��������� � �������� �� Print �������� �� .ini ��������
procedure TfrmObrabotkaRN.zacuvajPrintVoIni(ime:AnsiString);
var
  AStream: TFileStream;
begin
  AStream := TFileStream.Create(dmKon.RabotenDir + ime + 'Print.ini', fmCreate);
  try
    AStream.WriteComponent(dxComponentPrinter1Link1);
  finally
    AStream.Free;
  end;

end;

// ��������� �� ������ �� ��������� � �������� �� Print �������� �������� �� .ini ��������
procedure TfrmObrabotkaRN.procitajPrintOdIni(ime:AnsiString);
var
  AStream: TFileStream;
begin
  AStream := TFileStream.Create(dmKon.RabotenDir + ime + 'Print.ini', fmOpenRead);
  try
    dxComponentPrinter1Link1.RestoreDefaults;
    AStream.ReadComponent(dxComponentPrinter1Link1);
  finally
    AStream.Free;
  end;
end;

procedure TfrmObrabotkaRN.SetirajLukap(sender:TObject; tabela:TDataSet; tip:TcxDBTextEdit; sifra:TcxDBTextEdit; lukap:TcxLookupComboBox);
begin
         if (tip.Text <>'') and (sifra.Text<>'')  then
         begin
          if(tabela.Locate('ARTVID;ID',VarArrayOf([tip.text,sifra.text]),[])) then
               lukap.Text:=tabela.FieldByName('NAZIV').Value;
         end
         else
         begin
            lukap.Clear;
         end;
end;

procedure TfrmObrabotkaRN.CustomFields();
begin
//  custom1 := getCustomField('MAN_RABOTEN_NALOG_STAVKA','CUSTOM1');
//  custom2 := getCustomField('MAN_RABOTEN_NALOG_STAVKA','CUSTOM2');
//  custom3 := getCustomField('MAN_RABOTEN_NALOG_STAVKA','CUSTOM3');
//
//  if custom1 <> '' then
//  begin
//    cxGrid3DBTableView1CUSTOM1.Visible := true;
//    cxGrid3DBTableView1CUSTOM1.VisibleForCustomization := true;
//    cxGrid3DBTableView1CUSTOM1.Caption := custom1;
//
//    lblCustom1.Visible := true;
//    lblCustom1.Caption := custom1 + ' :';
//    txtCustom1.Visible := true;
//  end;
//  if custom2 <> '' then
//  begin
//    cxGrid3DBTableView1CUSTOM2.Visible := true;
//    cxGrid3DBTableView1CUSTOM2.VisibleForCustomization := true;
//    cxGrid3DBTableView1CUSTOM2.Caption := custom2;
//
//    lblCustom2.Visible := true;
//    lblCustom2.Caption := custom2 + ' :';
//    txtCustom2.Visible := true;
//  end;
//  if custom3 <> '' then
//  begin
//    cxGrid3DBTableView1CUSTOM3.Visible := true;
//    cxGrid3DBTableView1CUSTOM3.VisibleForCustomization := true;
//    cxGrid3DBTableView1CUSTOM3.Caption := custom3;
//
//    lblCustom3.Visible := true;
//    lblCustom3.Caption := custom3 + ' :';
//    txtCustom3.Visible := true;
//  end;
end;

procedure TfrmObrabotkaRN.StatusBoi;
begin
 // if dm.tblRabotenNalogSTATUS.Value then

end;
end.
