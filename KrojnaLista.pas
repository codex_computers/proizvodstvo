unit KrojnaLista;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData, cxEdit,
  DB, cxDBData, cxGridCustomPopupMenu, cxGridPopupMenu,
  ComCtrls, cxContainer, cxTextEdit, cxDBEdit, StdCtrls, cxGridLevel,
  cxClasses, cxControls, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGrid, ExtCtrls, Menus, ActnList,
  cxGridExportLink, cxExport, cxLookAndFeelPainters, cxDataStorage,
  cxButtons, dxStatusBar, dxRibbonStatusBar, dxRibbon, dxBar, dxBarExtItems,
  dxPSGlbl, dxPSUtl, dxPSEngn, dxPrnPg, dxBkgnd, dxWrap, dxPrnDev,
  dxPSCompsProvider, dxPSFillPatterns, dxPSEdgePatterns, dxPSCore, dxPScxCommon,
  cxPC, cxMemo, cxLocalization, cxLookAndFeels,
  dxPSPDFExportCore, dxPSPDFExport, cxDrawTextUtils, dxPSPrVwStd, dxPSPrVwAdv,
  dxPSPrVwRibbon, dxPScxEditorProducers, dxPScxExtEditorProducers,
  dxPScxPageControlProducer, dxPgsDlg, cxCheckBox, cxBarEditItem, dxSkinsCore,
  dxSkinBlack, dxSkinBlue, dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom,
  dxSkinDarkSide, dxSkinFoggy, dxSkinGlassOceans, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven,
  dxSkinSharp, dxSkinSilver, dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008,
  dxSkinsDefaultPainters, dxSkinValentine, dxSkinXmas2008Blue,
  dxSkinsdxRibbonPainter, dxSkinsdxBarPainter, dxSkinscxPCPainter,
  dxBarSkinnedCustForm, cxDropDownEdit, cxCalendar, cxDBLookupComboBox,
  cxMaskEdit, cxLookupEdit, cxDBLookupEdit, cxDBExtLookupComboBox, cxGroupBox,
  cxRadioGroup, dxRibbonSkins, dxPScxGridLnk, dxPScxGridLayoutViewLnk,
  dxScreenTip, dxSkinBlueprint, dxSkinDevExpressDarkStyle,
  dxSkinDevExpressStyle, dxSkinHighContrast, dxSkinSevenClassic,
  dxSkinSharpPlus, dxSkinTheAsphaltWorld, dxSkinVS2010, dxSkinWhiteprint, dmResources,
  dxSkinMetropolis, dxSkinMetropolisDark, dxSkinOffice2013DarkGray,
  dxSkinOffice2013LightGray, dxSkinOffice2013White, dxSkinOffice2016Colorful,
  dxSkinOffice2016Dark, dxSkinVisualStudio2013Blue, dxSkinVisualStudio2013Dark,
  dxSkinVisualStudio2013Light, dxRibbonCustomizationForm, System.Actions,
  cxNavigator, cxLabel, FIBDataSet, pFIBDataSet, dxSkinBasic,
  dxSkinOffice2019Black, dxSkinOffice2019Colorful, dxSkinOffice2019DarkGray,
  dxSkinOffice2019White, dxSkinTheBezier, dxCore, dxDateRanges,
  dxScrollbarAnnotations, dxBarBuiltInMenu, cxDBLabel;

type
//  niza = Array[1..5] of Variant;

  TfrmKrojnaLista = class(TForm)
    cxGridPopupMenu1: TcxGridPopupMenu;
    PopupMenu1: TPopupMenu;
    N1: TMenuItem;
    Excel1: TMenuItem;
    dxRibbon1Tab1: TdxRibbonTab;
    dxRibbon1: TdxRibbon;
    StatusBar1: TdxRibbonStatusBar;
    bmElement: TdxBarManager;
    dxBarManager1Bar1: TdxBar;
    dxBarManager1Bar2: TdxBar;
    dxBarManager1Bar3: TdxBar;
    dxBarLargeButton2: TdxBarLargeButton;
    dxBarLargeButton3: TdxBarLargeButton;
    dxBarLargeButton4: TdxBarLargeButton;
    dxBarLargeButton5: TdxBarLargeButton;
    dxBarLargeButton6: TdxBarLargeButton;
    dxBarLargeButton7: TdxBarLargeButton;
    dxBarLargeButton8: TdxBarLargeButton;
    dxBarLargeButton1: TdxBarLargeButton;
    ActionList1: TActionList;
    aNov: TAction;
    aAzuriraj: TAction;
    aBrisi: TAction;
    aSnimiIzgled: TAction;
    aZacuvajExcel: TAction;
    aPecatiTabela: TAction;
    aHelp: TAction;
    aIzlez: TAction;
    aZapisi: TAction;
    aOtkazi: TAction;
    aRefresh: TAction;
    dxRibbon1Tab2: TdxRibbonTab;
    dxBarManager1Bar4: TdxBar;
    dxBarLargeButton9: TdxBarLargeButton;
    aSnimiPecatenje: TAction;
    dxComponentPrinter1: TdxComponentPrinter;
    dxComponentPrinter1Link1: TdxGridReportLink;
    dxBarLargeButton11: TdxBarLargeButton;
    ribbonFilterReset: TdxBarLargeButton;
    aPodesuvanjePecatenje: TAction;
    dxBarLargeButton12: TdxBarLargeButton;
    aBrisiPodesuvanjePecatenje: TAction;
    dxBarLargeButton13: TdxBarLargeButton;
    dxBarLargeButton14: TdxBarLargeButton;
    aPageSetup: TAction;
    dxBarLargeButton15: TdxBarLargeButton;
    dxBarLargeButton16: TdxBarLargeButton;
    dxBarLargeButton17: TdxBarLargeButton;
    cxBarEditItem1: TcxBarEditItem;
    dxBarSubItem1: TdxBarSubItem;
    aFormConfig: TAction;
    dxBarScreenTipRepository1: TdxBarScreenTipRepository;
    tipSnimiIzgled: TdxBarScreenTip;
    tipZacuvajExcel: TdxBarScreenTip;
    tipDizajnReport: TdxBarScreenTip;
    tipSetiranjeStrana: TdxBarScreenTip;
    tipSnimiKonf: TdxBarScreenTip;
    tipBrisiKonf: TdxBarScreenTip;
    dxBarManager1BarIzgledGrid: TdxBar;
    dxBarLBtnSnimiIzgled: TdxBarLargeButton;
    dxBarLBtnBrisiIzgled: TdxBarLargeButton;
    aBrisiIzgled: TAction;
    aSpustiSoberi: TAction;
    dxBarLargeButton10: TdxBarLargeButton;
    tblArtikalElement: TpFIBDataSet;
    dsArtikalElement: TDataSource;
    tblArtikalElementARTVID: TFIBIntegerField;
    tblArtikalElementID: TFIBIntegerField;
    tblArtikalElementNAZIV: TFIBStringField;
    tblArtikalElementMERKA: TFIBStringField;
    tblArtikalElementMERKA_KOLICINA: TFIBStringField;
    dsArtikalSurovina: TDataSource;
    tblArtikalSurovina: TpFIBDataSet;
    FIBIntegerField1: TFIBIntegerField;
    FIBIntegerField2: TFIBIntegerField;
    FIBStringField1: TFIBStringField;
    FIBStringField2: TFIBStringField;
    FIBStringField3: TFIBStringField;
    cxPageControl1: TcxPageControl;
    tsGotovProizvod: TcxTabSheet;
    tsElement: TcxTabSheet;
    pnl1: TPanel;
    cxGrid2DBTableView1: TcxGridDBTableView;
    cxGrid2Level1: TcxGridLevel;
    cxGrid2: TcxGrid;
    cxGrid2DBTableView1ARTVID: TcxGridDBColumn;
    cxGrid2DBTableView1ID: TcxGridDBColumn;
    cxGrid2DBTableView1NAZIV: TcxGridDBColumn;
    cxGrid2DBTableView1MERKA: TcxGridDBColumn;
    cxGrid2DBTableView1MERKA_KOLICINA: TcxGridDBColumn;
    cxGrid2DBTableView1ID_KL: TcxGridDBColumn;
    lPanel: TPanel;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1DBTableView1ID: TcxGridDBColumn;
    cxGrid1DBTableView1VID_ARTIKAL: TcxGridDBColumn;
    cxGrid1DBTableView1ARTIKAL: TcxGridDBColumn;
    cxGrid1DBTableView1ARTIKAL_NAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1VID_ELEMENT: TcxGridDBColumn;
    cxGrid1DBTableView1ELEMENT: TcxGridDBColumn;
    cxGrid1DBTableView1ELEMENT_NAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1DOLZINA: TcxGridDBColumn;
    cxGrid1DBTableView1SIRINA: TcxGridDBColumn;
    cxGrid1DBTableView1VISINA: TcxGridDBColumn;
    cxGrid1DBTableView1OPERACIJA: TcxGridDBColumn;
    cxGrid1DBTableView1KOLICINA: TcxGridDBColumn;
    cxGrid1DBTableView1TS_INS: TcxGridDBColumn;
    cxGrid1DBTableView1TS_UPD: TcxGridDBColumn;
    cxGrid1DBTableView1USR_INS: TcxGridDBColumn;
    cxGrid1DBTableView1USR_UPD: TcxGridDBColumn;
    cxGrid1Level1: TcxGridLevel;
    dPanel: TPanel;
    Label8: TLabel;
    cxLabel8: TcxLabel;
    txtVidArtikal: TcxDBTextEdit;
    txtArtikal: TcxDBTextEdit;
    cbNazivArtikal: TcxExtLookupComboBox;
    cxLabel7: TcxLabel;
    txtVidElement: TcxDBTextEdit;
    txtElement: TcxDBTextEdit;
    cbElement: TcxExtLookupComboBox;
    cxGroupBox1: TcxGroupBox;
    cxLabel1: TcxLabel;
    txtDolzina: TcxDBTextEdit;
    cxLabel2: TcxLabel;
    txtSirina: TcxDBTextEdit;
    cxLabel3: TcxLabel;
    txtVisina: TcxDBTextEdit;
    txtZabeleska: TcxDBMemo;
    txtEdKol: TcxDBTextEdit;
    cxLabel17: TcxLabel;
    ZapisiButton: TcxButton;
    cxButton2: TcxButton;
    cxLabel4: TcxLabel;
    txtOperacija: TcxDBTextEdit;
    aElement: TAction;
    tsSurovini: TcxTabSheet;
    lPanelP: TPanel;
    cxGrid4: TcxGrid;
    cxGridDBTableView1: TcxGridDBTableView;
    cxGridLevel1: TcxGridLevel;
    dPanelP: TPanel;
    gbElementi: TcxGroupBox;
    Label1: TLabel;
    cxButton4: TcxButton;
    cxButton5: TcxButton;
    txtZabeleskaElement: TcxDBMemo;
    cxLabel33: TcxLabel;
    cxLabel38: TcxLabel;
    cxLabel35: TcxLabel;
    cxLabel5: TcxLabel;
    txtVidSurovina: TcxDBTextEdit;
    txtSurovina: TcxDBTextEdit;
    cbSurovina: TcxExtLookupComboBox;
    cxGroupBox2: TcxGroupBox;
    cxLabel6: TcxLabel;
    txtDolzinaGM: TcxDBTextEdit;
    cxLabel9: TcxLabel;
    txtSirinaGM: TcxDBTextEdit;
    cxLabel10: TcxLabel;
    txtVisinaGM: TcxDBTextEdit;
    bmElementBar1: TdxBar;
    aNovaSurovina: TAction;
    cxGridDBTableView1ID: TcxGridDBColumn;
    cxGridDBTableView1ID_KL: TcxGridDBColumn;
    cxGridDBTableView1VID_ELEMENT: TcxGridDBColumn;
    cxGridDBTableView1ELEMENT: TcxGridDBColumn;
    cxGridDBTableView1KOLICINA: TcxGridDBColumn;
    cxGridDBTableView1VISINA: TcxGridDBColumn;
    cxGridDBTableView1SIRINA: TcxGridDBColumn;
    cxGridDBTableView1DOLZINA: TcxGridDBColumn;
    cxGridDBTableView1NAZIV_SUROVINA: TcxGridDBColumn;
    cxGridDBTableView1NAZIV_ELEMENT: TcxGridDBColumn;
    cxGridDBTableView1ZABELESKA: TcxGridDBColumn;
    dxBarLargeButton18: TdxBarLargeButton;

    procedure cxDBTextEditAllEnter(Sender: TObject);
    procedure cxDBTextEditAllExit(Sender: TObject);
    procedure EnterKakoTab(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    //procedure PrvPosledenTab(panel:TPanel;var posledna,prva:TWinControl);
    procedure SaveToIniFileExecute(Sender: TObject);
    procedure cxGrid1DBTableView1KeyPress(Sender: TObject; var Key: Char);
    procedure aZapisiExecute(Sender: TObject);
    procedure aOtkaziExecute(Sender: TObject);
    procedure aNovExecute(Sender: TObject);
    procedure aAzurirajExecute(Sender: TObject);
    procedure aBrisiExecute(Sender: TObject);
    procedure aZacuvajExcelExecute(Sender: TObject);
    procedure aRefreshExecute(Sender: TObject);
    procedure aSnimiIzgledExecute(Sender: TObject);
    procedure aIzlezExecute(Sender: TObject);
    procedure aPecatiTabelaExecute(Sender: TObject);
    procedure aSnimiPecatenjeExecute(Sender: TObject);
    procedure zacuvajPrintVoIni(ime:AnsiString);
    procedure procitajPrintOdIni(ime:AnsiString);
    procedure aPodesuvanjePecatenjeExecute(Sender: TObject);
    procedure aBrisiPodesuvanjePecatenjeExecute(Sender: TObject);
    procedure aPageSetupExecute(Sender: TObject);
    procedure aFormConfigExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure PartnerPropertiesChange(Sender: TObject);
    procedure aBrisiIzgledExecute(Sender: TObject);
    procedure aSpustiSoberiExecute(Sender: TObject);
    procedure cxLabel7Click(Sender: TObject);
    procedure SetirajLukap(sender:TObject; tabela:TDataSet; tip:TcxDBTextEdit; sifra:TcxDBTextEdit; lukap:TcxExtLookupComboBox);
    procedure cxGrid1DBTableView1FocusedRecordChanged(
      Sender: TcxCustomGridTableView; APrevFocusedRecord,
      AFocusedRecord: TcxCustomGridRecord;
      ANewItemRecordFocusingChanged: Boolean);
    procedure cbNazivArtikalPropertiesChange(Sender: TObject);
    procedure cxGrid2DBTableView1StylesGetContentStyle(
      Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord;
      AItem: TcxCustomGridTableItem; var AStyle: TcxStyle);
    procedure cxGrid2DBTableView1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure aElementExecute(Sender: TObject);
    procedure cxGrid2DBTableView1DblClick(Sender: TObject);
    procedure aNovaSurovinaExecute(Sender: TObject);

  private
    { Private declarations }

    _sifra_kluc :array[1..10] of string;  // �������� ���������� �� ������ �� ������� �� ������������� ���

  protected
    list : TList;
    inserting, sobrano : boolean;
    prva, posledna :TWinControl;
    procedure prefrli;

  public
    { Public declarations }
    constructor Create(Owner : TComponent; insert : boolean = true);reintroduce; overload;

    procedure SetSifra(br: integer; s : string);
    function GetSifra(br: integer) : string;

    //������� �� ���������� ������������ _sifra_kluc
    property sifra_kluc[br :integer]: string read GetSifra write SetSifra;
  end;

var
  frmKrojnaLista: TfrmKrojnaLista;
  rData : TRepositoryData;
  vid_art, sif_art:string;
implementation

uses DaNe, dmKonekcija, Utils, FormConfig, NurkoRepository, dmUnit;

{$R *.dfm}
//------------------------------------------------------------------------------

constructor TfrmKrojnaLista.Create(Owner : TComponent; insert : boolean = true);
begin
inherited Create(Owner);
//	�������� ���� ������� � �������� �� ����������� ���������
//	insert = true -> ����� ���� ����� ���������� �� ������� �� Insert Mode
//  insert = false -> ����� ���� ����� ������� �� ����� �� ������. �� ������� ����� ����� �� �� �������� F5
  inserting := insert;
end;

//	����� �� �����������. �� ��������� ������� �� ������, �� �������� ������� �� ���� ��������,
//	�� ������� ������� �� ������ ������ � �� ������� �� Insert Mode
procedure TfrmKrojnaLista.aNovaSurovinaExecute(Sender: TObject);
begin
  if(cxGridDBTableView1.DataController.DataSource.State = dsBrowse) then
  begin
    cxPageControl1.ActivePageIndex := 2;
    dPanelP.Enabled:=True;
    lPanelP.Enabled:=False;
   // if vid_art = '' then  prva.SetFocus else;
     txtVidSurovina.SetFocus;
   // cbNazivArtikal.Clear;
    cbSurovina.Clear;
  //  dm.tblksVidSurovina.value := 1;
    cxGridDBTableView1.DataController.DataSet.Insert;
    dm.tblKLSVID_ELEMENT.Value := dm.tblKrojnaListaVID_ELEMENT.Value;
    dm.tblKLSELEMENT.Value := dm.tblKrojnaListaELEMENT.Value;
    //   vid_art;
   // txtArtikal.Text := dm.tblArtklID sif_art;
 //   dm.tblKlsojnaListaID.Value := dm.tblArtKLid.Value;
  //  SetirajLukap(sender,dm.tblArtikalRN,txtVidArtikal,txtArtikal, cbNazivArtikal);
   //   SetirajLukap(sender,tblArtikalElement,txtVidElement,txtEl, cbEl);
  end
  else ShowMessage('������ �������� �� ��� �������� �� �� ��������� �������,' + sLineBreak + '� ����� ������� �� ������� ��� �����!');

end;

procedure TfrmKrojnaLista.aNovExecute(Sender: TObject);
begin
  if(cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) then
  begin

    cxPageControl1.ActivePageIndex := 1;
    dPanel.Enabled:=True;
    lPanel.Enabled:=False;
   // if vid_art = '' then  prva.SetFocus else;
       txtVidElement.SetFocus;
   // cbNazivArtikal.Clear;
    cbElement.Clear;
    cxGrid1DBTableView1.DataController.DataSet.Insert;
    dm.tblKrojnaListaVID_ARTIKAL.Value := dm.tblArtKLARTVID.Value;
    //   vid_art;
   // txtArtikal.Text := dm.tblArtklID sif_art;
    dm.tblKrojnaListaID.Value := dm.tblArtKLid.Value;
   // SetirajLukap(sender,dm.tblArtikalRN,txtVidArtikal,txtArtikal, cbNazivArtikal);

  end
  else ShowMessage('������ �������� �� ��� �������� �� �� ��������� �������,' + sLineBreak + '� ����� ������� �� ������� ��� �����!');
end;

//	����� �� ���������. �� ��������� ������� �� ������, �� �������� ������� �� ���� ��������,
//	�� ������� ������� �� ������ ������ � �� ������� �� Edit Mode
procedure TfrmKrojnaLista.aAzurirajExecute(Sender: TObject);
begin
  if(cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) then
  begin
    dPanel.Enabled:=True;
    lPanel.Enabled:=False;
    prva.SetFocus;
    cxGrid1DBTableView1.DataController.DataSet.Edit;
  end
  else ShowMessage('������ �������� �� ��� �������� �� �� ��������� �������,' + sLineBreak + '� ����� ������� �� ������� ��� �����!');
end;

//	����� �� ������ �� ������������� �����
procedure TfrmKrojnaLista.aBrisiExecute(Sender: TObject);
begin
  if ((cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) and
     (cxGrid1DBTableView1.DataController.RecordCount <> 0)) then
    cxGrid1DBTableView1.DataController.DataSet.Delete();
end;

procedure TfrmKrojnaLista.aBrisiIzgledExecute(Sender: TObject);
begin
  brisiGridVoBaza(Name,cxGrid1DBTableView1);
  //brisiPivotVoBaza(Name,cxDBPivotGrid1); �� pivot ��� ���
  BrisiFormaIzgled(self);
end;

//	����� �� ���������� �� ����������
procedure TfrmKrojnaLista.aRefreshExecute(Sender: TObject);
begin
  cxGrid1DBTableView1.DataController.Filter.Root.Clear; //���������� �� ��������
  cxGrid1DBTableView1.DataController.DataSet.Refresh;
end;

//	����� �� ����� �� ������� �� ������� �� ������� �����
procedure TfrmKrojnaLista.aIzlezExecute(Sender: TObject);
begin
  Close;
end;

//	����� �� ������� �� �������� �� ������ �� ���� (Utils.pas)
procedure TfrmKrojnaLista.aSnimiIzgledExecute(Sender: TObject);
begin
  zacuvajGridVoBaza(Name,cxGrid1DBTableView1);
  ZacuvajFormaIzgled(self);
end;

//	����� �� ������� �� ������ �� Excel ������ (Utils.pas)
procedure TfrmKrojnaLista.aZacuvajExcelExecute(Sender: TObject);
begin
  zacuvajVoExcel(cxGrid1, Caption);
end;

//  ��������� �� ������ �� ������� �� ���������� �� ����� �� Enter
procedure TfrmKrojnaLista.EnterKakoTab(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  kom : TWinControl;
begin
    kom := Sender as TWinControl;
    case Key of
        VK_DOWN:
        begin
          PostMessage(Handle,WM_NextDlgCtl,0,0);
          key:=0;
        end;
        VK_UP:
        begin
          PostMessage(Handle,WM_NextDlgCtl,1,0);
          key:=0;
        end;
        VK_RETURN:
        begin
          PostMessage(Handle,WM_NextDlgCtl,0,0);
        end;
        VK_INSERT:
        begin
          //  �� ����� ��������                                          si
//          if (kom = cxExtLookupComboBox1)  then
//          begin
//            frmNurkoRepository := TfrmNurkoRepository.Create(nil,Name,rData);
//            frmNurkoRepository.kontrola_naziv := kom.Name;
//            frmNurkoRepository.ShowModal;
//
//            if (frmNurkoRepository.ModalResult = mrOk) then
//              cxExtLookupComboBox1.EditValue := frmNurkoRepository.cxGrid1Level1.GridView.DataController.GetRecordId(frmNurkoRepository.cxGrid1Level1.GridView.DataController.FocusedRecordIndex);
//
//            frmNurkoRepository.Free;
//       	 end;
	end;
    end;
end;

//	������� �� ��� �� ������ ��� �������� �� �����
procedure TfrmKrojnaLista.cxDBTextEditAllEnter(Sender: TObject);
begin
    TEdit(Sender).Color:=clSkyBlue;
end;

//	������� �� ��� �� ������ ��� ����� �� �����
procedure TfrmKrojnaLista.cxDBTextEditAllExit(Sender: TObject);
begin
    TEdit(Sender).Color:=clWhite;

   if (dm.tblKrojnaLista.State) in [dsInsert, dsEdit] then
    begin
     if ((Sender as TWinControl)= cbNazivArtikal) then
         begin
            if (cbNazivArtikal.Text <>'') then
            begin
          		txtVidArtikal.Text := cbNazivArtikal.EditValue[0];
	            txtArtikal.Text := cbNazivArtikal.EditValue[1];
            end
            else
            begin
              dm.tblKrojnaListaVID_ARTIKAL.Clear;
              dm.tblKrojnaListaARTIKAL.Clear;
            end;
         end;
         if ((Sender as TWinControl)= txtArtikal) or ((Sender as TWinControl)= txtVidArtikal) then
         begin
             SetirajLukap(sender,dm.tblArtikalRN,txtVidArtikal,txtArtikal, cbNazivArtikal);
         end;
         if ((Sender as TWinControl)= cbElement) then
         begin
            if (cbElement.Text <>'') then
            begin
          		txtVidElement.Text := cbElement.EditValue[0];
	            txtElement.Text := cbElement.EditValue[1];
            end
            else
            begin
              dm.tblKrojnaListaVID_ELEMENT.Clear;
              dm.tblKrojnaListaELEMENT.Clear;
            end;
         end;
         if ((Sender as TWinControl)= txtElement) or ((Sender as TWinControl)= txtVidElement) then
         begin
             SetirajLukap(sender,tblArtikalElement,txtVidElement,txtElement, cbElement);
         end;


        if ((Sender as TWinControl)= cbSurovina) then
         begin
            if (cbSurovina.Text <>'') then
            begin
          		txtVidSurovina.Text := cbsurovina.EditValue[0];
	            txtSurovina.Text := cbSurovina.EditValue[1];
            end
            else
            begin
              dm.tblKLSVID_SUROVINA.Clear;
              dm.tblKLSSUROVINA.Clear;
            end;
         end;
         if ((Sender as TWinControl)= txtsurovina) or ((Sender as TWinControl)= txtVidSurovina) then
         begin
             SetirajLukap(sender,tblArtikalSurovina,txtVidSurovina,txtSurovina, cbSurovina);
         end;

    end;
end;

procedure TfrmKrojnaLista.cxGrid1DBTableView1FocusedRecordChanged(
  Sender: TcxCustomGridTableView; APrevFocusedRecord,
  AFocusedRecord: TcxCustomGridRecord; ANewItemRecordFocusingChanged: Boolean);
begin
 if dm.tblKrojnaLista.State = dsBrowse then
 begin
     SetirajLukap(sender,dm.tblArtikalRN,txtVidArtikal,txtArtikal, cbNazivArtikal);
     SetirajLukap(sender,tblArtikalElement,txtVidElement,txtElement, cbElement);
 end;
end;

//  ��������� �� ������ �� ��������� �� ���������� ���� �� ��������
//  ��������� ��: br - ����� ��� �� ������ (�� 0 �� 9) �� ��������� �� ����� �� ����� �� ���������� ����
//  s - ��������� �� ������ �� ������ (���������� �� string �� ����� �� integer �� IntToStr ���������)
procedure TfrmKrojnaLista.SetSifra(br: integer; s : string);
begin
  _sifra_kluc[br] := s;
end;

//  ������� �� ������ �� ��������� �� ���������� ���� �� ��������
//  ��������� ��: br - ����� ��� �� ������ (�� 0 �� 9) �� ��������� �� ����� �� ����� �� ���������� ����
function TfrmKrojnaLista.GetSifra(br: integer) : string;
begin
  Result := _sifra_kluc[br];
end;

procedure TfrmKrojnaLista.PartnerPropertiesChange(Sender: TObject);
begin

end;

//  �� ������� �� ������� ��� ����������� �� Master-�� �� �������� �� ��������� �� ������ ����� SetSifra
procedure TfrmKrojnaLista.prefrli;
begin
end;

procedure TfrmKrojnaLista.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    //	��������� ��� ��������� �� �������
    if (cxGrid1DBTableView1.DataController.DataSource.State = dsEdit) or (cxGrid1DBTableView1.DataController.DataSource.State = dsInsert) then
    begin
        frmDaNe := TfrmDaNe.Create(self, '���������� ��������', '���������� �� �� ��������. ���� ������ �� �� �������?', 1);
        if (frmDaNe.ShowModal <> mrYes) then
        begin
            cxGrid1DBTableView1.DataController.DataSet.Cancel;
            Action := caFree;
        end
        else
          if (Validacija(dPanel) = false) then
          begin
            cxGrid1DBTableView1.DataController.DataSet.Post;
            Action := caFree;
          end
          else Action := caNone;
    end;

  dmRes.FreeRepository(rData);
end;
procedure TfrmKrojnaLista.FormCreate(Sender: TObject);
begin
  ProcitajFormaIzgled(self);
  if (dmRes.use_skin = true) then dxRibbon1.ColorSchemeName := dmRes.skin_name;

  rData := TRepositoryData.Create();  // kreirame instance od klasata TRepositoryData sto se naogja vo dmRes
end;

//------------------------------------------------------------------------------

procedure TfrmKrojnaLista.FormShow(Sender: TObject);
begin
  //  ������� �� ������������ ������������/���������� �� ���������� �� ���� �����
    SpremiForma(self);
  //	�� ��������� �� ������� ������ ��� ���������� � ���� � ��� �������� �� �������
    PrvPosledenTab(dPanel,posledna,prva);
    dxBarManager1Bar1.Caption := Caption;
    dxComponentPrinter1Link1.ReportTitle.Text := Caption;

  //	������� �� ������������ �� ������ �� ������
    procitajGridOdBaza(Name,cxGrid1DBTableView1,false,false);
  //	������� �� ������������ �� ��������� �� ������
    procitajPrintOdBaza(Name,cxGrid1DBTableView1.Name, dxComponentPrinter1Link1);
    sobrano := true;

    dm.tblartkl.close;
    dm.tblartkl.open;

    dm.tblArtikalRN.Close;
    dm.tblArtikalRN.Open;
    tblArtikalElement.Open;
    tblArtikalSurovina.Open;
    dm.tblKrojnaLista.Close;
    dm.tblKrojnaLista.Open;
    dm.tblKLS.Close;
    dm.tblKLS.Open;

    cxPageControl1.ActivePageIndex := 0;
    cxGrid2.SetFocus;
    cxGrid2DBTableView1.Controller.GoToFirst(True);

    SetirajLukap(sender,dm.tblArtikalRN,txtVidArtikal,txtArtikal, cbNazivArtikal);
    SetirajLukap(sender,tblArtikalElement,txtVidElement,txtElement, cbElement);
    SetirajLukap(sender,tblArtikalSurovina,txtVidsurovina,txtsurovina, cbsurovina);

  	cbNazivArtikal.RepositoryItem := dmRes.InitRepository(-101, 'cbNazivArtikal', Name, rData);
    cbElement.RepositoryItem := dmRes.InitRepository(-104, 'cbElement', Name, rData);
    cbSurovina.RepositoryItem := dmRes.InitRepository(-105, 'cbSurovina', Name, rData);
   	cbNazivArtikal.RepositoryItem := dmRes.InitRepositoryRefresh(-101, 'cbNazivArtikal', Name, rData);
    cbElement.RepositoryItem := dmRes.InitRepositoryRefresh(-104, 'cbElement', Name, rData);
    cbSurovina.RepositoryItem := dmRes.InitRepositoryRefresh(-105, 'cbSurovina', Name, rData);

//  �������� �� ����� �� ������ �� cxExtLookupComboBox ��� cxDBExtLookupComboBox ��������
    //cxExtLookupComboBox1.RepositoryItem := dmRes.InitRepository(23, 'cxExtLookupComboBox1', Name, rData);
  	//procitajGridOdBaza(Name + '_' + IntToStr(rData.nurkoRepository[rData.br_repository]), rData.gridRepository[rData.br_repository] ,false,false);
end;
//------------------------------------------------------------------------------

procedure TfrmKrojnaLista.SaveToIniFileExecute(Sender: TObject);
begin
    cxGrid1DBTableView1.StoreToIniFile(Name,True,[]);
end;
//------------------------------------------------------------------------------

procedure TfrmKrojnaLista.cxGrid1DBTableView1KeyPress(Sender: TObject;
  var Key: Char);
begin
//  if(Ord(Key) <> VK_RETURN) then cxSortiraj(cxGrid1DBTableView1);
end;

procedure TfrmKrojnaLista.cxGrid2DBTableView1DblClick(Sender: TObject);
begin
  aElement.Execute;
end;

procedure TfrmKrojnaLista.cxGrid2DBTableView1KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
if key = VK_RETURN  then
        begin
           aElement.Execute;
        end;

end;

procedure TfrmKrojnaLista.cxGrid2DBTableView1StylesGetContentStyle(
  Sender: TcxCustomGridTableView; ARecord: TcxCustomGridRecord;
  AItem: TcxCustomGridTableItem; var AStyle: TcxStyle);
var  AStatus:Integer;
begin
  AStatus:=0;
  if (ARecord<>nil) and (AItem<>nil) then
    if not VarIsNull(TcxCustomGridRecord(ARecord).Values[cxGrid2DBTableView1ID_KL.Index])
      then
         begin
//           if (VarAsType(TcxCustomGridRecord(ARecord).Values[cxGrid1DBTableView1KAKOV.Index], varString) = '0')
//             then
//                       AStatus:=0;
//           if (VarAsType(TcxCustomGridRecord(ARecord).Values[cxGrid1DBTableView2IMENITEL.Index], varInteger) > 1  )
//           then
//                       AStatus:=1;
//           if (VarAsType(TcxCustomGridRecord(ARecord).Values[cxGrid1DBTableView2IMENITEL.Index], varInteger) = 1)
//           then
//                       AStatus:=2;
           if (VarAsType(TcxCustomGridRecord(ARecord).Values[cxGrid2DBTableView1ID_KL.Index], varString) = null)
           then
                       AStatus:=3
           else AStatus := 1;

     case AStatus of
     //0:AStyle:=dm.cxStyleRazdolzeni;
     1:AStyle:= dm.cxStyle29;
     2:AStyle:=dm.cxStyle38;
     3:AStyle:=dm.cxStyle38;
    end;

   end;

end;

procedure TfrmKrojnaLista.cxLabel7Click(Sender: TObject);
begin

end;

//  ����� �� �����
procedure TfrmKrojnaLista.aZapisiExecute(Sender: TObject);
var
  st: TDataSetState;
begin
//  ZapisiButton.SetFocus;
//
 if cxPageControl1.ActivePageIndex = 1 then
 begin
  st := cxGrid1DBTableView1.DataController.DataSet.State;
  if st in [dsEdit,dsInsert] then
  begin
    if (Validacija(dPanel) = false) then
    begin

      if ((st = dsInsert) and inserting) then
      begin
        vid_art := txtVidArtikal.Text;;
        sif_art := txtArtikal.Text;
        cxGrid1DBTableView1.DataController.DataSet.Post;
        aNov.Execute;
      end;

      if ((st = dsInsert) and (not inserting)) then
      begin
        vid_art := txtVidArtikal.Text;;
        sif_art := txtArtikal.Text;
        cxGrid1DBTableView1.DataController.DataSet.Post;
        dPanel.Enabled:=false;
        lPanel.Enabled:=true;
        cxGrid1.SetFocus;
      end;

      if (st = dsEdit) then
      begin
        cxGrid1DBTableView1.DataController.DataSet.Post;
        dPanel.Enabled:=false;
        lPanel.Enabled:=true;
        cxGrid1.SetFocus;
      end;
    end;

  end;
end
else  if cxPageControl1.ActivePageIndex = 2 then
 begin
   st := cxGridDBTableView1.DataController.DataSet.State;
  if st in [dsEdit,dsInsert] then
  begin
    if (Validacija(dPanelp) = false) then
    begin

      if ((st = dsInsert) and inserting) then
      begin
      //  vid_art := txtVidArtikal.Text;;
      //  sif_art := txtArtikal.Text;
        cxGridDBTableView1.DataController.DataSet.Post;
        aNovaSurovina.Execute;
      end;

      if ((st = dsInsert) and (not inserting)) then
      begin
       // vid_art := txtVidArtikal.Text;;
       // sif_art := txtArtikal.Text;
        cxGridDBTableView1.DataController.DataSet.Post;
        dPanelP.Enabled:=false;
        lPanelP.Enabled:=true;
        cxGrid4.SetFocus;
      end;

      if (st = dsEdit) then
      begin
        cxGridDBTableView1.DataController.DataSet.Post;
        dPanelP.Enabled:=false;
        lPanelP.Enabled:=true;
        cxGrid4.SetFocus;
      end;
    end;

  end;

 end;
end;

procedure TfrmKrojnaLista.cbNazivArtikalPropertiesChange(Sender: TObject);
begin

end;

//	����� �� ���������� �� �������
procedure TfrmKrojnaLista.aOtkaziExecute(Sender: TObject);
begin
  if (cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) then
  begin
      ModalResult := mrCancel;
      Close();
  end
  else
  begin
      cxGrid1DBTableView1.DataController.DataSet.Cancel;
      RestoreControls(dPanel);
      dPanel.Enabled := false;
      lPanel.Enabled := true;
      cxGrid1.SetFocus;
  end;
end;

//----------------------------------------------------------------------------------
// ��������� �� ��������, ���������� �� ��������� � �������� �� Print ��������
//----------------------------------------------------------------------------------

//	����� �� �������� �� �������� �� �������
procedure TfrmKrojnaLista.aPageSetupExecute(Sender: TObject);
begin
  dxComponentPrinter1Link1.PageSetup;
end;

//	����� �� ������� �� ������
procedure TfrmKrojnaLista.aPecatiTabelaExecute(Sender: TObject);
begin
  dxComponentPrinter1Link1.ReportTitle.Text := Caption;

  dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Clear;
  dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Add(dmkon.firma_naziv);
  dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Add(DateTimeToStr(Now));

  dxComponentPrinter1Link1.PrinterPage.PageHeader.LeftTitle.Clear;
  //dxComponentPrinter1Link1.PrinterPage.PageHeader.LeftTitle.Add('������� : ' + TipSifra.Text + '-' + IdSifra.Text + ' ' + Partner.Text);

  dxComponentPrinter1.Preview(true, dxComponentPrinter1Link1);
end;

//	����� �� ���������� �� ��������� �� �������
procedure TfrmKrojnaLista.aPodesuvanjePecatenjeExecute(Sender: TObject);
begin
  dxComponentPrinter1Link1.DesignReport();
end;

procedure TfrmKrojnaLista.aSnimiPecatenjeExecute(Sender: TObject);
begin
  zacuvajPrintVoBaza(Name,cxGrid1DBTableView1.Name,dxComponentPrinter1Link1);
end;

//	����� �� �������� ��� �������� �� �������� �� ������ ���� ��� ��������� �� ����� ������
procedure TfrmKrojnaLista.aSpustiSoberiExecute(Sender: TObject);
begin
  if (sobrano = true) then
  begin
    cxGrid1DBTableView1.ViewData.Expand(false);
    sobrano := false;
  end
  else
  begin
    cxGrid1DBTableView1.ViewData.Collapse(false);
    sobrano := true;
  end;
end;

//	����� �� ������ �� ����������� �������� �� ������� (Utils.pas)
procedure TfrmKrojnaLista.aBrisiPodesuvanjePecatenjeExecute(Sender: TObject);
begin
  brisiPrintOdBaza(Name,cxGrid1DBTableView1.Name, dxComponentPrinter1Link1);
end;

procedure TfrmKrojnaLista.aElementExecute(Sender: TObject);
begin
    cxPageControl1.ActivePageIndex := 1;
end;

//  ����� �� ����� �� ����� �� ������������� �� �������� � ��������������� ��
//  �������(������ ���������� ��������)
procedure TfrmKrojnaLista.aFormConfigExecute(Sender: TObject);
begin
  frmFormConfig := TfrmFormConfig.Create(Application);
  frmFormConfig.formPtr := Addr(Self);
  frmFormConfig.ShowModal;
  frmFormConfig.Free;
end;

//----------------------------------------------------------------------------------
// ���� -> ��������� �� ��������, ���������� �� ��������� � �������� �� Print ��������
//----------------------------------------------------------------------------------

// ��������� �� ���������� �� ��������� � �������� �� Print �������� �� .ini ��������
procedure TfrmKrojnaLista.zacuvajPrintVoIni(ime:AnsiString);
var
  AStream: TFileStream;
begin
  AStream := TFileStream.Create(dmKon.RabotenDir + ime + 'Print.ini', fmCreate);
  try
    AStream.WriteComponent(dxComponentPrinter1Link1);
  finally
    AStream.Free;
  end;

end;

// ��������� �� ������ �� ��������� � �������� �� Print �������� �������� �� .ini ��������
procedure TfrmKrojnaLista.procitajPrintOdIni(ime:AnsiString);
var
  AStream: TFileStream;
begin
  AStream := TFileStream.Create(dmKon.RabotenDir + ime + 'Print.ini', fmOpenRead);
  try
    dxComponentPrinter1Link1.RestoreDefaults;
    AStream.ReadComponent(dxComponentPrinter1Link1);
  finally
    AStream.Free;
  end;
end;

procedure TfrmKrojnaLista.SetirajLukap(sender:TObject; tabela:TDataSet; tip:TcxDBTextEdit; sifra:TcxDBTextEdit; lukap:TcxExtLookupComboBox);
begin
       if (tip.Text <>'') and (sifra.Text<>'')  then
       begin
         lukap.EditValue := VarArrayOf([StrToInt(tip.text), StrToInt(sifra.Text)]);
       end
       else
         lukap.Clear;
end;


end.
