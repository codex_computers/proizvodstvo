inherited frmNormativ: TfrmNormativ
  ActiveControl = nil
  Caption = #1053#1086#1088#1084#1072#1090#1080#1074
  ClientHeight = 741
  ClientWidth = 1024
  ExplicitWidth = 1040
  ExplicitHeight = 780
  TextHeight = 13
  inherited lPanel: TPanel
    Width = 262
    Height = 584
    Align = alLeft
    ExplicitWidth = 262
    ExplicitHeight = 369
    object dxTree: TdxDBTreeView [0]
      Left = 2
      Top = 23
      Width = 258
      Height = 559
      ShowNodeHint = True
      OnCustomDraw = grid
      HotTrack = True
      RowSelect = True
      DisplayField = 'NAZIV_ARTIKAL'
      KeyField = 'ID'
      ListField = 'NAZIV_ARTIKAL'
      ParentField = 'KOREN'
      RootValue = '-1'
      SeparatedSt = ' -> '
      RaiseOnError = True
      ReadOnly = True
      Indent = 19
      OnChange = dxTreeChange
      Align = alClient
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      Color = clBtnFace
      ParentColor = False
      Options = [trDBConfirmDelete, trCanDBNavigate, trSmartRecordCopy, trCheckHasChildren]
      SortType = stData
      SelectedIndex = -1
      TabOrder = 1
      OnClick = dxTreeClick
      PopupMenu = PopupMenu2
      ParentFont = False
    end
    inherited cxGrid1: TcxGrid
      Top = 23
      Width = 223
      Height = 274
      Align = alNone
      Visible = False
      ExplicitTop = 23
      ExplicitWidth = 223
      ExplicitHeight = 274
      inherited cxGrid1DBTableView1: TcxGridDBTableView
        OnFocusedRecordChanged = cxGrid1DBTableView1FocusedRecordChanged
        DataController.DataSource = dm.dsNormativ
        DataController.Filter.OnChanged = cxGrid1DBTableView1DataControllerFilterChanged
        DataController.Filter.Active = True
        OptionsView.GroupByBox = False
        object cxGrid1DBTableView1ID: TcxGridDBColumn
          Caption = #1064#1080#1092#1088#1072
          DataBinding.FieldName = 'ID'
          Visible = False
        end
        object cxGrid1DBTableView1VID_ARTIKAL: TcxGridDBColumn
          Caption = #1042#1080#1076' '#1085#1072' '#1072#1088#1090#1080#1082#1072#1083
          DataBinding.FieldName = 'VID_ARTIKAL'
          Visible = False
          Width = 103
        end
        object cxGrid1DBTableView1ARTIKAL: TcxGridDBColumn
          Caption = #1064#1080#1092#1088#1072' '#1085#1072' '#1072#1088#1090#1080#1082#1072#1083
          DataBinding.FieldName = 'ARTIKAL'
          Visible = False
          Width = 118
        end
        object cxGrid1DBTableView1NAZIV_ARIKAL: TcxGridDBColumn
          Caption = #1053#1072#1079#1080#1074
          DataBinding.FieldName = 'NAZIV_ARTIKAL'
          Width = 238
        end
        object cxGrid1DBTableView1KOLICINA_ARTIKAL: TcxGridDBColumn
          Caption = #1050#1086#1083#1080#1095#1080#1085#1072
          DataBinding.FieldName = 'KOLICINA'
          Visible = False
        end
        object cxGrid1DBTableView1MERKA_ARTIKAL: TcxGridDBColumn
          Caption = #1052#1077#1088#1082#1072
          DataBinding.FieldName = 'MERKA'
          Width = 53
        end
        object cxGrid1DBTableView1AKTIVEN: TcxGridDBColumn
          Caption = #1040#1082#1090#1080#1074#1077#1085
          DataBinding.FieldName = 'AKTIVEN'
          Visible = False
        end
        object cxGrid1DBTableView1KOREN: TcxGridDBColumn
          DataBinding.FieldName = 'KOREN'
          Visible = False
        end
      end
    end
    object txtSearchBox: TSearchBox
      Left = 2
      Top = 2
      Width = 258
      Height = 21
      Hint = #1042#1085#1077#1089#1077#1090#1077' '#1076#1077#1083' '#1086#1076' '#1085#1072#1079#1080#1074#1086#1090' '#1085#1072' '#1087#1088#1086#1080#1079#1074#1086#1076#1086#1090' '#1080' '#1082#1083#1080#1082#1085#1077#1090#1077' '#1045#1085#1090#1077#1088
      Align = alTop
      TabOrder = 2
      OnKeyDown = txtSearchBoxKeyDown
    end
    object cxDBTreeList1: TcxDBTreeList
      Left = 2
      Top = 23
      Width = 258
      Height = 559
      Align = alClient
      Bands = <
        item
        end>
      DataController.DataSource = dm.dsNormativ
      DataController.ParentField = 'KOREN'
      DataController.KeyField = 'ID'
      Navigator.Buttons.CustomButtons = <>
      OptionsData.Editing = False
      OptionsData.Deleting = False
      PopupMenu = PopupMenu2
      RootValue = -1
      ScrollbarAnnotations.CustomAnnotations = <>
      TabOrder = 3
      OnChange = cxDBTreeList1Change
      OnClick = dxTreeClick
      ExplicitTop = 164
      ExplicitHeight = 203
      object cxDBTreeList1cxDBTreeListColumn1: TcxDBTreeListColumn
        Caption.Text = #1053#1072#1079#1080#1074
        DataBinding.FieldName = 'NAZIV_ARTIKAL'
        Width = 254
        Position.ColIndex = 0
        Position.RowIndex = 0
        Position.BandIndex = 0
        Summary.FooterSummaryItems = <>
        Summary.GroupFooterSummaryItems = <>
      end
    end
  end
  inherited dPanel: TPanel
    Left = 264
    Top = 126
    Width = 760
    Height = 584
    Align = alRight
    Anchors = [akLeft, akTop, akRight, akBottom]
    Enabled = True
    ExplicitLeft = 264
    ExplicitTop = 126
    ExplicitWidth = 760
    ExplicitHeight = 369
    inherited Label1: TLabel
      Left = 549
      Top = 30
      ExplicitLeft = 549
      ExplicitTop = 30
    end
    inherited Sifra: TcxDBTextEdit
      Tag = 0
      Left = 605
      Top = 27
      Anchors = [akLeft, akTop, akRight, akBottom]
      ExplicitLeft = 605
      ExplicitTop = 27
      ExplicitWidth = 304
      Width = 304
    end
    object cxGroupBox2: TcxGroupBox [2]
      Left = 2
      Top = 2
      Align = alTop
      Caption = '  '#1053#1086#1088#1084#1072#1090#1080#1074' '#1079#1072' '#1072#1088#1090#1080#1082#1083#1086#1090'  ...'
      ParentBackground = False
      ParentFont = False
      Style.Font.Charset = RUSSIAN_CHARSET
      Style.Font.Color = clWindowText
      Style.Font.Height = -12
      Style.Font.Name = 'Tahoma'
      Style.Font.Style = []
      Style.TextStyle = [fsBold]
      Style.TransparentBorder = False
      Style.IsFontAssigned = True
      TabOrder = 1
      OnExit = cxGroupBox2Exit
      DesignSize = (
        756
        120)
      Height = 120
      Width = 756
      object cxLabel6: TcxLabel
        Left = 46
        Top = 36
        Caption = #1040#1088#1090#1080#1082#1072#1083
        Style.TextColor = clRed
        Properties.WordWrap = True
        Transparent = True
        Width = 51
      end
      object txtVidArtikal: TcxDBTextEdit
        Tag = 1
        Left = 101
        Top = 34
        Hint = #1042#1080#1076' '#1085#1072' '#1072#1088#1090#1080#1082#1072#1083'/'#1087#1088#1086#1080#1079#1074#1086#1076
        DataBinding.DataField = 'VID_ARTIKAL'
        DataBinding.DataSource = dm.dsNormativ
        ParentFont = False
        TabOrder = 0
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 59
      end
      object txtArtikal: TcxDBTextEdit
        Tag = 1
        Left = 160
        Top = 34
        Hint = #1064#1080#1092#1088#1072' '#1085#1072' '#1072#1088#1090#1080#1082#1072#1083' / '#1087#1088#1086#1080#1079#1074#1086#1076
        DataBinding.DataField = 'ARTIKAL'
        DataBinding.DataSource = dm.dsNormativ
        ParentFont = False
        TabOrder = 1
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 58
      end
      object cbNazivArtikal: TcxLookupComboBox
        Left = 217
        Top = 34
        Hint = '* '#1044#1074#1086#1077#1085' '#1082#1083#1080#1082' (Insert), '#1079#1072' '#1080#1079#1073#1086#1088' ('#1074#1085#1077#1089') '#1085#1072' '#1072#1088#1090#1080#1082#1072#1083
        Properties.DropDownAutoSize = True
        Properties.DropDownSizeable = True
        Properties.KeyFieldNames = 'ARTVID;id'
        Properties.ListColumns = <
          item
            Width = 30
            FieldName = 'ARTVID'
          end
          item
            Width = 40
            FieldName = 'id'
          end
          item
            Width = 150
            FieldName = 'naziv'
          end>
        Properties.ListFieldIndex = 2
        Properties.ListOptions.SyncMode = True
        Properties.ListSource = dmMat.dsArtikal
        TabOrder = 2
        OnDblClick = cbNazivArtikalClick
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 436
      end
      object cxLabel7: TcxLabel
        Left = 55
        Top = 89
        Caption = #1058#1080#1087' '#1085#1086#1088#1084#1072#1090#1080#1074
        ParentColor = False
        Style.Color = 15790320
        Style.TextColor = clNavy
        Transparent = True
        Visible = False
      end
      object txtTipNormativ: TcxDBTextEdit
        Tag = 1
        Left = 144
        Top = 90
        DataBinding.DataField = 'ID_TIP_NORMATIV'
        DataBinding.DataSource = dm.dsNormativ
        Style.BorderStyle = ebsUltraFlat
        Style.LookAndFeel.NativeStyle = False
        Style.LookAndFeel.SkinName = ''
        Style.TextStyle = []
        Style.TransparentBorder = False
        StyleDisabled.LookAndFeel.NativeStyle = False
        StyleDisabled.LookAndFeel.SkinName = ''
        StyleDisabled.TextColor = clWindowText
        StyleFocused.LookAndFeel.NativeStyle = False
        StyleFocused.LookAndFeel.SkinName = ''
        StyleHot.LookAndFeel.NativeStyle = False
        StyleHot.LookAndFeel.SkinName = ''
        TabOrder = 4
        Visible = False
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 56
      end
      object cbTipNormativ: TcxDBLookupComboBox
        Left = 202
        Top = 89
        Hint = #1050#1083#1080#1082#1085#1077#1090#1077' '#1076#1074#1072' '#1087#1072#1090#1080' '#1079#1072' '#1074#1085#1077#1089' '#1085#1072' '#1085#1086#1074' '#1090#1080#1087' '#1085#1072' '#1085#1086#1088#1084#1072#1090#1080#1074
        AutoSize = False
        DataBinding.DataField = 'ID_TIP_NORMATIV'
        DataBinding.DataSource = dm.dsNormativ
        Properties.ClearKey = 46
        Properties.DropDownAutoSize = True
        Properties.DropDownSizeable = True
        Properties.ImmediatePost = True
        Properties.KeyFieldNames = 'ID'
        Properties.ListColumns = <
          item
            Width = 50
            FieldName = 'NAZIV'
          end>
        Properties.ListSource = dm.dsTipNormativ
        TabOrder = 5
        Visible = False
        OnDblClick = DblClick
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Height = 21
        Width = 243
      end
      object cxLabel8: TcxLabel
        Left = 547
        Top = 61
        Caption = #1050#1086#1088#1077#1085
        Style.TextColor = clNavy
        Transparent = True
        Visible = False
      end
      object txtKoren: TcxDBTextEdit
        Left = 590
        Top = 62
        DataBinding.DataField = 'KOREN'
        DataBinding.DataSource = dm.dsNormativ
        ParentFont = False
        Style.BorderStyle = ebsUltraFlat
        Style.Font.Charset = RUSSIAN_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -11
        Style.Font.Name = 'Tahoma'
        Style.Font.Style = []
        Style.LookAndFeel.NativeStyle = False
        Style.LookAndFeel.SkinName = ''
        Style.TextStyle = []
        Style.TransparentBorder = False
        Style.IsFontAssigned = True
        StyleDisabled.LookAndFeel.NativeStyle = False
        StyleDisabled.LookAndFeel.SkinName = ''
        StyleDisabled.TextColor = clWindowText
        StyleFocused.LookAndFeel.NativeStyle = False
        StyleFocused.LookAndFeel.SkinName = ''
        StyleHot.LookAndFeel.NativeStyle = False
        StyleHot.LookAndFeel.SkinName = ''
        TabOrder = 3
        Visible = False
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 56
      end
      object cxLabel10: TcxLabel
        Left = 41
        Top = 62
        Caption = #1044#1072#1090#1091#1084' '#1086#1076
        Style.TextColor = clNavy
        Transparent = True
      end
      object cxLabel11: TcxLabel
        Left = 225
        Top = 62
        Caption = #1044#1072#1090#1091#1084' '#1076#1086
        Style.TextColor = clNavy
        Transparent = True
      end
      object txtDatumDo: TcxDBDateEdit
        Left = 286
        Top = 61
        Hint = #1044#1072#1090#1091#1084' '#1076#1086' '#1082#1086#1075#1072' '#1074#1072#1078#1080' '#1085#1086#1088#1084#1072#1090#1080#1074#1086#1090
        DataBinding.DataField = 'DATUM_DO'
        DataBinding.DataSource = dm.dsNormativ
        TabOrder = 7
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 121
      end
      object txtDatumOd: TcxDBDateEdit
        Left = 101
        Top = 61
        Hint = #1044#1072#1090#1091#1084' '#1086#1076' '#1082#1086#1075#1072' '#1074#1072#1078#1080' '#1085#1086#1088#1084#1072#1090#1080#1074#1086#1090
        DataBinding.DataField = 'DATUM_OD'
        DataBinding.DataSource = dm.dsNormativ
        TabOrder = 6
        OnEnter = cxDBTextEditAllEnter
        OnExit = cxDBTextEditAllExit
        OnKeyDown = EnterKakoTab
        Width = 117
      end
      object cxDBImage1: TcxDBImage
        Left = 670
        Top = 31
        Hint = #1057#1083#1080#1082#1072' '#1085#1072' '#1072#1088#1090#1080#1082#1083#1086#1090' / '#1087#1088#1086#1080#1079#1074#1086#1076#1086#1090
        DataBinding.DataField = 'SLIKA'
        DataBinding.DataSource = dm.dsNormativ
        Properties.GraphicClassName = 'TdxSmartImage'
        Properties.GraphicTransparency = gtTransparent
        Style.LookAndFeel.NativeStyle = False
        Style.Shadow = False
        StyleDisabled.LookAndFeel.NativeStyle = False
        StyleFocused.LookAndFeel.NativeStyle = False
        StyleHot.LookAndFeel.NativeStyle = False
        TabOrder = 13
        Height = 60
        Width = 75
      end
      object cxGroupBox3: TcxGroupBox
        Left = 0
        Top = -2
        Anchors = [akLeft, akTop, akRight, akBottom]
        Caption = '  '#1053#1086#1088#1084#1072#1090#1080#1074' '#1079#1072' '#1072#1088#1090#1080#1082#1083#1086#1090'  ...'
        Enabled = False
        ParentBackground = False
        ParentFont = False
        Style.Font.Charset = RUSSIAN_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -12
        Style.Font.Name = 'Tahoma'
        Style.Font.Style = []
        Style.Shadow = False
        Style.TextStyle = [fsBold]
        Style.TransparentBorder = False
        Style.IsFontAssigned = True
        TabOrder = 14
        Visible = False
        Height = 130
        Width = 912
        object cxLabel17: TcxLabel
          Left = 46
          Top = 36
          Caption = #1040#1088#1090#1080#1082#1072#1083
          Style.TextColor = clRed
          Properties.WordWrap = True
          Transparent = True
          Width = 51
        end
        object cxLabel18: TcxLabel
          Left = 96
          Top = 88
          Caption = #1058#1080#1087' '#1085#1086#1088#1084#1072#1090#1080#1074
          ParentColor = False
          Style.Color = 15790320
          Style.TextColor = clNavy
          Transparent = True
          Visible = False
        end
        object cxLabel19: TcxLabel
          Left = 555
          Top = 62
          Caption = #1050#1086#1088#1077#1085
          Style.TextColor = clNavy
          Transparent = True
          Visible = False
        end
        object txtK: TcxDBTextEdit
          Left = 594
          Top = 63
          DataBinding.DataField = 'KOREN'
          DataBinding.DataSource = dm.dsNormativ
          ParentFont = False
          Style.BorderStyle = ebsUltraFlat
          Style.Font.Charset = RUSSIAN_CHARSET
          Style.Font.Color = clWindowText
          Style.Font.Height = -11
          Style.Font.Name = 'Tahoma'
          Style.Font.Style = []
          Style.LookAndFeel.NativeStyle = False
          Style.LookAndFeel.SkinName = ''
          Style.TextStyle = []
          Style.TransparentBorder = False
          Style.IsFontAssigned = True
          StyleDisabled.LookAndFeel.NativeStyle = False
          StyleDisabled.LookAndFeel.SkinName = ''
          StyleDisabled.TextColor = clWindowText
          StyleFocused.LookAndFeel.NativeStyle = False
          StyleFocused.LookAndFeel.SkinName = ''
          StyleHot.LookAndFeel.NativeStyle = False
          StyleHot.LookAndFeel.SkinName = ''
          TabOrder = 0
          Visible = False
          Width = 56
        end
        object cxLabel22: TcxLabel
          Left = 42
          Top = 62
          Caption = #1044#1072#1090#1091#1084' '#1086#1076
          Style.TextColor = clNavy
          Transparent = True
        end
        object cxLabel23: TcxLabel
          Left = 280
          Top = 62
          Caption = #1044#1072#1090#1091#1084' '#1076#1086
          Style.TextColor = clNavy
          Transparent = True
        end
        object txtA: TcxTextEdit
          Left = 157
          Top = 31
          Hint = #1064#1080#1092#1088#1072' '#1085#1072' '#1072#1088#1090#1080#1082#1072#1083
          TabOrder = 6
          Width = 57
        end
        object cbA: TcxTextEdit
          Left = 214
          Top = 31
          TabOrder = 7
          Width = 433
        end
        object txtVidA: TcxTextEdit
          Left = 104
          Top = 31
          Hint = #1042#1080#1076' '#1085#1072' '#1072#1088#1090#1080#1082#1072#1083
          TabOrder = 8
          Width = 53
        end
        object cbTN: TcxTextEdit
          Left = 237
          Top = 86
          TabOrder = 9
          Visible = False
          Width = 249
        end
        object txtTN: TcxTextEdit
          Left = 184
          Top = 86
          TabOrder = 10
          Visible = False
          Width = 53
        end
        object txtDod: TcxDateEdit
          Left = 103
          Top = 61
          TabOrder = 11
          Width = 116
        end
        object txtDdo: TcxDateEdit
          Left = 339
          Top = 61
          TabOrder = 12
          Width = 118
        end
        object cxDBImage2: TcxDBImage
          Left = 670
          Top = 31
          DataBinding.DataField = 'SLIKA'
          DataBinding.DataSource = dm.dsNormativ
          Properties.GraphicClassName = 'TdxSmartImage'
          Style.BorderStyle = ebsUltraFlat
          Style.Color = clWindow
          Style.Edges = [bLeft, bTop, bRight, bBottom]
          Style.HotTrack = True
          Style.LookAndFeel.NativeStyle = False
          Style.Shadow = False
          Style.TextStyle = []
          Style.TransparentBorder = True
          StyleDisabled.LookAndFeel.NativeStyle = False
          StyleFocused.LookAndFeel.NativeStyle = False
          StyleHot.LookAndFeel.NativeStyle = False
          TabOrder = 13
          Height = 60
          Width = 75
        end
      end
    end
    object PanelSurovini: TPanel [3]
      Left = 2
      Top = 241
      Width = 756
      Height = 341
      Align = alClient
      TabOrder = 2
      ExplicitHeight = 126
      object cxGrid4: TcxGrid
        Left = 1
        Top = 1
        Width = 754
        Height = 339
        Align = alClient
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
        RootLevelOptions.DetailTabsPosition = dtpTop
        ExplicitHeight = 88
        object cxGrid4DBTableView1: TcxGridDBTableView
          Navigator.Buttons.CustomButtons = <>
          ScrollbarAnnotations.CustomAnnotations = <>
          DataController.DataSource = dm.dsNormativO
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          object cxGrid4DBTableView1ID: TcxGridDBColumn
            Caption = #1064#1080#1092#1088#1072' '#1085#1072' '#1085#1086#1088#1084#1072#1090#1080#1074
            DataBinding.FieldName = 'ID'
          end
          object cxGrid4DBTableView1VID_ARTIKAL: TcxGridDBColumn
            Caption = #1042#1080#1076' '#1085#1072' '#1072#1088#1090#1080#1082#1072#1083
            DataBinding.FieldName = 'VID_ARTIKAL'
          end
          object cxGrid4DBTableView1ARTIKAL: TcxGridDBColumn
            Caption = #1064#1080#1092#1088#1072' '#1085#1072' '#1072#1088#1090#1080#1082#1072#1083
            DataBinding.FieldName = 'ARTIKAL'
            Width = 114
          end
          object cxGrid4DBTableView1NAZIV_ARTIKAL: TcxGridDBColumn
            Caption = #1040#1088#1090#1080#1082#1072#1083
            DataBinding.FieldName = 'NAZIV_ARTIKAL'
            Width = 229
          end
          object cxGrid4DBTableView1KOLICINA: TcxGridDBColumn
            Caption = #1050#1086#1083#1080#1095#1080#1085#1072
            DataBinding.FieldName = 'KOLICINA'
          end
          object cxGrid4DBTableView1MERKA: TcxGridDBColumn
            Caption = #1052#1077#1088#1082#1072
            DataBinding.FieldName = 'MERKA'
            Width = 62
          end
          object cxGrid4DBTableView1SEKVENCA: TcxGridDBColumn
            Caption = #1057#1077#1082#1074#1077#1085#1094#1072
            DataBinding.FieldName = 'SEKVENCA'
          end
          object cxGrid4DBTableView1DATUM_OD: TcxGridDBColumn
            Caption = #1044#1072#1090#1091#1084' '#1086#1076
            DataBinding.FieldName = 'DATUM_OD'
          end
          object cxGrid4DBTableView1DATUM_DO: TcxGridDBColumn
            Caption = #1044#1072#1090#1091#1084' '#1076#1086
            DataBinding.FieldName = 'DATUM_DO'
          end
        end
        object cxGrid4DBBandedTableView1: TcxGridDBBandedTableView
          Navigator.Buttons.CustomButtons = <>
          ScrollbarAnnotations.CustomAnnotations = <>
          DataController.DataSource = dm.dsNormativ
          DataController.Filter.Active = True
          DataController.Options = [dcoAnsiSort, dcoAssignGroupingValues, dcoAssignMasterDetailKeys, dcoSaveExpanding]
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          FilterRow.InfoText = #1050#1083#1080#1082#1085#1077#1090#1077' '#1090#1091#1082#1072' '#1079#1072' '#1076#1072' '#1076#1077#1092#1080#1085#1080#1088#1072#1090#1077' '#1092#1080#1083#1090#1077#1088
          FilterRow.Visible = True
          OptionsBehavior.IncSearch = True
          OptionsCustomize.ColumnsQuickCustomization = True
          OptionsData.Deleting = False
          OptionsData.Editing = False
          OptionsData.Inserting = False
          OptionsView.ColumnAutoWidth = True
          OptionsView.GroupByBox = False
          OptionsView.Indicator = True
          OptionsView.BandCaptionsInColumnAlternateCaption = True
          Bands = <
            item
              Caption = #1057#1091#1088#1086#1074#1080#1085#1080' '#1079#1072' '#1080#1079#1073#1088#1072#1085#1080#1086#1090' '#1072#1088#1090#1080#1082#1072#1083
              HeaderAlignmentHorz = taLeftJustify
              Styles.Header = dmRes.Autumn
              Width = 1522
            end>
          object cxGrid4DBBandedTableView1SEKVENCA: TcxGridDBBandedColumn
            Caption = #1057#1077#1082#1074#1077#1085#1094#1072
            DataBinding.FieldName = 'SEKVENCA'
            Width = 238
            Position.BandIndex = 0
            Position.ColIndex = 6
            Position.RowIndex = 0
          end
          object cxGrid4DBBandedTableView1ID: TcxGridDBBandedColumn
            Caption = #1064#1080#1092#1088#1072' '#1085#1072' '#1085#1086#1088#1084#1072#1090#1080#1074
            DataBinding.FieldName = 'ID'
            Visible = False
            Width = 65
            Position.BandIndex = 0
            Position.ColIndex = 0
            Position.RowIndex = 0
          end
          object cxGrid4DBBandedTableView1VID_ARTIKAL: TcxGridDBBandedColumn
            Caption = #1042#1080#1076' '#1085#1072' '#1072#1088#1090#1080#1082#1072#1083
            DataBinding.FieldName = 'VID_ARTIKAL'
            Width = 84
            Position.BandIndex = 0
            Position.ColIndex = 1
            Position.RowIndex = 0
          end
          object cxGrid4DBBandedTableView1ARTIKAL: TcxGridDBBandedColumn
            Caption = #1064#1080#1092#1088#1072' '#1085#1072' '#1072#1088#1090#1080#1082#1072#1083
            DataBinding.FieldName = 'ARTIKAL'
            Width = 78
            Position.BandIndex = 0
            Position.ColIndex = 2
            Position.RowIndex = 0
          end
          object cxGrid4DBBandedTableView1NAZIV_ARTIKAL: TcxGridDBBandedColumn
            Caption = #1040#1088#1090#1080#1082#1072#1083
            DataBinding.FieldName = 'NAZIV_ARTIKAL'
            Width = 360
            Position.BandIndex = 0
            Position.ColIndex = 3
            Position.RowIndex = 0
          end
          object cxGrid4DBBandedTableView1KOLICINA: TcxGridDBBandedColumn
            Caption = #1050#1086#1083#1080#1095#1080#1085#1072' '
            DataBinding.FieldName = 'KOLICINA'
            Width = 43
            Position.BandIndex = 0
            Position.ColIndex = 4
            Position.RowIndex = 0
          end
          object cxGrid4DBBandedTableView1MERKA: TcxGridDBBandedColumn
            Caption = #1052#1077#1088#1082#1072
            DataBinding.FieldName = 'MERKA'
            Width = 30
            Position.BandIndex = 0
            Position.ColIndex = 5
            Position.RowIndex = 0
          end
          object cxGrid4DBBandedTableView1KOREN: TcxGridDBBandedColumn
            DataBinding.FieldName = 'KOREN'
            Visible = False
            Position.BandIndex = 0
            Position.ColIndex = 7
            Position.RowIndex = 0
          end
        end
        object cxGrid4DBBandedTableView2: TcxGridDBBandedTableView
          Navigator.Buttons.CustomButtons = <>
          ScrollbarAnnotations.CustomAnnotations = <>
          DataController.DataSource = dm.dsNormativOperacija
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          FilterRow.InfoText = #1050#1083#1080#1082#1085#1077#1090#1077' '#1090#1091#1082#1072' '#1079#1072' '#1076#1072' '#1076#1077#1092#1080#1085#1080#1088#1072#1090#1077' '#1092#1080#1083#1090#1077#1088
          FilterRow.Visible = True
          OptionsBehavior.IncSearch = True
          OptionsCustomize.ColumnsQuickCustomization = True
          OptionsCustomize.BandsQuickCustomization = True
          OptionsData.Deleting = False
          OptionsData.Editing = False
          OptionsData.Inserting = False
          OptionsView.GroupByBox = False
          OptionsView.Indicator = True
          Bands = <
            item
              Caption = #1054#1087#1077#1088#1072#1094#1080#1080' '#1085#1072' '#1085#1086#1088#1084#1072#1090#1080#1074#1086#1090
              HeaderAlignmentHorz = taLeftJustify
              Styles.Header = dmRes.Autumn
            end>
          object cxGrid4DBBandedTableView2ID: TcxGridDBBandedColumn
            Caption = #1064#1080#1092#1088#1072
            DataBinding.FieldName = 'ID'
            Position.BandIndex = 0
            Position.ColIndex = 0
            Position.RowIndex = 0
          end
          object cxGrid4DBBandedTableView2ID_PR_OPERACIJA: TcxGridDBBandedColumn
            Caption = #1064#1080#1092#1088#1072' '#1085#1072' '#1086#1087#1077#1088#1072#1094#1080#1112#1072
            DataBinding.FieldName = 'ID_PR_OPERACIJA'
            Position.BandIndex = 0
            Position.ColIndex = 1
            Position.RowIndex = 0
          end
          object cxGrid4DBBandedTableView2OPERACIJA: TcxGridDBBandedColumn
            Caption = #1054#1087#1077#1088#1072#1094#1080#1112#1072
            DataBinding.FieldName = 'OPERACIJA'
            Position.BandIndex = 0
            Position.ColIndex = 2
            Position.RowIndex = 0
          end
          object cxGrid4DBBandedTableView2SEKVENCA: TcxGridDBBandedColumn
            Caption = #1057#1077#1082#1074#1077#1085#1094#1072
            DataBinding.FieldName = 'SEKVENCA'
            Position.BandIndex = 0
            Position.ColIndex = 3
            Position.RowIndex = 0
          end
          object cxGrid4DBBandedTableView2POTREBNO_VREME: TcxGridDBBandedColumn
            Caption = #1055#1086#1090#1088#1077#1073#1085#1086' '#1074#1088#1077#1084#1077
            DataBinding.FieldName = 'POTREBNO_VREME'
            Width = 50
            Position.BandIndex = 0
            Position.ColIndex = 4
            Position.RowIndex = 0
          end
          object cxGrid4DBBandedTableView2MERNA_EDINICA: TcxGridDBBandedColumn
            Caption = #1052#1077#1088'.'#1045#1076#1080#1085'.'
            DataBinding.FieldName = 'MERNA_EDINICA'
            Position.BandIndex = 0
            Position.ColIndex = 5
            Position.RowIndex = 0
          end
        end
        object cxGrid4Level1: TcxGridLevel
          Caption = #1057#1091#1088#1086#1074#1080#1085#1080
          GridView = cxGrid4DBBandedTableView1
        end
        object cxGrid4Level2: TcxGridLevel
          Caption = #1054#1087#1077#1088#1072#1094#1080#1080
          GridView = cxGrid4DBBandedTableView2
        end
      end
    end
    inherited OtkaziButton: TcxButton
      Left = 671
      Top = 373
      TabOrder = 3
      Visible = False
      ExplicitLeft = 671
      ExplicitTop = 158
    end
    inherited ZapisiButton: TcxButton
      Left = 590
      Top = 379
      TabOrder = 4
      Visible = False
      ExplicitLeft = 590
      ExplicitTop = 164
    end
    object pcSurOpe: TcxPageControl
      Left = 2
      Top = 122
      Width = 756
      Height = 119
      Align = alTop
      TabOrder = 5
      Visible = False
      Properties.ActivePage = tsSurovini
      Properties.CustomButtons.Buttons = <>
      ClientRectBottom = 119
      ClientRectRight = 756
      ClientRectTop = 24
      object tsSurovini: TcxTabSheet
        Caption = #1057#1091#1088#1086#1074#1080#1085#1080
        object PanelVnesSurovina: TPanel
          Left = 0
          Top = 0
          Width = 756
          Height = 95
          Align = alClient
          Enabled = False
          TabOrder = 0
          object cxLabel3: TcxLabel
            Left = 28
            Top = 5
            Caption = #1057#1091#1088#1086#1074#1080#1085#1072' ('#1084#1072#1090#1077#1088#1080#1112#1072#1083')'
            Style.TextColor = clRed
            Properties.WordWrap = True
            Transparent = True
            Width = 63
          end
          object txtVidSurovina: TcxDBTextEdit
            Tag = 1
            Left = 102
            Top = 12
            Hint = #1042#1080#1076' '#1085#1072' '#1089#1091#1088#1086#1074#1080#1085#1072
            DataBinding.DataField = 'VID_ARTIKAL'
            DataBinding.DataSource = dm.dsNormativ
            ParentFont = False
            TabOrder = 1
            OnEnter = cxDBTextEditAllEnter
            OnExit = cxDBTextEditAllExit
            OnKeyDown = EnterKakoTab
            Width = 59
          end
          object txtSurovina: TcxDBTextEdit
            Tag = 1
            Left = 160
            Top = 12
            Hint = #1064#1080#1092#1088#1072' '#1085#1072' '#1089#1091#1088#1086#1074#1080#1085#1072
            DataBinding.DataField = 'ARTIKAL'
            DataBinding.DataSource = dm.dsNormativ
            ParentFont = False
            TabOrder = 2
            OnEnter = cxDBTextEditAllEnter
            OnExit = cxDBTextEditAllExit
            OnKeyDown = EnterKakoTab
            Width = 58
          end
          object cbSurovina: TcxLookupComboBox
            Left = 217
            Top = 12
            Hint = #1053#1072#1079#1080#1074' '#1085#1072' '#1089#1091#1088#1086#1074#1080#1085#1072
            Properties.DropDownAutoSize = True
            Properties.DropDownSizeable = True
            Properties.KeyFieldNames = 'ARTVID;id'
            Properties.ListColumns = <
              item
                Width = 30
                FieldName = 'ARTVID'
              end
              item
                Width = 40
                FieldName = 'id'
              end
              item
                Width = 150
                FieldName = 'naziv'
              end>
            Properties.ListFieldIndex = 2
            Properties.ListOptions.SyncMode = True
            Properties.ListSource = dm.dsArtikal
            TabOrder = 3
            OnEnter = cxDBTextEditAllEnter
            OnExit = cxDBTextEditAllExit
            OnKeyDown = EnterKakoTab
            Width = 414
          end
          object cxLabel15: TcxLabel
            Left = 28
            Top = 37
            AutoSize = False
            Caption = #1056#1077#1076#1086#1089#1083#1077#1076' ('#1089#1077#1082#1074#1077#1085#1094#1072')'
            Style.TextColor = clNavy
            Properties.WordWrap = True
            Transparent = True
            Height = 31
            Width = 63
          end
          object txtSekvenca: TcxDBTextEdit
            Left = 102
            Top = 43
            Hint = #1056#1077#1076#1086#1089#1083#1077#1076' '#1085#1072' '#1089#1091#1088#1086#1074#1080#1085#1072#1090#1072
            DataBinding.DataField = 'SEKVENCA'
            DataBinding.DataSource = dm.dsNormativ
            ParentFont = False
            Style.BorderStyle = ebsUltraFlat
            Style.Font.Charset = RUSSIAN_CHARSET
            Style.Font.Color = clWindowText
            Style.Font.Height = -11
            Style.Font.Name = 'Tahoma'
            Style.Font.Style = []
            Style.LookAndFeel.NativeStyle = False
            Style.LookAndFeel.SkinName = ''
            Style.TextStyle = []
            Style.TransparentBorder = False
            Style.IsFontAssigned = True
            StyleDisabled.LookAndFeel.NativeStyle = False
            StyleDisabled.LookAndFeel.SkinName = ''
            StyleFocused.LookAndFeel.NativeStyle = False
            StyleFocused.LookAndFeel.SkinName = ''
            StyleHot.LookAndFeel.NativeStyle = False
            StyleHot.LookAndFeel.SkinName = ''
            TabOrder = 5
            OnEnter = cxDBTextEditAllEnter
            OnExit = cxDBTextEditAllExit
            OnKeyDown = EnterKakoTab
            Width = 116
          end
          object cxLabel4: TcxLabel
            Left = 253
            Top = 45
            Caption = #1050#1086#1083#1080#1095#1080#1085#1072
            Style.TextColor = clRed
            Transparent = True
          end
          object txtKolicina: TcxDBTextEdit
            Tag = 1
            Left = 314
            Top = 43
            Hint = #1050#1086#1083#1080#1095#1080#1085#1072' '#1085#1072' '#1089#1091#1088#1086#1074#1080#1085#1072#1090#1072', '#1087#1086#1090#1088#1077#1073#1085#1086' '#1079#1072' '#1085#1086#1088#1084#1072#1090#1080#1074#1086#1090
            DataBinding.DataField = 'KOLICINA'
            DataBinding.DataSource = dm.dsNormativ
            ParentFont = False
            Style.BorderStyle = ebsUltraFlat
            Style.Font.Charset = RUSSIAN_CHARSET
            Style.Font.Color = clWindowText
            Style.Font.Height = -11
            Style.Font.Name = 'Tahoma'
            Style.Font.Style = []
            Style.LookAndFeel.NativeStyle = False
            Style.LookAndFeel.SkinName = ''
            Style.TextStyle = []
            Style.TransparentBorder = False
            Style.IsFontAssigned = True
            StyleDisabled.LookAndFeel.NativeStyle = False
            StyleDisabled.LookAndFeel.SkinName = ''
            StyleFocused.LookAndFeel.NativeStyle = False
            StyleFocused.LookAndFeel.SkinName = ''
            StyleHot.LookAndFeel.NativeStyle = False
            StyleHot.LookAndFeel.SkinName = ''
            TabOrder = 7
            OnEnter = cxDBTextEditAllEnter
            OnExit = cxDBTextEditAllExit
            OnKeyDown = EnterKakoTab
            Width = 59
          end
          object cxLabel5: TcxLabel
            Left = 394
            Top = 45
            Caption = #1052#1077#1088#1082#1072
            Style.TextColor = clRed
            Transparent = True
          end
          object txtMerka: TcxDBTextEdit
            Tag = 1
            Left = 430
            Top = 43
            Hint = #1064#1080#1092#1088#1072' '#1085#1072' '#1077#1076#1080#1085#1080#1094#1072' '#1084#1077#1088#1082#1072
            DataBinding.DataField = 'MERKA'
            DataBinding.DataSource = dm.dsNormativ
            ParentFont = False
            Style.BorderStyle = ebsUltraFlat
            Style.Font.Charset = RUSSIAN_CHARSET
            Style.Font.Color = clWindowText
            Style.Font.Height = -11
            Style.Font.Name = 'Tahoma'
            Style.Font.Style = []
            Style.LookAndFeel.NativeStyle = False
            Style.LookAndFeel.SkinName = ''
            Style.TextStyle = []
            Style.TransparentBorder = False
            Style.IsFontAssigned = True
            StyleDisabled.LookAndFeel.NativeStyle = False
            StyleDisabled.LookAndFeel.SkinName = ''
            StyleDisabled.TextColor = clWindowText
            StyleFocused.LookAndFeel.NativeStyle = False
            StyleFocused.LookAndFeel.SkinName = ''
            StyleHot.LookAndFeel.NativeStyle = False
            StyleHot.LookAndFeel.SkinName = ''
            TabOrder = 9
            OnEnter = cxDBTextEditAllEnter
            OnExit = cxDBTextEditAllExit
            OnKeyDown = EnterKakoTab
            Width = 56
          end
          object cbMerka: TcxDBLookupComboBox
            Left = 486
            Top = 42
            Hint = #1053#1072#1079#1080#1074' '#1085#1072' '#1077#1076#1085#1080#1094#1072' '#1084#1077#1088#1082#1072
            AutoSize = False
            DataBinding.DataField = 'MERKA'
            DataBinding.DataSource = dm.dsNormativ
            Properties.DropDownAutoSize = True
            Properties.DropDownSizeable = True
            Properties.KeyFieldNames = 'ID'
            Properties.ListColumns = <
              item
                FieldName = 'NAZIV'
              end>
            Properties.ListSource = dmMat.dsMerka
            TabOrder = 10
            OnEnter = cxDBTextEditAllEnter
            OnExit = cxDBTextEditAllExit
            OnKeyDown = EnterKakoTab
            Height = 21
            Width = 145
          end
          object cbAktiven: TcxDBCheckBox
            Left = 663
            Top = 12
            Caption = #1040#1082#1090#1080#1074#1077#1085
            DataBinding.DataField = 'AKTIVEN'
            DataBinding.DataSource = dm.dsNormativ
            Properties.Alignment = taLeftJustify
            Properties.ValueChecked = 1
            Properties.ValueUnchecked = 0
            Style.TextColor = clRed
            TabOrder = 13
            Transparent = True
            OnEnter = cxDBTextEditAllEnter
            OnExit = cxDBTextEditAllExit
            OnKeyDown = EnterKakoTab
          end
          object ZapisiButton1: TcxButton
            Left = 593
            Top = 66
            Width = 73
            Height = 20
            Action = aZapisi
            Colors.Pressed = clGradientActiveCaption
            TabOrder = 11
          end
          object pnormativorg: TcxButton
            Left = 671
            Top = 66
            Width = 73
            Height = 20
            Action = aOtkazi
            Colors.Pressed = clGradientActiveCaption
            TabOrder = 12
          end
        end
      end
      object tsOperacii: TcxTabSheet
        Caption = #1054#1087#1077#1088#1072#1094#1080#1080
        ImageIndex = 1
        object PanelVnesOperacija: TPanel
          Left = 0
          Top = 0
          Width = 756
          Height = 95
          Align = alClient
          Enabled = False
          TabOrder = 0
          DesignSize = (
            756
            95)
          object cxLabel27: TcxLabel
            Left = 34
            Top = 20
            Caption = #1054#1087#1077#1088#1072#1094#1080#1112#1072
            Style.TextColor = clRed
            Transparent = True
          end
          object cxLabel28: TcxLabel
            Left = 263
            Top = 42
            Caption = #1056#1077#1076#1086#1089#1083#1077#1076' ('#1089#1077#1082#1074#1077#1085#1094#1072')'
            Style.TextColor = clNavy
            Properties.WordWrap = True
            Transparent = True
            Width = 59
          end
          object cxButton5: TcxButton
            Left = 528
            Top = 65
            Width = 75
            Height = 25
            Action = aZapisiOperacija
            Anchors = [akRight, akBottom]
            Colors.Pressed = clGradientActiveCaption
            TabOrder = 6
          end
          object cxButton6: TcxButton
            Left = 609
            Top = 65
            Width = 75
            Height = 25
            Action = aOtkazi
            Anchors = [akRight, akBottom]
            Colors.Pressed = clGradientActiveCaption
            TabOrder = 8
          end
          object txtSekvencaO: TcxDBTextEdit
            Left = 328
            Top = 47
            Hint = #1056#1077#1076#1086#1089#1083#1077#1076' '#1085#1072' '#1086#1087#1077#1088#1072#1094#1080#1112#1072#1090#1072
            DataBinding.DataField = 'SEKVENCA'
            DataBinding.DataSource = dm.dsNormativOperacija
            ParentFont = False
            Style.BorderStyle = ebsUltraFlat
            Style.Font.Charset = RUSSIAN_CHARSET
            Style.Font.Color = clWindowText
            Style.Font.Height = -11
            Style.Font.Name = 'Tahoma'
            Style.Font.Style = []
            Style.LookAndFeel.NativeStyle = False
            Style.LookAndFeel.SkinName = ''
            Style.TextStyle = []
            Style.TransparentBorder = False
            Style.IsFontAssigned = True
            StyleDisabled.LookAndFeel.NativeStyle = False
            StyleDisabled.LookAndFeel.SkinName = ''
            StyleFocused.LookAndFeel.NativeStyle = False
            StyleFocused.LookAndFeel.SkinName = ''
            StyleHot.LookAndFeel.NativeStyle = False
            StyleHot.LookAndFeel.SkinName = ''
            TabOrder = 4
            OnEnter = cxDBTextEditAllEnter
            OnExit = cxDBTextEditAllExit
            OnKeyDown = EnterKakoTab
            Width = 116
          end
          object cbOperacija: TcxDBLookupComboBox
            Left = 154
            Top = 20
            Hint = #1053#1072#1079#1080#1074' '#1085#1072' '#1086#1087#1077#1088#1072#1094#1080#1112#1072
            DataBinding.DataField = 'ID_PR_OPERACIJA'
            DataBinding.DataSource = dm.dsNormativOperacija
            Properties.DropDownAutoSize = True
            Properties.DropDownSizeable = True
            Properties.ImmediatePost = True
            Properties.KeyFieldNames = 'ID'
            Properties.ListColumns = <
              item
                Width = 150
                FieldName = 'NAZIV_OPERACIJA'
              end
              item
                Caption = #1055#1088#1086#1080#1079#1074#1086#1076#1089#1090#1074#1077#1085' '#1088#1077#1089#1091#1088#1089
                Width = 150
                FieldName = 'proizvodstven_resurs'
              end>
            Properties.ListOptions.SyncMode = True
            Properties.ListSource = dm.dsOperacijaPR
            TabOrder = 1
            OnEnter = cxDBTextEditAllEnter
            OnExit = cxDBTextEditAllExit
            OnKeyDown = EnterKakoTab
            Width = 399
          end
          object txtOperacija: TcxDBTextEdit
            Tag = 1
            Left = 97
            Top = 21
            Hint = #1064#1080#1092#1088#1072' '#1085#1072' '#1086#1087#1077#1088#1072#1094#1080#1112#1072
            DataBinding.DataField = 'ID_PR_OPERACIJA'
            DataBinding.DataSource = dm.dsNormativOperacija
            ParentFont = False
            Style.BorderStyle = ebsUltraFlat
            Style.LookAndFeel.NativeStyle = False
            Style.LookAndFeel.SkinName = ''
            Style.TextStyle = []
            Style.TransparentBorder = False
            StyleDisabled.LookAndFeel.NativeStyle = False
            StyleDisabled.LookAndFeel.SkinName = ''
            StyleFocused.LookAndFeel.NativeStyle = False
            StyleFocused.LookAndFeel.SkinName = ''
            StyleHot.LookAndFeel.NativeStyle = False
            StyleHot.LookAndFeel.SkinName = ''
            TabOrder = 0
            OnEnter = cxDBTextEditAllEnter
            OnExit = cxDBTextEditAllExit
            OnKeyDown = EnterKakoTab
            Width = 55
          end
          object txtPotrebnoVreme: TcxDBTextEdit
            Left = 97
            Top = 47
            Hint = #1055#1086#1090#1088#1077#1073#1085#1086' '#1074#1088#1077#1084#1077
            DataBinding.DataField = 'POTREBNO_VREME'
            DataBinding.DataSource = dm.dsNormativOperacija
            ParentFont = False
            Properties.ReadOnly = False
            Style.BorderStyle = ebsUltraFlat
            Style.Font.Charset = RUSSIAN_CHARSET
            Style.Font.Color = clWindowText
            Style.Font.Height = -11
            Style.Font.Name = 'Tahoma'
            Style.Font.Style = []
            Style.LookAndFeel.NativeStyle = False
            Style.LookAndFeel.SkinName = ''
            Style.TextStyle = []
            Style.TransparentBorder = False
            Style.IsFontAssigned = True
            StyleDisabled.LookAndFeel.NativeStyle = False
            StyleDisabled.LookAndFeel.SkinName = ''
            StyleFocused.LookAndFeel.NativeStyle = False
            StyleFocused.LookAndFeel.SkinName = ''
            StyleHot.LookAndFeel.NativeStyle = False
            StyleHot.LookAndFeel.SkinName = ''
            TabOrder = 2
            OnEnter = cxDBTextEditAllEnter
            OnExit = cxDBTextEditAllExit
            OnKeyDown = EnterKakoTab
            Width = 55
          end
          object cxlbl1: TcxLabel
            Left = 32
            Top = 42
            Caption = #1055#1086#1090#1088#1077#1073#1085#1086' '#1074#1088#1077#1084#1077
            Style.TextColor = clNavy
            Properties.WordWrap = True
            Transparent = True
            Width = 53
          end
          object cxDBLabel1: TcxDBLabel
            Left = 155
            Top = 49
            Hint = #1042#1088#1077#1084#1077#1085#1089#1082#1072' '#1084#1077#1088#1082#1072
            DataBinding.DataField = 'MERNA_EDINICA'
            DataBinding.DataSource = dm.dsNormativOperacija
            Transparent = True
            Height = 17
            Width = 38
          end
        end
      end
    end
  end
  inherited dxRibbon1: TdxRibbon
    Width = 1024
    ExplicitWidth = 1024
    inherited dxRibbon1Tab1: TdxRibbonTab
      Groups = <
        item
          ToolbarName = 'dxBarManager1Bar1'
        end
        item
          ToolbarName = 'dxBarManager1Bar5'
        end
        item
          Caption = #1054#1087#1077#1088#1072#1094#1080#1112#1072
          ToolbarName = 'dxBarManager1Bar7'
        end
        item
          Caption = #1056#1077#1074#1080#1079#1080#1112#1072
          ToolbarName = 'dxBarManager1Bar6'
        end
        item
          ToolbarName = 'dxBarManager1Bar2'
        end
        item
          ToolbarName = 'dxBarManager1Bar3'
        end>
      Index = 0
    end
    inherited dxRibbon1Tab2: TdxRibbonTab
      Index = 1
    end
  end
  inherited StatusBar1: TdxRibbonStatusBar
    Top = 718
    Width = 1024
    ExplicitTop = 718
    ExplicitWidth = 1024
  end
  object Panel1: TPanel [4]
    Left = 0
    Top = 710
    Width = 1024
    Height = 0
    Align = alBottom
    TabOrder = 4
    ExplicitTop = 718
    object cxGrid2: TcxGrid
      Left = 260
      Top = 1
      Width = 133
      Height = 12
      Align = alLeft
      TabOrder = 0
      Visible = False
      ExplicitHeight = 213
      object cxGrid2DBTableView1: TcxGridDBTableView
        Navigator.Buttons.CustomButtons = <>
        ScrollbarAnnotations.CustomAnnotations = <>
        OnFocusedRecordChanged = cxGrid2DBTableView1FocusedRecordChanged
        DataController.DataSource = dm.dsNormativ
        DataController.Options = [dcoAssignGroupingValues, dcoAssignMasterDetailKeys, dcoSaveExpanding, dcoGroupsAlwaysExpanded]
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        FilterRow.InfoText = #1050#1083#1080#1082#1085#1080' '#1086#1074#1076#1077' '#1079#1072' '#1076#1072' '#1076#1077#1092#1080#1085#1080#1088#1072#1096' '#1092#1080#1083#1090#1077#1088
        FilterRow.Visible = True
        FilterRow.ApplyChanges = fracImmediately
        OptionsBehavior.IncSearch = True
        OptionsBehavior.ExpandMasterRowOnDblClick = False
        OptionsCustomize.ColumnsQuickCustomization = True
        OptionsData.Deleting = False
        OptionsData.Editing = False
        OptionsData.Inserting = False
        OptionsView.Footer = True
        object cxGrid2DBTableView1ID: TcxGridDBColumn
          Caption = #1064#1080#1092#1088#1072
          DataBinding.FieldName = 'ID'
          Visible = False
          SortIndex = 0
          SortOrder = soAscending
        end
        object cxGrid2DBTableView1VID_ARTIKAL: TcxGridDBColumn
          Caption = #1042#1080#1076' '#1040#1088#1090#1080#1082#1072#1083
          DataBinding.FieldName = 'VID_ARTIKAL'
          Visible = False
        end
        object cxGrid2DBTableView1ARTIKAL: TcxGridDBColumn
          Caption = #1064#1080#1092#1088#1072' '#1085#1072' '#1072#1088#1090#1080#1082#1072#1083
          DataBinding.FieldName = 'ARTIKAL'
          Visible = False
        end
        object cxGrid2DBTableView1NAZIV_ARIKAL: TcxGridDBColumn
          Caption = #1040#1088#1090#1080#1082#1072#1083':'
          DataBinding.FieldName = 'NAZIV_ARTIKAL'
          Visible = False
          GroupIndex = 0
          Width = 162
        end
        object cxGrid2DBTableView1KOLICINA_ARTIKAL: TcxGridDBColumn
          Caption = #1050#1086#1083#1080#1095#1080#1085#1072' '#1085#1072' '#1072#1088#1090#1080#1082#1072#1083
          DataBinding.FieldName = 'KOLICINA_ARTIKAL'
          Visible = False
          Width = 76
        end
        object cxGrid2DBTableView1NAZIV_TIP_NORMATIV: TcxGridDBColumn
          Caption = #1058#1080#1087' '#1085#1072' '#1085#1086#1088#1084#1072#1090#1080#1074
          DataBinding.FieldName = 'NAZIV_TIP_NORMATIV'
          Visible = False
          Width = 200
        end
        object cxGrid2DBTableView1MERKA_ARTIKAL: TcxGridDBColumn
          Caption = #1045#1076'.'#1084#1077#1088#1082#1072' '#1085#1072' '#1072#1088#1090#1080#1082#1072#1083
          DataBinding.FieldName = 'MERKA_ARTIKAL'
          Visible = False
          Width = 105
        end
        object cxGrid2DBTableView1VID_SUROVINA: TcxGridDBColumn
          Caption = #1042#1080#1076' '#1085#1072' '#1089#1091#1088#1086#1074#1080#1085#1072
          DataBinding.FieldName = 'VID_SUROVINA'
          Visible = False
        end
        object cxGrid2DBTableView1SUROVINA: TcxGridDBColumn
          Caption = #1064#1080#1092#1088#1072' '#1085#1072' '#1089#1091#1088#1086#1074#1080#1085#1072
          DataBinding.FieldName = 'SUROVINA'
          Width = 26
        end
        object cxGrid2DBTableView1NAZIV_SUROVINA: TcxGridDBColumn
          Caption = #1057#1091#1088#1086#1074#1080#1085#1072
          DataBinding.FieldName = 'NAZIV_SUROVINA'
          Width = 124
        end
        object cxGrid2DBTableView1KOLICINA: TcxGridDBColumn
          Caption = #1050#1086#1083#1080#1095#1080#1085#1072
          DataBinding.FieldName = 'KOLICINA'
          Width = 57
        end
        object cxGrid2DBTableView1MERKA: TcxGridDBColumn
          Caption = #1045#1076' '#1084#1077#1088#1082#1072
          DataBinding.FieldName = 'MERKA'
          Width = 67
        end
        object cxGrid2DBTableView1AKTIVEN: TcxGridDBColumn
          Caption = #1040#1082#1090#1080#1074#1077#1085
          DataBinding.FieldName = 'AKTIVEN'
          Width = 72
        end
        object cxGrid2DBTableView1ID_TIP_NORMATIV: TcxGridDBColumn
          DataBinding.FieldName = 'ID_TIP_NORMATIV'
          Visible = False
        end
        object cxGrid2DBTableView1SEKVENCA: TcxGridDBColumn
          Caption = #1057#1077#1082#1074#1077#1085#1094#1072
          DataBinding.FieldName = 'SEKVENCA'
        end
      end
      object cxGrid2DBBandedTableView1: TcxGridDBBandedTableView
        Navigator.Buttons.CustomButtons = <>
        ScrollbarAnnotations.CustomAnnotations = <>
        DataController.DataSource = dm.dsNormativ
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        Bands = <
          item
            Caption = 'Normativ'
          end
          item
          end
          item
          end
          item
          end
          item
          end
          item
          end
          item
          end
          item
          end>
        object cxGrid2DBBandedTableView1ID: TcxGridDBBandedColumn
          DataBinding.FieldName = 'ID'
          Position.BandIndex = 0
          Position.ColIndex = 0
          Position.RowIndex = 0
        end
        object cxGrid2DBBandedTableView1VID_ARTIKAL: TcxGridDBBandedColumn
          DataBinding.FieldName = 'VID_ARTIKAL'
          Position.BandIndex = 0
          Position.ColIndex = 1
          Position.RowIndex = 0
        end
        object cxGrid2DBBandedTableView1ARTIKAL: TcxGridDBBandedColumn
          DataBinding.FieldName = 'ARTIKAL'
          Position.BandIndex = 0
          Position.ColIndex = 2
          Position.RowIndex = 0
        end
        object cxGrid2DBBandedTableView1KOLICINA_ARTIKAL: TcxGridDBBandedColumn
          DataBinding.FieldName = 'KOLICINA_ARTIKAL'
          Position.BandIndex = 0
          Position.ColIndex = 3
          Position.RowIndex = 0
        end
        object cxGrid2DBBandedTableView1MERKA_ARTIKAL: TcxGridDBBandedColumn
          DataBinding.FieldName = 'MERKA_ARTIKAL'
          Position.BandIndex = 0
          Position.ColIndex = 4
          Position.RowIndex = 0
        end
        object cxGrid2DBBandedTableView1AKTIVEN: TcxGridDBBandedColumn
          DataBinding.FieldName = 'AKTIVEN'
          Position.BandIndex = 0
          Position.ColIndex = 5
          Position.RowIndex = 0
        end
        object cxGrid2DBBandedTableView1VID_SUROVINA: TcxGridDBBandedColumn
          DataBinding.FieldName = 'VID_SUROVINA'
          Position.BandIndex = 0
          Position.ColIndex = 6
          Position.RowIndex = 0
        end
        object cxGrid2DBBandedTableView1SUROVINA: TcxGridDBBandedColumn
          DataBinding.FieldName = 'SUROVINA'
          Position.BandIndex = 1
          Position.ColIndex = 0
          Position.RowIndex = 0
        end
        object cxGrid2DBBandedTableView1NAZIV_SUROVINA: TcxGridDBBandedColumn
          DataBinding.FieldName = 'NAZIV_SUROVINA'
          Position.BandIndex = 1
          Position.ColIndex = 1
          Position.RowIndex = 0
        end
        object cxGrid2DBBandedTableView1KOLICINA: TcxGridDBBandedColumn
          DataBinding.FieldName = 'KOLICINA'
          Position.BandIndex = 1
          Position.ColIndex = 2
          Position.RowIndex = 0
        end
        object cxGrid2DBBandedTableView1MERKA: TcxGridDBBandedColumn
          DataBinding.FieldName = 'MERKA'
          Position.BandIndex = 1
          Position.ColIndex = 3
          Position.RowIndex = 0
        end
        object cxGrid2DBBandedTableView1ID_TIP_NORMATIV: TcxGridDBBandedColumn
          DataBinding.FieldName = 'ID_TIP_NORMATIV'
          Position.BandIndex = 0
          Position.ColIndex = 7
          Position.RowIndex = 0
        end
        object cxGrid2DBBandedTableView1NAZIV_TIP_NORMATIV: TcxGridDBBandedColumn
          DataBinding.FieldName = 'NAZIV_TIP_NORMATIV'
          Position.BandIndex = 0
          Position.ColIndex = 8
          Position.RowIndex = 0
        end
        object cxGrid2DBBandedTableView1ID_OPERACIJA: TcxGridDBBandedColumn
          DataBinding.FieldName = 'ID_OPERACIJA'
          Position.BandIndex = 0
          Position.ColIndex = 9
          Position.RowIndex = 0
        end
        object cxGrid2DBBandedTableView1SEKVENCA: TcxGridDBBandedColumn
          DataBinding.FieldName = 'SEKVENCA'
          Position.BandIndex = 0
          Position.ColIndex = 10
          Position.RowIndex = 0
        end
        object cxGrid2DBBandedTableView1VID_ARTIKAL_RODITEL: TcxGridDBBandedColumn
          DataBinding.FieldName = 'VID_ARTIKAL_RODITEL'
          Position.BandIndex = 0
          Position.ColIndex = 11
          Position.RowIndex = 0
        end
        object cxGrid2DBBandedTableView1ARTIKAL_RODITEL: TcxGridDBBandedColumn
          DataBinding.FieldName = 'ARTIKAL_RODITEL'
          Position.BandIndex = 0
          Position.ColIndex = 12
          Position.RowIndex = 0
        end
        object cxGrid2DBBandedTableView1POTEKLO: TcxGridDBBandedColumn
          DataBinding.FieldName = 'POTEKLO'
          Position.BandIndex = 0
          Position.ColIndex = 13
          Position.RowIndex = 0
        end
        object cxGrid2DBBandedTableView1DATUM_OD: TcxGridDBBandedColumn
          DataBinding.FieldName = 'DATUM_OD'
          Position.BandIndex = 0
          Position.ColIndex = 14
          Position.RowIndex = 0
        end
        object cxGrid2DBBandedTableView1DATUM_DO: TcxGridDBBandedColumn
          DataBinding.FieldName = 'DATUM_DO'
          Position.BandIndex = 0
          Position.ColIndex = 15
          Position.RowIndex = 0
        end
        object cxGrid2DBBandedTableView1KOREN: TcxGridDBBandedColumn
          DataBinding.FieldName = 'KOREN'
          Position.BandIndex = 0
          Position.ColIndex = 16
          Position.RowIndex = 0
        end
        object cxGrid2DBBandedTableView1NAZIV_ARTIKAL: TcxGridDBBandedColumn
          DataBinding.FieldName = 'NAZIV_ARTIKAL'
          Position.BandIndex = 0
          Position.ColIndex = 17
          Position.RowIndex = 0
        end
        object cxGrid2DBBandedTableView1NAZIV_ORG: TcxGridDBBandedColumn
          DataBinding.FieldName = 'NAZIV_ORG'
          Position.BandIndex = 0
          Position.ColIndex = 18
          Position.RowIndex = 0
        end
      end
      object cxGrid2DBCardView1: TcxGridDBCardView
        Navigator.Buttons.CustomButtons = <>
        ScrollbarAnnotations.CustomAnnotations = <>
        DataController.DataSource = dm.dsNormativ
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        OptionsView.CardIndent = 7
        object cxGrid2DBCardView1ID: TcxGridDBCardViewRow
          DataBinding.FieldName = 'ID'
          Position.BeginsLayer = True
        end
        object cxGrid2DBCardView1VID_ARTIKAL: TcxGridDBCardViewRow
          DataBinding.FieldName = 'VID_ARTIKAL'
          Position.BeginsLayer = True
        end
        object cxGrid2DBCardView1ARTIKAL: TcxGridDBCardViewRow
          DataBinding.FieldName = 'ARTIKAL'
          Position.BeginsLayer = True
        end
        object cxGrid2DBCardView1KOLICINA_ARTIKAL: TcxGridDBCardViewRow
          DataBinding.FieldName = 'KOLICINA_ARTIKAL'
          Position.BeginsLayer = True
        end
        object cxGrid2DBCardView1MERKA_ARTIKAL: TcxGridDBCardViewRow
          DataBinding.FieldName = 'MERKA_ARTIKAL'
          Position.BeginsLayer = True
        end
        object cxGrid2DBCardView1AKTIVEN: TcxGridDBCardViewRow
          DataBinding.FieldName = 'AKTIVEN'
          Position.BeginsLayer = True
        end
        object cxGrid2DBCardView1VID_SUROVINA: TcxGridDBCardViewRow
          DataBinding.FieldName = 'VID_SUROVINA'
          Position.BeginsLayer = True
        end
        object cxGrid2DBCardView1SUROVINA: TcxGridDBCardViewRow
          DataBinding.FieldName = 'SUROVINA'
          Position.BeginsLayer = True
        end
        object cxGrid2DBCardView1NAZIV_SUROVINA: TcxGridDBCardViewRow
          DataBinding.FieldName = 'NAZIV_SUROVINA'
          Position.BeginsLayer = True
        end
        object cxGrid2DBCardView1KOLICINA: TcxGridDBCardViewRow
          DataBinding.FieldName = 'KOLICINA'
          Position.BeginsLayer = True
        end
        object cxGrid2DBCardView1MERKA: TcxGridDBCardViewRow
          DataBinding.FieldName = 'MERKA'
          Position.BeginsLayer = True
        end
        object cxGrid2DBCardView1ID_TIP_NORMATIV: TcxGridDBCardViewRow
          DataBinding.FieldName = 'ID_TIP_NORMATIV'
          Position.BeginsLayer = True
        end
        object cxGrid2DBCardView1NAZIV_TIP_NORMATIV: TcxGridDBCardViewRow
          DataBinding.FieldName = 'NAZIV_TIP_NORMATIV'
          Position.BeginsLayer = True
        end
        object cxGrid2DBCardView1ID_OPERACIJA: TcxGridDBCardViewRow
          DataBinding.FieldName = 'ID_OPERACIJA'
          Position.BeginsLayer = True
        end
        object cxGrid2DBCardView1SEKVENCA: TcxGridDBCardViewRow
          DataBinding.FieldName = 'SEKVENCA'
          Position.BeginsLayer = True
        end
        object cxGrid2DBCardView1VID_ARTIKAL_RODITEL: TcxGridDBCardViewRow
          DataBinding.FieldName = 'VID_ARTIKAL_RODITEL'
          Position.BeginsLayer = True
        end
        object cxGrid2DBCardView1ARTIKAL_RODITEL: TcxGridDBCardViewRow
          DataBinding.FieldName = 'ARTIKAL_RODITEL'
          Position.BeginsLayer = True
        end
        object cxGrid2DBCardView1POTEKLO: TcxGridDBCardViewRow
          DataBinding.FieldName = 'POTEKLO'
          Position.BeginsLayer = True
        end
        object cxGrid2DBCardView1DATUM_OD: TcxGridDBCardViewRow
          DataBinding.FieldName = 'DATUM_OD'
          Position.BeginsLayer = True
        end
        object cxGrid2DBCardView1DATUM_DO: TcxGridDBCardViewRow
          DataBinding.FieldName = 'DATUM_DO'
          Position.BeginsLayer = True
        end
        object cxGrid2DBCardView1KOREN: TcxGridDBCardViewRow
          DataBinding.FieldName = 'KOREN'
          Position.BeginsLayer = True
        end
        object cxGrid2DBCardView1NAZIV_ARTIKAL: TcxGridDBCardViewRow
          DataBinding.FieldName = 'NAZIV_ARTIKAL'
          Position.BeginsLayer = True
        end
        object cxGrid2DBCardView1NAZIV_ORG: TcxGridDBCardViewRow
          DataBinding.FieldName = 'NAZIV_ORG'
          Position.BeginsLayer = True
        end
      end
      object cxGrid2Level1: TcxGridLevel
        GridView = cxGrid2DBTableView1
      end
    end
    object orgRM: TdxDbOrgChart
      Left = 1
      Top = 1
      Width = 259
      Height = 12
      Hint = #1043#1088#1072#1092#1080#1095#1082#1080' '#1087#1088#1077#1075#1083#1077#1076' '#1085#1072' '#1085#1086#1088#1084#1072#1090#1080#1074#1086#1090
      ParentCustomHint = False
      KeyFieldName = 'ID'
      ParentFieldName = 'KOREN'
      TextFieldName = 'NAZIV_ARTIKAL'
      OrderFieldName = 'id_rmre'
      ImageFieldName = 'SLIKA'
      DefaultNodeWidth = 80
      DefaultNodeHeight = 50
      Options = [ocSelect, ocFocus, ocButtons, ocDblClick, ocEdit, ocCanDrag, ocShowDrag, ocInsDel, ocAnimate]
      EditMode = [emCenter, emVCenter, emWrap]
      Images = dmRes.cxSmallImages
      DefaultImageAlign = iaLT
      Rotated = True
      Align = alLeft
      Color = 16579061
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -9
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      ExplicitHeight = 213
    end
    object cxPageControl1: TcxPageControl
      Left = 592
      Top = 1
      Width = 431
      Height = 0
      Align = alRight
      Anchors = [akLeft, akTop, akRight, akBottom]
      TabOrder = 2
      Visible = False
      Properties.ActivePage = tsRevizija
      Properties.CustomButtons.Buttons = <>
      ExplicitHeight = 213
      ClientRectRight = 0
      ClientRectTop = 0
      object tsRevizija: TcxTabSheet
        Caption = #1056#1077#1074#1080#1079#1080#1112#1072
        ImageIndex = 1
        ExplicitTop = 24
        ExplicitWidth = 431
        ExplicitHeight = 189
        object PanelRevizija: TPanel
          Left = 0
          Top = 0
          Width = 0
          Height = 0
          Align = alClient
          TabOrder = 0
          ExplicitWidth = 431
          ExplicitHeight = 189
          object PanelRVnes: TPanel
            Left = 1
            Top = 1
            Width = 429
            Height = 76
            Align = alTop
            Enabled = False
            TabOrder = 0
            DesignSize = (
              429
              76)
            object cxLabel12: TcxLabel
              Left = 21
              Top = 14
              Caption = #1054#1087#1080#1089
              Style.TextColor = clRed
              Transparent = True
            end
            object txtNaziv: TcxDBTextEdit
              Tag = 1
              Left = 56
              Top = 12
              Hint = #1054#1087#1080#1089' '#1085#1072' '#1088#1077#1074#1080#1079#1080#1112#1072
              Anchors = [akLeft, akTop, akRight]
              DataBinding.DataField = 'NAZIV'
              DataBinding.DataSource = dm.dsNormativRevizija
              ParentFont = False
              Style.BorderStyle = ebsUltraFlat
              Style.Font.Charset = RUSSIAN_CHARSET
              Style.Font.Color = clWindowText
              Style.Font.Height = -11
              Style.Font.Name = 'Tahoma'
              Style.Font.Style = []
              Style.LookAndFeel.NativeStyle = False
              Style.LookAndFeel.SkinName = ''
              Style.TextStyle = []
              Style.TransparentBorder = False
              Style.IsFontAssigned = True
              StyleDisabled.LookAndFeel.NativeStyle = False
              StyleDisabled.LookAndFeel.SkinName = ''
              StyleFocused.LookAndFeel.NativeStyle = False
              StyleFocused.LookAndFeel.SkinName = ''
              StyleHot.LookAndFeel.NativeStyle = False
              StyleHot.LookAndFeel.SkinName = ''
              TabOrder = 0
              Width = 256
            end
            object txtDatum: TcxDBDateEdit
              Left = 56
              Top = 33
              Hint = #1044#1072#1090#1091#1084' '#1085#1072' '#1088#1077#1074#1080#1079#1080#1112#1072
              DataBinding.DataField = 'DATUM'
              DataBinding.DataSource = dm.dsNormativRevizija
              TabOrder = 1
              Width = 117
            end
            object cxLabel13: TcxLabel
              Left = 14
              Top = 37
              Caption = #1044#1072#1090#1091#1084
              Style.TextColor = clNavy
              Transparent = True
            end
            object cxButton1: TcxButton
              Left = 155
              Top = 45
              Width = 74
              Height = 20
              Action = aZapisiRevizija
              Anchors = [akRight, akBottom]
              Colors.Pressed = clGradientActiveCaption
              TabOrder = 4
            end
            object cxButton2: TcxButton
              Left = 236
              Top = 45
              Width = 74
              Height = 20
              Action = aOtkaziRevizija
              Anchors = [akRight, akBottom]
              Colors.Pressed = clGradientActiveCaption
              TabOrder = 5
            end
          end
          object PanelRGrid: TPanel
            Left = 1
            Top = 77
            Width = 429
            Height = 32
            Align = alClient
            TabOrder = 1
            ExplicitHeight = 111
            object cxGrid3: TcxGrid
              Left = 1
              Top = 1
              Width = 427
              Height = 30
              Align = alClient
              TabOrder = 0
              ExplicitHeight = 109
              object cxGrid3DBTableView1: TcxGridDBTableView
                Navigator.Buttons.CustomButtons = <>
                ScrollbarAnnotations.CustomAnnotations = <>
                DataController.Summary.DefaultGroupSummaryItems = <>
                DataController.Summary.FooterSummaryItems = <>
                DataController.Summary.SummaryGroups = <>
              end
              object cxGrid3DBBandedTableView1: TcxGridDBBandedTableView
                Navigator.Buttons.CustomButtons = <>
                ScrollbarAnnotations.CustomAnnotations = <>
                DataController.DataSource = dm.dsNormativRevizija
                DataController.Summary.DefaultGroupSummaryItems = <>
                DataController.Summary.FooterSummaryItems = <>
                DataController.Summary.SummaryGroups = <>
                Bands = <
                  item
                    Caption = #1056#1077#1074#1080#1079#1080#1112#1072
                    Width = 473
                  end>
                object cxGrid3DBBandedTableView1ID: TcxGridDBBandedColumn
                  Caption = #1064#1080#1092#1088#1072
                  DataBinding.FieldName = 'ID'
                  Visible = False
                  Position.BandIndex = 0
                  Position.ColIndex = 0
                  Position.RowIndex = 0
                end
                object cxGrid3DBBandedTableView1ID_NORMATIV: TcxGridDBBandedColumn
                  Caption = #1064#1080#1092#1088#1072' '#1085#1072' '#1085#1086#1088#1084#1072#1090#1080#1074
                  DataBinding.FieldName = 'ID_NORMATIV'
                  Visible = False
                  Position.BandIndex = 0
                  Position.ColIndex = 1
                  Position.RowIndex = 0
                end
                object cxGrid3DBBandedTableView1NAZIV: TcxGridDBBandedColumn
                  Caption = #1054#1087#1080#1089
                  DataBinding.FieldName = 'NAZIV'
                  Width = 380
                  Position.BandIndex = 0
                  Position.ColIndex = 2
                  Position.RowIndex = 0
                end
                object cxGrid3DBBandedTableView1DATUM: TcxGridDBBandedColumn
                  Caption = #1044#1072#1090#1091#1084
                  DataBinding.FieldName = 'DATUM'
                  Width = 93
                  Position.BandIndex = 0
                  Position.ColIndex = 3
                  Position.RowIndex = 0
                end
              end
              object cxGrid3Level1: TcxGridLevel
                GridView = cxGrid3DBBandedTableView1
              end
            end
          end
        end
      end
    end
    object cxGrid5: TcxGrid
      Left = -16
      Top = -17
      Width = 250
      Height = 200
      TabOrder = 3
      Visible = False
      object cxGrid5DBTableView1: TcxGridDBTableView
        Navigator.Buttons.CustomButtons = <>
        ScrollbarAnnotations.CustomAnnotations = <>
        DataController.DataSource = dm.dsNormativGrid
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        object cxGrid5DBTableView1ID: TcxGridDBColumn
          Caption = #1064#1080#1092#1088#1072
          DataBinding.FieldName = 'ID'
        end
        object cxGrid5DBTableView1VID_ARTIKAL: TcxGridDBColumn
          Caption = #1042#1080#1076' '#1085#1072' '#1072#1088#1090#1080#1082#1072#1083
          DataBinding.FieldName = 'VID_ARTIKAL'
        end
        object cxGrid5DBTableView1ARTIKAL: TcxGridDBColumn
          Caption = #1064#1080#1092#1088#1072' '#1085#1072'  '#1072#1088#1090#1080#1082#1072#1083
          DataBinding.FieldName = 'ARTIKAL'
        end
        object cxGrid5DBTableView1NAZIV_ARTIKAL: TcxGridDBColumn
          Caption = #1053#1072#1079#1080#1074' '
          DataBinding.FieldName = 'NAZIV_ARTIKAL'
          Width = 150
        end
        object cxGrid5DBTableView1MERKA_ARTIKAL: TcxGridDBColumn
          Caption = #1052#1077#1088#1082#1072
          DataBinding.FieldName = 'MERKA_ARTIKAL'
          Visible = False
        end
        object cxGrid5DBTableView1KOLICINA: TcxGridDBColumn
          Caption = #1050#1086#1083#1080#1095#1080#1085#1072
          DataBinding.FieldName = 'KOLICINA'
        end
        object cxGrid5DBTableView1MERKA: TcxGridDBColumn
          Caption = #1052#1077#1088#1082#1072
          DataBinding.FieldName = 'MERKA'
        end
      end
      object cxGrid5Level1: TcxGridLevel
        GridView = cxGrid5DBTableView1
      end
    end
  end
  object cxSplitter1: TcxSplitter [5]
    Left = 0
    Top = 710
    Width = 1024
    Height = 8
    HotZoneClassName = 'TcxMediaPlayer9Style'
    HotZone.SizePercent = 31
    AlignSplitter = salBottom
    Control = Panel1
    ExplicitTop = 495
  end
  inherited cxGridPopupMenu1: TcxGridPopupMenu
    Grid = cxGrid4
    Left = 48
    Top = 665
  end
  inherited PopupMenu1: TPopupMenu
    Left = 80
    Top = 697
    inherited N1: TMenuItem
      Visible = False
    end
    inherited Excel1: TMenuItem
      Visible = False
    end
    object N14: TMenuItem
      Caption = '-'
    end
  end
  inherited dxBarManager1: TdxBarManager
    Left = 80
    Top = 665
    PixelsPerInch = 96
    inherited dxBarManager1Bar1: TdxBar
      Caption = #1055#1088#1086#1080#1079#1074#1086#1076
      FloatClientWidth = 69
      FloatClientHeight = 162
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton25'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton1'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarLargeButton26'
        end>
    end
    inherited dxBarManager1Bar2: TdxBar
      DockedLeft = 574
      FloatClientWidth = 102
      FloatClientHeight = 166
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton10'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton11'
        end>
    end
    inherited dxBarManager1Bar3: TdxBar
      DockedLeft = 718
      FloatClientWidth = 59
      FloatClientHeight = 108
      UseOwnFont = True
    end
    inherited dxBarManager1Bar4: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 141
      FloatClientHeight = 221
    end
    inherited dxBarManager1BarTabela: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 174
      FloatClientHeight = 113
    end
    object dxBarManager1Bar5: TdxBar [5]
      Caption = #1057#1091#1088#1086#1074#1080#1085#1072
      CaptionButtons = <>
      DockedLeft = 220
      DockedTop = 0
      FloatLeft = 919
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton16'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton17'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton18'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar6: TdxBar [6]
      Caption = 'Custom 1'
      CaptionButtons = <>
      DockedDockingStyle = dsTop
      DockedLeft = 574
      DockedTop = 0
      FloatLeft = 1355
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton19'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton21'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton20'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = False
      WholeRow = False
    end
    object dxBarManager1Bar7: TdxBar [7]
      Caption = 'Custom 2'
      CaptionButtons = <>
      DockedLeft = 397
      DockedTop = 0
      FloatLeft = 1355
      FloatTop = 10
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton22'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton23'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton24'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarLargeButton10: TdxBarLargeButton
      Action = aAktiven
      Category = 0
      Visible = ivNever
    end
    object dxBarLargeButton11: TdxBarLargeButton
      Action = aNeaktiven
      Category = 0
      Visible = ivNever
    end
    object dxBarLargeButton16: TdxBarLargeButton
      Action = aNovaSurovina
      Caption = #1053#1086#1074#1072
      Category = 0
    end
    object dxBarLargeButton17: TdxBarLargeButton
      Action = aAzurirajSurovina
      Caption = #1040#1078#1091#1088#1080#1088#1072#1112
      Category = 0
    end
    object dxBarLargeButton18: TdxBarLargeButton
      Action = aBrisiSurovina
      Caption = #1041#1088#1080#1096#1080
      Category = 0
    end
    object dxBarLargeButton19: TdxBarLargeButton
      Action = aNovR
      Caption = #1053#1086#1074#1072
      Category = 0
    end
    object dxBarLargeButton20: TdxBarLargeButton
      Action = aBrisiR
      Caption = #1041#1088#1080#1096#1080
      Category = 0
    end
    object dxBarLargeButton21: TdxBarLargeButton
      Action = aAzurirajR
      Caption = #1040#1078#1091#1088#1080#1088#1072#1112
      Category = 0
    end
    object dxBarLargeButton22: TdxBarLargeButton
      Action = aNovaOperacija
      Caption = #1053#1086#1074#1072
      Category = 0
    end
    object dxBarLargeButton23: TdxBarLargeButton
      Action = aAzurirajOperacija
      Caption = #1040#1078#1091#1088#1080#1088#1072#1112
      Category = 0
    end
    object dxBarLargeButton24: TdxBarLargeButton
      Action = aBrisiOperacija
      Caption = #1041#1088#1080#1096#1080
      Category = 0
    end
    object dxBarLargeButton25: TdxBarLargeButton
      Action = aKopiraj
      Category = 0
      Hint = #1050#1086#1087#1080#1088#1072#1112' '#1087#1086#1089#1090#1086#1077#1095#1082#1080' '#1085#1086#1088#1084#1072#1090#1080#1074
    end
    object dxBarLargeButton26: TdxBarLargeButton
      Action = aSwitchView
      Category = 0
      Visible = ivNever
    end
    object dxBarLargeButton1: TdxBarLargeButton
      Action = aNov
      Category = 0
    end
  end
  inherited ActionList1: TActionList
    Left = 16
    Top = 665
    object aZapisiNormativ: TAction
      Caption = 'aZapisiNormativ'
      OnExecute = aZapisiNormativExecute
    end
    object aNovaSurovina: TAction
      Caption = #1053#1086#1074#1072' '#1089#1091#1088#1086#1074#1080#1085#1072
      ImageIndex = 10
      OnExecute = aNovaSurovinaExecute
    end
    object aAktiven: TAction
      Caption = #1040#1082#1090#1080#1074#1077#1085
      ImageIndex = 62
      OnExecute = aAktivenExecute
    end
    object aNeaktiven: TAction
      Caption = #1053#1077#1072#1082#1090#1080#1074#1077#1085
      ImageIndex = 61
      OnExecute = aNeaktivenExecute
    end
    object aAzurirajSurovina: TAction
      Caption = #1040#1078#1091#1088#1080#1088#1072#1112' '#1089#1091#1088#1086#1074#1080#1085#1072
      ImageIndex = 12
      OnExecute = aAzurirajSurovinaExecute
    end
    object aBrisiSurovina: TAction
      Caption = #1041#1088#1080#1096#1080' '#1089#1091#1088#1086#1074#1080#1085#1072
      ImageIndex = 13
      OnExecute = aBrisiSurovinaExecute
    end
    object aNovR: TAction
      Caption = #1053#1086#1074#1072' '#1088#1077#1074#1080#1079#1080#1112#1072
      ImageIndex = 10
      OnExecute = aNovRExecute
    end
    object aAzurirajR: TAction
      Caption = #1040#1078#1091#1088#1080#1088#1072#1112' '#1088#1077#1074#1080#1079#1080#1112#1072
      ImageIndex = 12
      OnExecute = aAzurirajRExecute
    end
    object aBrisiR: TAction
      Caption = #1041#1088#1080#1096#1080' '#1088#1077#1074#1080#1079#1080#1112#1072
      ImageIndex = 13
      OnExecute = aBrisiRExecute
    end
    object aZapisiRevizija: TAction
      Caption = #1047#1072#1087#1080#1096#1080
      ImageIndex = 6
      OnExecute = aZapisiRevizijaExecute
    end
    object aOtkaziRevizija: TAction
      Caption = #1054#1090#1082#1072#1078#1080
      ImageIndex = 21
      OnExecute = aOtkaziRevizijaExecute
    end
    object aVnesiNormativ: TAction
      Caption = #1042#1085#1077#1089#1080' '#1085#1086#1088#1084#1072#1090#1080#1074
      OnExecute = aVnesiNormativExecute
    end
    object aNovaOperacija: TAction
      Caption = #1053#1086#1074#1072' '#1086#1087#1077#1088#1072#1094#1080#1112#1072
      ImageIndex = 4
      OnExecute = aNovaOperacijaExecute
    end
    object aZapisiOperacija: TAction
      Caption = #1047#1072#1087#1080#1096#1080
      ImageIndex = 7
      OnExecute = aZapisiOperacijaExecute
    end
    object aOtkaziOperacija: TAction
      Caption = #1054#1090#1082#1072#1078#1080
      ImageIndex = 21
    end
    object aAzurirajOperacija: TAction
      Caption = #1040#1078#1091#1088#1080#1088#1072#1112' '#1086#1087#1077#1088#1072#1094#1080#1112#1072
      ImageIndex = 12
      OnExecute = aAzurirajOperacijaExecute
    end
    object aBrisiOperacija: TAction
      Caption = #1041#1088#1080#1096#1080' '#1086#1087#1077#1088#1072#1094#1080#1112#1072
      ImageIndex = 13
      OnExecute = aBrisiOperacijaExecute
    end
    object aArtikal: TAction
      Caption = #1040#1088#1090#1080#1082#1072#1083
      ShortCut = 45
      OnExecute = aArtikalExecute
    end
    object aKopiraj: TAction
      Caption = #1050#1086#1087#1080#1088#1072#1112' '#1087#1086#1089#1090#1086#1077#1095#1082#1080
      ImageIndex = 18
      OnExecute = aKopirajExecute
    end
    object aSwitchView: TAction
      Caption = #1057#1090#1077#1073#1083#1086'/'#1043#1088#1080#1076
      ImageIndex = 14
      OnExecute = aSwitchViewExecute
    end
    object aFilter: TAction
      Caption = 'aFilter'
      OnExecute = aFilterExecute
    end
  end
  inherited dxComponentPrinter1: TdxComponentPrinter
    Left = 144
    Top = 665
    PixelsPerInch = 96
    inherited dxComponentPrinter1Link1: TdxGridReportLink
      ReportDocument.CreationDate = 41316.640212766200000000
      Styles.BandHeader = nil
      Styles.Caption = nil
      Styles.CardCaptionRow = nil
      Styles.CardRowCaption = nil
      Styles.Content = nil
      Styles.ContentEven = nil
      Styles.ContentOdd = nil
      Styles.FilterBar = nil
      Styles.Footer = nil
      Styles.Group = nil
      Styles.Header = nil
      Styles.Preview = nil
      Styles.Selection = nil
      PixelsPerInch = 96
      BuiltInReportLink = True
    end
  end
  inherited dxBarScreenTipRepository1: TdxBarScreenTipRepository
    Left = 112
    Top = 665
    PixelsPerInch = 96
  end
  object qPoteklo: TpFIBQuery
    Transaction = dmKon.qTransaction
    Database = dmKon.fibBaza
    SQL.Strings = (
      'select poteklo '
      'from man_normativ n '
      'where n.id like :koren ')
    Left = 16
    Top = 697
    qoAutoCommit = True
    qoStartTransaction = True
  end
  object PopupMenu2: TPopupMenu
    Left = 48
    Top = 697
    object N2: TMenuItem
      Action = aVnesiNormativ
      Caption = #1042#1085#1077#1089#1080'/'#1086#1079#1085#1072#1095#1080' '#1085#1086#1088#1084#1072#1090#1080#1074
    end
    object N11: TMenuItem
      Caption = '-'
    end
    object N13: TMenuItem
      Action = aNov
      Caption = #1053#1086#1074' '#1085#1086#1088#1084#1072#1090#1080#1074
    end
    object N12: TMenuItem
      Action = aKopiraj
      Caption = #1050#1086#1087#1080#1088#1072#1112' '#1087#1086#1089#1090#1086#1077#1095#1082#1080' '#1085#1086#1088#1084#1072#1090#1080#1074
    end
    object N15: TMenuItem
      Caption = '-'
    end
    object N16: TMenuItem
      Action = aAzuriraj
      Caption = #1040#1078#1091#1088#1080#1088#1072#1112' '#1085#1086#1088#1084#1072#1090#1080#1074
    end
    object N17: TMenuItem
      Action = aBrisi
      Caption = #1041#1088#1080#1096#1080' '#1085#1086#1088#1084#1072#1090#1080#1074
    end
    object N4: TMenuItem
      Caption = '-'
    end
    object N3: TMenuItem
      Action = aNovaSurovina
    end
    object N5: TMenuItem
      Action = aAzurirajSurovina
    end
    object N6: TMenuItem
      Action = aBrisiSurovina
    end
    object N7: TMenuItem
      Caption = '-'
    end
    object N8: TMenuItem
      Action = aNovaOperacija
    end
    object N9: TMenuItem
      Action = aAzurirajOperacija
    end
    object N10: TMenuItem
      Action = aBrisiOperacija
    end
  end
end
