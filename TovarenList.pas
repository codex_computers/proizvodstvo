unit TovarenList;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData, cxEdit,
  DB, cxDBData, cxGridCustomPopupMenu, cxGridPopupMenu,
  ComCtrls, cxContainer, cxTextEdit, cxDBEdit, StdCtrls, cxGridLevel,
  cxClasses, cxControls, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGrid, ExtCtrls, Menus, ActnList,
  cxGridExportLink, cxExport, cxLookAndFeelPainters, cxDataStorage,
  cxButtons, dxStatusBar, dxRibbonStatusBar, dxRibbon, dxBar, dxBarExtItems,
  dxPSGlbl, dxPSUtl, dxPSEngn, dxPrnPg, dxBkgnd, dxWrap, dxPrnDev,
  dxPSCompsProvider, dxPSFillPatterns, dxPSEdgePatterns, dxPSCore, dxPScxCommon,
  cxPC, cxMemo, cxLocalization, cxLookAndFeels,
  dxPSPDFExportCore, dxPSPDFExport, cxDrawTextUtils, dxPSPrVwStd, dxPSPrVwAdv,
  dxPSPrVwRibbon, dxPScxEditorProducers, dxPScxExtEditorProducers,
  dxPScxPageControlProducer, dxPgsDlg, cxCheckBox, cxBarEditItem, dxSkinsCore,
  dxSkinOffice2007Blue, dxSkinscxPCPainter, dxSkinsdxRibbonPainter,
  dxSkinsdxBarPainter, dxBarSkinnedCustForm, dxSkinBlack, dxSkinBlue,
  dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom, dxSkinDarkSide, dxSkinFoggy,
  dxSkinGlassOceans, dxSkiniMaginary, dxSkinLilian, dxSkinLiquidSky,
  dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins, dxSkinOffice2007Black,
  dxSkinOffice2007Green, dxSkinOffice2007Pink, dxSkinOffice2007Silver,
  dxSkinPumpkin, dxSkinSeven, dxSkinSharp, dxSkinSilver, dxSkinSpringTime,
  dxSkinStardust, dxSkinSummer2008, dxSkinsDefaultPainters, dxSkinValentine,
  dxSkinXmas2008Blue, cxDropDownEdit, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxRibbonSkins, dxPScxGridLnk,
  dxPScxGridLayoutViewLnk, dxScreenTip, dxSkinBlueprint,
  dxSkinDevExpressDarkStyle, dxSkinDevExpressStyle, dxSkinHighContrast,
  dxSkinSevenClassic, dxSkinSharpPlus, dxSkinTheAsphaltWorld, dxSkinVS2010,
  dxSkinWhiteprint, dxPScxPivotGridLnk, dmResources, cxMaskEdit, cxCalendar,
  cxGroupBox, cxLookupEdit, cxDBLookupEdit, cxDBExtLookupComboBox, FIBQuery,
  pFIBQuery, Vcl.DBActns, Vcl.StdActns, Vcl.ExtActns, cxDBLookupComboBox;

type
//  niza = Array[1..5] of Variant;

  TfrmTovarenList = class(TForm)
    dPanel: TPanel;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    lPanel: TPanel;
    Label1: TLabel;
    Broj: TcxDBTextEdit;
    cxGridPopupMenu1: TcxGridPopupMenu;
    PopupMenu1: TPopupMenu;
    N1: TMenuItem;
    Excel1: TMenuItem;
    ZapisiButton: TcxButton;
    OtkaziButton: TcxButton;
    dxRibbon1Tab1: TdxRibbonTab;
    dxRibbon1: TdxRibbon;
    StatusBar1: TdxRibbonStatusBar;
    dxBarManager1: TdxBarManager;
    dxBarManager1Bar1: TdxBar;
    dxBarManager1Bar2: TdxBar;
    dxBarManager1Bar3: TdxBar;
    dxBarLargeButton2: TdxBarLargeButton;
    dxBarLargeButton3: TdxBarLargeButton;
    dxBarLargeButton4: TdxBarLargeButton;
    dxBarLargeButton5: TdxBarLargeButton;
    dxBarLargeButton6: TdxBarLargeButton;
    dxBarLargeButton7: TdxBarLargeButton;
    dxBarLargeButton8: TdxBarLargeButton;
    dxBarLargeButton1: TdxBarLargeButton;
    ActionList1: TActionList;
    aNov: TAction;
    aAzuriraj: TAction;
    aBrisi: TAction;
    aSnimiIzgled: TAction;
    aZacuvajExcel: TAction;
    aPecatiTabela: TAction;
    aHelp: TAction;
    aIzlez: TAction;
    aZapisi: TAction;
    aOtkazi: TAction;
    aRefresh: TAction;
    dxRibbon1Tab2: TdxRibbonTab;
    dxBarManager1Bar4: TdxBar;
    dxBarLargeButton9: TdxBarLargeButton;
    aSnimiPecatenje: TAction;
    dxComponentPrinter1: TdxComponentPrinter;
    dxComponentPrinter1Link1: TdxGridReportLink;
    ribbonFilterReset: TdxBarLargeButton;
    aPodesuvanjePecatenje: TAction;
    dxBarLargeButton12: TdxBarLargeButton;
    aBrisiPodesuvanjePecatenje: TAction;
    dxBarLargeButton13: TdxBarLargeButton;
    dxBarLargeButton14: TdxBarLargeButton;
    aPageSetup: TAction;
    dxBarLargeButton15: TdxBarLargeButton;
    dxBarSubItem1: TdxBarSubItem;
    aFormConfig: TAction;
    dxBarScreenTipRepository1: TdxBarScreenTipRepository;
    tipSnimiIzgled: TdxBarScreenTip;
    tipZacuvajExcel: TdxBarScreenTip;
    tipDizajnReport: TdxBarScreenTip;
    tipSetiranjeStrana: TdxBarScreenTip;
    tipSnimiKonf: TdxBarScreenTip;
    tipBrisiKonf: TdxBarScreenTip;
    cxBarEditItem1: TcxBarEditItem;
    dxBarManager1BarTabela: TdxBar;
    dxBarLBtnSnimiIzgled: TdxBarLargeButton;
    dxBarLBtnBrisiIzgled: TdxBarLargeButton;
    aBrisiIzgled: TAction;
    dxBarLargeButtonSoberi: TdxBarLargeButton;
    aSpustiSoberi: TAction;
    dxBarLargeButton10: TdxBarLargeButton;
    aEditStavka: TAction;
    cxGrid1DBTableView1ID: TcxGridDBColumn;
    cxGrid1DBTableView1RE: TcxGridDBColumn;
    cxGrid1DBTableView1BROJ: TcxGridDBColumn;
    cxGrid1DBTableView1GODINA: TcxGridDBColumn;
    cxGrid1DBTableView1DATUM: TcxGridDBColumn;
    cxGrid1DBTableView1TP: TcxGridDBColumn;
    cxGrid1DBTableView1P: TcxGridDBColumn;
    cxGrid1DBTableView1PARTNER_NAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1MESTO_NAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1PREVOZNIK: TcxGridDBColumn;
    cxGrid1DBTableView1PREVOZNIK_ADRESA: TcxGridDBColumn;
    cxGrid1DBTableView1PREVOZNIK_MESTO: TcxGridDBColumn;
    cxGrid1DBTableView1PREVOZNIK_REGISTRACIJA: TcxGridDBColumn;
    cxGrid1DBTableView1UTOVAR_MESTO: TcxGridDBColumn;
    cxGrid1DBTableView1UTOVAR_DATA: TcxGridDBColumn;
    cxGrid1DBTableView1ISTOVAR_MESTO: TcxGridDBColumn;
    cxGrid1DBTableView1ISTOVAR_DATA: TcxGridDBColumn;
    cxGrid1DBTableView1ZABELESKA: TcxGridDBColumn;
    cxGrid1DBTableView1INSTRUKCII: TcxGridDBColumn;
    cxGrid1DBTableView1STATUS_ID: TcxGridDBColumn;
    cxGrid1DBTableView1STATUS: TcxGridDBColumn;
    cxGrid1DBTableView1STATUS_NAZIV: TcxGridDBColumn;
    cxGrid1DBTableView1STATUS_BOJA: TcxGridDBColumn;
    cxGrid1DBTableView1CUSTOM1: TcxGridDBColumn;
    cxGrid1DBTableView1CUSTOM2: TcxGridDBColumn;
    cxGrid1DBTableView1CUSTOM3: TcxGridDBColumn;
    cxGrid1DBTableView1TS_INS: TcxGridDBColumn;
    cxGrid1DBTableView1TS_UPD: TcxGridDBColumn;
    cxGrid1DBTableView1USR_INS: TcxGridDBColumn;
    cxGrid1DBTableView1USR_UPD: TcxGridDBColumn;
    Label4: TLabel;
    cxGroupBoxPrevoznik: TcxGroupBox;
    Prevoznik: TcxDBTextEdit;
    Label2: TLabel;
    PrevoznikAdresa: TcxDBTextEdit;
    Label3: TLabel;
    PrevoznikRegistracija: TcxDBTextEdit;
    Label6: TLabel;
    Datum: TcxDBDateEdit;
    lblPartnerK: TLabel;
    TipPartner: TcxDBTextEdit;
    Partner: TcxDBTextEdit;
    cxExtPartner: TcxExtLookupComboBox;
    cxGroupBoxUtovar: TcxGroupBox;
    cxGroupBoxIstovar: TcxGroupBox;
    Label7: TLabel;
    UtovarMesto: TcxDBTextEdit;
    UtovarDatum: TcxDBDateEdit;
    Label8: TLabel;
    Label9: TLabel;
    IstovarMesto: TcxDBTextEdit;
    IstovarDatum: TcxDBDateEdit;
    Label10: TLabel;
    PrevoznikMesto: TcxDBTextEdit;
    Label11: TLabel;
    Label12: TLabel;
    Label13: TLabel;
    Zabeleska: TcxDBMemo;
    Instrukcii: TcxDBMemo;
    qryZemiBroj: TpFIBQuery;
    aResetNurkovci: TAction;
    dxBarLargeButton11: TdxBarLargeButton;
    aPecatiTL: TAction;
    aDizajnTL: TAction;
    Label5: TLabel;
    cxGrid1DBTableView1VOZILO: TcxGridDBColumn;
    cxGrid1DBTableView1VOZ_MODEL: TcxGridDBColumn;
    cxGrid1DBTableView1VOZ_REG_BR: TcxGridDBColumn;
    lcbVozilo: TcxDBLookupComboBox;

    procedure cxDBTextEditAllEnter(Sender: TObject);
    procedure cxDBTextEditAllExit(Sender: TObject);
    procedure EnterKakoTab(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    //procedure PrvPosledenTab(panel:TPanel;var posledna,prva:TWinControl);
    procedure SaveToIniFileExecute(Sender: TObject);
    procedure cxGrid1DBTableView1KeyPress(Sender: TObject; var Key: Char);
    procedure aZapisiExecute(Sender: TObject);
    procedure aOtkaziExecute(Sender: TObject);
    procedure aNovExecute(Sender: TObject);
    procedure aAzurirajExecute(Sender: TObject);
    procedure aBrisiExecute(Sender: TObject);
    procedure aZacuvajExcelExecute(Sender: TObject);
    procedure aRefreshExecute(Sender: TObject);
    procedure aSnimiIzgledExecute(Sender: TObject);
    procedure aIzlezExecute(Sender: TObject);
    procedure aPecatiTabelaExecute(Sender: TObject);
    procedure aSnimiPecatenjeExecute(Sender: TObject);
    procedure zacuvajPrintVoIni(ime:AnsiString);
    procedure procitajPrintOdIni(ime:AnsiString);
    procedure aPodesuvanjePecatenjeExecute(Sender: TObject);
    procedure aBrisiPodesuvanjePecatenjeExecute(Sender: TObject);
    procedure aPageSetupExecute(Sender: TObject);
    procedure cxGrid1DBTableView1FilterCustomization(
      Sender: TcxCustomGridTableView; var ADone: Boolean);
    procedure aFormConfigExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure cxBarEditItem1PropertiesChange(Sender: TObject);
    procedure aBrisiIzgledExecute(Sender: TObject);
    procedure aSpustiSoberiExecute(Sender: TObject);
    procedure aResetNurkovciExecute(Sender: TObject);
    procedure PartnerPropertiesChange(Sender: TObject);
    procedure cxExtPartnerExit(Sender: TObject);
    procedure SmeniNaziv();
    procedure dxBarLargeButton10Click(Sender: TObject);
    procedure aEditStavkaExecute(Sender: TObject);
    procedure aPecatiTLExecute(Sender: TObject);
    procedure aDizajnTLExecute(Sender: TObject);
    procedure cxGrid1DBTableView1DblClick(Sender: TObject);
    procedure cxGrid1DBTableView1KeyDown(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure lcbVoziloExit(Sender: TObject);
  private
    { Private declarations }

    _sifra_kluc :array[1..10] of string;  // �������� ���������� �� ������ �� ������� �� ������������� ���

  protected
    list : TList;
    inserting, sobrano : boolean;
    prva, posledna :TWinControl;
    procedure prefrli;

  public
    { Public declarations }
    constructor Create(Owner : TComponent; insert : boolean = true);reintroduce; overload;

    procedure SetSifra(br: integer; s : string);
    function GetSifra(br: integer) : string;

    //������� �� ���������� ������������ _sifra_kluc
    property sifra_kluc[br :integer]: string read GetSifra write SetSifra;
  end;

var
  frmTovarenList: TfrmTovarenList;
  rData : TRepositoryData;

implementation

uses DaNe, dmKonekcija, Utils, FormConfig, dmUnit, dmMaticni, StavkiTovarenList, Vozilo;

{$R *.dfm}
//------------------------------------------------------------------------------

constructor TfrmTovarenList.Create(Owner : TComponent; insert : boolean = true);
begin
inherited Create(Owner);
//	�������� ���� ������� � �������� �� ����������� ���������
//	insert = true -> ����� ���� ����� ���������� �� ������� �� Insert Mode
//  insert = false -> ����� ���� ����� ������� �� ����� �� ������. �� ������� ����� ����� �� �� �������� F5
  inserting := insert;
end;

//	����� �� �����������. �� ��������� ������� �� ������, �� �������� ������� �� ���� ��������,
//	�� ������� ������� �� ������ ������ � �� ������� �� Insert Mode
procedure TfrmTovarenList.aNovExecute(Sender: TObject);
begin
  if(cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) then
  begin
    dPanel.Enabled:=True;
    lPanel.Enabled:=False;
    prva.SetFocus;
    cxGrid1DBTableView1.DataController.DataSet.Insert;

    dm.tblTovarenListRE.Value := dmKon.re;
    dm.tblTovarenListGODINA.Value := dm.godina;
    dm.tblTovarenListDATUM.Value := Date;

    qryZemiBroj.Close;
    qryZemiBroj.ParamByName('god').Value := dm.godina;
    qryZemiBroj.ParamByName('re').Value := dmKon.re;
    qryZemiBroj.ExecQuery;

    dm.tblTovarenListBROJ.Value := qryZemiBroj.FieldValue('maks',false) + 1;

    if (not dmKon.viewFirmiMESTONAZIV.IsNull) then
    begin
      dm.tblTovarenListUTOVAR_MESTO.Value := dmKon.viewFirmiMESTONAZIV.Value;
      dm.tblTovarenListISTOVAR_MESTO.Value := dmKon.viewFirmiMESTONAZIV.Value;
    end;
    dm.tblTovarenListUTOVAR_DATA.Value := Datum.Date;
    dm.tblTovarenListISTOVAR_DATA.Value := Datum.Date;
  end
  else ShowMessage('������ �������� �� ��� �������� �� �� ��������� �������,' + sLineBreak + '� ����� ������� �� ������� ��� �����!');
end;

//	����� �� ���������. �� ��������� ������� �� ������, �� �������� ������� �� ���� ��������,
//	�� ������� ������� �� ������ ������ � �� ������� �� Edit Mode
procedure TfrmTovarenList.aAzurirajExecute(Sender: TObject);
begin
  if(cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) then
  begin
    dPanel.Enabled:=True;
    lPanel.Enabled:=False;
    prva.SetFocus;
    cxGrid1DBTableView1.DataController.DataSet.Edit;
  end
  else ShowMessage('������ �������� �� ��� �������� �� �� ��������� �������,' + sLineBreak + '� ����� ������� �� ���������!');
end;

//	����� �� ������ �� ������������� �����
procedure TfrmTovarenList.aBrisiExecute(Sender: TObject);
begin
  if ((dm.tblTovarenList.State = dsBrowse) and (dm.tblTovarenList.RecordCount <> 0)) then
  begin
    dm.tblStavkaTovarenList.Close;
    dm.tblStavkaTovarenList.Open;
    dm.tblStavkaTovarenList.FetchAll;

    if (dm.tblStavkaTovarenList.RecordCount > 0) then
    begin
      frmDaNe := TfrmDaNe.Create(self, '�������!', '��������� ���� ��� ' + IntToStr(dm.tblStavkaTovarenList.RecordCount) + ' ������. �������� �� ������?', 1);
      if (frmDaNe.ShowModal = mrYes) then
      begin
        dm.tblTovarenList.Delete();
      end
    end
    else
      dm.tblTovarenList.Delete();
  end;

end;

procedure TfrmTovarenList.aBrisiIzgledExecute(Sender: TObject);
begin
  brisiGridVoBaza(Name,cxGrid1DBTableView1);
//  brisiPivotVoBaza(Name,cxDBPivotGrid1); �� pivot ��� ���
  BrisiFormaIzgled(self);
end;

//	����� �� ���������� �� ����������
procedure TfrmTovarenList.aRefreshExecute(Sender: TObject);
begin
  cxGrid1DBTableView1.DataController.Filter.Root.Clear; //���������� �� ��������

  aResetNurkovci.Execute;
  dm.tblTovarenList.FullRefresh;
end;

procedure TfrmTovarenList.aResetNurkovciExecute(Sender: TObject);
begin
  //�������� �� ���������� �� ������ ����� ������ ��������� �� ������� �� ��������� �� sql-��
   dmRes.FreeRepository(rData);
   rData := TRepositoryData.Create();

   cxExtPartner.RepositoryItem := dmRes.InitRepositoryRefresh(-1, 'cxExtPartner', Name, rData);
end;

//	����� �� ����� �� ������� �� ������� �� ������� �����
procedure TfrmTovarenList.aIzlezExecute(Sender: TObject);
begin
  Close;
end;

//	����� �� ������� �� �������� �� ������ �� ���� (Utils.pas)
procedure TfrmTovarenList.aSnimiIzgledExecute(Sender: TObject);
begin
  zacuvajGridVoBaza(Name,cxGrid1DBTableView1);
  ZacuvajFormaIzgled(self);
end;

//	����� �� ������� �� ������ �� Excel ������ (Utils.pas)
procedure TfrmTovarenList.aZacuvajExcelExecute(Sender: TObject);
begin
  zacuvajVoExcel(cxGrid1, Caption);
end;

//  ��������� �� ������ �� ������� �� ���������� �� ����� �� Enter
procedure TfrmTovarenList.EnterKakoTab(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
    case Key of
        VK_DOWN:
        begin
          PostMessage(Handle,WM_NextDlgCtl,0,0);
          key:=0;
        end;
        VK_UP:
        begin
          PostMessage(Handle,WM_NextDlgCtl,1,0);
          key:=0;
        end;
        VK_RETURN:
        begin
          PostMessage(Handle,WM_NextDlgCtl,0,0);
        end;
    end;
end;

//	������� �� ��� �� ������ ��� �������� �� �����
procedure TfrmTovarenList.cxBarEditItem1PropertiesChange(Sender: TObject);
begin
  dmRes.SkinPromeni(Sender);
  dxRibbon1.ColorSchemeName := dmRes.skin_name;
end;

procedure TfrmTovarenList.cxDBTextEditAllEnter(Sender: TObject);
begin
    TEdit(Sender).Color:=clSkyBlue;
end;

//	������� �� ��� �� ������ ��� ����� �� �����
procedure TfrmTovarenList.cxDBTextEditAllExit(Sender: TObject);
begin
    TEdit(Sender).Color:=clWhite;
end;

procedure TfrmTovarenList.cxExtPartnerExit(Sender: TObject);
begin
 if cxGrid1DBTableView1.DataController.DataSet.State in [dsEdit,dsInsert] then
  begin
    SmeniNaziv();
  end;

end;

procedure TfrmTovarenList.SmeniNaziv();
begin
  if ((dm.tblTovarenList.State = dsEdit) or (dm.tblTovarenList.State = dsInsert)) then
  begin
    if cxExtPartner.Text <> '' then
    begin
      dm.tblTovarenListTP.Value := cxExtPartner.EditValue[0];
      dm.tblTovarenListP.Value := cxExtPartner.EditValue[1];
    end
    else
    begin
      dm.tblTovarenListTP.Clear;
      dm.tblTovarenListP.Clear;
    end;
  end;
end;

procedure TfrmTovarenList.cxGrid1DBTableView1DblClick(Sender: TObject);
begin
  if (dm.tblTovarenList.VisibleRecordCount > 0) then aEditStavka.Execute;
end;

procedure TfrmTovarenList.cxGrid1DBTableView1FilterCustomization(
  Sender: TcxCustomGridTableView; var ADone: Boolean);
begin
end;

//  ��������� �� ������ �� ��������� �� ���������� ���� �� ��������
//  ��������� ��: br - ����� ��� �� ������ (�� 0 �� 9) �� ��������� �� ����� �� ����� �� ���������� ����
//  s - ��������� �� ������ �� ������ (���������� �� string �� ����� �� integer �� IntToStr ���������)
procedure TfrmTovarenList.SetSifra(br: integer; s : string);
begin
  _sifra_kluc[br] := s;
end;

//  ������� �� ������ �� ��������� �� ���������� ���� �� ��������
//  ��������� ��: br - ����� ��� �� ������ (�� 0 �� 9) �� ��������� �� ����� �� ����� �� ���������� ����
function TfrmTovarenList.GetSifra(br: integer) : string;
begin
  Result := _sifra_kluc[br];
end;

procedure TfrmTovarenList.lcbVoziloExit(Sender: TObject);
begin
  if lcbVozilo.Text <> '' then
  begin
    dm.tblTovarenListPREVOZNIK.Value := dmKon.viewFirmiNAZIV.Value;
    if (not dmKon.viewFirmiADRESA.IsNull) then
      dm.tblTovarenListPREVOZNIK_ADRESA.Value := dmKon.viewFirmiADRESA.Value;
    if (not dmKon.viewFirmiMESTONAZIV.IsNull) then
      dm.tblTovarenListPREVOZNIK_MESTO.Value := dmKon.viewFirmiMESTONAZIV.Value;
    dm.tblTovarenListPREVOZNIK_REGISTRACIJA.Value := dmMat.tblVoziloREG_BROJ.Value;
  end;
end;

procedure TfrmTovarenList.PartnerPropertiesChange(Sender: TObject);
begin
  // smeni go nazivot na ext lookup-ot
  if (dm.tblTovarenList.State in [dsInsert,dsEdit]) then
  begin
    if ((TipPartner.Text <> '') and (Partner.Text <> '') and (IsStrANumber(TipPartner.Text)) and (IsStrANumber(Partner.Text)))  then
    begin
      cxExtPartner.EditValue := VarArrayOf([StrToInt(TipPartner.Text), StrToInt(Partner.Text)]);
    end
    else
    begin
      cxExtPartner.Clear;
    end;
  end
  else
  begin
      if ((not dm.tblTovarenListTP.IsNull) and (not dm.tblTovarenListP.isNull)) then
      begin
        cxExtPartner.EditValue := VarArrayOf([dm.tblTovarenListTP.Value, dm.tblTovarenListP.Value]);
      end
      else
      begin
        cxExtPartner.Clear;
      end;
  end;
end;

//  �� ������� �� ������� ��� ����������� �� Master-�� �� �������� �� ��������� �� ������ ����� SetSifra
procedure TfrmTovarenList.prefrli;
begin
end;

procedure TfrmTovarenList.FormClose(Sender: TObject; var Action: TCloseAction);
begin
 //	��������� ��� ��������� �� �������
    if (cxGrid1DBTableView1.DataController.DataSource.State = dsEdit) or (cxGrid1DBTableView1.DataController.DataSource.State = dsInsert) then
    begin
        frmDaNe := TfrmDaNe.Create(self, '���������� ��������', '���������� �� �� ��������. ���� ������ �� �� �������?', 1);
        if (frmDaNe.ShowModal <> mrYes) then
        begin
            cxGrid1DBTableView1.DataController.DataSet.Cancel;
            Action := caFree;
            dmRes.FreeRepository(rData);
        end
        else
          if (Validacija(dPanel) = false) then
          begin
            cxGrid1DBTableView1.DataController.DataSet.Post;
            Action := caFree;
            dmRes.FreeRepository(rData);
          end
          else Action := caNone;
    end
    else
      dmRes.FreeRepository(rData);
end;

procedure TfrmTovarenList.FormCreate(Sender: TObject);
begin
  ProcitajFormaIzgled(self);
  if (dmRes.use_skin = true) then dxRibbon1.ColorSchemeName := dmRes.skin_name;

  rData := TRepositoryData.Create();  // kreirame instance od klasata TRepositoryData sto se naogja vo dmRes
end;

//------------------------------------------------------------------------------

procedure TfrmTovarenList.FormShow(Sender: TObject);
begin
// ������� �� ������������ ������������/���������� �� ���������� �� ���� �����
    SpremiForma(self);
//	�� ��������� �� ������� ������ ��� ���������� � ���� � ��� �������� �� �������
    PrvPosledenTab(dPanel,posledna,prva);
//    dxBarManager1Bar1.Caption := Caption;
    dxComponentPrinter1Link1.ReportTitle.Text := Caption;

//	������� �� ������������ �� ������ �� ������
    procitajGridOdBaza(Name,cxGrid1DBTableView1,false,false);
//	������� �� ������������ �� ��������� �� ������
    procitajPrintOdBaza(Name,cxGrid1DBTableView1.Name, dxComponentPrinter1Link1);

    sobrano := true;

    cxExtPartner.RepositoryItem := dmRes.InitRepository(-1, 'cxExtPartner', Name, rData);

    dm.tblTovarenList.Close;
    dm.tblTovarenList.Open;
end;
//------------------------------------------------------------------------------

procedure TfrmTovarenList.SaveToIniFileExecute(Sender: TObject);
begin
    cxGrid1DBTableView1.StoreToIniFile(Name,True,[]);
end;
//------------------------------------------------------------------------------

procedure TfrmTovarenList.cxGrid1DBTableView1KeyDown(Sender: TObject;
  var Key: Word; Shift: TShiftState);
begin
  if ((Key = VK_RETURN) and (dm.tblTovarenList.VisibleRecordCount > 0)) then aEditStavka.Execute;
end;

procedure TfrmTovarenList.cxGrid1DBTableView1KeyPress(Sender: TObject;
  var Key: Char);
begin
  if(Ord(Key) <> VK_RETURN) then cxSortiraj(cxGrid1DBTableView1);
end;

procedure TfrmTovarenList.dxBarLargeButton10Click(Sender: TObject);
begin

end;

//  ����� �� �����
procedure TfrmTovarenList.aZapisiExecute(Sender: TObject);
var
  st: TDataSetState;
begin
  if(cxGrid1DBTableView1.DataController.DataSource.State <> dsBrowse) then
  begin
    ZapisiButton.SetFocus;

    st := cxGrid1DBTableView1.DataController.DataSet.State;
    if st in [dsEdit,dsInsert] then
    begin
      if (Validacija(dPanel) = false) then
      begin
        if ((st = dsInsert) and inserting) then
        begin
          cxGrid1DBTableView1.DataController.DataSet.Post;
          aNov.Execute;
        end;

        if ((st = dsInsert) and (not inserting)) then
        begin
          cxGrid1DBTableView1.DataController.DataSet.Post;
          dPanel.Enabled:=false;
          lPanel.Enabled:=true;
          cxGrid1.SetFocus;
        end;

        if (st = dsEdit) then
        begin
          cxGrid1DBTableView1.DataController.DataSet.Post;
          dPanel.Enabled:=false;
          lPanel.Enabled:=true;
          cxGrid1.SetFocus;
        end;
      end;
    end;
  end;
end;

//	����� �� ���������� �� �������
procedure TfrmTovarenList.aOtkaziExecute(Sender: TObject);
begin
  if (cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) then
  begin
      ModalResult := mrCancel;
      Close();
  end
  else
  begin
      cxGrid1DBTableView1.DataController.DataSet.Cancel;
      RestoreControls(dPanel);
      dPanel.Enabled := false;
      lPanel.Enabled := true;
      cxGrid1.SetFocus;
  end;
end;

//----------------------------------------------------------------------------------
// ��������� �� ��������, ���������� �� ��������� � �������� �� Print ��������
//----------------------------------------------------------------------------------

//	����� �� �������� �� �������� �� �������
procedure TfrmTovarenList.aPageSetupExecute(Sender: TObject);
begin
  dxComponentPrinter1Link1.PageSetup;
end;

//	����� �� ������� �� ������
procedure TfrmTovarenList.aPecatiTabelaExecute(Sender: TObject);
begin
  dxComponentPrinter1Link1.ReportTitle.Text := Caption;

  dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Clear;
  dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Add(dmkon.firma_naziv);
  dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Add(DateTimeToStr(Now));

  dxComponentPrinter1Link1.PrinterPage.PageHeader.LeftTitle.Clear;
  //dxComponentPrinter1Link1.PrinterPage.PageHeader.LeftTitle.Add('������� : ' + TipSifra.Text + '-' + IdSifra.Text + ' ' + Partner.Text);

  dxComponentPrinter1.Preview(true, dxComponentPrinter1Link1);
end;

procedure TfrmTovarenList.aPecatiTLExecute(Sender: TObject);
begin
  if (cxGrid1DBTableView1.DataController.RecordCount > 0) then
  begin
    try
      dmRes.Spremi('MTR',9007);
      dmKon.tblSqlReport.ParamByName('id').Value := dm.tblTovarenListID.Value;
      dmKon.tblSqlReport.Open;

      //dmRes.frxReport1.Variables.AddVariable('DATUM','DATA_OD', OdDatum.Date);
      //dmRes.frxReport1.Variables.AddVariable('DATUM','DATA_DO', DoDatum.Date);

      //dmRes.frxReport1.PrintOptions.ShowDialog := false;
      dmRes.frxReport1.ShowReport();
    except
      ShowMessage('�� ���� �� �� ������� ���������!');
    end;
  end;
end;

//	����� �� ���������� �� ��������� �� �������
procedure TfrmTovarenList.aPodesuvanjePecatenjeExecute(Sender: TObject);
begin
  dxComponentPrinter1Link1.DesignReport();
end;

procedure TfrmTovarenList.aSnimiPecatenjeExecute(Sender: TObject);
begin
  zacuvajPrintVoBaza(Name,cxGrid1DBTableView1.Name,dxComponentPrinter1Link1);
end;

//	����� �� �������� ��� �������� �� �������� �� ������ ���� ��� ��������� �� ����� ������
procedure TfrmTovarenList.aSpustiSoberiExecute(Sender: TObject);
begin
  if (sobrano = true) then
  begin
    cxGrid1DBTableView1.ViewData.Expand(false);
    sobrano := false;
  end
  else
  begin
    cxGrid1DBTableView1.ViewData.Collapse(false);
    sobrano := true;
  end;
end;

//	����� �� ������ �� ����������� �������� �� ������� (Utils.pas)
procedure TfrmTovarenList.aBrisiPodesuvanjePecatenjeExecute(Sender: TObject);
begin
  brisiPrintOdBaza(Name,cxGrid1DBTableView1.Name, dxComponentPrinter1Link1);
end;

procedure TfrmTovarenList.aDizajnTLExecute(Sender: TObject);
begin
 try
    dmRes.Spremi('MTR',9007);
    dmRes.frxReport1.DesignReport();
  except
    ShowMessage('�� ���� �� �� ������� ���������!');
  end;
end;

procedure TfrmTovarenList.aEditStavkaExecute(Sender: TObject);
begin
  frmStavkiTovarenList := TfrmStavkiTovarenList.Create(self, false);
  frmStavkiTovarenList.ShowModal;
  frmStavkiTovarenList.Free;
end;

//  ����� �� ����� �� ����� �� ������������� �� �������� � ��������������� ��
//  �������(������ ���������� ��������)
procedure TfrmTovarenList.aFormConfigExecute(Sender: TObject);
begin
  frmFormConfig := TfrmFormConfig.Create(Application);
  frmFormConfig.formPtr := Addr(Self);
  frmFormConfig.ShowModal;
  frmFormConfig.Free;
end;

//----------------------------------------------------------------------------------
// ���� -> ��������� �� ��������, ���������� �� ��������� � �������� �� Print ��������
//----------------------------------------------------------------------------------

// ��������� �� ���������� �� ��������� � �������� �� Print �������� �� .ini ��������
procedure TfrmTovarenList.zacuvajPrintVoIni(ime:AnsiString);
var
  AStream: TFileStream;
begin
  AStream := TFileStream.Create(dmKon.RabotenDir + ime + 'Print.ini', fmCreate);
  try
    AStream.WriteComponent(dxComponentPrinter1Link1);
  finally
    AStream.Free;
  end;

end;

// ��������� �� ������ �� ��������� � �������� �� Print �������� �������� �� .ini ��������
procedure TfrmTovarenList.procitajPrintOdIni(ime:AnsiString);
var
  AStream: TFileStream;
begin
  AStream := TFileStream.Create(dmKon.RabotenDir + ime + 'Print.ini', fmOpenRead);
  try
    dxComponentPrinter1Link1.RestoreDefaults;
    AStream.ReadComponent(dxComponentPrinter1Link1);
  finally
    AStream.Free;
  end;
end;

end.
