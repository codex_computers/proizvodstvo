object frmOutLots: TfrmOutLots
  Left = 128
  Top = 212
  ActiveControl = cxGrid1
  BorderIcons = [biSystemMenu, biMaximize]
  Caption = 'LOT '#1073#1088#1086#1077#1074#1080' '#1080' '#1082#1086#1083#1080#1095#1080#1085#1080' '#1079#1072' '#1072#1088#1090#1080#1082#1083#1086#1090
  ClientHeight = 553
  ClientWidth = 614
  Color = clBtnFace
  Constraints.MinHeight = 500
  Constraints.MinWidth = 610
  Font.Charset = RUSSIAN_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  ShowHint = True
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object lPanel: TPanel
    Left = 0
    Top = 126
    Width = 614
    Height = 298
    Align = alClient
    BevelInner = bvLowered
    BevelOuter = bvSpace
    TabOrder = 0
    object cxGrid1: TcxGrid
      Left = 2
      Top = 2
      Width = 610
      Height = 294
      Align = alClient
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      object cxGrid1DBTableView1: TcxGridDBTableView
        OnKeyPress = cxGrid1DBTableView1KeyPress
        Navigator.Buttons.CustomButtons = <>
        OnFilterCustomization = cxGrid1DBTableView1FilterCustomization
        DataController.DataSource = dsUpotrebeni
        DataController.Filter.Options = [fcoCaseInsensitive]
        DataController.Options = [dcoAnsiSort, dcoAssignGroupingValues, dcoAssignMasterDetailKeys, dcoSaveExpanding]
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <
          item
            Kind = skCount
            Column = cxGrid1DBTableView1ID
          end>
        DataController.Summary.SummaryGroups = <>
        Filtering.ColumnFilteredItemsList = True
        FilterRow.InfoText = #1050#1083#1080#1082#1085#1080' '#1086#1074#1076#1077' '#1079#1072' '#1076#1072' '#1076#1077#1092#1080#1085#1080#1088#1072#1096' '#1092#1080#1083#1090#1077#1088
        FilterRow.Visible = True
        FilterRow.ApplyChanges = fracImmediately
        OptionsBehavior.CellHints = True
        OptionsBehavior.IncSearch = True
        OptionsCustomize.ColumnsQuickCustomization = True
        OptionsData.Deleting = False
        OptionsData.Editing = False
        OptionsData.Inserting = False
        OptionsSelection.HideFocusRectOnExit = False
        OptionsView.Footer = True
        OptionsView.GroupFooters = gfVisibleWhenExpanded
        object cxGrid1DBTableView1ID: TcxGridDBColumn
          DataBinding.FieldName = 'ID'
        end
        object cxGrid1DBTableView1OUT_S_ID: TcxGridDBColumn
          DataBinding.FieldName = 'OUT_S_ID'
          Visible = False
          Width = 75
        end
        object cxGrid1DBTableView1LOT_NO: TcxGridDBColumn
          DataBinding.FieldName = 'LOT_NO'
          Width = 230
        end
        object cxGrid1DBTableView1KOLICINA: TcxGridDBColumn
          DataBinding.FieldName = 'KOLICINA'
          Width = 114
        end
        object cxGrid1DBTableView1VAZI_DO: TcxGridDBColumn
          DataBinding.FieldName = 'VAZI_DO'
          Width = 124
        end
        object cxGrid1DBTableView1CUSTOM1: TcxGridDBColumn
          DataBinding.FieldName = 'CUSTOM1'
          Visible = False
        end
        object cxGrid1DBTableView1CUSTOM2: TcxGridDBColumn
          DataBinding.FieldName = 'CUSTOM2'
          Visible = False
        end
        object cxGrid1DBTableView1CUSTOM3: TcxGridDBColumn
          DataBinding.FieldName = 'CUSTOM3'
          Visible = False
        end
        object cxGrid1DBTableView1TS_INS: TcxGridDBColumn
          DataBinding.FieldName = 'TS_INS'
          Visible = False
        end
        object cxGrid1DBTableView1TS_UPD: TcxGridDBColumn
          DataBinding.FieldName = 'TS_UPD'
          Visible = False
        end
        object cxGrid1DBTableView1USR_INS: TcxGridDBColumn
          DataBinding.FieldName = 'USR_INS'
          Visible = False
        end
        object cxGrid1DBTableView1USR_UPD: TcxGridDBColumn
          DataBinding.FieldName = 'USR_UPD'
          Visible = False
        end
      end
      object cxGrid1Level1: TcxGridLevel
        GridView = cxGrid1DBTableView1
      end
    end
  end
  object dPanel: TPanel
    Left = 0
    Top = 424
    Width = 614
    Height = 106
    Align = alBottom
    BevelInner = bvLowered
    BevelOuter = bvSpace
    Color = 15790320
    Enabled = False
    ParentBackground = False
    PopupMenu = PopupMenu1
    TabOrder = 1
    DesignSize = (
      614
      106)
    object Label1: TLabel
      Left = 22
      Top = 21
      Width = 55
      Height = 13
      Caption = 'LOT '#1073#1088#1086#1112' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label2: TLabel
      Left = 17
      Top = 48
      Width = 60
      Height = 13
      Caption = #1050#1086#1083#1080#1095#1080#1085#1072' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label3: TLabel
      Left = 21
      Top = 75
      Width = 56
      Height = 13
      Caption = #1042#1072#1078#1080' '#1076#1086' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clBlue
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Sifra: TcxDBTextEdit
      Tag = 1
      Left = 83
      Top = 18
      BeepOnEnter = False
      DataBinding.DataField = 'LOT_NO'
      DataBinding.DataSource = dsUpotrebeni
      ParentFont = False
      Properties.BeepOnError = True
      Style.Shadow = False
      TabOrder = 0
      OnEnter = SifraEnter
      OnExit = SifraExit
      OnKeyDown = EnterKakoTab
      Width = 318
    end
    object OtkaziButton: TcxButton
      Left = 523
      Top = 66
      Width = 75
      Height = 25
      Action = aOtkazi
      Anchors = [akRight, akBottom]
      Colors.Pressed = clGradientActiveCaption
      TabOrder = 4
    end
    object ZapisiButton: TcxButton
      Left = 442
      Top = 66
      Width = 75
      Height = 25
      Action = aZapisi
      Anchors = [akRight, akBottom]
      Colors.Pressed = clGradientActiveCaption
      TabOrder = 3
    end
    object Kolicina: TcxDBTextEdit
      Tag = 1
      Left = 83
      Top = 45
      BeepOnEnter = False
      DataBinding.DataField = 'KOLICINA'
      DataBinding.DataSource = dsUpotrebeni
      ParentFont = False
      Properties.BeepOnError = True
      Style.Shadow = False
      TabOrder = 1
      OnKeyDown = EnterKakoTab
      Width = 121
    end
    object DatumK: TcxDBDateEdit
      Left = 83
      Top = 72
      BeepOnEnter = False
      DataBinding.DataField = 'VAZI_DO'
      DataBinding.DataSource = dsUpotrebeni
      TabOrder = 2
      OnKeyDown = EnterKakoTab
      Width = 121
    end
  end
  object dxRibbon1: TdxRibbon
    Left = 0
    Top = 0
    Width = 614
    Height = 126
    BarManager = dxBarManager1
    ColorSchemeName = 'Blue'
    Contexts = <>
    TabOrder = 2
    TabStop = False
    object dxRibbon1Tab1: TdxRibbonTab
      Active = True
      Caption = #1052#1077#1085#1080
      Groups = <
        item
          Caption = 'LOT '#1073#1088#1086#1112
          ToolbarName = 'dxBarManager1Bar1'
        end
        item
          ToolbarName = 'dxBarManager1Bar2'
        end
        item
          ToolbarName = 'dxBarManager1Bar3'
        end>
      Index = 0
    end
    object dxRibbon1Tab2: TdxRibbonTab
      Caption = #1055#1086#1076#1077#1089#1091#1074#1072#1114#1072
      Groups = <
        item
          ToolbarName = 'dxBarManager1Bar4'
        end
        item
          ToolbarName = 'dxBarManager1BarTabela'
        end>
      Index = 1
    end
  end
  object StatusBar1: TdxRibbonStatusBar
    Left = 0
    Top = 530
    Width = 614
    Height = 23
    Panels = <
      item
        PanelStyleClassName = 'TdxStatusBarTextPanelStyle'
        Text = 
          ' F5 - '#1053#1086#1074', F6 - '#1040#1078#1091#1088#1080#1088#1072#1112', F7 - '#1054#1089#1074#1077#1078#1080', F8 - '#1041#1088#1080#1096#1080', F9 - '#1047#1072#1087#1080#1096#1080', ' +
          'Esc - '#1054#1090#1082#1072#1078#1080' / '#1048#1079#1083#1077#1079
      end>
    Ribbon = dxRibbon1
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clDefault
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
  end
  object cxGridPopupMenu1: TcxGridPopupMenu
    Grid = cxGrid1
    PopupMenus = <>
    Left = 248
    Top = 176
  end
  object PopupMenu1: TPopupMenu
    Left = 360
    Top = 192
    object N1: TMenuItem
      Caption = #1057#1085#1080#1084#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      ShortCut = 49235
      OnClick = SaveToIniFileExecute
    end
    object Excel1: TMenuItem
      Caption = #1057#1085#1080#1084#1080' '#1075#1086' '#1074#1086' Excel'
      ShortCut = 49221
    end
  end
  object dxBarManager1: TdxBarManager
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Segoe UI'
    Font.Style = []
    Categories.Strings = (
      'Default')
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    ImageOptions.Images = dmRes.cxSmallImages
    ImageOptions.LargeImages = dmRes.cxLargeImages
    PopupMenuLinks = <>
    UseSystemFont = True
    Left = 432
    Top = 208
    DockControlHeights = (
      0
      0
      0
      0)
    object dxBarManager1Bar1: TdxBar
      Caption = 'Custom 1'
      CaptionButtons = <>
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 715
      FloatTop = 157
      FloatClientWidth = 51
      FloatClientHeight = 120
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton10'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarLargeButton1'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton2'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarLargeButton3'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar2: TdxBar
      Caption = #1040#1082#1094#1080#1080
      CaptionButtons = <>
      DockedLeft = 249
      DockedTop = 0
      FloatLeft = 789
      FloatTop = 120
      FloatClientWidth = 51
      FloatClientHeight = 120
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton5'
        end
        item
          Visible = True
          ItemName = 'dxBarSubItem1'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar3: TdxBar
      Caption = #1048#1079#1083#1077#1079
      CaptionButtons = <>
      DockedLeft = 387
      DockedTop = 0
      FloatLeft = 718
      FloatTop = 62
      FloatClientWidth = 51
      FloatClientHeight = 80
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton7'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton8'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar4: TdxBar
      Caption = #1055#1077#1095#1072#1090#1077#1114#1077
      CaptionButtons = <>
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 676
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton12'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton15'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton9'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarLargeButton13'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1BarTabela: TdxBar
      Caption = #1058#1072#1073#1077#1083#1072
      CaptionButtons = <>
      DockedLeft = 363
      DockedTop = 0
      FloatLeft = 767
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLBtnSnimiIzgled'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarLBtnBrisiIzgled'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarLargeButton2: TdxBarLargeButton
      Action = aAzuriraj
      Category = 0
    end
    object dxBarLargeButton3: TdxBarLargeButton
      Action = aBrisi
      Category = 0
    end
    object dxBarLargeButton4: TdxBarLargeButton
      Action = aSnimiIzgled
      Category = 0
      ScreenTip = tipSnimiIzgled
    end
    object dxBarLargeButton5: TdxBarLargeButton
      Action = aZacuvajExcel
      Category = 0
      ScreenTip = tipZacuvajExcel
    end
    object dxBarLargeButton6: TdxBarLargeButton
      Action = aPecatiTabela
      Caption = #1055#1077#1095#1072#1090#1080' '#1090#1072#1073#1077#1083#1072
      Category = 0
    end
    object dxBarLargeButton7: TdxBarLargeButton
      Action = aHelp
      Category = 0
    end
    object dxBarLargeButton8: TdxBarLargeButton
      Action = aIzlez
      Category = 0
    end
    object dxBarLargeButton1: TdxBarLargeButton
      Action = aNov
      Category = 0
    end
    object dxBarLargeButton9: TdxBarLargeButton
      Action = aSnimiPecatenje
      Category = 0
      ScreenTip = tipSnimiKonf
    end
    object ribbonFilterReset: TdxBarLargeButton
      Caption = #1056#1077#1089#1077#1090#1080#1088#1072#1112' '#1092#1080#1083#1090#1077#1088
      Category = 0
      Hint = #1056#1077#1089#1077#1090#1080#1088#1072#1112' '#1092#1080#1083#1090#1077#1088
      Visible = ivAlways
    end
    object dxBarLargeButton12: TdxBarLargeButton
      Action = aPodesuvanjePecatenje
      Category = 0
      ScreenTip = tipDizajnReport
    end
    object dxBarLargeButton13: TdxBarLargeButton
      Action = aBrisiPodesuvanjePecatenje
      Category = 0
      ScreenTip = tipBrisiKonf
    end
    object dxBarLargeButton14: TdxBarLargeButton
      Caption = #1048#1079#1073#1088#1080#1096#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      Category = 0
      Visible = ivAlways
      LargeImageIndex = 40
    end
    object dxBarLargeButton15: TdxBarLargeButton
      Action = aPageSetup
      Category = 0
      ScreenTip = tipSetiranjeStrana
    end
    object dxBarSubItem1: TdxBarSubItem
      Caption = #1055#1077#1095#1072#1090#1077#1114#1077
      Category = 0
      Visible = ivAlways
      ImageIndex = 30
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton6'
        end>
    end
    object cxBarEditItem1: TcxBarEditItem
      Caption = 'Skin :'
      Category = 0
      Hint = 'Skin :'
      Visible = ivAlways
      Width = 160
      PropertiesClassName = 'TcxComboBoxProperties'
      Properties.OnChange = cxBarEditItem1PropertiesChange
    end
    object dxBarLBtnSnimiIzgled: TdxBarLargeButton
      Action = aSnimiIzgled
      Category = 0
    end
    object dxBarLBtnBrisiIzgled: TdxBarLargeButton
      Action = aBrisiIzgled
      Category = 0
    end
    object dxBarLargeButtonSoberi: TdxBarLargeButton
      Action = aSpustiSoberi
      Category = 0
    end
    object dxBarLargeButton10: TdxBarLargeButton
      Action = aOdberi
      Category = 0
    end
  end
  object ActionList1: TActionList
    Images = dmRes.cxSmallImages
    Left = 144
    Top = 184
    object aNov: TAction
      Caption = #1053#1086#1074
      ImageIndex = 10
      ShortCut = 116
      OnExecute = aNovExecute
    end
    object aAzuriraj: TAction
      Caption = #1040#1078#1091#1088#1080#1088#1072#1112
      ImageIndex = 12
      ShortCut = 117
      OnExecute = aAzurirajExecute
    end
    object aBrisi: TAction
      Caption = #1041#1088#1080#1096#1080
      ImageIndex = 11
      ShortCut = 119
      OnExecute = aBrisiExecute
    end
    object aSnimiIzgled: TAction
      Caption = #1057#1085#1080#1084#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      ImageIndex = 9
      ShortCut = 24659
      OnExecute = aSnimiIzgledExecute
    end
    object aZacuvajExcel: TAction
      Caption = #1047#1072#1095#1091#1074#1072#1112' '#1074#1086' Excel'
      ImageIndex = 9
      ShortCut = 24645
      OnExecute = aZacuvajExcelExecute
    end
    object aPecatiTabela: TAction
      Caption = #1055#1077#1095#1072#1090#1080
      ImageIndex = 30
      OnExecute = aPecatiTabelaExecute
    end
    object aHelp: TAction
      Caption = #1055#1086#1084#1086#1096
      ImageIndex = 24
    end
    object aIzlez: TAction
      Caption = #1048#1079#1083#1077#1079
      ImageIndex = 25
      OnExecute = aIzlezExecute
    end
    object aZapisi: TAction
      Caption = #1047#1072#1087#1080#1096#1080
      ImageIndex = 7
      ShortCut = 120
      OnExecute = aZapisiExecute
    end
    object aOtkazi: TAction
      Caption = #1054#1090#1082#1072#1078#1080
      ImageIndex = 21
      ShortCut = 27
      OnExecute = aOtkaziExecute
    end
    object aRefresh: TAction
      Caption = 'aRefresh'
      ImageIndex = 18
      ShortCut = 118
      OnExecute = aRefreshExecute
    end
    object aSnimiPecatenje: TAction
      Caption = #1057#1085#1080#1084#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      ImageIndex = 39
      OnExecute = aSnimiPecatenjeExecute
    end
    object aPodesuvanjePecatenje: TAction
      Caption = #1044#1080#1079#1072#1112#1085' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090
      ImageIndex = 38
      OnExecute = aPodesuvanjePecatenjeExecute
    end
    object aBrisiPodesuvanjePecatenje: TAction
      Caption = #1041#1088#1080#1096#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      ImageIndex = 37
      OnExecute = aBrisiPodesuvanjePecatenjeExecute
    end
    object aPageSetup: TAction
      Caption = #1057#1077#1090#1080#1088#1072#1114#1077' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072
      ImageIndex = 38
      OnExecute = aPageSetupExecute
    end
    object aFormConfig: TAction
      Caption = 'aFormConfig'
      ShortCut = 24699
      OnExecute = aFormConfigExecute
    end
    object aBrisiIzgled: TAction
      Caption = #1041#1088#1080#1096#1080' '#1075#1086' '#1079#1072#1095#1091#1074#1072#1085#1080#1086#1090' '#1080#1079#1075#1083#1077#1076
      ImageIndex = 56
      ShortCut = 24644
      OnExecute = aBrisiIzgledExecute
    end
    object aSpustiSoberi: TAction
      Caption = #1057#1087#1091#1096#1090#1080'/'#1057#1086#1073#1077#1088#1080' '#1058#1072#1073#1077#1083#1072
      ImageIndex = 65
      OnExecute = aSpustiSoberiExecute
    end
    object aOdberi: TAction
      Caption = #1054#1076#1073#1077#1088#1080' '#1086#1076' '#1074#1083#1077#1079
      ImageIndex = 16
      OnExecute = aOdberiExecute
    end
  end
  object dxComponentPrinter1: TdxComponentPrinter
    CurrentLink = dxComponentPrinter1Link1
    Version = 0
    Left = 64
    Top = 248
    object dxComponentPrinter1Link1: TdxGridReportLink
      Active = True
      Component = cxGrid1
      DateFormat = 0
      PageNumberFormat = pnfNumeral
      PrinterPage.DMPaper = 9
      PrinterPage.Footer = 6350
      PrinterPage.GrayShading = True
      PrinterPage.Header = 6350
      PrinterPage.Margins.Bottom = 12700
      PrinterPage.Margins.Left = 12700
      PrinterPage.Margins.Right = 12700
      PrinterPage.Margins.Top = 12700
      PrinterPage.Orientation = poLandscape
      PrinterPage.PageFooter.RightTitle.Strings = (
        #1057#1090#1088'.[Page #] '#1086#1076' [Total Pages]')
      PrinterPage.PageHeader.RightTitle.Strings = (
        '')
      PrinterPage.PageSize.X = 210000
      PrinterPage.PageSize.Y = 297000
      PrinterPage._dxMeasurementUnits_ = 0
      PrinterPage._dxLastMU_ = 2
      ReportDocument.CreationDate = 42613.593191712960000000
      TimeFormat = 0
      OptionsFormatting.UseNativeStyles = True
      OptionsOnEveryPage.FilterBar = False
      OptionsView.Caption = False
      OptionsView.FilterBar = False
      StyleRepository = dmRes.cxStyleRepositoryPrinting
      Styles.StyleSheet = dmRes.dxGridReportLinkStyleSheet
      BuiltInReportLink = True
    end
  end
  object dxBarScreenTipRepository1: TdxBarScreenTipRepository
    Left = 544
    Top = 184
    object tipSnimiIzgled: TdxBarScreenTip
      Header.Text = #1057#1085#1080#1084#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      Description.Text = #1047#1072#1095#1091#1074#1072#1112' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090' '#1085#1072' '#1092#1086#1088#1084#1072#1090#1072' '#1080' '#1085#1072' '#1090#1072#1073#1077#1083#1072#1090#1072
    end
    object tipZacuvajExcel: TdxBarScreenTip
      Header.Text = #1047#1072#1095#1091#1074#1072#1112' '#1074#1086' Excel'
      Description.Text = #1057#1085#1080#1084#1080' '#1075#1080' '#1087#1086#1076#1072#1090#1086#1094#1080#1090#1077' '#1086#1076' '#1090#1072#1073#1077#1083#1072#1090#1072' '#1074#1086' Excel '#1092#1086#1088#1084#1072#1090
    end
    object tipDizajnReport: TdxBarScreenTip
      Header.Text = #1044#1080#1079#1072#1112#1085' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090
      Description.Text = #1055#1086#1076#1077#1089#1091#1074#1072#1114#1072' '#1085#1072' '#1080#1079#1075#1083#1077#1076#1086#1090' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090' '#1085#1072' '#1090#1072#1073#1077#1083#1072#1090#1072
    end
    object tipSetiranjeStrana: TdxBarScreenTip
      Header.Text = #1057#1077#1090#1080#1088#1072#1114#1077' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072
      Description.Text = #1055#1086#1076#1077#1089#1091#1074#1072#1114#1077' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072' '#1085#1072' '#1082#1086#1112#1072' '#1116#1077' '#1089#1077' '#1087#1077#1095#1072#1090#1080' '#1090#1072#1073#1077#1083#1072#1090#1072
    end
    object tipSnimiKonf: TdxBarScreenTip
      Header.Text = #1057#1085#1080#1084#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      Description.Text = #1047#1072#1095#1091#1074#1072#1112' '#1075#1080' '#1087#1086#1076#1077#1089#1091#1074#1072#1114#1072#1090#1072' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090' '#1080' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072' '#1079#1072' '#1087#1077#1095#1072#1090#1077#1114#1077
    end
    object tipBrisiKonf: TdxBarScreenTip
      Header.Text = #1041#1088#1080#1096#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      Description.Text = #1048#1079#1073#1088#1080#1096#1080' '#1075#1080' '#1087#1086#1076#1077#1089#1091#1074#1072#1114#1072#1090#1072' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090' '#1080' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072' '#1079#1072' '#1087#1077#1095#1072#1090#1077#1114#1077
    end
  end
  object dsUpotrebeni: TDataSource
    DataSet = tblUpotrebeni
    Left = 176
    Top = 272
  end
  object tblUpotrebeni: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE MTR_OUT_S_LOT'
      'SET '
      '    ID = :ID,'
      '    OUT_S_ID = :OUT_S_ID,'
      '    LOT_NO = :LOT_NO,'
      '    KOLICINA = :KOLICINA,'
      '    VAZI_DO = :VAZI_DO,'
      '    CUSTOM1 = :CUSTOM1,'
      '    CUSTOM2 = :CUSTOM2,'
      '    CUSTOM3 = :CUSTOM3,'
      '    TS_INS = :TS_INS,'
      '    TS_UPD = :TS_UPD,'
      '    USR_INS = :USR_INS,'
      '    USR_UPD = :USR_UPD'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    MTR_OUT_S_LOT'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO MTR_OUT_S_LOT('
      '    ID,'
      '    OUT_S_ID,'
      '    LOT_NO,'
      '    KOLICINA,'
      '    VAZI_DO,'
      '    CUSTOM1,'
      '    CUSTOM2,'
      '    CUSTOM3,'
      '    TS_INS,'
      '    TS_UPD,'
      '    USR_INS,'
      '    USR_UPD'
      ')'
      'VALUES('
      '    :ID,'
      '    :OUT_S_ID,'
      '    :LOT_NO,'
      '    :KOLICINA,'
      '    :VAZI_DO,'
      '    :CUSTOM1,'
      '    :CUSTOM2,'
      '    :CUSTOM3,'
      '    :TS_INS,'
      '    :TS_UPD,'
      '    :USR_INS,'
      '    :USR_UPD'
      ')')
    RefreshSQL.Strings = (
      'select SL.ID,'
      '       SL.OUT_S_ID,'
      '       SL.LOT_NO,'
      '       SL.KOLICINA,'
      '       SL.VAZI_DO,'
      '       SL.CUSTOM1,'
      '       SL.CUSTOM2,'
      '       SL.CUSTOM3,'
      '       SL.TS_INS,'
      '       SL.TS_UPD,'
      '       SL.USR_INS,'
      '       SL.USR_UPD'
      'from MTR_OUT_S_LOT SL'
      'where(  SL.OUT_S_ID = :IN_ID'
      '     ) and (     SL.ID = :OLD_ID'
      '     )'
      '      ')
    SelectSQL.Strings = (
      'select SL.ID,'
      '       SL.OUT_S_ID,'
      '       SL.LOT_NO,'
      '       SL.KOLICINA,'
      '       SL.VAZI_DO,'
      '       SL.CUSTOM1,'
      '       SL.CUSTOM2,'
      '       SL.CUSTOM3,'
      '       SL.TS_INS,'
      '       SL.TS_UPD,'
      '       SL.USR_INS,'
      '       SL.USR_UPD'
      'from MTR_OUT_S_LOT SL'
      'where SL.OUT_S_ID = :IN_ID  ')
    AfterInsert = tblUpotrebeniAfterInsert
    BeforeDelete = TabelaBeforeDelete
    BeforeOpen = tblUpotrebeniBeforeOpen
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 256
    Top = 272
    poSQLINT64ToBCD = True
    object tblUpotrebeniID: TFIBBCDField
      DisplayLabel = #1064#1080#1092'.'
      FieldName = 'ID'
      Size = 0
    end
    object tblUpotrebeniOUT_S_ID: TFIBBCDField
      DisplayLabel = #1057#1090#1072#1074#1082#1072' '#1096#1080#1092'.'
      FieldName = 'OUT_S_ID'
      Size = 0
    end
    object tblUpotrebeniLOT_NO: TFIBStringField
      DisplayLabel = 'LOT '#1073#1088'.'
      FieldName = 'LOT_NO'
      Size = 30
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblUpotrebeniKOLICINA: TFIBFloatField
      DisplayLabel = #1050#1086#1083#1080#1095#1080#1085#1072
      FieldName = 'KOLICINA'
      DisplayFormat = '###,###0.000'
      EditFormat = '0.000'
    end
    object tblUpotrebeniVAZI_DO: TFIBDateField
      DisplayLabel = #1042#1072#1078#1080' '#1076#1086
      FieldName = 'VAZI_DO'
    end
    object tblUpotrebeniCUSTOM1: TFIBStringField
      FieldName = 'CUSTOM1'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblUpotrebeniCUSTOM2: TFIBStringField
      FieldName = 'CUSTOM2'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblUpotrebeniCUSTOM3: TFIBStringField
      FieldName = 'CUSTOM3'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblUpotrebeniTS_INS: TFIBDateTimeField
      FieldName = 'TS_INS'
    end
    object tblUpotrebeniTS_UPD: TFIBDateTimeField
      FieldName = 'TS_UPD'
    end
    object tblUpotrebeniUSR_INS: TFIBStringField
      FieldName = 'USR_INS'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblUpotrebeniUSR_UPD: TFIBStringField
      FieldName = 'USR_UPD'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object tblOutStavka: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE MTR_OUT_S'
      'SET '
      '    ID = :ID,'
      '    MTR_OUT_ID = :MTR_OUT_ID,'
      '    ART_VID = :ART_VID,'
      '    ART_SIF = :ART_SIF,'
      '    KOLICINA = :KOLICINA,'
      '    SLEDLIVOST_KOLICINA = :SLEDLIVOST_KOLICINA,'
      '    PAKETI = :PAKETI,'
      '    IZV_EM = :IZV_EM,'
      '    BR_PAKET = :BR_PAKET,'
      '    CENA_SO_DDV = :CENA_SO_DDV,'
      '    CENA_BEZ_DDV = :CENA_BEZ_DDV,'
      '    DANOK_PR = :DANOK_PR,'
      '    RABAT_PR = :RABAT_PR,'
      '    DANOK_IZNOS = :DANOK_IZNOS,'
      '    RABAT_IZNOS = :RABAT_IZNOS,'
      '    IZNOS_SO_DDV = :IZNOS_SO_DDV,'
      '    IZNOS_BEZ_DDV = :IZNOS_BEZ_DDV,'
      '    KNIG_CENA = :KNIG_CENA,'
      '    KNIG_IZNOS = :KNIG_IZNOS,'
      '    CENA_BEZ_RABAT_SO_DDV = :CENA_BEZ_RABAT_SO_DDV,'
      '    CENA_BEZ_RABAT_BEZ_DDV = :CENA_BEZ_RABAT_BEZ_DDV,'
      '    VAZI_DO = :VAZI_DO,'
      '    LOT_ID = :LOT_ID,'
      '    LOT_NO = :LOT_NO,'
      '    REF_ID = :REF_ID,'
      '    OPIS = :OPIS,'
      '    CUSTOM1 = :CUSTOM1,'
      '    CUSTOM2 = :CUSTOM2,'
      '    CUSTOM3 = :CUSTOM3,'
      '    TS_INS = :TS_INS,'
      '    TS_UPD = :TS_UPD,'
      '    USR_INS = :USR_INS,'
      '    USR_UPD = :USR_UPD'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    MTR_OUT_S'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO MTR_OUT_S('
      '    ID,'
      '    MTR_OUT_ID,'
      '    ART_VID,'
      '    ART_SIF,'
      '    KOLICINA,'
      '    SLEDLIVOST_KOLICINA,'
      '    PAKETI,'
      '    IZV_EM,'
      '    BR_PAKET,'
      '    CENA_SO_DDV,'
      '    CENA_BEZ_DDV,'
      '    DANOK_PR,'
      '    RABAT_PR,'
      '    DANOK_IZNOS,'
      '    RABAT_IZNOS,'
      '    IZNOS_SO_DDV,'
      '    IZNOS_BEZ_DDV,'
      '    KNIG_CENA,'
      '    KNIG_IZNOS,'
      '    CENA_BEZ_RABAT_SO_DDV,'
      '    CENA_BEZ_RABAT_BEZ_DDV,'
      '    VAZI_DO,'
      '    LOT_ID,'
      '    LOT_NO,'
      '    REF_ID,'
      '    OPIS,'
      '    CUSTOM1,'
      '    CUSTOM2,'
      '    CUSTOM3,'
      '    TS_INS,'
      '    TS_UPD,'
      '    USR_INS,'
      '    USR_UPD'
      ')'
      'VALUES('
      '    :ID,'
      '    :MTR_OUT_ID,'
      '    :ART_VID,'
      '    :ART_SIF,'
      '    :KOLICINA,'
      '    :SLEDLIVOST_KOLICINA,'
      '    :PAKETI,'
      '    :IZV_EM,'
      '    :BR_PAKET,'
      '    :CENA_SO_DDV,'
      '    :CENA_BEZ_DDV,'
      '    :DANOK_PR,'
      '    :RABAT_PR,'
      '    :DANOK_IZNOS,'
      '    :RABAT_IZNOS,'
      '    :IZNOS_SO_DDV,'
      '    :IZNOS_BEZ_DDV,'
      '    :KNIG_CENA,'
      '    :KNIG_IZNOS,'
      '    :CENA_BEZ_RABAT_SO_DDV,'
      '    :CENA_BEZ_RABAT_BEZ_DDV,'
      '    :VAZI_DO,'
      '    :LOT_ID,'
      '    :LOT_NO,'
      '    :REF_ID,'
      '    :OPIS,'
      '    :CUSTOM1,'
      '    :CUSTOM2,'
      '    :CUSTOM3,'
      '    :TS_INS,'
      '    :TS_UPD,'
      '    :USR_INS,'
      '    :USR_UPD'
      ')')
    RefreshSQL.Strings = (
      'select S.ID,'
      '       S.MTR_OUT_ID,'
      '       S.ART_VID,'
      '       S.ART_SIF,'
      '       A.NAZIV as ART_NAZIV,'
      '       S.KOLICINA,'
      '       S.SLEDLIVOST_KOLICINA,'
      '       A.MERKA as ART_MERKA,'
      '       A.KATALOG as ART_KATALOG,'
      '       A.BARKOD as ART_BARKOD,'
      '       A.SKU as ART_SKU,'
      '       S.PAKETI,'
      '       S.IZV_EM,'
      '       IE.IZVEDENA_EM,'
      '       S.BR_PAKET,'
      '       S.CENA_SO_DDV,'
      '       S.CENA_BEZ_DDV,'
      '       S.DANOK_PR,'
      '       S.RABAT_PR,'
      '       S.DANOK_IZNOS,'
      '       S.RABAT_IZNOS,'
      '       S.IZNOS_SO_DDV,'
      '       S.IZNOS_BEZ_DDV,'
      '       S.KNIG_CENA,'
      '       S.KNIG_IZNOS,'
      '       S.CENA_BEZ_RABAT_SO_DDV,'
      '       S.CENA_BEZ_RABAT_BEZ_DDV,'
      '       S.VAZI_DO,'
      '       S.LOT_ID,'
      '       S.LOT_NO,'
      '       S.REF_ID,'
      '       S.OPIS,'
      '       S.CUSTOM1,'
      '       S.CUSTOM2,'
      '       S.CUSTOM3,'
      '       S.TS_INS,'
      '       S.TS_UPD,'
      '       S.USR_INS,'
      '       S.USR_UPD'
      'from MTR_OUT_S S'
      'join MTR_ARTIKAL A on A.ARTVID = S.ART_VID and A.ID = S.ART_SIF'
      'left join MTR_IZVEDENA_EM IE on IE.ID = S.IZV_EM'
      'where(  S.MTR_OUT_ID = :MAS_ID'
      '     ) and (     S.ID = :OLD_ID'
      '     )'
      '       ')
    SelectSQL.Strings = (
      'select S.ID,'
      '       S.MTR_OUT_ID,'
      '       S.ART_VID,'
      '       S.ART_SIF,'
      '       A.NAZIV as ART_NAZIV,'
      '       S.KOLICINA,'
      '       S.SLEDLIVOST_KOLICINA,'
      '       A.MERKA as ART_MERKA,'
      '       A.KATALOG as ART_KATALOG,'
      '       A.BARKOD as ART_BARKOD,'
      '       A.SKU as ART_SKU,'
      '       S.PAKETI,'
      '       S.IZV_EM,'
      '       IE.IZVEDENA_EM,'
      '       S.BR_PAKET,'
      '       S.CENA_SO_DDV,'
      '       S.CENA_BEZ_DDV,'
      '       S.DANOK_PR,'
      '       S.RABAT_PR,'
      '       S.DANOK_IZNOS,'
      '       S.RABAT_IZNOS,'
      '       S.IZNOS_SO_DDV,'
      '       S.IZNOS_BEZ_DDV,'
      '       S.KNIG_CENA,'
      '       S.KNIG_IZNOS,'
      '       S.CENA_BEZ_RABAT_SO_DDV,'
      '       S.CENA_BEZ_RABAT_BEZ_DDV,'
      '       S.VAZI_DO,'
      '       S.LOT_ID,'
      '       S.LOT_NO,'
      '       S.REF_ID,'
      '       S.OPIS,'
      '       S.CUSTOM1,'
      '       S.CUSTOM2,'
      '       S.CUSTOM3,'
      '       S.TS_INS,'
      '       S.TS_UPD,'
      '       S.USR_INS,'
      '       S.USR_UPD'
      'from MTR_OUT_S S'
      'join MTR_ARTIKAL A on A.ARTVID = S.ART_VID and A.ID = S.ART_SIF'
      'left join MTR_IZVEDENA_EM IE on IE.ID = S.IZV_EM'
      'where S.MTR_OUT_ID = :MAS_ID   ')
    AutoUpdateOptions.UpdateTableName = 'MTR_OUT_S'
    AutoUpdateOptions.KeyFields = 'ID'
    AutoUpdateOptions.GeneratorName = 'GEN_MTR_OUT_S_ID'
    AutoUpdateOptions.WhenGetGenID = wgBeforePost
    BeforeDelete = TabelaBeforeDelete
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    DataSource = dsOut
    Left = 472
    Top = 80
    poSQLINT64ToBCD = True
    object tblOutStavkaID: TFIBBCDField
      DisplayLabel = #1064#1080#1092'.'
      FieldName = 'ID'
      Size = 0
    end
    object tblOutStavkaMTR_OUT_ID: TFIBBCDField
      DisplayLabel = #1048#1089#1087'. '#1096#1080#1092'.'
      FieldName = 'MTR_OUT_ID'
      Size = 0
    end
    object tblOutStavkaART_VID: TFIBIntegerField
      DisplayLabel = #1040#1088#1090'. '#1074#1080#1076
      FieldName = 'ART_VID'
    end
    object tblOutStavkaART_SIF: TFIBIntegerField
      DisplayLabel = #1040#1088#1090'. '#1096#1080#1092'.'
      FieldName = 'ART_SIF'
    end
    object tblOutStavkaART_NAZIV: TFIBStringField
      DisplayLabel = #1040#1088#1090'. '#1085#1072#1079#1080#1074
      FieldName = 'ART_NAZIV'
      Size = 500
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblOutStavkaKOLICINA: TFIBFloatField
      DisplayLabel = #1050#1086#1083#1080#1095#1080#1085#1072
      FieldName = 'KOLICINA'
      DisplayFormat = '###,###0.000'
      EditFormat = '0.000'
    end
    object tblOutStavkaPAKETI: TFIBIntegerField
      DisplayLabel = #1055#1072#1082#1077#1090#1080
      FieldName = 'PAKETI'
    end
    object tblOutStavkaIZV_EM: TFIBIntegerField
      DisplayLabel = #1048'.'#1077#1076'.'#1084#1077#1088#1082#1072' '#1096#1080#1092'.'
      FieldName = 'IZV_EM'
    end
    object tblOutStavkaIZVEDENA_EM: TFIBStringField
      DisplayLabel = #1048#1079#1074#1077#1076#1077#1085#1072' '#1077#1076'. '#1084#1077#1088#1082#1072
      FieldName = 'IZVEDENA_EM'
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblOutStavkaBR_PAKET: TFIBBCDField
      DisplayLabel = #1041#1088'. '#1074#1086' '#1087#1072#1082#1077#1090
      FieldName = 'BR_PAKET'
      Size = 8
    end
    object tblOutStavkaCENA_SO_DDV: TFIBFloatField
      DisplayLabel = #1062#1077#1085#1072' '#1089#1086' '#1044#1044#1042
      FieldName = 'CENA_SO_DDV'
      DisplayFormat = '###,##0.00'
      EditFormat = '0.00'
    end
    object tblOutStavkaCENA_BEZ_DDV: TFIBFloatField
      DisplayLabel = #1062#1077#1085#1072' '#1073#1077#1079' '#1044#1044#1042
      FieldName = 'CENA_BEZ_DDV'
      DisplayFormat = '###,##0.00'
      EditFormat = '0.00'
    end
    object tblOutStavkaDANOK_PR: TFIBBCDField
      DisplayLabel = #1055#1088#1086#1094'. '#1085#1072' '#1076#1072#1085#1086#1082
      FieldName = 'DANOK_PR'
      Size = 2
    end
    object tblOutStavkaRABAT_PR: TFIBBCDField
      DisplayLabel = #1055#1088#1086#1094'. '#1084#1072' '#1088#1072#1073#1072#1090
      FieldName = 'RABAT_PR'
      Size = 2
    end
    object tblOutStavkaKNIG_CENA: TFIBFloatField
      DisplayLabel = #1050#1085#1080#1075'. '#1094#1077#1085#1072
      FieldName = 'KNIG_CENA'
      DisplayFormat = '###,##0.00'
      EditFormat = '0.00'
    end
    object tblOutStavkaKNIG_IZNOS: TFIBBCDField
      DisplayLabel = #1050#1085#1080#1075'. '#1080#1079#1085#1086#1089
      FieldName = 'KNIG_IZNOS'
      Size = 2
    end
    object tblOutStavkaIZNOS_BEZ_DDV: TFIBBCDField
      DisplayLabel = #1048#1079#1085#1086#1089' '#1073#1077#1079' '#1044#1044#1042
      FieldName = 'IZNOS_BEZ_DDV'
      Size = 2
    end
    object tblOutStavkaIZNOS_SO_DDV: TFIBBCDField
      DisplayLabel = #1048#1079#1085#1086#1089' '#1089#1086' '#1044#1044#1042
      FieldName = 'IZNOS_SO_DDV'
      Size = 2
    end
    object tblOutStavkaRABAT_IZNOS: TFIBBCDField
      DisplayLabel = #1048#1079#1085#1086#1089' '#1085#1072' '#1088#1072#1073#1072#1090
      FieldName = 'RABAT_IZNOS'
      Size = 2
    end
    object tblOutStavkaDANOK_IZNOS: TFIBBCDField
      DisplayLabel = #1048#1079#1085#1086#1089' '#1085#1072' '#1044#1044#1042
      FieldName = 'DANOK_IZNOS'
      Size = 2
    end
    object tblOutStavkaLOT_NO: TFIBStringField
      DisplayLabel = 'LOT '#1073#1088'.'
      FieldName = 'LOT_NO'
      Size = 30
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblOutStavkaVAZI_DO: TFIBDateField
      DisplayLabel = #1042#1072#1078#1080' '#1076#1086
      FieldName = 'VAZI_DO'
    end
    object tblOutStavkaTS_INS: TFIBDateTimeField
      FieldName = 'TS_INS'
    end
    object tblOutStavkaTS_UPD: TFIBDateTimeField
      FieldName = 'TS_UPD'
    end
    object tblOutStavkaUSR_INS: TFIBStringField
      FieldName = 'USR_INS'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblOutStavkaUSR_UPD: TFIBStringField
      FieldName = 'USR_UPD'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblOutStavkaCUSTOM1: TFIBStringField
      FieldName = 'CUSTOM1'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblOutStavkaCUSTOM2: TFIBStringField
      FieldName = 'CUSTOM2'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblOutStavkaCUSTOM3: TFIBStringField
      FieldName = 'CUSTOM3'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblOutStavkaREF_ID: TFIBBCDField
      FieldName = 'REF_ID'
      Size = 0
    end
    object tblOutStavkaOPIS: TFIBStringField
      DisplayLabel = #1054#1087#1080#1089
      FieldName = 'OPIS'
      Size = 200
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblOutStavkaCALC_DANOK_IZNOS: TIBBCDField
      FieldKind = fkCalculated
      FieldName = 'CALC_DANOK_IZNOS'
      Size = 2
      Calculated = True
    end
    object tblOutStavkaART_MERKA: TFIBStringField
      DisplayLabel = #1040#1088#1090'. '#1084#1077#1088#1082#1072
      FieldName = 'ART_MERKA'
      Size = 5
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblOutStavkaCENA_BEZ_RABAT_SO_DDV: TFIBFloatField
      DisplayLabel = #1062#1077#1085#1072' '#1073#1077#1079' '#1088#1072#1073#1072#1090' '#1089#1086' '#1044#1044#1042
      FieldName = 'CENA_BEZ_RABAT_SO_DDV'
      DisplayFormat = '###,##0.00'
      EditFormat = '0.00'
    end
    object tblOutStavkaCENA_BEZ_RABAT_BEZ_DDV: TFIBFloatField
      DisplayLabel = #1062#1077#1085#1072' '#1073#1077#1079' '#1088#1072#1073#1072#1090' '#1073#1077#1079' '#1044#1044#1042
      FieldName = 'CENA_BEZ_RABAT_BEZ_DDV'
      DisplayFormat = '###,##0.00'
      EditFormat = '0.00'
    end
    object tblOutStavkaLOT_ID: TFIBBCDField
      DisplayLabel = 'LOT '#1096#1080#1092'.'
      FieldName = 'LOT_ID'
      Size = 0
    end
    object tblOutStavkaSLEDLIVOST_KOLICINA: TFIBSmallIntField
      DisplayLabel = #1057#1083#1077#1076#1083#1080#1074#1086#1089#1090
      FieldName = 'SLEDLIVOST_KOLICINA'
    end
    object tblOutStavkaART_KATALOG: TFIBStringField
      DisplayLabel = #1040#1088#1090'. '#1082#1072#1090#1072#1083#1086#1096#1082#1080' '#1073#1088'.'
      FieldName = 'ART_KATALOG'
      Size = 30
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblOutStavkaART_BARKOD: TFIBStringField
      DisplayLabel = #1040#1088#1090'. '#1073#1072#1088#1082#1086#1076
      FieldName = 'ART_BARKOD'
      Size = 30
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblOutStavkaART_SKU: TFIBStringField
      DisplayLabel = #1040#1088#1090'. SKU'
      FieldName = 'ART_SKU'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object dsOutStavka: TDataSource
    DataSet = tblOutStavka
    Left = 408
    Top = 80
  end
  object tblPovratnicaDobavuvac: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE MTR_OUT'
      'SET '
      '    ID = :ID,'
      '    GODINA = :GODINA,'
      '    RE = :RE,'
      '    TIP_DOK = :TIP_DOK,'
      '    BROJ = :BROJ,'
      '    BROJ_DOK = :BROJ_DOK,'
      '    DATUM = :DATUM,'
      '    TP = :TP,'
      '    P = :P,'
      '    RE2 = :RE2,'
      '    OBJEKT = :OBJEKT,'
      '    OPIS = :OPIS,'
      '    REF_NO = :REF_NO,'
      '    REF_DATE = :REF_DATE,'
      '    VALUTA = :VALUTA,'
      '    KURS = :KURS,'
      '    CENOVNIK = :CENOVNIK,'
      '    FAKTURA_ID = :FAKTURA_ID,'
      '    TIP_KNIZENJE = :TIP_KNIZENJE,'
      '    NALOG = :NALOG_ID,'
      '    REF_ID = :REF_ID,'
      '    SO_REF_ID = :SO_REF_ID,'
      '    WO_REF_ID = :WO_REF_ID,'
      '    AKTIVEN = :AKTIVEN,'
      '    KONTROLOR = :KONTROLOR,'
      '    KONTROLIRAN = :KONTROLIRAN,'
      '    KONTROLIRAN_NA = :KONTROLIRAN_NA,'
      '    LIKVIDIRAN = :LIKVIDIRAN,'
      '    LIKVIDIRAN_NA = :LIKVIDIRAN_NA,'
      '    LIKVIDATOR = :LIKVIDATOR,'
      '    PECATEN = :PECATEN,'
      '    PECATEN_NA = :PECATEN_NA,'
      '    PECATEN_OD = :PECATEN_OD,'
      '    SITE_ID = :SITE_ID,'
      '    STORNO_FLAG = :STORNO_FLAG,'
      '    STORNO_DATUM = :STORNO_DATUM,'
      '    STORNO_USER = :STORNO_USER,'
      '    STORNO_NALOG = :STORNO_NALOG_ID,'
      '    STORNO_ZABELESKA = :STORNO_ZABELESKA,'
      '    CUSTOM1 = :CUSTOM1,'
      '    CUSTOM2 = :CUSTOM2,'
      '    CUSTOM3 = :CUSTOM3,'
      '    TS_INS = :TS_INS,'
      '    TS_UPD = :TS_UPD,'
      '    USR_INS = :USR_INS,'
      '    USR_UPD = :USR_UPD'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    MTR_OUT'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO MTR_OUT('
      '    ID,'
      '    GODINA,'
      '    RE,'
      '    TIP_DOK,'
      '    BROJ,'
      '    BROJ_DOK,'
      '    DATUM,'
      '    TP,'
      '    P,'
      '    RE2,'
      '    OBJEKT,'
      '    OPIS,'
      '    REF_NO,'
      '    REF_DATE,'
      '    VALUTA,'
      '    KURS,'
      '    CENOVNIK,'
      '    FAKTURA_ID,'
      '    TIP_KNIZENJE,'
      '    NALOG,'
      '    REF_ID,'
      '    SO_REF_ID,'
      '    WO_REF_ID,'
      '    AKTIVEN,'
      '    KONTROLOR,'
      '    KONTROLIRAN,'
      '    KONTROLIRAN_NA,'
      '    LIKVIDIRAN,'
      '    LIKVIDIRAN_NA,'
      '    LIKVIDATOR,'
      '    PECATEN,'
      '    PECATEN_NA,'
      '    PECATEN_OD,'
      '    SITE_ID,'
      '    STORNO_FLAG,'
      '    STORNO_DATUM,'
      '    STORNO_USER,'
      '    STORNO_NALOG,'
      '    STORNO_ZABELESKA,'
      '    CUSTOM1,'
      '    CUSTOM2,'
      '    CUSTOM3,'
      '    TS_INS,'
      '    TS_UPD,'
      '    USR_INS,'
      '    USR_UPD'
      ')'
      'VALUES('
      '    :ID,'
      '    :GODINA,'
      '    :RE,'
      '    :TIP_DOK,'
      '    :BROJ,'
      '    :BROJ_DOK,'
      '    :DATUM,'
      '    :TP,'
      '    :P,'
      '    :RE2,'
      '    :OBJEKT,'
      '    :OPIS,'
      '    :REF_NO,'
      '    :REF_DATE,'
      '    :VALUTA,'
      '    :KURS,'
      '    :CENOVNIK,'
      '    :FAKTURA_ID,'
      '    :TIP_KNIZENJE,'
      '    :NALOG_ID,'
      '    :REF_ID,'
      '    :SO_REF_ID,'
      '    :WO_REF_ID,'
      '    :AKTIVEN,'
      '    :KONTROLOR,'
      '    :KONTROLIRAN,'
      '    :KONTROLIRAN_NA,'
      '    :LIKVIDIRAN,'
      '    :LIKVIDIRAN_NA,'
      '    :LIKVIDATOR,'
      '    :PECATEN,'
      '    :PECATEN_NA,'
      '    :PECATEN_OD,'
      '    :SITE_ID,'
      '    :STORNO_FLAG,'
      '    :STORNO_DATUM,'
      '    :STORNO_USER,'
      '    :STORNO_NALOG_ID,'
      '    :STORNO_ZABELESKA,'
      '    :CUSTOM1,'
      '    :CUSTOM2,'
      '    :CUSTOM3,'
      '    :TS_INS,'
      '    :TS_UPD,'
      '    :USR_INS,'
      '    :USR_UPD'
      ')')
    RefreshSQL.Strings = (
      'select O.ID,'
      '       O.GODINA,'
      '       O.RE,'
      '       RE.NAZIV as MAG_NAZIV,'
      '       O.TIP_DOK,'
      '       TD.NAZIV as TIP_DOK_NAZIV,'
      '       O.BROJ,'
      '       O.BROJ_DOK,'
      '       O.DATUM,'
      '       O.TP,'
      '       O.P,'
      '       P.NAZIV as PARTNER_NAZIV,'
      '       M.NAZIV as MESTO_NAZIV,'
      '       O.RE2,'
      '       R.NAZIV as RE2_NAZIV,'
      '       O.OBJEKT,'
      '       MO.NAZIV as OBJ_NAZIV,'
      '       MO.TP as OBJ_TP,'
      '       MO.P as OBJ_P,'
      '       MP.NAZIV as OBJ_PARTNER_NAZIV,'
      '       MO.REGION as OBJ_REGION,'
      '       MR.NAZIV as OBJ_REGION_NAZIV,'
      '       MO.TIP as OBJ_TIP,'
      '       MO.ADRESA as OBJ_ADRESA,'
      '       MO.MESTO as OBJ_MESTO,'
      '       MM.NAZIV as OBJ_MESTO_NAZIV,'
      '       O.OPIS,'
      '       O.REF_NO,'
      '       O.REF_DATE,'
      '       O.VALUTA,'
      '       V.NAZIV as VAL_NAZIV,'
      '       O.KURS,'
      '       O.CENOVNIK,'
      '       C.OPIS as CEN_OPIS,'
      '       C.VALUTA as CEN_VAL,'
      '       C.SO_DDV as CEN_DDV,'
      '       C.AKTIVEN as CEN_AKTIVEN,'
      '       C.VAZI_OD as CEN_VAZI_OD,'
      '       C.TIP as CEN_TIP,'
      '       O.FAKTURA_ID,'
      '       O.TIP_KNIZENJE,'
      '       TK.NAZIV as TK_NAZIV,'
      '       TK.KNIZENJE as TK_KNIZENJE,'
      '       O.NALOG as NALOG_ID,'
      '       NG.TIP_NALOG,'
      '       NG.NALOG,'
      '       NG.DATUM as NALOG_DATUM,'
      '       NG.OPIS as NALOG_OPIS,'
      '       NG.VID_NALOG as NALOG_VID,'
      '       O.REF_ID,'
      '       O2.RE as POVRATNICA_RE,'
      '       O2.TIP_DOK as POVRATNICA_TD,'
      '       O2.BROJ as POVRATNICA_BR,'
      '       O.SO_REF_ID,'
      '       SG.PREDMET as SAL_PREDMET,'
      '       SG.BROJ as SAL_BROJ,'
      '       SG.GODINA as SAL_GODINA,'
      '       O.WO_REF_ID,'
      '       O.AKTIVEN,'
      '       O.KONTROLOR,'
      '       O.KONTROLIRAN,'
      '       O.KONTROLIRAN_NA,'
      '       O.LIKVIDIRAN,'
      '       O.LIKVIDIRAN_NA,'
      '       O.LIKVIDATOR,'
      '       O.PECATEN,'
      '       O.PECATEN_NA,'
      '       O.PECATEN_OD,'
      '       O.SITE_ID,'
      '       O.STORNO_FLAG,'
      '       O.STORNO_DATUM,'
      '       O.STORNO_USER,'
      '       O.STORNO_NALOG as STORNO_NALOG_ID,'
      '       NG.TIP_NALOG as STORNO_TIP_NALOG,'
      '       NG.NALOG as STORNO_NALOG,'
      '       NG.DATUM as STORNO_NALOG_DATUM,'
      '       NG.OPIS as STORNO_NALOG_OPIS,'
      '       NG.VID_NALOG as STORNO_NALOG_VID,'
      '       O.STORNO_ZABELESKA,'
      '       O.CUSTOM1,'
      '       O.CUSTOM2,'
      '       O.CUSTOM3,'
      '       O.TS_INS,'
      '       O.TS_UPD,'
      '       O.USR_INS,'
      '       O.USR_UPD'
      'from MTR_OUT O'
      'join MTR_RE_MAGACIN RM on RM.ID = O.RE'
      'join MAT_RE RE on RE.ID = RM.ID'
      'join MTR_TIP_DOK TD on TD.ID = O.TIP_DOK'
      'left join MAT_PARTNER P on P.TIP_PARTNER = O.TP and P.ID = O.P'
      'left join MAT_MESTO M on M.ID = P.MESTO'
      'left join MAT_RE R on R.ID = O.RE2'
      'left join MAT_OBJEKT MO on MO.ID = O.OBJEKT'
      
        'left join MAT_PARTNER MP on MP.TIP_PARTNER = MO.TP and MP.ID = M' +
        'O.P'
      'left join MAT_MESTO MM on MM.ID = MO.MESTO'
      'left join MAT_REGIONI MR on MR.ID = MO.REGION'
      'left join MAT_VALUTA V on V.ID = O.VALUTA'
      'left join MTR_CENOVNIK C on C.ID = O.CENOVNIK'
      
        'left join FIN_TIP_KNIZENJE TK on TK.TIP_KNIZENJE = O.TIP_KNIZENJ' +
        'E'
      'left join FIN_NG NG on NG.ID = O.NALOG'
      'left join FIN_NG NG2 on NG2.ID = O.STORNO_NALOG'
      'left join SAL_POR_GLAVA SG on SG.ID = O.SO_REF_ID'
      'left join MTR_OUT O2 on O2.ID = O.REF_ID'
      'where(  O.RE = :MAGACIN'
      '      and O.GODINA = :GOD'
      
        '      and O.TIP_DOK = 12 -- fiksno 12 za povratnica kon dobavuva' +
        'c'
      '     ) and (     O.ID = :OLD_ID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      'select O.ID,'
      '       O.GODINA,'
      '       O.RE,'
      '       RE.NAZIV as MAG_NAZIV,'
      '       O.TIP_DOK,'
      '       TD.NAZIV as TIP_DOK_NAZIV,'
      '       O.BROJ,'
      '       O.BROJ_DOK,'
      '       O.DATUM,'
      '       O.TP,'
      '       O.P,'
      '       P.NAZIV as PARTNER_NAZIV,'
      '       M.NAZIV as MESTO_NAZIV,'
      '       O.RE2,'
      '       R.NAZIV as RE2_NAZIV,'
      '       O.OBJEKT,'
      '       MO.NAZIV as OBJ_NAZIV,'
      '       MO.TP as OBJ_TP,'
      '       MO.P as OBJ_P,'
      '       MP.NAZIV as OBJ_PARTNER_NAZIV,'
      '       MO.REGION as OBJ_REGION,'
      '       MR.NAZIV as OBJ_REGION_NAZIV,'
      '       MO.TIP as OBJ_TIP,'
      '       MO.ADRESA as OBJ_ADRESA,'
      '       MO.MESTO as OBJ_MESTO,'
      '       MM.NAZIV as OBJ_MESTO_NAZIV,'
      '       O.OPIS,'
      '       O.REF_NO,'
      '       O.REF_DATE,'
      '       O.VALUTA,'
      '       V.NAZIV as VAL_NAZIV,'
      '       O.KURS,'
      '       O.CENOVNIK,'
      '       C.OPIS as CEN_OPIS,'
      '       C.VALUTA as CEN_VAL,'
      '       C.SO_DDV as CEN_DDV,'
      '       C.AKTIVEN as CEN_AKTIVEN,'
      '       C.VAZI_OD as CEN_VAZI_OD,'
      '       C.TIP as CEN_TIP,'
      '       O.FAKTURA_ID,'
      '       O.TIP_KNIZENJE,'
      '       TK.NAZIV as TK_NAZIV,'
      '       TK.KNIZENJE as TK_KNIZENJE,'
      '       O.NALOG as NALOG_ID,'
      '       NG.TIP_NALOG,'
      '       NG.NALOG,'
      '       NG.DATUM as NALOG_DATUM,'
      '       NG.OPIS as NALOG_OPIS,'
      '       NG.VID_NALOG as NALOG_VID,'
      '       O.REF_ID,'
      '       O2.RE as POVRATNICA_RE,'
      '       O2.TIP_DOK as POVRATNICA_TD,'
      '       O2.BROJ as POVRATNICA_BR,'
      '       O.SO_REF_ID,'
      '       SG.PREDMET as SAL_PREDMET,'
      '       SG.BROJ as SAL_BROJ,'
      '       SG.GODINA as SAL_GODINA,'
      '       O.WO_REF_ID,'
      '       O.AKTIVEN,'
      '       O.KONTROLOR,'
      '       O.KONTROLIRAN,'
      '       O.KONTROLIRAN_NA,'
      '       O.LIKVIDIRAN,'
      '       O.LIKVIDIRAN_NA,'
      '       O.LIKVIDATOR,'
      '       O.PECATEN,'
      '       O.PECATEN_NA,'
      '       O.PECATEN_OD,'
      '       O.SITE_ID,'
      '       O.STORNO_FLAG,'
      '       O.STORNO_DATUM,'
      '       O.STORNO_USER,'
      '       O.STORNO_NALOG as STORNO_NALOG_ID,'
      '       NG.TIP_NALOG as STORNO_TIP_NALOG,'
      '       NG.NALOG as STORNO_NALOG,'
      '       NG.DATUM as STORNO_NALOG_DATUM,'
      '       NG.OPIS as STORNO_NALOG_OPIS,'
      '       NG.VID_NALOG as STORNO_NALOG_VID,'
      '       O.STORNO_ZABELESKA,'
      '       O.CUSTOM1,'
      '       O.CUSTOM2,'
      '       O.CUSTOM3,'
      '       O.TS_INS,'
      '       O.TS_UPD,'
      '       O.USR_INS,'
      '       O.USR_UPD'
      'from MTR_OUT O'
      'join MTR_RE_MAGACIN RM on RM.ID = O.RE'
      'join MAT_RE RE on RE.ID = RM.ID'
      'join MTR_TIP_DOK TD on TD.ID = O.TIP_DOK'
      'left join MAT_PARTNER P on P.TIP_PARTNER = O.TP and P.ID = O.P'
      'left join MAT_MESTO M on M.ID = P.MESTO'
      'left join MAT_RE R on R.ID = O.RE2'
      'left join MAT_OBJEKT MO on MO.ID = O.OBJEKT'
      
        'left join MAT_PARTNER MP on MP.TIP_PARTNER = MO.TP and MP.ID = M' +
        'O.P'
      'left join MAT_MESTO MM on MM.ID = MO.MESTO'
      'left join MAT_REGIONI MR on MR.ID = MO.REGION'
      'left join MAT_VALUTA V on V.ID = O.VALUTA'
      'left join MTR_CENOVNIK C on C.ID = O.CENOVNIK'
      
        'left join FIN_TIP_KNIZENJE TK on TK.TIP_KNIZENJE = O.TIP_KNIZENJ' +
        'E'
      'left join FIN_NG NG on NG.ID = O.NALOG'
      'left join FIN_NG NG2 on NG2.ID = O.STORNO_NALOG'
      'left join SAL_POR_GLAVA SG on SG.ID = O.SO_REF_ID'
      'left join MTR_OUT O2 on O2.ID = O.REF_ID'
      'where O.RE = :MAGACIN'
      '      and O.GODINA = :GOD'
      
        '      and O.TIP_DOK = 12 -- fiksno 12 za povratnica kon dobavuva' +
        'c'
      'order by O.ID desc  ')
    AutoUpdateOptions.UpdateTableName = 'MTR_OUT'
    AutoUpdateOptions.KeyFields = 'ID'
    AutoUpdateOptions.GeneratorName = 'GEN_MTR_OUT_ID'
    AutoUpdateOptions.WhenGetGenID = wgBeforePost
    BeforeDelete = TabelaBeforeDelete
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 496
    Top = 472
    poSQLINT64ToBCD = True
    object tblPovratnicaDobavuvacID: TFIBBCDField
      DisplayLabel = #1064#1080#1092'.'
      FieldName = 'ID'
      Size = 0
    end
    object tblPovratnicaDobavuvacGODINA: TFIBIntegerField
      DisplayLabel = #1043#1086#1076#1080#1085#1072
      FieldName = 'GODINA'
    end
    object tblPovratnicaDobavuvacRE: TFIBIntegerField
      DisplayLabel = #1056#1045
      FieldName = 'RE'
    end
    object tblPovratnicaDobavuvacMAG_NAZIV: TFIBStringField
      DisplayLabel = #1052#1072#1075'. '#1085#1072#1079#1080#1074
      FieldName = 'MAG_NAZIV'
      Size = 255
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPovratnicaDobavuvacTIP_DOK: TFIBIntegerField
      DisplayLabel = #1058#1080#1087' '#1076#1086#1082'.'
      FieldName = 'TIP_DOK'
    end
    object tblPovratnicaDobavuvacTIP_DOK_NAZIV: TFIBStringField
      DisplayLabel = #1058#1080#1087' '#1076#1086#1082'. '#1085#1072#1079#1080#1074
      FieldName = 'TIP_DOK_NAZIV'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPovratnicaDobavuvacBROJ: TFIBIntegerField
      DisplayLabel = #1041#1088#1086#1112
      FieldName = 'BROJ'
    end
    object tblPovratnicaDobavuvacBROJ_DOK: TFIBStringField
      DisplayLabel = #1044#1086#1082'. '#1073#1088#1086#1112
      FieldName = 'BROJ_DOK'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPovratnicaDobavuvacDATUM: TFIBDateField
      DisplayLabel = #1044#1072#1090#1091#1084
      FieldName = 'DATUM'
    end
    object tblPovratnicaDobavuvacTP: TFIBIntegerField
      DisplayLabel = #1058#1055
      FieldName = 'TP'
    end
    object tblPovratnicaDobavuvacP: TFIBIntegerField
      DisplayLabel = #1055
      FieldName = 'P'
    end
    object tblPovratnicaDobavuvacPARTNER_NAZIV: TFIBStringField
      DisplayLabel = #1055#1072#1088#1090#1085#1077#1088
      FieldName = 'PARTNER_NAZIV'
      Size = 255
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPovratnicaDobavuvacMESTO_NAZIV: TFIBStringField
      DisplayLabel = #1052#1077#1089#1090#1086
      FieldName = 'MESTO_NAZIV'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPovratnicaDobavuvacRE2: TFIBIntegerField
      DisplayLabel = #1056#1072#1073'.'#1077#1076'.'
      FieldName = 'RE2'
    end
    object tblPovratnicaDobavuvacRE2_NAZIV: TFIBStringField
      DisplayLabel = #1056#1072#1073'.'#1077#1076'. '#1085#1072#1079#1080#1074
      FieldName = 'RE2_NAZIV'
      Size = 255
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPovratnicaDobavuvacOBJEKT: TFIBIntegerField
      DisplayLabel = #1054#1073#1112'. '#1096#1080#1092'.'
      FieldName = 'OBJEKT'
    end
    object tblPovratnicaDobavuvacOBJ_NAZIV: TFIBStringField
      DisplayLabel = #1054#1073#1112#1077#1082#1090
      FieldName = 'OBJ_NAZIV'
      Size = 255
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPovratnicaDobavuvacOBJ_TP: TFIBIntegerField
      DisplayLabel = #1054#1073#1112'. '#1058#1055
      FieldName = 'OBJ_TP'
    end
    object tblPovratnicaDobavuvacOBJ_P: TFIBIntegerField
      DisplayLabel = #1054#1073#1112'. '#1055
      FieldName = 'OBJ_P'
    end
    object tblPovratnicaDobavuvacOBJ_PARTNER_NAZIV: TFIBStringField
      DisplayLabel = #1054#1073#1112'. '#1087#1072#1088#1090#1085#1077#1088
      FieldName = 'OBJ_PARTNER_NAZIV'
      Size = 255
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPovratnicaDobavuvacOBJ_REGION: TFIBIntegerField
      DisplayLabel = #1054#1073#1112'. '#1088#1077#1075#1080#1086#1085' '#1096#1080#1092'.'
      FieldName = 'OBJ_REGION'
    end
    object tblPovratnicaDobavuvacOBJ_REGION_NAZIV: TFIBStringField
      DisplayLabel = #1054#1073#1112'. '#1088#1077#1075#1080#1086#1085
      FieldName = 'OBJ_REGION_NAZIV'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPovratnicaDobavuvacOBJ_TIP: TFIBIntegerField
      DisplayLabel = #1054#1073#1112'. '#1090#1080#1087
      FieldName = 'OBJ_TIP'
    end
    object tblPovratnicaDobavuvacOBJ_ADRESA: TFIBStringField
      DisplayLabel = #1054#1073#1112'. '#1072#1076#1088#1077#1089#1072
      FieldName = 'OBJ_ADRESA'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPovratnicaDobavuvacOBJ_MESTO: TFIBIntegerField
      DisplayLabel = #1054#1073#1112'. '#1084#1077#1089#1090#1086' '#1096#1080#1092'.'
      FieldName = 'OBJ_MESTO'
    end
    object tblPovratnicaDobavuvacOBJ_MESTO_NAZIV: TFIBStringField
      DisplayLabel = #1054#1073#1112'. '#1084#1077#1089#1090#1086
      FieldName = 'OBJ_MESTO_NAZIV'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPovratnicaDobavuvacOPIS: TFIBStringField
      DisplayLabel = #1054#1087#1080#1089
      FieldName = 'OPIS'
      Size = 200
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPovratnicaDobavuvacREF_NO: TFIBStringField
      FieldName = 'REF_NO'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPovratnicaDobavuvacVALUTA: TFIBStringField
      DisplayLabel = #1042#1072#1083#1091#1090#1072
      FieldName = 'VALUTA'
      Size = 3
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPovratnicaDobavuvacVAL_NAZIV: TFIBStringField
      DisplayLabel = #1042#1072#1083'. '#1085#1072#1079#1080#1074
      FieldName = 'VAL_NAZIV'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPovratnicaDobavuvacKURS: TFIBBCDField
      DisplayLabel = #1050#1091#1088#1089
      FieldName = 'KURS'
      Size = 5
    end
    object tblPovratnicaDobavuvacCENOVNIK: TFIBIntegerField
      DisplayLabel = #1062#1077#1085#1086#1074#1085#1080#1082
      FieldName = 'CENOVNIK'
    end
    object tblPovratnicaDobavuvacCEN_OPIS: TFIBStringField
      DisplayLabel = #1062#1077#1085'. '#1086#1087#1080#1089
      FieldName = 'CEN_OPIS'
      Size = 255
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPovratnicaDobavuvacCEN_VAL: TFIBStringField
      DisplayLabel = #1062#1077#1085'. '#1074#1072#1083#1091#1090#1072
      FieldName = 'CEN_VAL'
      Size = 3
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPovratnicaDobavuvacCEN_DDV: TFIBSmallIntField
      DisplayLabel = #1062#1077#1085'. '#1044#1044#1042
      FieldName = 'CEN_DDV'
    end
    object tblPovratnicaDobavuvacCEN_AKTIVEN: TFIBSmallIntField
      DisplayLabel = #1062#1077#1085'. '#1072#1082#1090#1080#1074#1077#1085
      FieldName = 'CEN_AKTIVEN'
    end
    object tblPovratnicaDobavuvacCEN_VAZI_OD: TFIBDateField
      DisplayLabel = #1062#1077#1085'. '#1074#1072#1078#1080' '#1086#1076
      FieldName = 'CEN_VAZI_OD'
    end
    object tblPovratnicaDobavuvacCEN_TIP: TFIBSmallIntField
      DisplayLabel = #1062#1077#1085'. '#1090#1080#1087
      FieldName = 'CEN_TIP'
    end
    object tblPovratnicaDobavuvacFAKTURA_ID: TFIBBCDField
      DisplayLabel = #1060#1072#1082#1090#1091#1088#1072' '#1096#1080#1092'.'
      FieldName = 'FAKTURA_ID'
      Size = 0
    end
    object tblPovratnicaDobavuvacTIP_KNIZENJE: TFIBIntegerField
      DisplayLabel = #1058#1080#1087' '#1082#1085#1080#1078'. '#1096#1080#1092'.'
      FieldName = 'TIP_KNIZENJE'
    end
    object tblPovratnicaDobavuvacTK_NAZIV: TFIBStringField
      DisplayLabel = #1058#1080#1087' '#1082#1085#1080#1078#1077#1114#1077
      FieldName = 'TK_NAZIV'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPovratnicaDobavuvacTK_KNIZENJE: TFIBIntegerField
      DisplayLabel = #1058#1080#1087' '#1082#1085#1080#1078'. '#1073#1088#1086#1112
      FieldName = 'TK_KNIZENJE'
    end
    object tblPovratnicaDobavuvacTIP_NALOG: TFIBStringField
      DisplayLabel = #1058#1080#1087' '#1085#1072#1083#1086#1075
      FieldName = 'TIP_NALOG'
      Size = 5
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPovratnicaDobavuvacNALOG_ID: TFIBBCDField
      DisplayLabel = #1053#1072#1083#1086#1075' ID'
      FieldName = 'NALOG_ID'
      Size = 0
    end
    object tblPovratnicaDobavuvacNALOG: TFIBIntegerField
      DisplayLabel = #1053#1072#1083#1086#1075
      FieldName = 'NALOG'
    end
    object tblPovratnicaDobavuvacNALOG_DATUM: TFIBDateField
      DisplayLabel = #1053#1072#1083#1086#1075' '#1076#1072#1090#1091#1084
      FieldName = 'NALOG_DATUM'
    end
    object tblPovratnicaDobavuvacNALOG_OPIS: TFIBStringField
      DisplayLabel = #1053#1072#1083#1086#1075' '#1086#1087#1080#1089
      FieldName = 'NALOG_OPIS'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPovratnicaDobavuvacNALOG_VID: TFIBSmallIntField
      DisplayLabel = #1053#1072#1083#1086#1075' '#1074#1080#1076
      FieldName = 'NALOG_VID'
    end
    object tblPovratnicaDobavuvacREF_ID: TFIBBCDField
      FieldName = 'REF_ID'
      Size = 0
    end
    object tblPovratnicaDobavuvacREF_DATE: TFIBDateField
      FieldName = 'REF_DATE'
    end
    object tblPovratnicaDobavuvacSO_REF_ID: TFIBIntegerField
      DisplayLabel = #1055#1088#1086#1076#1072#1078#1073#1072' '#1096#1080#1092'.'
      FieldName = 'SO_REF_ID'
    end
    object tblPovratnicaDobavuvacSAL_PREDMET: TFIBStringField
      DisplayLabel = #1055#1088#1086#1076#1072#1078#1073#1072' '#1087#1088#1077#1076#1084#1077#1090
      FieldName = 'SAL_PREDMET'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPovratnicaDobavuvacSAL_BROJ: TFIBIntegerField
      DisplayLabel = #1055#1088#1086#1076#1072#1078#1073#1072' '#1073#1088'.'
      FieldName = 'SAL_BROJ'
    end
    object tblPovratnicaDobavuvacSAL_GODINA: TFIBIntegerField
      DisplayLabel = #1055#1088#1086#1076#1072#1078#1073#1072' '#1075#1086#1076'.'
      FieldName = 'SAL_GODINA'
    end
    object tblPovratnicaDobavuvacAKTIVEN: TFIBSmallIntField
      DisplayLabel = #1040#1082#1090#1080#1074#1077#1085
      FieldName = 'AKTIVEN'
    end
    object tblPovratnicaDobavuvacKONTROLOR: TFIBStringField
      DisplayLabel = #1050#1086#1085#1090#1088#1086#1083#1086#1088
      FieldName = 'KONTROLOR'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPovratnicaDobavuvacKONTROLIRAN: TFIBSmallIntField
      DisplayLabel = #1050#1086#1085#1090#1088#1086#1083#1080#1088#1072#1085#1086
      FieldName = 'KONTROLIRAN'
    end
    object tblPovratnicaDobavuvacKONTROLIRAN_NA: TFIBDateTimeField
      DisplayLabel = #1044#1072#1090#1091#1084' '#1082#1086#1085#1090#1088#1086#1083#1072
      FieldName = 'KONTROLIRAN_NA'
    end
    object tblPovratnicaDobavuvacLIKVIDIRAN: TFIBSmallIntField
      DisplayLabel = #1051#1080#1082#1074#1080#1076#1080#1088#1072#1085#1086
      FieldName = 'LIKVIDIRAN'
    end
    object tblPovratnicaDobavuvacLIKVIDIRAN_NA: TFIBDateTimeField
      DisplayLabel = #1044#1072#1090#1091#1084' '#1083#1080#1082#1074#1080#1076#1080#1088#1072#1085#1086
      FieldName = 'LIKVIDIRAN_NA'
    end
    object tblPovratnicaDobavuvacLIKVIDATOR: TFIBStringField
      DisplayLabel = #1051#1080#1082#1074#1080#1076#1072#1090#1086#1088
      FieldName = 'LIKVIDATOR'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPovratnicaDobavuvacPECATEN: TFIBSmallIntField
      DisplayLabel = #1055#1077#1095#1072#1090#1077#1085#1086
      FieldName = 'PECATEN'
    end
    object tblPovratnicaDobavuvacPECATEN_NA: TFIBDateTimeField
      DisplayLabel = #1044#1072#1090#1091#1084' '#1087#1077#1095#1072#1090#1077#1085#1086
      FieldName = 'PECATEN_NA'
    end
    object tblPovratnicaDobavuvacPECATEN_OD: TFIBStringField
      DisplayLabel = #1055#1077#1095#1072#1090#1077#1085#1086' '#1086#1076
      FieldName = 'PECATEN_OD'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPovratnicaDobavuvacSITE_ID: TFIBSmallIntField
      FieldName = 'SITE_ID'
    end
    object tblPovratnicaDobavuvacCUSTOM1: TFIBStringField
      FieldName = 'CUSTOM1'
      Size = 200
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPovratnicaDobavuvacCUSTOM2: TFIBStringField
      FieldName = 'CUSTOM2'
      Size = 200
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPovratnicaDobavuvacCUSTOM3: TFIBStringField
      FieldName = 'CUSTOM3'
      Size = 200
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPovratnicaDobavuvacTS_INS: TFIBDateTimeField
      FieldName = 'TS_INS'
    end
    object tblPovratnicaDobavuvacTS_UPD: TFIBDateTimeField
      FieldName = 'TS_UPD'
    end
    object tblPovratnicaDobavuvacUSR_INS: TFIBStringField
      FieldName = 'USR_INS'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPovratnicaDobavuvacUSR_UPD: TFIBStringField
      FieldName = 'USR_UPD'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPovratnicaDobavuvacPOVRATNICA_RE: TFIBIntegerField
      DisplayLabel = #1055#1086#1074#1088#1072#1090#1085#1080#1094#1072' '#1084#1072#1075'.'
      FieldName = 'POVRATNICA_RE'
    end
    object tblPovratnicaDobavuvacPOVRATNICA_TD: TFIBIntegerField
      DisplayLabel = #1055#1086#1074#1088#1072#1090#1085#1080#1094#1072' '#1090#1080#1087' '#1076#1086#1082'.'
      FieldName = 'POVRATNICA_TD'
    end
    object tblPovratnicaDobavuvacPOVRATNICA_BR: TFIBIntegerField
      DisplayLabel = #1055#1086#1074#1088#1072#1090#1085#1080#1094#1072' '#1073#1088'.'
      FieldName = 'POVRATNICA_BR'
    end
    object tblPovratnicaDobavuvacSTORNO_FLAG: TFIBSmallIntField
      DisplayLabel = #1057#1090#1086#1088#1085#1080#1088#1072#1085#1086
      FieldName = 'STORNO_FLAG'
    end
    object tblPovratnicaDobavuvacSTORNO_DATUM: TFIBDateField
      DisplayLabel = #1057#1090#1086#1088#1085#1086' '#1076#1072#1090#1091#1084
      FieldName = 'STORNO_DATUM'
    end
    object tblPovratnicaDobavuvacSTORNO_USER: TFIBStringField
      DisplayLabel = #1057#1090#1086#1088#1085#1080#1088#1072#1083
      FieldName = 'STORNO_USER'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPovratnicaDobavuvacSTORNO_NALOG_ID: TFIBBCDField
      DisplayLabel = #1057#1090#1086#1088#1085#1086' '#1085#1072#1083#1086#1075' '#1096#1080#1092'.'
      FieldName = 'STORNO_NALOG_ID'
      Size = 0
    end
    object tblPovratnicaDobavuvacSTORNO_TIP_NALOG: TFIBStringField
      DisplayLabel = #1057#1090#1086#1088#1085#1086' '#1090#1080#1087' '#1085#1072#1083#1086#1075
      FieldName = 'STORNO_TIP_NALOG'
      Size = 5
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPovratnicaDobavuvacSTORNO_NALOG: TFIBIntegerField
      DisplayLabel = #1057#1090#1086#1088#1085#1086' '#1085#1072#1083#1086#1075
      FieldName = 'STORNO_NALOG'
    end
    object tblPovratnicaDobavuvacSTORNO_NALOG_DATUM: TFIBDateField
      DisplayLabel = #1057#1090#1086#1088#1085#1086' '#1085#1072#1083#1086#1075' '#1076#1072#1090#1091#1084
      FieldName = 'STORNO_NALOG_DATUM'
    end
    object tblPovratnicaDobavuvacSTORNO_NALOG_OPIS: TFIBStringField
      FieldName = 'STORNO_NALOG_OPIS'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPovratnicaDobavuvacSTORNO_NALOG_VID: TFIBSmallIntField
      DisplayLabel = #1057#1090#1086#1088#1085#1086' '#1074#1080#1076' '#1085#1072#1083#1086#1075
      FieldName = 'STORNO_NALOG_VID'
    end
    object tblPovratnicaDobavuvacSTORNO_ZABELESKA: TFIBStringField
      DisplayLabel = #1057#1090#1086#1088#1085#1086' '#1079#1072#1073#1077#1083#1077#1096#1082#1072
      FieldName = 'STORNO_ZABELESKA'
      Size = 200
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPovratnicaDobavuvacWO_REF_ID: TFIBBCDField
      DisplayLabel = #1056#1072#1073'. '#1085#1072#1083#1086#1075' '#1096#1080#1092'.'
      FieldName = 'WO_REF_ID'
      Size = 0
    end
  end
  object dsPovratnicaDobavuvac: TDataSource
    DataSet = tblPovratnicaDobavuvac
    Left = 432
    Top = 472
  end
  object tblPovratnicaDobavuvacStavka: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE MTR_OUT_S'
      'SET '
      '    ID = :ID,'
      '    MTR_OUT_ID = :MTR_OUT_ID,'
      '    ART_VID = :ART_VID,'
      '    ART_SIF = :ART_SIF,'
      '    KOLICINA = :KOLICINA,'
      '    SLEDLIVOST_KOLICINA = :SLEDLIVOST_KOLICINA,'
      '    PAKETI = :PAKETI,'
      '    IZV_EM = :IZV_EM,'
      '    BR_PAKET = :BR_PAKET,'
      '    CENA_SO_DDV = :CENA_SO_DDV,'
      '    CENA_BEZ_DDV = :CENA_BEZ_DDV,'
      '    DANOK_PR = :DANOK_PR,'
      '    RABAT_PR = :RABAT_PR,'
      '    DANOK_IZNOS = :DANOK_IZNOS,'
      '    RABAT_IZNOS = :RABAT_IZNOS,'
      '    IZNOS_SO_DDV = :IZNOS_SO_DDV,'
      '    IZNOS_BEZ_DDV = :IZNOS_BEZ_DDV,'
      '    KNIG_CENA = :KNIG_CENA,'
      '    KNIG_IZNOS = :KNIG_IZNOS,'
      '    CENA_BEZ_RABAT_SO_DDV = :CENA_BEZ_RABAT_SO_DDV,'
      '    CENA_BEZ_RABAT_BEZ_DDV = :CENA_BEZ_RABAT_BEZ_DDV,'
      '    VAZI_DO = :VAZI_DO,'
      '    LOT_NO = :LOT_NO,'
      '    REF_ID = :REF_ID,'
      '    OPIS = :OPIS,'
      '    CUSTOM1 = :CUSTOM1,'
      '    CUSTOM2 = :CUSTOM2,'
      '    CUSTOM3 = :CUSTOM3,'
      '    TS_INS = :TS_INS,'
      '    TS_UPD = :TS_UPD,'
      '    USR_INS = :USR_INS,'
      '    USR_UPD = :USR_UPD'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    MTR_OUT_S'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO MTR_OUT_S('
      '    ID,'
      '    MTR_OUT_ID,'
      '    ART_VID,'
      '    ART_SIF,'
      '    KOLICINA,'
      '    SLEDLIVOST_KOLICINA,'
      '    PAKETI,'
      '    IZV_EM,'
      '    BR_PAKET,'
      '    CENA_SO_DDV,'
      '    CENA_BEZ_DDV,'
      '    DANOK_PR,'
      '    RABAT_PR,'
      '    DANOK_IZNOS,'
      '    RABAT_IZNOS,'
      '    IZNOS_SO_DDV,'
      '    IZNOS_BEZ_DDV,'
      '    KNIG_CENA,'
      '    KNIG_IZNOS,'
      '    CENA_BEZ_RABAT_SO_DDV,'
      '    CENA_BEZ_RABAT_BEZ_DDV,'
      '    VAZI_DO,'
      '    LOT_NO,'
      '    REF_ID,'
      '    OPIS,'
      '    CUSTOM1,'
      '    CUSTOM2,'
      '    CUSTOM3,'
      '    TS_INS,'
      '    TS_UPD,'
      '    USR_INS,'
      '    USR_UPD'
      ')'
      'VALUES('
      '    :ID,'
      '    :MTR_OUT_ID,'
      '    :ART_VID,'
      '    :ART_SIF,'
      '    :KOLICINA,'
      '    :SLEDLIVOST_KOLICINA,'
      '    :PAKETI,'
      '    :IZV_EM,'
      '    :BR_PAKET,'
      '    :CENA_SO_DDV,'
      '    :CENA_BEZ_DDV,'
      '    :DANOK_PR,'
      '    :RABAT_PR,'
      '    :DANOK_IZNOS,'
      '    :RABAT_IZNOS,'
      '    :IZNOS_SO_DDV,'
      '    :IZNOS_BEZ_DDV,'
      '    :KNIG_CENA,'
      '    :KNIG_IZNOS,'
      '    :CENA_BEZ_RABAT_SO_DDV,'
      '    :CENA_BEZ_RABAT_BEZ_DDV,'
      '    :VAZI_DO,'
      '    :LOT_NO,'
      '    :REF_ID,'
      '    :OPIS,'
      '    :CUSTOM1,'
      '    :CUSTOM2,'
      '    :CUSTOM3,'
      '    :TS_INS,'
      '    :TS_UPD,'
      '    :USR_INS,'
      '    :USR_UPD'
      ')')
    RefreshSQL.Strings = (
      'select S.ID,'
      '       S.MTR_OUT_ID,'
      '       S.ART_VID,'
      '       S.ART_SIF,'
      '       A.NAZIV as ART_NAZIV,'
      '       S.KOLICINA,'
      '       A.MERKA as ART_MERKA,'
      '       A.KATALOG as ART_KATALOG,'
      '       A.BARKOD as ART_BARKOD,'
      '       A.SKU as ART_SKU,'
      '       S.SLEDLIVOST_KOLICINA,'
      '       S.PAKETI,'
      '       S.IZV_EM,'
      '       IE.IZVEDENA_EM,'
      '       S.BR_PAKET,'
      '       S.CENA_SO_DDV,'
      '       S.CENA_BEZ_DDV,'
      '       S.DANOK_PR,'
      '       S.RABAT_PR,'
      '       S.DANOK_IZNOS,'
      '       S.RABAT_IZNOS,'
      '       S.IZNOS_SO_DDV,'
      '       S.IZNOS_BEZ_DDV,'
      '       S.KNIG_CENA,'
      '       S.KNIG_IZNOS,'
      '       S.CENA_BEZ_RABAT_SO_DDV,'
      '       S.CENA_BEZ_RABAT_BEZ_DDV,'
      '       S.VAZI_DO,'
      '       S.LOT_NO,'
      '       S.REF_ID,'
      '       S.OPIS,'
      '       S.CUSTOM1,'
      '       S.CUSTOM2,'
      '       S.CUSTOM3,'
      '       S.TS_INS,'
      '       S.TS_UPD,'
      '       S.USR_INS,'
      '       S.USR_UPD'
      'from MTR_OUT_S S'
      'join MTR_ARTIKAL A on A.ARTVID = S.ART_VID and A.ID = S.ART_SIF'
      'left join MTR_IZVEDENA_EM IE on IE.ID = S.IZV_EM'
      'where(  S.MTR_OUT_ID = :MAS_ID'
      '     ) and (     S.ID = :OLD_ID'
      '     )'
      '       ')
    SelectSQL.Strings = (
      'select S.ID,'
      '       S.MTR_OUT_ID,'
      '       S.ART_VID,'
      '       S.ART_SIF,'
      '       A.NAZIV as ART_NAZIV,'
      '       S.KOLICINA,'
      '       A.MERKA as ART_MERKA,'
      '       A.KATALOG as ART_KATALOG,'
      '       A.BARKOD as ART_BARKOD,'
      '       A.SKU as ART_SKU,'
      '       S.SLEDLIVOST_KOLICINA,'
      '       S.PAKETI,'
      '       S.IZV_EM,'
      '       IE.IZVEDENA_EM,'
      '       S.BR_PAKET,'
      '       S.CENA_SO_DDV,'
      '       S.CENA_BEZ_DDV,'
      '       S.DANOK_PR,'
      '       S.RABAT_PR,'
      '       S.DANOK_IZNOS,'
      '       S.RABAT_IZNOS,'
      '       S.IZNOS_SO_DDV,'
      '       S.IZNOS_BEZ_DDV,'
      '       S.KNIG_CENA,'
      '       S.KNIG_IZNOS,'
      '       S.CENA_BEZ_RABAT_SO_DDV,'
      '       S.CENA_BEZ_RABAT_BEZ_DDV,'
      '       S.VAZI_DO,'
      '       S.LOT_NO,'
      '       S.REF_ID,'
      '       S.OPIS,'
      '       S.CUSTOM1,'
      '       S.CUSTOM2,'
      '       S.CUSTOM3,'
      '       S.TS_INS,'
      '       S.TS_UPD,'
      '       S.USR_INS,'
      '       S.USR_UPD'
      'from MTR_OUT_S S'
      'join MTR_ARTIKAL A on A.ARTVID = S.ART_VID and A.ID = S.ART_SIF'
      'left join MTR_IZVEDENA_EM IE on IE.ID = S.IZV_EM'
      'where S.MTR_OUT_ID = :MAS_ID   ')
    AutoUpdateOptions.UpdateTableName = 'MTR_OUT_S'
    AutoUpdateOptions.KeyFields = 'ID'
    AutoUpdateOptions.GeneratorName = 'GEN_MTR_OUT_S_ID'
    AutoUpdateOptions.WhenGetGenID = wgBeforePost
    BeforeDelete = TabelaBeforeDelete
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    DataSource = dsPovratnicaDobavuvac
    Left = 496
    Top = 525
    poSQLINT64ToBCD = True
    object tblPovratnicaDobavuvacStavkaID: TFIBBCDField
      DisplayLabel = #1064#1080#1092'.'
      FieldName = 'ID'
      Size = 0
    end
    object tblPovratnicaDobavuvacStavkaMTR_OUT_ID: TFIBBCDField
      DisplayLabel = #1048#1089#1087'. '#1096#1080#1092'.'
      FieldName = 'MTR_OUT_ID'
      Size = 0
    end
    object tblPovratnicaDobavuvacStavkaART_VID: TFIBIntegerField
      DisplayLabel = #1040#1088#1090'. '#1074#1080#1076
      FieldName = 'ART_VID'
    end
    object tblPovratnicaDobavuvacStavkaART_SIF: TFIBIntegerField
      DisplayLabel = #1040#1088#1090'. '#1096#1080#1092'.'
      FieldName = 'ART_SIF'
    end
    object tblPovratnicaDobavuvacStavkaART_NAZIV: TFIBStringField
      DisplayLabel = #1040#1088#1090'. '#1085#1072#1079#1080#1074
      FieldName = 'ART_NAZIV'
      Size = 500
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPovratnicaDobavuvacStavkaKOLICINA: TFIBFloatField
      DisplayLabel = #1050#1086#1083#1080#1095#1080#1085#1072
      FieldName = 'KOLICINA'
      DisplayFormat = '###,###0.000'
      EditFormat = '0.000'
    end
    object tblPovratnicaDobavuvacStavkaPAKETI: TFIBIntegerField
      DisplayLabel = #1055#1072#1082#1077#1090#1080
      FieldName = 'PAKETI'
    end
    object tblPovratnicaDobavuvacStavkaIZV_EM: TFIBIntegerField
      DisplayLabel = #1048'.'#1077#1076'.'#1084#1077#1088#1082#1072' '#1096#1080#1092'.'
      FieldName = 'IZV_EM'
    end
    object tblPovratnicaDobavuvacStavkaIZVEDENA_EM: TFIBStringField
      DisplayLabel = #1048#1079#1074#1077#1076#1077#1085#1072' '#1077#1076'. '#1084#1077#1088#1082#1072
      FieldName = 'IZVEDENA_EM'
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPovratnicaDobavuvacStavkaBR_PAKET: TFIBBCDField
      DisplayLabel = #1041#1088'. '#1074#1086' '#1087#1072#1082#1077#1090
      FieldName = 'BR_PAKET'
      Size = 8
    end
    object tblPovratnicaDobavuvacStavkaCENA_SO_DDV: TFIBFloatField
      DisplayLabel = #1062#1077#1085#1072' '#1089#1086' '#1044#1044#1042
      FieldName = 'CENA_SO_DDV'
      DisplayFormat = '###,##0.00'
      EditFormat = '0.00'
    end
    object tblPovratnicaDobavuvacStavkaCENA_BEZ_DDV: TFIBFloatField
      DisplayLabel = #1062#1077#1085#1072' '#1073#1077#1079' '#1044#1044#1042
      FieldName = 'CENA_BEZ_DDV'
      DisplayFormat = '###,##0.00'
      EditFormat = '0.00'
    end
    object tblPovratnicaDobavuvacStavkaDANOK_PR: TFIBBCDField
      DisplayLabel = #1055#1088#1086#1094'. '#1085#1072' '#1076#1072#1085#1086#1082
      FieldName = 'DANOK_PR'
      Size = 2
    end
    object tblPovratnicaDobavuvacStavkaRABAT_PR: TFIBBCDField
      DisplayLabel = #1055#1088#1086#1094'. '#1084#1072' '#1088#1072#1073#1072#1090
      FieldName = 'RABAT_PR'
      Size = 2
    end
    object tblPovratnicaDobavuvacStavkaKNIG_CENA: TFIBFloatField
      DisplayLabel = #1050#1085#1080#1075'. '#1094#1077#1085#1072
      FieldName = 'KNIG_CENA'
      DisplayFormat = '###,##0.00'
    end
    object tblPovratnicaDobavuvacStavkaKNIG_IZNOS: TFIBBCDField
      DisplayLabel = #1050#1085#1080#1075'. '#1080#1079#1085#1086#1089
      FieldName = 'KNIG_IZNOS'
      Size = 2
    end
    object tblPovratnicaDobavuvacStavkaIZNOS_BEZ_DDV: TFIBBCDField
      DisplayLabel = #1048#1079#1085#1086#1089' '#1073#1077#1079' '#1044#1044#1042
      FieldName = 'IZNOS_BEZ_DDV'
      Size = 2
    end
    object tblPovratnicaDobavuvacStavkaIZNOS_SO_DDV: TFIBBCDField
      DisplayLabel = #1048#1079#1085#1086#1089' '#1089#1086' '#1044#1044#1042
      FieldName = 'IZNOS_SO_DDV'
      Size = 2
    end
    object tblPovratnicaDobavuvacStavkaRABAT_IZNOS: TFIBBCDField
      DisplayLabel = #1048#1079#1085#1086#1089' '#1085#1072' '#1088#1072#1073#1072#1090
      FieldName = 'RABAT_IZNOS'
      Size = 2
    end
    object tblPovratnicaDobavuvacStavkaDANOK_IZNOS: TFIBBCDField
      DisplayLabel = #1048#1079#1085#1086#1089' '#1085#1072' '#1044#1044#1042
      FieldName = 'DANOK_IZNOS'
      Size = 2
    end
    object tblPovratnicaDobavuvacStavkaLOT_NO: TFIBStringField
      DisplayLabel = 'LOT '#1073#1088'.'
      FieldName = 'LOT_NO'
      Size = 30
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPovratnicaDobavuvacStavkaVAZI_DO: TFIBDateField
      DisplayLabel = #1042#1072#1078#1080' '#1076#1086
      FieldName = 'VAZI_DO'
    end
    object tblPovratnicaDobavuvacStavkaTS_INS: TFIBDateTimeField
      FieldName = 'TS_INS'
    end
    object tblPovratnicaDobavuvacStavkaTS_UPD: TFIBDateTimeField
      FieldName = 'TS_UPD'
    end
    object tblPovratnicaDobavuvacStavkaUSR_INS: TFIBStringField
      FieldName = 'USR_INS'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPovratnicaDobavuvacStavkaUSR_UPD: TFIBStringField
      FieldName = 'USR_UPD'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPovratnicaDobavuvacStavkaCUSTOM1: TFIBStringField
      FieldName = 'CUSTOM1'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPovratnicaDobavuvacStavkaCUSTOM2: TFIBStringField
      FieldName = 'CUSTOM2'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPovratnicaDobavuvacStavkaCUSTOM3: TFIBStringField
      FieldName = 'CUSTOM3'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPovratnicaDobavuvacStavkaART_MERKA: TFIBStringField
      DisplayLabel = #1040#1088#1090'. '#1084#1077#1088#1082#1072
      FieldName = 'ART_MERKA'
      Size = 5
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPovratnicaDobavuvacStavkaCENA_BEZ_RABAT_SO_DDV: TFIBFloatField
      DisplayLabel = #1062#1077#1085#1072' '#1073#1077#1079' '#1088#1072#1073#1072#1090' '#1089#1086' '#1044#1044#1042
      FieldName = 'CENA_BEZ_RABAT_SO_DDV'
      DisplayFormat = '###,##0.00'
      EditFormat = '0.00'
    end
    object tblPovratnicaDobavuvacStavkaCENA_BEZ_RABAT_BEZ_DDV: TFIBFloatField
      DisplayLabel = #1062#1077#1085#1072' '#1073#1077#1079' '#1088#1072#1073#1072#1090' '#1073#1077#1079' '#1044#1044#1042
      FieldName = 'CENA_BEZ_RABAT_BEZ_DDV'
      DisplayFormat = '###,##0.00'
      EditFormat = '0.00'
    end
    object tblPovratnicaDobavuvacStavkaREF_ID: TFIBBCDField
      DisplayLabel = #1056#1077#1092'. '#1096#1080#1092'.'
      FieldName = 'REF_ID'
      Size = 0
    end
    object tblPovratnicaDobavuvacStavkaOPIS: TFIBStringField
      DisplayLabel = #1054#1087#1080#1089
      FieldName = 'OPIS'
      Size = 200
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPovratnicaDobavuvacStavkaSLEDLIVOST_KOLICINA: TFIBSmallIntField
      DisplayLabel = #1057#1083#1077#1076#1083#1080#1074#1086#1089#1090
      FieldName = 'SLEDLIVOST_KOLICINA'
    end
    object tblPovratnicaDobavuvacStavkaCALC_DANOK_IZNOS: TFIBBCDField
      FieldKind = fkCalculated
      FieldName = 'CALC_DANOK_IZNOS'
      Size = 2
      Calculated = True
    end
    object tblPovratnicaDobavuvacStavkaART_KATALOG: TFIBStringField
      DisplayLabel = #1040#1088#1090'. '#1082#1072#1090#1072#1083#1086#1096#1082#1080' '#1073#1088'.'
      FieldName = 'ART_KATALOG'
      Size = 30
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPovratnicaDobavuvacStavkaART_BARKOD: TFIBStringField
      DisplayLabel = #1040#1088#1090'. '#1073#1072#1088#1082#1086#1076
      FieldName = 'ART_BARKOD'
      Size = 30
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblPovratnicaDobavuvacStavkaART_SKU: TFIBStringField
      DisplayLabel = #1040#1088#1090'. SKU'
      FieldName = 'ART_SKU'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object dsPovratnicaDobavuvacStavka: TDataSource
    DataSet = tblPovratnicaDobavuvacStavka
    Left = 432
    Top = 525
  end
  object tblOut: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE MTR_OUT'
      'SET '
      '    ID = :ID,'
      '    GODINA = :GODINA,'
      '    RE = :RE,'
      '    TIP_DOK = :TIP_DOK,'
      '    BROJ = :BROJ,'
      '    BROJ_DOK = :BROJ_DOK,'
      '    DATUM = :DATUM,'
      '    TP = :TP,'
      '    P = :P,'
      '    RE2 = :RE2,'
      '    OBJEKT = :OBJEKT,'
      '    OPIS = :OPIS,'
      '    REF_NO = :REF_NO,'
      '    VALUTA = :VALUTA,'
      '    KURS = :KURS,'
      '    CENOVNIK = :CENOVNIK,'
      '    FAKTURA_ID = :FAKTURA_ID,'
      '    TIP_KNIZENJE = :TIP_KNIZENJE,'
      '    NALOG = :NALOG,'
      '    REF_ID = :REF_ID,'
      '    SO_REF_ID = :SO_REF_ID,'
      '    WO_REF_ID = :WO_REF_ID,'
      '    AKTIVEN = :AKTIVEN,'
      '    KONTROLOR = :KONTROLOR,'
      '    KONTROLIRAN = :KONTROLIRAN,'
      '    KONTROLIRAN_NA = :KONTROLIRAN_NA,'
      '    LIKVIDIRAN = :LIKVIDIRAN,'
      '    LIKVIDIRAN_NA = :LIKVIDIRAN_NA,'
      '    LIKVIDATOR = :LIKVIDATOR,'
      '    PECATEN = :PECATEN,'
      '    PECATEN_NA = :PECATEN_NA,'
      '    PECATEN_OD = :PECATEN_OD,'
      '    SITE_ID = :SITE_ID,'
      '    STORNO_FLAG = :STORNO_FLAG,'
      '    STORNO_DATUM = :STORNO_DATUM,'
      '    STORNO_USER = :STORNO_USER,'
      '    STORNO_NALOG = :STORNO_NALOG_ID,'
      '    STORNO_ZABELESKA = :STORNO_ZABELESKA,'
      '    PLT_REF_ID = :PLT_REF_ID,'
      '    VOZILO = :VOZILO,'
      '    AGENT_TP = :AGENT_TP,'
      '    AGENT_P = :AGENT_P,'
      '    PLATENA_GOTOVO = :PLATENA_GOTOVO,'
      '    PLATENO = :PLATENO,'
      '    PLATENO_DATUM = :PLATENO_DATUM,'
      '    FISKAL_BR = :FISKAL_BR,'
      '    FOND_VODA = :FOND_VODA,'
      '    TREBOVANJE_ID = :TREBOVANJE_ID,'
      '    JN_BARANJE = :JN_BARANJE,'
      '    RABAT_PRESMETKA = :RABAT_PRESMETKA,'
      '    CUSTOM1 = :CUSTOM1,'
      '    CUSTOM2 = :CUSTOM2,'
      '    CUSTOM3 = :CUSTOM3,'
      '    TS_INS = :TS_INS,'
      '    TS_UPD = :TS_UPD,'
      '    USR_INS = :USR_INS,'
      '    USR_UPD = :USR_UPD'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    MTR_OUT'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO MTR_OUT('
      '    ID,'
      '    GODINA,'
      '    RE,'
      '    TIP_DOK,'
      '    BROJ,'
      '    BROJ_DOK,'
      '    DATUM,'
      '    TP,'
      '    P,'
      '    RE2,'
      '    OBJEKT,'
      '    OPIS,'
      '    REF_NO,'
      '    VALUTA,'
      '    KURS,'
      '    CENOVNIK,'
      '    FAKTURA_ID,'
      '    TIP_KNIZENJE,'
      '    NALOG,'
      '    REF_ID,'
      '    SO_REF_ID,'
      '    WO_REF_ID,'
      '    AKTIVEN,'
      '    KONTROLOR,'
      '    KONTROLIRAN,'
      '    KONTROLIRAN_NA,'
      '    LIKVIDIRAN,'
      '    LIKVIDIRAN_NA,'
      '    LIKVIDATOR,'
      '    PECATEN,'
      '    PECATEN_NA,'
      '    PECATEN_OD,'
      '    SITE_ID,'
      '    STORNO_FLAG,'
      '    STORNO_DATUM,'
      '    STORNO_USER,'
      '    STORNO_NALOG,'
      '    STORNO_ZABELESKA,'
      '    PLT_REF_ID,'
      '    VOZILO,'
      '    AGENT_TP,'
      '    AGENT_P,'
      '    PLATENA_GOTOVO,'
      '    PLATENO,'
      '    PLATENO_DATUM,'
      '    FISKAL_BR,'
      '    FOND_VODA,'
      '    TREBOVANJE_ID,'
      '    JN_BARANJE,'
      '    RABAT_PRESMETKA,'
      '    CUSTOM1,'
      '    CUSTOM2,'
      '    CUSTOM3,'
      '    TS_INS,'
      '    TS_UPD,'
      '    USR_INS,'
      '    USR_UPD'
      ')'
      'VALUES('
      '    :ID,'
      '    :GODINA,'
      '    :RE,'
      '    :TIP_DOK,'
      '    :BROJ,'
      '    :BROJ_DOK,'
      '    :DATUM,'
      '    :TP,'
      '    :P,'
      '    :RE2,'
      '    :OBJEKT,'
      '    :OPIS,'
      '    :REF_NO,'
      '    :VALUTA,'
      '    :KURS,'
      '    :CENOVNIK,'
      '    :FAKTURA_ID,'
      '    :TIP_KNIZENJE,'
      '    :NALOG,'
      '    :REF_ID,'
      '    :SO_REF_ID,'
      '    :WO_REF_ID,'
      '    :AKTIVEN,'
      '    :KONTROLOR,'
      '    :KONTROLIRAN,'
      '    :KONTROLIRAN_NA,'
      '    :LIKVIDIRAN,'
      '    :LIKVIDIRAN_NA,'
      '    :LIKVIDATOR,'
      '    :PECATEN,'
      '    :PECATEN_NA,'
      '    :PECATEN_OD,'
      '    :SITE_ID,'
      '    :STORNO_FLAG,'
      '    :STORNO_DATUM,'
      '    :STORNO_USER,'
      '    :STORNO_NALOG_ID,'
      '    :STORNO_ZABELESKA,'
      '    :PLT_REF_ID,'
      '    :VOZILO,'
      '    :AGENT_TP,'
      '    :AGENT_P,'
      '    :PLATENA_GOTOVO,'
      '    :PLATENO,'
      '    :PLATENO_DATUM,'
      '    :FISKAL_BR,'
      '    :FOND_VODA,'
      '    :TREBOVANJE_ID,'
      '    :JN_BARANJE,'
      '    :RABAT_PRESMETKA,'
      '    :CUSTOM1,'
      '    :CUSTOM2,'
      '    :CUSTOM3,'
      '    :TS_INS,'
      '    :TS_UPD,'
      '    :USR_INS,'
      '    :USR_UPD'
      ')')
    RefreshSQL.Strings = (
      'select O.ID,'
      '       O.GODINA,'
      '       O.RE,'
      '       RE.NAZIV as MAG_NAZIV,'
      '       O.TIP_DOK,'
      '       TD.NAZIV as TIP_DOK_NAZIV,'
      '       O.BROJ,'
      '       O.BROJ_DOK,'
      '       O.DATUM,'
      '       O.TP,'
      '       O.P,'
      '       P.NAZIV as PARTNER_NAZIV,'
      '       M.NAZIV as MESTO_NAZIV,'
      '       O.RE2,'
      '       R.NAZIV as RE2_NAZIV,'
      '       O.OBJEKT,'
      '       MO.NAZIV as OBJ_NAZIV,'
      '       MO.TP as OBJ_TP,'
      '       MO.P as OBJ_P,'
      '       MP.NAZIV as OBJ_PARTNER_NAZIV,'
      '       MO.REGION as OBJ_REGION,'
      '       MR.NAZIV as OBJ_REGION_NAZIV,'
      '       MO.TIP as OBJ_TIP,'
      '       MO.ADRESA as OBJ_ADRESA,'
      '       MO.MESTO as OBJ_MESTO,'
      '       MM.NAZIV as OBJ_MESTO_NAZIV,'
      '       O.OPIS,'
      '       O.REF_NO,'
      '       O.VALUTA,'
      '       V.NAZIV as VAL_NAZIV,'
      '       O.KURS,'
      '       O.CENOVNIK,'
      '       C.OPIS as CEN_OPIS,'
      '       C.VALUTA as CEN_VAL,'
      '       C.SO_DDV as CEN_DDV,'
      '       C.AKTIVEN as CEN_AKTIVEN,'
      '       C.VAZI_OD as CEN_VAZI_OD,'
      '       C.TIP as CEN_TIP,'
      '       O.FAKTURA_ID,'
      '       O.TIP_KNIZENJE,'
      '       TK.NAZIV as TK_NAZIV,'
      '       TK.KNIZENJE as TK_KNIZENJE,'
      '       O.NALOG,'
      '       NG.TIP_NALOG,'
      '       NG.NALOG as NALOG_BR,'
      '       NG.DATUM as NALOG_DATUM,'
      '       NG.OPIS as NALOG_OPIS,'
      '       NG.VID_NALOG as NALOG_VID,'
      '       O.REF_ID,'
      '       O2.RE as POVRATNICA_RE,'
      '       O2.TIP_DOK as POVRATNICA_TD,'
      '       O2.BROJ as POVRATNICA_BR,'
      '       O.SO_REF_ID,'
      '       SG.PREDMET as SAL_PREDMET,'
      '       SG.BROJ as SAL_BROJ,'
      '       SG.GODINA as SAL_GODINA,'
      '       O.WO_REF_ID,'
      '       O.AKTIVEN,'
      '       O.KONTROLOR,'
      '       O.KONTROLIRAN,'
      '       O.KONTROLIRAN_NA,'
      '       O.LIKVIDIRAN,'
      '       O.LIKVIDIRAN_NA,'
      '       O.LIKVIDATOR,'
      '       O.PECATEN,'
      '       O.PECATEN_NA,'
      '       O.PECATEN_OD,'
      '       O.SITE_ID,'
      '       SS.SITE_NAME,'
      '       O.STORNO_FLAG,'
      '       O.STORNO_DATUM,'
      '       O.STORNO_USER,'
      '       O.STORNO_NALOG as STORNO_NALOG_ID,'
      '       NG.TIP_NALOG as STORNO_TIP_NALOG,'
      '       NG.NALOG as STORNO_NALOG,'
      '       NG.DATUM as STORNO_NALOG_DATUM,'
      '       NG.OPIS as STORNO_NALOG_OPIS,'
      '       NG.VID_NALOG as STORNO_NALOG_VID,'
      '       O.STORNO_ZABELESKA,'
      '       F.ID as F_ID,'
      '       F.BROJ as F_BROJ,'
      '       F.DATUM as F_DATUM,'
      '       F.FISKAL_BR as F_FISKAL_BR,'
      '       F.PLATENA_GOTOVO as F_PLATENA_GOTOVO,'
      '       F.PLATENO as F_PLATENO,'
      '       F.PLATENO_DATUM as F_PLATENO_DATUM,'
      '       F.BROJ_DOK as F_BROJ_DOK,'
      '       O.PLT_REF_ID,'
      '       I.BROJ as PLT_BROJ,'
      '       I.BROJ_DOK as PLT_BROJ_DOK,'
      '       I.DATUM as PLT_DATUM,'
      '       O.VOZILO,'
      '       O.AGENT_TP,'
      '       O.AGENT_P,'
      '       PA.NAZIV as AGENT_NAZIV,'
      '       O.PLATENA_GOTOVO,'
      '       O.PLATENO,'
      '       O.PLATENO_DATUM,'
      '       O.FISKAL_BR,'
      '       O.FOND_VODA,'
      '       O.TREBOVANJE_ID,'
      '       T.GODINA as TREB_GOD,'
      '       T.RE as TREB_RE,'
      '       T.BROJ as TREB_BR,'
      '       RM2.VIRTUELNA_PRODAVNICA as RE2_VIRTUELNA_PROD,'
      '       O.JN_BARANJE,'
      '       O.RABAT_PRESMETKA,'
      '       O.CUSTOM1,'
      '       O.CUSTOM2,'
      '       O.CUSTOM3,'
      '       O.TS_INS,'
      '       O.TS_UPD,'
      '       O.USR_INS,'
      '       O.USR_UPD,'
      
        '       (select cast(coalesce(sum(S2.IZNOS_BEZ_DDV * O.KURS), 0) ' +
        'as numeric(15,2))'
      '        from MTR_OUT_S S2'
      '        where S2.MTR_OUT_ID = O.ID) as IZNOS_BEZ_DDV,'
      
        '       (select cast(coalesce(sum(S2.IZNOS_SO_DDV * O.KURS), 0) a' +
        's numeric(15,2))'
      '        from MTR_OUT_S S2'
      '        where S2.MTR_OUT_ID = O.ID) as IZNOS_SO_DDV,'
      
        '       (select cast(coalesce(sum(S2.KNIG_IZNOS), 0) as numeric(1' +
        '5,2))'
      '        from MTR_OUT_S S2'
      '        where S2.MTR_OUT_ID = O.ID) as KNIG_IZNOS'
      'from MTR_OUT O'
      'join MTR_RE_MAGACIN RM on RM.ID = O.RE'
      'join MAT_RE RE on RE.ID = RM.ID'
      'join MTR_TIP_DOK TD on TD.ID = O.TIP_DOK'
      'left join MAT_PARTNER P on P.TIP_PARTNER = O.TP and P.ID = O.P'
      'left join MAT_MESTO M on M.ID = P.MESTO'
      'left join MAT_RE R on R.ID = O.RE2'
      'left join MTR_RE_MAGACIN RM2 on RM2.ID = O.RE2'
      'left join MAT_OBJEKT MO on MO.ID = O.OBJEKT'
      
        'left join MAT_PARTNER MP on MP.TIP_PARTNER = MO.TP and MP.ID = M' +
        'O.P'
      'left join MAT_MESTO MM on MM.ID = MO.MESTO'
      'left join MAT_REGIONI MR on MR.ID = MO.REGION'
      'left join MAT_VALUTA V on V.ID = O.VALUTA'
      'left join MTR_CENOVNIK C on C.ID = O.CENOVNIK'
      
        'left join FIN_TIP_KNIZENJE TK on TK.TIP_KNIZENJE = O.TIP_KNIZENJ' +
        'E'
      'left join FIN_NG NG on NG.ID = O.NALOG'
      'left join FIN_NG NG2 on NG2.ID = O.STORNO_NALOG'
      'left join SAL_POR_GLAVA SG on SG.ID = O.SO_REF_ID'
      'left join MTR_OUT O2 on O2.ID = O.REF_ID'
      'left join MTR_OUT_FAK_S FS on FS.OUT_ID = O.ID'
      'left join MTR_OUT_FAK F on F.ID = FS.FAK_ID'
      'left join SYS_SITE_SETUP SS on SS.ID = O.SITE_ID'
      
        'left join MAT_PARTNER PA on PA.TIP_PARTNER = O.AGENT_TP and PA.I' +
        'D = O.AGENT_P'
      'left join MTR_IN I on I.ID = O.PLT_REF_ID'
      'left join MTR_TREBUVANJE T on T.ID = O.TREBOVANJE_ID'
      'where(  O.RE = :MAGACIN'
      '      and O.GODINA = :GOD'
      
        '      and ((O.TIP_DOK = 11) or (O.TIP_DOK = 16)) -- fiksno 11 za' +
        ' ispratnici ili 16 za revers'
      '     ) and (     O.ID = :OLD_ID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      'select O.ID,'
      '       O.GODINA,'
      '       O.RE,'
      '       RE.NAZIV as MAG_NAZIV,'
      '       O.TIP_DOK,'
      '       TD.NAZIV as TIP_DOK_NAZIV,'
      '       O.BROJ,'
      '       O.BROJ_DOK,'
      '       O.DATUM,'
      '       O.TP,'
      '       O.P,'
      '       P.NAZIV as PARTNER_NAZIV,'
      '       M.NAZIV as MESTO_NAZIV,'
      '       O.RE2,'
      '       R.NAZIV as RE2_NAZIV,'
      '       O.OBJEKT,'
      '       MO.NAZIV as OBJ_NAZIV,'
      '       MO.TP as OBJ_TP,'
      '       MO.P as OBJ_P,'
      '       MP.NAZIV as OBJ_PARTNER_NAZIV,'
      '       MO.REGION as OBJ_REGION,'
      '       MR.NAZIV as OBJ_REGION_NAZIV,'
      '       MO.TIP as OBJ_TIP,'
      '       MO.ADRESA as OBJ_ADRESA,'
      '       MO.MESTO as OBJ_MESTO,'
      '       MM.NAZIV as OBJ_MESTO_NAZIV,'
      '       O.OPIS,'
      '       O.REF_NO,'
      '       O.VALUTA,'
      '       V.NAZIV as VAL_NAZIV,'
      '       O.KURS,'
      '       O.CENOVNIK,'
      '       C.OPIS as CEN_OPIS,'
      '       C.VALUTA as CEN_VAL,'
      '       C.SO_DDV as CEN_DDV,'
      '       C.AKTIVEN as CEN_AKTIVEN,'
      '       C.VAZI_OD as CEN_VAZI_OD,'
      '       C.TIP as CEN_TIP,'
      '       O.FAKTURA_ID,'
      '       O.TIP_KNIZENJE,'
      '       TK.NAZIV as TK_NAZIV,'
      '       TK.KNIZENJE as TK_KNIZENJE,'
      '       O.NALOG,'
      '       NG.TIP_NALOG,'
      '       NG.NALOG as NALOG_BR,'
      '       NG.DATUM as NALOG_DATUM,'
      '       NG.OPIS as NALOG_OPIS,'
      '       NG.VID_NALOG as NALOG_VID,'
      '       O.REF_ID,'
      '       O2.RE as POVRATNICA_RE,'
      '       O2.TIP_DOK as POVRATNICA_TD,'
      '       O2.BROJ as POVRATNICA_BR,'
      '       O.SO_REF_ID,'
      '       SG.PREDMET as SAL_PREDMET,'
      '       SG.BROJ as SAL_BROJ,'
      '       SG.GODINA as SAL_GODINA,'
      '       O.WO_REF_ID,'
      '       O.AKTIVEN,'
      '       O.KONTROLOR,'
      '       O.KONTROLIRAN,'
      '       O.KONTROLIRAN_NA,'
      '       O.LIKVIDIRAN,'
      '       O.LIKVIDIRAN_NA,'
      '       O.LIKVIDATOR,'
      '       O.PECATEN,'
      '       O.PECATEN_NA,'
      '       O.PECATEN_OD,'
      '       O.SITE_ID,'
      '       SS.SITE_NAME,'
      '       O.STORNO_FLAG,'
      '       O.STORNO_DATUM,'
      '       O.STORNO_USER,'
      '       O.STORNO_NALOG as STORNO_NALOG_ID,'
      '       NG.TIP_NALOG as STORNO_TIP_NALOG,'
      '       NG.NALOG as STORNO_NALOG,'
      '       NG.DATUM as STORNO_NALOG_DATUM,'
      '       NG.OPIS as STORNO_NALOG_OPIS,'
      '       NG.VID_NALOG as STORNO_NALOG_VID,'
      '       O.STORNO_ZABELESKA,'
      '       F.ID as F_ID,'
      '       F.BROJ as F_BROJ,'
      '       F.DATUM as F_DATUM,'
      '       F.FISKAL_BR as F_FISKAL_BR,'
      '       F.PLATENA_GOTOVO as F_PLATENA_GOTOVO,'
      '       F.PLATENO as F_PLATENO,'
      '       F.PLATENO_DATUM as F_PLATENO_DATUM,'
      '       F.BROJ_DOK as F_BROJ_DOK,'
      '       O.PLT_REF_ID,'
      '       I.BROJ as PLT_BROJ,'
      '       I.BROJ_DOK as PLT_BROJ_DOK,'
      '       I.DATUM as PLT_DATUM,'
      '       O.VOZILO,'
      '       O.AGENT_TP,'
      '       O.AGENT_P,'
      '       PA.NAZIV as AGENT_NAZIV,'
      '       O.PLATENA_GOTOVO,'
      '       O.PLATENO,'
      '       O.PLATENO_DATUM,'
      '       O.FISKAL_BR,'
      '       O.FOND_VODA,'
      '       O.TREBOVANJE_ID,'
      '       T.GODINA as TREB_GOD,'
      '       T.RE as TREB_RE,'
      '       T.BROJ as TREB_BR,'
      '       RM2.VIRTUELNA_PRODAVNICA as RE2_VIRTUELNA_PROD,'
      '       O.JN_BARANJE,'
      '       O.RABAT_PRESMETKA,'
      '       O.CUSTOM1,'
      '       O.CUSTOM2,'
      '       O.CUSTOM3,'
      '       O.TS_INS,'
      '       O.TS_UPD,'
      '       O.USR_INS,'
      '       O.USR_UPD,'
      
        '       (select cast(coalesce(sum(S2.IZNOS_BEZ_DDV * O.KURS), 0) ' +
        'as numeric(15,2))'
      '        from MTR_OUT_S S2'
      '        where S2.MTR_OUT_ID = O.ID) as IZNOS_BEZ_DDV,'
      
        '       (select cast(coalesce(sum(S2.IZNOS_SO_DDV * O.KURS), 0) a' +
        's numeric(15,2))'
      '        from MTR_OUT_S S2'
      '        where S2.MTR_OUT_ID = O.ID) as IZNOS_SO_DDV,'
      
        '       (select cast(coalesce(sum(S2.KNIG_IZNOS), 0) as numeric(1' +
        '5,2))'
      '        from MTR_OUT_S S2'
      '        where S2.MTR_OUT_ID = O.ID) as KNIG_IZNOS'
      'from MTR_OUT O'
      'join MTR_RE_MAGACIN RM on RM.ID = O.RE'
      'join MAT_RE RE on RE.ID = RM.ID'
      'join MTR_TIP_DOK TD on TD.ID = O.TIP_DOK'
      'left join MAT_PARTNER P on P.TIP_PARTNER = O.TP and P.ID = O.P'
      'left join MAT_MESTO M on M.ID = P.MESTO'
      'left join MAT_RE R on R.ID = O.RE2'
      'left join MTR_RE_MAGACIN RM2 on RM2.ID = O.RE2'
      'left join MAT_OBJEKT MO on MO.ID = O.OBJEKT'
      
        'left join MAT_PARTNER MP on MP.TIP_PARTNER = MO.TP and MP.ID = M' +
        'O.P'
      'left join MAT_MESTO MM on MM.ID = MO.MESTO'
      'left join MAT_REGIONI MR on MR.ID = MO.REGION'
      'left join MAT_VALUTA V on V.ID = O.VALUTA'
      'left join MTR_CENOVNIK C on C.ID = O.CENOVNIK'
      
        'left join FIN_TIP_KNIZENJE TK on TK.TIP_KNIZENJE = O.TIP_KNIZENJ' +
        'E'
      'left join FIN_NG NG on NG.ID = O.NALOG'
      'left join FIN_NG NG2 on NG2.ID = O.STORNO_NALOG'
      'left join SAL_POR_GLAVA SG on SG.ID = O.SO_REF_ID'
      'left join MTR_OUT O2 on O2.ID = O.REF_ID'
      'left join MTR_OUT_FAK_S FS on FS.OUT_ID = O.ID'
      'left join MTR_OUT_FAK F on F.ID = FS.FAK_ID'
      'left join SYS_SITE_SETUP SS on SS.ID = O.SITE_ID'
      
        'left join MAT_PARTNER PA on PA.TIP_PARTNER = O.AGENT_TP and PA.I' +
        'D = O.AGENT_P'
      'left join MTR_IN I on I.ID = O.PLT_REF_ID'
      'left join MTR_TREBUVANJE T on T.ID = O.TREBOVANJE_ID'
      'where O.RE = :MAGACIN'
      '      and O.GODINA = :GOD'
      
        '      and ((O.TIP_DOK = 11) or (O.TIP_DOK = 16)) -- fiksno 11 za' +
        ' ispratnici ili 16 za revers'
      'order by O.ID desc  ')
    AutoUpdateOptions.UpdateTableName = 'MTR_OUT'
    AutoUpdateOptions.KeyFields = 'ID'
    AutoUpdateOptions.GeneratorName = 'GEN_MTR_OUT_ID'
    AutoUpdateOptions.WhenGetGenID = wgBeforePost
    BeforeDelete = TabelaBeforeDelete
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 472
    Top = 24
    poSQLINT64ToBCD = True
    object tblOutID: TFIBBCDField
      DisplayLabel = #1064#1080#1092'.'
      FieldName = 'ID'
      Size = 0
    end
    object tblOutGODINA: TFIBIntegerField
      DisplayLabel = #1043#1086#1076#1080#1085#1072
      FieldName = 'GODINA'
    end
    object tblOutRE: TFIBIntegerField
      DisplayLabel = #1056#1045
      FieldName = 'RE'
    end
    object tblOutMAG_NAZIV: TFIBStringField
      DisplayLabel = #1052#1072#1075'. '#1085#1072#1079#1080#1074
      FieldName = 'MAG_NAZIV'
      Size = 255
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblOutTIP_DOK: TFIBIntegerField
      DisplayLabel = #1058#1080#1087' '#1076#1086#1082'. '#1096#1080#1092'.'
      FieldName = 'TIP_DOK'
    end
    object tblOutTIP_DOK_NAZIV: TFIBStringField
      DisplayLabel = #1058#1080#1087' '#1076#1086#1082'.'
      FieldName = 'TIP_DOK_NAZIV'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblOutBROJ: TFIBIntegerField
      DisplayLabel = #1041#1088#1086#1112
      FieldName = 'BROJ'
    end
    object tblOutDATUM: TFIBDateField
      DisplayLabel = #1044#1072#1090#1091#1084
      FieldName = 'DATUM'
    end
    object tblOutTP: TFIBIntegerField
      DisplayLabel = #1058#1055
      FieldName = 'TP'
    end
    object tblOutP: TFIBIntegerField
      DisplayLabel = #1055
      FieldName = 'P'
    end
    object tblOutPARTNER_NAZIV: TFIBStringField
      DisplayLabel = #1055#1072#1088#1090#1085#1077#1088
      FieldName = 'PARTNER_NAZIV'
      Size = 255
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblOutMESTO_NAZIV: TFIBStringField
      DisplayLabel = #1052#1077#1089#1090#1086
      FieldName = 'MESTO_NAZIV'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblOutRE2: TFIBIntegerField
      DisplayLabel = #1056#1072#1073'.'#1077#1076'.'
      FieldName = 'RE2'
    end
    object tblOutRE2_NAZIV: TFIBStringField
      DisplayLabel = #1056#1072#1073'.'#1077#1076'. '#1085#1072#1079#1080#1074
      FieldName = 'RE2_NAZIV'
      Size = 255
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblOutOBJEKT: TFIBIntegerField
      DisplayLabel = #1054#1073#1112'. '#1096#1080#1092'.'
      FieldName = 'OBJEKT'
    end
    object tblOutOBJ_NAZIV: TFIBStringField
      DisplayLabel = #1054#1073#1112#1077#1082#1090
      FieldName = 'OBJ_NAZIV'
      Size = 255
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblOutOBJ_TP: TFIBIntegerField
      DisplayLabel = #1054#1073#1112'. '#1058#1055
      FieldName = 'OBJ_TP'
    end
    object tblOutOBJ_P: TFIBIntegerField
      DisplayLabel = #1054#1073#1112'. '#1055
      FieldName = 'OBJ_P'
    end
    object tblOutOBJ_PARTNER_NAZIV: TFIBStringField
      DisplayLabel = #1054#1073#1112'. '#1087#1072#1088#1090#1085#1077#1088
      FieldName = 'OBJ_PARTNER_NAZIV'
      Size = 255
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblOutOBJ_REGION: TFIBIntegerField
      DisplayLabel = #1054#1073#1112'. '#1088#1077#1075#1080#1086#1085' '#1096#1080#1092'.'
      FieldName = 'OBJ_REGION'
    end
    object tblOutOBJ_REGION_NAZIV: TFIBStringField
      DisplayLabel = #1054#1073#1112'. '#1088#1077#1075#1080#1086#1085
      FieldName = 'OBJ_REGION_NAZIV'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblOutOBJ_TIP: TFIBIntegerField
      DisplayLabel = #1054#1073#1112'. '#1090#1080#1087
      FieldName = 'OBJ_TIP'
    end
    object tblOutOBJ_ADRESA: TFIBStringField
      DisplayLabel = #1054#1073#1112'. '#1072#1076#1088#1077#1089#1072
      FieldName = 'OBJ_ADRESA'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblOutOBJ_MESTO: TFIBIntegerField
      DisplayLabel = #1054#1073#1112'. '#1084#1077#1089#1090#1086' '#1096#1080#1092'.'
      FieldName = 'OBJ_MESTO'
    end
    object tblOutOBJ_MESTO_NAZIV: TFIBStringField
      DisplayLabel = #1054#1073#1112'. '#1084#1077#1089#1090#1086
      FieldName = 'OBJ_MESTO_NAZIV'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblOutOPIS: TFIBStringField
      DisplayLabel = #1054#1087#1080#1089
      FieldName = 'OPIS'
      Size = 200
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblOutREF_NO: TFIBStringField
      DisplayLabel = #1056#1077#1092'. '#1073#1088'.'
      FieldName = 'REF_NO'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblOutVALUTA: TFIBStringField
      DisplayLabel = #1042#1072#1083#1091#1090#1072
      FieldName = 'VALUTA'
      Size = 3
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblOutVAL_NAZIV: TFIBStringField
      DisplayLabel = #1042#1072#1083'. '#1085#1072#1079#1080#1074
      FieldName = 'VAL_NAZIV'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblOutKURS: TFIBBCDField
      DisplayLabel = #1050#1091#1088#1089
      FieldName = 'KURS'
      Size = 5
    end
    object tblOutCENOVNIK: TFIBIntegerField
      DisplayLabel = #1062#1077#1085#1086#1074#1085#1080#1082
      FieldName = 'CENOVNIK'
    end
    object tblOutCEN_OPIS: TFIBStringField
      DisplayLabel = #1062#1077#1085'. '#1086#1087#1080#1089
      FieldName = 'CEN_OPIS'
      Size = 255
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblOutCEN_VAL: TFIBStringField
      DisplayLabel = #1062#1077#1085'. '#1074#1072#1083#1091#1090#1072
      FieldName = 'CEN_VAL'
      Size = 3
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblOutCEN_DDV: TFIBSmallIntField
      DisplayLabel = #1062#1077#1085'. '#1044#1044#1042
      FieldName = 'CEN_DDV'
    end
    object tblOutCEN_AKTIVEN: TFIBSmallIntField
      DisplayLabel = #1062#1077#1085'. '#1072#1082#1090#1080#1074#1077#1085
      FieldName = 'CEN_AKTIVEN'
    end
    object tblOutCEN_VAZI_OD: TFIBDateField
      DisplayLabel = #1062#1077#1085'. '#1074#1072#1078#1080' '#1086#1076
      FieldName = 'CEN_VAZI_OD'
    end
    object tblOutCEN_TIP: TFIBSmallIntField
      DisplayLabel = #1062#1077#1085'. '#1090#1080#1087
      FieldName = 'CEN_TIP'
    end
    object tblOutFAKTURA_ID: TFIBBCDField
      DisplayLabel = #1060#1072#1082#1090#1091#1088#1072' '#1096#1080#1092'.'
      FieldName = 'FAKTURA_ID'
      Size = 0
    end
    object tblOutTIP_KNIZENJE: TFIBIntegerField
      DisplayLabel = #1058#1080#1087' '#1082#1085#1080#1078'. '#1096#1080#1092'.'
      FieldName = 'TIP_KNIZENJE'
    end
    object tblOutTK_NAZIV: TFIBStringField
      DisplayLabel = #1058#1080#1087' '#1082#1085#1080#1078#1077#1114#1077
      FieldName = 'TK_NAZIV'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblOutTK_KNIZENJE: TFIBIntegerField
      DisplayLabel = #1058#1080#1087' '#1082#1085#1080#1078'. '#1073#1088#1086#1112
      FieldName = 'TK_KNIZENJE'
    end
    object tblOutTIP_NALOG: TFIBStringField
      DisplayLabel = #1058#1080#1087' '#1085#1072#1083#1086#1075
      FieldName = 'TIP_NALOG'
      Size = 5
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblOutNALOG: TFIBBCDField
      DisplayLabel = #1053#1072#1083#1086#1075' '#1096#1080#1092'.'
      FieldName = 'NALOG'
      Size = 0
    end
    object tblOutNALOG_BR: TFIBIntegerField
      DisplayLabel = #1053#1072#1083#1086#1075
      FieldName = 'NALOG_BR'
    end
    object tblOutNALOG_DATUM: TFIBDateField
      DisplayLabel = #1053#1072#1083#1086#1075' '#1076#1072#1090#1091#1084
      FieldName = 'NALOG_DATUM'
    end
    object tblOutNALOG_OPIS: TFIBStringField
      DisplayLabel = #1053#1072#1083#1086#1075' '#1086#1087#1080#1089
      FieldName = 'NALOG_OPIS'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblOutNALOG_VID: TFIBSmallIntField
      DisplayLabel = #1053#1072#1083#1086#1075' '#1074#1080#1076
      FieldName = 'NALOG_VID'
    end
    object tblOutREF_ID: TFIBBCDField
      FieldName = 'REF_ID'
      Size = 0
    end
    object tblOutSO_REF_ID: TFIBIntegerField
      DisplayLabel = #1055#1088#1086#1076#1072#1078#1073#1072' '#1096#1080#1092'.'
      FieldName = 'SO_REF_ID'
    end
    object tblOutSAL_PREDMET: TFIBStringField
      DisplayLabel = #1055#1088#1086#1076#1072#1078#1073#1072' '#1087#1088#1077#1076#1084#1077#1090
      FieldName = 'SAL_PREDMET'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblOutSAL_BROJ: TFIBIntegerField
      DisplayLabel = #1055#1088#1086#1076#1072#1078#1073#1072' '#1073#1088'.'
      FieldName = 'SAL_BROJ'
    end
    object tblOutSAL_GODINA: TFIBIntegerField
      DisplayLabel = #1055#1088#1086#1076#1072#1078#1073#1072' '#1075#1086#1076'.'
      FieldName = 'SAL_GODINA'
    end
    object tblOutAKTIVEN: TFIBSmallIntField
      DisplayLabel = #1040#1082#1090#1080#1074#1077#1085
      FieldName = 'AKTIVEN'
    end
    object tblOutKONTROLOR: TFIBStringField
      DisplayLabel = #1050#1086#1085#1090#1088#1086#1083#1086#1088
      FieldName = 'KONTROLOR'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblOutKONTROLIRAN: TFIBSmallIntField
      DisplayLabel = #1050#1086#1085#1090#1088#1086#1083#1080#1088#1072#1085#1086
      FieldName = 'KONTROLIRAN'
    end
    object tblOutKONTROLIRAN_NA: TFIBDateTimeField
      DisplayLabel = #1044#1072#1090#1091#1084' '#1082#1086#1085#1090#1088#1086#1083#1072
      FieldName = 'KONTROLIRAN_NA'
    end
    object tblOutLIKVIDIRAN: TFIBSmallIntField
      DisplayLabel = #1051#1080#1082#1074#1080#1076#1080#1088#1072#1085#1086
      FieldName = 'LIKVIDIRAN'
    end
    object tblOutLIKVIDIRAN_NA: TFIBDateTimeField
      DisplayLabel = #1044#1072#1090#1091#1084' '#1083#1080#1082#1074#1080#1076#1080#1088#1072#1085#1086
      FieldName = 'LIKVIDIRAN_NA'
    end
    object tblOutLIKVIDATOR: TFIBStringField
      DisplayLabel = #1051#1080#1082#1074#1080#1076#1072#1090#1086#1088
      FieldName = 'LIKVIDATOR'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblOutPECATEN: TFIBSmallIntField
      DisplayLabel = #1055#1077#1095#1072#1090#1077#1085#1086
      FieldName = 'PECATEN'
    end
    object tblOutPECATEN_NA: TFIBDateTimeField
      DisplayLabel = #1044#1072#1090#1091#1084' '#1087#1077#1095#1072#1090#1077#1085#1086
      FieldName = 'PECATEN_NA'
    end
    object tblOutPECATEN_OD: TFIBStringField
      DisplayLabel = #1055#1077#1095#1072#1090#1077#1085#1086' '#1086#1076
      FieldName = 'PECATEN_OD'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblOutSITE_ID: TFIBSmallIntField
      DisplayLabel = #1057#1072#1112#1090' '#1096#1080#1092'.'
      FieldName = 'SITE_ID'
    end
    object tblOutSITE_NAME: TFIBStringField
      DisplayLabel = #1057#1072#1112#1090
      FieldName = 'SITE_NAME'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblOutCUSTOM1: TFIBStringField
      FieldName = 'CUSTOM1'
      Size = 200
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblOutCUSTOM2: TFIBStringField
      FieldName = 'CUSTOM2'
      Size = 200
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblOutCUSTOM3: TFIBStringField
      FieldName = 'CUSTOM3'
      Size = 200
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblOutTS_INS: TFIBDateTimeField
      FieldName = 'TS_INS'
    end
    object tblOutTS_UPD: TFIBDateTimeField
      FieldName = 'TS_UPD'
    end
    object tblOutUSR_INS: TFIBStringField
      FieldName = 'USR_INS'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblOutUSR_UPD: TFIBStringField
      FieldName = 'USR_UPD'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblOutPOVRATNICA_RE: TFIBIntegerField
      DisplayLabel = #1055#1086#1074#1088#1072#1090#1085#1080#1094#1072' '#1084#1072#1075'.'
      FieldName = 'POVRATNICA_RE'
    end
    object tblOutPOVRATNICA_TD: TFIBIntegerField
      DisplayLabel = #1055#1086#1074#1088#1072#1090#1085#1080#1094#1072' '#1090#1080#1087' '#1076#1086#1082'.'
      FieldName = 'POVRATNICA_TD'
    end
    object tblOutPOVRATNICA_BR: TFIBIntegerField
      DisplayLabel = #1055#1086#1074#1088#1072#1090#1085#1080#1094#1072' '#1073#1088'.'
      FieldName = 'POVRATNICA_BR'
    end
    object tblOutSTORNO_FLAG: TFIBSmallIntField
      DisplayLabel = #1057#1090#1086#1088#1085#1080#1088#1072#1085#1086
      FieldName = 'STORNO_FLAG'
    end
    object tblOutSTORNO_DATUM: TFIBDateField
      DisplayLabel = #1057#1090#1086#1088#1085#1086' '#1076#1072#1090#1091#1084
      FieldName = 'STORNO_DATUM'
    end
    object tblOutSTORNO_USER: TFIBStringField
      DisplayLabel = #1057#1090#1086#1088#1085#1080#1088#1072#1083
      FieldName = 'STORNO_USER'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblOutSTORNO_NALOG_ID: TFIBBCDField
      DisplayLabel = #1057#1090#1086#1088#1085#1086' '#1085#1072#1083#1086#1075' '#1096#1080#1092'.'
      FieldName = 'STORNO_NALOG_ID'
      Size = 0
    end
    object tblOutSTORNO_TIP_NALOG: TFIBStringField
      DisplayLabel = #1057#1090#1086#1088#1085#1086' '#1090#1080#1087' '#1085#1072#1083#1086#1075
      FieldName = 'STORNO_TIP_NALOG'
      Size = 5
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblOutSTORNO_NALOG: TFIBIntegerField
      DisplayLabel = #1057#1090#1086#1088#1085#1086' '#1085#1072#1083#1086#1075
      FieldName = 'STORNO_NALOG'
    end
    object tblOutSTORNO_NALOG_DATUM: TFIBDateField
      DisplayLabel = #1057#1090#1086#1088#1085#1086' '#1085#1072#1083#1086#1075' '#1076#1072#1090#1091#1084
      FieldName = 'STORNO_NALOG_DATUM'
    end
    object tblOutSTORNO_NALOG_OPIS: TFIBStringField
      DisplayLabel = #1057#1090#1086#1088#1085#1086' '#1085#1072#1083#1086#1075' '#1086#1087#1080#1089
      FieldName = 'STORNO_NALOG_OPIS'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblOutSTORNO_NALOG_VID: TFIBSmallIntField
      DisplayLabel = #1057#1090#1086#1088#1085#1086' '#1074#1080#1076' '#1085#1072#1083#1086#1075
      FieldName = 'STORNO_NALOG_VID'
    end
    object tblOutSTORNO_ZABELESKA: TFIBStringField
      DisplayLabel = #1057#1090#1086#1088#1085#1086' '#1079#1072#1073#1077#1083#1077#1096#1082#1072
      FieldName = 'STORNO_ZABELESKA'
      Size = 200
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblOutWO_REF_ID: TFIBBCDField
      DisplayLabel = #1056#1072#1073'. '#1085#1072#1083#1086#1075' '#1096#1080#1092'.'
      FieldName = 'WO_REF_ID'
      Size = 0
    end
    object tblOutF_BROJ: TFIBIntegerField
      DisplayLabel = #1060#1072#1082'. '#1073#1088'.'
      FieldName = 'F_BROJ'
    end
    object tblOutF_DATUM: TFIBDateField
      DisplayLabel = #1060#1072#1082'. '#1076#1072#1090#1091#1084
      FieldName = 'F_DATUM'
    end
    object tblOutF_ID: TFIBBCDField
      DisplayLabel = #1060#1072#1082'. '#1096#1080#1092'.'
      FieldName = 'F_ID'
      Size = 0
    end
    object tblOutF_BROJ_DOK: TFIBStringField
      DisplayLabel = #1060#1072#1082'. '#1076#1086#1082'. '#1073#1088'.'
      FieldName = 'F_BROJ_DOK'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblOutBROJ_DOK: TFIBStringField
      DisplayLabel = #1044#1086#1082'. '#1073#1088'.'
      FieldName = 'BROJ_DOK'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblOutPLT_REF_ID: TFIBBCDField
      DisplayLabel = #1055#1051#1058' '#1088#1077#1092'. '#1096#1080#1092'.'
      FieldName = 'PLT_REF_ID'
      Size = 0
    end
    object tblOutVOZILO: TFIBIntegerField
      DisplayLabel = #1042#1086#1079#1080#1083#1086' '#1096#1080#1092'.'
      FieldName = 'VOZILO'
    end
    object tblOutAGENT_NAZIV: TFIBStringField
      DisplayLabel = #1040#1075#1077#1085#1090
      FieldName = 'AGENT_NAZIV'
      Size = 255
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblOutAGENT_TP: TFIBIntegerField
      DisplayLabel = #1058#1055' '#1040#1075#1077#1085#1090
      FieldName = 'AGENT_TP'
    end
    object tblOutAGENT_P: TFIBIntegerField
      DisplayLabel = #1055' '#1040#1075#1077#1085#1090
      FieldName = 'AGENT_P'
    end
    object tblOutPLATENA_GOTOVO: TFIBSmallIntField
      DisplayLabel = #1053#1072#1095#1080#1085' '#1087#1083#1072#1116#1072#1114#1077
      FieldName = 'PLATENA_GOTOVO'
    end
    object tblOutPLATENO: TFIBSmallIntField
      DisplayLabel = #1055#1083#1072#1090#1077#1085#1086
      FieldName = 'PLATENO'
    end
    object tblOutPLATENO_DATUM: TFIBDateField
      DisplayLabel = #1055#1083#1072#1090#1077#1085#1086' '#1076#1072#1090#1091#1084
      FieldName = 'PLATENO_DATUM'
    end
    object tblOutPLT_BROJ: TFIBIntegerField
      DisplayLabel = #1055#1051#1058' '#1073#1088'.'
      FieldName = 'PLT_BROJ'
    end
    object tblOutPLT_BROJ_DOK: TFIBStringField
      DisplayLabel = #1055#1051#1058' '#1073#1088'. '#1076#1086#1082'.'
      FieldName = 'PLT_BROJ_DOK'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblOutPLT_DATUM: TFIBDateField
      DisplayLabel = #1055#1051#1058' '#1076#1072#1090#1091#1084
      FieldName = 'PLT_DATUM'
    end
    object tblOutF_FISKAL_BR: TFIBIntegerField
      DisplayLabel = #1060#1072#1082'. '#1092#1080#1089#1082#1072#1083#1085#1072
      FieldName = 'F_FISKAL_BR'
    end
    object tblOutFISKAL_BR: TFIBIntegerField
      DisplayLabel = #1060#1080#1089#1082#1072#1083#1085#1072
      FieldName = 'FISKAL_BR'
    end
    object tblOutFOND_VODA: TFIBBCDField
      DisplayLabel = #1060#1086#1085#1076' '#1074#1086#1076#1072
      FieldName = 'FOND_VODA'
      Size = 2
    end
    object tblOutTREBOVANJE_ID: TFIBBCDField
      DisplayLabel = #1058#1088#1077#1073'. '#1096#1080#1092'.'
      FieldName = 'TREBOVANJE_ID'
      Size = 0
    end
    object tblOutTREB_GOD: TFIBIntegerField
      DisplayLabel = #1058#1088#1077#1073'. '#1075#1086#1076'.'
      FieldName = 'TREB_GOD'
    end
    object tblOutTREB_RE: TFIBIntegerField
      DisplayLabel = #1058#1088#1077#1073'. '#1056#1045
      FieldName = 'TREB_RE'
    end
    object tblOutTREB_BR: TFIBIntegerField
      DisplayLabel = #1058#1088#1077#1073'. '#1073#1088'.'
      FieldName = 'TREB_BR'
    end
    object tblOutRE2_VIRTUELNA_PROD: TFIBSmallIntField
      DisplayLabel = #1056#1045'2 '#1074#1080#1088#1090#1091#1077#1083#1085#1072' '#1087#1088#1086#1076'.'
      FieldName = 'RE2_VIRTUELNA_PROD'
    end
    object tblOutJN_BARANJE: TFIBStringField
      DisplayLabel = #1041#1072#1088#1072#1114#1077' '#1073#1088'.  '#1112#1072#1074#1085#1080' '#1085#1072#1073'.'
      FieldName = 'JN_BARANJE'
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblOutF_PLATENA_GOTOVO: TFIBSmallIntField
      DisplayLabel = #1060#1072#1082'. '#1085#1072#1095#1080#1085' '#1087#1083#1072#1116#1072#1114#1077
      FieldName = 'F_PLATENA_GOTOVO'
    end
    object tblOutF_PLATENO: TFIBSmallIntField
      DisplayLabel = #1060#1072#1082'. '#1087#1083#1072#1090#1077#1085#1086
      FieldName = 'F_PLATENO'
    end
    object tblOutF_PLATENO_DATUM: TFIBDateField
      DisplayLabel = #1060#1072#1082'. '#1087#1083#1072#1090#1077#1085#1086' '#1076#1072#1090#1091#1084
      FieldName = 'F_PLATENO_DATUM'
    end
    object tblOutIZNOS_BEZ_DDV: TFIBBCDField
      DisplayLabel = #1042#1082'. '#1080#1079#1085#1086#1089' '#1073#1077#1079' '#1044#1044#1042
      FieldName = 'IZNOS_BEZ_DDV'
      Size = 2
    end
    object tblOutIZNOS_SO_DDV: TFIBBCDField
      DisplayLabel = #1042#1082'. '#1080#1079#1085#1086#1089' '#1089#1086' '#1044#1044#1042
      FieldName = 'IZNOS_SO_DDV'
      Size = 2
    end
    object tblOutKNIG_IZNOS: TFIBBCDField
      DisplayLabel = #1042#1082'. '#1082#1085#1080#1075'. '#1080#1079#1085#1086#1089
      FieldName = 'KNIG_IZNOS'
      Size = 2
    end
    object tblOutRABAT_PRESMETKA: TFIBSmallIntField
      DisplayLabel = #1056#1072#1073#1072#1090' '#1087#1088#1077#1089#1084#1077#1090#1082#1072
      FieldName = 'RABAT_PRESMETKA'
    end
  end
  object dsOut: TDataSource
    DataSet = tblOut
    Left = 408
    Top = 24
  end
end
