unit KTVlez;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ExtCtrls, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxStyles, dxSkinsCore, dxSkinBlack,
  dxSkinBlue, dxSkinBlueprint, dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom,
  dxSkinDarkSide, dxSkinDevExpressDarkStyle, dxSkinDevExpressStyle, dxSkinFoggy,
  dxSkinGlassOceans, dxSkinHighContrast, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven,
  dxSkinSevenClassic, dxSkinSharp, dxSkinSharpPlus, dxSkinSilver,
  dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008, dxSkinTheAsphaltWorld,
  dxSkinsDefaultPainters, dxSkinValentine, dxSkinVS2010, dxSkinWhiteprint,
  dxSkinXmas2008Blue, dxSkinscxPCPainter, cxCustomData, cxFilter, cxData,
  cxDataStorage, cxEdit, Data.DB, cxDBData, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGridLevel, cxClasses, cxGridCustomView,
  cxGrid, cxTrackBar, cxSpinEdit, cxContainer, cxLabel, cxDBLabel,
  cxDropDownEdit, cxMRUEdit, cxDBEdit, cxHyperLinkEdit, cxTextEdit, cxMaskEdit,
  cxButtonEdit, cxImageComboBox, cxImage, Vcl.ActnList, DateUtils, Vcl.Menus,
  Vcl.StdCtrls, cxButtons, Vcl.ImgList, FIBQuery, pFIBQuery, cxCheckBox,
  cxGroupBox;

type
  TfrmKTVlez = class(TForm)
    PanelGore: TPanel;
    PanelDesno: TPanel;
    PanelSredina: TPanel;
    PanelRN: TPanel;
    cxGrid2: TcxGrid;
    cxGrid2DBTableView1: TcxGridDBTableView;
    cxGrid2Level1: TcxGridLevel;
    cxGrid2DBTableView1BROJ: TcxGridDBColumn;
    cxGrid2DBTableView1DATUM_PLANIRAN_POCETOK: TcxGridDBColumn;
    cxGrid2DBTableView1ZABELESKA: TcxGridDBColumn;
    lblRN: TcxDBLabel;
    ActionList1: TActionList;
    aPrezemiSite: TAction;
    btRN: TcxButton;
    aIzberiRN: TAction;
    aSlednaKT: TAction;
    aNamali: TAction;
    aZgolemi: TAction;
    btSokrijZavrseni: TcxButton;
    btZavrseni: TcxButton;
    btResetiraj: TcxButton;
    btOznaciP: TcxButton;
    btZapisi: TcxButton;
    aZapisi: TAction;
    aIzlez: TAction;
    cxImageList1: TcxImageList;
    cxImageList2: TcxImageList;
    cxMainLarge: TcxImageList;
    cxButton3: TcxButton;
    aNamali10: TAction;
    aZgolemi10: TAction;
    qUpdateRN: TpFIBUpdateObject;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1DBTableView1ZABELESKA: TcxGridDBColumn;
    cxGrid1DBTableView1NAZIV_PARTNER_PORACKA: TcxGridDBColumn;
    cxGrid1DBTableView1NAZIV_PROIZVOD: TcxGridDBColumn;
    cxGrid1DBTableView1OPIS_PROIZVOD: TcxGridDBColumn;
    cxGrid1DBTableView1ZAVRSENI: TcxGridDBColumn;
    cxGrid1DBTableView1MINUS: TcxGridDBColumn;
    cxGrid1DBTableView1PLUS: TcxGridDBColumn;
    cxGrid1DBTableView1ID_RN_STAVKA: TcxGridDBColumn;
    cxGrid1DBTableView1ZAVRSENO: TcxGridDBColumn;
    cxGrid1DBTableView1ZATVORI: TcxGridDBColumn;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1DBTableView1NAZIV_KT: TcxGridDBColumn;
    cxGrid1DBTableView1ID_KT_OPERACIJA: TcxGridDBColumn;
    cxGrid1DBTableView1NAZIV_OPERACIJA: TcxGridDBColumn;
    cxGrid1DBTableView1ID_KT: TcxGridDBColumn;
    cxGrid1DBTableView1NAZIV_OKT: TcxGridDBColumn;
    cxGrid1DBTableView1ID_RABOTEN_NALOG: TcxGridDBColumn;
    cxGrid1DBTableView1ID: TcxGridDBColumn;
    cxGrid1DBTableView1VID_ARTIKAL: TcxGridDBColumn;
    cxGrid1DBTableView1ARTIKAL: TcxGridDBColumn;
    cxGrid1DBTableView1ID_EKT: TcxGridDBColumn;
    cxButton1: TcxButton;
    cxGroupBox1: TcxGroupBox;
    cxLabel1: TcxLabel;
    lblKorisnik: TcxDBLabel;
    cxLabel2: TcxLabel;
    lblRE: TcxDBLabel;
    cxLabel5: TcxLabel;
    lblKT: TcxDBLabel;
    cxGrid1DBTableView1PRIMENI: TcxGridDBColumn;
    btVlezOdKT: TcxButton;
    cxGrid2DBTableView1NAZIV_OD_KT: TcxGridDBColumn;
    procedure FormCreate(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure OtvoriTabeli;
    procedure OtvoriRN;
    procedure cxGrid2DBTableView1CellClick(Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);
    procedure cxGrid1DBTableView1MINUSPropertiesButtonClick(Sender: TObject;
      AButtonIndex: Integer);
    procedure aPrezemiSiteExecute(Sender: TObject);
    procedure cxGrid3DBTableView1CellClick(Sender: TcxCustomGridTableView;
      ACellViewInfo: TcxGridTableDataCellViewInfo; AButton: TMouseButton;
      AShift: TShiftState; var AHandled: Boolean);
    procedure cxDBLabel2Click(Sender: TObject);
    procedure aIzberiRNExecute(Sender: TObject);
    procedure aSlednaKTExecute(Sender: TObject);
    procedure aNamaliExecute(Sender: TObject);
    procedure aZgolemiExecute(Sender: TObject);
    procedure OtvoriKT;
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure aIzlezExecute(Sender: TObject);
    procedure aNamali10Execute(Sender: TObject);
    procedure aZgolemi10Execute(Sender: TObject);
    procedure cxButton1Click(Sender: TObject);
  private
    { Private declarations }
  public
    p:byte;
    { Public declarations }
  end;

var
  frmKTVlez: TfrmKTVlez;

implementation

{$R *.dfm}

uses dmUnit, dmKonekcija, dmMaticni, dmResources, dmSystem, mk, utils, cxConstantsMak, DaNe;

procedure TfrmKTVlez.aSlednaKTExecute(Sender: TObject);
begin
//       PanelRN.Visible := false;
//       PanelSlednaKT.Visible := true;
//       cxGrid3.SetFocus;
//       cxGrid3DBTableView1.Controller.GoToFirst(true);

end;

procedure TfrmKTVlez.aZgolemi10Execute(Sender: TObject);
begin
 if cxGrid1DBTableView1ZAVRSENI.EditValue = null  then
     cxGrid1DBTableView1ZAVRSENI.EditValue := 0;
  cxGrid1DBTableView1ZAVRSENI.EditValue := cxGrid1DBTableView1ZAVRSENI.EditValue + 10;
end;

procedure TfrmKTVlez.aZgolemiExecute(Sender: TObject);
begin
  if cxGrid1DBTableView1ZAVRSENI.EditValue = null  then
     cxGrid1DBTableView1ZAVRSENI.EditValue := 0;
   cxGrid1DBTableView1ZAVRSENI.EditValue := cxGrid1DBTableView1ZAVRSENI.EditValue + 1;
end;

procedure TfrmKTVlez.aIzberiRNExecute(Sender: TObject);
begin
   //PanelSlednaKT.Visible := false;
   PanelRN.Visible := true;
   cxGrid2.SetFocus;
   cxGrid2DBTableView1.Controller.GoToFirst(true);
end;

procedure TfrmKTVlez.aIzlezExecute(Sender: TObject);
begin
  if self.Focused then
  begin
      close;
  end
  else
  if cxGrid2.IsFocused then
  begin

      // �� ����� �����, ����� �� �� ������ ���������� ����� �� ��,
      //����� ��� �� ������������� ��� ������ �� �� ������ ������� �� �������
      PanelRN.Visible := false;
  end
//  else
//  if cxGrid3.IsFocused then
//  begin
//      PanelSlednaKT.Visible := false;
//  end
  else
    Close;
end;

procedure TfrmKTVlez.aNamali10Execute(Sender: TObject);
begin
 if cxGrid1DBTableView1ZAVRSENI.EditValue = null  then
     cxGrid1DBTableView1ZAVRSENI.EditValue := 0;
  cxGrid1DBTableView1ZAVRSENI.EditValue := cxGrid1DBTableView1ZAVRSENI.EditValue - 10;
end;

procedure TfrmKTVlez.aNamaliExecute(Sender: TObject);
begin
  if cxGrid1DBTableView1ZAVRSENI.EditValue = null  then
     cxGrid1DBTableView1ZAVRSENI.EditValue := 0;
  cxGrid1DBTableView1ZAVRSENI.EditValue := cxGrid1DBTableView1ZAVRSENI.EditValue - 1;
end;

procedure TfrmKTVlez.aPrezemiSiteExecute(Sender: TObject);
var i : integer;
begin
  //�� �� �������� ���������� �� �� �� ��������
  //������ �� ��� ���� � ������� ������
  //dm.tblKTVlez.Close;
  //dm.tblKTVlez.Open;
  if btOznaciP.Focused then
     begin
//              if lblSlednaKT.Caption = '' then
//              begin
//                ShowMessage('������� ������ ��������� �����!');
//                Abort;
//              end;

            with cxGrid1DBTableView1.DataController do
              for I := 0 to RecordCount - 1 do
              begin
                         dm.tblKTVlez.insert;
                         dm.tblKTVlezID_KT.AsInteger := dm.tblKorisnikKTID_KT.Value;
                         dm.tblKTVlezID_EKT.Value := Values[i,cxGrid1DBTableView1ID_EKT.Index];
                         //dm.tblKTVlezID_KT_OPERACIJA.Value := dm.tblKTOperacijaID_KT.Value;
                         dm.tblKTVlezID_RABOTEN_NALOG.AsInteger := Values[i,cxGrid1DBTableView1ID_RABOTEN_NALOG.Index];
                         //dm.tblKTVlezID_RE.asInteger := dmKon.firma_id;
                         dm.tblKTVlezPRIMENI.Value := Values[i,cxGrid1DBTableView1ZAVRSENO.Index];
                         dm.tblKTVlezDATUM.Value := Now;
                         dm.tblKTVlezVID_ARTIKAL.AsInteger := Values[i,cxGrid1DBTableView1VID_ARTIKAL.Index];
                         dm.tblKTVlezARTIKAL.AsInteger := Values[i,cxGrid1DBTableView1ARTIKAL.Index];

                         dm.tblKTVlezGODINA.AsInteger := YearOf(Now);

                         //dm.tblKTVlezID_RN_STAVKA.AsInteger := Values[i,cxGrid1DBTableView1ID_RN_STAVKA.Index];
                         dm.tblKTVlez.Post;

                        // qUpdateRN.Close;
                        // qUpdateRN.ParamByName('id').Value := Values[i,cxGrid1DBTableView1ID_RN_STAVKA.Index];
                        // qUpdateRN.ExecQuery;
              end;
              OtvoriRN;
     end
     else
     if btZapisi.Focused then
     begin
//          if lblSlednaKT.Caption = '' then
//            begin
//              ShowMessage('������� ������ ��������� �����!');
//              Abort;
//            end;

        with cxGrid1DBTableView1.DataController do
              for I := 0 to RecordCount - 1 do
              if Values[i,cxGrid1DBTableView1ZAVRSENI.Index] <> null then
              begin
//                         dm.tblKTVlez.insert;
//                         dm.tblKTVlezID_KT.AsInteger := dm.tblKorisnikKTID_KT.Value;
//                         dm.tblKTVlezID_KT_OPERACIJA.Value := dm.tblKTOperacijaID_KT.Value;
//                         dm.tblKTVlezID_RABOTEN_NALOG.AsInteger := Values[i,cxGrid1DBTableView1ID.Index];
//                         dm.tblKTVlezID_RE.asInteger := dmKon.firma_id;
//                         dm.tblKTVlezZAVRSENO.AsString := (Values[i,cxGrid1DBTableView1ZAVRSENI.Index]);
//                         dm.tblKTVlezVID_ARTIKAL.AsInteger := Values[i,cxGrid1DBTableView1VID_ARTIKAL.Index];
//                         dm.tblKTVlezARTIKAL.AsInteger := Values[i,cxGrid1DBTableView1ARTIKAL.Index];
//                         dm.tblKTVlezDATUM.Value := Now;
//                         dm.tblKTVlezGODINA.AsInteger := YearOf(Now);
//                         dm.tblKTVlezSLEDNA_KT.Value := dm.tblKTID.Value;
//                         dm.tblKTVlezID_RN_STAVKA.AsInteger := Values[i,cxGrid1DBTableView1ID_RN_STAVKA.Index];
//                         dm.tblKTVlez.Post;


                         dm.tblKTVlez.insert;
                         dm.tblKTVlezID_KT.AsInteger := dm.tblKorisnikKTID_KT.Value;
                         dm.tblKTVlezID_EKT.Value := Values[i,cxGrid1DBTableView1ID_EKT.Index];
                         //dm.tblKTVlezID_KT_OPERACIJA.Value := dm.tblKTOperacijaID_KT.Value;
                         dm.tblKTVlezID_RABOTEN_NALOG.AsInteger := Values[i,cxGrid1DBTableView1ID_RABOTEN_NALOG.Index];
                         //dm.tblKTVlezID_RE.asInteger := dmKon.firma_id;
                         dm.tblKTVlezPRIMENI.Value := Values[i,cxGrid1DBTableView1ZAVRSENI.Index];
                         dm.tblKTVlezDATUM.Value := Now;
                         dm.tblKTVlezVID_ARTIKAL.AsInteger := Values[i,cxGrid1DBTableView1VID_ARTIKAL.Index];
                         dm.tblKTVlezARTIKAL.AsInteger := Values[i,cxGrid1DBTableView1ARTIKAL.Index];

                         dm.tblKTVlezGODINA.AsInteger := YearOf(Now);

                         //dm.tblKTVlezID_RN_STAVKA.AsInteger := Values[i,cxGrid1DBTableView1ID_RN_STAVKA.Index];
                         dm.tblKTVlez.Post;
              end;
              OtvoriRN;
     end
     else
     if btResetiraj.Focused then
     begin
       with cxGrid1DBTableView1.DataController do
              for I := 0 to RecordCount - 1 do
              if Values[i,cxGrid1DBTableView1ZAVRSENI.Index] <> null then
              begin
                        Values[i,cxGrid1DBTableView1ZAVRSENI.Index] := null;
              end;
              OtvoriRN;
     end
     else
     if btZavrseni.Focused then
     begin
       p := btZavrseni.Tag;
       OtvoriRN;
     end
     else
     if btSokrijZavrseni.Focused then
     begin
       p := btSokrijZavrseni.Tag;
       OtvoriRN;
     end
end;

procedure TfrmKTVlez.cxButton1Click(Sender: TObject);
begin
  PanelRN.Visible := false;
end;

procedure TfrmKTVlez.cxDBLabel2Click(Sender: TObject);
begin
 //  PanelSlednaKT.Visible := true;
end;

procedure TfrmKTVlez.cxGrid1DBTableView1MINUSPropertiesButtonClick(
  Sender: TObject; AButtonIndex: Integer);
begin
  //�� ���� ���� �� �� ��������� ��� �� �� �������� ���������
end;

procedure TfrmKTVlez.cxGrid2DBTableView1CellClick(
  Sender: TcxCustomGridTableView; ACellViewInfo: TcxGridTableDataCellViewInfo;
  AButton: TMouseButton; AShift: TShiftState; var AHandled: Boolean);
begin
    PanelRN.Visible := false;
    p := 0;
    OtvoriTabeli;
    btRN.Caption := dm.tblRNVNAZIV_PARTNER_PORACKA.Value;
    btVlezOdKT.Caption := '���� ��: '#10#13+dm.tblRNVlezNAZIV_OKT.Value;
end;

procedure TfrmKTVlez.cxGrid3DBTableView1CellClick(
  Sender: TcxCustomGridTableView; ACellViewInfo: TcxGridTableDataCellViewInfo;
  AButton: TMouseButton; AShift: TShiftState; var AHandled: Boolean);
begin
//    PanelSlednaKT.Visible := false;
//    p := 0;
////    OtvoriTabeli;
//    lblSlednaKT.Caption := dm.tblKTNAZIV_KT.Value;
end;

procedure TfrmKTVlez.FormClose(Sender: TObject; var Action: TCloseAction);
begin
   dm.tblRNVlez.Close;
end;

procedure TfrmKTVlez.FormCreate(Sender: TObject);
begin
    ProcitajFormaIzgled(self);
end;

procedure TfrmKTVlez.FormShow(Sender: TObject);
begin
  frmMK := TfrmMK.Create(nil);
  frmMK.ShowModal;
  frmMK.Free;
  // ������� �� ������������ ������������/���������� �� ���������� �� ���� �����
  SpremiForma(self);
 // cbgodina.Text := inttostr(yearof(now));
  dm.tblRNV.Close;
  dm.tblRNV.ParamByName('status').Asstring := '1';
  dm.tblRNV.ParamByName('godina').AsString := '%';
  if masinsko<>0 then
    dm.tblRNV.ParamByName('id_re').AsInteger := masinsko
  else
    dm.tblRNV.ParamByName('id_re').AsString := '%';
  dm.tblRNV.ParamByName('id_kt').AsInteger := dm.tblKorisnikKTID_KT.Value;
  dm.tblRNV.Open;

  Caption := '��������� �� ���� �� '+dm.tblKorisnikKTNAZIV.Value;
 // OtvoriTabeli;
//  tsArtikal.TabVisible := false;
//  cxPageControl2.ActivePage := tsPoedinecni;
//  cxPageControl1.ActivePage := tsKontrolnaTocka;
//  cbArtikal.Text := dm.tblKTVlezNAZIV_ARTIKAL.Value;
//  KtLayoutGrid.Site.Refresh;
//  KtLayoutGrid.Site.VScrollBar.Width := 40;
//  KtLayoutGrid.LayoutChanged(True);
//
//  RnLayoutGrid.Site.Refresh;
//  RnLayoutGrid.Site.VScrollBar.Width := 40;
//  RnLayoutGrid.LayoutChanged(True);

end;
procedure TfrmKTVlez.OtvoriTabeli;
begin
//   dm.tblRePartner.Close;
//   dm.tblRePartner.ParamByName('id').Asstring := dmKon.qSysUserPARTNER.AsString;
//   dm.tblRePartner.ParamByName('tip_partner').Asstring := dmKon.qSysUserTIP_PARTNER.AsString;
//   dm.tblRePartner.Open;
//
//   rab_edinica := dm.tblRePartnerID_RE.Value;
//
//   dm.qSetupM.Close;
//   dm.qSetupM.ExecQuery;
//
//   // komentirano na 08032013 - kje vidam za ponatamu
//  // if dm.qSetupM.FldByName['v1'].AsInteger = rab_edinica then
//   //     masinsko := dm.qSetupM.FldByName['v1'].AsInteger
//  // else masinsko := 0;
//
//   OtvoriKT;
//
//   dm.tblOperacijaPR.close;
//   dm.tblOperacijaPR.Open;
//
//   OtvoriRN;
//
//   dm.tblPoslednaKT.Open;
//   dm.tblKtRe.Close;
//   dm.tblKtRe.ParamByName('id_re').AsInteger := rab_edinica;
//   dm.tblKtRe.Open;
//   if dm.tblKtRe.RecordCount = 1 then
//   begin
//     kta := dm.tblKtReID.Value;
//   end
//   else
//   begin
//     kta := 0;
//   end;
//   dm.tblKTOperacija.Close;
//   dm.tblKTOperacija.Open;
 //  dm.tblartikalCB.Close;
  //dm.tblArtikalcb.ParamByName('godina').AsString := cbGodina.Text;
//   if masinsko <> 0 then
 //     dm.tblArtikalcb.ParamByName('id_re').AsInteger := masinsko
  //  else
///      dm.tblArtikalcb.ParamByName('id_re').AsString := '%';
//   dm.tblArtikalCB.ParamByName('rn').Value := dm.tblRNVlezID.Value;
 //  dm.tblArtikalCB.Open;
   //rab_edinica := dm.qRePartner.FldByName['']



//   dm.tblRePartner.Close;
//   dm.tblRePartner.ParamByName('id').Asstring := dmKon.qSysUserPARTNER.AsString;
//   dm.tblRePartner.ParamByName('tip_partner').Asstring := dmKon.qSysUserTIP_PARTNER.AsString;
//   dm.tblRePartner.Open;
//
//   rab_edinica := dm.tblRePartnerID_RE.Value;

   dm.qSetupM.Close;
   dm.qSetupM.ExecQuery;

   if dm.qSetupM.FldByName['v1'].AsInteger = rab_edinica then
        masinsko := dm.qSetupM.FldByName['v1'].AsInteger
   else masinsko := 0;

   OtvoriKT;

   dm.tblOperacijaPR.close;
   dm.tblOperacijaPR.Open;

   OtvoriRN;

   dm.tblPoslednaKT.Open;
   dm.tblKtRe.Close;
   dm.tblKtRe.ParamByName('id_re').AsInteger := rab_edinica;
   dm.tblKtRe.Open;
   if dm.tblKtRe.RecordCount = 1 then
   begin
     kta := dm.tblKtReID.Value;
   end
   else
   begin
     kta := 0;
   end;
   dm.tblKTOperacija.Close;
   dm.tblKTOperacija.Open;
   dm.tblartikalCB.Close;
   dm.tblArtikalcb.ParamByName('godina').AsString := '%';
   if masinsko <> 0 then
      dm.tblArtikalcb.ParamByName('id_re').AsInteger := masinsko
    else
      dm.tblArtikalcb.ParamByName('id_re').AsString := '%';
   dm.tblArtikalCB.ParamByName('rn').Value := dm.tblRNVlezID.Value;
   dm.tblArtikalCB.Open;

end;
procedure TfrmKTVlez.OtvoriRN;
begin

  dm.tblRNVlez.Close;
  dm.tblRNVlez.ParamByName('status').Asstring := '1';
 // dm.tblRNVlez.ParamByName('godina').AsString := '%';
//  if masinsko<>0 then
//    dm.tblRNVlez.ParamByName('id_re').AsInteger := masinsko
//  else
//    dm.tblRNVlez.ParamByName('id_re').AsString := '%';
  dm.tblRNVlez.ParamByName('id_rn').Value := dm.tblRNVID.Value;
  dm.tblRNVlez.ParamByName('id_kt').Value := dm.tblKorisnikKTID_KT.Value;
  dm.tblRNVlez.ParamByName('kt').Value := dm.tblRNVID_OD_KT.Value;

  dm.tblRNVlez.ParamByName('p').AsInteger := p;
  dm.tblRNVlez.Open;
end;

procedure TfrmKTVlez.OtvoriKT;
begin
  dm.tblKTVlez.Close;
  dm.tblKTVlez.ParamByName('godina').AsString := '%';
  dm.tblKTVlez.ParamByName('id_kt').AsString := dm.tblKorisnikKTID_KT.AsString;
  dm.tblKTVlez.ParamByName('korisnik').AsString := dmKon.qSysUserUSERNAME.AsString;
  dm.tblKTVlez.Open;
end;

end.
