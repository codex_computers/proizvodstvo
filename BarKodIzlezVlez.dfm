object frmIzlezVlez: TfrmIzlezVlez
  Left = 0
  Top = 0
  Caption = #1048#1079#1073#1077#1088#1077#1090#1077
  ClientHeight = 347
  ClientWidth = 635
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 635
    Height = 347
    Align = alClient
    TabOrder = 0
    ExplicitHeight = 458
    object cxGroupBox1: TcxGroupBox
      Left = 1
      Top = 131
      Align = alTop
      Caption = #1054#1076'/'#1047#1072' '#1082#1072#1076#1077' '#1090#1088#1077#1073#1072' '#1076#1072' '#1076#1086#1072#1107#1072#1072#1090' / '#1079#1072#1084#1080#1085#1091#1074#1072#1072#1090' '#1087#1088#1086#1080#1079#1074#1086#1076#1080#1090#1077
      TabOrder = 0
      ExplicitLeft = 0
      ExplicitTop = 137
      Height = 104
      Width = 633
      object lbl3: TLabel
        Left = 19
        Top = 56
        Width = 123
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1057#1083#1077#1076#1085#1072' '#1050#1058' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object lbl1: TLabel
        Left = 5
        Top = 27
        Width = 136
        Height = 27
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1054#1076' '#1082#1086#1085#1090#1088#1086#1083#1085#1072' '#1090#1086#1095#1082#1072' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clNavy
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        WordWrap = True
      end
      object cbOdKT: TcxLookupComboBox
        Left = 148
        Top = 23
        Properties.ImmediatePost = True
        Properties.KeyFieldNames = 'ID'
        Properties.ListColumns = <
          item
            FieldName = 'id'
          end
          item
            FieldName = 'naziv_okt'
          end
          item
            FieldName = 'naziv_partner_poracka'
          end>
        Properties.ListFieldIndex = 1
        Properties.ListOptions.SyncMode = True
        Properties.ListSource = dm.dsRNVlez
        TabOrder = 0
        Width = 443
      end
      object cbSlednaKT: TcxLookupComboBox
        Left = 148
        Top = 52
        Properties.ClearKey = 46
        Properties.DropDownSizeable = True
        Properties.ImmediatePost = True
        Properties.KeyFieldNames = 'ID'
        Properties.ListColumns = <
          item
            FieldName = 'ID'
          end
          item
            FieldName = 'naziv'
          end>
        Properties.ListFieldIndex = 1
        Properties.ListOptions.SyncMode = True
        Properties.ListSource = dm.dsKT
        TabOrder = 1
        Width = 443
      end
    end
    object cxGroupBox2: TcxGroupBox
      Left = 1
      Top = 1
      Align = alTop
      Caption = #1055#1086#1095#1077#1090#1085#1072' '#1089#1080#1090#1091#1072#1094#1080#1112#1072
      TabOrder = 1
      ExplicitLeft = 6
      ExplicitTop = 0
      Height = 130
      Width = 633
      object lbl4: TLabel
        Left = 17
        Top = 28
        Width = 126
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1056#1072#1073#1086#1090#1085#1072' '#1077#1076#1080#1085#1080#1094#1072' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object lbl5: TLabel
        Left = 19
        Top = 55
        Width = 123
        Height = 13
        Alignment = taRightJustify
        AutoSize = False
        Caption = #1050#1086#1085#1090#1088#1086#1083#1085#1072' '#1090#1086#1095#1082#1072' :'
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clRed
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
      object cxlbl2: TcxLabel
        Left = 79
        Top = 81
        Caption = #1054#1087#1077#1088#1072#1094#1080#1112#1072' :'
        Style.TextColor = clRed
        Transparent = True
      end
      object cbRE: TcxLookupComboBox
        Left = 148
        Top = 25
        Properties.ClearKey = 46
        Properties.DropDownSizeable = True
        Properties.KeyFieldNames = 'ID'
        Properties.ListColumns = <
          item
            FieldName = 'ID'
          end
          item
            FieldName = 'naziv'
          end>
        Properties.ListFieldIndex = 1
        Properties.ListOptions.SyncMode = True
        Properties.ListSource = dm.dsRE
        TabOrder = 0
        Width = 443
      end
      object cbKT: TcxLookupComboBox
        Left = 148
        Top = 52
        Properties.ClearKey = 46
        Properties.DropDownSizeable = True
        Properties.ImmediatePost = True
        Properties.KeyFieldNames = 'ID'
        Properties.ListColumns = <
          item
            FieldName = 'ID'
          end
          item
            FieldName = 'naziv'
          end>
        Properties.ListFieldIndex = 1
        Properties.ListOptions.SyncMode = True
        Properties.ListSource = dm.dsKT
        TabOrder = 1
        Width = 443
      end
      object cbOperacija: TcxLookupComboBox
        Left = 148
        Top = 78
        Properties.ClearKey = 46
        Properties.DropDownAutoSize = True
        Properties.DropDownSizeable = True
        Properties.ImmediatePost = True
        Properties.KeyFieldNames = 'ID'
        Properties.ListColumns = <
          item
            FieldName = 'ID'
          end
          item
            FieldName = 'NAZIV_OPERACIJA'
          end>
        Properties.ListFieldIndex = 1
        Properties.ListOptions.SyncMode = True
        Properties.ListSource = dm.dsKTOperacija
        TabOrder = 2
        Width = 443
      end
    end
    object cxButton1: TcxButton
      Left = 400
      Top = 296
      Width = 75
      Height = 25
      Caption = 'cxButton1'
      TabOrder = 2
    end
    object cxButton2: TcxButton
      Left = 517
      Top = 296
      Width = 75
      Height = 25
      Caption = 'cxButton2'
      TabOrder = 3
    end
  end
  object actnlst1: TActionList
    Left = 113
    Top = 33
    object aSkeniraj: TAction
      Caption = #1057#1082#1077#1085#1080#1088#1072#1112
      OnExecute = aSkenirajExecute
    end
  end
end
