object frmPrefStOdEdenKamVoDrug: TfrmPrefStOdEdenKamVoDrug
  Left = 128
  Top = 212
  BorderIcons = [biSystemMenu, biMaximize]
  Caption = #1055#1088#1077#1092#1088#1083#1072#1114#1077' '#1085#1072' '#1089#1090#1072#1074#1082#1080' '#1086#1076' '#1077#1076#1077#1085' '#1050#1072#1084#1080#1086#1085' '#1074#1086' '#1044#1088#1091#1075
  ClientHeight = 682
  ClientWidth = 1315
  Color = clBtnFace
  Constraints.MinHeight = 500
  Constraints.MinWidth = 610
  Font.Charset = RUSSIAN_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  ShowHint = True
  WindowState = wsMaximized
  OnClose = FormClose
  OnCreate = FormCreate
  OnResize = FormResize
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object dxRibbon1: TdxRibbon
    Left = 0
    Top = 0
    Width = 1315
    Height = 126
    BarManager = dxBarManager1
    ColorSchemeName = 'Blue'
    Contexts = <>
    TabOrder = 0
    TabStop = False
    object dxRibbon1Tab1: TdxRibbonTab
      Active = True
      Caption = #1052#1077#1085#1080
      Groups = <
        item
          ToolbarName = 'dxBarManager1Bar2'
        end
        item
          ToolbarName = 'dxBarManager1Bar3'
        end>
      Index = 0
    end
    object dxRibbon1Tab2: TdxRibbonTab
      Caption = #1055#1086#1076#1077#1089#1091#1074#1072#1114#1072
      Groups = <
        item
          ToolbarName = 'dxBarManager1Bar4'
        end
        item
          ToolbarName = 'dxBarManager1BarIzgledGrid'
        end>
      Index = 1
    end
  end
  object StatusBar1: TdxRibbonStatusBar
    Left = 0
    Top = 659
    Width = 1315
    Height = 23
    Panels = <
      item
        PanelStyleClassName = 'TdxStatusBarTextPanelStyle'
        Text = 'Esc - '#1054#1090#1082#1072#1078#1080' / '#1048#1079#1083#1077#1079
      end>
    Ribbon = dxRibbon1
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clDefault
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
  end
  object Panel1: TPanel
    Left = 0
    Top = 126
    Width = 1315
    Height = 533
    Align = alClient
    Caption = 'Panel1'
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 2
    object Panel: TPanel
      Left = 772
      Top = 1
      Width = 108
      Height = 531
      Align = alLeft
      Anchors = [akTop, akRight, akBottom]
      TabOrder = 0
      object cxButton1: TcxButton
        Left = 1
        Top = 1
        Width = 106
        Height = 529
        Hint = #1055#1088#1077#1092#1088#1083#1072#1114#1077' '#1085#1072' '#1089#1090#1072#1074#1082#1080' '#1074#1086' '#1076#1088#1091#1075' '#1082#1072#1084#1080#1086#1085
        Align = alClient
        Action = aPrefrliStavki
        TabOrder = 0
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clWindowText
        Font.Height = -13
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
      end
    end
    object lPanel: TPanel
      Left = 1
      Top = 1
      Width = 771
      Height = 531
      Align = alLeft
      Anchors = [akTop, akRight, akBottom]
      Caption = 'lPanel'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 1
      object cxGrid1: TcxGrid
        Left = 1
        Top = 1
        Width = 769
        Height = 529
        Align = alClient
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
        RootLevelOptions.DetailTabsPosition = dtpTop
        object cxGrid1DBTableView1: TcxGridDBTableView
          Navigator.Buttons.CustomButtons = <>
          DataController.DataSource = dsKamion
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          FilterRow.InfoText = #1050#1083#1080#1082#1085#1077#1090#1077' '#1090#1091#1082#1072' '#1079#1072' '#1076#1072' '#1076#1077#1092#1080#1085#1080#1088#1072#1090#1077' '#1092#1080#1083#1090#1077#1088
          FilterRow.Visible = True
          OptionsBehavior.IncSearch = True
          OptionsCustomize.ColumnsQuickCustomization = True
          OptionsData.Deleting = False
          OptionsSelection.MultiSelect = True
          object cxGrid1DBTableView1Izberi: TcxGridDBColumn
            Caption = #1048#1079#1073#1077#1088#1080
            DataBinding.ValueType = 'Boolean'
            PropertiesClassName = 'TcxCheckBoxProperties'
            Properties.ImmediatePost = True
            Properties.NullStyle = nssUnchecked
            Width = 36
          end
          object cxGrid1DBTableView1ID_TRAILER: TcxGridDBColumn
            Caption = #1064#1080#1092#1088#1072' '#1085#1072' '#1082#1072#1084#1080#1086#1085
            DataBinding.FieldName = 'ID_TRAILER'
            Options.Editing = False
            Width = 41
          end
          object cxGrid1DBTableView1NAZIV_TRAILER: TcxGridDBColumn
            Caption = #1053#1072#1079#1080#1074
            DataBinding.FieldName = 'NAZIV_TRAILER'
            Visible = False
            GroupIndex = 0
            Options.Editing = False
            Width = 150
          end
          object cxGrid1DBTableView1DATUM_TRANSPORT: TcxGridDBColumn
            Caption = #1044#1072#1090#1091#1084' '#1085#1072' '#1090#1088#1072#1085#1089#1087#1086#1088#1090
            DataBinding.FieldName = 'DATUM_TRANSPORT'
            Options.Editing = False
            Width = 46
          end
          object cxGrid1DBTableView1KOL_TRAILER: TcxGridDBColumn
            Caption = #1050#1086#1083#1080#1095#1080#1085#1072
            DataBinding.FieldName = 'KOL_TRAILER'
            Options.Editing = False
            Width = 60
          end
          object cxGrid1DBTableView1KOL_PREF_BRI: TcxGridDBColumn
            Caption = #1050#1086#1083#1080#1095#1080#1085#1072' '#1079#1072' '#1087#1088#1077#1092#1083#1072#1114#1077'/'#1073#1088#1080#1096#1077#1114#1077
            DataBinding.ValueType = 'Integer'
            Width = 122
          end
          object cxGrid1DBTableView1VID_ARTIKAL: TcxGridDBColumn
            Caption = #1042#1080#1076' '#1072#1088#1090#1080#1082#1072#1083
            DataBinding.FieldName = 'VID_ARTIKAL'
            Options.Editing = False
            Width = 47
          end
          object cxGrid1DBTableView1ARTIKAL: TcxGridDBColumn
            Caption = #1064#1080#1092#1088#1072' '#1085#1072' '#1072#1088#1090#1080#1082#1072#1083
            DataBinding.FieldName = 'ARTIKAL'
            Options.Editing = False
            Width = 42
          end
          object cxGrid1DBTableView1NAZIV_ARTIKAL: TcxGridDBColumn
            Caption = #1040#1088#1090#1080#1082#1072#1083
            DataBinding.FieldName = 'NAZIV_ARTIKAL'
            Options.Editing = False
            Width = 150
          end
          object cxGrid1DBTableView1OPIS: TcxGridDBColumn
            Caption = #1054#1087#1080#1089
            DataBinding.FieldName = 'OPIS'
            Visible = False
            Options.Editing = False
            Width = 64
          end
          object cxGrid1DBTableView1ISPORACANA_KOLICINA: TcxGridDBColumn
            Caption = #1048#1089#1087#1086#1088#1072#1095#1072#1085#1072' '#1082#1086#1083#1080#1095#1080#1085#1072
            DataBinding.FieldName = 'ISPORACANA_KOLICINA'
            Visible = False
            Options.Editing = False
            Width = 64
          end
          object cxGrid1DBTableView1CUSTOMER_NAME: TcxGridDBColumn
            DataBinding.FieldName = 'CUSTOMER_NAME'
            Width = 112
          end
          object cxGrid1DBTableView1VK_KOLICINA: TcxGridDBColumn
            Caption = #1042#1082'.'#1082#1086#1083#1080#1095#1080#1085#1072
            DataBinding.FieldName = 'VK_KOLICINA'
            Options.Editing = False
            Width = 64
          end
          object cxGrid1DBTableView1REF_NO: TcxGridDBColumn
            DataBinding.FieldName = 'REF_NO'
            Options.Editing = False
            Width = 64
          end
          object cxGrid1DBTableView1SUNGER: TcxGridDBColumn
            DataBinding.FieldName = 'SUNGER'
            Options.Editing = False
            Width = 64
          end
          object cxGrid1DBTableView1BOJA_NOGARKI: TcxGridDBColumn
            DataBinding.FieldName = 'BOJA_NOGARKI'
            Options.Editing = False
            Width = 64
          end
          object cxGrid1DBTableView1LABELS: TcxGridDBColumn
            DataBinding.FieldName = 'LABELS'
            Options.Editing = False
            Width = 64
          end
          object cxGrid1DBTableView1GLIDERS: TcxGridDBColumn
            DataBinding.FieldName = 'GLIDERS'
            Options.Editing = False
            Width = 64
          end
          object cxGrid1DBTableView1ID_PORACKA_STAVKA: TcxGridDBColumn
            DataBinding.FieldName = 'ID_PORACKA_STAVKA'
            Visible = False
            Options.Editing = False
          end
          object cxGrid1DBTableView1ID: TcxGridDBColumn
            DataBinding.FieldName = 'ID'
            Visible = False
            Options.Editing = False
          end
          object cxGrid1DBTableView1LOT: TcxGridDBColumn
            DataBinding.FieldName = 'LOT'
            Visible = False
          end
          object cxGrid1DBTableView1BROJ: TcxGridDBColumn
            Caption = #1040#1088#1090#1080#1082#1072#1083' '#1073#1088#1086#1112
            DataBinding.FieldName = 'BROJ'
            Visible = False
            Width = 100
          end
          object cxGrid1DBTableView1ID_RN_STAVKA: TcxGridDBColumn
            DataBinding.FieldName = 'ID_RN_STAVKA'
            Visible = False
          end
          object cxGrid1DBTableView1SKU: TcxGridDBColumn
            DataBinding.FieldName = 'SKU'
            Visible = False
            Width = 50
          end
          object cxGrid1DBTableView1ISPORAKA_DO_DATUM: TcxGridDBColumn
            Caption = #1044#1072#1090#1091#1084' '#1080#1089#1087#1086#1088#1072#1082#1072
            DataBinding.FieldName = 'ISPORAKA_DO_DATUM'
          end
          object cxGrid1DBTableView1ISPORAKA_OD_DATUM: TcxGridDBColumn
            Caption = #1044#1072#1090#1091#1084' '#1076#1086' '#1080#1089#1087#1086#1088#1072#1082#1072
            DataBinding.FieldName = 'ISPORAKA_OD_DATUM'
            Visible = False
          end
        end
        object cxGrid1DBTableView2: TcxGridDBTableView
          Navigator.Buttons.CustomButtons = <>
          DataController.DataSource = dsTapacir
          DataController.DetailKeyFieldNames = 'ID_PORACKA_STAVKA'
          DataController.KeyFieldNames = 'ID_PORACKA_STAVKA'
          DataController.MasterKeyFieldNames = 'ID_PORACKA_STAVKA'
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          OptionsData.Deleting = False
          OptionsData.Editing = False
          OptionsData.Inserting = False
          OptionsView.GroupByBox = False
          object cxGrid1DBTableView2ID_PORACKA_STAVKA: TcxGridDBColumn
            DataBinding.FieldName = 'ID_PORACKA_STAVKA'
            Visible = False
          end
          object cxGrid1DBTableView2VID_TAPACIR: TcxGridDBColumn
            DataBinding.FieldName = 'VID_TAPACIR'
            Visible = False
          end
          object cxGrid1DBTableView2TAPACIR: TcxGridDBColumn
            DataBinding.FieldName = 'TAPACIR'
            Visible = False
          end
          object cxGrid1DBTableView2NAZIV_TAPACIR: TcxGridDBColumn
            Caption = #1042#1080#1076' '#1085#1072' '#1058#1072#1087#1072#1094#1080#1088
            DataBinding.FieldName = 'NAZIV_TAPACIR'
            Width = 200
          end
          object cxGrid1DBTableView2VID_SUROVINA: TcxGridDBColumn
            Caption = #1042#1080#1076' '#1085#1072' '#1084#1072#1090#1077#1088#1080#1112#1072#1083
            DataBinding.FieldName = 'VID_SUROVINA'
          end
          object cxGrid1DBTableView2SUROVINA: TcxGridDBColumn
            Caption = #1064#1080#1092#1088#1072' '#1085#1072' '#1084#1072#1090#1077#1088#1080#1112#1072#1083
            DataBinding.FieldName = 'SUROVINA'
          end
          object cxGrid1DBTableView2NAZIV_SUROVINA: TcxGridDBColumn
            Caption = #1052#1072#1090#1077#1088#1080#1112#1072#1083
            DataBinding.FieldName = 'NAZIV_SUROVINA'
            Width = 200
          end
          object cxGrid1DBTableView2KOLICINA: TcxGridDBColumn
            Caption = #1050#1086#1083#1080#1095#1080#1085#1072
            DataBinding.FieldName = 'KOLICINA'
          end
          object cxGrid1DBTableView2MERKA: TcxGridDBColumn
            Caption = #1052#1077#1088#1082#1072
            DataBinding.FieldName = 'MERKA'
          end
          object cxGrid1DBTableView2PALETA: TcxGridDBColumn
            Caption = #1055#1072#1083#1077#1090#1072
            DataBinding.FieldName = 'PALETA'
            Width = 100
          end
        end
        object cxGrid1Level1: TcxGridLevel
          Caption = #1055#1056#1045#1060#1056#1051#1048' '#1057#1058#1040#1042#1050#1048' '#1054#1044' '#1050#1040#1052#1048#1054#1053
          GridView = cxGrid1DBTableView1
          Options.DetailTabsPosition = dtpTop
          object cxGrid1Level2: TcxGridLevel
            GridView = cxGrid1DBTableView2
          end
        end
      end
    end
    object dPanel: TPanel
      Left = 880
      Top = 1
      Width = 434
      Height = 531
      Align = alClient
      Caption = 'dPanel'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 2
      object cxGrid2: TcxGrid
        Left = 1
        Top = 1
        Width = 432
        Height = 529
        Align = alClient
        Anchors = [akTop, akRight, akBottom]
        Font.Charset = RUSSIAN_CHARSET
        Font.Color = clWindowText
        Font.Height = -11
        Font.Name = 'Tahoma'
        Font.Style = []
        ParentFont = False
        TabOrder = 0
        RootLevelOptions.DetailTabsPosition = dtpTop
        object cxGrid2DBTableView1: TcxGridDBTableView
          Navigator.Buttons.CustomButtons = <>
          DataController.DataSource = dsVoKamion
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          FilterRow.InfoText = #1050#1083#1080#1082#1085#1077#1090#1077' '#1090#1091#1082#1072' '#1079#1072' '#1076#1072' '#1076#1077#1092#1080#1085#1080#1088#1072#1090#1077' '#1092#1080#1083#1090#1077#1088
          FilterRow.Visible = True
          OptionsBehavior.IncSearch = True
          OptionsCustomize.ColumnsQuickCustomization = True
          OptionsData.Deleting = False
          OptionsData.Editing = False
          OptionsData.Inserting = False
          object cxGrid2DBTableView1DATUM_TRANSPORT: TcxGridDBColumn
            Caption = #1044#1072#1090#1091#1084
            DataBinding.FieldName = 'DATUM_TRANSPORT'
          end
          object cxGrid2DBTableView1ID_TRAILER: TcxGridDBColumn
            Caption = #1064#1080#1092#1088#1072' '#1085#1072' '#1082#1072#1084#1080#1086#1085
            DataBinding.FieldName = 'ID_TRAILER'
            Width = 44
          end
          object cxGrid2DBTableView1KOL_TRAILER: TcxGridDBColumn
            Caption = #1050#1086#1083#1080#1095#1080#1085#1072
            DataBinding.FieldName = 'KOL_TRAILER'
          end
          object cxGrid2DBTableView1NAZIV_TRAILER: TcxGridDBColumn
            Caption = #1053#1072#1079#1080#1074
            DataBinding.FieldName = 'NAZIV_TRAILER'
            Visible = False
            GroupIndex = 0
            Width = 64
          end
          object cxGrid2DBTableView1VID_ARTIKAL: TcxGridDBColumn
            Caption = #1042#1080#1076' '#1085#1072' '#1072#1088#1090#1080#1082#1072#1083
            DataBinding.FieldName = 'VID_ARTIKAL'
          end
          object cxGrid2DBTableView1ARTIKAL: TcxGridDBColumn
            Caption = #1064#1080#1092#1088#1072' '#1085#1072' '#1072#1088#1090#1080#1082#1072#1083
            DataBinding.FieldName = 'ARTIKAL'
          end
          object cxGrid2DBTableView1NAZIV_ARTIKAL: TcxGridDBColumn
            Caption = #1040#1088#1090#1080#1082#1072#1083
            DataBinding.FieldName = 'NAZIV_ARTIKAL'
            Width = 64
          end
          object cxGrid2DBTableView1CUSTOMER_NAME: TcxGridDBColumn
            DataBinding.FieldName = 'CUSTOMER_NAME'
            Width = 91
          end
          object cxGrid2DBTableView1ISPORACANA_KOLICINA: TcxGridDBColumn
            Caption = #1048#1089#1087#1086#1088#1072#1095#1072#1085#1072' '#1082#1086#1083#1080#1095#1080#1085#1072
            DataBinding.FieldName = 'ISPORACANA_KOLICINA'
            Visible = False
          end
          object cxGrid2DBTableView1VK_KOLICINA: TcxGridDBColumn
            Caption = #1042#1082'.'#1082#1086#1083#1080#1095#1080#1085#1072
            DataBinding.FieldName = 'VK_KOLICINA'
          end
          object cxGrid2DBTableView1REF_NO: TcxGridDBColumn
            DataBinding.FieldName = 'REF_NO'
            Width = 64
          end
          object cxGrid2DBTableView1SUNGER: TcxGridDBColumn
            DataBinding.FieldName = 'SUNGER'
            Width = 64
          end
          object cxGrid2DBTableView1BOJA_NOGARKI: TcxGridDBColumn
            DataBinding.FieldName = 'BOJA_NOGARKI'
            Width = 64
          end
          object cxGrid2DBTableView1LABELS: TcxGridDBColumn
            DataBinding.FieldName = 'LABELS'
            Width = 64
          end
          object cxGrid2DBTableView1GLIDERS: TcxGridDBColumn
            DataBinding.FieldName = 'GLIDERS'
            Width = 64
          end
          object cxGrid2DBTableView1ID_PORACKA_STAVKA: TcxGridDBColumn
            DataBinding.FieldName = 'ID_PORACKA_STAVKA'
            Visible = False
          end
          object cxGrid2DBTableView1BROJ: TcxGridDBColumn
            Caption = #1040#1088#1090#1080#1082#1072#1083' '#1073#1088#1086#1112
            DataBinding.FieldName = 'BROJ'
            Visible = False
            Width = 100
          end
          object cxGrid2DBTableView1SKU: TcxGridDBColumn
            DataBinding.FieldName = 'SKU'
            Visible = False
            Width = 50
          end
          object cxGrid2DBTableView1ISPORAKA_DO_DATUM: TcxGridDBColumn
            Caption = #1044#1072#1090#1091#1084' '#1080#1089#1087#1086#1088#1072#1082#1072
            DataBinding.FieldName = 'ISPORAKA_DO_DATUM'
          end
          object cxGrid2DBTableView1ISPORAKA_OD_DATUM: TcxGridDBColumn
            Caption = #1044#1072#1090#1091#1084' '#1076#1086' '#1080#1089#1087#1086#1088#1072#1082#1072
            DataBinding.FieldName = 'ISPORAKA_OD_DATUM'
            Visible = False
          end
        end
        object cxGrid2DBTableView2: TcxGridDBTableView
          Navigator.Buttons.CustomButtons = <>
          DataController.DataSource = dsTapacir
          DataController.DetailKeyFieldNames = 'ID_PORACKA_STAVKA'
          DataController.KeyFieldNames = 'ID_PORACKA_STAVKA'
          DataController.MasterKeyFieldNames = 'ID_PORACKA_STAVKA'
          DataController.Summary.DefaultGroupSummaryItems = <>
          DataController.Summary.FooterSummaryItems = <>
          DataController.Summary.SummaryGroups = <>
          OptionsData.Deleting = False
          OptionsData.Editing = False
          OptionsData.Inserting = False
          OptionsView.GroupByBox = False
          object cxGrid2DBTableView2ID_PORACKA_STAVKA: TcxGridDBColumn
            DataBinding.FieldName = 'ID_PORACKA_STAVKA'
            Visible = False
          end
          object cxGrid2DBTableView2VID_TAPACIR: TcxGridDBColumn
            DataBinding.FieldName = 'VID_TAPACIR'
            Visible = False
          end
          object cxGrid2DBTableView2TAPACIR: TcxGridDBColumn
            DataBinding.FieldName = 'TAPACIR'
            Visible = False
          end
          object cxGrid2DBTableView2NAZIV_TAPACIR: TcxGridDBColumn
            Caption = #1042#1080#1076' '#1085#1072' '#1090#1072#1087#1072#1094#1080#1088
            DataBinding.FieldName = 'NAZIV_TAPACIR'
            Width = 200
          end
          object cxGrid2DBTableView2VID_SUROVINA: TcxGridDBColumn
            Caption = #1042#1080#1076' '#1085#1072' '#1084#1072#1090#1077#1088#1080#1112#1072#1083
            DataBinding.FieldName = 'VID_SUROVINA'
          end
          object cxGrid2DBTableView2SUROVINA: TcxGridDBColumn
            Caption = #1064#1080#1092#1088#1072' '#1085#1072' '#1084#1072#1090#1077#1088#1080#1112#1072#1083
            DataBinding.FieldName = 'SUROVINA'
          end
          object cxGrid2DBTableView2NAZIV_SUROVINA: TcxGridDBColumn
            Caption = #1052#1072#1090#1077#1088#1080#1112#1072#1083
            DataBinding.FieldName = 'NAZIV_SUROVINA'
            Width = 200
          end
          object cxGrid2DBTableView2KOLICINA: TcxGridDBColumn
            Caption = #1050#1086#1083#1080#1095#1080#1085#1072
            DataBinding.FieldName = 'KOLICINA'
          end
          object cxGrid2DBTableView2MERKA: TcxGridDBColumn
            Caption = #1052#1077#1088#1082#1072
            DataBinding.FieldName = 'MERKA'
          end
          object cxGrid2DBTableView2PALETA: TcxGridDBColumn
            Caption = #1055#1072#1083#1077#1090#1072
            DataBinding.FieldName = 'PALETA'
            Width = 100
          end
        end
        object cxGrid2Level1: TcxGridLevel
          Caption = #1055#1056#1045#1060#1056#1051#1048' '#1057#1058#1040#1042#1050#1048' '#1042#1054' '#1050#1040#1052#1048#1054#1053
          GridView = cxGrid2DBTableView1
          object cxGrid2Level2: TcxGridLevel
            GridView = cxGrid2DBTableView2
          end
        end
      end
    end
  end
  object cxGridPopupMenu1: TcxGridPopupMenu
    Grid = cxGrid1
    PopupMenus = <>
    Left = 590
    Top = 16
  end
  object PopupMenu1: TPopupMenu
    Left = 730
    Top = 16
    object N1: TMenuItem
      Caption = #1057#1085#1080#1084#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      ShortCut = 49235
      OnClick = SaveToIniFileExecute
    end
    object Excel1: TMenuItem
      Caption = #1057#1085#1080#1084#1080' '#1075#1086' '#1074#1086' Excel'
      ShortCut = 49221
    end
  end
  object dxBarManager1: TdxBarManager
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Segoe UI'
    Font.Style = []
    Categories.Strings = (
      'Default')
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    ImageOptions.Images = dmRes.cxSmallImages
    ImageOptions.LargeImages = dmRes.cxLargeImages
    PopupMenuLinks = <>
    UseSystemFont = False
    Left = 625
    Top = 16
    PixelsPerInch = 96
    object dxBarManager1Bar2: TdxBar
      Caption = #1040#1082#1094#1080#1080
      CaptionButtons = <>
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 789
      FloatTop = 120
      FloatClientWidth = 51
      FloatClientHeight = 120
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton10'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton5'
        end
        item
          Visible = True
          ItemName = 'dxBarSubItem1'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxbrlrgbtn1'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar3: TdxBar
      Caption = #1048#1079#1083#1077#1079
      CaptionButtons = <>
      DockedLeft = 341
      DockedTop = 0
      FloatLeft = 718
      FloatTop = 62
      FloatClientWidth = 51
      FloatClientHeight = 80
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton7'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton8'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar4: TdxBar
      Caption = #1055#1077#1095#1072#1090#1077#1114#1077
      CaptionButtons = <>
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 676
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton12'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton15'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton9'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarLargeButton13'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1BarIzgledGrid: TdxBar
      Caption = #1058#1072#1073#1077#1083#1072
      CaptionButtons = <>
      DockedLeft = 363
      DockedTop = 0
      FloatLeft = 808
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLBtnSnimiIzgled'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarLBtnBrisiIzgled'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarLargeButton2: TdxBarLargeButton
      Action = aAzuriraj
      Category = 0
    end
    object dxBarLargeButton3: TdxBarLargeButton
      Action = aBrisi
      Category = 0
    end
    object dxBarLargeButton4: TdxBarLargeButton
      Action = aSnimiIzgled
      Category = 0
      ScreenTip = tipSnimiIzgled
    end
    object dxBarLargeButton5: TdxBarLargeButton
      Action = aZacuvajExcel
      Category = 0
      ScreenTip = tipZacuvajExcel
    end
    object dxBarLargeButton6: TdxBarLargeButton
      Action = aPecatiTabela
      Caption = #1055#1077#1095#1072#1090#1080' '#1090#1072#1073#1077#1083#1072
      Category = 0
    end
    object dxBarLargeButton7: TdxBarLargeButton
      Action = aHelp
      Category = 0
    end
    object dxBarLargeButton8: TdxBarLargeButton
      Action = aIzlez
      Category = 0
    end
    object dxBarLargeButton1: TdxBarLargeButton
      Action = aNov
      Category = 0
    end
    object dxBarLargeButton9: TdxBarLargeButton
      Action = aSnimiPecatenje
      Category = 0
      ScreenTip = tipSnimiKonf
    end
    object dxBarLargeButton11: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object ribbonFilterReset: TdxBarLargeButton
      Caption = #1056#1077#1089#1077#1090#1080#1088#1072#1112' '#1092#1080#1083#1090#1077#1088
      Category = 0
      Hint = #1056#1077#1089#1077#1090#1080#1088#1072#1112' '#1092#1080#1083#1090#1077#1088
      Visible = ivAlways
    end
    object dxBarLargeButton12: TdxBarLargeButton
      Action = aPodesuvanjePecatenje
      Category = 0
      ScreenTip = tipDizajnReport
    end
    object dxBarLargeButton13: TdxBarLargeButton
      Action = aBrisiPodesuvanjePecatenje
      Category = 0
      ScreenTip = tipBrisiKonf
    end
    object dxBarLargeButton14: TdxBarLargeButton
      Caption = #1048#1079#1073#1088#1080#1096#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      Category = 0
      Visible = ivAlways
      LargeImageIndex = 40
    end
    object dxBarLargeButton15: TdxBarLargeButton
      Action = aPageSetup
      Category = 0
      ScreenTip = tipSetiranjeStrana
    end
    object dxBarLargeButton16: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object dxBarLargeButton17: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object cxBarEditItem1: TcxBarEditItem
      Caption = 'New Item'
      Category = 0
      Hint = 'New Item'
      Visible = ivAlways
      PropertiesClassName = 'TcxCheckBoxProperties'
    end
    object dxBarSubItem1: TdxBarSubItem
      Caption = #1055#1077#1095#1072#1090#1077#1114#1077
      Category = 0
      Visible = ivAlways
      ImageIndex = 30
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton6'
        end>
    end
    object dxBarLBtnSnimiIzgled: TdxBarLargeButton
      Action = aSnimiIzgled
      Category = 0
    end
    object dxBarLBtnBrisiIzgled: TdxBarLargeButton
      Action = aBrisiIzgled
      Category = 0
    end
    object dxBarLargeButton10: TdxBarLargeButton
      Action = aSpustiSoberi
      Category = 0
    end
    object dxbrlrgbtn1: TdxBarLargeButton
      Action = aBrisi
      Caption = #1041#1088#1080#1096#1080' '#1057#1077#1083#1077#1082#1090#1080#1088#1072#1085#1080
      Category = 0
    end
  end
  object ActionList1: TActionList
    Images = dmRes.cxSmallImages
    Left = 555
    Top = 16
    object aNov: TAction
      Caption = #1053#1086#1074
      ImageIndex = 10
      ShortCut = 116
      OnExecute = aNovExecute
    end
    object aAzuriraj: TAction
      Caption = #1040#1078#1091#1088#1080#1088#1072#1112
      ImageIndex = 12
      ShortCut = 117
      OnExecute = aAzurirajExecute
    end
    object aBrisi: TAction
      Caption = #1041#1088#1080#1096#1080
      ImageIndex = 11
      ShortCut = 119
      OnExecute = aBrisiExecute
    end
    object aSnimiIzgled: TAction
      Caption = #1057#1085#1080#1084#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      ImageIndex = 9
      ShortCut = 24659
      OnExecute = aSnimiIzgledExecute
    end
    object aZacuvajExcel: TAction
      Caption = #1047#1072#1095#1091#1074#1072#1112' '#1074#1086' Excel'
      ImageIndex = 9
      ShortCut = 24645
      OnExecute = aZacuvajExcelExecute
    end
    object aPecatiTabela: TAction
      Caption = #1055#1077#1095#1072#1090#1080
      ImageIndex = 30
      OnExecute = aPecatiTabelaExecute
    end
    object aHelp: TAction
      Caption = #1055#1086#1084#1086#1096
      ImageIndex = 24
    end
    object aIzlez: TAction
      Caption = #1048#1079#1083#1077#1079
      ImageIndex = 25
      OnExecute = aIzlezExecute
    end
    object aZapisi: TAction
      Caption = #1047#1072#1087#1080#1096#1080
      ImageIndex = 7
      ShortCut = 120
      OnExecute = aZapisiExecute
    end
    object aOtkazi: TAction
      Caption = #1054#1090#1082#1072#1078#1080
      ImageIndex = 21
      ShortCut = 27
      OnExecute = aOtkaziExecute
    end
    object aRefresh: TAction
      Caption = 'aRefresh'
      ImageIndex = 18
      ShortCut = 118
      OnExecute = aRefreshExecute
    end
    object aSnimiPecatenje: TAction
      Caption = #1057#1085#1080#1084#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      ImageIndex = 39
      OnExecute = aSnimiPecatenjeExecute
    end
    object aPodesuvanjePecatenje: TAction
      Caption = #1044#1080#1079#1072#1112#1085' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090
      ImageIndex = 38
      OnExecute = aPodesuvanjePecatenjeExecute
    end
    object aBrisiPodesuvanjePecatenje: TAction
      Caption = #1041#1088#1080#1096#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      ImageIndex = 37
      OnExecute = aBrisiPodesuvanjePecatenjeExecute
    end
    object aPageSetup: TAction
      Caption = #1057#1077#1090#1080#1088#1072#1114#1077' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072
      ImageIndex = 38
      OnExecute = aPageSetupExecute
    end
    object aFormConfig: TAction
      Caption = 'aFormConfig'
      ShortCut = 24699
      OnExecute = aFormConfigExecute
    end
    object aBrisiIzgled: TAction
      Caption = #1041#1088#1080#1096#1080' '#1075#1086' '#1079#1072#1095#1091#1074#1072#1085#1080#1086#1090' '#1080#1079#1075#1083#1077#1076
      ImageIndex = 56
      OnExecute = aBrisiIzgledExecute
    end
    object aSpustiSoberi: TAction
      Caption = #1057#1087#1091#1096#1090#1080'/'#1057#1086#1073#1077#1088#1080' '#1058#1072#1073#1077#1083#1072
      ImageIndex = 65
      OnExecute = aSpustiSoberiExecute
    end
    object aPrefrliStavki: TAction
      Caption = #1055#1056#1045#1060#1056#1051#1048' >>'
      ImageIndex = 47
      OnExecute = aPrefrliStavkiExecute
    end
  end
  object dxComponentPrinter1: TdxComponentPrinter
    CurrentLink = dxComponentPrinter1Link1
    Version = 0
    Left = 695
    Top = 16
    object dxComponentPrinter1Link1: TdxGridReportLink
      Component = cxGrid1
      PrinterPage.DMPaper = 9
      PrinterPage.Footer = 6350
      PrinterPage.GrayShading = True
      PrinterPage.Header = 6350
      PrinterPage.Margins.Bottom = 12700
      PrinterPage.Margins.Left = 12700
      PrinterPage.Margins.Right = 12700
      PrinterPage.Margins.Top = 12700
      PrinterPage.Orientation = poLandscape
      PrinterPage.PageFooter.RightTitle.Strings = (
        #1057#1090#1088'.[Page #] '#1086#1076' [Total Pages]')
      PrinterPage.PageSize.X = 210000
      PrinterPage.PageSize.Y = 297000
      PrinterPage._dxMeasurementUnits_ = 0
      PrinterPage._dxLastMU_ = 2
      ReportDocument.CreationDate = 40336.425359629630000000
      OptionsFormatting.UseNativeStyles = True
      OptionsOnEveryPage.Caption = False
      OptionsOnEveryPage.FilterBar = False
      OptionsView.Caption = False
      OptionsView.FilterBar = False
      StyleRepository = dmRes.cxStyleRepositoryPrinting
      Styles.StyleSheet = dmRes.dxGridReportLinkStyleSheet
      BuiltInReportLink = True
    end
  end
  object dxBarScreenTipRepository1: TdxBarScreenTipRepository
    Left = 660
    Top = 16
    PixelsPerInch = 96
    object tipSnimiIzgled: TdxBarScreenTip
      Header.Text = #1057#1085#1080#1084#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      Description.Text = #1047#1072#1095#1091#1074#1072#1112' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090' '#1085#1072' '#1092#1086#1088#1084#1072#1090#1072' '#1080' '#1085#1072' '#1090#1072#1073#1077#1083#1072#1090#1072
    end
    object tipZacuvajExcel: TdxBarScreenTip
      Header.Text = #1047#1072#1095#1091#1074#1072#1112' '#1074#1086' Excel'
      Description.Text = #1057#1085#1080#1084#1080' '#1075#1080' '#1087#1086#1076#1072#1090#1086#1094#1080#1090#1077' '#1086#1076' '#1090#1072#1073#1077#1083#1072#1090#1072' '#1074#1086' Excel '#1092#1086#1088#1084#1072#1090
    end
    object tipDizajnReport: TdxBarScreenTip
      Header.Text = #1044#1080#1079#1072#1112#1085' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090
      Description.Text = #1055#1086#1076#1077#1089#1091#1074#1072#1114#1072' '#1085#1072' '#1080#1079#1075#1083#1077#1076#1086#1090' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090' '#1085#1072' '#1090#1072#1073#1077#1083#1072#1090#1072
    end
    object tipSetiranjeStrana: TdxBarScreenTip
      Header.Text = #1057#1077#1090#1080#1088#1072#1114#1077' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072
      Description.Text = #1055#1086#1076#1077#1089#1091#1074#1072#1114#1077' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072' '#1085#1072' '#1082#1086#1112#1072' '#1116#1077' '#1089#1077' '#1087#1077#1095#1072#1090#1080' '#1090#1072#1073#1077#1083#1072#1090#1072
    end
    object tipSnimiKonf: TdxBarScreenTip
      Header.Text = #1057#1085#1080#1084#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      Description.Text = #1047#1072#1095#1091#1074#1072#1112' '#1075#1080' '#1087#1086#1076#1077#1089#1091#1074#1072#1114#1072#1090#1072' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090' '#1080' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072' '#1079#1072' '#1087#1077#1095#1072#1090#1077#1114#1077
    end
    object tipBrisiKonf: TdxBarScreenTip
      Header.Text = #1041#1088#1080#1096#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      Description.Text = #1048#1079#1073#1088#1080#1096#1080' '#1075#1080' '#1087#1086#1076#1077#1089#1091#1074#1072#1114#1072#1090#1072' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090' '#1080' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072' '#1079#1072' '#1087#1077#1095#1072#1090#1077#1114#1077
    end
  end
  object dsKamion: TDataSource
    DataSet = tblKamion
    Left = 746
    Top = 51
  end
  object tblVoKamion: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE MAN_POR_S_TRAILER'
      'SET '
      '    ID_PORACKA_STAVKA = :ID_PORACKA_STAVKA,'
      '    ID_TRAILER = :ID_TRAILER,'
      '    KOLICINA = :KOL_TRAILER'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    MAN_POR_S_TRAILER'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO MAN_POR_S_TRAILER('
      '    ID,'
      '    ID_PORACKA_STAVKA,'
      '    ID_TRAILER,'
      '    KOLICINA'
      ')'
      'VALUES('
      '    :ID,'
      '    :ID_PORACKA_STAVKA,'
      '    :ID_TRAILER,'
      '    :KOLICINA'
      ')')
    RefreshSQL.Strings = (
      
        'select '#39'K'#1072#1084#1080#1086#1085' : '#39' || T.ID || '#39' / '#39' || T.NAZIV OPIS, PST.ID_PORA' +
        'CKA_STAVKA, T.ID ID_TRAILER,'
      
        '       sum(PST.KOLICINA) KOL_TRAILER, T.NAZIV NAZIV_TRAILER, T.D' +
        'ATUM DATUM_TRANSPORT, P.REF_NO, PS.VID_ARTIKAL,'
      
        '       PS.ARTIKAL, PS.BROJ, PS.SUNGER, PS.BOJA_NOGARKI, PS.LABEL' +
        'S, PS.GLIDERS, MA.NAZIV NAZIV_ARTIKAL,'
      
        '       PS.KOLICINA VK_KOLICINA, PS.CUSTOMER_NAME, sum(coalesce((' +
        'select MOS.KOLICINA'
      
        '                                                                ' +
        'from MTR_OUT MO'
      
        '                                                                ' +
        'inner join MTR_OUT_S MOS on MOS.MTR_OUT_ID = MO.ID'
      
        '                                                                ' +
        'inner join MAN_RABOTEN_NALOG_STAVKA RNS1 on RNS1.ID = MOS.ID'
      
        '                                                                ' +
        'where MO.WO_REF_ID = RNS1.ID_RABOTEN_NALOG), 0)) ISPORACANA_KOLI' +
        'CINA , ps.SKU, ps.ISPORAKA_DO_DATUM, ps.ISPORAKA_OD_DATUM'
      'from MAN_TRAILER T'
      'left join MAN_POR_S_TRAILER PST on T.ID = PST.ID_TRAILER'
      
        'left join MAN_PORACKA_STAVKA PS on PS.ID = PST.ID_PORACKA_STAVKA' +
        ' and'
      '      PS.STATUS <> 3'
      'left join MAN_PORACKA P on P.ID = PS.ID_PORACKA and'
      '      P.STATUS <> 3'
      'left join MTR_ARTIKAL MA on MA.ARTVID = PS.VID_ARTIKAL and'
      '      MA.ID = PS.ARTIKAL'
      'where T.STATUS = 0'
      
        'group by 1, 2, 3, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17 ' +
        ', ps.SKU, ps.ISPORAKA_DO_DATUM, ps.ISPORAKA_OD_DATUM '
      ''
      
        '/*select T.ID, '#39'K'#1072#1084#1080#1086#1085' : '#39' || T.ID || '#39' / '#39' || T.NAZIV OPIS, PST' +
        '.ID_PORACKA_STAVKA, T.ID ID_TRAILER,'
      
        '       PST.KOLICINA KOL_TRAILER, T.NAZIV NAZIV_TRAILER, T.DATUM ' +
        'DATUM_TRANSPORT, P.REF_NO, PS.VID_ARTIKAL, PS.ARTIKAL,'
      '       ps.BROJ,'
      '    --   PS.TAPACIR_SEDISTE, PS.TAPACIR_NAZAD, '
      '        (select AA.NAZIV'
      '        from MTR_ARTIKAL AA'
      '        where AA.ARTVID = PS.VID_TCS and'
      '              AA.ID = PS.ARTIKAL_TSC) TAPACIR_CEL_STOL,'
      '       (select AA.NAZIV'
      '        from MTR_ARTIKAL AA'
      '        where AA.ARTVID = PS.VID_TSED and'
      '              AA.ID = PS.ARTIKAL_TSED) TAPACIR_SEDISTE,'
      '       (select AA.NAZIV'
      '        from MTR_ARTIKAL AA'
      '        where AA.ARTVID = PS.VID_TNAZ and'
      '              AA.ID = PS.ARTIKAL_TNAZ) TAPACIR_NAZAD,'
      
        '       PS.SUNGER, PS.BOJA_NOGARKI, PS.LABELS, PS.GLIDERS, MA.NAZ' +
        'IV NAZIV_ARTIKAL,'
      '       PS.KOLICINA VK_KOLICINA, PS.CUSTOMER_NAME, '
      '       ps.PALETA_BR1, ps.PALETA_BR2, ps.PALETA_BR3,'
      '       sum(coalesce((select MOS.KOLICINA'
      
        '                                                                ' +
        'from MTR_OUT MO'
      
        '                                                                ' +
        'inner join MTR_OUT_S MOS on MOS.MTR_OUT_ID = MO.ID'
      
        '                                                                ' +
        'inner join MAN_RABOTEN_NALOG_STAVKA RNS1 on RNS1.ID = MOS.ID'
      
        '                                                                ' +
        'where MO.WO_REF_ID = RNS1.ID_RABOTEN_NALOG), 0)) ISPORACANA_KOLI' +
        'CINA'
      'from   MAN_TRAILER T'
      'left join MAN_POR_S_TRAILER PST  on T.ID = PST.ID_TRAILER'
      
        'left join MAN_PORACKA_STAVKA PS on PS.ID = PST.ID_PORACKA_STAVKA' +
        ' and'
      '      PS.STATUS <> 3'
      'left join MAN_PORACKA P on P.ID = PS.ID_PORACKA and'
      '      P.STATUS <> 3'
      'left join MTR_ARTIKAL MA on MA.ARTVID = PS.VID_ARTIKAL and'
      '      MA.ID = PS.ARTIKAL'
      'where --(pst.id_poracka_stavka = :MAS_ID)'
      '      --t.id = :trailer'
      '      T.STATUS = 0'
      
        'group by 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11 ,ps.VID_TCS,ps.VID_TN' +
        'AZ, ps.VID_TSED, ps.ARTIKAL_TSC, ps.ARTIKAL_TNAZ, ps.ARTIKAL_TSE' +
        'D,'
      ' --11, 12, 13,'
      '  15, 16, 17, 18, 19,20,21,22,23,24 */')
    SelectSQL.Strings = (
      
        'select '#39'K'#1072#1084#1080#1086#1085' : '#39' || T.ID || '#39' / '#39' || T.NAZIV OPIS, PST.ID_PORA' +
        'CKA_STAVKA, T.ID ID_TRAILER,'
      
        '       sum(PST.KOLICINA) KOL_TRAILER, T.NAZIV NAZIV_TRAILER, T.D' +
        'ATUM DATUM_TRANSPORT, P.REF_NO, PS.VID_ARTIKAL,'
      
        '       PS.ARTIKAL, PS.BROJ, PS.SUNGER, PS.BOJA_NOGARKI, PS.LABEL' +
        'S, PS.GLIDERS, MA.NAZIV NAZIV_ARTIKAL,'
      
        '       PS.KOLICINA VK_KOLICINA, PS.CUSTOMER_NAME, sum(coalesce((' +
        'select MOS.KOLICINA'
      
        '                                                                ' +
        'from MTR_OUT MO'
      
        '                                                                ' +
        'inner join MTR_OUT_S MOS on MOS.MTR_OUT_ID = MO.ID'
      
        '                                                                ' +
        'inner join MAN_RABOTEN_NALOG_STAVKA RNS1 on RNS1.ID = MOS.ID'
      
        '                                                                ' +
        'where MO.WO_REF_ID = RNS1.ID_RABOTEN_NALOG), 0)) ISPORACANA_KOLI' +
        'CINA, ps.SKU, ps.ISPORAKA_DO_DATUM, ps.ISPORAKA_OD_DATUM'
      'from MAN_TRAILER T'
      'left join MAN_POR_S_TRAILER PST on T.ID = PST.ID_TRAILER'
      
        'left join MAN_PORACKA_STAVKA PS on PS.ID = PST.ID_PORACKA_STAVKA' +
        ' and'
      '      PS.STATUS <> 3'
      'left join MAN_PORACKA P on P.ID = PS.ID_PORACKA and'
      '      P.STATUS <> 3'
      'left join MTR_ARTIKAL MA on MA.ARTVID = PS.VID_ARTIKAL and'
      '      MA.ID = PS.ARTIKAL'
      'where T.STATUS = 0'
      
        'group by 1, 2, 3, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17,' +
        ' ps.SKU, ps.ISPORAKA_DO_DATUM, ps.ISPORAKA_OD_DATUM  '
      ''
      
        '/*select T.ID, '#39'K'#1072#1084#1080#1086#1085' : '#39' || T.ID || '#39' / '#39' || T.NAZIV OPIS, PST' +
        '.ID_PORACKA_STAVKA, T.ID ID_TRAILER,'
      
        '       PST.KOLICINA KOL_TRAILER, T.NAZIV NAZIV_TRAILER, T.DATUM ' +
        'DATUM_TRANSPORT, P.REF_NO, PS.VID_ARTIKAL, PS.ARTIKAL,'
      '       ps.BROJ,'
      '    --   PS.TAPACIR_SEDISTE, PS.TAPACIR_NAZAD, '
      '        (select AA.NAZIV'
      '        from MTR_ARTIKAL AA'
      '        where AA.ARTVID = PS.VID_TCS and'
      '              AA.ID = PS.ARTIKAL_TSC) TAPACIR_CEL_STOL,'
      '       (select AA.NAZIV'
      '        from MTR_ARTIKAL AA'
      '        where AA.ARTVID = PS.VID_TSED and'
      '              AA.ID = PS.ARTIKAL_TSED) TAPACIR_SEDISTE,'
      '       (select AA.NAZIV'
      '        from MTR_ARTIKAL AA'
      '        where AA.ARTVID = PS.VID_TNAZ and'
      '              AA.ID = PS.ARTIKAL_TNAZ) TAPACIR_NAZAD,'
      
        '       PS.SUNGER, PS.BOJA_NOGARKI, PS.LABELS, PS.GLIDERS, MA.NAZ' +
        'IV NAZIV_ARTIKAL,'
      '       PS.KOLICINA VK_KOLICINA, PS.CUSTOMER_NAME, '
      '       ps.PALETA_BR1, ps.PALETA_BR2, ps.PALETA_BR3,'
      '       sum(coalesce((select MOS.KOLICINA'
      
        '                                                                ' +
        'from MTR_OUT MO'
      
        '                                                                ' +
        'inner join MTR_OUT_S MOS on MOS.MTR_OUT_ID = MO.ID'
      
        '                                                                ' +
        'inner join MAN_RABOTEN_NALOG_STAVKA RNS1 on RNS1.ID = MOS.ID'
      
        '                                                                ' +
        'where MO.WO_REF_ID = RNS1.ID_RABOTEN_NALOG), 0)) ISPORACANA_KOLI' +
        'CINA'
      'from   MAN_TRAILER T'
      'left join MAN_POR_S_TRAILER PST  on T.ID = PST.ID_TRAILER'
      
        'left join MAN_PORACKA_STAVKA PS on PS.ID = PST.ID_PORACKA_STAVKA' +
        ' and'
      '      PS.STATUS <> 3'
      'left join MAN_PORACKA P on P.ID = PS.ID_PORACKA and'
      '      P.STATUS <> 3'
      'left join MTR_ARTIKAL MA on MA.ARTVID = PS.VID_ARTIKAL and'
      '      MA.ID = PS.ARTIKAL'
      'where --(pst.id_poracka_stavka = :MAS_ID)'
      '      --t.id = :trailer'
      '      T.STATUS = 0'
      
        'group by 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11 ,ps.VID_TCS,ps.VID_TN' +
        'AZ, ps.VID_TSED, ps.ARTIKAL_TSC, ps.ARTIKAL_TNAZ, ps.ARTIKAL_TSE' +
        'D,'
      ' --11, 12, 13,'
      '  15, 16, 17, 18, 19,20,21,22,23,24 */')
    AutoUpdateOptions.GeneratorName = 'GEN__ID'
    AutoUpdateOptions.WhenGetGenID = wgBeforePost
    AutoUpdateOptions.AutoParamsToFields = True
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 690
    Top = 94
    object tblVoKamionOPIS: TFIBStringField
      FieldName = 'OPIS'
      Size = 123
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblVoKamionID_PORACKA_STAVKA: TFIBBCDField
      FieldName = 'ID_PORACKA_STAVKA'
      Size = 0
    end
    object tblVoKamionID_TRAILER: TFIBIntegerField
      FieldName = 'ID_TRAILER'
    end
    object tblVoKamionNAZIV_TRAILER: TFIBStringField
      FieldName = 'NAZIV_TRAILER'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblVoKamionDATUM_TRANSPORT: TFIBDateField
      FieldName = 'DATUM_TRANSPORT'
    end
    object tblVoKamionREF_NO: TFIBStringField
      FieldName = 'REF_NO'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblVoKamionVID_ARTIKAL: TFIBIntegerField
      FieldName = 'VID_ARTIKAL'
    end
    object tblVoKamionARTIKAL: TFIBIntegerField
      FieldName = 'ARTIKAL'
    end
    object tblVoKamionBROJ: TFIBStringField
      FieldName = 'BROJ'
      Size = 200
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblVoKamionSUNGER: TFIBStringField
      FieldName = 'SUNGER'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblVoKamionBOJA_NOGARKI: TFIBStringField
      FieldName = 'BOJA_NOGARKI'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblVoKamionLABELS: TFIBStringField
      FieldName = 'LABELS'
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblVoKamionGLIDERS: TFIBStringField
      FieldName = 'GLIDERS'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblVoKamionNAZIV_ARTIKAL: TFIBStringField
      FieldName = 'NAZIV_ARTIKAL'
      Size = 500
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblVoKamionVK_KOLICINA: TFIBFloatField
      FieldName = 'VK_KOLICINA'
    end
    object tblVoKamionCUSTOMER_NAME: TFIBStringField
      FieldName = 'CUSTOMER_NAME'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblVoKamionISPORACANA_KOLICINA: TFIBFloatField
      FieldName = 'ISPORACANA_KOLICINA'
    end
    object tblVoKamionKOL_TRAILER: TFIBBCDField
      FieldName = 'KOL_TRAILER'
      Size = 0
    end
    object tblVoKamionSKU: TFIBStringField
      FieldName = 'SKU'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblVoKamionISPORAKA_DO_DATUM: TFIBDateField
      FieldName = 'ISPORAKA_DO_DATUM'
    end
    object tblVoKamionISPORAKA_OD_DATUM: TFIBDateField
      FieldName = 'ISPORAKA_OD_DATUM'
    end
  end
  object dsVoKamion: TDataSource
    DataSet = tblVoKamion
    OnDataChange = dsVoKamionDataChange
    Left = 690
    Top = 59
  end
  object cxGridPopupMenu2: TcxGridPopupMenu
    Grid = cxGrid2
    PopupMenus = <>
    Left = 590
    Top = 65520
  end
  object qInsertVoKamion: TpFIBQuery
    Transaction = dmKon.qTransaction
    Database = dmKon.fibBaza
    SQL.Strings = (
      'INSERT INTO MAN_POR_S_TRAILER('
      '    ID_PORACKA_STAVKA,'
      '    ID_TRAILER,'
      '    KOLICINA ,'
      '    id_rn_stavka'
      ')'
      'VALUES('
      '    :ID_PORACKA_STAVKA,'
      '    :ID_TRAILER,'
      '    :KOLICINA ,'
      '    :id_rn_stavka'
      ')')
    Left = 601
    Top = 78
    qoAutoCommit = True
    qoStartTransaction = True
  end
  object qBrisiOdKamion: TpFIBQuery
    Transaction = dmKon.qTransaction
    Database = dmKon.fibBaza
    SQL.Strings = (
      'DELETE FROM'
      '    MAN_POR_S_TRAILER'
      'WHERE'
      '        ID = :id'
      '    ')
    Left = 529
    Top = 78
    qoAutoCommit = True
    qoStartTransaction = True
  end
  object qUpdateKolKamion: TpFIBQuery
    Transaction = dmKon.qTransaction
    Database = dmKon.fibBaza
    SQL.Strings = (
      'update man_por_s_trailer pst'
      'set pst.kolicina = (pst.kolicina - cast(:KolPrefBri as integer))'
      'where ( pst.id = :id)')
    Left = 521
    Top = 134
    qoAutoCommit = True
    qoStartTransaction = True
  end
  object tblKamion: TpFIBDataSet
    SelectSQL.Strings = (
      
        'select PST.ID, '#39'K'#1072#1084#1080#1086#1085' : '#39' || T.ID || '#39' / '#39' || T.NAZIV OPIS, PST' +
        '.ID_PORACKA_STAVKA, PST.ID_TRAILER,'
      
        '       PST.KOLICINA KOL_TRAILER, T.NAZIV NAZIV_TRAILER, T.DATUM ' +
        'DATUM_TRANSPORT, P.REF_NO, PS.VID_ARTIKAL, PS.ARTIKAL,'
      '       PS.BROJ,'
      
        '       PS.SUNGER, PS.BOJA_NOGARKI, PS.LABELS, PS.GLIDERS, MA.NAZ' +
        'IV NAZIV_ARTIKAL,'
      
        '       PS.KOLICINA VK_KOLICINA, PS.CUSTOMER_NAME, PST.ID_RN_STAV' +
        'KA,'
      '       ((select first 1 RNS.LOT'
      '         from MAN_RABOTEN_NALOG_STAVKA RNS'
      '         where RNS.ID_PORACKA_STAVKA = PS.ID and'
      
        '               RNS.STATUS <> 3)) LOT, sum(coalesce((select MOS.K' +
        'OLICINA'
      
        '                                                    from MTR_OUT' +
        ' MO'
      
        '                                                    inner join M' +
        'TR_OUT_S MOS on MOS.MTR_OUT_ID = MO.ID'
      
        '                                                    inner join M' +
        'AN_RABOTEN_NALOG_STAVKA RNS1 on RNS1.ID = MOS.ID'
      
        '                                                    where MO.WO_' +
        'REF_ID = RNS1.ID_RABOTEN_NALOG), 0)) ISPORACANA_KOLICINA, ps.SKU' +
        ', ps.ISPORAKA_DO_DATUM, ps.ISPORAKA_OD_DATUM'
      ''
      'from MAN_POR_S_TRAILER PST'
      'inner join MAN_TRAILER T on T.ID = PST.ID_TRAILER'
      
        'inner join MAN_PORACKA_STAVKA PS on PS.ID = PST.ID_PORACKA_STAVK' +
        'A and'
      '      PS.STATUS <> 3'
      'inner join MAN_PORACKA P on P.ID = PS.ID_PORACKA and'
      '      P.STATUS <> 3'
      'inner join MTR_ARTIKAL MA on MA.ARTVID = PS.VID_ARTIKAL and'
      '      MA.ID = PS.ARTIKAL'
      'where T.STATUS = 0'
      
        'group by 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, ' +
        '17, 18, 19, PS.ID, ps.SKU, ps.ISPORAKA_DO_DATUM, ps.ISPORAKA_OD_' +
        'DATUM  '
      ''
      ''
      
        '/*select T.ID, '#39'K'#1072#1084#1080#1086#1085' : '#39' || T.ID || '#39' / '#39' || T.NAZIV OPIS, PST' +
        '.ID_PORACKA_STAVKA, PST.ID_TRAILER,'
      
        '       PST.KOLICINA KOL_TRAILER, T.NAZIV NAZIV_TRAILER, T.DATUM ' +
        'DATUM_TRANSPORT, P.REF_NO, PS.VID_ARTIKAL, PS.ARTIKAL,'
      '       ps.BROJ,'
      '      -- PS.TAPACIR_SEDISTE, PS.TAPACIR_NAZAD,'
      '       (select AA.NAZIV'
      '        from MTR_ARTIKAL AA'
      '        where AA.ARTVID = PS.VID_TCS and'
      '              AA.ID = PS.ARTIKAL_TSC) TAPACIR_CEL_STOL,'
      '       (select AA.NAZIV'
      '        from MTR_ARTIKAL AA'
      '        where AA.ARTVID = PS.VID_TSED and'
      '              AA.ID = PS.ARTIKAL_TSED) TAPACIR_SEDISTE,'
      '       (select AA.NAZIV'
      '        from MTR_ARTIKAL AA'
      '        where AA.ARTVID = PS.VID_TNAZ and'
      '              AA.ID = PS.ARTIKAL_TNAZ) TAPACIR_NAZAD,'
      
        '        PS.SUNGER, PS.BOJA_NOGARKI, PS.LABELS, PS.GLIDERS, MA.NA' +
        'ZIV NAZIV_ARTIKAL,'
      
        '       EM.IZVEDENA_EM, EM.KOEFICIENT, M.NAZIV NAZIV_MERKA, PS.KO' +
        'LICINA VK_KOLICINA,'
      
        '       round(PST.KOLICINA / coalesce(EM.KOEFICIENT, 1)) BR_KUTII' +
        ', coalesce(MA.VOLUMEN, 0) VOLUMEN, PST.ID_RN_STAVKA,'
      '       PS.CUSTOMER_NAME,'
      '       ((select first 1 RNS.LOT'
      '         from MAN_RABOTEN_NALOG_STAVKA RNS'
      '         where RNS.ID_PORACKA_STAVKA = PS.ID and'
      '               RNS.STATUS <> 3)) LOT, '
      '               ps.PALETA_BR1, ps.PALETA_BR2, ps.PALETA_BR3,'
      '               sum(coalesce((select MOS.KOLICINA'
      
        '                                                    from MTR_OUT' +
        ' MO'
      
        '                                                    inner join M' +
        'TR_OUT_S MOS on MOS.MTR_OUT_ID = MO.ID'
      
        '                                                    inner join M' +
        'AN_RABOTEN_NALOG_STAVKA RNS1 on RNS1.ID = MOS.ID'
      
        '                                                    where MO.WO_' +
        'REF_ID = RNS1.ID_RABOTEN_NALOG), 0)) ISPORACANA_KOLICINA'
      ''
      'from MAN_POR_S_TRAILER PST'
      'inner join MAN_TRAILER T on T.ID = PST.ID_TRAILER'
      
        'inner join MAN_PORACKA_STAVKA PS on PS.ID = PST.ID_PORACKA_STAVK' +
        'A and'
      '      PS.STATUS <> 3'
      'inner join MAN_PORACKA P on P.ID = PS.ID_PORACKA and'
      '      P.STATUS <> 3'
      'inner join MTR_ARTIKAL MA on MA.ARTVID = PS.VID_ARTIKAL and'
      '      MA.ID = PS.ARTIKAL'
      'left join MTR_IZVEDENA_EM EM on EM.ARTVID = MA.ARTVID and'
      '      MA.ID = EM.ARTSIF'
      'left join MAT_MERKA M on M.ID = EM.IZVEDENA_EM'
      'where --(pst.id_poracka_stavka = :MAS_ID)'
      '      --t.id = :trailer'
      '      T.STATUS = 0'
      'group by 1, 2, 3, 4, 5, 6, 7, 8, 9, 10,11,'
      
        'ps.VID_TCS, ps.ARTIKAL_TSC, ps.VID_TNAZ, ps.ARTIKAL_TNAZ, ps.VID' +
        '_TSED, ps.ARTIKAL_TSED,'
      ' --11, 12, 13,'
      
        '   15, 16, 17, 18, 19, 20, 21, 22, 23, 24,25,26, PS.CUSTOMER_NAM' +
        'E, PS.ID,ps.PALETA_BR1, ps.PALETA_BR2, ps.PALETA_BR3 */'
      '   '
      '   ')
    AutoUpdateOptions.GeneratorName = 'GEN__ID'
    AutoUpdateOptions.WhenGetGenID = wgBeforePost
    AutoUpdateOptions.AutoParamsToFields = True
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 746
    Top = 94
    oVisibleRecno = True
    object tblKamionOPIS: TFIBStringField
      FieldName = 'OPIS'
      Size = 123
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblKamionID_PORACKA_STAVKA: TFIBBCDField
      FieldName = 'ID_PORACKA_STAVKA'
      Size = 0
    end
    object tblKamionID_TRAILER: TFIBIntegerField
      FieldName = 'ID_TRAILER'
    end
    object tblKamionNAZIV_TRAILER: TFIBStringField
      FieldName = 'NAZIV_TRAILER'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblKamionDATUM_TRANSPORT: TFIBDateField
      FieldName = 'DATUM_TRANSPORT'
    end
    object tblKamionREF_NO: TFIBStringField
      FieldName = 'REF_NO'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblKamionVID_ARTIKAL: TFIBIntegerField
      FieldName = 'VID_ARTIKAL'
    end
    object tblKamionARTIKAL: TFIBIntegerField
      FieldName = 'ARTIKAL'
    end
    object tblKamionBROJ: TFIBStringField
      FieldName = 'BROJ'
      Size = 200
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblKamionSUNGER: TFIBStringField
      FieldName = 'SUNGER'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblKamionBOJA_NOGARKI: TFIBStringField
      FieldName = 'BOJA_NOGARKI'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblKamionLABELS: TFIBStringField
      FieldName = 'LABELS'
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblKamionGLIDERS: TFIBStringField
      FieldName = 'GLIDERS'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblKamionNAZIV_ARTIKAL: TFIBStringField
      FieldName = 'NAZIV_ARTIKAL'
      Size = 500
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblKamionVK_KOLICINA: TFIBFloatField
      FieldName = 'VK_KOLICINA'
    end
    object tblKamionCUSTOMER_NAME: TFIBStringField
      FieldName = 'CUSTOMER_NAME'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblKamionLOT: TFIBStringField
      FieldName = 'LOT'
      Size = 30
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblKamionISPORACANA_KOLICINA: TFIBFloatField
      FieldName = 'ISPORACANA_KOLICINA'
    end
    object tblKamionID: TFIBIntegerField
      FieldName = 'ID'
    end
    object tblKamionID_RN_STAVKA: TFIBBCDField
      FieldName = 'ID_RN_STAVKA'
      Size = 0
    end
    object tblKamionKOL_TRAILER: TFIBIntegerField
      FieldName = 'KOL_TRAILER'
    end
    object tblKamionSKU: TFIBStringField
      FieldName = 'SKU'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblKamionISPORAKA_DO_DATUM: TFIBDateField
      FieldName = 'ISPORAKA_DO_DATUM'
    end
    object tblKamionISPORAKA_OD_DATUM: TFIBDateField
      FieldName = 'ISPORAKA_OD_DATUM'
    end
  end
  object tblTapacir: TpFIBDataSet
    RefreshSQL.Strings = (
      
        'select PE.ID_PORACKA_STAVKA, PE.VID_TAPACIR, PE.TAPACIR, T.NAZIV' +
        ' NAZIV_TAPACIR, PE.VID_SUROVINA, PE.SUROVINA,'
      '       S.NAZIV NAZIV_SUROVINA, PE.KOLICINA, PE.MERKA, PE.PALETA'
      'from MAN_PORACKA_ELEMENT PE'
      'inner join MTR_ARTIKAL T on T.ARTVID = PE.VID_TAPACIR and'
      '      T.ID = PE.TAPACIR'
      'inner join MTR_ARTIKAL S on S.ARTVID = PE.VID_SUROVINA and'
      '      S.ID = PE.SUROVINA'
      '--where PE.ID_PORACKA_STAVKA = :ID_PORACKA_STAVKA')
    SelectSQL.Strings = (
      
        'select PE.ID_PORACKA_STAVKA, PE.VID_TAPACIR, PE.TAPACIR, T.NAZIV' +
        ' NAZIV_TAPACIR, PE.VID_SUROVINA, PE.SUROVINA,'
      '       S.NAZIV NAZIV_SUROVINA, PE.KOLICINA, PE.MERKA, PE.PALETA'
      'from MAN_PORACKA_ELEMENT PE'
      'inner join MTR_ARTIKAL T on T.ARTVID = PE.VID_TAPACIR and'
      '      T.ID = PE.TAPACIR'
      'inner join MTR_ARTIKAL S on S.ARTVID = PE.VID_SUROVINA and'
      '      S.ID = PE.SUROVINA'
      '--where PE.ID_PORACKA_STAVKA = :ID_PORACKA_STAVKA')
    AutoUpdateOptions.GeneratorName = 'GEN__ID'
    AutoUpdateOptions.WhenGetGenID = wgBeforePost
    AutoUpdateOptions.AutoParamsToFields = True
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 1039
    Top = 128
    object tblTapacirID_PORACKA_STAVKA: TFIBBCDField
      FieldName = 'ID_PORACKA_STAVKA'
      Size = 0
    end
    object tblTapacirVID_TAPACIR: TFIBIntegerField
      FieldName = 'VID_TAPACIR'
    end
    object tblTapacirTAPACIR: TFIBIntegerField
      FieldName = 'TAPACIR'
    end
    object tblTapacirNAZIV_TAPACIR: TFIBStringField
      FieldName = 'NAZIV_TAPACIR'
      Size = 500
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblTapacirVID_SUROVINA: TFIBIntegerField
      FieldName = 'VID_SUROVINA'
    end
    object tblTapacirSUROVINA: TFIBIntegerField
      FieldName = 'SUROVINA'
    end
    object tblTapacirNAZIV_SUROVINA: TFIBStringField
      FieldName = 'NAZIV_SUROVINA'
      Size = 500
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblTapacirKOLICINA: TFIBFloatField
      FieldName = 'KOLICINA'
    end
    object tblTapacirMERKA: TFIBStringField
      FieldName = 'MERKA'
      Size = 5
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblTapacirPALETA: TFIBStringField
      FieldName = 'PALETA'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object dsTapacir: TDataSource
    DataSet = tblTapacir
    Left = 1055
    Top = 40
  end
end
