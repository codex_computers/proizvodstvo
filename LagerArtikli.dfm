object frmLagerArtikli: TfrmLagerArtikli
  Left = 128
  Top = 212
  BorderIcons = [biSystemMenu, biMaximize]
  Caption = #1051#1072#1075#1077#1088' '#1083#1080#1089#1090#1072
  ClientHeight = 687
  ClientWidth = 934
  Color = clBtnFace
  Constraints.MinHeight = 500
  Constraints.MinWidth = 610
  Font.Charset = RUSSIAN_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  ShowHint = True
  WindowState = wsMaximized
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object dxRibbon1: TdxRibbon
    Left = 0
    Top = 0
    Width = 934
    Height = 126
    BarManager = dxBarManager1
    ColorSchemeName = 'Blue'
    Contexts = <>
    TabOrder = 0
    TabStop = False
    object dxRibbon1Tab1: TdxRibbonTab
      Active = True
      Caption = #1052#1077#1085#1080
      Groups = <
        item
          ToolbarName = 'dxBarManager1Bar2'
        end
        item
          ToolbarName = 'dxBarManager1Bar3'
        end>
      Index = 0
    end
    object dxRibbon1Tab2: TdxRibbonTab
      Caption = #1055#1086#1076#1077#1089#1091#1074#1072#1114#1072
      Groups = <
        item
          ToolbarName = 'dxBarManager1Bar4'
        end
        item
          Caption = #1058#1072#1073#1077#1083#1072
          ToolbarName = 'dxBarManager1Bar5'
        end>
      Index = 1
    end
  end
  object StatusBar1: TdxRibbonStatusBar
    Left = 0
    Top = 664
    Width = 934
    Height = 23
    Panels = <
      item
        PanelStyleClassName = 'TdxStatusBarTextPanelStyle'
        Text = ' F9 - '#1055#1088#1080#1082#1072#1078#1080', F10 - '#1055#1077#1095#1072#1090#1080' '#1083#1072#1075#1077#1088' '#1083#1080#1089#1090#1072', Esc - '#1048#1079#1083#1077#1079
      end>
    Ribbon = dxRibbon1
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clDefault
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
  end
  object dPanel: TPanel
    Left = 0
    Top = 126
    Width = 934
    Height = 51
    Align = alTop
    Color = 15790320
    ParentBackground = False
    TabOrder = 2
    object lblPartner: TLabel
      Left = 14
      Top = 17
      Width = 57
      Height = 13
      Caption = #1052#1072#1075#1072#1094#1080#1085' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label3: TLabel
      Left = 207
      Top = 17
      Width = 64
      Height = 13
      Alignment = taRightJustify
      Caption = #1044#1072#1090#1091#1084' '#1076#1086' :'
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object DoDatum: TcxDateEdit
      Tag = 1
      Left = 277
      Top = 14
      BeepOnEnter = False
      Properties.ClearKey = 46
      Properties.ShowTime = False
      TabOrder = 1
      OnKeyDown = EnterKakoTab
      Width = 111
    end
    object ZapisiButton: TcxButton
      Left = 414
      Top = 12
      Width = 100
      Height = 25
      Action = aPrikazi
      Colors.Pressed = clGradientActiveCaption
      TabOrder = 2
    end
    object lcbMagacin: TcxLookupComboBox
      Tag = 1
      Left = 77
      Top = 14
      Properties.DropDownAutoSize = True
      Properties.DropDownSizeable = True
      Properties.KeyFieldNames = 'ID'
      Properties.ListColumns = <
        item
          FieldName = 'ID'
        end
        item
          Width = 400
          FieldName = 'NAZIV'
        end>
      Properties.ListOptions.SyncMode = True
      Properties.ListSource = dsReMagacin
      TabOrder = 0
      OnKeyDown = EnterKakoTab
      Width = 111
    end
  end
  object lPanel: TPanel
    Left = 0
    Top = 185
    Width = 934
    Height = 479
    Align = alClient
    Color = 15790320
    ParentBackground = False
    TabOrder = 7
    object cxPageControl1: TcxPageControl
      Left = 1
      Top = 1
      Width = 932
      Height = 477
      Align = alClient
      TabOrder = 0
      Properties.ActivePage = cxTabTabelaren
      Properties.CustomButtons.Buttons = <>
      Properties.TabHeight = 25
      Properties.TabWidth = 200
      ClientRectBottom = 477
      ClientRectRight = 932
      ClientRectTop = 27
      object cxTabTabelaren: TcxTabSheet
        Caption = #1058#1072#1073#1077#1083#1072#1088#1077#1085
        ImageIndex = 0
        object cxGrid1: TcxGrid
          Left = 0
          Top = 0
          Width = 932
          Height = 450
          Align = alClient
          TabOrder = 0
          RootLevelOptions.DetailTabsPosition = dtpTop
          object cxGrid1DBTableView1: TcxGridDBTableView
            OnKeyPress = cxGrid1DBTableView1KeyPress
            Navigator.Buttons.CustomButtons = <>
            DataController.DataSource = dm.dsLagerArtikli
            DataController.Summary.DefaultGroupSummaryItems = <
              item
                Format = '###,##0.00'
                Kind = skSum
                Position = spFooter
              end
              item
                Format = '###,##0.00'
                Kind = skSum
                Position = spFooter
              end
              item
                Format = '###,##0.00'
                Kind = skSum
                Position = spFooter
              end
              item
                Format = '###,##0.00'
                Kind = skSum
                Position = spFooter
              end
              item
                Format = '###,##0.00'
                Kind = skSum
                Position = spFooter
              end
              item
                Format = '###,##0.00'
                Kind = skSum
                Position = spFooter
              end
              item
                Kind = skCount
                Position = spFooter
                Column = cxGrid1DBTableView1O_ART_SIF
              end
              item
                Format = '###,##0.00'
                Kind = skSum
                Position = spFooter
              end
              item
                Format = '###,##0.00'
                Kind = skSum
                Position = spFooter
              end
              item
                Format = '###,##0.00'
                Kind = skSum
                Position = spFooter
              end
              item
                Format = '###,##0.00'
                Kind = skSum
                Position = spFooter
              end
              item
                Format = '###,##0.00'
                Kind = skSum
                Position = spFooter
                Column = cxGrid1DBTableView1O_LAGER
              end
              item
                Format = '###,##0.00'
                Kind = skSum
                Position = spFooter
                Column = cxGrid1DBTableView1O_IZLEZ
              end
              item
                Format = '###,##0.00'
                Kind = skSum
                Position = spFooter
                Column = cxGrid1DBTableView1O_VLEZ_NEKONTROLIRAN
              end
              item
                Format = '###,##0.00'
                Kind = skSum
                Position = spFooter
                Column = cxGrid1DBTableView1O_VLEZ
              end>
            DataController.Summary.FooterSummaryItems = <
              item
                Kind = skCount
              end
              item
                Format = '###,##0.00'
                Kind = skSum
              end
              item
                Format = '###,##0.00'
                Kind = skSum
              end
              item
                Format = '###,##0.00'
                Kind = skSum
              end
              item
                Format = '###,##0.00'
                Kind = skSum
              end
              item
                Format = '###,##0.00'
                Kind = skSum
              end
              item
                Format = '###,##0.00'
                Kind = skSum
              end
              item
                Column = cxGrid1DBTableView1ARTIKAL_NAZIV
              end
              item
                Kind = skCount
                Column = cxGrid1DBTableView1O_ART_SIF
              end
              item
                Format = '###,##0.00'
                Kind = skSum
                Column = cxGrid1DBTableView1O_VLEZ
              end
              item
                Format = '###,##0.00'
                Kind = skSum
                Column = cxGrid1DBTableView1O_VLEZ_NEKONTROLIRAN
              end
              item
                Format = '###,##0.00'
                Kind = skSum
                Column = cxGrid1DBTableView1O_IZLEZ
              end
              item
                Format = '###,##0.00'
                Kind = skSum
                Column = cxGrid1DBTableView1O_LAGER
              end
              item
                Format = '###,##0.00'
                Kind = skSum
              end
              item
                Format = '###,##0.00'
                Kind = skSum
              end
              item
                Format = '###,##0.00'
                Kind = skSum
              end
              item
                Format = '###,##0.00'
                Kind = skSum
              end>
            DataController.Summary.SummaryGroups = <>
            Filtering.ColumnFilteredItemsList = True
            FilterRow.InfoText = #1050#1083#1080#1082#1085#1080' '#1086#1074#1076#1077' '#1079#1072' '#1076#1072' '#1076#1077#1092#1080#1085#1080#1088#1072#1096' '#1092#1080#1083#1090#1077#1088
            FilterRow.Visible = True
            FilterRow.ApplyChanges = fracImmediately
            OptionsBehavior.CellHints = True
            OptionsBehavior.IncSearch = True
            OptionsCustomize.ColumnsQuickCustomization = True
            OptionsData.Deleting = False
            OptionsData.Editing = False
            OptionsData.Inserting = False
            OptionsView.Footer = True
            OptionsView.GroupFooters = gfVisibleWhenExpanded
            OptionsView.HeaderAutoHeight = True
            object cxGrid1DBTableView1O_ART_VID: TcxGridDBColumn
              DataBinding.FieldName = 'O_ART_VID'
              Width = 50
            end
            object cxGrid1DBTableView1O_ART_SIF: TcxGridDBColumn
              DataBinding.FieldName = 'O_ART_SIF'
              Width = 60
            end
            object cxGrid1DBTableView1ARTIKAL_NAZIV: TcxGridDBColumn
              DataBinding.FieldName = 'ARTIKAL_NAZIV'
              Width = 400
            end
            object cxGrid1DBTableView1O_VLEZ: TcxGridDBColumn
              DataBinding.FieldName = 'O_VLEZ'
              Width = 90
            end
            object cxGrid1DBTableView1O_VLEZ_NEKONTROLIRAN: TcxGridDBColumn
              DataBinding.FieldName = 'O_VLEZ_NEKONTROLIRAN'
              Width = 90
            end
            object cxGrid1DBTableView1O_IZLEZ: TcxGridDBColumn
              DataBinding.FieldName = 'O_IZLEZ'
              Width = 90
            end
            object cxGrid1DBTableView1O_LAGER: TcxGridDBColumn
              DataBinding.FieldName = 'O_LAGER'
              Width = 90
            end
          end
          object cxGrid1DBTableView2: TcxGridDBTableView
            OnKeyPress = cxGrid1DBTableView1KeyPress
            Navigator.Buttons.CustomButtons = <>
            DataController.DataSource = dm.dsLagerArtikliSerija
            DataController.Summary.DefaultGroupSummaryItems = <
              item
                Kind = skCount
                Position = spFooter
                Column = cxGrid1DBTableView2O_ART_SIF
              end
              item
                Format = '###,##0.00'
                Kind = skSum
                Position = spFooter
                Column = cxGrid1DBTableView2O_VLEZ
              end
              item
                Format = '###,##0.00'
                Kind = skSum
                Position = spFooter
                Column = cxGrid1DBTableView2O_VLEZ_NEKONTROLIRAN
              end
              item
                Format = '###,##0.00'
                Kind = skSum
                Position = spFooter
                Column = cxGrid1DBTableView2O_IZLEZ
              end
              item
                Format = '###,##0.00'
                Kind = skSum
                Position = spFooter
                Column = cxGrid1DBTableView2O_LAGER
              end
              item
                Format = '###,##0.00'
                Kind = skSum
                Position = spFooter
              end
              item
                Format = '###,##0.00'
                Kind = skSum
                Position = spFooter
              end
              item
                Format = '###,##0.00'
                Kind = skSum
                Position = spFooter
              end
              item
                Format = '###,##0.00'
                Kind = skSum
                Position = spFooter
              end>
            DataController.Summary.FooterSummaryItems = <
              item
                Kind = skCount
                Column = cxGrid1DBTableView2O_ART_SIF
              end
              item
                Format = '###,##0.00'
                Kind = skSum
                Column = cxGrid1DBTableView2O_VLEZ
              end
              item
                Format = '###,##0.00'
                Kind = skSum
                Column = cxGrid1DBTableView2O_VLEZ_NEKONTROLIRAN
              end
              item
                Format = '###,##0.00'
                Kind = skSum
                Column = cxGrid1DBTableView2O_IZLEZ
              end
              item
                Format = '###,##0.00'
                Kind = skSum
                Column = cxGrid1DBTableView2O_LAGER
              end
              item
                Format = '###,##0.00'
                Kind = skSum
              end
              item
                Format = '###,##0.00'
                Kind = skSum
              end
              item
                Format = '###,##0.00'
                Kind = skSum
              end
              item
                Format = '###,##0.00'
                Kind = skSum
              end>
            DataController.Summary.SummaryGroups = <>
            Filtering.ColumnFilteredItemsList = True
            FilterRow.InfoText = #1050#1083#1080#1082#1085#1080' '#1086#1074#1076#1077' '#1079#1072' '#1076#1072' '#1076#1077#1092#1080#1085#1080#1088#1072#1096' '#1092#1080#1083#1090#1077#1088
            FilterRow.Visible = True
            FilterRow.ApplyChanges = fracImmediately
            OptionsBehavior.CellHints = True
            OptionsBehavior.IncSearch = True
            OptionsCustomize.ColumnsQuickCustomization = True
            OptionsData.Deleting = False
            OptionsData.Editing = False
            OptionsData.Inserting = False
            OptionsView.Footer = True
            OptionsView.GroupFooters = gfVisibleWhenExpanded
            OptionsView.HeaderAutoHeight = True
            object cxGrid1DBTableView2O_LOT_NO: TcxGridDBColumn
              DataBinding.FieldName = 'O_LOT_NO'
            end
            object cxGrid1DBTableView2O_REF_ID: TcxGridDBColumn
              DataBinding.FieldName = 'O_REF_ID'
              Visible = False
            end
            object cxGrid1DBTableView2O_ART_VID: TcxGridDBColumn
              DataBinding.FieldName = 'O_ART_VID'
              Width = 50
            end
            object cxGrid1DBTableView2O_ART_SIF: TcxGridDBColumn
              DataBinding.FieldName = 'O_ART_SIF'
              Width = 60
            end
            object cxGrid1DBTableView2ARTIKAL_NAZIV: TcxGridDBColumn
              DataBinding.FieldName = 'ARTIKAL_NAZIV'
              Width = 400
            end
            object cxGrid1DBTableView2O_VLEZ: TcxGridDBColumn
              DataBinding.FieldName = 'O_VLEZ'
              Width = 90
            end
            object cxGrid1DBTableView2O_VLEZ_NEKONTROLIRAN: TcxGridDBColumn
              DataBinding.FieldName = 'O_VLEZ_NEKONTROLIRAN'
              Width = 90
            end
            object cxGrid1DBTableView2O_IZLEZ: TcxGridDBColumn
              DataBinding.FieldName = 'O_IZLEZ'
              Width = 90
            end
            object cxGrid1DBTableView2O_LAGER: TcxGridDBColumn
              DataBinding.FieldName = 'O_LAGER'
              Width = 90
            end
          end
          object cxGrid1LevelFIFO: TcxGridLevel
            Caption = #1042#1082#1091#1087#1085#1086
            GridView = cxGrid1DBTableView1
          end
          object cxGrid1LevelSerija: TcxGridLevel
            Caption = #1055#1086' '#1089#1077#1088#1080#1112#1072
            GridView = cxGrid1DBTableView2
          end
        end
      end
      object cxTabGrafik: TcxTabSheet
        Caption = #1043#1088#1072#1092#1080#1095#1082#1080
        ImageIndex = 1
        object cxGrid2: TcxGrid
          Left = 0
          Top = 0
          Width = 932
          Height = 450
          Align = alClient
          TabOrder = 0
          object cxGrid2DBChartView1: TcxGridDBChartView
            Categories.DataBinding.FieldName = 'DATUM_DOKUMENT'
            Categories.SortOrder = soAscending
            DataController.Options = [dcoAnsiSort, dcoAssignGroupingValues, dcoAssignMasterDetailKeys, dcoSaveExpanding, dcoImmediatePost]
            DiagramArea.Active = True
            DiagramArea.Values.CaptionPosition = ldvcpAbove
            DiagramBar.Values.CaptionPosition = cdvcpOutsideEnd
            DiagramColumn.Values.CaptionPosition = cdvcpOutsideEnd
            DiagramLine.Values.CaptionPosition = ldvcpAbove
            DiagramPie.Values.CaptionPosition = pdvcpOutsideEnd
            DiagramStackedArea.Values.CaptionPosition = ldvcpAbove
            DiagramStackedBar.Values.CaptionPosition = cdvcpOutsideEnd
            DiagramStackedColumn.Values.CaptionPosition = cdvcpOutsideEnd
            Legend.Alignment = cpaCenter
            Legend.KeyBorder = lbSingle
            Legend.Position = cppBottom
            OptionsView.CategoriesPerPage = 20
            ToolBox.CustomizeButton = True
            ToolBox.DiagramSelector = True
            OnGetValueHint = cxGrid2DBChartView1GetValueHint
          end
          object cxGrid2Level1: TcxGridLevel
            GridView = cxGrid2DBChartView1
          end
        end
      end
    end
  end
  object cxSplitter1: TcxSplitter
    Left = 0
    Top = 177
    Width = 934
    Height = 8
    HotZoneClassName = 'TcxSimpleStyle'
    AlignSplitter = salTop
    Control = dPanel
  end
  object cxGridPopupMenu1: TcxGridPopupMenu
    Grid = cxGrid1
    PopupMenus = <>
    Left = 528
    Top = 392
  end
  object PopupMenu1: TPopupMenu
    Left = 480
    Top = 488
    object N1: TMenuItem
      Caption = #1057#1085#1080#1084#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      ShortCut = 49235
      OnClick = SaveToIniFileExecute
    end
    object Excel1: TMenuItem
      Caption = #1057#1085#1080#1084#1080' '#1075#1086' '#1074#1086' Excel'
      ShortCut = 49221
    end
  end
  object dxBarManager1: TdxBarManager
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Segoe UI'
    Font.Style = []
    Categories.Strings = (
      'Default')
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    ImageOptions.Images = dmRes.cxSmallImages
    ImageOptions.LargeImages = dmRes.cxLargeImages
    PopupMenuLinks = <>
    UseSystemFont = True
    Left = 560
    Top = 464
    DockControlHeights = (
      0
      0
      0
      0)
    object dxBarManager1Bar2: TdxBar
      Caption = #1040#1082#1094#1080#1080
      CaptionButtons = <>
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 789
      FloatTop = 120
      FloatClientWidth = 51
      FloatClientHeight = 120
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton24'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton5'
        end
        item
          Visible = True
          ItemName = 'dxBarSubItem1'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar3: TdxBar
      Caption = #1048#1079#1083#1077#1079
      CaptionButtons = <>
      DockedLeft = 245
      DockedTop = 0
      FloatLeft = 718
      FloatTop = 62
      FloatClientWidth = 51
      FloatClientHeight = 80
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton7'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton8'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar4: TdxBar
      Caption = #1055#1077#1095#1072#1090#1077#1114#1077
      CaptionButtons = <>
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 676
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton12'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton15'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton9'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton13'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar5: TdxBar
      Caption = 'Custom 1'
      CaptionButtons = <>
      DockedLeft = 355
      DockedTop = 0
      FloatLeft = 968
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton27'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarLargeButton28'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarLargeButton2: TdxBarLargeButton
      Action = aAzuriraj
      Caption = #1055#1088#1086#1084#1077#1085#1080
      Category = 0
    end
    object dxBarLargeButton3: TdxBarLargeButton
      Action = aBrisi
      Category = 0
    end
    object dxBarLargeButton4: TdxBarLargeButton
      Action = aSnimiIzgled
      Category = 0
      ScreenTip = tipSnimiIzgled
    end
    object dxBarLargeButton5: TdxBarLargeButton
      Action = aZacuvajExcel
      Category = 0
      ScreenTip = tipZacuvajExcel
    end
    object dxBarLargeButton6: TdxBarLargeButton
      Action = aPecatiTabela
      Category = 0
    end
    object dxBarLargeButton7: TdxBarLargeButton
      Action = aHelp
      Category = 0
    end
    object dxBarLargeButton8: TdxBarLargeButton
      Action = aIzlez
      Category = 0
    end
    object dxBarLargeButton1: TdxBarLargeButton
      Action = aNov
      Caption = #1053#1086#1074#1080
      Category = 0
    end
    object dxBarLargeButton9: TdxBarLargeButton
      Action = aSnimiPecatenje
      Category = 0
      ScreenTip = tipSnimiKonf
    end
    object dxBarLargeButton11: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object ribbonFilterReset: TdxBarLargeButton
      Caption = #1056#1077#1089#1077#1090#1080#1088#1072#1112' '#1092#1080#1083#1090#1077#1088
      Category = 0
      Hint = #1056#1077#1089#1077#1090#1080#1088#1072#1112' '#1092#1080#1083#1090#1077#1088
      Visible = ivAlways
    end
    object dxBarLargeButton12: TdxBarLargeButton
      Action = aPodesuvanjePecatenje
      Category = 0
      ScreenTip = tipDizajnReport
    end
    object dxBarLargeButton13: TdxBarLargeButton
      Action = aBrisiPodesuvanjePecatenje
      Category = 0
      ScreenTip = tipBrisiKonf
    end
    object dxBarLargeButton14: TdxBarLargeButton
      Caption = #1048#1079#1073#1088#1080#1096#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      Category = 0
      Visible = ivAlways
      LargeImageIndex = 40
    end
    object dxBarLargeButton15: TdxBarLargeButton
      Action = aPageSetup
      Category = 0
      ScreenTip = tipSetiranjeStrana
    end
    object dxBarLargeButton16: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object dxBarLargeButton17: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object cxBarEditItem1: TcxBarEditItem
      Caption = 'New Item'
      Category = 0
      Hint = 'New Item'
      Visible = ivAlways
      PropertiesClassName = 'TcxCheckBoxProperties'
    end
    object dxBarSubItem1: TdxBarSubItem
      Caption = #1055#1077#1095#1072#1090#1077#1114#1077
      Category = 0
      Visible = ivAlways
      ImageIndex = 30
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton6'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarLargeButton20'
        end>
      ItemOptions.ShowDescriptions = False
    end
    object dxBarLargeButton10: TdxBarLargeButton
      Caption = #1054#1090#1074#1086#1088#1080' '#1085#1072#1083#1086#1075
      Category = 0
      Hint = #1054#1090#1074#1086#1088#1080' '#1075#1086' '#1089#1077#1083#1077#1082#1090#1080#1088#1072#1085#1080#1086#1090' '#1085#1072#1083#1086#1075' '#1086#1076' '#1090#1072#1073#1077#1083#1072#1090#1072
      Visible = ivAlways
      LargeImageIndex = 35
    end
    object dxBarLargeButton18: TdxBarLargeButton
      Caption = #1055#1088#1072#1090#1080' '#1082#1072#1088#1090#1080#1094#1072' '#1087#1086' email'
      Category = 0
      Hint = #1055#1088#1080#1082#1072#1078#1080' '#1086#1087#1086#1084#1077#1085#1072' '#1079#1072' '#1086#1076#1073#1088#1072#1085' '#1087#1072#1088#1090#1085#1077#1088
      Visible = ivAlways
      LargeImageIndex = 45
      ShortCut = 122
      OnClick = aKarticaMailExecute
    end
    object dxBarLargeButton19: TdxBarLargeButton
      Action = aPecatiKartica
      Category = 0
    end
    object dxBarSubItem2: TdxBarSubItem
      Caption = 'New SubItem'
      Category = 0
      Visible = ivAlways
      ItemLinks = <>
    end
    object dxBarLargeButton20: TdxBarLargeButton
      Action = aPecatiGrafik
      Category = 0
    end
    object dxBarLargeButton21: TdxBarLargeButton
      Caption = #1055#1088#1072#1090#1080' '#1082#1072#1088#1090#1080#1094#1072' '#1087#1086' email'
      Category = 0
      Hint = #1055#1088#1080#1082#1072#1078#1080' '#1086#1087#1086#1084#1077#1085#1072' '#1079#1072' '#1086#1076#1073#1088#1072#1085' '#1087#1072#1088#1090#1085#1077#1088
      Visible = ivAlways
      LargeImageIndex = 45
      ShortCut = 122
      OnClick = aKarticaMailExecute
    end
    object dxBarLargeButton22: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object dxBarLargeButton23: TdxBarLargeButton
      Caption = 'Client Dashboard'
      Category = 0
      Visible = ivAlways
      LargeImageIndex = 64
    end
    object dxBarLargeButton24: TdxBarLargeButton
      Action = aSpustiSoberi
      Category = 0
    end
    object dxBarLargeButton25: TdxBarLargeButton
      Caption = #1052#1072#1090#1077#1088#1080#1112#1072#1083#1085#1086' '#1092#1072#1082#1090#1091#1088#1072
      Category = 0
      Visible = ivAlways
      LargeImageIndex = 19
    end
    object dxBarLargeButton26: TdxBarLargeButton
      Caption = #1055#1077#1095#1072#1090#1080' '#1089#1080#1085#1090#1077#1090#1080#1095#1082#1072' '#1082#1072#1088#1090#1080#1094#1072' '#1079#1072' '#1087#1072#1088#1090#1085#1077#1088
      Category = 0
      Visible = ivAlways
      LargeImageIndex = 30
      ShortCut = 122
      OnClick = aPecatiSintetickaKarticaExecute
    end
    object dxBarLargeButton27: TdxBarLargeButton
      Action = aSnimiIzgled
      Category = 0
    end
    object dxBarLargeButton28: TdxBarLargeButton
      Action = aBrisiIzgled
      Category = 0
    end
    object dxBarLargeButton29: TdxBarLargeButton
      Action = aMagacin
      Category = 0
    end
  end
  object ActionList1: TActionList
    Images = dmRes.cxSmallImages
    Left = 272
    Top = 480
    object aNov: TAction
      Caption = #1053#1086#1074
      ImageIndex = 10
      ShortCut = 116
      OnExecute = aNovExecute
    end
    object aAzuriraj: TAction
      Caption = #1040#1078#1091#1088#1080#1088#1072#1112
      ImageIndex = 12
      ShortCut = 117
      OnExecute = aAzurirajExecute
    end
    object aBrisi: TAction
      Caption = #1041#1088#1080#1096#1080
      ImageIndex = 11
      ShortCut = 119
      OnExecute = aBrisiExecute
    end
    object aSnimiIzgled: TAction
      Caption = #1057#1085#1080#1084#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      ImageIndex = 9
      ShortCut = 24659
      OnExecute = aSnimiIzgledExecute
    end
    object aZacuvajExcel: TAction
      Caption = #1047#1072#1095#1091#1074#1072#1112' '#1074#1086' Excel'
      ImageIndex = 9
      ShortCut = 24645
      OnExecute = aZacuvajExcelExecute
    end
    object aPecatiTabela: TAction
      Caption = #1055#1077#1095#1072#1090#1080' '#1090#1072#1073#1077#1083#1072
      ImageIndex = 30
      OnExecute = aPecatiTabelaExecute
    end
    object aHelp: TAction
      Caption = #1055#1086#1084#1086#1096
      ImageIndex = 24
    end
    object aIzlez: TAction
      Caption = #1048#1079#1083#1077#1079
      ImageIndex = 25
      OnExecute = aIzlezExecute
    end
    object aZapisi: TAction
      Caption = #1047#1072#1087#1080#1096#1080
      ImageIndex = 7
      OnExecute = aZapisiExecute
    end
    object aOtkazi: TAction
      Caption = #1054#1090#1082#1072#1078#1080
      ImageIndex = 21
      ShortCut = 27
      OnExecute = aOtkaziExecute
    end
    object aRefresh: TAction
      Caption = 'aRefresh'
      ImageIndex = 18
      ShortCut = 118
      OnExecute = aRefreshExecute
    end
    object aSnimiPecatenje: TAction
      Caption = #1057#1085#1080#1084#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      ImageIndex = 39
      OnExecute = aSnimiPecatenjeExecute
    end
    object aPodesuvanjePecatenje: TAction
      Caption = #1044#1080#1079#1072#1112#1085' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090
      ImageIndex = 38
      OnExecute = aPodesuvanjePecatenjeExecute
    end
    object aBrisiPodesuvanjePecatenje: TAction
      Caption = #1041#1088#1080#1096#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      ImageIndex = 37
      OnExecute = aBrisiPodesuvanjePecatenjeExecute
    end
    object aPageSetup: TAction
      Caption = #1057#1077#1090#1080#1088#1072#1114#1077' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072
      ImageIndex = 38
      OnExecute = aPageSetupExecute
    end
    object aFormConfig: TAction
      Caption = 'aFormConfig'
      ShortCut = 24699
      OnExecute = aFormConfigExecute
    end
    object aPrikazi: TAction
      Caption = #1055#1088#1080#1082#1072#1078#1080
      ImageIndex = 19
      ShortCut = 120
      OnExecute = aPrikaziExecute
    end
    object aDizajn: TAction
      Caption = 'aDizajn'
      ShortCut = 24697
      OnExecute = aDizajnExecute
    end
    object aPecatiKartica: TAction
      Caption = #1055#1077#1095#1072#1090#1080' '#1082#1072#1088#1090#1080#1094#1072' '#1079#1072' '#1087#1072#1088#1090#1085#1077#1088
      Hint = #1055#1088#1080#1082#1072#1078#1080' '#1082#1072#1088#1090#1080#1094#1072' '#1085#1072' '#1086#1076#1073#1088#1072#1085' '#1087#1072#1088#1090#1085#1077#1088' '#1079#1072' '#1087#1077#1095#1072#1090#1077#1114#1077
      ImageIndex = 30
      ShortCut = 121
      OnExecute = aPecatiKarticaExecute
    end
    object aPecatiGrafik: TAction
      Caption = #1055#1077#1095#1072#1090#1080' '#1075#1088#1072#1092#1080#1082
      ImageIndex = 30
      OnExecute = aPecatiGrafikExecute
    end
    object aSpustiSoberi: TAction
      Caption = #1057#1087#1091#1096#1090#1080'/'#1057#1086#1073#1077#1088#1080' '#1058#1072#1073#1077#1083#1072
      ImageIndex = 65
      OnExecute = aSpustiSoberiExecute
    end
    object aDizajnSint: TAction
      Caption = 'aDizajnSint'
      ShortCut = 24698
      OnExecute = aDizajnSintExecute
    end
    object aBrisiIzgled: TAction
      Caption = #1041#1088#1080#1096#1080' '#1075#1086' '#1079#1072#1095#1091#1074#1072#1085#1080#1086#1090' '#1080#1079#1075#1083#1077#1076
      ImageIndex = 56
      OnExecute = aBrisiIzgledExecute
    end
    object aMagacin: TAction
      Caption = #1052#1072#1075#1072#1094#1080#1085
      Hint = #1055#1088#1086#1084#1077#1085#1080' '#1075#1086' '#1084#1086#1084#1077#1085#1090#1072#1083#1085#1080#1086#1090' '#1084#1072#1075#1072#1094#1080#1085
      ImageIndex = 53
      ShortCut = 113
      OnExecute = aMagacinExecute
    end
    object aHideFields: TAction
      Caption = 'aHideFields'
      OnExecute = aHideFieldsExecute
    end
  end
  object dxComponentPrinter1: TdxComponentPrinter
    CurrentLink = dxComponentPrinter1Link1
    Version = 0
    Left = 184
    Top = 544
    object dxComponentPrinter1Link1: TdxGridReportLink
      Active = True
      Component = cxGrid1
      PageNumberFormat = pnfNumeral
      PrinterPage.DMPaper = 9
      PrinterPage.Footer = 6350
      PrinterPage.GrayShading = True
      PrinterPage.Header = 6350
      PrinterPage.Margins.Bottom = 12700
      PrinterPage.Margins.Left = 12700
      PrinterPage.Margins.Right = 12700
      PrinterPage.Margins.Top = 18000
      PrinterPage.Orientation = poLandscape
      PrinterPage.PageFooter.RightTitle.Strings = (
        #1057#1090#1088'.[Page #] '#1086#1076' [Total Pages]')
      PrinterPage.PageSize.X = 210000
      PrinterPage.PageSize.Y = 297000
      PrinterPage.ScaleMode = smFit
      PrinterPage._dxMeasurementUnits_ = 0
      PrinterPage._dxLastMU_ = 2
      ReportDocument.CreationDate = 41656.530567083330000000
      ShrinkToPageWidth = True
      AssignedFormatValues = [fvDate, fvTime, fvPageNumber]
      OptionsFormatting.UseNativeStyles = True
      OptionsOnEveryPage.Caption = False
      OptionsOnEveryPage.FilterBar = False
      OptionsView.Caption = False
      OptionsView.FilterBar = False
      StyleRepository = dmRes.cxStyleRepositoryPrinting
      Styles.StyleSheet = dmRes.dxGridReportLinkStyleSheet
      BuiltInReportLink = True
    end
    object dxComponentPrinter1Link2: TdxGridReportLink
      Active = True
      Component = cxGrid2
      PageNumberFormat = pnfNumeral
      PrinterPage.DMPaper = 9
      PrinterPage.Footer = 6350
      PrinterPage.GrayShading = True
      PrinterPage.Header = 6350
      PrinterPage.Margins.Bottom = 12700
      PrinterPage.Margins.Left = 12700
      PrinterPage.Margins.Right = 12700
      PrinterPage.Margins.Top = 25000
      PrinterPage.Orientation = poLandscape
      PrinterPage.PageSize.X = 210000
      PrinterPage.PageSize.Y = 297000
      PrinterPage.ScaleMode = smFit
      PrinterPage._dxMeasurementUnits_ = 0
      PrinterPage._dxLastMU_ = 2
      ReportDocument.CreationDate = 41656.530567118050000000
      ShrinkToPageWidth = True
      AssignedFormatValues = [fvDate, fvTime, fvPageNumber]
      OptionsFormatting.UseNativeStyles = True
      StyleRepository = dmRes.cxStyleRepositoryPrinting
      Styles.StyleSheet = dmRes.dxGridReportLinkStyleSheet
      BuiltInReportLink = True
    end
  end
  object dxBarScreenTipRepository1: TdxBarScreenTipRepository
    Left = 88
    Top = 416
    object tipSnimiIzgled: TdxBarScreenTip
      Header.Text = #1057#1085#1080#1084#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      Description.Text = #1047#1072#1095#1091#1074#1072#1112' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090' '#1085#1072' '#1092#1086#1088#1084#1072#1090#1072' '#1080' '#1085#1072' '#1090#1072#1073#1077#1083#1072#1090#1072
    end
    object tipZacuvajExcel: TdxBarScreenTip
      Header.Text = #1047#1072#1095#1091#1074#1072#1112' '#1074#1086' Excel'
      Description.Text = #1057#1085#1080#1084#1080' '#1075#1080' '#1087#1086#1076#1072#1090#1086#1094#1080#1090#1077' '#1086#1076' '#1090#1072#1073#1077#1083#1072#1090#1072' '#1074#1086' Excel '#1092#1086#1088#1084#1072#1090
    end
    object tipDizajnReport: TdxBarScreenTip
      Header.Text = #1044#1080#1079#1072#1112#1085' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090
      Description.Text = #1055#1086#1076#1077#1089#1091#1074#1072#1114#1072' '#1085#1072' '#1080#1079#1075#1083#1077#1076#1086#1090' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090' '#1085#1072' '#1090#1072#1073#1077#1083#1072#1090#1072
    end
    object tipSetiranjeStrana: TdxBarScreenTip
      Header.Text = #1057#1077#1090#1080#1088#1072#1114#1077' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072
      Description.Text = #1055#1086#1076#1077#1089#1091#1074#1072#1114#1077' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072' '#1085#1072' '#1082#1086#1112#1072' '#1116#1077' '#1089#1077' '#1087#1077#1095#1072#1090#1080' '#1090#1072#1073#1077#1083#1072#1090#1072
    end
    object tipSnimiKonf: TdxBarScreenTip
      Header.Text = #1057#1085#1080#1084#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      Description.Text = #1047#1072#1095#1091#1074#1072#1112' '#1075#1080' '#1087#1086#1076#1077#1089#1091#1074#1072#1114#1072#1090#1072' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090' '#1080' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072' '#1079#1072' '#1087#1077#1095#1072#1090#1077#1114#1077
    end
    object tipBrisiKonf: TdxBarScreenTip
      Header.Text = #1041#1088#1080#1096#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      Description.Text = #1048#1079#1073#1088#1080#1096#1080' '#1075#1080' '#1087#1086#1076#1077#1089#1091#1074#1072#1114#1072#1090#1072' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090' '#1080' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072' '#1079#1072' '#1087#1077#1095#1072#1090#1077#1114#1077
    end
  end
  object cxPivotGridChartConnection1: TcxPivotGridChartConnection
    Left = 88
    Top = 464
  end
  object tblReMagacin: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE MTR_RE_MAGACIN'
      'SET '
      '    ID = :ID,'
      '    ADRESA = :ADRESA,'
      '    MESTO = :MESTO,'
      '    ODGOVOREN = :ODGOVOREN,'
      '    KONTO = :KONTO,'
      '    KONTO_T1 = :KONTO_T1,'
      '    KONTO_T2 = :KONTO_T2,'
      '    KONTO_P = :KONTO_P,'
      '    OSLOBODUVANJE = :OSLOBODUVANJE,'
      '    ZADOLZUVANJE = :ZADOLZUVANJE,'
      '    GODINA = :GODINA,'
      '    AVTOMATSKA_KONTROLA = :AVTOMATSKA_KONTROLA'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    MTR_RE_MAGACIN'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO MTR_RE_MAGACIN('
      '    ID,'
      '    ADRESA,'
      '    MESTO,'
      '    ODGOVOREN,'
      '    KONTO,'
      '    KONTO_T1,'
      '    KONTO_T2,'
      '    KONTO_P,'
      '    OSLOBODUVANJE,'
      '    ZADOLZUVANJE,'
      '    GODINA,'
      '    AVTOMATSKA_KONTROLA'
      ')'
      'VALUES('
      '    :ID,'
      '    :ADRESA,'
      '    :MESTO,'
      '    :ODGOVOREN,'
      '    :KONTO,'
      '    :KONTO_T1,'
      '    :KONTO_T2,'
      '    :KONTO_P,'
      '    :OSLOBODUVANJE,'
      '    :ZADOLZUVANJE,'
      '    :GODINA,'
      '    :AVTOMATSKA_KONTROLA'
      ')')
    RefreshSQL.Strings = (
      'select distinct RM.ID,'
      '                R.NAZIV,'
      '                R.TIP_PARTNER,'
      '                R.PARTNER,'
      '                P.NAZIV as NAZIV_PARTNER,'
      '                R.KOREN,'
      '                RM.ADRESA,'
      '                RM.MESTO,'
      '                M.NAZIV as MESTO_NAZIV,'
      '                RM.ODGOVOREN,'
      '                RM.KONTO,'
      '                RM.KONTO_T1,'
      '                RM.KONTO_T2,'
      '                RM.KONTO_P,'
      '                RM.OSLOBODUVANJE,'
      '                RM.ZADOLZUVANJE,'
      '                RM.GODINA,'
      '                RM.AVTOMATSKA_KONTROLA,'
      '                KP1.NAZIV as NAZIV_KONTO,'
      '                KP2.NAZIV as NAZIV_KONTO_T1,'
      '                KP3.NAZIV as NAZIV_KONTO_T2,'
      '                KP4.NAZIV as NAZIV_KONTO_P'
      'from MTR_RE_MAGACIN RM'
      'left join SYS_USER_RE_APP UR on UR.RE = RM.ID'
      'join MAT_RE R on R.ID = RM.ID'
      
        'join MAT_PARTNER P on P.TIP_PARTNER = R.TIP_PARTNER and P.ID = R' +
        '.PARTNER'
      'left join MAT_MESTO M on M.ID = RM.MESTO'
      
        'left join VIEW_MAT_KONTEN_PLAN KP1 on KP1.SIFRA = RM.KONTO and K' +
        'P1.GODINA = RM.GODINA'
      
        'left join VIEW_MAT_KONTEN_PLAN KP2 on KP2.SIFRA = RM.KONTO_T1 an' +
        'd KP2.GODINA = RM.GODINA'
      
        'left join VIEW_MAT_KONTEN_PLAN KP3 on KP3.SIFRA = RM.KONTO_T2 an' +
        'd KP3.GODINA = RM.GODINA'
      
        'left join VIEW_MAT_KONTEN_PLAN KP4 on KP4.SIFRA = RM.KONTO_P and' +
        ' KP4.GODINA = RM.GODINA'
      'where(  R.M = 1'
      
        '      and ((UR.USERNAME = :user) or (cast(:user as varchar(31)) ' +
        '= '#39'SYSDBA'#39'))'
      
        '      and ((UR.APP = :APP) or (cast(:user as varchar(31)) = '#39'SYS' +
        'DBA'#39'))'
      '     ) and (     RM.ID = :OLD_ID'
      '     )'
      '       ')
    SelectSQL.Strings = (
      'select distinct RM.ID,'
      '                R.NAZIV,'
      '                R.TIP_PARTNER,'
      '                R.PARTNER,'
      '                P.NAZIV as NAZIV_PARTNER,'
      '                R.KOREN,'
      '                RM.ADRESA,'
      '                RM.MESTO,'
      '                M.NAZIV as MESTO_NAZIV,'
      '                RM.ODGOVOREN,'
      '                RM.KONTO,'
      '                RM.KONTO_T1,'
      '                RM.KONTO_T2,'
      '                RM.KONTO_P,'
      '                RM.OSLOBODUVANJE,'
      '                RM.ZADOLZUVANJE,'
      '                RM.GODINA,'
      '                RM.AVTOMATSKA_KONTROLA,'
      '                KP1.NAZIV as NAZIV_KONTO,'
      '                KP2.NAZIV as NAZIV_KONTO_T1,'
      '                KP3.NAZIV as NAZIV_KONTO_T2,'
      '                KP4.NAZIV as NAZIV_KONTO_P'
      'from MTR_RE_MAGACIN RM'
      'left join SYS_USER_RE_APP UR on UR.RE = RM.ID'
      'join MAT_RE R on R.ID = RM.ID'
      
        'join MAT_PARTNER P on P.TIP_PARTNER = R.TIP_PARTNER and P.ID = R' +
        '.PARTNER'
      'left join MAT_MESTO M on M.ID = RM.MESTO'
      
        'left join VIEW_MAT_KONTEN_PLAN KP1 on KP1.SIFRA = RM.KONTO and K' +
        'P1.GODINA = RM.GODINA'
      
        'left join VIEW_MAT_KONTEN_PLAN KP2 on KP2.SIFRA = RM.KONTO_T1 an' +
        'd KP2.GODINA = RM.GODINA'
      
        'left join VIEW_MAT_KONTEN_PLAN KP3 on KP3.SIFRA = RM.KONTO_T2 an' +
        'd KP3.GODINA = RM.GODINA'
      
        'left join VIEW_MAT_KONTEN_PLAN KP4 on KP4.SIFRA = RM.KONTO_P and' +
        ' KP4.GODINA = RM.GODINA'
      'where R.M = 1'
      
        '      and ((UR.USERNAME = :user) or (cast(:user as varchar(31)) ' +
        '= '#39'SYSDBA'#39'))'
      
        '      and ((UR.APP = :APP) or (cast(:user as varchar(31)) = '#39'SYS' +
        'DBA'#39'))   ')
    BeforeOpen = tblReMagacinBeforeOpen
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 792
    Top = 376
    poSQLINT64ToBCD = True
    object tblReMagacinID: TFIBIntegerField
      DisplayLabel = #1064#1080#1092'.'
      FieldName = 'ID'
    end
    object tblReMagacinGODINA: TFIBIntegerField
      DisplayLabel = #1043#1086#1076#1080#1085#1072
      FieldName = 'GODINA'
    end
    object tblReMagacinADRESA: TFIBStringField
      DisplayLabel = #1040#1076#1088#1077#1089#1072
      FieldName = 'ADRESA'
      Size = 250
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblReMagacinMESTO: TFIBIntegerField
      DisplayLabel = #1052#1077#1089#1090#1086' '#1096#1080#1092'.'
      FieldName = 'MESTO'
    end
    object tblReMagacinMESTO_NAZIV: TFIBStringField
      DisplayLabel = #1052#1077#1089#1090#1086
      FieldName = 'MESTO_NAZIV'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblReMagacinODGOVOREN: TFIBStringField
      DisplayLabel = #1054#1076#1075#1086#1074#1086#1088#1077#1085
      FieldName = 'ODGOVOREN'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblReMagacinKONTO: TFIBStringField
      DisplayLabel = #1050#1086#1085#1090#1086
      FieldName = 'KONTO'
      Size = 15
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblReMagacinKONTO_T1: TFIBStringField
      DisplayLabel = #1050#1086#1085#1090#1086' '#1058'1'
      FieldName = 'KONTO_T1'
      Size = 15
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblReMagacinKONTO_T2: TFIBStringField
      DisplayLabel = #1050#1086#1085#1090#1086' '#1058'2'
      FieldName = 'KONTO_T2'
      Size = 15
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblReMagacinKONTO_P: TFIBStringField
      DisplayLabel = #1050#1086#1085#1090#1086' '#1055'.'
      FieldName = 'KONTO_P'
      Size = 15
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblReMagacinOSLOBODUVANJE: TFIBSmallIntField
      DisplayLabel = #1054#1089#1083#1086#1073#1086#1076#1091#1074#1072#1114#1077
      FieldName = 'OSLOBODUVANJE'
    end
    object tblReMagacinZADOLZUVANJE: TFIBSmallIntField
      DisplayLabel = #1047#1072#1076#1086#1083#1078#1091#1074#1072#1114#1077
      FieldName = 'ZADOLZUVANJE'
    end
    object tblReMagacinNAZIV_KONTO: TFIBStringField
      DisplayLabel = #1053#1072#1079#1080#1074' '#1082#1086#1085#1090#1086
      FieldName = 'NAZIV_KONTO'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblReMagacinNAZIV_KONTO_T1: TFIBStringField
      DisplayLabel = #1053#1072#1079#1080#1074' '#1082#1086#1085#1090#1086' '#1058'1'
      FieldName = 'NAZIV_KONTO_T1'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblReMagacinNAZIV_KONTO_T2: TFIBStringField
      DisplayLabel = #1053#1072#1079#1080#1074' '#1082#1086#1085#1090#1086' '#1058'2'
      FieldName = 'NAZIV_KONTO_T2'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblReMagacinNAZIV_KONTO_P: TFIBStringField
      DisplayLabel = #1053#1072#1079#1080#1074' '#1082#1086#1085#1090#1086' '#1055'.'
      FieldName = 'NAZIV_KONTO_P'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblReMagacinNAZIV: TFIBStringField
      DisplayLabel = #1053#1072#1079#1080#1074
      FieldName = 'NAZIV'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblReMagacinTIP_PARTNER: TFIBIntegerField
      DisplayLabel = #1058#1055
      FieldName = 'TIP_PARTNER'
    end
    object tblReMagacinPARTNER: TFIBIntegerField
      DisplayLabel = #1055
      FieldName = 'PARTNER'
    end
    object tblReMagacinNAZIV_PARTNER: TFIBStringField
      DisplayLabel = #1053#1072#1079#1080#1074' '#1087#1072#1088#1090#1085#1077#1088
      FieldName = 'NAZIV_PARTNER'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblReMagacinKOREN: TFIBIntegerField
      DisplayLabel = #1050#1086#1088#1077#1085
      FieldName = 'KOREN'
    end
  end
  object dsReMagacin: TDataSource
    AutoEdit = False
    DataSet = tblReMagacin
    Left = 720
    Top = 376
  end
end
