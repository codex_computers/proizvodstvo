inherited frmOperacijaKT: TfrmOperacijaKT
  Caption = #1054#1087#1077#1088#1072#1094#1080#1080' '#1074#1086' '#1050#1086#1085#1090#1088#1086#1083#1085#1072' '#1090#1086#1095#1082#1072
  ExplicitWidth = 749
  ExplicitHeight = 591
  PixelsPerInch = 96
  TextHeight = 13
  inherited lPanel: TPanel
    inherited cxGrid1: TcxGrid
      inherited cxGrid1DBTableView1: TcxGridDBTableView
        OnFocusedRecordChanged = cxGrid1DBTableView1FocusedRecordChanged
        DataController.DataSource = dm.dsKTOperacija
        object cxGrid1DBTableView1ID: TcxGridDBColumn
          DataBinding.FieldName = 'ID'
          Visible = False
          Width = 47
        end
        object cxGrid1DBTableView1ID_KT: TcxGridDBColumn
          Caption = #1064#1080#1092#1088#1072' '#1085#1072' '#1050#1058
          DataBinding.FieldName = 'ID_KT'
        end
        object cxGrid1DBTableView1NAZIV_KT: TcxGridDBColumn
          Caption = #1050#1086#1085#1090#1088#1086#1083#1085#1072' '#1090#1086#1095#1082#1072
          DataBinding.FieldName = 'NAZIV_KT'
          Width = 134
        end
        object cxGrid1DBTableView1ID_PR_OPERACIJA: TcxGridDBColumn
          Caption = #1064#1080#1092#1088#1072' '#1085#1072' '#1086#1087#1077#1088#1072#1094#1080#1112#1072
          DataBinding.FieldName = 'ID_PR_OPERACIJA'
        end
        object cxGrid1DBTableView1NAZIV_OPERACIJA: TcxGridDBColumn
          DataBinding.FieldName = 'NAZIV_OPERACIJA'
          Width = 133
        end
        object cxGrid1DBTableView1NAZIV_PR: TcxGridDBColumn
          Caption = #1055#1088#1086#1080#1079#1074#1086#1076#1089#1090#1074#1077#1085' '#1088#1077#1089#1091#1088#1089
          DataBinding.FieldName = 'NAZIV_PR'
          Width = 233
        end
        object cxGrid1DBTableView1AKTIVEN: TcxGridDBColumn
          Caption = #1040#1082#1090#1080#1074#1077#1085
          DataBinding.FieldName = 'AKTIVEN'
        end
      end
    end
  end
  inherited dPanel: TPanel
    inherited Label1: TLabel
      Left = 91
      Width = 43
      Font.Style = []
      ExplicitLeft = 91
      ExplicitWidth = 43
    end
    object Label2: TLabel [1]
      Left = 18
      Top = 49
      Width = 123
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1050#1086#1085#1090#1088#1086#1083#1085#1072' '#1090#1086#1095#1082#1072' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    inherited Sifra: TcxDBTextEdit
      Left = 147
      DataBinding.DataField = 'ID'
      DataBinding.DataSource = dm.dsKTOperacija
      ExplicitLeft = 147
    end
    inherited OtkaziButton: TcxButton
      TabOrder = 7
    end
    inherited ZapisiButton: TcxButton
      TabOrder = 5
    end
    object txtKT: TcxDBTextEdit
      Tag = 1
      Left = 147
      Top = 46
      BeepOnEnter = False
      DataBinding.DataField = 'ID_KT'
      DataBinding.DataSource = dm.dsKTOperacija
      ParentFont = False
      Properties.BeepOnError = True
      Properties.CharCase = ecUpperCase
      Style.Shadow = False
      TabOrder = 1
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 80
    end
    object cxLabel27: TcxLabel
      Left = 77
      Top = 74
      Caption = #1054#1087#1077#1088#1072#1094#1080#1112#1072' :'
      Style.TextColor = clRed
      Properties.Alignment.Horz = taRightJustify
      Transparent = True
      AnchorX = 141
    end
    object txtOperacija: TcxDBTextEdit
      Tag = 1
      Left = 147
      Top = 73
      DataBinding.DataField = 'ID_PR_OPERACIJA'
      DataBinding.DataSource = dm.dsKTOperacija
      ParentFont = False
      Style.BorderStyle = ebsUltraFlat
      Style.LookAndFeel.NativeStyle = False
      Style.LookAndFeel.SkinName = ''
      Style.TextStyle = []
      Style.TransparentBorder = False
      StyleDisabled.LookAndFeel.NativeStyle = False
      StyleDisabled.LookAndFeel.SkinName = ''
      StyleFocused.LookAndFeel.NativeStyle = False
      StyleFocused.LookAndFeel.SkinName = ''
      StyleHot.LookAndFeel.NativeStyle = False
      StyleHot.LookAndFeel.SkinName = ''
      TabOrder = 3
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 80
    end
    object cbOperacija: TcxDBLookupComboBox
      Left = 229
      Top = 73
      Anchors = [akRight, akBottom]
      DataBinding.DataField = 'ID_PR_OPERACIJA'
      DataBinding.DataSource = dm.dsKTOperacija
      Properties.DropDownAutoSize = True
      Properties.DropDownSizeable = True
      Properties.ImmediatePost = True
      Properties.KeyFieldNames = 'ID'
      Properties.ListColumns = <
        item
          Width = 150
          FieldName = 'NAZIV_OPERACIJA'
        end
        item
          Caption = #1055#1088#1086#1080#1079#1074#1086#1076#1089#1090#1074#1077#1085' '#1088#1077#1089#1091#1088#1089
          Width = 150
          FieldName = 'proizvodstven_resurs'
        end>
      Properties.ListSource = dm.dsOperacijaPR
      TabOrder = 4
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 357
    end
    object cbKT: TcxDBLookupComboBox
      Left = 229
      Top = 47
      Anchors = [akRight, akBottom]
      DataBinding.DataField = 'ID_KT'
      DataBinding.DataSource = dm.dsKTOperacija
      Properties.DropDownAutoSize = True
      Properties.DropDownSizeable = True
      Properties.ImmediatePost = True
      Properties.KeyFieldNames = 'ID'
      Properties.ListColumns = <
        item
          FieldName = 'ID'
        end
        item
          FieldName = 'naziv'
        end>
      Properties.ListFieldIndex = 1
      Properties.ListSource = dm.dsKT
      TabOrder = 2
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 357
    end
  end
  inherited dxRibbon1: TdxRibbon
    inherited dxRibbon1Tab1: TdxRibbonTab
      Index = 0
    end
    inherited dxRibbon1Tab2: TdxRibbonTab
      Index = 1
    end
  end
  inherited dxBarManager1: TdxBarManager
    DockControlHeights = (
      0
      0
      0
      0)
    inherited dxBarManager1Bar1: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 69
      FloatClientHeight = 167
    end
    inherited dxBarManager1Bar2: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 149
      FloatClientHeight = 130
    end
    inherited dxBarManager1Bar3: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 59
      FloatClientHeight = 108
    end
    inherited dxBarManager1Bar4: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 141
      FloatClientHeight = 221
    end
    inherited dxBarManager1BarTabela: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 174
      FloatClientHeight = 113
    end
  end
  inherited dxComponentPrinter1: TdxComponentPrinter
    inherited dxComponentPrinter1Link1: TdxGridReportLink
      ReportDocument.CreationDate = 41179.553103738430000000
      AssignedFormatValues = [fvDate, fvTime, fvPageNumber]
      Styles.BandHeader = nil
      Styles.Caption = nil
      Styles.CardCaptionRow = nil
      Styles.CardRowCaption = nil
      Styles.Content = nil
      Styles.ContentEven = nil
      Styles.ContentOdd = nil
      Styles.FilterBar = nil
      Styles.Footer = nil
      Styles.Group = nil
      Styles.Header = nil
      Styles.Preview = nil
      Styles.Selection = nil
      BuiltInReportLink = True
    end
  end
end
