object frmIzberiProizvod: TfrmIzberiProizvod
  Left = 128
  Top = 212
  BorderIcons = [biSystemMenu, biMaximize]
  Caption = #1048#1079#1073#1077#1088#1077#1090#1077' '#1087#1088#1086#1080#1079#1074#1086#1076'/'#1080
  ClientHeight = 661
  ClientWidth = 914
  Color = clBtnFace
  Constraints.MinHeight = 500
  Constraints.MinWidth = 610
  Font.Charset = RUSSIAN_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  ShowHint = True
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object StatusBar1: TdxRibbonStatusBar
    Left = 0
    Top = 638
    Width = 914
    Height = 23
    Panels = <
      item
        PanelStyleClassName = 'TdxStatusBarTextPanelStyle'
        Text = 
          ' F5 - '#1053#1086#1074', F6 - '#1040#1078#1091#1088#1080#1088#1072#1112', F7 - '#1054#1089#1074#1077#1078#1080', F8 - '#1041#1088#1080#1096#1080', F9 - '#1047#1072#1087#1080#1096#1080', ' +
          'Esc - '#1054#1090#1082#1072#1078#1080' / '#1048#1079#1083#1077#1079
      end>
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clDefault
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ExplicitTop = 659
    ExplicitWidth = 774
  end
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 914
    Height = 638
    Align = alClient
    TabOrder = 4
    ExplicitHeight = 659
    DesignSize = (
      914
      638)
    object cxGrid1: TcxGrid
      Left = 1
      Top = 1
      Width = 912
      Height = 592
      Align = alTop
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -16
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      ExplicitLeft = 2
      ExplicitTop = 0
      object cxGrid1DBTableView1: TcxGridDBTableView
        Navigator.Buttons.CustomButtons = <>
        Navigator.Visible = True
        DataController.DataSource = dmBK.dsEtiketa
        DataController.Filter.Options = [fcoCaseInsensitive]
        DataController.Options = [dcoAnsiSort, dcoCaseInsensitive, dcoAssignGroupingValues, dcoAssignMasterDetailKeys, dcoSaveExpanding, dcoGroupsAlwaysExpanded]
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        Filtering.ColumnFilteredItemsList = True
        FilterRow.InfoText = #1050#1083#1080#1082#1085#1080' '#1086#1074#1076#1077' '#1079#1072' '#1076#1072' '#1076#1077#1092#1080#1085#1080#1088#1072#1096' '#1092#1080#1083#1090#1077#1088
        FilterRow.ApplyChanges = fracImmediately
        OptionsBehavior.IncSearch = True
        OptionsCustomize.ColumnsQuickCustomization = True
        OptionsData.Deleting = False
        OptionsData.Editing = False
        OptionsData.Inserting = False
        OptionsView.NoDataToDisplayInfoText = '<'#1053#1077#1084#1072' '#1077#1090#1080#1082#1077#1090#1080'>'
        OptionsView.GroupByBox = False
        object cxGrid1DBTableView1DATUM: TcxGridDBColumn
          Caption = #1044#1072#1090#1091#1084
          DataBinding.FieldName = 'DATUM'
          Width = 202
        end
        object cxGrid1DBTableView1TIP_ETIKETA: TcxGridDBColumn
          Caption = #1058#1080#1087' '#1085#1072' '#1077#1090#1080#1082#1077#1090#1072
          DataBinding.FieldName = 'TIP_ETIKETA'
          PropertiesClassName = 'TcxImageComboBoxProperties'
          Properties.Items = <
            item
              Description = #1061#1086#1084#1086#1075#1077#1085#1072
              ImageIndex = 0
              Value = 0
            end
            item
              Description = #1061#1077#1090#1077#1088#1086#1075#1077#1085#1072
              Value = 1
            end>
          Visible = False
          Width = 159
        end
        object cxGrid1DBTableView1ID: TcxGridDBColumn
          Caption = #1064#1080#1092#1088#1072
          DataBinding.FieldName = 'ID'
          Visible = False
        end
        object cxGrid1DBTableView1ID_RABOTEN_NALOG: TcxGridDBColumn
          Caption = #1064#1080#1092#1088#1072' '#1085#1072' '#1056#1053
          DataBinding.FieldName = 'ID_RABOTEN_NALOG'
          Visible = False
        end
        object cxGrid1DBTableView1BR_RN: TcxGridDBColumn
          Caption = #1041#1088#1086#1112' '#1085#1072' '#1056#1053
          DataBinding.FieldName = 'BR_RN'
          Width = 347
        end
        object cxGrid1DBTableView1DATUM_RN: TcxGridDBColumn
          Caption = #1044#1072#1090#1091#1084' '#1085#1072' '#1056#1053
          DataBinding.FieldName = 'DATUM_RN'
          Width = 122
        end
        object cxGrid1DBTableView1SSCC: TcxGridDBColumn
          DataBinding.FieldName = 'SSCC'
          Visible = False
          Width = 165
        end
        object cxGrid1DBTableView1GODINA: TcxGridDBColumn
          Caption = #1043#1086#1076#1080#1085#1072
          DataBinding.FieldName = 'GODINA'
          Visible = False
          Width = 76
        end
      end
      object cxGrid1DBTableView2: TcxGridDBTableView
        Navigator.Buttons.CustomButtons = <>
        DataController.DataSource = dsEtiketaStavka
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        OptionsBehavior.IncSearch = True
        OptionsCustomize.ColumnsQuickCustomization = True
        OptionsData.Deleting = False
        OptionsData.Inserting = False
        OptionsSelection.UnselectFocusedRecordOnExit = False
        OptionsView.NoDataToDisplayInfoText = '<'#1053#1077#1084#1072' '#1087#1086#1076#1072#1090#1086#1094#1080'>'
        OptionsView.GroupByBox = False
        Preview.Visible = True
        Styles.Preview = dmRes.cxStyle103
        object cxGrid1DBTableView2Izberi: TcxGridDBColumn
          Caption = #1048#1079#1073#1077#1088#1080
          DataBinding.ValueType = 'Boolean'
          PropertiesClassName = 'TcxCheckBoxProperties'
          Properties.ImmediatePost = True
          Properties.NullStyle = nssUnchecked
          Width = 76
        end
        object cxGrid1DBTableView2NAZIV_ARTIKAL: TcxGridDBColumn
          Caption = #1055#1088#1086#1080#1079#1074#1086#1076
          DataBinding.FieldName = 'NAZIV_ARTIKAL'
          Options.Editing = False
          Width = 325
        end
        object cxGrid1DBTableView2GTIN: TcxGridDBColumn
          DataBinding.FieldName = 'GTIN'
          Visible = False
          Options.Editing = False
          Width = 85
        end
        object cxGrid1DBTableView2SSCC: TcxGridDBColumn
          DataBinding.FieldName = 'SSCC'
          Visible = False
          Options.Editing = False
          Width = 72
        end
        object cxGrid1DBTableView2SERISKI_BROJ: TcxGridDBColumn
          Caption = #1057#1077#1088#1080#1089#1082#1080' '#1073#1088#1086#1112
          DataBinding.FieldName = 'SERISKI_BROJ'
          Visible = False
          Options.Editing = False
          Width = 69
        end
        object cxGrid1DBTableView2BARKOD: TcxGridDBColumn
          Caption = #1041#1072#1088#1082#1086#1076
          DataBinding.FieldName = 'BARKOD'
          Options.Editing = False
          Width = 313
        end
        object cxGrid1DBTableView2LOT: TcxGridDBColumn
          Caption = #1048#1085#1090#1077#1088#1077#1085' '#1051#1054#1058
          DataBinding.FieldName = 'LOT'
          Options.Editing = False
          Width = 125
        end
        object cxGrid1DBTableView2PRIMENI: TcxGridDBColumn
          Caption = #1055#1088#1080#1084#1077#1085#1080
          DataBinding.FieldName = 'PRIMENI'
          Options.Editing = False
          Width = 104
        end
        object cxGrid1DBTableView2ID: TcxGridDBColumn
          DataBinding.FieldName = 'ID'
          Visible = False
          Options.Editing = False
        end
        object cxGrid1DBTableView2BR_KUTIJA: TcxGridDBColumn
          Caption = #1041#1088#1086#1112' '#1085#1072' '#1082#1091#1090#1080#1112#1072
          DataBinding.FieldName = 'BR_KUTIJA'
        end
      end
      object cxGrid1Level1: TcxGridLevel
        GridView = cxGrid1DBTableView2
      end
    end
    object cxButton1: TcxButton
      Left = 653
      Top = 599
      Width = 113
      Height = 30
      Action = aIzberi
      Anchors = [akTop, akRight]
      TabOrder = 1
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object cxButton2: TcxButton
      Left = 799
      Top = 599
      Width = 102
      Height = 30
      Action = aOtkazi
      Anchors = [akTop, akRight]
      TabOrder = 2
    end
  end
  object cxGridPopupMenu1: TcxGridPopupMenu
    PopupMenus = <>
    Left = 590
    Top = 16
  end
  object PopupMenu1: TPopupMenu
    Left = 730
    Top = 16
    object N1: TMenuItem
      Caption = #1057#1085#1080#1084#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      ShortCut = 49235
      OnClick = SaveToIniFileExecute
    end
    object Excel1: TMenuItem
      Caption = #1057#1085#1080#1084#1080' '#1075#1086' '#1074#1086' Excel'
      ShortCut = 49221
    end
  end
  object dxBarManager1: TdxBarManager
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Segoe UI'
    Font.Style = []
    Categories.Strings = (
      'Default')
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    ImageOptions.Images = dmRes.cxSmallImages
    ImageOptions.LargeImages = dmRes.cxLargeImages
    PopupMenuLinks = <>
    UseSystemFont = True
    Left = 625
    Top = 16
    DockControlHeights = (
      0
      0
      0
      0)
    object dxBarManager1Bar1: TdxBar
      Caption = 'Custom 1'
      CaptionButtons = <>
      DockedDockingStyle = dsNone
      DockedLeft = 0
      DockedTop = 0
      DockingStyle = dsNone
      FloatLeft = 715
      FloatTop = 157
      FloatClientWidth = 69
      FloatClientHeight = 167
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton1'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton2'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarLargeButton3'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = False
      WholeRow = False
    end
    object dxBarManager1Bar2: TdxBar
      Caption = #1040#1082#1094#1080#1080
      CaptionButtons = <>
      DockedDockingStyle = dsNone
      DockedLeft = 183
      DockedTop = 0
      DockingStyle = dsNone
      FloatLeft = 789
      FloatTop = 120
      FloatClientWidth = 149
      FloatClientHeight = 130
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton10'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton5'
        end
        item
          Visible = True
          ItemName = 'dxBarSubItem1'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = False
      WholeRow = False
    end
    object dxBarManager1Bar3: TdxBar
      Caption = #1048#1079#1083#1077#1079
      CaptionButtons = <>
      DockedDockingStyle = dsNone
      DockedLeft = 428
      DockedTop = 0
      DockingStyle = dsNone
      FloatLeft = 718
      FloatTop = 62
      FloatClientWidth = 59
      FloatClientHeight = 108
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton7'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton8'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = False
      WholeRow = False
    end
    object dxBarManager1Bar4: TdxBar
      Caption = #1055#1077#1095#1072#1090#1077#1114#1077
      CaptionButtons = <>
      DockedDockingStyle = dsNone
      DockedLeft = 0
      DockedTop = 0
      DockingStyle = dsNone
      FloatLeft = 676
      FloatTop = 8
      FloatClientWidth = 141
      FloatClientHeight = 221
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton12'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton15'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton9'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarLargeButton13'
        end>
      OneOnRow = True
      Row = 0
      UseOwnFont = False
      Visible = False
      WholeRow = False
    end
    object dxBarManager1BarIzgledGrid: TdxBar
      Caption = #1058#1072#1073#1077#1083#1072
      CaptionButtons = <>
      DockedDockingStyle = dsNone
      DockedLeft = 363
      DockedTop = 0
      DockingStyle = dsNone
      FloatLeft = 808
      FloatTop = 8
      FloatClientWidth = 174
      FloatClientHeight = 113
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLBtnSnimiIzgled'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarLBtnBrisiIzgled'
        end>
      OneOnRow = True
      Row = 0
      UseOwnFont = False
      Visible = False
      WholeRow = False
    end
    object dxBarLargeButton2: TdxBarLargeButton
      Action = aAzuriraj
      Category = 0
    end
    object dxBarLargeButton3: TdxBarLargeButton
      Action = aBrisi
      Category = 0
    end
    object dxBarLargeButton4: TdxBarLargeButton
      Action = aSnimiIzgled
      Category = 0
      ScreenTip = tipSnimiIzgled
    end
    object dxBarLargeButton5: TdxBarLargeButton
      Action = aZacuvajExcel
      Category = 0
      ScreenTip = tipZacuvajExcel
    end
    object dxBarLargeButton6: TdxBarLargeButton
      Action = aPecatiTabela
      Caption = #1055#1077#1095#1072#1090#1080' '#1090#1072#1073#1077#1083#1072
      Category = 0
    end
    object dxBarLargeButton7: TdxBarLargeButton
      Action = aHelp
      Category = 0
    end
    object dxBarLargeButton8: TdxBarLargeButton
      Action = aIzlez
      Category = 0
    end
    object dxBarLargeButton1: TdxBarLargeButton
      Action = aNov
      Category = 0
    end
    object dxBarLargeButton9: TdxBarLargeButton
      Action = aSnimiPecatenje
      Category = 0
      ScreenTip = tipSnimiKonf
    end
    object dxBarLargeButton11: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object ribbonFilterReset: TdxBarLargeButton
      Caption = #1056#1077#1089#1077#1090#1080#1088#1072#1112' '#1092#1080#1083#1090#1077#1088
      Category = 0
      Hint = #1056#1077#1089#1077#1090#1080#1088#1072#1112' '#1092#1080#1083#1090#1077#1088
      Visible = ivAlways
    end
    object dxBarLargeButton12: TdxBarLargeButton
      Action = aPodesuvanjePecatenje
      Category = 0
      ScreenTip = tipDizajnReport
    end
    object dxBarLargeButton13: TdxBarLargeButton
      Action = aBrisiPodesuvanjePecatenje
      Category = 0
      ScreenTip = tipBrisiKonf
    end
    object dxBarLargeButton14: TdxBarLargeButton
      Caption = #1048#1079#1073#1088#1080#1096#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      Category = 0
      Visible = ivAlways
      LargeImageIndex = 40
    end
    object dxBarLargeButton15: TdxBarLargeButton
      Action = aPageSetup
      Category = 0
      ScreenTip = tipSetiranjeStrana
    end
    object dxBarLargeButton16: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object dxBarLargeButton17: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object cxBarEditItem1: TcxBarEditItem
      Caption = 'New Item'
      Category = 0
      Hint = 'New Item'
      Visible = ivAlways
      PropertiesClassName = 'TcxCheckBoxProperties'
    end
    object dxBarSubItem1: TdxBarSubItem
      Caption = #1055#1077#1095#1072#1090#1077#1114#1077
      Category = 0
      Visible = ivAlways
      ImageIndex = 30
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton6'
        end>
    end
    object dxBarLBtnSnimiIzgled: TdxBarLargeButton
      Action = aSnimiIzgled
      Category = 0
    end
    object dxBarLBtnBrisiIzgled: TdxBarLargeButton
      Action = aBrisiIzgled
      Category = 0
    end
    object dxBarLargeButton10: TdxBarLargeButton
      Action = aSpustiSoberi
      Category = 0
    end
  end
  object ActionList1: TActionList
    Images = dmRes.cxSmallImages
    Left = 555
    Top = 16
    object aNov: TAction
      Caption = #1053#1086#1074
      ImageIndex = 10
      ShortCut = 116
      OnExecute = aNovExecute
    end
    object aAzuriraj: TAction
      Caption = #1040#1078#1091#1088#1080#1088#1072#1112
      ImageIndex = 12
      ShortCut = 117
      OnExecute = aAzurirajExecute
    end
    object aBrisi: TAction
      Caption = #1041#1088#1080#1096#1080
      ImageIndex = 11
      ShortCut = 119
      OnExecute = aBrisiExecute
    end
    object aSnimiIzgled: TAction
      Caption = #1057#1085#1080#1084#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      ImageIndex = 9
      ShortCut = 24659
      OnExecute = aSnimiIzgledExecute
    end
    object aZacuvajExcel: TAction
      Caption = #1047#1072#1095#1091#1074#1072#1112' '#1074#1086' Excel'
      ImageIndex = 9
      ShortCut = 24645
      OnExecute = aZacuvajExcelExecute
    end
    object aPecatiTabela: TAction
      Caption = #1055#1077#1095#1072#1090#1080
      ImageIndex = 30
      OnExecute = aPecatiTabelaExecute
    end
    object aHelp: TAction
      Caption = #1055#1086#1084#1086#1096
      ImageIndex = 24
    end
    object aIzlez: TAction
      Caption = #1048#1079#1083#1077#1079
      ImageIndex = 25
      OnExecute = aIzlezExecute
    end
    object aZapisi: TAction
      Caption = #1047#1072#1087#1080#1096#1080
      ImageIndex = 7
      ShortCut = 120
      OnExecute = aZapisiExecute
    end
    object aOtkazi: TAction
      Caption = #1054#1090#1082#1072#1078#1080
      ImageIndex = 21
      ShortCut = 27
      OnExecute = aOtkaziExecute
    end
    object aRefresh: TAction
      Caption = 'aRefresh'
      ImageIndex = 18
      ShortCut = 118
      OnExecute = aRefreshExecute
    end
    object aSnimiPecatenje: TAction
      Caption = #1057#1085#1080#1084#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      ImageIndex = 39
      OnExecute = aSnimiPecatenjeExecute
    end
    object aPodesuvanjePecatenje: TAction
      Caption = #1044#1080#1079#1072#1112#1085' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090
      ImageIndex = 38
      OnExecute = aPodesuvanjePecatenjeExecute
    end
    object aBrisiPodesuvanjePecatenje: TAction
      Caption = #1041#1088#1080#1096#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      ImageIndex = 37
      OnExecute = aBrisiPodesuvanjePecatenjeExecute
    end
    object aPageSetup: TAction
      Caption = #1057#1077#1090#1080#1088#1072#1114#1077' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072
      ImageIndex = 38
      OnExecute = aPageSetupExecute
    end
    object aFormConfig: TAction
      Caption = 'aFormConfig'
      ShortCut = 24699
      OnExecute = aFormConfigExecute
    end
    object aBrisiIzgled: TAction
      Caption = #1041#1088#1080#1096#1080' '#1075#1086' '#1079#1072#1095#1091#1074#1072#1085#1080#1086#1090' '#1080#1079#1075#1083#1077#1076
      ImageIndex = 56
      OnExecute = aBrisiIzgledExecute
    end
    object aSpustiSoberi: TAction
      Caption = #1057#1087#1091#1096#1090#1080'/'#1057#1086#1073#1077#1088#1080' '#1058#1072#1073#1077#1083#1072
      ImageIndex = 65
      OnExecute = aSpustiSoberiExecute
    end
    object aIzberi: TAction
      Caption = #1048#1079#1073#1077#1088#1080
      ImageIndex = 85
      OnExecute = aIzberiExecute
    end
  end
  object dxComponentPrinter1: TdxComponentPrinter
    CurrentLink = dxComponentPrinter1Link1
    Version = 0
    Left = 695
    Top = 16
    object dxComponentPrinter1Link1: TdxGridReportLink
      PageNumberFormat = pnfNumeral
      PrinterPage.DMPaper = 9
      PrinterPage.Footer = 6350
      PrinterPage.GrayShading = True
      PrinterPage.Header = 6350
      PrinterPage.Margins.Bottom = 12700
      PrinterPage.Margins.Left = 12700
      PrinterPage.Margins.Right = 12700
      PrinterPage.Margins.Top = 12700
      PrinterPage.Orientation = poLandscape
      PrinterPage.PageFooter.RightTitle.Strings = (
        #1057#1090#1088'.[Page #] '#1086#1076' [Total Pages]')
      PrinterPage.PageSize.X = 210000
      PrinterPage.PageSize.Y = 297000
      PrinterPage._dxMeasurementUnits_ = 0
      PrinterPage._dxLastMU_ = 2
      ReportDocument.CreationDate = 40336.425359629630000000
      AssignedFormatValues = [fvDate, fvTime, fvPageNumber]
      OptionsFormatting.UseNativeStyles = True
      OptionsOnEveryPage.Caption = False
      OptionsOnEveryPage.FilterBar = False
      OptionsView.Caption = False
      OptionsView.FilterBar = False
      StyleRepository = dmRes.cxStyleRepositoryPrinting
      Styles.StyleSheet = dmRes.dxGridReportLinkStyleSheet
      BuiltInReportLink = True
    end
  end
  object dxBarScreenTipRepository1: TdxBarScreenTipRepository
    Left = 660
    Top = 16
    object tipSnimiIzgled: TdxBarScreenTip
      Header.Text = #1057#1085#1080#1084#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      Description.Text = #1047#1072#1095#1091#1074#1072#1112' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090' '#1085#1072' '#1092#1086#1088#1084#1072#1090#1072' '#1080' '#1085#1072' '#1090#1072#1073#1077#1083#1072#1090#1072
    end
    object tipZacuvajExcel: TdxBarScreenTip
      Header.Text = #1047#1072#1095#1091#1074#1072#1112' '#1074#1086' Excel'
      Description.Text = #1057#1085#1080#1084#1080' '#1075#1080' '#1087#1086#1076#1072#1090#1086#1094#1080#1090#1077' '#1086#1076' '#1090#1072#1073#1077#1083#1072#1090#1072' '#1074#1086' Excel '#1092#1086#1088#1084#1072#1090
    end
    object tipDizajnReport: TdxBarScreenTip
      Header.Text = #1044#1080#1079#1072#1112#1085' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090
      Description.Text = #1055#1086#1076#1077#1089#1091#1074#1072#1114#1072' '#1085#1072' '#1080#1079#1075#1083#1077#1076#1086#1090' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090' '#1085#1072' '#1090#1072#1073#1077#1083#1072#1090#1072
    end
    object tipSetiranjeStrana: TdxBarScreenTip
      Header.Text = #1057#1077#1090#1080#1088#1072#1114#1077' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072
      Description.Text = #1055#1086#1076#1077#1089#1091#1074#1072#1114#1077' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072' '#1085#1072' '#1082#1086#1112#1072' '#1116#1077' '#1089#1077' '#1087#1077#1095#1072#1090#1080' '#1090#1072#1073#1077#1083#1072#1090#1072
    end
    object tipSnimiKonf: TdxBarScreenTip
      Header.Text = #1057#1085#1080#1084#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      Description.Text = #1047#1072#1095#1091#1074#1072#1112' '#1075#1080' '#1087#1086#1076#1077#1089#1091#1074#1072#1114#1072#1090#1072' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090' '#1080' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072' '#1079#1072' '#1087#1077#1095#1072#1090#1077#1114#1077
    end
    object tipBrisiKonf: TdxBarScreenTip
      Header.Text = #1041#1088#1080#1096#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      Description.Text = #1048#1079#1073#1088#1080#1096#1080' '#1075#1080' '#1087#1086#1076#1077#1089#1091#1074#1072#1114#1072#1090#1072' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090' '#1080' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072' '#1079#1072' '#1087#1077#1095#1072#1090#1077#1114#1077
    end
  end
  object tblEtiketaStavka: TpFIBDataSet
    RefreshSQL.Strings = (
      'select mes.id,'
      'MES.ID_ETIKETA,'
      'MES.VID_ARTIKAL,'
      'MES.ARTIKAL,'
      'MA.NAZIV NAZIV_ARTIKAL,'
      'MES.GTIN,'
      'MES.SERISKI_BROJ,'
      'MES.BARKOD,'
      'MES.LOT,'
      'mes.br_kutija,'
      'sum(KTV.PRIMENI) PRIMENI'
      'from MAN_KT_VLEZ KTV'
      
        'inner join MAN_ETIKETA ME on ME.ID_RABOTEN_NALOG = KTV.ID_RABOTE' +
        'N_NALOG'
      
        'inner join MAN_ETIKETA_STAVKA MES on MES.ID_ETIKETA = ME.ID and ' +
        'KTV.VID_ARTIKAL = MES.VID_ARTIKAL and KTV.ARTIKAL = MES.ARTIKAL ' +
        'and KTV.ID_KT = 19'
      'inner join MAN_RABOTEN_NALOG RN on ME.ID_RABOTEN_NALOG = RN.ID'
      
        'inner join MAN_RABOTEN_NALOG_STAVKA RNS on RNS.ID_RABOTEN_NALOG ' +
        '= RN.ID and MES.VID_ARTIKAL = RNS.VID_ARTIKAL and MES.ARTIKAL = ' +
        'RNS.ARTIKAL and RNS.ID = MES.ID_RNS'
      
        'inner join MTR_ARTIKAL MA on MA.ARTVID = MES.VID_ARTIKAL and MA.' +
        'ID = MES.ARTIKAL'
      
        'inner join MTR_IZVEDENA_EM IM on IM.ARTVID = MES.VID_ARTIKAL and' +
        ' IM.ARTSIF = MES.ARTIKAL and IM.KOEFICIENT <> MES.KOLICINA_BROJ'
      'left join MTR_IN MI on MI.WO_REF_ID = RN.ID and MI.TIP_DOK = 11'
      'where --cast(KTV.DATUM as date) = :DATUM and'
      '      KTV.PRIMENI > 0 and'
      '      MI.ID is null and'
      '      mes.br_kutija is null'
      
        'group by mes.id, MES.ID_ETIKETA, MES.VID_ARTIKAL, MES.ARTIKAL, M' +
        'A.NAZIV, MES.GTIN, MES.SERISKI_BROJ, MES.BARKOD, MES.LOT, mes.br' +
        '_kutija  ')
    SelectSQL.Strings = (
      'select mes.id,'
      'MES.ID_ETIKETA,'
      'MES.VID_ARTIKAL,'
      'MES.ARTIKAL,'
      'MA.NAZIV NAZIV_ARTIKAL,'
      'MES.GTIN,'
      'MES.SERISKI_BROJ,'
      'MES.BARKOD,'
      'MES.LOT,'
      'mes.br_kutija,'
      'sum(KTV.PRIMENI) PRIMENI'
      'from MAN_KT_VLEZ KTV'
      
        'inner join MAN_ETIKETA ME on ME.ID_RABOTEN_NALOG = KTV.ID_RABOTE' +
        'N_NALOG'
      
        'inner join MAN_ETIKETA_STAVKA MES on MES.ID_ETIKETA = ME.ID and ' +
        'KTV.VID_ARTIKAL = MES.VID_ARTIKAL and KTV.ARTIKAL = MES.ARTIKAL ' +
        'and KTV.ID_KT = 19'
      'inner join MAN_RABOTEN_NALOG RN on ME.ID_RABOTEN_NALOG = RN.ID'
      
        'inner join MAN_RABOTEN_NALOG_STAVKA RNS on RNS.ID_RABOTEN_NALOG ' +
        '= RN.ID and MES.VID_ARTIKAL = RNS.VID_ARTIKAL and MES.ARTIKAL = ' +
        'RNS.ARTIKAL and RNS.ID = MES.ID_RNS'
      
        'inner join MTR_ARTIKAL MA on MA.ARTVID = MES.VID_ARTIKAL and MA.' +
        'ID = MES.ARTIKAL'
      
        'inner join MTR_IZVEDENA_EM IM on IM.ARTVID = MES.VID_ARTIKAL and' +
        ' IM.ARTSIF = MES.ARTIKAL and IM.KOEFICIENT <> MES.KOLICINA_BROJ'
      'left join MTR_IN MI on MI.WO_REF_ID = RN.ID and MI.TIP_DOK = 11'
      'where --cast(KTV.DATUM as date) = :DATUM and'
      '      KTV.PRIMENI > 0 and'
      '      MI.ID is null and'
      '      mes.br_kutija is null'
      
        'group by mes.id, MES.ID_ETIKETA, MES.VID_ARTIKAL, MES.ARTIKAL, M' +
        'A.NAZIV, MES.GTIN, MES.SERISKI_BROJ, MES.BARKOD, MES.LOT, mes.br' +
        '_kutija  ')
    AutoUpdateOptions.UpdateTableName = 'MAN_ETIKETA_STAVKA'
    AutoUpdateOptions.KeyFields = 'ID'
    AutoUpdateOptions.GeneratorName = 'GEN_MAN_ETIKETA_STAVKA_ID'
    AutoUpdateOptions.WhenGetGenID = wgBeforePost
    AutoUpdateOptions.AutoParamsToFields = True
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 696
    Top = 51
    object tblEtiketaStavkaID_ETIKETA: TFIBIntegerField
      FieldName = 'ID_ETIKETA'
    end
    object tblEtiketaStavkaVID_ARTIKAL: TFIBIntegerField
      FieldName = 'VID_ARTIKAL'
    end
    object tblEtiketaStavkaARTIKAL: TFIBIntegerField
      FieldName = 'ARTIKAL'
    end
    object tblEtiketaStavkaNAZIV_ARTIKAL: TFIBStringField
      FieldName = 'NAZIV_ARTIKAL'
      Size = 500
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblEtiketaStavkaGTIN: TFIBStringField
      FieldName = 'GTIN'
      Size = 30
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblEtiketaStavkaSERISKI_BROJ: TFIBIntegerField
      FieldName = 'SERISKI_BROJ'
    end
    object tblEtiketaStavkaBARKOD: TFIBStringField
      FieldName = 'BARKOD'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblEtiketaStavkaLOT: TFIBStringField
      FieldName = 'LOT'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblEtiketaStavkaPRIMENI: TFIBBCDField
      FieldName = 'PRIMENI'
      Size = 0
    end
    object tblEtiketaStavkaID: TFIBIntegerField
      FieldName = 'ID'
    end
    object tblEtiketaStavkaBR_KUTIJA: TFIBIntegerField
      FieldName = 'BR_KUTIJA'
    end
  end
  object dsEtiketaStavka: TDataSource
    DataSet = tblEtiketaStavka
    Left = 746
    Top = 16
  end
  object qUpdateBK: TpFIBQuery
    Transaction = dmKon.qTransaction
    Database = dmKon.fibBaza
    SQL.Strings = (
      'update man_etiketa_stavka es '
      'set es.br_kutija = :br_kutija'
      'where es.id = :id')
    Left = 552
    Top = 64
    qoAutoCommit = True
    qoStartTransaction = True
  end
end
