inherited frmOperacijaPR: TfrmOperacijaPR
  Caption = #1054#1087#1077#1088#1072#1094#1080#1080' '#1074#1086' '#1055#1088#1086#1080#1079#1074#1086#1076#1089#1090#1074#1077#1085' '#1088#1077#1089#1091#1088#1089
  ClientHeight = 611
  ExplicitWidth = 749
  ExplicitHeight = 649
  PixelsPerInch = 96
  TextHeight = 13
  inherited lPanel: TPanel
    Height = 274
    ExplicitHeight = 274
    inherited cxGrid1: TcxGrid
      Height = 270
      ExplicitHeight = 270
      inherited cxGrid1DBTableView1: TcxGridDBTableView
        OnFocusedRecordChanged = cxGrid1DBTableView1FocusedRecordChanged
        DataController.DataSource = dm.dsOperacijaPR
        object cxGrid1DBTableView1ID: TcxGridDBColumn
          DataBinding.FieldName = 'ID'
          Visible = False
        end
        object cxGrid1DBTableView1ID_PROIZVODSTVEN_RESURS: TcxGridDBColumn
          Caption = #1064#1080#1092#1088#1072' '#1085#1072' '#1087#1088#1086#1080#1079#1074#1086#1076#1089#1090#1074#1077#1085' '#1088#1077#1089#1091#1088#1089
          DataBinding.FieldName = 'ID_PROIZVODSTVEN_RESURS'
          Visible = False
        end
        object cxGrid1DBTableView1PROIZVODSTVEN_RESURS: TcxGridDBColumn
          Caption = #1055#1088#1086#1080#1079#1074#1086#1076#1089#1090#1074#1077#1085' '#1088#1077#1089#1091#1088#1089
          DataBinding.FieldName = 'PROIZVODSTVEN_RESURS'
          Width = 261
        end
        object cxGrid1DBTableView1ID_OPERACIJA: TcxGridDBColumn
          Caption = #1064#1080#1092#1088#1072' '#1085#1072' '#1086#1087#1077#1088#1072#1094#1080#1112#1072
          DataBinding.FieldName = 'ID_OPERACIJA'
          Width = 78
        end
        object cxGrid1DBTableView1NAZIV_OPERACIJA: TcxGridDBColumn
          DataBinding.FieldName = 'NAZIV_OPERACIJA'
          Width = 167
        end
        object cxGrid1DBTableView1SEKVENCA: TcxGridDBColumn
          Caption = #1057#1077#1082#1074#1077#1085#1094#1072
          DataBinding.FieldName = 'SEKVENCA'
        end
        object cxGrid1DBTableView1BROJ_CASOVI: TcxGridDBColumn
          Caption = #1041#1088#1086#1112' '#1085#1072' '#1095#1072#1089#1086#1074#1080
          DataBinding.FieldName = 'BROJ_CASOVI'
        end
        object cxGrid1DBTableView1BROJ_CIKLUSI: TcxGridDBColumn
          Caption = #1041#1086#1112' '#1085#1072' '#1094#1080#1082#1083#1091#1089#1080
          DataBinding.FieldName = 'BROJ_CIKLUSI'
        end
      end
    end
  end
  inherited dPanel: TPanel
    Top = 400
    Height = 188
    ExplicitTop = 400
    ExplicitHeight = 188
    inherited Label1: TLabel
      Left = 573
      Visible = False
      ExplicitLeft = 573
    end
    object Label7: TLabel [1]
      Left = 16
      Top = 21
      Width = 126
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1055#1088#1086#1080#1079#1074#1086#1076#1089#1090#1074#1077#1085' '#1088#1077#1089#1091#1088#1089' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label2: TLabel [2]
      Left = 64
      Top = 74
      Width = 78
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1057#1077#1082#1074#1077#1085#1094#1072' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label3: TLabel [3]
      Left = 40
      Top = 126
      Width = 102
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1055#1086#1090#1088#1077#1073#1085#1086' '#1074#1088#1077#1084#1077' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label4: TLabel [4]
      Left = 40
      Top = 151
      Width = 102
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1041#1088#1086#1112' '#1085#1072' '#1094#1080#1082#1083#1091#1089#1080' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    object Label5: TLabel [5]
      Left = -32
      Top = 99
      Width = 173
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1052#1077#1088#1085#1072' '#1077#1076#1080#1085#1080#1094#1072' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
    end
    inherited Sifra: TcxDBTextEdit
      Tag = 0
      Left = 637
      TabOrder = 10
      Visible = False
      ExplicitLeft = 637
    end
    inherited OtkaziButton: TcxButton
      Top = 148
      TabOrder = 11
      ExplicitTop = 148
    end
    inherited ZapisiButton: TcxButton
      Top = 148
      TabOrder = 9
      ExplicitTop = 148
    end
    object cxLabel6: TcxLabel
      Left = 78
      Top = 46
      Caption = #1054#1087#1077#1088#1072#1094#1080#1112#1072' :'
      Style.TextColor = clRed
      Properties.WordWrap = True
      Width = 64
    end
    object txtOperacija: TcxDBTextEdit
      Tag = 1
      Left = 148
      Top = 45
      DataBinding.DataField = 'ID_OPERACIJA'
      DataBinding.DataSource = dm.dsOperacijaPR
      ParentFont = False
      TabOrder = 2
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 62
    end
    object txtRE: TcxDBTextEdit
      Tag = 1
      Left = 148
      Top = 18
      BeepOnEnter = False
      DataBinding.DataField = 'ID_PROIZVODSTVEN_RESURS'
      DataBinding.DataSource = dm.dsOperacijaPR
      ParentFont = False
      Properties.BeepOnError = True
      StyleDisabled.BorderStyle = ebsUltraFlat
      StyleDisabled.Color = clWindow
      StyleDisabled.TextColor = clWindowText
      TabOrder = 0
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 62
    end
    object cbRE: TcxDBLookupComboBox
      Left = 210
      Top = 18
      Anchors = [akRight, akBottom]
      DataBinding.DataField = 'ID_PROIZVODSTVEN_RESURS'
      DataBinding.DataSource = dm.dsOperacijaPR
      Properties.ClearKey = 46
      Properties.DropDownSizeable = True
      Properties.ImmediatePost = True
      Properties.KeyFieldNames = 'ID'
      Properties.ListColumns = <
        item
          FieldName = 'ID'
        end
        item
          FieldName = 'naziv'
        end>
      Properties.ListFieldIndex = 1
      Properties.ListSource = dm.dsPR
      StyleDisabled.BorderStyle = ebsUltraFlat
      StyleDisabled.Color = clWindow
      StyleDisabled.TextColor = clWindowText
      TabOrder = 1
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 357
    end
    object txtSekvenca: TcxDBTextEdit
      Tag = 1
      Left = 148
      Top = 71
      BeepOnEnter = False
      DataBinding.DataField = 'SEKVENCA'
      DataBinding.DataSource = dm.dsOperacijaPR
      ParentFont = False
      Properties.BeepOnError = True
      Properties.CharCase = ecUpperCase
      Style.Shadow = False
      TabOrder = 4
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 62
    end
    object txtBrojCiklusi: TcxDBTextEdit
      Left = 148
      Top = 148
      BeepOnEnter = False
      DataBinding.DataField = 'BROJ_CIKLUSI'
      DataBinding.DataSource = dm.dsOperacijaPR
      ParentFont = False
      Properties.BeepOnError = True
      Properties.CharCase = ecUpperCase
      Style.Shadow = False
      TabOrder = 8
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 62
    end
    object cbOperacija: TcxDBLookupComboBox
      Left = 210
      Top = 46
      Anchors = [akRight, akBottom]
      DataBinding.DataField = 'ID_OPERACIJA'
      DataBinding.DataSource = dm.dsOperacijaPR
      Properties.ClearKey = 46
      Properties.DropDownSizeable = True
      Properties.ImmediatePost = True
      Properties.KeyFieldNames = 'ID'
      Properties.ListColumns = <
        item
          FieldName = 'ID'
        end
        item
          FieldName = 'naziv'
        end>
      Properties.ListFieldIndex = 1
      Properties.ListSource = dm.dsOperacija
      StyleDisabled.BorderStyle = ebsUltraFlat
      StyleDisabled.Color = clWindow
      StyleDisabled.TextColor = clWindowText
      TabOrder = 3
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 357
    end
    object txtVremenskaME: TcxDBTextEdit
      Tag = 1
      Left = 148
      Top = 96
      BeepOnEnter = False
      DataBinding.DataField = 'MERNA_EDINICA'
      DataBinding.DataSource = dm.dsOperacijaPR
      ParentFont = False
      Properties.BeepOnError = True
      TabOrder = 5
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 62
    end
    object cbVremenskaME: TcxDBLookupComboBox
      Left = 210
      Top = 96
      Hint = #1050#1083#1080#1082#1085#1077#1090#1077' '#1076#1074#1072' '#1087#1072#1090#1080' '#1079#1072' '#1076#1072' '#1074#1085#1077#1089#1077#1090#1077' '#1085#1086#1074#1072' '#1084#1077#1088#1085#1072' '#1077#1076#1080#1085#1080#1094#1072'!'
      Anchors = [akLeft, akTop, akRight, akBottom]
      DataBinding.DataField = 'MERNA_EDINICA'
      DataBinding.DataSource = dm.dsOperacijaPR
      Properties.ClearKey = 46
      Properties.DropDownSizeable = True
      Properties.ImmediatePost = True
      Properties.KeyFieldNames = 'ID'
      Properties.ListColumns = <
        item
          FieldName = 'ID'
        end
        item
          FieldName = 'naziv'
        end>
      Properties.ListFieldIndex = 1
      Properties.ListSource = dmMat.dsMerka
      TabOrder = 6
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 217
    end
    object txtPotrebnoVreme: TcxDBTextEdit
      Left = 148
      Top = 123
      BeepOnEnter = False
      DataBinding.DataField = 'POTREBNO_VREME'
      DataBinding.DataSource = dm.dsOperacijaPR
      ParentFont = False
      Properties.BeepOnError = True
      Properties.CharCase = ecUpperCase
      Style.Shadow = False
      TabOrder = 7
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 62
    end
  end
  inherited dxRibbon1: TdxRibbon
    inherited dxRibbon1Tab1: TdxRibbonTab
      Index = 0
    end
    inherited dxRibbon1Tab2: TdxRibbonTab
      Index = 1
    end
  end
  inherited StatusBar1: TdxRibbonStatusBar
    Top = 588
    ExplicitTop = 588
  end
  inherited dxBarManager1: TdxBarManager
    DockControlHeights = (
      0
      0
      0
      0)
    inherited dxBarManager1Bar1: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 69
      FloatClientHeight = 167
    end
    inherited dxBarManager1Bar2: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 149
      FloatClientHeight = 130
    end
    inherited dxBarManager1Bar3: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 59
      FloatClientHeight = 108
    end
    inherited dxBarManager1Bar4: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 141
      FloatClientHeight = 221
    end
    inherited dxBarManager1BarTabela: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 174
      FloatClientHeight = 113
    end
  end
  inherited dxComponentPrinter1: TdxComponentPrinter
    inherited dxComponentPrinter1Link1: TdxGridReportLink
      ReportDocument.CreationDate = 40994.461620509260000000
      AssignedFormatValues = [fvDate, fvTime, fvPageNumber]
      Styles.BandHeader = nil
      Styles.Caption = nil
      Styles.CardCaptionRow = nil
      Styles.CardRowCaption = nil
      Styles.Content = nil
      Styles.ContentEven = nil
      Styles.ContentOdd = nil
      Styles.FilterBar = nil
      Styles.Footer = nil
      Styles.Group = nil
      Styles.Header = nil
      Styles.Preview = nil
      Styles.Selection = nil
      BuiltInReportLink = True
    end
  end
end
