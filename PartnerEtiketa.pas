unit PartnerEtiketa;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Master, cxGraphics, cxControls,
  cxLookAndFeels, cxLookAndFeelPainters, cxStyles, dxSkinsCore, dxSkinBlack,
  dxSkinBlue, dxSkinBlueprint, dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom,
  dxSkinDarkSide, dxSkinDevExpressDarkStyle, dxSkinDevExpressStyle, dxSkinFoggy,
  dxSkinGlassOceans, dxSkinHighContrast, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinOffice2013White,
  dxSkinPumpkin, dxSkinSeven, dxSkinSevenClassic, dxSkinSharp, dxSkinSharpPlus,
  dxSkinSilver, dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008,
  dxSkinTheAsphaltWorld, dxSkinsDefaultPainters, dxSkinValentine, dxSkinVS2010,
  dxSkinWhiteprint, dxSkinXmas2008Blue, dxSkinscxPCPainter, cxCustomData,
  cxFilter, cxData, cxDataStorage, cxEdit, cxNavigator, Data.DB, cxDBData,
  cxContainer, Vcl.Menus, dxRibbonSkins, dxSkinsdxRibbonPainter,
  dxSkinsdxBarPainter, cxDropDownEdit, dxPSGlbl, dxPSUtl, dxPSEngn, dxPrnPg,
  dxBkgnd, dxWrap, dxPrnDev, dxPSCompsProvider, dxPSFillPatterns,
  dxPSEdgePatterns, dxPSPDFExportCore, dxPSPDFExport, cxDrawTextUtils,
  dxPSPrVwStd, dxPSPrVwAdv, dxPSPrVwRibbon, dxPScxPageControlProducer,
  dxPScxGridLnk, dxPScxGridLayoutViewLnk, dxPScxEditorProducers,
  dxPScxExtEditorProducers, dxScreenTip, dxBar, dxPSCore, dxPScxCommon,
  System.Actions, Vcl.ActnList, cxBarEditItem, cxGridCustomPopupMenu,
  cxGridPopupMenu, dxStatusBar, dxRibbonStatusBar, cxClasses, dxRibbon,
  Vcl.StdCtrls, cxButtons, cxTextEdit, cxDBEdit, cxGridLevel, cxGridCustomView,
  cxGridCustomTableView, cxGridTableView, cxGridDBTableView, cxGrid,
  Vcl.ExtCtrls, cxDBLookupComboBox, cxMaskEdit, cxLookupEdit, cxDBLookupEdit,
  cxLabel, dxSkinMetropolis, dxSkinMetropolisDark, dxSkinOffice2013DarkGray,
  dxSkinOffice2013LightGray, dxRibbonCustomizationForm;

type
  TfrmPartnerEtiketa = class(TfrmMaster)
    cxLabel36: TcxLabel;
    txtTipPartner: TcxDBTextEdit;
    txtPartner: TcxDBTextEdit;
    cbNazivPartner: TcxLookupComboBox;
    cxLabel38: TcxLabel;
    txtizvestaj: TcxDBTextEdit;
    cbIzvestaj: TcxDBLookupComboBox;
    cxGrid1DBTableView1ID: TcxGridDBColumn;
    cxGrid1DBTableView1TIP_PARNER: TcxGridDBColumn;
    cxGrid1DBTableView1PARTNER: TcxGridDBColumn;
    cxGrid1DBTableView1ID_SYS_REPORT: TcxGridDBColumn;
    cxGrid1DBTableView1NASLOV: TcxGridDBColumn;
    cxGrid1DBTableView1NAZIV_PARTNER: TcxGridDBColumn;
    procedure cxDBTextEditAllExit(Sender: TObject);
    procedure FormShow(Sender: TObject);
    procedure cxGrid1DBTableView1FocusedRecordChanged(
      Sender: TcxCustomGridTableView; APrevFocusedRecord,
      AFocusedRecord: TcxCustomGridRecord;
      ANewItemRecordFocusingChanged: Boolean);
    procedure aNovExecute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmPartnerEtiketa: TfrmPartnerEtiketa;

implementation

{$R *.dfm}

uses dmKonekcija, dmMaticni, dmResources, dmSystem, dmUnit;

procedure TfrmPartnerEtiketa.aNovExecute(Sender: TObject);
begin
  cbNazivPartner.Clear;
  inherited;

end;

procedure TfrmPartnerEtiketa.cxDBTextEditAllExit(Sender: TObject);
begin
 if (dm.tblPartnerEtiketa.State) in [dsInsert, dsEdit] then
    begin
     if ((Sender as TWinControl)= txtPartner) or ((Sender as TWinControl)= txtTipPartner) then
         begin
              dm.SetirajLukap(sender,dmMat.tblPartner,txtTipPartner,txtPartner, cbNazivPartner);
         end
         else
         if ((Sender as TWinControl)= cbNazivPartner) then
         begin
            if (cbNazivPartner.Text <>'') then
            begin
              txtTipPartner.Text := dmMat.tblPartnerTIP_PARTNER.AsString;
              txtPartner.Text := dmMat.tblPartnerID.AsString;
            end
            else
            begin
              dm.tblPartnerEtiketaTIP_PARNER.Clear;
              dm.tblPartnerEtiketaPARTNER.Clear;
            end;
         end;

         if ((Sender as TWinControl)= txtPartner) or ((Sender as TWinControl)= txtTipPartner) then
         begin
              dm.SetirajLukap(sender,dmMat.tblPartner,txtTipPartner,txtPartner, cbNazivPartner);
         end;

    end;
    inherited;
end;

procedure TfrmPartnerEtiketa.cxGrid1DBTableView1FocusedRecordChanged(
  Sender: TcxCustomGridTableView; APrevFocusedRecord,
  AFocusedRecord: TcxCustomGridRecord; ANewItemRecordFocusingChanged: Boolean);
begin
  inherited;
  dm.SetirajLukap(sender,dmMat.tblPartner,txtTipPartner,txtPartner, cbNazivPartner);
end;

procedure TfrmPartnerEtiketa.FormShow(Sender: TObject);
begin
  inherited;
  dm.SetirajLukap(sender,dmMat.tblPartner,txtTipPartner,txtPartner, cbNazivPartner);
end;

end.
