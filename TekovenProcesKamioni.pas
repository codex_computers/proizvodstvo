unit TekovenProcesKamioni;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData, cxEdit,
  DB, cxDBData, cxGridCustomPopupMenu, cxGridPopupMenu,
  ComCtrls, cxContainer, cxTextEdit, cxDBEdit, StdCtrls, cxGridLevel,
  cxClasses, cxControls, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGrid, ExtCtrls, Menus, ActnList,
  cxGridExportLink, cxExport, cxLookAndFeelPainters, cxDataStorage,
  cxButtons, dxStatusBar, dxRibbonStatusBar, dxRibbon, dxBar, dxBarExtItems,
  dxPSGlbl, dxPSUtl, dxPSEngn, dxPrnPg, dxBkgnd, dxWrap, dxPrnDev,
  dxPSCompsProvider, dxPSFillPatterns, dxPSEdgePatterns, dxPSCore, dxPScxCommon,
  cxPC, cxMemo, cxLocalization, cxLookAndFeels,
  dxPSPDFExportCore, dxPSPDFExport, cxDrawTextUtils, dxPSPrVwStd, dxPSPrVwAdv,
  dxPSPrVwRibbon, dxPScxEditorProducers, dxPScxExtEditorProducers,
  dxPScxPageControlProducer, dxPgsDlg, cxCheckBox, cxBarEditItem, dxSkinsCore,
  dxSkinBlack, dxSkinBlue, dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom,
  dxSkinDarkSide, dxSkinFoggy, dxSkinGlassOceans, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven,
  dxSkinSharp, dxSkinSilver, dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008,
  dxSkinsDefaultPainters, dxSkinValentine, dxSkinXmas2008Blue,
  dxSkinsdxRibbonPainter, dxSkinsdxBarPainter, dxSkinscxPCPainter,
  dxBarSkinnedCustForm, cxDropDownEdit, cxCalendar, cxDBLookupComboBox,
  cxMaskEdit, cxLookupEdit, cxDBLookupEdit, cxDBExtLookupComboBox, cxGroupBox,
  cxRadioGroup, dxRibbonSkins, dxPScxGridLnk, dxPScxGridLayoutViewLnk,
  dxScreenTip, dxSkinBlueprint, dxSkinDevExpressDarkStyle,
  dxSkinDevExpressStyle, dxSkinHighContrast, dxSkinSevenClassic,
  dxSkinSharpPlus, dxSkinTheAsphaltWorld, dxSkinVS2010, dxSkinWhiteprint,
  cxLabel, cxDBLabel, FIBDataSet, pFIBDataSet, dxSkinMetropolis,
  dxSkinMetropolisDark, dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray,
  dxSkinOffice2013White, dxRibbonCustomizationForm, dxCore, cxDateUtils,
  System.Actions, dxSkinOffice2016Colorful, dxSkinOffice2016Dark,
  dxSkinVisualStudio2013Blue, dxSkinVisualStudio2013Dark,
  dxSkinVisualStudio2013Light;

type
//  niza = Array[1..5] of Variant;

  TfrmTekovenProcesKamioni = class(TForm)
    cxGridPopupMenu1: TcxGridPopupMenu;
    PopupMenu1: TPopupMenu;
    N1: TMenuItem;
    Excel1: TMenuItem;
    dxRibbon1Tab1: TdxRibbonTab;
    dxRibbon1: TdxRibbon;
    StatusBar1: TdxRibbonStatusBar;
    dxBarManager1: TdxBarManager;
    dxBarManager1Bar3: TdxBar;
    dxBarLargeButton2: TdxBarLargeButton;
    dxBarLargeButton3: TdxBarLargeButton;
    dxBarLargeButton4: TdxBarLargeButton;
    dxBarLargeButton5: TdxBarLargeButton;
    dxBarLargeButton6: TdxBarLargeButton;
    dxBarLargeButton7: TdxBarLargeButton;
    dxBarLargeButton8: TdxBarLargeButton;
    dxBarLargeButton1: TdxBarLargeButton;
    ActionList1: TActionList;
    aNov: TAction;
    aAzuriraj: TAction;
    aBrisi: TAction;
    aSnimiIzgled: TAction;
    aZacuvajExcel: TAction;
    aPecatiTabela: TAction;
    aHelp: TAction;
    aIzlez: TAction;
    aZapisi: TAction;
    aOtkazi: TAction;
    aRefresh: TAction;
    dxRibbon1Tab2: TdxRibbonTab;
    dxBarManager1Bar4: TdxBar;
    dxBarLargeButton9: TdxBarLargeButton;
    aSnimiPecatenje: TAction;
    dxComponentPrinter1: TdxComponentPrinter;
    dxComponentPrinter1Link1: TdxGridReportLink;
    dxBarLargeButton11: TdxBarLargeButton;
    ribbonFilterReset: TdxBarLargeButton;
    aPodesuvanjePecatenje: TAction;
    dxBarLargeButton12: TdxBarLargeButton;
    aBrisiPodesuvanjePecatenje: TAction;
    dxBarLargeButton13: TdxBarLargeButton;
    dxBarLargeButton14: TdxBarLargeButton;
    aPageSetup: TAction;
    dxBarLargeButton15: TdxBarLargeButton;
    dxBarLargeButton16: TdxBarLargeButton;
    dxBarLargeButton17: TdxBarLargeButton;
    cxBarEditItem1: TcxBarEditItem;
    dxBarSubItem1: TdxBarSubItem;
    aFormConfig: TAction;
    dxBarScreenTipRepository1: TdxBarScreenTipRepository;
    tipSnimiIzgled: TdxBarScreenTip;
    tipZacuvajExcel: TdxBarScreenTip;
    tipDizajnReport: TdxBarScreenTip;
    tipSetiranjeStrana: TdxBarScreenTip;
    tipSnimiKonf: TdxBarScreenTip;
    tipBrisiKonf: TdxBarScreenTip;
    dxBarManager1BarIzgledGrid: TdxBar;
    dxBarLBtnSnimiIzgled: TdxBarLargeButton;
    dxBarLBtnBrisiIzgled: TdxBarLargeButton;
    aBrisiIzgled: TAction;
    PanelMain: TPanel;
    gbPrebaruvanje: TcxGroupBox;
    Label14: TLabel;
    cxLabel8: TcxLabel;
    cxLabel7: TcxLabel;
    txtDatum: TcxDateEdit;
    cbKamion: TcxLookupComboBox;
    cxButton3: TcxButton;
    txtTP: TcxTextEdit;
    txtP: TcxTextEdit;
    aPogled: TAction;
    Label1: TLabel;
    txtVidArtikal: TcxTextEdit;
    txtArtikal: TcxTextEdit;
    cbArtikal: TcxLookupComboBox;
    cbP: TcxTextEdit;
    tblIzberiKamioni: TpFIBDataSet;
    dsIzberiKamioni: TDataSource;
    tblArtikalRN: TpFIBDataSet;
    tblArtikalRNARTVID: TFIBIntegerField;
    tblArtikalRNID: TFIBIntegerField;
    tblArtikalRNNAZIV: TFIBStringField;
    dsArtikalRN: TDataSource;
    aPoveke: TAction;
    fbstrngfldIzberiKamioniNAZIV_TRAILER: TFIBStringField;
    fbdtfldIzberiKamioniDATUM_TRANSPORT: TFIBDateField;
    GridPanel1: TGridPanel;
    dPanel5: TPanel;
    btMasinsko: TcxButton;
    cxlbl16: TcxLabel;
    cxlbl17: TcxLabel;
    cxlbl18: TcxLabel;
    txtVkKolicina1: TcxDBTextEdit;
    txtZavrseni1: TcxDBTextEdit;
    txtOstanati1: TcxDBTextEdit;
    cxDBLabel1: TcxDBLabel;
    dPanel6: TPanel;
    cxlbl19: TcxLabel;
    cxlbl20: TcxLabel;
    cxlbl21: TcxLabel;
    btFarbara: TcxButton;
    txtVkKolicina2: TcxDBTextEdit;
    txtZavrseni2: TcxDBTextEdit;
    txtOstanati2: TcxDBTextEdit;
    cxDBLabel2: TcxDBLabel;
    cxlbl22: TcxLabel;
    txtPrimeni2: TcxDBTextEdit;
    dPanel7: TPanel;
    cxButton9: TcxButton;
    cxlbl23: TcxLabel;
    cxlbl24: TcxLabel;
    cxlbl25: TcxLabel;
    cxButton10: TcxButton;
    cxButton15: TcxButton;
    txtVkKolicina5: TcxDBTextEdit;
    txtZavrrseni5: TcxDBTextEdit;
    txtOstanati5: TcxDBTextEdit;
    cxDBLabel5: TcxDBLabel;
    cxlbl26: TcxLabel;
    txtPrimeni5: TcxDBTextEdit;
    btMagGP: TcxButton;
    dPanel8: TPanel;
    cxButton5: TcxButton;
    cxlbl27: TcxLabel;
    cxlbl28: TcxLabel;
    cxlbl29: TcxLabel;
    cxButton6: TcxButton;
    cxButton13: TcxButton;
    txtVkKolicina3: TcxDBTextEdit;
    txtZavrseni3: TcxDBTextEdit;
    txtOstanati3: TcxDBTextEdit;
    cxDBLabel3: TcxDBLabel;
    cxlbl30: TcxLabel;
    txtPrimeni31: TcxDBTextEdit;
    btSivara: TcxButton;
    dPanel9: TPanel;
    cxButton7: TcxButton;
    cxlbl31: TcxLabel;
    cxlbl32: TcxLabel;
    cxlbl33: TcxLabel;
    cxButton8: TcxButton;
    cxButton14: TcxButton;
    txtVkKolicina4: TcxDBTextEdit;
    txtZavrseni4: TcxDBTextEdit;
    txtOstanati4: TcxDBTextEdit;
    cxDBLabel4: TcxDBLabel;
    cxlbl34: TcxLabel;
    txtPrimeni4: TcxDBTextEdit;
    cxlbl35: TcxLabel;
    txtPrimeni3: TcxDBTextEdit;
    btTapacersko: TcxButton;
    dPanel1: TPanel;
    btMasinskoNapredok: TcxButton;
    cxlbl1: TcxLabel;
    cxlbl2: TcxLabel;
    cxlbl3: TcxLabel;
    txtVkKolicina6: TcxDBTextEdit;
    txtZavrseni6: TcxDBTextEdit;
    txtOstanati6: TcxDBTextEdit;
    cxDBLabel6: TcxDBLabel;
    dPanel2: TPanel;
    btn1: TcxButton;
    cxlbl4: TcxLabel;
    cxlbl5: TcxLabel;
    cxlbl6: TcxLabel;
    btMebelStip: TcxButton;
    txtVkKolicina8: TcxDBTextEdit;
    txtZavrseni8: TcxDBTextEdit;
    txtOstanati8: TcxDBTextEdit;
    cxDBLabel8: TcxDBLabel;
    cxlbl7: TcxLabel;
    txtPrimeni8: TcxDBTextEdit;
    dPanel3: TPanel;
    btn2: TcxButton;
    cxlbl8: TcxLabel;
    cxlbl9: TcxLabel;
    cxlbl10: TcxLabel;
    btDorabotkaStip: TcxButton;
    txtVkKolicina7: TcxDBTextEdit;
    txtZavrseni7: TcxDBTextEdit;
    txtOstanati7: TcxDBTextEdit;
    cxDBLabel7: TcxDBLabel;
    cxlbl11: TcxLabel;
    txtPrimeni7: TcxDBTextEdit;
    dPanel4: TPanel;
    btn3: TcxButton;
    cxlbl12: TcxLabel;
    cxlbl13: TcxLabel;
    cxlbl14: TcxLabel;
    btn4: TcxButton;
    btn5: TcxButton;
    txtVkKolicina9: TcxDBTextEdit;
    txtZavrseni9: TcxDBTextEdit;
    txtOstanati9: TcxDBTextEdit;
    cxDBLabel9: TcxDBLabel;
    cxlbl15: TcxLabel;
    txtPrimeni9: TcxDBTextEdit;
    btTrgovskaStoka: TcxButton;
    tblIzberiKamioniID_TRAILER: TFIBIntegerField;

    procedure cxDBTextEditAllEnter(Sender: TObject);
    procedure cxDBTextEditAllExit(Sender: TObject);
    procedure EnterKakoTab(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    //procedure PrvPosledenTab(panel:TPanel;var posledna,prva:TWinControl);
    procedure SaveToIniFileExecute(Sender: TObject);
    procedure cxGrid1DBTableView1KeyPress(Sender: TObject; var Key: Char);
    procedure aZapisiExecute(Sender: TObject);
    procedure aOtkaziExecute(Sender: TObject);
    procedure aNovExecute(Sender: TObject);
    procedure aAzurirajExecute(Sender: TObject);
    procedure aBrisiExecute(Sender: TObject);
    procedure aZacuvajExcelExecute(Sender: TObject);
    procedure aRefreshExecute(Sender: TObject);
    procedure aSnimiIzgledExecute(Sender: TObject);
    procedure aIzlezExecute(Sender: TObject);
    procedure aPecatiTabelaExecute(Sender: TObject);
    procedure aSnimiPecatenjeExecute(Sender: TObject);
    procedure zacuvajPrintVoIni(ime:AnsiString);
    procedure procitajPrintOdIni(ime:AnsiString);
    procedure aPodesuvanjePecatenjeExecute(Sender: TObject);
    procedure aBrisiPodesuvanjePecatenjeExecute(Sender: TObject);
    procedure aPageSetupExecute(Sender: TObject);
    procedure cxGrid1DBTableView1FilterCustomization(
      Sender: TcxCustomGridTableView; var ADone: Boolean);
    procedure aFormConfigExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure PartnerPropertiesChange(Sender: TObject);
    procedure aBrisiIzgledExecute(Sender: TObject);
    procedure SetirajLukap(sender:TObject; tabela:TDataSet; tip:TcxTextEdit; sifra:TcxTextEdit; lukap:TcxLookupComboBox);
    procedure aPogledExecute(Sender: TObject);
    procedure cbRNPropertiesChange(Sender: TObject);
    procedure aPovekeExecute(Sender: TObject);
    procedure gbPrebaruvanjeClick(Sender: TObject);
    procedure cxlbl16Click(Sender: TObject);
  private
    { Private declarations }

    _sifra_kluc :array[1..10] of string;  // �������� ���������� �� ������ �� ������� �� ������������� ���

  protected
    list : TList;
    inserting : boolean;
    prva, posledna :TWinControl;
    procedure prefrli;

  public
    { Public declarations }
    constructor Create(Owner : TComponent; insert : boolean = true);reintroduce; overload;

    procedure SetSifra(br: integer; s : string);
    function GetSifra(br: integer) : string;

    //������� �� ���������� ������������ _sifra_kluc
    property sifra_kluc[br :integer]: string read GetSifra write SetSifra;
  end;

var
  frmTekovenProcesKamioni: TfrmTekovenProcesKamioni;

implementation

uses DaNe, dmKonekcija, Utils, dmResources, FormConfig, dmMaticni, dmSystem,
  dmUnit, KTIzlez, KTIzlezKamion;

{$R *.dfm}
//------------------------------------------------------------------------------

constructor TfrmTekovenProcesKamioni.Create(Owner : TComponent; insert : boolean = true);
begin
inherited Create(Owner);
//	�������� ���� ������� � �������� �� ����������� ���������
//	insert = true -> ����� ���� ����� ���������� �� ������� �� Insert Mode
//  insert = false -> ����� ���� ����� ������� �� ����� �� ������. �� ������� ����� ����� �� �� �������� F5
  inserting := insert;
end;

//	����� �� �����������. �� ��������� ������� �� ������, �� �������� ������� �� ���� ��������,
//	�� ������� ������� �� ������ ������ � �� ������� �� Insert Mode
procedure TfrmTekovenProcesKamioni.aNovExecute(Sender: TObject);
begin
//  if(cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) then
//  begin
//    dPanel.Enabled:=True;
//    lPanel.Enabled:=False;
//    prva.SetFocus;
//    cxGrid1DBTableView1.DataController.DataSet.Insert;
//  end
//  else ShowMessage('������ �������� �� ��� �������� �� �� ��������� �������/r/n � ����� ������� �� ������� ��� �����!');
end;

//	����� �� ���������. �� ��������� ������� �� ������, �� �������� ������� �� ���� ��������,
//	�� ������� ������� �� ������ ������ � �� ������� �� Edit Mode
procedure TfrmTekovenProcesKamioni.aAzurirajExecute(Sender: TObject);
begin
//  if(cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) then
//  begin
//    dPanel.Enabled:=True;
//    lPanel.Enabled:=False;
//    prva.SetFocus;
//    cxGrid1DBTableView1.DataController.DataSet.Edit;
//  end
//  else ShowMessage('������ �������� �� ��� �������� �� �� ��������� �������/r/n � ����� ������� �� ������� ��� �����!');
end;

//	����� �� ������ �� ������������� �����
procedure TfrmTekovenProcesKamioni.aBrisiExecute(Sender: TObject);
begin
//  if ((cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) and
//     (cxGrid1DBTableView1.DataController.RecordCount <> 0)) then
//    cxGrid1DBTableView1.DataController.DataSet.Delete();
end;

procedure TfrmTekovenProcesKamioni.aBrisiIzgledExecute(Sender: TObject);
begin
  //brisiGridVoBaza(Name,cxGrid1DBTableView1);
  //brisiPivotVoBaza(Name,cxDBPivotGrid1); �� pivot ��� ���
  BrisiFormaIzgled(self);
end;

//	����� �� ���������� �� ����������
procedure TfrmTekovenProcesKamioni.aRefreshExecute(Sender: TObject);
begin
//  cxGrid1DBTableView1.DataController.Filter.Root.Clear; //���������� �� ��������
//  cxGrid1DBTableView1.DataController.DataSet.Refresh;
end;

//	����� �� ����� �� ������� �� ������� �� ������� �����
procedure TfrmTekovenProcesKamioni.aIzlezExecute(Sender: TObject);
begin
  Close;
end;

//	����� �� ������� �� �������� �� ������ �� ���� (Utils.pas)
procedure TfrmTekovenProcesKamioni.aSnimiIzgledExecute(Sender: TObject);
begin
//  zacuvajGridVoBaza(Name,cxGrid1DBTableView1);
  ZacuvajFormaIzgled(self);
end;

//	����� �� ������� �� ������ �� Excel ������ (Utils.pas)
procedure TfrmTekovenProcesKamioni.aZacuvajExcelExecute(Sender: TObject);
begin
//  zacuvajVoExcel(cxGrid1, Caption);
end;

//  ��������� �� ������ �� ������� �� ���������� �� ����� �� Enter
procedure TfrmTekovenProcesKamioni.EnterKakoTab(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
    case Key of
        VK_DOWN:
        begin
          PostMessage(Handle,WM_NextDlgCtl,0,0);
          key:=0;
        end;
        VK_UP:
        begin
          PostMessage(Handle,WM_NextDlgCtl,1,0);
          key:=0;
        end;
        VK_RETURN:
        begin
          PostMessage(Handle,WM_NextDlgCtl,0,0);
        end;
    end;
end;

//	������� �� ��� �� ������ ��� �������� �� �����
procedure TfrmTekovenProcesKamioni.cxDBTextEditAllEnter(Sender: TObject);
begin
    TEdit(Sender).Color:=clSkyBlue;
end;

//	������� �� ��� �� ������ ��� ����� �� �����
procedure TfrmTekovenProcesKamioni.cxDBTextEditAllExit(Sender: TObject);
begin
   if ((Sender as TWinControl)= cbKamion) then
   begin
     //txtTP.Text := tblIzberikRNTP_PORACKA.AsString;
     //txtP.Text := tblIzberiRNP_PORACKA.AsString;
    // SetirajLukap();
     //cbP.Text := tblIzberiRNNAZIV_PARTNER_PORACKA.Value;
     tblArtikalRN.Close;
     //tblArtikalRN.ParamByName('id').Value := tblIzberiRNID.Value;
     tblArtikalRN.Open;

   //  txtRN.Text := dm.tblRabotenNalogBROJ.AsString;
   end;


   if ((Sender as TWinControl)= cbP) then
        begin
            if (cbP.Text <>'') then
            begin
              txtTP.Text := cbP.EditValue[0]; // dm.tblKooperantiTIP_PARTNER.AsString; //cbPartner.EditValue[0];
              txtP.Text := cbP.EditValue[1]; // dm.tblKooperantiID.AsString;
            //  txtMesto.Text :=  dm.tblKooperantiNAZIV_MESTO.Value;  //cbKoop.EditValue[3];
           //   txtAdresa.Text := dm.tblKooperantiADRESA.Value;  //cbKoop.EditValue[4];
            end
            else
            begin
              txtTP.Clear;
              txtP.Clear;
           //   txtMesto.Clear;
            //  txtAdresa.Clear;
            end;
         end;
         if ((Sender as TWinControl)= txtP) or ((Sender as TWinControl)= txtTP) then
         begin
          if (txtTP.Text <>'') and (txtP.Text<>'')  then
          begin
            if(dmMat.tblPartner.Locate('TIP_PARTNER;ID',VarArrayOf([txtTP.text,txtP.text]),[])) then
            begin
               cbP.Text:=dmMat.tblPartnerNAZIV.Value;
             //  txtMesto.Text := dm.tblKooperantiNAZIV_MESTO.Value;
             //  txtAdresa.Text := dm.tblKooperantiADRESA.Value;
            end
           end
            else
            begin
               cbP.Clear;
               //txtMesto.Clear;
               //txtAdresa.Clear;
             end;

         end;

   // if (dm.tblRNStavka.State) in [dsInsert, dsEdit] then
   // begin
   if ((Sender as TWinControl)= cbArtikal) then
        begin
            if (cbArtikal.Text <>'') then
            begin
              txtVidArtikal.Text := tblArtikalRNARTVID.AsString;
              txtArtikal.Text := tblArtikalRNID.AsString;
              //dm.tblNormativO.Close;
              //dm.tblNormativO.ParamByName('mas_id').va
            end
            else
            begin
              txtVidArtikal.Clear;
              txtArtikal.Clear;
            end;
           end
           else
            if ((Sender as TWinControl)= txtArtikal) or ((Sender as TWinControl)= txtVidArtikal) then
                SetirajLukap(sender,tblArtikalRN,txtVidArtikal,txtArtikal, cbArtikal);


//         if (not tblIzberiRNID.IsNull) and (not tblArtikalRN.Active) then
//         begin
//                end;

  //  end;
    TEdit(Sender).Color:=clWhite;
end;

procedure TfrmTekovenProcesKamioni.cxGrid1DBTableView1FilterCustomization(
  Sender: TcxCustomGridTableView; var ADone: Boolean);
begin
end;

//  ��������� �� ������ �� ��������� �� ���������� ���� �� ��������
//  ��������� ��: br - ����� ��� �� ������ (�� 0 �� 9) �� ��������� �� ����� �� ����� �� ���������� ����
//  s - ��������� �� ������ �� ������ (���������� �� string �� ����� �� integer �� IntToStr ���������)
procedure TfrmTekovenProcesKamioni.SetSifra(br: integer; s : string);
begin
  _sifra_kluc[br] := s;
end;

//  ������� �� ������ �� ��������� �� ���������� ���� �� ��������
//  ��������� ��: br - ����� ��� �� ������ (�� 0 �� 9) �� ��������� �� ����� �� ����� �� ���������� ����
function TfrmTekovenProcesKamioni.GetSifra(br: integer) : string;
begin
  Result := _sifra_kluc[br];
end;

procedure TfrmTekovenProcesKamioni.PartnerPropertiesChange(Sender: TObject);
begin

end;

//  �� ������� �� ������� ��� ����������� �� Master-�� �� �������� �� ��������� �� ������ ����� SetSifra
procedure TfrmTekovenProcesKamioni.prefrli;
begin
end;

procedure TfrmTekovenProcesKamioni.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    //	��������� ��� ��������� �� �������
//    if (cxGrid1DBTableView1.DataController.DataSource.State = dsEdit) or (cxGrid1DBTableView1.DataController.DataSource.State = dsInsert) then
//    begin
//        frmDaNe := TfrmDaNe.Create(self, '���������� ��������', '���������� �� �� ��������. ���� ������ �� �� �������?', 1);
//        if (frmDaNe.ShowModal <> mrYes) then
//        begin
//            cxGrid1DBTableView1.DataController.DataSet.Cancel;
//            Action := caFree;
//        end
//        else
//          if (Validacija(dPanel) = false) then
//          begin
//            cxGrid1DBTableView1.DataController.DataSet.Post;
            dm.pTekovenProcesKamion.Close;
            Action := caFree;
//          end
//          else Action := caNone;
//    end;
end;
procedure TfrmTekovenProcesKamioni.FormCreate(Sender: TObject);
begin
  ProcitajFormaIzgled(self);
  if (dmRes.use_skin = true) then dxRibbon1.ColorSchemeName := dmRes.skin_name;

 // dm.tblRabotenNalog.Close;
 // dm.tblRabotenNalog.ParamByName('status').AsString := '1';
 // dm.tblRabotenNalog.ParamByName('godina').AsString := '%';
  // ����������� site = 2 ����� �� �� ������� ���� �� ��� �� �� ������� �� ����� �.�. ������ �� ���� ��
 // dm.tblRabotenNalog.ParamByName('site').AsString := '2';
//if inttostr(dmKon.UserRE) <> ''  then
//  dm.tblRabotenNalog.ParamByName('re').AsInteger := dmkon.UserRE
//else
//  dm.tblRabotenNalog.ParamByName('re').AsString := '%' ;
//  dm.tblRabotenNalog.Open;
end;

//------------------------------------------------------------------------------

procedure TfrmTekovenProcesKamioni.FormShow(Sender: TObject);
begin
  //  ������� �� ������������ ������������/���������� �� ���������� �� ���� �����
    SpremiForma(self);
  //	�� ��������� �� ������� ������ ��� ���������� � ���� � ��� �������� �� �������
//    PrvPosledenTab(dPanel,posledna,prva);
    //dxBarManager1Bar1.Caption := Caption;
//    dxComponentPrinter1Link1.ReportTitle.Text := Caption;

  //	������� �� ������������ �� ������ �� ������
//    procitajGridOdBaza(Name,cxGrid1DBTableView1,false,false);
  //	������� �� ������������ �� ��������� �� ������
//    procitajPrintOdBaza(Name,cxGrid1DBTableView1.Name, dxComponentPrinter1Link1);


      tblIzberiKamioni.Close;
      tblIzberiKamioni.ParamByName('status').AsString := '0';
      tblIzberiKamioni.Open;
      txtDatum.Date := now;
end;
procedure TfrmTekovenProcesKamioni.gbPrebaruvanjeClick(Sender: TObject);
begin

end;

//------------------------------------------------------------------------------

procedure TfrmTekovenProcesKamioni.SaveToIniFileExecute(Sender: TObject);
begin
//    cxGrid1DBTableView1.StoreToIniFile(Name,True,[]);
end;
//------------------------------------------------------------------------------

procedure TfrmTekovenProcesKamioni.cxGrid1DBTableView1KeyPress(Sender: TObject;
  var Key: Char);
begin
//  if(Ord(Key) <> VK_RETURN) then cxSortiraj(cxGrid1DBTableView1);
end;

procedure TfrmTekovenProcesKamioni.cxlbl16Click(Sender: TObject);
begin

end;

//  ����� �� �����
procedure TfrmTekovenProcesKamioni.aZapisiExecute(Sender: TObject);
var
  st: TDataSetState;
begin
//  ZapisiButton.SetFocus;

//  st := cxGrid1DBTableView1.DataController.DataSet.State;
//  if st in [dsEdit,dsInsert] then
//  begin
//    if (Validacija(dPanel) = false) then
//    begin
//      if ((st = dsInsert) and inserting) then
//      begin
//        cxGrid1DBTableView1.DataController.DataSet.Post;
//        aNov.Execute;
//      end;
//
//      if ((st = dsInsert) and (not inserting)) then
//      begin
//        cxGrid1DBTableView1.DataController.DataSet.Post;
//        dPanel.Enabled:=false;
//        lPanel.Enabled:=true;
//        cxGrid1.SetFocus;
//      end;
//
//      if (st = dsEdit) then
//      begin
//        cxGrid1DBTableView1.DataController.DataSet.Post;
//        dPanel.Enabled:=false;
//        lPanel.Enabled:=true;
//        cxGrid1.SetFocus;
//      end;
//    end;
//  end;
end;

procedure TfrmTekovenProcesKamioni.cbRNPropertiesChange(Sender: TObject);
begin

end;

//	����� �� ���������� �� �������
procedure TfrmTekovenProcesKamioni.aOtkaziExecute(Sender: TObject);
begin
//  if (cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) then
//  begin
//      ModalResult := mrCancel;
     Close();
//  end
//  else
//  begin
//      cxGrid1DBTableView1.DataController.DataSet.Cancel;
//      RestoreControls(dPanel);
//      dPanel.Enabled := false;
//      lPanel.Enabled := true;
//      cxGrid1.SetFocus;
//  end;
end;

//----------------------------------------------------------------------------------
// ��������� �� ��������, ���������� �� ��������� � �������� �� Print ��������
//----------------------------------------------------------------------------------

//	����� �� �������� �� �������� �� �������
procedure TfrmTekovenProcesKamioni.aPageSetupExecute(Sender: TObject);
begin
  dxComponentPrinter1Link1.PageSetup;
end;

//	����� �� ������� �� ������
procedure TfrmTekovenProcesKamioni.aPecatiTabelaExecute(Sender: TObject);
begin
  dxComponentPrinter1Link1.ReportTitle.Text := Caption;

  dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Clear;
  dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Add(dmkon.firma_naziv);
  dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Add(DateTimeToStr(Now));

  dxComponentPrinter1Link1.PrinterPage.PageHeader.LeftTitle.Clear;
  //dxComponentPrinter1Link1.PrinterPage.PageHeader.LeftTitle.Add('������� : ' + TipSifra.Text + '-' + IdSifra.Text + ' ' + Partner.Text);

  dxComponentPrinter1.Preview(true, dxComponentPrinter1Link1);
end;

//	����� �� ���������� �� ��������� �� �������
procedure TfrmTekovenProcesKamioni.aPodesuvanjePecatenjeExecute(Sender: TObject);
begin
  dxComponentPrinter1Link1.DesignReport();
end;

procedure TfrmTekovenProcesKamioni.aPogledExecute(Sender: TObject);
begin
 if (cbKamion.Text <> '') and (txtDatum.Text <> '') then
   begin
       dm.pTekovenProcesKamion.Close;
     dm.pTekovenProcesKamion.ParamByName('kamion').AsString := tblIzberiKamioniID_TRAILER.AsString;;
       dm.pTekovenProcesKamion.ParamByName('datum').AsString := txtDatum.Text;

     if txtVidArtikal.Text <> '' then
      begin
       dm.pTekovenProcesKamion.ParamByName('vid_Artikal').AsString := txtVidArtikal.Text;
       dm.pTekovenProcesKamion.ParamByName('artikal').AsString := txtArtikal.Text;
      end
      else
      begin
       dm.pTekovenProcesKamion.ParamByName('vid_Artikal').AsString := '%';
       dm.pTekovenProcesKamion.ParamByName('artikal').AsString := '%';
      end;
       dm.pTekovenProcesKamion.Open;
   end
   else
   begin
     ShowMessage('������� �� �������������� �����!');
     Abort;
   end;
end;

procedure TfrmTekovenProcesKamioni.aPovekeExecute(Sender: TObject);
begin
  frmKTIzlezKamion := TfrmKTIzlezKamion.Create(Self);
  frmKTIzlezKamion.id_trailer := tblIzberiKamioniID_TRAILER.Value;
 // frmKTIzlezKamion.id_re := tblIzberiRNID_RE.Value;
  if txtVidArtikal.Text <> '' then
  begin
    frmKTIzlezKamion.vid_artikal := txtVidArtikal.Text;
    frmKTIzlezKamion.artikal := txtArtikal.Text;
  end
  else
  begin
    frmKTIzlezKamion.vid_artikal := '%';
    frmKTIzlezKamion.artikal := '%';
  end;
  if btMasinsko.Focused then
  begin
   frmKTIzlezKamion.id_kt := dm.pTekovenProcesKamionID_KT1.Value;
   frmKTIzlezKamion.Caption := '������� ������� '+dm.pTekovenProcesKamionKT_NAZIV1.Value;
   frmKTIzlezKamion.lblKT.Caption := '�� '+ dm.pTekovenProcesKamionKT_NAZIV1.Value+', ������:'+cbKamion.Text+', �� �����: '+txtDatum.Text;
   frmKTIzlezKamion.lblKT.Style.Color := $00C4FFFF;
  end
  else
  if btFarbara.Focused then
  begin
   frmKTIzlezKamion.id_kt := dm.pTekovenProcesKamionID_KT2.Value;
   frmKTIzlezKamion.Caption := '������� ������� '+dm.pTekovenProcesKamionKT_NAZIV2.Value;;
   frmKTIzlezKamion.lblKT.Caption := dm.pTekovenProcesKamionKT_NAZIV2.Value;
   frmKTIzlezKamion.lblKT.Caption := '�� '+ dm.pTekovenProcesKamionKT_NAZIV2.Value+', ������:'+cbKamion.Text+', �� �����: '+txtDatum.Text;
   frmKTIzlezKamion.lblKT.Style.Color := $00A8D3FF;
  end
  else
  if btTapacersko.Focused then
  begin
   frmKTIzlezKamion.id_kt := dm.pTekovenProcesKamionID_KT4.Value;
   frmKTIzlezKamion.Caption := '������� ������� '+ dm.pTekovenProcesKamionKT_NAZIV4.Value;
   frmKTIzlezKamion.lblKT.Caption := dm.pTekovenProcesKamionKT_NAZIV4.Value;
   frmKTIzlezKamion.lblKT.Caption := '�� '+ dm.pTekovenProcesKamionKT_NAZIV4.Value+', ������:'+cbKamion.Text+', �� �����: '+txtDatum.Text;
   frmKTIzlezKamion.lblKT.Style.Color := $00D2FFC4;
  end
  else
  if btMagGP.Focused then
  begin
   frmKTIzlezKamion.id_kt := dm.pTekovenProcesKamionID_KT5.Value;
   frmKTIzlezKamion.Caption := '������� ������� '+ dm.pTekovenProcesKamionKT_NAZIV5.Value;
   frmKTIzlezKamion.lblKT.Caption := dm.pTekovenProcesKamionKT_NAZIV5.Value;
   frmKTIzlezKamion.lblKT.Caption := '�� '+ dm.pTekovenProcesKamionKT_NAZIV5.Value+', ������:'+cbKamion.Text+', �� �����: '+txtDatum.Text;
   frmKTIzlezKamion.lblKT.Style.Color := $00FFE7B3;
  end
  else
  if btSivara.Focused then
  begin
   frmKTIzlezKamion.id_kt := dm.pTekovenProcesKamionID_KT3.Value;
   frmKTIzlezKamion.Caption := '������� ������� '+ dm.pTekovenProcesKamionKT_NAZIV3.Value;
   frmKTIzlezKamion.lblKT.Caption := dm.pTekovenProcesKamionKT_NAZIV3.Value;
   frmKTIzlezKamion.lblKT.Caption := '�� '+ dm.pTekovenProcesKamionKT_NAZIV3.Value+', ������:'+cbKamion.Text+', �� �����: '+txtDatum.Text;
   frmKTIzlezKamion.lblKT.Style.Color := $00E1C4FF;
  end
  else
  if btMasinskoNapredok.Focused then
  begin
   frmKTIzlezKamion.id_kt := dm.pTekovenProcesKamionID_KT6.Value;
   frmKTIzlezKamion.Caption := '������� ������� '+dm.pTekovenProcesKamionKT_NAZIV6.Value;
   frmKTIzlezKamion.lblKT.Caption := '�� '+ dm.pTekovenProcesKamionKT_NAZIV6.Value+', ������: '+cbKamion.Text+', �� �����: '+txtDatum.Text;
   frmKTIzlezKamion.lblKT.Style.Color := $00C4FFFF;
  end
  else
  if btDorabotkaStip.Focused then
  begin
   frmKTIzlezKamion.id_kt := dm.pTekovenProcesKamionID_KT9.Value;
   frmKTIzlezKamion.Caption := '������� ������� '+dm.pTekovenProcesKamionKT_NAZIV9.Value;;
   frmKTIzlezKamion.lblKT.Caption := dm.pTekovenProcesKamionKT_NAZIV9.Value;
   frmKTIzlezKamion.lblKT.Caption := '�� '+ dm.pTekovenProcesKamionKT_NAZIV9.Value+', ������: '+cbKamion.Text+', �� �����: '+txtDatum.Text;
   frmKTIzlezKamion.lblKT.Style.Color := $00A8D3FF;
  end
  else
  if btMebelStip.Focused then
  begin
   frmKTIzlezKamion.id_kt := dm.pTekovenProcesKamionID_KT7.Value;
   frmKTIzlezKamion.Caption := '������� ������� '+dm.pTekovenProcesKamionKT_NAZIV7.Value;;
   frmKTIzlezKamion.lblKT.Caption := dm.pTekovenProcesKamionKT_NAZIV7.Value;
   frmKTIzlezKamion.lblKT.Caption := '�� '+ dm.pTekovenProcesKamionKT_NAZIV7.Value+', ������: '+cbKamion.Text+', �� �����: '+txtDatum.Text;
   frmKTIzlezKamion.lblKT.Style.Color := $00A8D3FF;
  end
  else
  if btTrgovskaStoka.Focused then
  begin
   frmKTIzlezKamion.id_kt := dm.pTekovenProcesKamionID_KT8.Value;
   frmKTIzlezKamion.Caption := '������� ������� '+dm.pTekovenProcesKamionKT_NAZIV8.Value;;
   frmKTIzlezKamion.lblKT.Caption := dm.pTekovenProcesKamionKT_NAZIV8.Value;
   frmKTIzlezKamion.lblKT.Caption := '�� '+ dm.pTekovenProcesKamionKT_NAZIV8.Value+', ������: '+cbKamion.Text+', �� �����: '+txtDatum.Text;
   frmKTIzlezKamion.lblKT.Style.Color := $00A8D3FF;
  end;

  //frmKTIzlezKamion.Caption := '������� ������� '+cxDBLabel1.Caption;
  frmKTIzlezKamion.ShowModal;
  frmKTIzlezKamion.Free;
end;

procedure TfrmTekovenProcesKamioni.aSnimiPecatenjeExecute(Sender: TObject);
begin
//  zacuvajPrintVoBaza(Name,cxGrid1DBTableView1.Name,dxComponentPrinter1Link1);
end;

//	����� �� ������ �� ����������� �������� �� ������� (Utils.pas)
procedure TfrmTekovenProcesKamioni.aBrisiPodesuvanjePecatenjeExecute(Sender: TObject);
begin
//  brisiPrintOdBaza(Name,cxGrid1DBTableView1.Name, dxComponentPrinter1Link1);
end;

//  ����� �� ����� �� ����� �� ������������� �� �������� � ��������������� ��
//  �������(������ ���������� ��������)
procedure TfrmTekovenProcesKamioni.aFormConfigExecute(Sender: TObject);
begin
  frmFormConfig := TfrmFormConfig.Create(Application);
  frmFormConfig.formPtr := Addr(Self);
  frmFormConfig.ShowModal;
  frmFormConfig.Free;
end;

//----------------------------------------------------------------------------------
// ���� -> ��������� �� ��������, ���������� �� ��������� � �������� �� Print ��������
//----------------------------------------------------------------------------------

// ��������� �� ���������� �� ��������� � �������� �� Print �������� �� .ini ��������
procedure TfrmTekovenProcesKamioni.zacuvajPrintVoIni(ime:AnsiString);
var
  AStream: TFileStream;
begin
  AStream := TFileStream.Create(dmKon.RabotenDir + ime + 'Print.ini', fmCreate);
  try
    AStream.WriteComponent(dxComponentPrinter1Link1);
  finally
    AStream.Free;
  end;

end;

// ��������� �� ������ �� ��������� � �������� �� Print �������� �������� �� .ini ��������
procedure TfrmTekovenProcesKamioni.procitajPrintOdIni(ime:AnsiString);
var
  AStream: TFileStream;
begin
  AStream := TFileStream.Create(dmKon.RabotenDir + ime + 'Print.ini', fmOpenRead);
  try
    dxComponentPrinter1Link1.RestoreDefaults;
    AStream.ReadComponent(dxComponentPrinter1Link1);
  finally
    AStream.Free;
  end;
end;

procedure TfrmTekovenProcesKamioni.SetirajLukap(sender:TObject; tabela:TDataSet; tip:TcxTextEdit; sifra:TcxTextEdit; lukap:TcxLookupComboBox);
begin
         if (tip.Text <>'') and (sifra.Text<>'')  then
         begin
          if(tabela.Locate('ARTVID;ID',VarArrayOf([tip.text,sifra.text]),[])) then
               lukap.Text:=tabela.FieldByName('NAZIV').Value;
         end
         else
         begin
            lukap.Clear;
         end;
end;


end.
