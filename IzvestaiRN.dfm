﻿object frmIzvestaiRN: TfrmIzvestaiRN
  Left = 128
  Top = 212
  BorderIcons = [biSystemMenu, biMaximize]
  Caption = #1056#1072#1073#1086#1090#1077#1085' '#1085#1072#1083#1086#1075
  ClientHeight = 710
  ClientWidth = 1024
  Color = clBtnFace
  Constraints.MinHeight = 500
  Constraints.MinWidth = 610
  Font.Charset = RUSSIAN_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  ShowHint = True
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Label2: TLabel
    Left = 408
    Top = 168
    Width = 31
    Height = 13
    Caption = 'Label2'
  end
  object dxRibbon1: TdxRibbon
    Left = 0
    Top = 0
    Width = 1024
    Height = 126
    BarManager = dxBarManager1
    ColorSchemeName = 'Blue'
    Fonts.AssignedFonts = [afGroup, afGroupHeader]
    Fonts.Group.Charset = DEFAULT_CHARSET
    Fonts.Group.Color = 9126421
    Fonts.Group.Height = -12
    Fonts.Group.Name = 'Segoe UI'
    Fonts.Group.Style = []
    Fonts.GroupHeader.Charset = DEFAULT_CHARSET
    Fonts.GroupHeader.Color = 11168318
    Fonts.GroupHeader.Height = -12
    Fonts.GroupHeader.Name = 'Segoe UI'
    Fonts.GroupHeader.Style = []
    Contexts = <>
    TabOrder = 0
    TabStop = False
    OnTabChanging = dxRibbon1TabChanging
    object dxRibbon1Tab1: TdxRibbonTab
      Active = True
      Caption = #1052#1077#1085#1080
      Groups = <
        item
          ToolbarName = 'dxBarManager1Bar6'
        end
        item
          ToolbarName = 'bmRabotenNalog'
        end
        item
          ToolbarName = 'bmStavki'
        end
        item
          ToolbarName = 'dxBarManager1Bar2'
        end
        item
          ToolbarName = 'dxBarManager1Bar3'
        end>
      Index = 0
    end
    object dxRibbon1Tab2: TdxRibbonTab
      Caption = #1055#1086#1076#1077#1089#1091#1074#1072#1114#1072
      Groups = <
        item
          ToolbarName = 'dxBarManager1Bar4'
        end
        item
          ToolbarName = 'dxBarManager1BarIzgledGrid'
        end>
      Index = 1
    end
  end
  object StatusBar1: TdxRibbonStatusBar
    Left = 0
    Top = 687
    Width = 1024
    Height = 23
    Panels = <
      item
        PanelStyleClassName = 'TdxStatusBarTextPanelStyle'
        Text = 
          'F5 - '#1053#1086#1074', F6 - '#1040#1078#1091#1088#1080#1088#1072#1112', F7 - '#1054#1089#1074#1077#1078#1080', F8 - '#1041#1088#1080#1096#1080', F9 - '#1047#1072#1087#1080#1096#1080', C' +
          'trl+A - '#1040#1082#1090#1080#1074#1080#1088#1072#1112' / '#1044#1077#1072#1082#1090#1080#1074#1080#1088#1072#1112' '#1083#1080#1085#1080#1112#1072', Ctrl+Z - '#1047#1072#1090#1074#1086#1088#1080' '#1083#1080#1085#1080#1112#1072',' +
          '  Esc - '#1054#1090#1082#1072#1078#1080' / '#1048#1079#1083#1077#1079
      end>
    Ribbon = dxRibbon1
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clDefault
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
  end
  object cxPageControl1: TcxPageControl
    Left = 0
    Top = 126
    Width = 1024
    Height = 561
    Align = alClient
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
    TabOrder = 2
    Properties.ActivePage = tsRN
    Properties.CustomButtons.Buttons = <>
    Properties.Images = dmRes.cxSmallImages
    Properties.NavigatorPosition = npLeftTop
    Properties.Rotate = True
    Properties.ShowFrame = True
    Properties.TabCaptionAlignment = taLeftJustify
    Properties.TabPosition = tpLeft
    LookAndFeel.Kind = lfUltraFlat
    LookAndFeel.NativeStyle = False
    OnChange = cxPageControl1Change
    ClientRectBottom = 560
    ClientRectLeft = 172
    ClientRectRight = 1023
    ClientRectTop = 1
    object tsListaRN: TcxTabSheet
      Caption = ' '#1051#1080#1089#1090#1072' '#1085#1072' '#1056#1072#1073#1086#1090#1085#1080' '#1085#1072#1083#1086#1079#1080'  '
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
      ImageIndex = 0
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object lPanel: TPanel
        Left = 0
        Top = 0
        Width = 851
        Height = 559
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 0
        object cxGrid1: TcxGrid
          Left = 0
          Top = 0
          Width = 851
          Height = 559
          Align = alClient
          Font.Charset = RUSSIAN_CHARSET
          Font.Color = clWindowText
          Font.Height = -13
          Font.Name = 'Tahoma'
          Font.Style = []
          Images = dmRes.cxImageGrid
          ParentFont = False
          TabOrder = 0
          LevelTabs.CaptionAlignment = taLeftJustify
          RootLevelOptions.DetailTabsPosition = dtpTop
          object cxGrid1DBTableView1: TcxGridDBTableView
            OnDblClick = cxGrid1DBTableView1DblClick
            OnKeyDown = cxGrid1DBTableView1KeyDown
            Navigator.Buttons.CustomButtons = <>
            OnFocusedRecordChanged = cxGrid1DBTableView1FocusedRecordChanged
            DataController.DataSource = dm.dsRabotenNalog
            DataController.Options = [dcoAnsiSort, dcoAssignGroupingValues, dcoAssignMasterDetailKeys, dcoSaveExpanding]
            DataController.Summary.DefaultGroupSummaryItems = <>
            DataController.Summary.FooterSummaryItems = <
              item
                Kind = skCount
                OnGetText = cxGrid1DBTableView1TcxGridDBDataControllerTcxDataSummaryFooterSummaryItems0GetText
                FieldName = 'NAZIV_PARTNER_PORACKA'
                Column = cxGrid1DBTableView1NAZIV_PARTNER_PORACKA
              end>
            DataController.Summary.SummaryGroups = <>
            DataController.OnDetailCollapsed = cxGrid1DBTableView1DataControllerDetailCollapsed
            DataController.OnDetailExpanding = cxGrid1DBTableView1DataControllerDetailExpanding
            DataController.OnDetailExpanded = cxGrid1DBTableView1DataControllerDetailExpanded
            FilterRow.InfoText = #1050#1083#1080#1082#1085#1077#1090#1077' '#1090#1091#1082#1072' '#1079#1072' '#1076#1072' '#1076#1077#1092#1080#1085#1080#1088#1072#1090#1077' '#1092#1080#1083#1090#1077#1088
            FilterRow.Visible = True
            Images = dmRes.cxImageGrid
            OptionsBehavior.CellHints = True
            OptionsBehavior.IncSearch = True
            OptionsBehavior.ImmediateEditor = False
            OptionsCustomize.ColumnsQuickCustomization = True
            OptionsData.Deleting = False
            OptionsData.Editing = False
            OptionsData.Inserting = False
            OptionsView.Footer = True
            OptionsView.Indicator = True
            object cxGrid1DBTableView1STATUS: TcxGridDBColumn
              DataBinding.FieldName = 'STATUS'
              PropertiesClassName = 'TcxImageComboBoxProperties'
              Properties.Images = dmRes.cxImageGrid
              Properties.ImmediatePost = True
              Properties.Items = <
                item
                  ImageIndex = 11
                  Value = 1
                end
                item
                  ImageIndex = 12
                  Value = 2
                end
                item
                  ImageIndex = 13
                  Value = 3
                end>
              Width = 40
              IsCaptionAssigned = True
            end
            object cxGrid1DBTableView1BOJA: TcxGridDBColumn
              DataBinding.FieldName = 'BOJA'
              PropertiesClassName = 'TcxColorComboBoxProperties'
              Properties.CustomColors = <>
              Properties.ShowDescriptions = False
              Visible = False
              Width = 49
              IsCaptionAssigned = True
            end
            object cxGrid1DBTableView1NAZIV_STATUS: TcxGridDBColumn
              Caption = #1057#1090#1072#1090#1091#1089
              DataBinding.FieldName = 'NAZIV_STATUS'
              Width = 98
            end
            object cxGrid1DBTableView1NAZIV_STATUS_STAVKI: TcxGridDBColumn
              Caption = #1053#1072#1079#1080#1074' '#1089#1090#1072#1090#1091#1089' '#1089#1090#1072#1074#1082#1080
              DataBinding.FieldName = 'NAZIV_STATUS_STAVKI'
              Visible = False
              Width = 159
            end
            object cxGrid1DBTableView1ID: TcxGridDBColumn
              Caption = #1064#1080#1092#1088#1072
              DataBinding.FieldName = 'ID'
              Visible = False
            end
            object cxGrid1DBTableView1BROJ: TcxGridDBColumn
              DataBinding.FieldName = 'BROJ'
              Width = 91
            end
            object cxGrid1DBTableView1BROJ_SD: TcxGridDBColumn
              Caption = #1048#1079#1074#1086#1088#1077#1085' '#1076#1086#1082#1091#1084#1077#1085#1090
              DataBinding.FieldName = 'BROJ_SD'
              Width = 57
            end
            object cxGrid1DBTableView1ID_RE: TcxGridDBColumn
              Caption = #1064#1080#1092#1088#1072' '#1085#1072' '#1057#1077#1082#1090#1086#1088'/'#1054#1076#1077#1083#1077#1085#1080#1077
              DataBinding.FieldName = 'ID_RE'
            end
            object cxGrid1DBTableView1NAZIV_RE: TcxGridDBColumn
              Caption = #1057#1077#1082#1090#1086#1088'/'#1054#1076#1076#1077#1083#1077#1085#1080#1077
              DataBinding.FieldName = 'NAZIV_RE'
              Width = 109
            end
            object cxGrid1DBTableView1DATUM_PLANIRAN_POCETOK: TcxGridDBColumn
              Caption = #1044#1072#1090#1091#1084' '#1085#1072' '#1087#1083#1072#1085#1080#1088#1072#1085' '#1087#1086#1095#1077#1090#1086#1082
              DataBinding.FieldName = 'DATUM_PLANIRAN_POCETOK'
              SortIndex = 0
              SortOrder = soAscending
            end
            object cxGrid1DBTableView1SOURCE_DOKUMENT: TcxGridDBColumn
              Caption = #1048#1079#1074#1086#1088#1077#1085' '#1076#1086#1082#1091#1084#1077#1085#1090
              DataBinding.FieldName = 'SOURCE_DOKUMENT'
              Visible = False
              Width = 82
            end
            object cxGrid1DBTableView1ID_PORACKA: TcxGridDBColumn
              Caption = #1064#1080#1092#1088#1072' '#1085#1072' '#1087#1086#1088#1072#1095#1082#1072
              DataBinding.FieldName = 'ID_PORACKA'
              Visible = False
            end
            object cxGrid1DBTableView1TP_PORACKA: TcxGridDBColumn
              Caption = #1058#1080#1087' '#1085#1072' '#1082#1091#1087#1091#1074#1072#1095
              DataBinding.FieldName = 'TP_PORACKA'
              Visible = False
            end
            object cxGrid1DBTableView1P_PORACKA: TcxGridDBColumn
              Caption = #1064#1080#1092#1088#1072' '#1085#1072' '#1082#1091#1087#1091#1074#1072#1095
              DataBinding.FieldName = 'P_PORACKA'
              Visible = False
            end
            object cxGrid1DBTableView1NAZIV_PARTNER_PORACKA: TcxGridDBColumn
              Caption = #1055#1072#1088#1090#1085#1077#1088
              DataBinding.FieldName = 'NAZIV_PARTNER_PORACKA'
              Width = 167
            end
            object cxGrid1DBTableView1DATUM_START: TcxGridDBColumn
              Caption = #1055#1086#1095#1077#1090#1077#1085' '#1076#1072#1090#1091#1084
              DataBinding.FieldName = 'DATUM_START'
              Width = 101
            end
            object cxGrid1DBTableView1Column1: TcxGridDBColumn
              Caption = #1057#1090#1072#1090#1091#1089
              DataBinding.ValueType = 'String'
              PropertiesClassName = 'TcxButtonEditProperties'
              Properties.Buttons = <
                item
                  Default = True
                  Kind = bkEllipsis
                end>
              Visible = False
            end
            object cxGrid1DBTableView1ID_RE_SUROVINI: TcxGridDBColumn
              Caption = #1064#1080#1092#1088#1072' '#1085#1072' '#1084#1072#1075#1072#1094#1080#1085' '#1079#1072' '#1089#1091#1088#1086#1074#1080#1085#1080
              DataBinding.FieldName = 'ID_RE_SUROVINI'
            end
            object cxGrid1DBTableView1NAZIV_MAGACIN_SUROVINI: TcxGridDBColumn
              Caption = #1052#1072#1075#1072#1094#1080#1085' '#1079#1072' '#1089#1091#1088#1086#1074#1080#1085#1080
              DataBinding.FieldName = 'NAZIV_MAGACIN_SUROVINI'
              Width = 142
            end
            object cxGrid1DBTableView1ID_RE_GOTOV_PROIZVOD: TcxGridDBColumn
              Caption = #1064#1080#1092#1088#1072' '#1085#1072' '#1084#1072#1075#1072#1094#1080#1085' '#1079#1072' '#1075#1086#1090#1086#1074#1080' '#1087#1088#1086#1080#1079#1074#1086#1076#1080
              DataBinding.FieldName = 'ID_RE_GOTOV_PROIZVOD'
              Width = 57
            end
            object cxGrid1DBTableView1NAZIV_MAGACIN_GOTOVI_PROIZVODI: TcxGridDBColumn
              Caption = #1053#1072#1079#1080#1074' '#1085#1072' '#1084#1072#1075#1072#1094#1080#1085' '#1079#1072' '#1075#1086#1090#1086#1074#1080' '#1087#1088#1086#1080#1079#1074#1086#1076#1080
              DataBinding.FieldName = 'NAZIV_MAGACIN_GOTOVI_PROIZVODI'
              Width = 212
            end
            object cxGrid1DBTableView1PRIORITET: TcxGridDBColumn
              Caption = #1055#1088#1080#1086#1088#1080#1090#1077#1090
              DataBinding.FieldName = 'PRIORITET'
              Visible = False
            end
            object cxGrid1DBTableView1DATUM_PLANIRAN_KRAJ: TcxGridDBColumn
              Caption = #1055#1083#1072#1085#1080#1088#1072#1085' '#1076#1072#1090#1091#1084' '#1079#1072' '#1082#1088#1072#1112
              DataBinding.FieldName = 'DATUM_PLANIRAN_KRAJ'
            end
            object cxGrid1DBTableView1DATUM_KRAJ: TcxGridDBColumn
              Caption = #1050#1088#1072#1077#1085' '#1076#1072#1090#1091#1084
              DataBinding.FieldName = 'DATUM_KRAJ'
              Width = 98
            end
            object cxGrid1DBTableView1BR_CASOVI: TcxGridDBColumn
              Caption = #1042#1082#1091#1087#1077#1085' '#1073#1088#1086#1112' '#1085#1072' '#1095#1072#1089#1086#1074#1080
              DataBinding.FieldName = 'BR_CASOVI'
              Visible = False
            end
            object cxGrid1DBTableView1BR_CIKLUSI: TcxGridDBColumn
              Caption = #1042#1082#1091#1087#1077#1085' '#1073#1088#1086#1112' '#1085#1072' '#1094#1080#1082#1083#1091#1089#1080
              DataBinding.FieldName = 'BR_CIKLUSI'
              Visible = False
            end
            object cxGrid1DBTableView1GODINA: TcxGridDBColumn
              Caption = #1043#1086#1076#1080#1085#1072
              DataBinding.FieldName = 'GODINA'
              Visible = False
            end
            object cxGrid1DBTableView1STATUS_STAVKI: TcxGridDBColumn
              Caption = #1057#1090#1072#1090#1091#1089' '#1089#1090#1072#1074#1082#1080
              DataBinding.FieldName = 'STATUS_STAVKI'
              Visible = False
            end
          end
          object cxGrid1DBTableView2: TcxGridDBTableView
            Navigator.Buttons.CustomButtons = <>
            DataController.DataSource = dm.dsZavisniRN
            DataController.Summary.DefaultGroupSummaryItems = <>
            DataController.Summary.FooterSummaryItems = <>
            DataController.Summary.SummaryGroups = <>
            OptionsCustomize.ColumnsQuickCustomization = True
            OptionsData.Deleting = False
            OptionsData.Editing = False
            OptionsData.Inserting = False
            OptionsView.GroupByBox = False
            object cxGrid1DBTableView2BROJ: TcxGridDBColumn
              Caption = #1041#1088#1086#1112
              DataBinding.FieldName = 'BROJ'
              Width = 109
            end
            object cxGrid1DBTableView2GODINA: TcxGridDBColumn
              Caption = #1043#1086#1076#1080#1085#1072
              DataBinding.FieldName = 'GODINA'
              Visible = False
              Width = 74
            end
            object cxGrid1DBTableView2DATUM_PLANIRAN_POCETOK: TcxGridDBColumn
              Caption = #1044#1072#1090#1091#1084
              DataBinding.FieldName = 'DATUM_PLANIRAN_POCETOK'
              Width = 348
            end
          end
          object cxGrid1DBTableView3: TcxGridDBTableView
            Navigator.Buttons.CustomButtons = <>
            DataController.DataSource = dm.dsRNSD
            DataController.DetailKeyFieldNames = 'SOURCE_DOKUMENT'
            DataController.KeyFieldNames = 'ID'
            DataController.MasterKeyFieldNames = 'ID'
            DataController.Summary.DefaultGroupSummaryItems = <>
            DataController.Summary.FooterSummaryItems = <>
            DataController.Summary.SummaryGroups = <>
            OptionsCustomize.ColumnsQuickCustomization = True
            OptionsData.Deleting = False
            OptionsData.Editing = False
            OptionsData.Inserting = False
            OptionsView.GroupByBox = False
            object cxGrid1DBTableView3BOJA: TcxGridDBColumn
              DataBinding.FieldName = 'BOJA'
              PropertiesClassName = 'TcxColorComboBoxProperties'
              Properties.CustomColors = <>
              Properties.ShowDescriptions = False
              IsCaptionAssigned = True
            end
            object cxGrid1DBTableView3NAZIV_STATUS: TcxGridDBColumn
              Caption = #1057#1090#1072#1090#1091#1089
              DataBinding.FieldName = 'NAZIV_STATUS'
              Width = 79
            end
            object cxGrid1DBTableView3BROJ: TcxGridDBColumn
              DataBinding.FieldName = 'BROJ'
              Width = 70
            end
            object cxGrid1DBTableView3GODINA: TcxGridDBColumn
              Caption = #1043#1086#1076#1080#1085#1072
              DataBinding.FieldName = 'GODINA'
              Width = 73
            end
            object cxGrid1DBTableView3DATUM_PLANIRAN_POCETOK: TcxGridDBColumn
              Caption = #1044#1072#1090#1091#1084
              DataBinding.FieldName = 'DATUM_PLANIRAN_POCETOK'
              Width = 177
            end
          end
          object cxGrid1Level1: TcxGridLevel
            Caption = ' '#1056#1072#1073#1086#1090#1085#1080' '#1085#1072#1083#1086#1079#1080' '
            GridView = cxGrid1DBTableView1
            Options.DetailTabsPosition = dtpTop
            object cxGrid1Level2: TcxGridLevel
              Caption = #1047#1072#1074#1080#1089#1085#1080' '#1056#1072#1073#1086#1090#1085#1080' '#1085#1072#1083#1086#1079#1080
              GridView = cxGrid1DBTableView3
            end
          end
        end
      end
    end
    object tsRN: TcxTabSheet
      Caption = ' '#1056#1072#1073#1086#1090#1077#1085' '#1085#1072#1083#1086#1075' '
      ImageIndex = 1
      object dPanel: TPanel
        Left = 0
        Top = 0
        Width = 851
        Height = 401
        Align = alTop
        TabOrder = 0
        object gbRabotenNalog: TcxGroupBox
          Left = 1
          Top = 1
          Align = alTop
          Caption = '  '#1056#1040#1041#1054#1058#1045#1053' '#1053#1040#1051#1054#1043'  '
          Enabled = False
          TabOrder = 0
          DesignSize = (
            849
            360)
          Height = 360
          Width = 849
          object cxLabel15: TcxLabel
            Left = 27
            Top = 364
            Caption = #1042#1082#1091#1087#1077#1085' '#1073#1088#1086#1112' '#1085#1072' '#1094#1080#1082#1083#1091#1089#1080
            Style.TextColor = clNavy
            Properties.Alignment.Horz = taRightJustify
            Properties.WordWrap = True
            Transparent = True
            Visible = False
            Width = 82
            AnchorX = 109
          end
          object txtBrCiklusi: TcxDBTextEdit
            Left = 156
            Top = 364
            DataBinding.DataField = 'BR_CIKLUSI'
            DataBinding.DataSource = dm.dsRabotenNalog
            TabOrder = 20
            Visible = False
            OnEnter = cxDBTextEditAllEnter
            OnExit = cxDBTextEditAllExit
            OnKeyDown = EnterKakoTab
            Width = 61
          end
          object Број: TcxLabel
            Left = 34
            Top = 49
            AutoSize = False
            Caption = #1041#1088#1086#1112
            ParentFont = False
            Style.Font.Charset = RUSSIAN_CHARSET
            Style.Font.Color = clWindowText
            Style.Font.Height = -12
            Style.Font.Name = 'Tahoma'
            Style.Font.Style = []
            Style.TextColor = clRed
            Style.IsFontAssigned = True
            Properties.Alignment.Horz = taRightJustify
            Transparent = True
            Height = 21
            Width = 100
            AnchorX = 134
          end
          object txtBroj: TcxDBTextEdit
            Tag = 1
            Left = 141
            Top = 48
            DataBinding.DataField = 'BROJ'
            DataBinding.DataSource = dm.dsRabotenNalog
            TabOrder = 5
            OnEnter = cxDBTextEditAllEnter
            OnExit = cxDBTextEditAllExit
            OnKeyDown = EnterKakoTab
            Width = 120
          end
          object cxLabel2: TcxLabel
            Left = 34
            Top = 75
            AutoSize = False
            Caption = #1056#1072#1073#1086#1090#1085#1072' '#1077#1076#1080#1085#1080#1094#1072
            Style.TextColor = clRed
            Properties.Alignment.Horz = taRightJustify
            Transparent = True
            Height = 17
            Width = 100
            AnchorX = 134
          end
          object txtRE: TcxDBTextEdit
            Tag = 1
            Left = 141
            Top = 74
            DataBinding.DataField = 'ID_RE'
            DataBinding.DataSource = dm.dsRabotenNalog
            TabOrder = 8
            OnEnter = cxDBTextEditAllEnter
            OnExit = cxDBTextEditAllExit
            OnKeyDown = EnterKakoTab
            Width = 63
          end
          object cxLabel1: TcxLabel
            Left = 297
            Top = 44
            AutoSize = False
            Caption = #1055#1086#1088#1072#1095#1082#1072
            Style.TextColor = clNavy
            Properties.Alignment.Horz = taRightJustify
            Transparent = True
            Visible = False
            Height = 17
            Width = 98
            AnchorX = 395
          end
          object txtPoracka: TcxDBTextEdit
            Left = 404
            Top = 43
            DataBinding.DataField = 'ID_PORACKA'
            DataBinding.DataSource = dm.dsRabotenNalog
            TabOrder = 3
            Visible = False
            OnEnter = cxDBTextEditAllEnter
            OnExit = cxDBTextEditAllExit
            OnKeyDown = EnterKakoTab
            Width = 81
          end
          object cxLabel12: TcxLabel
            Left = 51
            Top = 368
            AutoSize = False
            Caption = #1055#1088#1080#1086#1080#1088#1080#1090#1077#1090
            Style.TextColor = clRed
            Properties.Alignment.Horz = taRightJustify
            Transparent = True
            Visible = False
            Height = 17
            Width = 100
            AnchorX = 151
          end
          object cbRE: TcxDBLookupComboBox
            Left = 203
            Top = 74
            DataBinding.DataField = 'ID_RE'
            DataBinding.DataSource = dm.dsRabotenNalog
            Properties.DropDownAutoSize = True
            Properties.DropDownSizeable = True
            Properties.KeyFieldNames = 'ID'
            Properties.ListColumns = <
              item
                Width = 20
                FieldName = 'ID'
              end
              item
                Width = 100
                FieldName = 'naziv'
              end>
            Properties.ListFieldIndex = 1
            Properties.ListSource = dsRE
            TabOrder = 9
            OnEnter = cxDBTextEditAllEnter
            OnExit = cxDBTextEditAllExit
            OnKeyDown = EnterKakoTab
            Width = 240
          end
          object cxGroupBox8: TcxGroupBox
            Left = 598
            Top = 106
            Caption = '  '#1056#1077#1072#1083#1080#1079#1080#1088#1072#1085' '#1076#1072#1090#1091#1084'  '
            Style.TextColor = clNavy
            TabOrder = 15
            Visible = False
            Height = 85
            Width = 216
            object cxLabel16: TcxLabel
              Left = 6
              Top = 23
              AutoSize = False
              Caption = #1055#1086#1095#1077#1090#1086#1082
              Style.TextColor = clNavy
              Properties.Alignment.Horz = taRightJustify
              Properties.WordWrap = True
              Transparent = True
              Height = 21
              Width = 60
              AnchorX = 66
            end
            object cxLabel17: TcxLabel
              Left = 40
              Top = 50
              Caption = #1050#1088#1072#1112
              Style.TextColor = clNavy
              Properties.Alignment.Horz = taRightJustify
              Properties.WordWrap = True
              Transparent = True
              Width = 26
              AnchorX = 66
            end
            object txtRealDatumPoc: TcxDBDateEdit
              Left = 72
              Top = 22
              DataBinding.DataField = 'DATUM_START'
              DataBinding.DataSource = dm.dsRabotenNalog
              Properties.Kind = ckDateTime
              TabOrder = 0
              OnEnter = cxDBTextEditAllEnter
              OnExit = cxDBTextEditAllExit
              OnKeyDown = EnterKakoTab
              Width = 130
            end
            object txtRealDatumKraj: TcxDBDateEdit
              Left = 72
              Top = 49
              DataBinding.DataField = 'DATUM_KRAJ'
              DataBinding.DataSource = dm.dsRabotenNalog
              TabOrder = 2
              OnEnter = cxDBTextEditAllEnter
              OnExit = cxDBTextEditAllExit
              OnKeyDown = EnterKakoTab
              Width = 130
            end
          end
          object cxDBImageComboBox1: TcxDBImageComboBox
            Left = 141
            Top = 15
            DataBinding.DataField = 'STATUS'
            DataBinding.DataSource = dm.dsRabotenNalog
            Properties.Images = dmRes.cxImageGrid
            Properties.Items = <
              item
                Description = #1055#1051#1040#1053
                ImageIndex = 4
                Value = 1
              end>
            Properties.ReadOnly = True
            Style.BorderStyle = ebsNone
            Style.Color = clBtnFace
            Style.LookAndFeel.NativeStyle = False
            Style.TransparentBorder = True
            Style.ButtonStyle = btsDefault
            Style.ButtonTransparency = ebtHideInactive
            StyleDisabled.LookAndFeel.NativeStyle = False
            StyleFocused.LookAndFeel.NativeStyle = False
            StyleHot.LookAndFeel.NativeStyle = False
            TabOrder = 0
            Visible = False
            Width = 140
          end
          object txtIzvorenDok: TcxDBTextEdit
            Left = 470
            Top = 15
            Hint = #1057#1086' '#1082#1086#1112' '#1056#1053' ('#1055#1086#1088#1072#1095#1082#1072') '#1077' '#1087#1086#1074#1088#1079#1072#1085' '#1056#1053
            DataBinding.DataField = 'SOURCE_DOKUMENT'
            DataBinding.DataSource = dm.dsRabotenNalog
            TabOrder = 1
            Visible = False
            OnEnter = cxDBTextEditAllEnter
            OnExit = cxDBTextEditAllExit
            OnKeyDown = EnterKakoTab
            Width = 60
          end
          object cxLabel41: TcxLabel
            Left = 267
            Top = 49
            AutoSize = False
            Caption = '/'
            Transparent = True
            Height = 17
            Width = 14
          end
          object lblGodina: TcxDBLabel
            Left = 281
            Top = 49
            DataBinding.DataField = 'GODINA'
            DataBinding.DataSource = dm.dsRabotenNalog
            Properties.Alignment.Horz = taLeftJustify
            Transparent = True
            Height = 21
            Width = 47
          end
          object cxLabel45: TcxLabel
            Left = 568
            Top = 84
            AutoSize = False
            Caption = #1057#1090#1072#1090#1091#1089
            Style.TextColor = clRed
            Properties.Alignment.Horz = taRightJustify
            Transparent = True
            Visible = False
            Height = 17
            Width = 46
            AnchorX = 614
          end
          object txtStatus: TcxDBTextEdit
            Tag = 1
            Left = 621
            Top = 83
            DataBinding.DataField = 'STATUS'
            DataBinding.DataSource = dm.dsRabotenNalog
            TabOrder = 11
            Visible = False
            OnEnter = cxDBTextEditAllEnter
            OnExit = cxDBTextEditAllExit
            OnKeyDown = EnterKakoTab
            Width = 63
          end
          object cbStatus: TcxDBLookupComboBox
            Left = 683
            Top = 83
            DataBinding.DataField = 'STATUS'
            DataBinding.DataSource = dm.dsRabotenNalog
            Properties.DropDownAutoSize = True
            Properties.DropDownSizeable = True
            Properties.KeyFieldNames = 'VREDNOST'
            Properties.ListColumns = <
              item
                Width = 20
                FieldName = 'vrednost'
              end
              item
                Width = 100
                FieldName = 'naziv'
              end>
            Properties.ListFieldIndex = 1
            Properties.ListSource = dm.dsMatStatus
            TabOrder = 12
            Visible = False
            OnEnter = cxDBTextEditAllEnter
            OnExit = cxDBTextEditAllExit
            OnKeyDown = EnterKakoTab
            Width = 136
          end
          object cxDBColorComboBox1: TcxDBColorComboBox
            Left = 2
            Top = 18
            Align = alTop
            AutoSize = False
            DataBinding.DataField = 'BOJA'
            DataBinding.DataSource = dm.dsRabotenNalog
            Properties.CustomColors = <>
            Properties.ReadOnly = True
            Properties.ShowDescriptions = False
            Style.BorderStyle = ebsNone
            Style.ButtonTransparency = ebtHideInactive
            TabOrder = 2
            Height = 16
            Width = 845
          end
          object cxGroupBox7: TcxGroupBox
            Left = 34
            Top = 104
            Caption = ' '#1055#1083#1072#1085#1080#1088#1072#1085' '#1044#1072#1090#1091#1084'  '
            Style.TextColor = clNavy
            TabOrder = 14
            Height = 57
            Width = 537
            object cxLabel6: TcxLabel
              Left = 315
              Top = 25
              Caption = #1044#1086
              Style.TextColor = clNavy
              Properties.Alignment.Horz = taRightJustify
              Properties.WordWrap = True
              Transparent = True
              Width = 18
              AnchorX = 333
            end
            object txtPlanDatumKraj: TcxDBDateEdit
              Left = 337
              Top = 25
              DataBinding.DataField = 'DATUM_PLANIRAN_KRAJ'
              DataBinding.DataSource = dm.dsRabotenNalog
              TabOrder = 3
              OnEnter = cxDBTextEditAllEnter
              OnExit = cxDBTextEditAllExit
              OnKeyDown = EnterKakoTab
              Width = 160
            end
            object cxLabel3: TcxLabel
              Left = 66
              Top = 25
              AutoSize = False
              Caption = #1054#1076
              Style.TextColor = clRed
              Properties.Alignment.Horz = taRightJustify
              Properties.WordWrap = True
              Transparent = True
              Height = 21
              Width = 31
              AnchorX = 97
            end
            object txtPlanDatumPoc: TcxDBDateEdit
              Tag = 1
              Left = 108
              Top = 24
              DataBinding.DataField = 'DATUM_PLANIRAN_POCETOK'
              DataBinding.DataSource = dm.dsRabotenNalog
              Properties.Kind = ckDateTime
              TabOrder = 0
              OnEnter = cxDBTextEditAllEnter
              OnExit = cxDBTextEditAllExit
              OnKeyDown = EnterKakoTab
              Width = 160
            end
          end
          object PanelInformacii: TPanel
            Left = 7
            Top = 207
            Width = 839
            Height = 146
            Anchors = [akLeft, akTop, akRight]
            BevelOuter = bvNone
            Color = cl3DLight
            Enabled = False
            ParentBackground = False
            TabOrder = 17
            object cxLabel36: TcxLabel
              Left = 3
              Top = 14
              AutoSize = False
              Caption = #1047#1072' '#1055#1072#1088#1090#1085#1077#1088' '
              ParentFont = False
              Style.Font.Charset = RUSSIAN_CHARSET
              Style.Font.Color = clWindowText
              Style.Font.Height = -11
              Style.Font.Name = 'Tahoma'
              Style.Font.Style = []
              Style.TextColor = clNavy
              Style.IsFontAssigned = True
              Properties.Alignment.Horz = taRightJustify
              Properties.WordWrap = True
              Transparent = True
              Height = 18
              Width = 125
              AnchorX = 128
            end
            object txtTipPartner: TcxDBTextEdit
              Left = 140
              Top = 13
              Hint = #1058#1080#1087' '#1085#1072' '#1055#1072#1088#1090#1085#1077#1088
              DataBinding.DataField = 'TP_PORACKA'
              DataBinding.DataSource = dm.dsRabotenNalog
              ParentFont = False
              TabOrder = 0
              OnEnter = cxDBTextEditAllEnter
              OnExit = cxDBTextEditAllExit
              OnKeyDown = EnterKakoTab
              Width = 63
            end
            object txtPartner: TcxDBTextEdit
              Left = 202
              Top = 13
              Hint = #1064#1080#1092#1088#1072' '#1085#1072' '#1055#1072#1088#1090#1085#1077#1088
              DataBinding.DataField = 'P_PORACKA'
              DataBinding.DataSource = dm.dsRabotenNalog
              ParentFont = False
              TabOrder = 1
              OnEnter = cxDBTextEditAllEnter
              OnExit = cxDBTextEditAllExit
              OnKeyDown = EnterKakoTab
              Width = 58
            end
            object cbNazivPartner: TcxLookupComboBox
              Left = 259
              Top = 13
              Hint = #1053#1072#1079#1080#1074' '#1085#1072' '#1055#1072#1088#1090#1085#1077#1088
              Properties.ClearKey = 46
              Properties.DropDownAutoSize = True
              Properties.DropDownSizeable = True
              Properties.KeyFieldNames = 'tip_partner;id'
              Properties.ListColumns = <
                item
                  Width = 40
                  FieldName = 'TIP_PARTNER'
                end
                item
                  Width = 60
                  FieldName = 'id'
                end
                item
                  Width = 170
                  FieldName = 'naziv'
                end>
              Properties.ListFieldIndex = 2
              Properties.ListOptions.SyncMode = True
              Properties.ListSource = dmMat.dsPartner
              TabOrder = 2
              OnEnter = cxDBTextEditAllEnter
              OnExit = cxDBTextEditAllExit
              OnKeyDown = EnterKakoTab
              Width = 309
            end
            object cxLabel5: TcxLabel
              Left = 3
              Top = 42
              AutoSize = False
              Caption = #1048#1079#1074#1086#1088#1077#1085' '#1076#1086#1082#1091#1084#1077#1085#1090
              Style.TextColor = clNavy
              Properties.Alignment.Horz = taRightJustify
              Transparent = True
              Height = 17
              Width = 125
              AnchorX = 128
            end
            object cbIzvorenDokument: TcxDBLookupComboBox
              Left = 140
              Top = 41
              Hint = #1057#1086' '#1082#1086#1112' '#1056#1053' ('#1055#1086#1088#1072#1095#1082#1072') '#1077' '#1087#1086#1074#1088#1079#1072#1085' '#1056#1053
              DataBinding.DataField = 'SOURCE_DOKUMENT'
              DataBinding.DataSource = dm.dsRabotenNalog
              Properties.DropDownAutoSize = True
              Properties.DropDownSizeable = True
              Properties.KeyFieldNames = 'ID'
              Properties.ListColumns = <
                item
                  FieldName = 'ID'
                end
                item
                  FieldName = 'broj'
                end>
              Properties.ListFieldIndex = 1
              Properties.ListSource = dm.dsIzberiRN
              TabOrder = 4
              OnEnter = cxDBTextEditAllEnter
              OnExit = cxDBTextEditAllExit
              OnKeyDown = EnterKakoTab
              Width = 428
            end
            object cxLabel10: TcxLabel
              Left = 3
              Top = 69
              AutoSize = False
              Caption = #1052#1072#1075#1072#1094#1080#1085' '#1079#1072' '#1089#1091#1088#1086#1074#1080#1085#1080
              Style.TextColor = clNavy
              Properties.Alignment.Horz = taRightJustify
              Properties.WordWrap = True
              Transparent = True
              Height = 30
              Width = 125
              AnchorX = 128
            end
            object txtMagacinS: TcxDBTextEdit
              Left = 140
              Top = 68
              DataBinding.DataField = 'ID_RE_SUROVINI'
              DataBinding.DataSource = dm.dsRabotenNalog
              TabOrder = 6
              OnEnter = cxDBTextEditAllEnter
              OnExit = cxDBTextEditAllExit
              OnKeyDown = EnterKakoTab
              Width = 63
            end
            object cbMagacinS: TcxDBLookupComboBox
              Left = 202
              Top = 68
              DataBinding.DataField = 'ID_RE_SUROVINI'
              DataBinding.DataSource = dm.dsRabotenNalog
              Properties.DropDownSizeable = True
              Properties.KeyFieldNames = 'ID'
              Properties.ListColumns = <
                item
                  FieldName = 'ID'
                end
                item
                  FieldName = 'naziv'
                end>
              Properties.ListFieldIndex = 1
              Properties.ListSource = dm.dsMagacin
              TabOrder = 7
              OnEnter = cxDBTextEditAllEnter
              OnExit = cxDBTextEditAllExit
              OnKeyDown = EnterKakoTab
              Width = 368
            end
            object cxLabel13: TcxLabel
              Left = 3
              Top = 92
              AutoSize = False
              Caption = #1052#1072#1075#1072#1094#1080#1085' '#1079#1072' '#1075#1086#1090#1086#1074#1080' '#1087#1088#1086#1080#1079#1074#1086#1076#1080
              Style.TextColor = clNavy
              Properties.Alignment.Horz = taRightJustify
              Properties.WordWrap = True
              Transparent = True
              Height = 30
              Width = 125
              AnchorX = 128
            end
            object txtMagacinGP: TcxDBTextEdit
              Left = 140
              Top = 95
              DataBinding.DataField = 'ID_RE_GOTOV_PROIZVOD'
              DataBinding.DataSource = dm.dsRabotenNalog
              TabOrder = 10
              OnEnter = cxDBTextEditAllEnter
              OnExit = cxDBTextEditAllExit
              OnKeyDown = EnterKakoTab
              Width = 63
            end
            object cbMagacinGP: TcxDBLookupComboBox
              Left = 202
              Top = 95
              DataBinding.DataField = 'ID_RE_GOTOV_PROIZVOD'
              DataBinding.DataSource = dm.dsRabotenNalog
              Properties.DropDownAutoSize = True
              Properties.DropDownSizeable = True
              Properties.KeyFieldNames = 'ID'
              Properties.ListColumns = <
                item
                  Width = 30
                  FieldName = 'ID'
                end
                item
                  Width = 150
                  FieldName = 'naziv'
                end>
              Properties.ListFieldIndex = 1
              Properties.ListSource = dm.dsMagacin
              TabOrder = 11
              OnEnter = cxDBTextEditAllEnter
              OnExit = cxDBTextEditAllExit
              OnKeyDown = EnterKakoTab
              Width = 368
            end
            object cxLabel38: TcxLabel
              Left = 3
              Top = 122
              AutoSize = False
              Caption = #1055#1088#1080#1086#1088#1080#1090#1077#1090
              Style.TextColor = clNavy
              Properties.Alignment.Horz = taRightJustify
              Transparent = True
              Height = 17
              Width = 125
              AnchorX = 128
            end
            object txtPrioritet: TcxDBTextEdit
              Left = 140
              Top = 121
              DataBinding.DataField = 'ID_PRIORITET'
              DataBinding.DataSource = dm.dsRabotenNalog
              TabOrder = 12
              OnEnter = cxDBTextEditAllEnter
              OnExit = cxDBTextEditAllExit
              OnKeyDown = EnterKakoTab
              Width = 63
            end
            object cbPrioritet: TcxDBLookupComboBox
              Left = 202
              Top = 121
              DataBinding.DataField = 'ID_PRIORITET'
              DataBinding.DataSource = dm.dsRabotenNalog
              Properties.DropDownAutoSize = True
              Properties.DropDownSizeable = True
              Properties.KeyFieldNames = 'ID'
              Properties.ListColumns = <
                item
                  FieldName = 'ID'
                end
                item
                  FieldName = 'naziv'
                end>
              Properties.ListFieldIndex = 1
              Properties.ListSource = dm.dsPrioritet
              TabOrder = 13
              OnEnter = cxDBTextEditAllEnter
              OnExit = cxDBTextEditAllExit
              OnKeyDown = EnterKakoTab
              Width = 136
            end
          end
          object cbInformacii: TcxCheckBox
            Left = 27
            Top = 180
            Caption = ' '#1044#1086#1087#1086#1083#1085#1080#1090#1077#1083#1085#1080' '#1080#1085#1092#1086#1088#1084#1072#1094#1080#1080'  >>'
            Properties.NullStyle = nssUnchecked
            TabOrder = 16
            Transparent = True
            OnClick = cbInformaciiClick
            OnEnter = cxDBTextEditAllEnter
            OnExit = cxDBTextEditAllExit
            OnKeyDown = EnterKakoTab
            Width = 188
          end
        end
      end
      object Panel1: TPanel
        Left = 0
        Top = 401
        Width = 851
        Height = 158
        Align = alClient
        BevelOuter = bvNone
        TabOrder = 1
        object cxDBNavigator1: TcxDBNavigator
          Left = 0
          Top = 0
          Width = 847
          Height = 26
          Buttons.CustomButtons = <>
          Buttons.Insert.Visible = False
          Buttons.Append.Visible = False
          Buttons.Delete.Visible = False
          Buttons.Edit.Visible = False
          Buttons.Post.Visible = False
          Buttons.Cancel.Visible = False
          Buttons.Refresh.Visible = False
          Buttons.SaveBookmark.Visible = False
          Buttons.GotoBookmark.Visible = False
          Buttons.Filter.Visible = False
          DataSource = dm.dsRabotenNalog
          InfoPanel.Font.Charset = RUSSIAN_CHARSET
          InfoPanel.Font.Color = clWindowText
          InfoPanel.Font.Height = -12
          InfoPanel.Font.Name = 'Tahoma'
          InfoPanel.Font.Style = []
          InfoPanel.ParentFont = False
          InfoPanel.Visible = True
          Align = alTop
          TabOrder = 0
        end
        object cxGroupBox4: TcxGroupBox
          Left = 0
          Top = 26
          Align = alClient
          TabOrder = 1
          DesignSize = (
            851
            132)
          Height = 132
          Width = 851
          object btStavki: TcxButton
            Left = 671
            Top = 22
            Width = 155
            Height = 23
            Anchors = [akRight, akBottom]
            Caption = #1057#1058#1040#1042#1050#1048'  >>'
            OptionsImage.ImageIndex = 8
            OptionsImage.Images = dmRes.cxSmallImages
            TabOrder = 0
            Font.Charset = RUSSIAN_CHARSET
            Font.Color = clWindowText
            Font.Height = -13
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            OnClick = btStavkiClick
          end
        end
      end
    end
    object tsStavki: TcxTabSheet
      Caption = #1057#1090#1072#1074#1082#1080' '#1074#1086' '#1056#1053'  '
      ImageIndex = 2
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object dPanelO: TPanel
        Left = 0
        Top = 0
        Width = 851
        Height = 356
        Align = alClient
        TabOrder = 0
        object cxGrid3: TcxGrid
          Left = 1
          Top = 1
          Width = 849
          Height = 354
          Align = alClient
          Images = dmRes.cxImageGrid
          PopupMenu = PopupMenu1
          TabOrder = 0
          LookAndFeel.NativeStyle = False
          RootLevelOptions.DetailTabsPosition = dtpTop
          OnFocusedViewChanged = cxGrid3FocusedViewChanged
          OnLayoutChanged = cxGrid3LayoutChanged
          object cxGrid3DBTableView1: TcxGridDBTableView
            Navigator.Buttons.CustomButtons = <>
            Navigator.Buttons.First.Visible = True
            Navigator.Buttons.Last.Visible = True
            Navigator.Buttons.Insert.Visible = False
            Navigator.Buttons.Delete.Visible = False
            Navigator.Buttons.Edit.Visible = False
            Navigator.Buttons.Post.Visible = False
            Navigator.Buttons.Refresh.Visible = False
            Navigator.Buttons.SaveBookmark.Visible = False
            Navigator.Buttons.GotoBookmark.Visible = False
            Navigator.Buttons.Filter.Visible = False
            Navigator.InfoPanel.Visible = True
            Navigator.Visible = True
            OnFocusedRecordChanged = cxGrid3DBTableView1FocusedRecordChanged
            DataController.DataSource = dm.dsRNStavka
            DataController.Summary.DefaultGroupSummaryItems = <>
            DataController.Summary.FooterSummaryItems = <>
            DataController.Summary.SummaryGroups = <>
            FilterRow.InfoText = #1050#1083#1080#1082#1085#1077#1090#1077' '#1090#1091#1082#1072' '#1079#1072' '#1076#1072' '#1076#1077#1092#1080#1085#1080#1088#1072#1090#1077' '#1092#1080#1083#1090#1077#1088
            FilterRow.Visible = True
            Images = dmRes.cxImageGrid
            OptionsBehavior.IncSearch = True
            OptionsCustomize.ColumnsQuickCustomization = True
            OptionsData.Deleting = False
            OptionsData.Inserting = False
            OptionsView.Footer = True
            object cxGrid3DBTableView1BOJA: TcxGridDBColumn
              DataBinding.FieldName = 'BOJA'
              PropertiesClassName = 'TcxColorComboBoxProperties'
              Properties.Alignment.Horz = taCenter
              Properties.CustomColors = <>
              Properties.DropDownSizeable = True
              Properties.NamingConvention = cxncNone
              Properties.ShowDescriptions = False
              Width = 45
              IsCaptionAssigned = True
            end
            object cxGrid3DBTableView1NAZIV_STATUS: TcxGridDBColumn
              Caption = #1057#1090#1072#1090#1091#1089
              DataBinding.FieldName = 'NAZIV_STATUS'
              Width = 51
            end
            object cxGrid3DBTableView1ID: TcxGridDBColumn
              Caption = #1064#1080#1092#1088#1072
              DataBinding.FieldName = 'ID'
              Visible = False
            end
            object cxGrid3DBTableView1LINIJA: TcxGridDBColumn
              Caption = #1051#1080#1085#1080#1112#1072
              DataBinding.FieldName = 'LINIJA'
              PropertiesClassName = 'TcxLookupComboBoxProperties'
              Properties.KeyFieldNames = 'ID'
              Properties.ListColumns = <
                item
                  FieldName = 'NAZIV'
                end>
              Properties.ListSource = dm.dsLinija
              HeaderImageIndex = 2
              Options.Editing = False
            end
            object cxGrid3DBTableView1STATUS_LINIJA: TcxGridDBColumn
              Caption = #1057#1090#1072#1090#1091#1089' '#1083#1080#1085#1080#1112#1072
              DataBinding.FieldName = 'STATUS_LINIJA'
              Options.Editing = False
            end
            object cxGrid3DBTableView1BROJ: TcxGridDBColumn
              Caption = #1041#1088#1086#1112
              DataBinding.FieldName = 'BROJ'
              Visible = False
              Width = 40
            end
            object cxGrid3DBTableView1ID_RABOTEN_NALOG: TcxGridDBColumn
              Caption = #1064#1080#1092#1088#1072' '#1085#1072' '#1056#1053
              DataBinding.FieldName = 'ID_RABOTEN_NALOG'
              Visible = False
            end
            object cxGrid3DBTableView1VID_ARTIKAL: TcxGridDBColumn
              Caption = #1042#1080#1076' '#1085#1072' '#1072#1088#1090#1080#1082#1072#1083
              DataBinding.FieldName = 'VID_ARTIKAL'
              Options.Editing = False
              Width = 54
            end
            object cxGrid3DBTableView1ARTIKAL: TcxGridDBColumn
              Caption = #1064#1080#1092#1088#1072' '#1085#1072' '#1072#1088#1090#1080#1082#1072#1083
              DataBinding.FieldName = 'ARTIKAL'
              Options.Editing = False
              Width = 46
            end
            object cxGrid3DBTableView1NAZIV_ARTIKAL: TcxGridDBColumn
              Caption = #1040#1088#1090#1080#1082#1072#1083
              DataBinding.FieldName = 'NAZIV_ARTIKAL'
              Options.Editing = False
              Width = 151
            end
            object cxGrid3DBTableView1KOLICINA: TcxGridDBColumn
              Caption = #1050#1086#1083#1080#1095#1080#1085#1072
              DataBinding.FieldName = 'KOLICINA'
              Options.Editing = False
              Width = 53
            end
            object cxGrid3DBTableView1MERKA_KOLICINA: TcxGridDBColumn
              Caption = #1052#1077#1088#1082#1072
              DataBinding.FieldName = 'MERKA_KOLICINA'
              Options.Editing = False
            end
            object cxGrid3DBTableView1KOLICINA_PAKUVANJE: TcxGridDBColumn
              Caption = #1050#1086#1083#1080#1095#1080#1085#1072' '#1085#1072' '#1087#1072#1082#1091#1074#1072#1114#1077
              DataBinding.FieldName = 'KOLICINA_PAKUVANJE'
              Options.Editing = False
              Width = 79
            end
            object cxGrid3DBTableView1MERKA: TcxGridDBColumn
              Caption = #1052#1077#1088#1082#1072' '#1082#1086#1083#1080#1095#1080#1085#1072
              DataBinding.FieldName = 'MERKA'
              Options.Editing = False
            end
            object cxGrid3DBTableView1NAZIV_MERKA: TcxGridDBColumn
              Caption = #1053#1072#1079#1080#1074' '#1084#1077#1088#1082#1072
              DataBinding.FieldName = 'NAZIV_MERKA'
              Visible = False
              Options.Editing = False
              Width = 70
            end
            object cxGrid3DBTableView1KOLICINA_BROJ: TcxGridDBColumn
              Caption = #1041#1088#1086#1112' '#1074#1086' '#1082#1086#1083#1080#1095#1080#1085#1072'/'#1096#1080#1096#1080#1114#1072
              DataBinding.FieldName = 'KOLICINA_BROJ'
              Visible = False
              Options.Editing = False
            end
            object cxGrid3DBTableView1CUSTOM1: TcxGridDBColumn
              Caption = 'custom1'
              DataBinding.FieldName = 'CUSTOM1'
              Visible = False
              Options.Editing = False
              Width = 136
            end
            object cxGrid3DBTableView1CUSTOM2: TcxGridDBColumn
              DataBinding.FieldName = 'CUSTOM2'
              Visible = False
              Options.Editing = False
              Width = 144
            end
            object cxGrid3DBTableView1CUSTOM3: TcxGridDBColumn
              DataBinding.FieldName = 'CUSTOM3'
              Visible = False
              Options.Editing = False
              Width = 148
            end
            object cxGrid3DBTableView1SLIKA: TcxGridDBColumn
              Caption = #1057#1083#1080#1082#1072
              DataBinding.FieldName = 'SLIKA'
              PropertiesClassName = 'TcxImageProperties'
              Properties.GraphicClassName = 'TdxSmartImage'
              Visible = False
              Options.Editing = False
              Width = 39
            end
            object cxGrid3DBTableView1OD_DATUM_PLANIRAN: TcxGridDBColumn
              Caption = #1054#1076' '#1087#1083#1072#1085#1080#1088#1072#1085' '#1076#1072#1090#1091#1084
              DataBinding.FieldName = 'OD_DATUM_PLANIRAN'
              Visible = False
              Options.Editing = False
            end
            object cxGrid3DBTableView1DO_DATUM_PLANIRAN: TcxGridDBColumn
              Caption = #1044#1086' '#1087#1083#1072#1085#1080#1088#1072#1085' '#1076#1072#1090#1091#1084
              DataBinding.FieldName = 'DO_DATUM_PLANIRAN'
              Visible = False
              Options.Editing = False
            end
            object cxGrid3DBTableView1OD_DATUM_REALIZACIJA: TcxGridDBColumn
              Caption = #1054#1076' '#1076#1072#1090#1091#1084' '#1085#1072' '#1088#1077#1072#1083#1080#1079#1072#1094#1080#1112#1072
              DataBinding.FieldName = 'OD_DATUM_REALIZACIJA'
              Visible = False
              Options.Editing = False
            end
            object cxGrid3DBTableView1DO_DATUM_REALIZACIJA: TcxGridDBColumn
              Caption = #1044#1086' '#1076#1072#1090#1091#1084' '#1085#1072' '#1088#1077#1072#1083#1080#1079#1072#1094#1080#1112#1072
              DataBinding.FieldName = 'DO_DATUM_REALIZACIJA'
              Visible = False
              Options.Editing = False
            end
            object cxGrid3DBTableView1ID_PORACKA: TcxGridDBColumn
              Caption = #1064#1080#1092#1088#1072' '#1085#1072' '#1087#1086#1088#1072#1095#1082#1072
              DataBinding.FieldName = 'ID_PORACKA'
              Visible = False
              Options.Editing = False
            end
            object cxGrid3DBTableView1DATUM_OD_ISPORAKA: TcxGridDBColumn
              Caption = #1054#1076' '#1076#1072#1090#1091#1084' '#1085#1072' '#1080#1089#1087#1086#1088#1072#1082#1072
              DataBinding.FieldName = 'DATUM_OD_ISPORAKA'
              Visible = False
              Options.Editing = False
            end
            object cxGrid3DBTableView1DATUM_DO_ISPORAKA: TcxGridDBColumn
              Caption = #1044#1086' '#1076#1072#1090#1091#1084' '#1085#1072' '#1080#1089#1087#1086#1088#1072#1082#1072
              DataBinding.FieldName = 'DATUM_DO_ISPORAKA'
              Visible = False
              Options.Editing = False
            end
            object cxGrid3DBTableView1ZABELESKA: TcxGridDBColumn
              Caption = #1047#1072#1073#1077#1083#1077#1096#1082#1072
              DataBinding.FieldName = 'ZABELESKA'
              PropertiesClassName = 'TcxBlobEditProperties'
              Properties.BlobEditKind = bekMemo
              Properties.BlobPaintStyle = bpsText
              Properties.ReadOnly = True
              HeaderGlyphAlignmentHorz = taRightJustify
              HeaderImageIndex = 6
              Width = 163
            end
            object cxGrid3DBTableView1KRAEN_DATUM_PREVOZ: TcxGridDBColumn
              Caption = #1050#1088#1072#1077#1085' '#1076#1072#1090#1091#1084' '#1085#1072' '#1087#1088#1077#1074#1086#1079
              DataBinding.FieldName = 'KRAEN_DATUM_PREVOZ'
              Visible = False
              Options.Editing = False
            end
            object cxGrid3DBTableView1PRIORITET: TcxGridDBColumn
              Caption = #1055#1088#1080#1086#1088#1080#1090#1077#1090
              DataBinding.FieldName = 'PRIORITET'
              Visible = False
              Options.Editing = False
            end
            object cxGrid3DBTableView1BROJ_CIKLUSI: TcxGridDBColumn
              Caption = #1041#1088#1086#1112' '#1085#1072' '#1094#1080#1082#1083#1091#1089#1080
              DataBinding.FieldName = 'BROJ_CIKLUSI'
              Visible = False
              Options.Editing = False
            end
            object cxGrid3DBTableView1BROJ_CASOVI: TcxGridDBColumn
              Caption = #1041#1088#1086#1112' '#1085#1072' '#1095#1072#1089#1086#1074#1080
              DataBinding.FieldName = 'BROJ_CASOVI'
              Visible = False
              Options.Editing = False
            end
            object cxGrid3DBTableView1TIP_ODOBRIL: TcxGridDBColumn
              DataBinding.FieldName = 'TIP_ODOBRIL'
              Visible = False
              Options.Editing = False
            end
            object cxGrid3DBTableView1ODOBRIL: TcxGridDBColumn
              DataBinding.FieldName = 'ODOBRIL'
              Visible = False
              Options.Editing = False
            end
            object cxGrid3DBTableView1NAZIV_ODOBRIL: TcxGridDBColumn
              Caption = #1054#1076#1086#1073#1088#1080#1083
              DataBinding.FieldName = 'NAZIV_ODOBRIL'
              Options.Editing = False
              Width = 150
            end
            object cxGrid3DBTableView1SERISKI_BROJ: TcxGridDBColumn
              Caption = #1057#1077#1088#1080#1112#1072
              DataBinding.FieldName = 'SERISKI_BROJ'
              Options.Editing = False
              Width = 156
            end
            object cxGrid3DBTableView1VID_AMBALAZA: TcxGridDBColumn
              Caption = #1042#1080#1076' '#1072#1084#1073#1072#1083#1072#1078#1072
              DataBinding.FieldName = 'VID_AMBALAZA'
              Options.Editing = False
            end
            object cxGrid3DBTableView1AMBALAZA: TcxGridDBColumn
              Caption = #1040#1084#1073#1072#1083#1072#1078#1072
              DataBinding.FieldName = 'AMBALAZA'
              Options.Editing = False
            end
            object cxGrid3DBTableView1NAZIV_AMBALAZA: TcxGridDBColumn
              Caption = #1040#1084#1073#1072#1083#1072#1078#1072
              DataBinding.FieldName = 'NAZIV_AMBALAZA'
              Options.Editing = False
              Width = 159
            end
            object cxGrid3DBTableView1ID_PAKUVANJE: TcxGridDBColumn
              Caption = #1064#1080#1092#1088#1072' '#1085#1072' '#1087#1072#1082#1091#1074#1072#1114#1077
              DataBinding.FieldName = 'ID_PAKUVANJE'
              Options.Editing = False
            end
            object cxGrid3DBTableView1STATUS: TcxGridDBColumn
              Caption = #1057#1090#1072#1090#1091#1089' '#1073#1088#1086#1112
              DataBinding.FieldName = 'STATUS'
              Visible = False
              Options.Editing = False
            end
            object cxGrid3DBTableView1VREDNOST: TcxGridDBColumn
              Caption = #1042#1088#1077#1076#1085#1086#1089#1090' '#1085#1072' '#1089#1090#1072#1090#1091#1089
              DataBinding.FieldName = 'VREDNOST'
              Visible = False
              Options.Editing = False
            end
            object cxGrid3DBTableView1LOT: TcxGridDBColumn
              DataBinding.FieldName = 'LOT'
              Options.Editing = False
              Width = 117
            end
            object cxGrid3DBTableView1KOLICINA_REALNA: TcxGridDBColumn
              DataBinding.FieldName = 'KOLICINA_REALNA'
              Visible = False
              Options.Editing = False
            end
            object cxGrid3DBTableView1ID_RN_OLD: TcxGridDBColumn
              DataBinding.FieldName = 'ID_RN_OLD'
              Visible = False
              Options.Editing = False
            end
            object cxGrid3DBTableView1ROK_UPOTREBA: TcxGridDBColumn
              DataBinding.FieldName = 'ROK_UPOTREBA'
              Visible = False
              Options.Editing = False
            end
            object cxGrid3DBTableView1ART_KOLICINA: TcxGridDBColumn
              DataBinding.FieldName = 'ART_KOLICINA'
              Visible = False
              Options.Editing = False
            end
            object cxGrid3DBTableView1ART_BROJ_PAK: TcxGridDBColumn
              DataBinding.FieldName = 'ART_BROJ_PAK'
              Visible = False
              Options.Editing = False
            end
            object cxGrid3DBTableView1ID_PORACKA_STAVKA: TcxGridDBColumn
              DataBinding.FieldName = 'ID_PORACKA_STAVKA'
              Visible = False
              Options.Editing = False
            end
          end
          object cxGrid3DBTableView2: TcxGridDBTableView
            Navigator.Buttons.CustomButtons = <>
            DataController.DataSource = dm.dspRNNormativ
            DataController.Summary.DefaultGroupSummaryItems = <>
            DataController.Summary.FooterSummaryItems = <>
            DataController.Summary.SummaryGroups = <>
            FilterRow.InfoText = #1050#1083#1080#1082#1085#1077#1090#1077' '#1090#1091#1082#1072' '#1079#1072' '#1076#1072' '#1076#1077#1092#1080#1085#1080#1088#1072#1090#1077' '#1092#1080#1083#1090#1077#1088
            FilterRow.Visible = True
            OptionsBehavior.IncSearch = True
            OptionsCustomize.ColumnsQuickCustomization = True
            OptionsData.Deleting = False
            OptionsData.Editing = False
            OptionsData.Inserting = False
            OptionsView.Indicator = True
            object cxGrid3DBTableView2VID_ARTIKAL_N: TcxGridDBColumn
              Caption = #1042#1080#1076' '#1085#1072' '#1072#1088#1090#1080#1082#1072#1083
              DataBinding.FieldName = 'VID_ARTIKAL_N'
            end
            object cxGrid3DBTableView2ARTIKAL_N: TcxGridDBColumn
              Caption = #1064#1080#1092#1088#1072' '#1085#1072' '#1072#1088#1090#1080#1082#1072#1083
              DataBinding.FieldName = 'ARTIKAL_N'
            end
            object cxGrid3DBTableView2NAZIV_ARTIKAL: TcxGridDBColumn
              Caption = #1040#1088#1090#1080#1082#1072#1083
              DataBinding.FieldName = 'NAZIV_ARTIKAL'
              Width = 316
            end
            object cxGrid3DBTableView2KOLICINA: TcxGridDBColumn
              Caption = #1050#1086#1083#1080#1095#1080#1085#1072
              DataBinding.FieldName = 'KOLICINA'
            end
            object cxGrid3DBTableView2MERKA: TcxGridDBColumn
              Caption = #1052#1077#1088#1082#1072
              DataBinding.FieldName = 'MERKA'
            end
          end
          object cxGrid3DBTableView3: TcxGridDBTableView
            Navigator.Buttons.CustomButtons = <>
            DataController.DataSource = dm.dspRNOperacija
            DataController.Summary.DefaultGroupSummaryItems = <>
            DataController.Summary.FooterSummaryItems = <>
            DataController.Summary.SummaryGroups = <>
            FilterRow.InfoText = #1050#1083#1080#1082#1085#1077#1090#1077' '#1090#1091#1082#1072' '#1079#1072' '#1076#1072' '#1076#1077#1092#1080#1085#1080#1088#1072#1090#1077' '#1092#1080#1083#1090#1077#1088
            FilterRow.Visible = True
            OptionsBehavior.IncSearch = True
            OptionsCustomize.ColumnsQuickCustomization = True
            OptionsData.Deleting = False
            OptionsData.Editing = False
            OptionsData.Inserting = False
            OptionsView.Indicator = True
            object cxGrid3DBTableView3OPERACIJA_N: TcxGridDBColumn
              Caption = #1064#1080#1092#1088#1072' '#1085#1072' '#1086#1087#1077#1088#1072#1094#1080#1112#1072
              DataBinding.FieldName = 'OPERACIJA_N'
              Width = 84
            end
            object cxGrid3DBTableView3NAZIV_OPERACIJA: TcxGridDBColumn
              Caption = #1054#1087#1077#1088#1072#1094#1080#1112#1072
              DataBinding.FieldName = 'NAZIV_OPERACIJA'
              Width = 293
            end
            object cxGrid3DBTableView3POTREBNO_VREME: TcxGridDBColumn
              Caption = #1055#1086#1090#1088#1086#1096#1077#1085#1086' '#1074#1088#1077#1084#1077
              DataBinding.FieldName = 'POTREBNO_VREME'
              Width = 81
            end
            object cxGrid3DBTableView3MERKA: TcxGridDBColumn
              Caption = #1052#1077#1088#1082#1072
              DataBinding.FieldName = 'MERKA'
              Width = 57
            end
            object cxGrid3DBTableView3SEKVENCA: TcxGridDBColumn
              Caption = #1057#1077#1082#1074#1077#1085#1094#1072
              DataBinding.FieldName = 'SEKVENCA'
            end
          end
          object cxGrid3Level1: TcxGridLevel
            Caption = #1056#1072#1073#1086#1090#1085#1080' '#1085#1072#1083#1086#1079#1080
            GridView = cxGrid3DBTableView1
          end
          object cxGrid3Level2: TcxGridLevel
            Caption = #1053#1086#1088#1084#1072#1090#1080#1074
            GridView = cxGrid3DBTableView2
          end
          object cxGrid3Level3: TcxGridLevel
            Caption = #1054#1087#1077#1088#1072#1094#1080#1080
            GridView = cxGrid3DBTableView3
          end
        end
      end
      object lPanelO: TPanel
        Left = 0
        Top = 356
        Width = 851
        Height = 203
        Align = alBottom
        TabOrder = 1
        object gbRNStavki: TcxGroupBox
          Left = 1
          Top = 1
          Align = alClient
          Enabled = False
          TabOrder = 0
          Height = 201
          Width = 849
          object lblCustom3: TLabel
            Left = 33
            Top = 270
            Width = 42
            Height = 13
            Alignment = taRightJustify
            Caption = 'Custom3'
            Font.Charset = RUSSIAN_CHARSET
            Font.Color = clBlue
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
            Visible = False
          end
          object Label3: TLabel
            Left = 352
            Top = 50
            Width = 56
            Height = 13
            Alignment = taRightJustify
            Caption = #1047#1072#1073#1077#1083#1077#1096#1082#1072
            Font.Charset = RUSSIAN_CHARSET
            Font.Color = clBlue
            Font.Height = -11
            Font.Name = 'Tahoma'
            Font.Style = []
            ParentFont = False
          end
          object cxLabel8: TcxLabel
            Left = 51
            Top = 19
            Caption = #1040#1088#1090#1080#1082#1072#1083' / '#1055#1088#1086#1080#1079#1074#1086#1076
            Style.TextColor = clRed
            Properties.Alignment.Horz = taRightJustify
            Properties.WordWrap = True
            Transparent = True
            Width = 57
            AnchorX = 108
          end
          object txtVidArtikal: TcxDBTextEdit
            Tag = 1
            Left = 120
            Top = 23
            Hint = #1042#1080#1076' '#1085#1072' '#1072#1088#1090#1080#1082#1072#1083
            DataBinding.DataField = 'VID_ARTIKAL'
            DataBinding.DataSource = dm.dsRNStavka
            ParentFont = False
            TabOrder = 3
            OnEnter = cxDBTextEditAllEnter
            OnExit = cxDBTextEditAllExit
            OnKeyDown = EnterKakoTab
            Width = 58
          end
          object txtArtikal: TcxDBTextEdit
            Tag = 1
            Left = 180
            Top = 23
            Hint = #1064#1080#1092#1088#1072' '#1085#1072' '#1072#1088#1090#1080#1082#1072#1083
            DataBinding.DataField = 'ARTIKAL'
            DataBinding.DataSource = dm.dsRNStavka
            ParentFont = False
            TabOrder = 4
            OnEnter = cxDBTextEditAllEnter
            OnExit = cxDBTextEditAllExit
            OnKeyDown = EnterKakoTab
            Width = 56
          end
          object cxLabel9: TcxLabel
            Left = 33
            Top = 59
            Hint = #1053#1077#1090#1086' '#1082#1086#1083#1080#1095#1080#1085#1072' '#1086#1076' '#1072#1088#1090#1080#1082#1083#1086#1090
            Caption = #1050#1086#1083#1080#1095#1080#1085#1072'/'#1085#1077#1090#1086
            Style.TextColor = clRed
            Properties.Alignment.Horz = taRightJustify
            Transparent = True
            AnchorX = 114
          end
          object txtKolicinaArtikal: TcxDBTextEdit
            Tag = 1
            Left = 120
            Top = 59
            Hint = #1050#1086#1083#1080#1095#1080#1085#1072' '#1082#1086#1112#1072' '#1090#1088#1077#1073#1072' '#1076#1072' '#1089#1077' '#1085#1072#1087#1088#1072#1074#1080' '#1087#1086' '#1056#1053
            DataBinding.DataField = 'KOLICINA'
            DataBinding.DataSource = dm.dsRNStavka
            ParentFont = False
            Style.BorderStyle = ebsUltraFlat
            Style.Font.Charset = RUSSIAN_CHARSET
            Style.Font.Color = clWindowText
            Style.Font.Height = -11
            Style.Font.Name = 'Tahoma'
            Style.Font.Style = []
            Style.LookAndFeel.NativeStyle = False
            Style.LookAndFeel.SkinName = ''
            Style.TextStyle = []
            Style.TransparentBorder = False
            Style.IsFontAssigned = True
            StyleDisabled.LookAndFeel.NativeStyle = False
            StyleDisabled.LookAndFeel.SkinName = ''
            StyleFocused.LookAndFeel.NativeStyle = False
            StyleFocused.LookAndFeel.SkinName = ''
            StyleHot.LookAndFeel.NativeStyle = False
            StyleHot.LookAndFeel.SkinName = ''
            TabOrder = 11
            OnEnter = cxDBTextEditAllEnter
            OnExit = cxDBTextEditAllExit
            OnKeyDown = EnterKakoTab
            Width = 92
          end
          object txtCustom3: TcxDBTextEdit
            Left = 81
            Top = 268
            BeepOnEnter = False
            DataBinding.DataField = 'CUSTOM3'
            DataBinding.DataSource = dm.dsRNStavka
            Properties.BeepOnError = True
            TabOrder = 29
            Visible = False
            OnEnter = cxDBTextEditAllEnter
            OnExit = cxDBTextEditAllExit
            OnKeyDown = EnterKakoTab
            Width = 367
          end
          object Button1: TButton
            Left = 59
            Top = 295
            Width = 125
            Height = 25
            Hint = 'Bitmap  image |*.bmp;*.dib'
            Caption = '...'
            TabOrder = 31
            TabStop = False
            Visible = False
            OnClick = Button1Click
          end
          object cxLabel22: TcxLabel
            Left = 149
            Top = 361
            Caption = #1055#1086#1088#1072#1095#1082#1072
            ParentFont = False
            Style.Font.Charset = RUSSIAN_CHARSET
            Style.Font.Color = clRed
            Style.Font.Height = -11
            Style.Font.Name = 'Tahoma'
            Style.Font.Style = []
            Style.TextColor = clBlue
            Style.IsFontAssigned = True
            Properties.Alignment.Horz = taRightJustify
            Transparent = True
            Visible = False
            AnchorX = 196
          end
          object txtPorackaS: TcxDBTextEdit
            Left = 218
            Top = 357
            DataBinding.DataField = 'ID_PORACKA'
            DataBinding.DataSource = dm.dsRNStavka
            ParentFont = False
            Style.BorderStyle = ebsUltraFlat
            Style.Font.Charset = RUSSIAN_CHARSET
            Style.Font.Color = clWindowText
            Style.Font.Height = -11
            Style.Font.Name = 'Tahoma'
            Style.Font.Style = []
            Style.LookAndFeel.NativeStyle = False
            Style.LookAndFeel.SkinName = ''
            Style.TextStyle = []
            Style.TransparentBorder = False
            Style.IsFontAssigned = True
            StyleDisabled.LookAndFeel.NativeStyle = False
            StyleDisabled.LookAndFeel.SkinName = ''
            StyleDisabled.TextColor = clWindowText
            StyleFocused.LookAndFeel.NativeStyle = False
            StyleFocused.LookAndFeel.SkinName = ''
            StyleHot.LookAndFeel.NativeStyle = False
            StyleHot.LookAndFeel.SkinName = ''
            TabOrder = 39
            Visible = False
            OnEnter = cxDBTextEditAllEnter
            OnExit = cxDBTextEditAllExit
            OnKeyDown = EnterKakoTab
            Width = 56
          end
          object cbPorackaS: TcxDBLookupComboBox
            Left = 276
            Top = 356
            RepositoryItem = dm.erLookupComboBox
            AutoSize = False
            DataBinding.DataField = 'ID_PORACKA'
            DataBinding.DataSource = dm.dsRNStavka
            Properties.KeyFieldNames = 'ID'
            Properties.ListColumns = <
              item
                FieldName = 'NAZIV'
              end>
            TabOrder = 37
            Visible = False
            OnEnter = cxDBTextEditAllEnter
            OnExit = cxDBTextEditAllExit
            OnKeyDown = EnterKakoTab
            Height = 21
            Width = 134
          end
          object txtKraenDatum: TcxDBDateEdit
            Left = 201
            Top = 321
            DataBinding.DataField = 'KRAEN_DATUM_PREVOZ'
            DataBinding.DataSource = dm.dsRNStavka
            TabOrder = 33
            Visible = False
            OnEnter = cxDBTextEditAllEnter
            OnExit = cxDBTextEditAllExit
            OnKeyDown = EnterKakoTab
            Width = 111
          end
          object cxLabel23: TcxLabel
            Left = 71
            Top = 322
            AutoSize = False
            Caption = #1050#1088#1072#1077#1085' '#1076#1072#1090#1091#1084' '#1085#1072' '#1087#1088#1077#1074#1086#1079
            Style.TextColor = clBlue
            StyleFocused.TextColor = clWindowText
            StyleHot.TextColor = clWindowText
            Properties.WordWrap = True
            Transparent = True
            Visible = False
            Height = 20
            Width = 124
          end
          object txtBrCiklusiS: TcxDBTextEdit
            Left = 239
            Top = 382
            DataBinding.DataField = 'BROJ_CIKLUSI'
            DataBinding.DataSource = dm.dsRNStavka
            TabOrder = 45
            Visible = False
            OnEnter = cxDBTextEditAllEnter
            OnExit = cxDBTextEditAllExit
            OnKeyDown = EnterKakoTab
            Width = 131
          end
          object cxLabel24: TcxLabel
            Left = 149
            Top = 382
            Caption = #1041#1088#1086#1112' '#1085#1072' '#1094#1080#1082#1083#1091#1089#1080
            Style.TextColor = clBlue
            Properties.Alignment.Horz = taRightJustify
            Properties.WordWrap = True
            Transparent = True
            Visible = False
            Width = 45
            AnchorX = 194
          end
          object cxLabel25: TcxLabel
            Left = 30
            Top = 354
            Caption = #1041#1088#1086#1112' '#1085#1072' '#1095#1072#1089#1086#1074#1080
            Style.TextColor = clBlue
            Properties.Alignment.Horz = taRightJustify
            Properties.WordWrap = True
            Transparent = True
            Visible = False
            Width = 43
            AnchorX = 73
          end
          object txtBrCasoviS: TcxDBTextEdit
            Left = 30
            Top = 377
            DataBinding.DataField = 'BROJ_CASOVI'
            DataBinding.DataSource = dm.dsRNStavka
            TabOrder = 42
            Visible = False
            OnEnter = cxDBTextEditAllEnter
            OnExit = cxDBTextEditAllExit
            OnKeyDown = EnterKakoTab
            Width = 131
          end
          object cxLabel28: TcxLabel
            Left = -62
            Top = 141
            AutoSize = False
            Caption = #1057#1090#1072#1090#1091#1089
            Style.TextColor = clRed
            Properties.Alignment.Horz = taRightJustify
            Transparent = True
            Visible = False
            Height = 17
            Width = 46
            AnchorX = -16
          end
          object txtStatusS: TcxDBTextEdit
            Left = -10
            Top = 155
            DataBinding.DataField = 'STATUS'
            DataBinding.DataSource = dm.dsRNStavka
            TabOrder = 25
            Visible = False
            OnEnter = cxDBTextEditAllEnter
            OnExit = cxDBTextEditAllExit
            OnKeyDown = EnterKakoTab
            Width = 56
          end
          object cbStatusS: TcxDBLookupComboBox
            Left = 52
            Top = 155
            DataBinding.DataField = 'STATUS'
            DataBinding.DataSource = dm.dsRNStavka
            Properties.DropDownAutoSize = True
            Properties.DropDownSizeable = True
            Properties.KeyFieldNames = 'VREDNOST'
            Properties.ListColumns = <
              item
                FieldName = 'vrednost'
              end
              item
                FieldName = 'naziv'
              end>
            Properties.ListFieldIndex = 1
            Properties.ListSource = dm.dsMatStatus
            TabOrder = 27
            Visible = False
            OnEnter = cxDBTextEditAllEnter
            OnExit = cxDBTextEditAllExit
            OnKeyDown = EnterKakoTab
            Width = 136
          end
          object cxLabel37: TcxLabel
            Left = 352
            Top = 132
            AutoSize = False
            Caption = #1054#1076#1086#1073#1088#1080#1083
            ParentFont = False
            Style.Font.Charset = RUSSIAN_CHARSET
            Style.Font.Color = clWindowText
            Style.Font.Height = -11
            Style.Font.Name = 'Tahoma'
            Style.Font.Style = []
            Style.TextColor = clBlue
            Style.IsFontAssigned = True
            Properties.Alignment.Horz = taRightJustify
            Properties.WordWrap = True
            Transparent = True
            Height = 20
            Width = 49
            AnchorX = 401
          end
          object txtTipOdobril: TcxDBTextEdit
            Left = 347
            Top = 149
            Hint = #1058#1080#1087' '#1085#1072' '#1086#1076#1086#1073#1088#1091#1074#1072#1095
            DataBinding.DataField = 'TIP_ODOBRIL'
            DataBinding.DataSource = dm.dsRNStavka
            ParentFont = False
            TabOrder = 22
            OnEnter = cxDBTextEditAllEnter
            OnExit = cxDBTextEditAllExit
            OnKeyDown = EnterKakoTab
            Width = 44
          end
          object txtOdobril: TcxDBTextEdit
            Left = 390
            Top = 149
            Hint = #1064#1080#1092#1088#1072' '#1085#1072' '#1086#1076#1086#1073#1088#1091#1074#1072#1095
            DataBinding.DataField = 'ODOBRIL'
            DataBinding.DataSource = dm.dsRNStavka
            ParentFont = False
            TabOrder = 23
            OnEnter = cxDBTextEditAllEnter
            OnExit = cxDBTextEditAllExit
            OnKeyDown = EnterKakoTab
            Width = 59
          end
          object cbOdobril: TcxLookupComboBox
            Left = 449
            Top = 149
            Hint = #1053#1072#1079#1080#1074' '#1085#1072' '#1054#1076#1086#1073#1088#1091#1074#1072#1095
            Properties.ClearKey = 46
            Properties.DropDownAutoSize = True
            Properties.DropDownSizeable = True
            Properties.KeyFieldNames = 'tip_partner;id'
            Properties.ListColumns = <
              item
                Width = 40
                FieldName = 'TIP_PARTNER'
              end
              item
                Width = 60
                FieldName = 'id'
              end
              item
                Width = 170
                FieldName = 'naziv'
              end>
            Properties.ListFieldIndex = 2
            Properties.ListOptions.SyncMode = True
            Properties.ListSource = dmMat.dsPartner
            TabOrder = 24
            OnEnter = cxDBTextEditAllEnter
            OnExit = cxDBTextEditAllExit
            OnKeyDown = EnterKakoTab
            Width = 214
          end
          object cxLabel39: TcxLabel
            Left = 652
            Top = 49
            Caption = #1055#1072#1082#1091#1074#1072#1114#1077
            Style.TextColor = clBlue
            Properties.Alignment.Horz = taRightJustify
            Transparent = True
            Visible = False
            AnchorX = 708
          end
          object txtPakuvanje: TcxDBTextEdit
            Left = 120
            Top = 111
            Hint = #1055#1072#1082#1091#1074#1072#1114#1077' '#1085#1072' '#1075#1086#1090#1086#1074#1080#1086#1090' '#1087#1088#1086#1080#1079#1074#1086#1076
            DataBinding.DataField = 'ID_PAKUVANJE'
            DataBinding.DataSource = dm.dsRNStavka
            ParentFont = False
            Style.BorderStyle = ebsUltraFlat
            Style.Font.Charset = RUSSIAN_CHARSET
            Style.Font.Color = clWindowText
            Style.Font.Height = -11
            Style.Font.Name = 'Tahoma'
            Style.Font.Style = []
            Style.LookAndFeel.NativeStyle = False
            Style.LookAndFeel.SkinName = ''
            Style.TextStyle = []
            Style.TransparentBorder = False
            Style.IsFontAssigned = True
            StyleDisabled.LookAndFeel.NativeStyle = False
            StyleDisabled.LookAndFeel.SkinName = ''
            StyleDisabled.TextColor = clWindowText
            StyleFocused.LookAndFeel.NativeStyle = False
            StyleFocused.LookAndFeel.SkinName = ''
            StyleHot.LookAndFeel.NativeStyle = False
            StyleHot.LookAndFeel.SkinName = ''
            TabOrder = 18
            OnEnter = cxDBTextEditAllEnter
            OnExit = cxDBTextEditAllExit
            OnKeyDown = EnterKakoTab
            Width = 56
          end
          object lblMerka: TcxDBLabel
            Left = 214
            Top = 85
            AutoSize = True
            DataBinding.DataField = 'MERKA'
            DataBinding.DataSource = dm.dsRNStavka
            Properties.Alignment.Horz = taLeftJustify
            Transparent = True
          end
          object cxLabel42: TcxLabel
            Left = 620
            Top = 24
            Caption = #1040#1084#1073#1072#1083#1072#1078#1072
            Style.TextColor = clBlue
            Properties.Alignment.Horz = taRightJustify
            Properties.WordWrap = True
            Transparent = True
            Visible = False
            Width = 55
            AnchorX = 675
          end
          object txtVidAmbalaza: TcxDBTextEdit
            Left = 717
            Top = 23
            Hint = #1042#1080#1076' '#1085#1072' '#1072#1084#1073#1072#1083#1072#1078#1072
            DataBinding.DataField = 'VID_AMBALAZA'
            DataBinding.DataSource = dm.dsRNStavka
            ParentFont = False
            TabOrder = 6
            Visible = False
            OnEnter = cxDBTextEditAllEnter
            OnExit = cxDBTextEditAllExit
            OnKeyDown = EnterKakoTab
            Width = 56
          end
          object txtAmbalaza: TcxDBTextEdit
            Left = 776
            Top = 23
            Hint = #1064#1080#1092#1088#1072' '#1085#1072' '#1072#1084#1073#1072#1083#1072#1078#1072
            DataBinding.DataField = 'AMBALAZA'
            DataBinding.DataSource = dm.dsRNStavka
            ParentFont = False
            TabOrder = 7
            Visible = False
            OnEnter = cxDBTextEditAllEnter
            OnExit = cxDBTextEditAllExit
            OnKeyDown = EnterKakoTab
            Width = 56
          end
          object cbNazivArtikal: TcxExtLookupComboBox
            Left = 236
            Top = 23
            Hint = #1053#1072#1079#1080#1074' '#1085#1072' '#1072#1088#1090#1080#1082#1072#1083'/'#1087#1088#1086#1080#1079#1074#1086#1076
            RepositoryItem = dmRes.cxNurkoEditRepositoryExtLookupComboBox
            Properties.ClearKey = 46
            Properties.DropDownAutoSize = True
            Properties.DropDownSizeable = True
            TabOrder = 5
            OnEnter = cxDBTextEditAllEnter
            OnExit = cxDBTextEditAllExit
            OnKeyDown = EnterKakoTab
            Width = 427
          end
          object cbAmbalaza: TcxExtLookupComboBox
            Left = 831
            Top = 23
            RepositoryItem = dmRes.cxNurkoEditRepositoryExtLookupComboBox
            Properties.ClearKey = 46
            Properties.DropDownAutoSize = True
            Properties.DropDownSizeable = True
            TabOrder = 8
            Visible = False
            OnEnter = cxDBTextEditAllEnter
            OnExit = cxDBTextEditAllExit
            OnKeyDown = EnterKakoTab
            Width = 225
          end
          object cbPakuvanje: TcxDBLookupComboBox
            Left = 177
            Top = 110
            DataBinding.DataField = 'ID_PAKUVANJE'
            DataBinding.DataSource = dm.dsRNStavka
            Properties.DropDownAutoSize = True
            Properties.DropDownSizeable = True
            Properties.ImmediatePost = True
            Properties.KeyFieldNames = 'ID'
            Properties.ListColumns = <
              item
                Caption = #1064#1080#1092#1088#1072
                Width = 20
                FieldName = 'ID'
              end
              item
                Caption = #1048#1079#1074#1077#1076#1077#1085#1072' '#1045#1052
                Width = 20
                FieldName = 'izvedena_em'
              end
              item
                Caption = #1053#1072#1079#1080#1074
                Width = 100
                FieldName = 'naziv'
              end>
            Properties.ListFieldIndex = 2
            Properties.ListOptions.SyncMode = True
            Properties.ListSource = dm.dsIzvedenaEM
            TabOrder = 15
            OnEnter = cxDBTextEditAllEnter
            OnExit = cxDBTextEditAllExit
            OnKeyDown = EnterKakoTab
            Width = 146
          end
          object cxLabel43: TcxLabel
            Left = 352
            Top = 110
            Caption = #1057#1077#1088#1080#1112#1072
            Style.TextColor = clBlue
            Properties.Alignment.Horz = taRightJustify
            Transparent = True
            AnchorX = 390
          end
          object txtSerija: TcxDBTextEdit
            Left = 391
            Top = 111
            Hint = #1057#1077#1088#1080#1089#1082#1080' '#1085#1072' '#1087#1088#1086#1080#1079#1074#1086#1076#1086#1090
            DataBinding.DataField = 'SERISKI_BROJ'
            DataBinding.DataSource = dm.dsRNStavka
            ParentFont = False
            Style.BorderStyle = ebsUltraFlat
            Style.Font.Charset = RUSSIAN_CHARSET
            Style.Font.Color = clWindowText
            Style.Font.Height = -11
            Style.Font.Name = 'Tahoma'
            Style.Font.Style = []
            Style.LookAndFeel.NativeStyle = False
            Style.LookAndFeel.SkinName = ''
            Style.TextStyle = []
            Style.TransparentBorder = False
            Style.IsFontAssigned = True
            StyleDisabled.LookAndFeel.NativeStyle = False
            StyleDisabled.LookAndFeel.SkinName = ''
            StyleFocused.LookAndFeel.NativeStyle = False
            StyleFocused.LookAndFeel.SkinName = ''
            StyleHot.LookAndFeel.NativeStyle = False
            StyleHot.LookAndFeel.SkinName = ''
            TabOrder = 19
            OnEnter = cxDBTextEditAllEnter
            OnExit = cxDBTextEditAllExit
            OnKeyDown = EnterKakoTab
            Width = 92
          end
          object cxLabel40: TcxLabel
            Left = 667
            Top = 109
            Caption = #1050#1086#1083#1080#1095#1080#1085#1072' '#1087#1072#1082'.'
            Style.TextColor = clBlue
            Properties.Alignment.Horz = taRightJustify
            Transparent = True
            Visible = False
            AnchorX = 745
          end
          object txtZabeleskaS: TcxDBMemo
            Left = 352
            Top = 64
            Hint = '*'#1050#1083#1080#1082#1085#1077#1090#1077' '#1076#1074#1072#1087#1072#1090#1080', '#1079#1072' '#1086#1090#1074#1086#1088#1072#1114#1077' '#1085#1072' Notepad!  '
            DataBinding.DataField = 'ZABELESKA'
            DataBinding.DataSource = dm.dsRNStavka
            TabOrder = 12
            OnDblClick = aUrediTextOpisExecute
            OnEnter = cxDBTextEditAllEnter
            OnExit = cxDBTextEditAllExit
            OnKeyDown = EnterKakoTab
            Height = 41
            Width = 311
          end
          object cxLabel11: TcxLabel
            Left = 3
            Top = 85
            AutoSize = False
            Caption = #1055#1072#1082#1091#1074#1072#1114#1077' '#1077#1076#1080#1085#1077#1095#1085#1086
            Style.TextColor = clBlue
            Properties.Alignment.Horz = taRightJustify
            Properties.WordWrap = True
            Transparent = True
            Height = 23
            Width = 111
            AnchorX = 114
          end
          object txtKolicinaEdMerka: TcxDBTextEdit
            Tag = 1
            Left = 120
            Top = 85
            Hint = #1050#1086#1083#1080#1095#1080#1085#1072' '#1074#1086' '#1087#1072#1082#1091#1074#1072#1114#1077' '#1087#1086' '#1077#1076#1080#1085#1080#1094#1072' '#1084#1077#1088#1082#1072
            DataBinding.DataField = 'KOLICINA_BROJ'
            DataBinding.DataSource = dm.dsRNStavka
            ParentFont = False
            Properties.ReadOnly = True
            Style.BorderStyle = ebsUltraFlat
            Style.Font.Charset = RUSSIAN_CHARSET
            Style.Font.Color = clWindowText
            Style.Font.Height = -11
            Style.Font.Name = 'Tahoma'
            Style.Font.Style = []
            Style.LookAndFeel.NativeStyle = False
            Style.LookAndFeel.SkinName = ''
            Style.TextStyle = []
            Style.TransparentBorder = False
            Style.IsFontAssigned = True
            StyleDisabled.LookAndFeel.NativeStyle = False
            StyleDisabled.LookAndFeel.SkinName = ''
            StyleFocused.LookAndFeel.NativeStyle = False
            StyleFocused.LookAndFeel.SkinName = ''
            StyleHot.LookAndFeel.NativeStyle = False
            StyleHot.LookAndFeel.SkinName = ''
            TabOrder = 13
            OnEnter = cxDBTextEditAllEnter
            OnExit = cxDBTextEditAllExit
            OnKeyDown = EnterKakoTab
            Width = 92
          end
          object lblMerkaK: TcxDBLabel
            Left = 214
            Top = 59
            AutoSize = True
            DataBinding.DataField = 'MERKA_KOLICINA'
            DataBinding.DataSource = dm.dsRNStavka
            Properties.Alignment.Horz = taLeftJustify
            Transparent = True
          end
          object cxLabel26: TcxLabel
            Left = 682
            Top = 12
            Caption = #1040#1084#1073#1072#1083#1072#1078#1072
            Style.TextColor = clBlue
            Properties.Alignment.Horz = taRightJustify
            Properties.WordWrap = True
            Transparent = True
            Visible = False
            Width = 55
            AnchorX = 737
          end
          object txtArtKolicina: TcxDBTextEdit
            Left = 744
            Top = 12
            Hint = #1041#1088#1086#1112' '#1085#1072' '#1096#1080#1096#1080#1114#1072
            DataBinding.DataField = 'ART_KOLICINA'
            DataBinding.DataSource = dm.dsRNStavka
            DragCursor = crAppStart
            ParentFont = False
            Properties.ReadOnly = True
            Style.BorderStyle = ebsUltraFlat
            Style.Font.Charset = RUSSIAN_CHARSET
            Style.Font.Color = clWindowText
            Style.Font.Height = -11
            Style.Font.Name = 'Tahoma'
            Style.Font.Style = []
            Style.LookAndFeel.NativeStyle = False
            Style.LookAndFeel.SkinName = ''
            Style.TextStyle = []
            Style.TransparentBorder = False
            Style.IsFontAssigned = True
            StyleDisabled.LookAndFeel.NativeStyle = False
            StyleDisabled.LookAndFeel.SkinName = ''
            StyleFocused.LookAndFeel.NativeStyle = False
            StyleFocused.LookAndFeel.SkinName = ''
            StyleHot.LookAndFeel.NativeStyle = False
            StyleHot.LookAndFeel.SkinName = ''
            TabOrder = 1
            Visible = False
            OnEnter = cxDBTextEditAllEnter
            OnExit = cxDBTextEditAllExit
            OnKeyDown = EnterKakoTab
            Width = 91
          end
          object cxDBLabel1: TcxDBLabel
            Left = 838
            Top = 12
            AutoSize = True
            DataBinding.DataField = 'MERKA_KOLICINA'
            DataBinding.DataSource = dm.dsRNStavka
            Properties.Alignment.Horz = taLeftJustify
            Transparent = True
            Visible = False
          end
          object txtArtBrPak: TcxDBTextEdit
            Left = 744
            Top = 37
            Hint = #1041#1088#1086#1112' '#1085#1072' '#1096#1080#1096#1080#1114#1072
            DataBinding.DataField = 'ART_BROJ_PAK'
            DataBinding.DataSource = dm.dsRNStavka
            DragCursor = crAppStart
            ParentFont = False
            Properties.ReadOnly = True
            Style.BorderStyle = ebsUltraFlat
            Style.Font.Charset = RUSSIAN_CHARSET
            Style.Font.Color = clWindowText
            Style.Font.Height = -11
            Style.Font.Name = 'Tahoma'
            Style.Font.Style = []
            Style.LookAndFeel.NativeStyle = False
            Style.LookAndFeel.SkinName = ''
            Style.TextStyle = []
            Style.TransparentBorder = False
            Style.IsFontAssigned = True
            StyleDisabled.LookAndFeel.NativeStyle = False
            StyleDisabled.LookAndFeel.SkinName = ''
            StyleFocused.LookAndFeel.NativeStyle = False
            StyleFocused.LookAndFeel.SkinName = ''
            StyleHot.LookAndFeel.NativeStyle = False
            StyleHot.LookAndFeel.SkinName = ''
            TabOrder = 9
            Visible = False
            OnEnter = cxDBTextEditAllEnter
            OnExit = cxDBTextEditAllExit
            OnKeyDown = EnterKakoTab
            Width = 91
          end
          object cxLabel27: TcxLabel
            Left = 52
            Top = 111
            Caption = #1055#1072#1082#1091#1074#1072#1114#1077
            Style.TextColor = clBlue
            Properties.Alignment.Horz = taRightJustify
            Properties.WordWrap = True
            Transparent = True
            Width = 56
            AnchorX = 108
          end
          object cxLabel46: TcxLabel
            Left = 16
            Top = 137
            AutoSize = False
            Caption = #1041#1088#1086#1112' '#1085#1072' '#1087#1072#1082#1091#1074#1072#1114#1072
            Style.TextColor = clBlue
            Properties.Alignment.Horz = taRightJustify
            Properties.WordWrap = True
            Transparent = True
            Height = 23
            Width = 98
            AnchorX = 114
          end
          object txtKolicinaPak: TcxDBTextEdit
            Tag = 1
            Left = 120
            Top = 137
            Hint = #1050#1086#1083#1080#1095#1080#1085#1072' '#1074#1086' '#1087#1072#1082#1091#1074#1072#1114#1077' '#1087#1086' '#1077#1076#1080#1085#1080#1094#1072' '#1084#1077#1088#1082#1072
            DataBinding.DataField = 'KOLICINA_PAKUVANJE'
            DataBinding.DataSource = dm.dsRNStavka
            ParentFont = False
            Properties.ReadOnly = True
            Style.BorderStyle = ebsUltraFlat
            Style.Font.Charset = RUSSIAN_CHARSET
            Style.Font.Color = clWindowText
            Style.Font.Height = -11
            Style.Font.Name = 'Tahoma'
            Style.Font.Style = []
            Style.LookAndFeel.NativeStyle = False
            Style.LookAndFeel.SkinName = ''
            Style.TextStyle = []
            Style.TransparentBorder = False
            Style.IsFontAssigned = True
            StyleDisabled.LookAndFeel.NativeStyle = False
            StyleDisabled.LookAndFeel.SkinName = ''
            StyleFocused.LookAndFeel.NativeStyle = False
            StyleFocused.LookAndFeel.SkinName = ''
            StyleHot.LookAndFeel.NativeStyle = False
            StyleHot.LookAndFeel.SkinName = ''
            TabOrder = 21
            OnEnter = cxDBTextEditAllEnter
            OnExit = cxDBTextEditAllExit
            OnKeyDown = EnterKakoTab
            Width = 92
          end
        end
      end
    end
    object tsNormativ: TcxTabSheet
      Caption = #1053#1086#1088#1084#1072#1090#1080#1074' ('#1089#1091#1088#1086#1074#1080#1085#1080')  '
      ImageIndex = 3
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object cxGroupBox3: TcxGroupBox
        Left = 0
        Top = 0
        Align = alTop
        Caption = '  '#1053#1086#1088#1084#1072#1090#1080#1074' ('#1089#1091#1088#1086#1074#1080#1085#1080' '#1082#1086#1080' '#1090#1088#1077#1073#1072' '#1076#1072' '#1089#1077' '#1082#1086#1088#1080#1089#1090#1072#1090')  '
        TabOrder = 0
        Height = 257
        Width = 851
        object cxGrid2: TcxGrid
          Left = 2
          Top = 18
          Width = 847
          Height = 237
          Align = alClient
          TabOrder = 0
          object cxGrid2DBTableView1: TcxGridDBTableView
            Navigator.Buttons.CustomButtons = <>
            DataController.DataSource = dm.dspRNNormativ
            DataController.Summary.DefaultGroupSummaryItems = <>
            DataController.Summary.FooterSummaryItems = <>
            DataController.Summary.SummaryGroups = <>
            FilterRow.InfoText = #1050#1083#1080#1082#1085#1077#1090#1077' '#1090#1091#1082#1072' '#1079#1072' '#1076#1072' '#1076#1077#1092#1080#1085#1080#1088#1072#1090#1077' '#1092#1080#1083#1090#1077#1088
            FilterRow.Visible = True
            OptionsBehavior.IncSearch = True
            OptionsCustomize.ColumnsQuickCustomization = True
            OptionsData.Deleting = False
            OptionsData.Editing = False
            OptionsData.Inserting = False
            OptionsView.Indicator = True
            object cxGrid2DBTableView1VID_ARTIKAL_N: TcxGridDBColumn
              Caption = #1042#1080#1076' '#1085#1072' '#1089#1091#1088#1086#1074#1080#1085#1072
              DataBinding.FieldName = 'VID_ARTIKAL_N'
              Width = 89
            end
            object cxGrid2DBTableView1ARTIKAL_N: TcxGridDBColumn
              Caption = #1064#1080#1092#1088#1072' '#1085#1072' '#1089#1091#1088#1086#1074#1080#1085#1072
              DataBinding.FieldName = 'ARTIKAL_N'
              Width = 106
            end
            object cxGrid2DBTableView1NAZIV_ARTIKAL: TcxGridDBColumn
              Caption = #1057#1091#1088#1086#1074#1080#1085#1072
              DataBinding.FieldName = 'NAZIV_ARTIKAL'
              Width = 459
            end
            object cxGrid2DBTableView1KOLICINA: TcxGridDBColumn
              Caption = #1050#1086#1083#1080#1095#1080#1085#1072
              DataBinding.FieldName = 'KOLICINA'
              Width = 112
            end
            object cxGrid2DBTableView1MERKA: TcxGridDBColumn
              Caption = #1052#1077#1088#1082#1072
              DataBinding.FieldName = 'MERKA'
              Width = 74
            end
            object cxGrid2DBTableView1AKTIVEN: TcxGridDBColumn
              Caption = #1040#1082#1090#1080#1074#1077#1085
              DataBinding.FieldName = 'AKTIVEN'
              Visible = False
            end
          end
          object cxGrid2Level1: TcxGridLevel
            GridView = cxGrid2DBTableView1
          end
        end
      end
      object gbSurovini: TcxGroupBox
        Left = 0
        Top = 257
        Align = alClient
        Caption = #1057#1091#1088#1086#1074#1080#1085#1080' '#1086#1076' '#1052#1072#1075#1072#1094#1080#1085' '#1079#1072' '#1088#1077#1087#1088#1086#1084#1072#1090#1077#1088#1080#1112#1072#1083#1080'  '
        TabOrder = 1
        Height = 302
        Width = 851
        object cxGrid7: TcxGrid
          Left = 2
          Top = 18
          Width = 847
          Height = 282
          Align = alClient
          TabOrder = 0
          object cxGridDBTableView4: TcxGridDBTableView
            Navigator.Buttons.CustomButtons = <>
            OnFocusedRecordChanged = cxGridDBTableView4FocusedRecordChanged
            DataController.DataSource = dm.dsRNSurovini
            DataController.Summary.DefaultGroupSummaryItems = <>
            DataController.Summary.FooterSummaryItems = <>
            DataController.Summary.SummaryGroups = <>
            FilterRow.InfoText = #1050#1083#1080#1082#1085#1077#1090#1077' '#1090#1091#1082#1072' '#1079#1072' '#1076#1072' '#1076#1077#1092#1080#1085#1080#1088#1072#1090#1077' '#1092#1080#1083#1090#1077#1088
            FilterRow.Visible = True
            OptionsCustomize.ColumnsQuickCustomization = True
            OptionsData.Deleting = False
            OptionsData.Editing = False
            OptionsData.Inserting = False
            OptionsView.Indicator = True
            object cxGridDBTableView4ID: TcxGridDBColumn
              Caption = #1064#1080#1092#1088#1072
              DataBinding.FieldName = 'ID'
              Visible = False
            end
            object cxGridDBTableView4VID_SUROVINA: TcxGridDBColumn
              Caption = #1042#1080#1076' '#1085#1072' '#1089#1091#1088#1086#1074#1080#1085#1072
              DataBinding.FieldName = 'VID_SUROVINA'
              Width = 26
            end
            object cxGridDBTableView4SUROVINA: TcxGridDBColumn
              Caption = #1064#1080#1092#1088#1072' '#1085#1072' '#1089#1091#1088#1086#1074#1080#1085#1072
              DataBinding.FieldName = 'SUROVINA'
              Width = 44
            end
            object cxGridDBTableView4NAZIV_SUROVINA: TcxGridDBColumn
              Caption = #1057#1091#1088#1086#1074#1080#1085#1072
              DataBinding.FieldName = 'NAZIV_SUROVINA'
              Width = 246
            end
            object cxGridDBTableView4ID_RN_STAVKA: TcxGridDBColumn
              Caption = #1064#1080#1092#1088#1072' '#1085#1072' '#1056#1053' '#1057#1090#1072#1074#1082#1072
              DataBinding.FieldName = 'ID_RN_STAVKA'
              Width = 45
            end
            object cxGridDBTableView4KOLICINA: TcxGridDBColumn
              Caption = #1050#1086#1083#1080#1095#1080#1085#1072
              DataBinding.FieldName = 'KOLICINA'
              Width = 57
            end
            object cxGridDBTableView4MERNA_EDINICA: TcxGridDBColumn
              Caption = #1064#1080#1092#1088#1072' '#1085#1072' '#1084#1077#1088#1082#1072
              DataBinding.FieldName = 'MERNA_EDINICA'
              Visible = False
            end
            object cxGridDBTableView4NAZIV_MERKA: TcxGridDBColumn
              Caption = #1052#1077#1088#1082#1072
              DataBinding.FieldName = 'NAZIV_MERKA'
              Width = 154
            end
            object cxGridDBTableView4PRODUKCISKI_LOT: TcxGridDBColumn
              Caption = #1055#1088#1086#1076#1091#1082#1094#1080#1089#1082#1080' '#1083#1086#1090
              DataBinding.FieldName = 'PRODUKCISKI_LOT'
              Visible = False
            end
            object cxGridDBTableView4ID_RE: TcxGridDBColumn
              Caption = #1064#1080#1092#1088#1072' '#1085#1072' '#1056#1045
              DataBinding.FieldName = 'ID_RE'
              Visible = False
            end
            object cxGridDBTableView4NAZIV_MAGACIN: TcxGridDBColumn
              Caption = #1053#1072#1079#1080#1074' '#1085#1072' '#1056#1045' ('#1084#1072#1075#1072#1094#1080#1085')'
              DataBinding.FieldName = 'NAZIV_MAGACIN'
              Width = 263
            end
          end
          object cxGridLevel4: TcxGridLevel
            GridView = cxGridDBTableView4
          end
        end
      end
    end
    object tsOperacii: TcxTabSheet
      Caption = #1054#1087#1077#1088#1072#1094#1080#1080'  '
      ImageIndex = 4
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object cxGroupBox2: TcxGroupBox
        Left = 0
        Top = 0
        Align = alClient
        Caption = '  '#1054#1087#1077#1088#1072#1094#1080#1080' '#1079#1072' '#1085#1086#1088#1084#1072#1090#1080#1074#1086#1090'  '
        TabOrder = 0
        Height = 559
        Width = 851
        object cxGrid5: TcxGrid
          Left = 2
          Top = 18
          Width = 847
          Height = 539
          Align = alClient
          TabOrder = 0
          object cxGridDBTableView2: TcxGridDBTableView
            Navigator.Buttons.CustomButtons = <>
            DataController.DataSource = dm.dspRNOperacija
            DataController.Summary.DefaultGroupSummaryItems = <>
            DataController.Summary.FooterSummaryItems = <>
            DataController.Summary.SummaryGroups = <>
            FilterRow.InfoText = #1050#1083#1080#1082#1085#1077#1090#1077' '#1090#1091#1082#1072' '#1079#1072' '#1076#1072' '#1076#1077#1092#1080#1085#1080#1088#1072#1090#1077' '#1092#1080#1083#1090#1077#1088
            FilterRow.Visible = True
            OptionsCustomize.ColumnsQuickCustomization = True
            OptionsData.Deleting = False
            OptionsData.Editing = False
            OptionsData.Inserting = False
            OptionsView.Footer = True
            OptionsView.Indicator = True
            object cxGridDBTableView2OPERACIJA_N: TcxGridDBColumn
              Caption = #1064#1080#1092#1088#1072' '#1085#1072' '#1086#1087#1077#1088#1072#1094#1080#1112#1072
              DataBinding.FieldName = 'OPERACIJA_N'
              Width = 40
            end
            object cxGridDBTableView2NAZIV_OPERACIJA: TcxGridDBColumn
              Caption = #1054#1087#1077#1088#1072#1094#1080#1112#1072
              DataBinding.FieldName = 'NAZIV_OPERACIJA'
              Width = 283
            end
            object cxGridDBTableView2POTREBNO_VREME: TcxGridDBColumn
              Caption = #1055#1086#1090#1088#1077#1073#1085#1086' '#1074#1088#1077#1084#1077
              DataBinding.FieldName = 'POTREBNO_VREME'
            end
            object cxGridDBTableView2MERKA: TcxGridDBColumn
              Caption = #1052#1077#1088#1082#1072
              DataBinding.FieldName = 'MERKA'
            end
            object cxGridDBTableView2AKTIVEN: TcxGridDBColumn
              Caption = #1040#1082#1090#1080#1074#1077#1085
              DataBinding.FieldName = 'AKTIVEN'
            end
            object cxGridDBTableView2SEKVENCA: TcxGridDBColumn
              Caption = #1057#1077#1082#1074#1077#1085#1094#1072
              DataBinding.FieldName = 'SEKVENCA'
            end
          end
          object cxGridLevel2: TcxGridLevel
            GridView = cxGridDBTableView2
          end
        end
      end
    end
    object cxTabSheet1: TcxTabSheet
      Caption = 'cxTabSheet1'
      ImageIndex = 5
      TabVisible = False
      ExplicitLeft = 0
      ExplicitTop = 0
      ExplicitWidth = 0
      ExplicitHeight = 0
      object Panel3: TPanel
        Left = 0
        Top = 38
        Width = 851
        Height = 521
        Align = alBottom
        TabOrder = 0
        object cxSchedulerOtsustva: TcxScheduler
          Left = 1
          Top = 1
          Width = 849
          Height = 519
          DateNavigator.RowCount = 2
          DateNavigator.ShowDatesContainingHolidaysInColor = True
          ViewDay.WorkTimeOnly = True
          ViewGantt.WorkDaysOnly = True
          ViewGantt.WorkTimeOnly = True
          ViewTimeGrid.ShowTimeAsClock = True
          ViewTimeGrid.WorkDaysOnly = True
          ViewTimeGrid.WorkTimeOnly = True
          ViewWeek.Active = True
          ViewYear.MonthHeaderPopupMenu.PopupMenu = PopupMenu1
          ViewYear.MonthHeaderPopupMenu.Items = [mhpmiFullYear, mhpmiHalfYear, mhpmiQuarter]
          Align = alClient
          ContentPopupMenu.PopupMenu = PopupMenu1
          ContentPopupMenu.UseBuiltInPopupMenu = False
          ControlBox.Control = cxGrid6
          EventOperations.DialogShowing = False
          EventOperations.ReadOnly = True
          EventPopupMenu.PopupMenu = PopupMenu1
          EventPopupMenu.UseBuiltInPopupMenu = False
          Font.Charset = DEFAULT_CHARSET
          Font.Color = clWindowText
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          OptionsCustomize.ControlsSizing = False
          OptionsCustomize.IntegralSizing = False
          OptionsView.ShowHints = False
          OptionsView.WorkDays = [dSunday, dMonday, dTuesday, dWednesday, dThursday, dFriday, dSaturday]
          OptionsView.WorkFinish = 0.666666666666666600
          PopupMenu = PopupMenu1
          TabOrder = 0
          Selection = 7
          Splitters = {
            C1020000FB0000005003000000010000BC02000001000000C102000006020000}
          StoredClientBounds = {01000000010000005003000006020000}
          object cxGrid6: TcxGrid
            Left = 0
            Top = 0
            Width = 143
            Height = 262
            Align = alClient
            TabOrder = 0
            object cxGridDBTableView3: TcxGridDBTableView
              Navigator.Buttons.CustomButtons = <>
              DataController.Summary.DefaultGroupSummaryItems = <>
              DataController.Summary.FooterSummaryItems = <>
              DataController.Summary.SummaryGroups = <>
              FilterRow.Visible = True
              OptionsCustomize.ColumnsQuickCustomization = True
              OptionsCustomize.ColumnsQuickCustomizationReordering = qcrEnabled
              OptionsData.Deleting = False
              OptionsData.Editing = False
              OptionsData.Inserting = False
              OptionsView.GroupByBox = False
              object cxGrid2DBTableView1BOJA: TcxGridDBColumn
                DataBinding.FieldName = 'BOJA'
                PropertiesClassName = 'TcxColorComboBoxProperties'
                Properties.CustomColors = <>
                Width = 29
              end
              object cxGrid2DBTableView1NAZIV: TcxGridDBColumn
                DataBinding.FieldName = 'NAZIV'
                Width = 300
              end
              object cxGrid2DBTableView1DENOVI: TcxGridDBColumn
                DataBinding.FieldName = 'DENOVI'
                Width = 70
              end
              object cxGrid2DBTableView1PlatenoNaziv: TcxGridDBColumn
                DataBinding.FieldName = 'PlatenoNaziv'
                Width = 100
              end
              object cxGrid2DBTableView1DogovorNaziv: TcxGridDBColumn
                DataBinding.FieldName = 'DogovorNaziv'
                Width = 100
              end
            end
            object cxGridLevel3: TcxGridLevel
              GridView = cxGridDBTableView3
            end
          end
        end
      end
    end
  end
  object cxGridPopupMenu1: TcxGridPopupMenu
    Grid = cxGrid1
    PopupMenus = <>
    Left = 664
  end
  object PopupMenu1: TPopupMenu
    Left = 784
    object N1: TMenuItem
      Action = aLinija
      Caption = #1040#1082#1090#1080#1074#1080#1088#1072#1112'/'#1044#1077#1072#1082#1090#1080#1074#1080#1088#1072#1112' '#1083#1080#1085#1080#1112#1072
    end
    object N2: TMenuItem
      Action = aZatvoriLinija
      ShortCut = 16474
    end
  end
  object dxBarManager1: TdxBarManager
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Segoe UI'
    Font.Style = []
    Categories.Strings = (
      'Default')
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    ImageOptions.Images = dmRes.cxSmallImages
    ImageOptions.LargeImages = dmRes.cxLargeImages
    PopupMenuLinks = <>
    UseSystemFont = True
    Left = 648
    Top = 65520
    PixelsPerInch = 96
    object bmRabotenNalog: TdxBar
      Caption = #1056#1040#1041#1054#1058#1045#1053' '#1053#1040#1051#1054#1043
      CaptionButtons = <>
      DockedLeft = 132
      DockedTop = 0
      FloatLeft = 715
      FloatTop = 157
      FloatClientWidth = 51
      FloatClientHeight = 120
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarSubItem2'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar2: TdxBar
      Caption = #1040#1082#1094#1080#1080
      CaptionButtons = <>
      DockedLeft = 307
      DockedTop = 0
      FloatLeft = 789
      FloatTop = 120
      FloatClientWidth = 51
      FloatClientHeight = 120
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarSubItem4'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar3: TdxBar
      Caption = #1048#1079#1083#1077#1079
      CaptionButtons = <>
      DockedLeft = 367
      DockedTop = 0
      FloatLeft = 718
      FloatTop = 62
      FloatClientWidth = 51
      FloatClientHeight = 80
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton8'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar4: TdxBar
      Caption = #1055#1077#1095#1072#1090#1077#1114#1077
      CaptionButtons = <>
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 676
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton12'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton15'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton9'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarLargeButton13'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1BarIzgledGrid: TdxBar
      Caption = #1058#1072#1073#1077#1083#1072
      CaptionButtons = <>
      DockedLeft = 363
      DockedTop = 0
      FloatLeft = 808
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLBtnSnimiIzgled'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarLBtnBrisiIzgled'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object bmStavki: TdxBar
      Caption = #1057#1058#1040#1042#1050#1040
      CaptionButtons = <>
      DockedLeft = 237
      DockedTop = 0
      FloatLeft = 1034
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarSubItem3'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar6: TdxBar
      Caption = '   '
      CaptionButtons = <>
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 1066
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          UserDefine = [udWidth]
          UserWidth = 75
          Visible = True
          ItemName = 'cbGodina'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarLargeButton2: TdxBarLargeButton
      Caption = #1040#1078#1091#1088#1080#1088#1072#1112
      Category = 0
      Visible = ivAlways
      LargeImageIndex = 12
    end
    object dxBarLargeButton3: TdxBarLargeButton
      Caption = #1041#1088#1080#1096#1080
      Category = 0
      Visible = ivAlways
      LargeImageIndex = 11
    end
    object dxBarLargeButton4: TdxBarLargeButton
      Action = aSnimiIzgled
      Category = 0
      ScreenTip = tipSnimiIzgled
    end
    object dxBarLargeButton5: TdxBarLargeButton
      Action = aZacuvajExcel
      Category = 0
      ScreenTip = tipZacuvajExcel
    end
    object dxBarLargeButton6: TdxBarLargeButton
      Action = aPecatiTabela
      Caption = #1055#1077#1095#1072#1090#1080' '#1090#1072#1073#1077#1083#1072
      Category = 0
    end
    object dxBarLargeButton7: TdxBarLargeButton
      Action = aHelp
      Category = 0
    end
    object dxBarLargeButton8: TdxBarLargeButton
      Action = aIzlez
      Category = 0
    end
    object dxBarLargeButton1: TdxBarLargeButton
      Caption = #1053#1086#1074' '#1056#1053
      Category = 0
      Hint = #1053#1086#1074' '#1056#1053' ('#1055#1086#1088#1072#1095#1082#1072')'
      Visible = ivAlways
      OnClick = aNovvExecute
      LargeImageIndex = 10
    end
    object dxBarLargeButton9: TdxBarLargeButton
      Action = aSnimiPecatenje
      Category = 0
      ScreenTip = tipSnimiKonf
    end
    object dxBarLargeButton11: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object ribbonFilterReset: TdxBarLargeButton
      Caption = #1056#1077#1089#1077#1090#1080#1088#1072#1112' '#1092#1080#1083#1090#1077#1088
      Category = 0
      Hint = #1056#1077#1089#1077#1090#1080#1088#1072#1112' '#1092#1080#1083#1090#1077#1088
      Visible = ivAlways
    end
    object dxBarLargeButton12: TdxBarLargeButton
      Action = aPodesuvanjePecatenje
      Category = 0
      ScreenTip = tipDizajnReport
    end
    object dxBarLargeButton13: TdxBarLargeButton
      Action = aBrisiPodesuvanjePecatenje
      Category = 0
      ScreenTip = tipBrisiKonf
    end
    object dxBarLargeButton14: TdxBarLargeButton
      Caption = #1048#1079#1073#1088#1080#1096#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      Category = 0
      Visible = ivAlways
      LargeImageIndex = 40
    end
    object dxBarLargeButton15: TdxBarLargeButton
      Action = aPageSetup
      Category = 0
      ScreenTip = tipSetiranjeStrana
    end
    object dxBarLargeButton16: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object dxBarLargeButton17: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object cxBarEditItem1: TcxBarEditItem
      Caption = 'New Item'
      Category = 0
      Hint = 'New Item'
      Visible = ivAlways
      PropertiesClassName = 'TcxCheckBoxProperties'
    end
    object dxBarSubItem1: TdxBarSubItem
      Caption = #1055#1077#1095#1072#1090#1077#1114#1077
      Category = 0
      Visible = ivAlways
      ImageIndex = 30
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton6'
        end>
    end
    object dxBarLBtnSnimiIzgled: TdxBarLargeButton
      Action = aSnimiIzgled
      Category = 0
    end
    object dxBarLBtnBrisiIzgled: TdxBarLargeButton
      Action = aBrisiIzgled
      Category = 0
    end
    object dxBarLargeButton10: TdxBarLargeButton
      Caption = #1053#1086#1074#1072
      Category = 0
      Visible = ivAlways
      LargeImageIndex = 10
    end
    object dxBarLargeButton18: TdxBarLargeButton
      Caption = #1040#1078#1091#1088#1080#1088#1072#1112
      Category = 0
      Visible = ivAlways
      LargeImageIndex = 12
    end
    object dxBarLargeButton19: TdxBarLargeButton
      Caption = #1041#1088#1080#1096#1080
      Category = 0
      Visible = ivAlways
      LargeImageIndex = 11
    end
    object dxBarLargeButton20: TdxBarLargeButton
      Action = aOtvoreniRN
      Category = 0
      SyncImageIndex = False
      ImageIndex = 2
    end
    object dxBarLargeButton21: TdxBarLargeButton
      Action = aSiteRN
      Category = 0
    end
    object dxBarDateCombo1: TdxBarDateCombo
      Caption = #1043#1086#1076#1080#1085#1072
      Category = 0
      Hint = #1043#1086#1076#1080#1085#1072
      Visible = ivAlways
    end
    object cbGodina: TcxBarEditItem
      Caption = #1043#1086#1076#1080#1085#1072
      Category = 0
      Hint = #1043#1086#1076#1080#1085#1072
      Visible = ivAlways
      OnChange = cbGodinaChange
      PropertiesClassName = 'TcxComboBoxProperties'
      Properties.DropDownSizeable = True
    end
    object cxBarEditItem2: TcxBarEditItem
      Caption = #1053#1086#1074' '#1056#1053' '#1079#1072' : '
      Category = 0
      Hint = #1053#1086#1074' '#1056#1053' '#1079#1072' : '
      Visible = ivAlways
      PropertiesClassName = 'TcxComboBoxProperties'
      Properties.Items.Strings = (
        #1052#1040#1064#1048#1053#1057#1050#1054' '#1054#1044#1044#1045#1051#1045#1053#1048#1045)
    end
    object cbNovRE: TdxBarLookupCombo
      Caption = #1053#1086#1074' '#1056#1053' '#1079#1072' '#1056#1045': '
      Category = 0
      Hint = #1053#1086#1074' '#1056#1053' '#1079#1072' '#1056#1072#1073#1086#1090#1085#1072' '#1077#1076#1080#1085#1080#1094#1072' '
      Visible = ivAlways
      OnChange = cbNovREChange
      Glyph.SourceDPI = 96
      Glyph.Data = {
        424D360400000000000036000000280000001000000010000000010020000000
        000000000000C40E0000C40E00000000000000000000FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00
        FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00FF00000000FF000000FF0000
        00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
        00FF000000FF000000FF000000FF000000FFFF00FF00000000FFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFF00FF00000000FFFFFFFFFF8080
        80FF808080FFFFFFFFFF000000FFFFFFFFFF808080FF808080FF808080FF8080
        80FF808080FF808080FFFFFFFFFF000000FFFF00FF00000000FFFF0000FFFF00
        00FFFF0000FFFF0000FF000000FFFF0000FFFF0000FFFF0000FFFF0000FFFF00
        00FFFF0000FFFF0000FFFF0000FF000000FFFF00FF00000000FFFF0000FF8080
        80FF808080FFFF0000FF000000FFFF0000FF808080FF808080FF808080FF8080
        80FF808080FF808080FFFF0000FF000000FFFF00FF00000000FFFF0000FFFF00
        00FFFF0000FFFF0000FF000000FFFF0000FFFF0000FFFF0000FFFF0000FFFF00
        00FFFF0000FFFF0000FFFF0000FF000000FFFF00FF00000000FFFFFFFFFF8080
        80FF808080FFFFFFFFFF000000FFFFFFFFFF808080FF808080FF808080FF8080
        80FF808080FF808080FFFFFFFFFF000000FFFF00FF00000000FFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFF00FF00000000FFFFFFFFFF8080
        80FF808080FFFFFFFFFF000000FFFFFFFFFF808080FF808080FF808080FF8080
        80FF808080FF808080FFFFFFFFFF000000FFFF00FF00000000FFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFF000000FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF
        FFFFFFFFFFFFFFFFFFFFFFFFFFFF000000FFFF00FF00000000FF000000FF0000
        00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
        00FF000000FF000000FF000000FF000000FFFF00FF00000000FFFFFFFFFFFFFF
        FFFFFFFFFFFFFF0000FFFF0000FFFF0000FFFF0000FFFFFFFFFFFFFFFFFFFFFF
        FFFF000000FFFF00FF00FF00FF00FF00FF00FF00FF00000000FFFFFFFFFF8080
        80FF808080FF808080FF808080FF808080FF808080FFFFFFFFFFFFFFFFFFFFFF
        FFFF000000FFFF00FF00FF00FF00FF00FF00FF00FF00000000FFFFFFFFFFFFFF
        FFFFFFFFFFFFFF0000FFFF0000FFFF0000FFFF0000FFFFFFFFFFFFFFFFFFFFFF
        FFFF000000FFFF00FF00FF00FF00FF00FF00FF00FF00000000FF000000FF0000
        00FF000000FF000000FF000000FF000000FF000000FF000000FF000000FF0000
        00FF000000FFFF00FF00FF00FF00FF00FF00FF00FF00}
      KeyField = 'ID'
      ListField = 'ID;naziv'
      ListFieldIndex = 1
      ListSource = dm.dsRE
      RowCount = 7
    end
    object cxBarEditItem3: TcxBarEditItem
      Caption = 'New Item'
      Category = 0
      Hint = 'New Item'
      Visible = ivAlways
      OnExit = cxDBTextEditAllExit
      PropertiesClassName = 'TcxLookupComboBoxProperties'
      Properties.KeyFieldNames = 'ID'
      Properties.ListColumns = <
        item
          FieldName = 'ID'
        end
        item
          FieldName = 'naziv'
        end>
      Properties.ListFieldIndex = 1
      Properties.ListSource = dmMat.dsRE
    end
    object dxBarLargeButton22: TdxBarLargeButton
      Action = aZatvoriRN
      Category = 0
    end
    object dxBarLargeButton23: TdxBarLargeButton
      Action = aIzberiPoracka
      Category = 0
    end
    object dxBarLargeButton24: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object dxBarButton1: TdxBarButton
      Action = aPecatiRN
      Category = 0
    end
    object dxBarButton2: TdxBarButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object dxBarButton3: TdxBarButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object dxBarButton4: TdxBarButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object dxBarButton5: TdxBarButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object dxBarButton6: TdxBarButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object dxBarButton7: TdxBarButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object dxBarButton8: TdxBarButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object dxBarButton9: TdxBarButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object dxBarButton10: TdxBarButton
      Caption = #1040#1078#1091#1088#1080#1088#1072#1112
      Category = 0
      Visible = ivAlways
      ImageIndex = 12
      ShortCut = 117
      OnClick = aAzurirajExecute
    end
    object dxBarButton11: TdxBarButton
      Caption = #1040#1078#1091#1088#1080#1088#1072#1112' (F6)'
      Category = 0
      Visible = ivAlways
      ImageIndex = 12
    end
    object dxBarButton12: TdxBarButton
      Caption = #1041#1088#1080#1096#1080' (F8)'
      Category = 0
      Visible = ivAlways
      ImageIndex = 11
    end
    object dxBarSubItem2: TdxBarSubItem
      Caption = #1057#1090#1072#1090#1091#1089#1080' '#1085#1072' '#1056#1053
      Category = 0
      Visible = ivAlways
      ImageIndex = 89
      LargeImageIndex = 66
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton20'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton21'
        end
        item
          Visible = True
          ItemName = 'dxBarButton24'
        end
        item
          Position = ipContinuesRow
          Visible = True
          ItemName = 'dxBarLargeButton22'
        end>
    end
    object dxBarButton13: TdxBarButton
      Caption = #1053#1086#1074#1072' (F5)'
      Category = 0
      Visible = ivAlways
      ImageIndex = 10
    end
    object dxBarButton14: TdxBarButton
      Caption = #1040#1078#1091#1088#1080#1088#1072#1112' (F6)'
      Category = 0
      Visible = ivAlways
      ImageIndex = 12
    end
    object dxBarButton15: TdxBarButton
      Caption = #1041#1088#1080#1096#1080' (F8)'
      Category = 0
      Visible = ivAlways
      ImageIndex = 11
    end
    object dxBarButton16: TdxBarButton
      Caption = #1053#1086#1074' (F5)'
      Category = 0
      Visible = ivAlways
      ImageIndex = 10
      OnClick = aNovvExecute
    end
    object dxBarLargeButton25: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object dxBarSubItem3: TdxBarSubItem
      Caption = #1057#1090#1072#1090#1091#1089#1080' '#1089#1090#1072#1074#1082#1080
      Category = 0
      Visible = ivAlways
      ImageIndex = 54
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarButton17'
        end>
    end
    object dxBarButton17: TdxBarButton
      Action = aZatvoriStavka
      Category = 0
      ImageIndex = 7
    end
    object bbRNPoracka: TdxBarButton
      Action = aIzberiPoracka
      Caption = #1053#1086#1074'/'#1076#1086#1076#1072#1076#1080' '#1055#1086#1088#1072#1095#1082#1072' '#1074#1086' '#1056#1053
      Category = 0
      ImageIndex = 54
    end
    object dxBarSubItem4: TdxBarSubItem
      Caption = #1040#1082#1094#1080#1080
      Category = 0
      Visible = ivAlways
      LargeImageIndex = 60
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton26'
        end
        item
          Visible = True
          ItemName = 'dxBarButton18'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton5'
        end
        item
          Visible = True
          ItemName = 'dxBarSubItem1'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton7'
        end>
    end
    object dxBarSubItem5: TdxBarSubItem
      Caption = 'New SubItem'
      Category = 0
      Visible = ivAlways
      ItemLinks = <>
    end
    object dxBarButton19: TdxBarButton
      Caption = #1053#1086#1074#1072
      Category = 0
      Visible = ivAlways
      ImageIndex = 10
    end
    object dxBarButton20: TdxBarButton
      Caption = #1040#1078#1091#1088#1080#1088#1072#1112
      Category = 0
      Visible = ivAlways
      ImageIndex = 12
    end
    object dxBarButton21: TdxBarButton
      Caption = #1041#1088#1080#1096#1080
      Category = 0
      Visible = ivAlways
      ImageIndex = 11
    end
    object bbIzberiStavki: TdxBarButton
      Action = aIzberiStavkiRN
      Caption = #1048#1079#1073#1077#1088#1080' '#1089#1090#1072#1074#1082#1080' '#1086#1076' '#1056#1053
      Category = 0
      ImageIndex = 16
    end
    object dxBarSubItem6: TdxBarSubItem
      Caption = 'New SubItem'
      Category = 0
      Visible = ivAlways
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarButton23'
        end>
    end
    object dxBarButton23: TdxBarButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object dxBarButton24: TdxBarButton
      Action = aPogledZavisniRN
      Category = 0
      Hint = #1056#1072#1073#1086#1090#1085#1080' '#1085#1072#1083#1086#1079#1080', '#1082#1086#1080' '#1089#1077' '#1079#1072#1074#1080#1089#1085#1080' '#1086#1076' '#1076#1088#1091#1075', '#1087#1088'. '#1056#1053' '#1079#1072' '#1052#1072#1096#1080#1085#1089#1082#1086
    end
    object dxBarLargeButton26: TdxBarLargeButton
      Action = aPecatiRN
      Category = 0
    end
    object dxBarButton25: TdxBarButton
      Action = aZavrsiRN
      Category = 0
      Visible = ivNever
    end
    object dxBarButton18: TdxBarButton
      Action = aLagerArtikli
      Category = 0
    end
    object dxBarButton22: TdxBarButton
      Action = aPogledPorackaRN
      Category = 0
    end
  end
  object ActionList1: TActionList
    Images = dmRes.cxSmallImages
    Left = 376
    Top = 65520
    object aSnimiIzgled: TAction
      Caption = #1057#1085#1080#1084#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      ImageIndex = 9
      ShortCut = 24659
      OnExecute = aSnimiIzgledExecute
    end
    object aZacuvajExcel: TAction
      Caption = #1047#1072#1095#1091#1074#1072#1112' '#1074#1086' Excel'
      ImageIndex = 9
      ShortCut = 24645
      OnExecute = aZacuvajExcelExecute
    end
    object aPecatiTabela: TAction
      Caption = #1055#1077#1095#1072#1090#1080
      ImageIndex = 30
      OnExecute = aPecatiTabelaExecute
    end
    object aHelp: TAction
      Caption = #1055#1086#1084#1086#1096
      ImageIndex = 24
    end
    object aIzlez: TAction
      Caption = #1048#1079#1083#1077#1079
      ImageIndex = 25
      OnExecute = aIzlezExecute
    end
    object aZapisi: TAction
      Caption = #1047#1072#1087#1080#1096#1080
      ImageIndex = 7
      ShortCut = 120
    end
    object aOtkazi: TAction
      Caption = #1054#1090#1082#1072#1078#1080
      ImageIndex = 21
      ShortCut = 27
      OnExecute = aOtkaziExecute
    end
    object aRefresh: TAction
      Caption = 'aRefresh'
      ImageIndex = 18
      ShortCut = 118
      OnExecute = aRefreshExecute
    end
    object aSnimiPecatenje: TAction
      Caption = #1057#1085#1080#1084#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      ImageIndex = 39
      OnExecute = aSnimiPecatenjeExecute
    end
    object aPodesuvanjePecatenje: TAction
      Caption = #1044#1080#1079#1072#1112#1085' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090
      ImageIndex = 38
      OnExecute = aPodesuvanjePecatenjeExecute
    end
    object aBrisiPodesuvanjePecatenje: TAction
      Caption = #1041#1088#1080#1096#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      ImageIndex = 37
      OnExecute = aBrisiPodesuvanjePecatenjeExecute
    end
    object aPageSetup: TAction
      Caption = #1057#1077#1090#1080#1088#1072#1114#1077' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072
      ImageIndex = 38
      OnExecute = aPageSetupExecute
    end
    object aFormConfig: TAction
      Caption = 'aFormConfig'
      ShortCut = 24699
      OnExecute = aFormConfigExecute
    end
    object aBrisiIzgled: TAction
      Caption = #1041#1088#1080#1096#1080' '#1075#1086' '#1079#1072#1095#1091#1074#1072#1085#1080#1086#1090' '#1080#1079#1075#1083#1077#1076
      ImageIndex = 56
      OnExecute = aBrisiIzgledExecute
    end
    object aListaRN: TAction
      Caption = #1051#1080#1089#1090#1072' '#1085#1072' '#1056#1053
      ImageIndex = 8
      OnExecute = aListaRNExecute
    end
    object aPogledRN: TAction
      Caption = #1055#1086#1075#1083#1077#1076' '#1085#1072' '#1056#1053
      OnExecute = aPogledRNExecute
    end
    object aSiteRN: TAction
      Caption = #1057#1080#1090#1077' '#1056#1053
      ImageIndex = 1
      OnExecute = aSiteRNExecute
    end
    object aOtvoreniRN: TAction
      Caption = #1054#1090#1074#1086#1088#1077#1085#1080' '#1056#1053
      ImageIndex = 2
      OnExecute = aOtvoreniRNExecute
    end
    object aSmeniStatus: TAction
      Caption = 'aSmeniStatus'
      OnExecute = aSmeniStatusExecute
    end
    object aRNMasinsko: TAction
      Caption = #1056#1053' '#1048#1085#1090#1077#1088#1077#1085
    end
    object aZatvoriRN: TAction
      Caption = #1047#1072#1090#1074#1086#1088#1080' '#1056#1053
      ImageIndex = 6
      OnExecute = aZatvoriRNExecute
    end
    object aZatvoriRNButton: TAction
      Caption = #1047#1072#1090#1074#1086#1088#1080
    end
    object aIzberiPoracka: TAction
      Caption = #1048#1079#1073#1077#1088#1080' '#1055#1086#1088#1072#1095#1082#1072
      OnExecute = aIzberiPorackaExecute
    end
    object aPecatiRN: TAction
      Caption = #1055#1077#1095#1072#1090#1080' '#1056#1053
      ImageIndex = 30
      ShortCut = 121
      OnExecute = aPecatiRNExecute
    end
    object aZatvoriStavka: TAction
      Caption = #1047#1072#1090#1074#1086#1088#1080' '#1089#1090#1072#1074#1082#1072
      OnExecute = aZatvoriStavkaExecute
    end
    object aIzberiStavkiRN: TAction
      Caption = 'aIzberiStavkiRN'
      OnExecute = aIzberiStavkiRNExecute
    end
    object aPogledZavisniRN: TAction
      Caption = #1047#1072#1074#1080#1089#1085#1080' '#1056#1053
      ImageIndex = 18
      OnExecute = aPogledZavisniRNExecute
    end
    object aDizajnerRN: TAction
      Caption = 'aDizajnerRN'
      ShortCut = 24697
      OnExecute = aDizajnerRNExecute
    end
    object aZavrsiRN: TAction
      Caption = #1047#1072#1074#1088#1096#1080' '#1056#1053
      ImageIndex = 0
      OnExecute = aZavrsiRNExecute
    end
    object aLagerArtikli: TAction
      Caption = #1051#1072#1075#1077#1088' '#1085#1072' '#1040#1088#1090#1080#1082#1083#1080
      ImageIndex = 33
      OnExecute = aLagerArtikliExecute
    end
    object aDodadiPRN: TAction
      Caption = #1044#1086#1076#1072#1076#1080' '#1055#1086#1088#1072#1095#1082#1072' '#1074#1086' '#1056#1053
      OnExecute = aDodadiPRNExecute
    end
    object aLinija: TAction
      Caption = #1040#1082#1090#1080#1074#1085#1072' '#1083#1080#1085#1080#1112#1072
      ShortCut = 16449
      OnExecute = aLinijaExecute
    end
    object aZatvoriLinija: TAction
      Caption = #1047#1072#1090#1074#1086#1088#1080' '#1083#1080#1085#1080#1112#1072
      OnExecute = aZatvoriLinijaExecute
    end
    object aPogledPorackaRN: TAction
      Caption = #1055#1086#1088#1072#1095#1082#1080' '#1074#1086' '#1056#1053
      ImageIndex = 17
      OnExecute = aPogledPorackaRNExecute
    end
    object aBrLinija: TAction
      Caption = #1041#1088#1086#1112' '#1085#1072' '#1083#1080#1085#1080#1112#1072
      OnExecute = aBrLinijaExecute
    end
  end
  object dxComponentPrinter1: TdxComponentPrinter
    CurrentLink = dxComponentPrinter1Link1
    Version = 0
    Left = 976
    Top = 16
    object dxComponentPrinter1Link1: TdxGridReportLink
      Component = cxGrid1
      DateFormat = 0
      PageNumberFormat = pnfNumeral
      PrinterPage.DMPaper = 9
      PrinterPage.Footer = 6350
      PrinterPage.GrayShading = True
      PrinterPage.Header = 6350
      PrinterPage.Margins.Bottom = 12700
      PrinterPage.Margins.Left = 12700
      PrinterPage.Margins.Right = 12700
      PrinterPage.Margins.Top = 12700
      PrinterPage.Orientation = poLandscape
      PrinterPage.PageFooter.RightTitle.Strings = (
        #1057#1090#1088'.[Page #] '#1086#1076' [Total Pages]')
      PrinterPage.PageSize.X = 210000
      PrinterPage.PageSize.Y = 297000
      PrinterPage._dxMeasurementUnits_ = 0
      PrinterPage._dxLastMU_ = 2
      ReportDocument.CreationDate = 40336.425359629630000000
      TimeFormat = 0
      OptionsFormatting.UseNativeStyles = True
      OptionsOnEveryPage.Caption = False
      OptionsOnEveryPage.FilterBar = False
      OptionsView.Caption = False
      OptionsView.FilterBar = False
      StyleRepository = dmRes.cxStyleRepositoryPrinting
      Styles.StyleSheet = dmRes.dxGridReportLinkStyleSheet
      BuiltInReportLink = True
    end
  end
  object dxBarScreenTipRepository1: TdxBarScreenTipRepository
    Left = 584
    Top = 65520
    PixelsPerInch = 96
    object tipSnimiIzgled: TdxBarScreenTip
      Header.Text = #1057#1085#1080#1084#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      Description.Text = #1047#1072#1095#1091#1074#1072#1112' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090' '#1085#1072' '#1092#1086#1088#1084#1072#1090#1072' '#1080' '#1085#1072' '#1090#1072#1073#1077#1083#1072#1090#1072
    end
    object tipZacuvajExcel: TdxBarScreenTip
      Header.Text = #1047#1072#1095#1091#1074#1072#1112' '#1074#1086' Excel'
      Description.Text = #1057#1085#1080#1084#1080' '#1075#1080' '#1087#1086#1076#1072#1090#1086#1094#1080#1090#1077' '#1086#1076' '#1090#1072#1073#1077#1083#1072#1090#1072' '#1074#1086' Excel '#1092#1086#1088#1084#1072#1090
    end
    object tipDizajnReport: TdxBarScreenTip
      Header.Text = #1044#1080#1079#1072#1112#1085' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090
      Description.Text = #1055#1086#1076#1077#1089#1091#1074#1072#1114#1072' '#1085#1072' '#1080#1079#1075#1083#1077#1076#1086#1090' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090' '#1085#1072' '#1090#1072#1073#1077#1083#1072#1090#1072
    end
    object tipSetiranjeStrana: TdxBarScreenTip
      Header.Text = #1057#1077#1090#1080#1088#1072#1114#1077' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072
      Description.Text = #1055#1086#1076#1077#1089#1091#1074#1072#1114#1077' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072' '#1085#1072' '#1082#1086#1112#1072' '#1116#1077' '#1089#1077' '#1087#1077#1095#1072#1090#1080' '#1090#1072#1073#1077#1083#1072#1090#1072
    end
    object tipSnimiKonf: TdxBarScreenTip
      Header.Text = #1057#1085#1080#1084#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      Description.Text = #1047#1072#1095#1091#1074#1072#1112' '#1075#1080' '#1087#1086#1076#1077#1089#1091#1074#1072#1114#1072#1090#1072' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090' '#1080' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072' '#1079#1072' '#1087#1077#1095#1072#1090#1077#1114#1077
    end
    object tipBrisiKonf: TdxBarScreenTip
      Header.Text = #1041#1088#1080#1096#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      Description.Text = #1048#1079#1073#1088#1080#1096#1080' '#1075#1080' '#1087#1086#1076#1077#1089#1091#1074#1072#1114#1072#1090#1072' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090' '#1080' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072' '#1079#1072' '#1087#1077#1095#1072#1090#1077#1114#1077
    end
  end
  object OpenPictureDialog1: TOpenPictureDialog
    Left = 296
    Top = 65520
  end
  object cxGridPopupMenu2: TcxGridPopupMenu
    Grid = cxGrid2
    PopupMenus = <>
    Left = 864
    Top = 8
  end
  object cxGridPopupMenu3: TcxGridPopupMenu
    Grid = cxGrid3
    PopupMenus = <>
    Left = 928
    Top = 16
  end
  object cxGridPopupMenu4: TcxGridPopupMenu
    Grid = cxGrid5
    PopupMenus = <>
    Left = 480
    Top = 65528
  end
  object cxGridPopupMenu5: TcxGridPopupMenu
    Grid = cxGrid7
    PopupMenus = <>
    Left = 832
    Top = 65528
  end
  object cxPropertiesStore1: TcxPropertiesStore
    Components = <>
    StorageName = 'cxPropertiesStore1'
  end
  object qArtikalMerka: TpFIBQuery
    Transaction = dmKon.qTransaction
    Database = dmKon.fibBaza
    SQL.Strings = (
      'select a.merka,a.barkod'
      'from mtr_artikal a'
      'where a.artvid = :vid_artikal and a.id = :artikal')
    Left = 960
    Top = 232
    qoStartTransaction = True
  end
  object qMaxBroj: TpFIBQuery
    Transaction = dmKon.qTransaction
    Database = dmKon.fibBaza
    SQL.Strings = (
      'select max(r.broj) broj'
      'from man_raboten_nalog r'
      'where r.godina = :godina')
    Left = 840
    Top = 232
    qoAutoCommit = True
    qoStartTransaction = True
  end
  object tblRE: TpFIBDataSet
    RefreshSQL.Strings = (
      'select r.id, r.naziv'
      'from mat_re r'
      'inner join sys_user_re_app a on a.re = r.id'
      'where(  a.username = :u'
      '     ) and (     R.ID = :OLD_ID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      'select r.id, r.naziv'
      'from mat_re r'
      'inner join sys_user_re_app a on a.re = r.id'
      'where a.username = :u')
    AutoUpdateOptions.AutoParamsToFields = True
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 840
    Top = 77
    poSQLINT64ToBCD = True
    object tblREID: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072
      FieldName = 'ID'
    end
    object tblRENAZIV: TFIBStringField
      DisplayLabel = #1053#1072#1079#1080#1074
      FieldName = 'NAZIV'
      Size = 255
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object dsRE: TDataSource
    AutoEdit = False
    DataSet = tblRE
    Left = 896
    Top = 77
  end
  object pImaLinija: TpFIBQuery
    Transaction = dmKon.qTransaction
    Database = dmKon.fibBaza
    SQL.Strings = (
      '--'#1082#1086#1085#1090#1088#1086#1083#1072#1090#1072' '#1076#1072' '#1073#1080#1076#1077' '#1085#1072' '#1085#1080#1074#1086' '#1085#1072' '#1072#1082#1090#1080#1074#1077#1085' '#1056#1072#1073#1086#1090#1077#1085' '#1085#1072#1083#1086#1075
      '--select'
      '--   max (case when rns.linija = 1 then 1 else 0 end) linija'
      '--from man_raboten_nalog_stavka rns'
      '--where rns.status = 1'
      ''
      '--'#1082#1086#1085#1090#1088#1086#1083#1072#1090#1072' '#1076#1072' '#1073#1080#1076#1077' '#1085#1072' '#1085#1080#1074#1086' '#1085#1072' '#1072#1082#1090#1080#1074#1077#1085' '#1056#1072#1073#1086#1090#1077#1085' '#1085#1072#1083#1086#1075
      'Select RNS.LINIJA'
      'From MAN_RABOTEN_NALOG_STAVKA RNS'
      'Inner Join MAN_PROIZVODSTVENA_LINIJA ML On ML.ID = RNS.LINIJA'
      'Where RNS.LINIJA = :LINIJA And'
      '      RNS.STATUS_LINIJA = 1 And'
      '      RNS.STATUS = 1   ')
    Left = 896
    Top = 232
    qoAutoCommit = True
    qoStartTransaction = True
  end
end
