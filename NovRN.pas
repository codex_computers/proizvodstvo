unit NovRN;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxContainer, cxEdit, dxSkinsCore, dxSkinBlack,
  dxSkinBlue, dxSkinBlueprint, dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom,
  dxSkinDarkSide, dxSkinDevExpressDarkStyle, dxSkinDevExpressStyle, dxSkinFoggy,
  dxSkinGlassOceans, dxSkinHighContrast, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMetropolis,
  dxSkinMetropolisDark, dxSkinMoneyTwins, dxSkinOffice2007Black,
  dxSkinOffice2007Blue, dxSkinOffice2007Green, dxSkinOffice2007Pink,
  dxSkinOffice2007Silver, dxSkinOffice2010Black, dxSkinOffice2010Blue,
  dxSkinOffice2010Silver, dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray,
  dxSkinOffice2013White, dxSkinPumpkin, dxSkinSeven, dxSkinSevenClassic,
  dxSkinSharp, dxSkinSharpPlus, dxSkinSilver, dxSkinSpringTime, dxSkinStardust,
  dxSkinSummer2008, dxSkinTheAsphaltWorld, dxSkinsDefaultPainters,
  dxSkinValentine, dxSkinVS2010, dxSkinWhiteprint, dxSkinXmas2008Blue,
  cxMaskEdit, cxDropDownEdit, cxCalendar, cxDBEdit, cxTextEdit, cxLabel,
  Vcl.ComCtrls, Vcl.ExtCtrls, Vcl.Menus, Vcl.StdCtrls, cxButtons,
  System.Actions, Vcl.ActnList, Data.DB, FIBQuery, pFIBQuery,
  dxSkinOffice2016Colorful, dxSkinOffice2016Dark, dxSkinVisualStudio2013Blue,
  dxSkinVisualStudio2013Dark, dxSkinVisualStudio2013Light;

type
  TfrmNovRN = class(TForm)
    Panel1: TPanel;
    stat1: TStatusBar;
    cxlblBroj: TcxLabel;
    txtNaziv: TcxDBTextEdit;
    txtPlanDatumPoc: TcxDBDateEdit;
    cxlbl1: TcxLabel;
    btnZapisi: TcxButton;
    btnOtkazi: TcxButton;
    actnlst1: TActionList;
    aZapisi: TAction;
    aOtkazi: TAction;
    qMaxBroj: TpFIBQuery;
    cxlbl2: TcxLabel;
    txtBroj: TcxTextEdit;
    procedure EnterKakoTab(Sender: TObject; var Key: Word; Shift: TShiftState);
    procedure cxDBTextEditAllExit(Sender: TObject);
    procedure cxDBTextEditAllEnter(Sender: TObject);
    procedure aZapisiExecute(Sender: TObject);
    procedure aOtkaziExecute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmNovRN: TfrmNovRN;
  godina_rn : Integer;

implementation

{$R *.dfm}

uses dmUnit, dmKonekcija, dmMaticni, dmResources;

procedure TfrmNovRN.aOtkaziExecute(Sender: TObject);
begin
  dm.tblRabotenNalog.Cancel;
  Close;
end;

procedure TfrmNovRN.aZapisiExecute(Sender: TObject);
begin
   if dm.tblRabotenNalog.State <> dsBrowse then
   begin
      dm.tblRabotenNalogBROJ.AsString := txtNaziv.Text+'/'+txtBroj.Text;
      dm.tblRabotenNalogID_RE.Value := dmKon.UserRE;
      dm.tblRabotenNalogGODINA.Value := godina_rn;
   //  dm.tblRabotenNalogID_PORACKA.Value := dm.tblRasredelbaID_PORACKA.Value;
      dm.tblRabotenNalog.Post;
     //�� ���������, �� �� ������ �� �� ����� � �������
      dm.qUpdateRNPoracka.Close;
      dm.qUpdateRNPoracka.ParamByName('id_rn').Value := dm.tblRabotenNalogID.Value;
      dm.qUpdateRNPoracka.ParamByName('id_poracka').Value := dm.tblRasredelbaID_PORACKA.Value;
      dm.qUpdateRNPoracka.ExecQuery;

      Close;
   end;

end;

procedure TfrmNovRN.cxDBTextEditAllEnter(Sender: TObject);
begin
  TEdit(Sender).Color:=clSkyBlue;
end;

procedure TfrmNovRN.cxDBTextEditAllExit(Sender: TObject);
begin
  TEdit(Sender).Color:=clWhite;
end;

procedure TfrmNovRN.EnterKakoTab(Sender: TObject; var Key: Word;
  Shift: TShiftState);
begin
    case Key of
        VK_DOWN:
        begin
          PostMessage(Handle,WM_NextDlgCtl,0,0);
          key:=0;
        end;
        VK_UP:
        begin
          PostMessage(Handle,WM_NextDlgCtl,1,0);
          key:=0;
        end;
        VK_RETURN:
        begin
          PostMessage(Handle,WM_NextDlgCtl,0,0);
        end;
    end;

end;

end.
