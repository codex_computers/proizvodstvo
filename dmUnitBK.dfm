object dmBK: TdmBK
  OldCreateOrder = False
  Height = 350
  Width = 667
  object dsEtiketa: TDataSource
    DataSet = tblEtiketa
    Left = 48
    Top = 32
  end
  object tblEtiketa: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE MAN_ETIKETA'
      'SET '
      '    ID_RABOTEN_NALOG = :ID_RABOTEN_NALOG,'
      '    DATUM = :DATUM,'
      '    TIP_ETIKETA = :TIP_ETIKETA,'
      '    SSCC = :SSCC'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    MAN_ETIKETA'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO MAN_ETIKETA('
      '    ID,'
      '    ID_RABOTEN_NALOG,'
      '    DATUM,'
      '    TIP_ETIKETA,'
      '    SSCC'
      ')'
      'VALUES('
      '    :ID,'
      '    :ID_RABOTEN_NALOG,'
      '    :DATUM,'
      '    :TIP_ETIKETA,'
      '    :SSCC'
      ')')
    RefreshSQL.Strings = (
      'select'
      '    me.id,'
      '    me.id_raboten_nalog,'
      '    rn.broj br_rn,'
      '    rn.datum_planiran_pocetok datum_rn,'
      '    rn.godina,'
      '    me.datum,'
      '    me.tip_etiketa,'
      '    me.sscc--,'
      '   -- me.id_trailer'
      'from man_etiketa me'
      'left join man_raboten_nalog rn on rn.id = me.id_raboten_nalog'
      ''
      ' WHERE '
      '        ME.ID = :OLD_ID'
      '    ')
    SelectSQL.Strings = (
      'select'
      '    me.id,'
      '    me.id_raboten_nalog,'
      '    rn.broj br_rn,'
      '    rn.datum_planiran_pocetok datum_rn,'
      '    rn.godina,'
      '    me.datum,'
      '    me.tip_etiketa,'
      '    me.sscc--,'
      '   -- me.id_trailer'
      'from man_etiketa me'
      'left join man_raboten_nalog rn on rn.id = me.id_raboten_nalog'
      'order by me.id desc')
    AutoUpdateOptions.UpdateTableName = 'MAN_ETIKETA'
    AutoUpdateOptions.KeyFields = 'ID'
    AutoUpdateOptions.GeneratorName = 'GEN_MAN_ETIKETA_ID'
    AutoUpdateOptions.WhenGetGenID = wgBeforePost
    AutoUpdateOptions.AutoParamsToFields = True
    BeforeDelete = AllTableBeforeDelete
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 120
    Top = 32
    object tblEtiketaID: TFIBIntegerField
      FieldName = 'ID'
    end
    object tblEtiketaID_RABOTEN_NALOG: TFIBBCDField
      FieldName = 'ID_RABOTEN_NALOG'
      Size = 0
    end
    object tblEtiketaBR_RN: TFIBStringField
      FieldName = 'BR_RN'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblEtiketaDATUM_RN: TFIBDateTimeField
      FieldName = 'DATUM_RN'
    end
    object tblEtiketaGODINA: TFIBIntegerField
      FieldName = 'GODINA'
    end
    object tblEtiketaDATUM: TFIBDateTimeField
      FieldName = 'DATUM'
    end
    object tblEtiketaTIP_ETIKETA: TFIBIntegerField
      FieldName = 'TIP_ETIKETA'
    end
    object tblEtiketaSSCC: TFIBStringField
      FieldName = 'SSCC'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object dsEtiketaStavka: TDataSource
    DataSet = tblEtiketaStavka
    Left = 48
    Top = 96
  end
  object tblEtiketaStavka: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE MAN_ETIKETA_STAVKA'
      'SET '
      '    ID_ETIKETA = :ID_ETIKETA,'
      '    VID_ARTIKAL = :VID_ARTIKAL,'
      '    ARTIKAL = :ARTIKAL,'
      '    GTIN = :GTIN,'
      '    SSCC = :SSCC,'
      '    SERISKI_BROJ = :SERISKI_BROJ,'
      '    BARKOD = :BARKOD,'
      '    LOT = :LOT,'
      '    KOLICINA_BROJ = :KOLICINA_BROJ,'
      '    BR_KUTIJA = :BR_KUTIJA,'
      '    ID_RNS = :ID_RNS,'
      '    imenitel = :imenitel,'
      '    id_sos_del = :id_sos_del'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    MAN_ETIKETA_STAVKA'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO MAN_ETIKETA_STAVKA('
      '    ID,'
      '    ID_ETIKETA,'
      '    VID_ARTIKAL,'
      '    ARTIKAL,'
      '    GTIN,'
      '    SSCC,'
      '    SERISKI_BROJ,'
      '    BARKOD,'
      '    LOT,'
      '    KOLICINA_BROJ,'
      '    BR_KUTIJA,'
      '    ID_RNS ,'
      '    imenitel,'
      '    id_sos_del'
      ')'
      'VALUES('
      '    :ID,'
      '    :ID_ETIKETA,'
      '    :VID_ARTIKAL,'
      '    :ARTIKAL,'
      '    :GTIN,'
      '    :SSCC,'
      '    :SERISKI_BROJ,'
      '    :BARKOD,'
      '    :LOT,'
      '    :KOLICINA_BROJ,'
      '    :BR_KUTIJA,'
      '    :ID_RNS,'
      '    :imenitel,'
      '    :id_sos_del'
      ')')
    RefreshSQL.Strings = (
      'select'
      '  --  me.id ,'
      '    mes.id,'
      '    mes.id_etiketa,'
      '    mes.vid_artikal,'
      '    mes.artikal,'
      '    ma.naziv naziv_artikal,'
      '    mes.gtin,'
      '    mes.sscc,'
      '    mes.seriski_broj,'
      '    mes.barkod,'
      '    mes.lot,'
      '    rns.kolicina,'
      '    rns.seriski_broj,'
      '    rns.zabeleska,'
      '    rns.kolicina_realna,'
      '    rns.vid_ambalaza,'
      '    rns.ambalaza,'
      '    amb.naziv naziv_ambalaza,'
      '    ma.barkod,'
      '    rns.kolicina_pakuvanje kolicina_vo_pakuvanje,'
      '    rns.id_pakuvanje,'
      '    rn.datum_planiran_pocetok datum_pak,'
      '    rns.rok_upotreba,'
      '    cast(rn.datum_planiran_pocetok as date)+ 365 datum_do,'
      '    rns.lot,'
      '    mes.kolicina_broj,'
      '    rns.id id_rns,'
      '    mes.br_kutija,'
      '    mes.imenitel,'
      '    mes.id_sos_del'
      'from man_etiketa me'
      'inner join man_etiketa_stavka mes on mes.id_etiketa = me.id'
      'inner join man_raboten_nalog rn on me.id_raboten_nalog = rn.id'
      
        'inner join man_raboten_nalog_stavka rns on rns.id_raboten_nalog ' +
        '= rn.id and mes.vid_artikal = rns.vid_artikal and mes.artikal = ' +
        'rns.artikal'
      
        'inner join mtr_artikal ma on ma.artvid = rns.vid_artikal and ma.' +
        'id = rns.artikal'
      'left outer join mtr_izvedena_em im on im.id = rns.id_pakuvanje'
      
        'left join mtr_artikal amb on amb.artvid = ma.artvid and amb.id =' +
        ' ma.id'
      'where mes.id_etiketa = :MAS_ID')
    SelectSQL.Strings = (
      'select'
      '  --  me.id ,'
      '    mes.id,'
      '    mes.id_etiketa,'
      '    mes.vid_artikal,'
      '    mes.artikal,'
      '    ma.naziv naziv_artikal,'
      '    mes.gtin,'
      '    mes.sscc,'
      '    mes.seriski_broj,'
      '    mes.barkod,'
      '    mes.lot,'
      '    rns.kolicina,'
      '    rns.seriski_broj,'
      '    rns.zabeleska,'
      '    rns.kolicina_realna,'
      '    rns.vid_ambalaza,'
      '    rns.ambalaza,'
      '    amb.naziv naziv_ambalaza,'
      '    ma.barkod,'
      '    rns.kolicina_pakuvanje kolicina_vo_pakuvanje,'
      '    rns.id_pakuvanje,'
      '    rn.datum_planiran_pocetok datum_pak,'
      '    rns.rok_upotreba,'
      '    cast(rn.datum_planiran_pocetok as date)+ 365 datum_do,'
      '    rns.lot,'
      '    mes.kolicina_broj,'
      '    rns.id id_rns,'
      '    mes.br_kutija,'
      '    mes.imenitel,'
      '    mes.id_sos_del'
      'from man_etiketa me'
      'inner join man_etiketa_stavka mes on mes.id_etiketa = me.id'
      'inner join man_raboten_nalog rn on me.id_raboten_nalog = rn.id'
      
        'inner join man_raboten_nalog_stavka rns on rns.id_raboten_nalog ' +
        '= rn.id and mes.vid_artikal = rns.vid_artikal and mes.artikal = ' +
        'rns.artikal'
      
        'inner join mtr_artikal ma on ma.artvid = rns.vid_artikal and ma.' +
        'id = rns.artikal'
      'left outer join mtr_izvedena_em im on im.id = rns.id_pakuvanje'
      
        'left join mtr_artikal amb on amb.artvid = ma.artvid and amb.id =' +
        ' ma.id'
      'where mes.id_etiketa = :MAS_ID')
    AutoUpdateOptions.UpdateTableName = 'MAN_ETIKETA_STAVKA'
    AutoUpdateOptions.KeyFields = 'ID'
    AutoUpdateOptions.GeneratorName = 'GEN_MAN_ETIKETA_STAVKA_ID'
    AutoUpdateOptions.WhenGetGenID = wgBeforePost
    AutoUpdateOptions.AutoParamsToFields = True
    BeforeDelete = AllTableBeforeDelete
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    DataSource = dsEtiketa
    Left = 120
    Top = 96
    object tblEtiketaStavkaID: TFIBIntegerField
      FieldName = 'ID'
    end
    object tblEtiketaStavkaID_ETIKETA: TFIBIntegerField
      FieldName = 'ID_ETIKETA'
    end
    object tblEtiketaStavkaVID_ARTIKAL: TFIBIntegerField
      FieldName = 'VID_ARTIKAL'
    end
    object tblEtiketaStavkaARTIKAL: TFIBIntegerField
      FieldName = 'ARTIKAL'
    end
    object tblEtiketaStavkaGTIN: TFIBStringField
      FieldName = 'GTIN'
      Size = 30
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblEtiketaStavkaSSCC: TFIBStringField
      FieldName = 'SSCC'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblEtiketaStavkaSERISKI_BROJ: TFIBIntegerField
      FieldName = 'SERISKI_BROJ'
    end
    object tblEtiketaStavkaBARKOD: TFIBStringField
      FieldName = 'BARKOD'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblEtiketaStavkaLOT: TFIBStringField
      FieldName = 'LOT'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblEtiketaStavkaKOLICINA: TFIBFloatField
      FieldName = 'KOLICINA'
    end
    object tblEtiketaStavkaSERISKI_BROJ1: TFIBStringField
      FieldName = 'SERISKI_BROJ1'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblEtiketaStavkaZABELESKA: TFIBStringField
      FieldName = 'ZABELESKA'
      Size = 500
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblEtiketaStavkaKOLICINA_REALNA: TFIBFloatField
      FieldName = 'KOLICINA_REALNA'
    end
    object tblEtiketaStavkaVID_AMBALAZA: TFIBIntegerField
      FieldName = 'VID_AMBALAZA'
    end
    object tblEtiketaStavkaAMBALAZA: TFIBIntegerField
      FieldName = 'AMBALAZA'
    end
    object tblEtiketaStavkaNAZIV_AMBALAZA: TFIBStringField
      FieldName = 'NAZIV_AMBALAZA'
      Size = 500
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblEtiketaStavkaBARKOD1: TFIBStringField
      FieldName = 'BARKOD1'
      Size = 30
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblEtiketaStavkaKOLICINA_VO_PAKUVANJE: TFIBFloatField
      FieldName = 'KOLICINA_VO_PAKUVANJE'
    end
    object tblEtiketaStavkaID_PAKUVANJE: TFIBIntegerField
      FieldName = 'ID_PAKUVANJE'
    end
    object tblEtiketaStavkaDATUM_PAK: TFIBDateTimeField
      FieldName = 'DATUM_PAK'
    end
    object tblEtiketaStavkaROK_UPOTREBA: TFIBIntegerField
      FieldName = 'ROK_UPOTREBA'
    end
    object tblEtiketaStavkaDATUM_DO: TFIBDateField
      FieldName = 'DATUM_DO'
    end
    object tblEtiketaStavkaLOT1: TFIBStringField
      FieldName = 'LOT1'
      Size = 30
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblEtiketaStavkaKOLICINA_BROJ: TFIBIntegerField
      FieldName = 'KOLICINA_BROJ'
    end
    object tblEtiketaStavkaID_RNS: TFIBBCDField
      FieldName = 'ID_RNS'
      Size = 0
    end
    object tblEtiketaStavkaNAZIV_ARTIKAL: TFIBStringField
      FieldName = 'NAZIV_ARTIKAL'
      Size = 500
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblEtiketaStavkaBR_KUTIJA: TFIBIntegerField
      FieldName = 'BR_KUTIJA'
    end
    object tblEtiketaStavkaIMENITEL: TFIBIntegerField
      FieldName = 'IMENITEL'
    end
    object tblEtiketaStavkaID_SOS_DEL: TFIBIntegerField
      FieldName = 'ID_SOS_DEL'
    end
  end
  object tblIzberiArtikal: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE MTR_ARTIKAL'
      'SET '
      '    ARTVID = :ARTVID,'
      '    ID = :ID,'
      '    NAZIV = :NAZIV_ARTIKAL,'
      '    BARKOD = :BARKOD'
      'WHERE'
      '    ARTVID = :OLD_ARTVID'
      '    and ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    MTR_ARTIKAL'
      'WHERE'
      '        ARTVID = :OLD_ARTVID'
      '    and ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO MTR_ARTIKAL('
      '    ARTVID,'
      '    ID,'
      '    NAZIV,'
      '    BARKOD'
      ')'
      'VALUES('
      '    :ARTVID,'
      '    :ID,'
      '    :NAZIV_ARTIKAL,'
      '    :BARKOD'
      ')')
    RefreshSQL.Strings = (
      'select'
      '    rns.id_raboten_nalog,'
      '    ma.artvid,'
      '    ma.id,'
      '    ma.naziv,'
      '    ma.merka,'
      '    rns.kolicina,'
      '    rns.seriski_broj,'
      '    rns.zabeleska,'
      '    rns.kolicina_realna,'
      '    rns.kolicina_broj,'
      '    rns.vid_ambalaza,'
      '    rns.ambalaza,'
      '    amb.naziv naziv_ambalaza,'
      '    ma.barkod,'
      '    rns.kolicina_pakuvanje kolicina_vo_pakuvanje,'
      '    rns.id_pakuvanje,'
      '    rn.datum_planiran_pocetok datum_pak,'
      '    rns.rok_upotreba,'
      '    cast(rn.datum_planiran_pocetok as date)+ 365 datum_do,'
      '    rns.lot,'
      '    rns.id id_rns,'
      '    ma.custom1 sis_paleta,'
      '    ma.custom2 pak_paleta,'
      '    ma.custom3 tez_paleta'
      'from man_raboten_nalog rn'
      
        'inner join man_raboten_nalog_stavka rns on rns.id_raboten_nalog ' +
        '= rn.id'
      
        'inner join mtr_artikal ma on ma.artvid = rns.vid_artikal and ma.' +
        'id = rns.artikal'
      'left outer join mtr_izvedena_em im on im.id = rns.id_pakuvanje'
      
        'left join mtr_artikal amb on amb.artvid = ma.artvid and amb.id =' +
        ' ma.id'
      'where --rns.status = 1 --and rn.source_dokument is not null'
      '--and '
      'rn.id = :MAS_ID and rns.id = :id_rns '
      '--rns.linija = :linija and rns.status_linija = 1')
    SelectSQL.Strings = (
      'select'
      '    rns.id_raboten_nalog,'
      '    ma.artvid,'
      '    ma.id,'
      '    ma.naziv,'
      '    ma.merka,'
      '    rns.kolicina,'
      '    rns.seriski_broj,'
      '    rns.zabeleska,'
      '    rns.kolicina_realna,'
      '    rns.kolicina_broj,'
      '    rns.vid_ambalaza,'
      '    rns.ambalaza,'
      '    amb.naziv naziv_ambalaza,'
      '    ma.barkod,'
      '    rns.kolicina_pakuvanje kolicina_vo_pakuvanje,'
      '    rns.id_pakuvanje,'
      '    rn.datum_planiran_pocetok datum_pak,'
      '    rns.rok_upotreba,'
      '    cast(rn.datum_planiran_pocetok as date)+ 365 datum_do,'
      '    rns.lot,'
      '    rns.id id_rns,'
      '    ma.custom1 sis_paleta,'
      '    ma.custom2 pak_paleta,'
      '    ma.custom3 tez_paleta'
      'from man_raboten_nalog rn'
      
        'inner join man_raboten_nalog_stavka rns on rns.id_raboten_nalog ' +
        '= rn.id'
      
        'inner join mtr_artikal ma on ma.artvid = rns.vid_artikal and ma.' +
        'id = rns.artikal'
      'left outer join mtr_izvedena_em im on im.id = rns.id_pakuvanje'
      
        'left join mtr_artikal amb on amb.artvid = ma.artvid and amb.id =' +
        ' ma.id'
      'where --rns.status = 1 --and rn.source_dokument is not null'
      '--and '
      'rn.id = :MAS_ID and rns.id = :id_rns '
      '--rns.linija = :linija and rns.status_linija = 1')
    AutoUpdateOptions.GeneratorName = 'GEN__ID'
    AutoUpdateOptions.WhenGetGenID = wgBeforePost
    AutoUpdateOptions.AutoParamsToFields = True
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    DataSource = dsIzberiRN
    Left = 282
    Top = 32
    poAskRecordCount = True
    object tblIzberiArtikalID_RABOTEN_NALOG: TFIBBCDField
      FieldName = 'ID_RABOTEN_NALOG'
      Size = 0
    end
    object tblIzberiArtikalSERISKI_BROJ: TFIBStringField
      FieldName = 'SERISKI_BROJ'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblIzberiArtikalZABELESKA: TFIBStringField
      FieldName = 'ZABELESKA'
      Size = 500
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblIzberiArtikalVID_AMBALAZA: TFIBIntegerField
      FieldName = 'VID_AMBALAZA'
    end
    object tblIzberiArtikalAMBALAZA: TFIBIntegerField
      FieldName = 'AMBALAZA'
    end
    object tblIzberiArtikalNAZIV_AMBALAZA: TFIBStringField
      FieldName = 'NAZIV_AMBALAZA'
      Size = 500
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblIzberiArtikalBARKOD: TFIBStringField
      FieldName = 'BARKOD'
      Size = 30
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblIzberiArtikalID_PAKUVANJE: TFIBIntegerField
      FieldName = 'ID_PAKUVANJE'
    end
    object tblIzberiArtikalARTVID: TFIBIntegerField
      FieldName = 'ARTVID'
    end
    object tblIzberiArtikalID: TFIBIntegerField
      FieldName = 'ID'
    end
    object tblIzberiArtikalDATUM_PAK: TFIBDateTimeField
      FieldName = 'DATUM_PAK'
    end
    object tblIzberiArtikalROK_UPOTREBA: TFIBIntegerField
      FieldName = 'ROK_UPOTREBA'
    end
    object tblIzberiArtikalDATUM_DO: TFIBDateField
      FieldName = 'DATUM_DO'
    end
    object tblIzberiArtikalNAZIV: TFIBStringField
      FieldName = 'NAZIV'
      Size = 500
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblIzberiArtikalLOT: TFIBStringField
      FieldName = 'LOT'
      Size = 30
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblIzberiArtikalKOLICINA_VO_PAKUVANJE: TFIBFloatField
      FieldName = 'KOLICINA_VO_PAKUVANJE'
      DisplayFormat = '#,##0.00'
    end
    object tblIzberiArtikalKOLICINA_BROJ: TFIBIntegerField
      FieldName = 'KOLICINA_BROJ'
    end
    object tblIzberiArtikalKOLICINA: TFIBFloatField
      FieldName = 'KOLICINA'
    end
    object tblIzberiArtikalKOLICINA_REALNA: TFIBFloatField
      FieldName = 'KOLICINA_REALNA'
    end
    object tblIzberiArtikalMERKA: TFIBStringField
      FieldName = 'MERKA'
      Size = 5
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblIzberiArtikalID_RNS: TFIBBCDField
      FieldName = 'ID_RNS'
      Size = 0
    end
    object tblIzberiArtikalSIS_PALETA: TFIBStringField
      FieldName = 'SIS_PALETA'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblIzberiArtikalPAK_PALETA: TFIBStringField
      FieldName = 'PAK_PALETA'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblIzberiArtikalTEZ_PALETA: TFIBStringField
      FieldName = 'TEZ_PALETA'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object dsIzberiArtikal: TDataSource
    DataSet = tblIzberiArtikal
    Left = 202
    Top = 32
  end
  object qSetupK: TpFIBQuery
    Transaction = dmKon.qTransaction
    Database = dmKon.fibBaza
    SQL.Strings = (
      'select  s.v1'
      'from sys_setup s'
      'where s.p1 = '#39'man'#39' and s.p2 = '#39'kompaniski_broj'#39)
    Left = 520
    Top = 64
    qoStartTransaction = True
  end
  object MaxEtiketaStavka: TpFIBStoredProc
    Transaction = dmKon.tProc
    Database = dmKon.fibBaza
    SQL.Strings = (
      'EXECUTE PROCEDURE MAX_MAN_ETIKETA_STAVKA ')
    StoredProcName = 'MAX_MAN_ETIKETA_STAVKA'
    Left = 520
    Top = 8
    qoAutoCommit = True
    qoStartTransaction = True
  end
  object dsAmbalaza: TDataSource
    AutoEdit = False
    DataSet = tblAmbalaza
    Left = 201
    Top = 92
  end
  object tblAmbalaza: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE MTR_ARTIKAL'
      'SET '
      '    NAZIV = :NAZIV,'
      '    MERKA = :MERKA,'
      '    CENA = :CENA,'
      '    BRPAK = :BRPAK,'
      '    KATALOG = :KATALOG,'
      '    BARKOD = :BARKOD,'
      '    PROZVODITEL = :PROZVODITEL,'
      '    GRUPA = :GRUPA,'
      '    KLASIFIKACIJA = :KLASIFIKACIJA,'
      '    OBLIK = :OBLIK,'
      '    JACINA = :JACINA,'
      '    KOLICINA = :KOLICINA,'
      '    UNINAZIV = :UNINAZIV,'
      '    UNIFONT = :UNIFONT,'
      '    VOLUMEN = :VOLUMEN,'
      '    TEZINA = :TEZINA,'
      '    MKDART = :MKDART,'
      '    TARIFA = :TARIFA,'
      '    AKTIVEN = :AKTIVEN,'
      '    TIP_ARTIKAL = :TIP_ARTIKAL,'
      '    OPIS = :OPIS,'
      '    NAZIV2 = :NAZIV2,'
      '    SKU = :SKU,'
      '    VISINA = :VISINA,'
      '    SIRINA = :SIRINA,'
      '    DOLZINA = :DOLZINA,'
      '    NABAVEN = :NABAVEN,'
      '    PRODAZBA = :PRODAZBA,'
      '    ZALIHA = :ZALIHA,'
      '    SERISKI_BROJ = :SERISKI_BROJ,'
      '    GENERIKA = :GENERIKA,'
      '    JN_CPV = :JN_CPV,'
      '    JN_TIP_ARTIKAL = :JN_TIP_ARTIKAL,'
      '    VID_ARTIKAL_ZAMENA = :VID_ARTIKAL_ZAMENA,'
      '    ARTIKAL_ZAMENA = :ARTIKAL_ZAMENA,'
      '    DEKLARACIJA = :DEKLARACIJA,'
      '    URL = :URL,'
      '    CUSTOM1 = :CUSTOM1,'
      '    CUSTOM2 = :CUSTOM2,'
      '    CUSTOM3 = :CUSTOM3'
      'WHERE'
      '    ARTVID = :OLD_ARTVID'
      '    and ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    MTR_ARTIKAL'
      'WHERE'
      '        ARTVID = :OLD_ARTVID'
      '    and ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO MTR_ARTIKAL('
      '    ARTVID,'
      '    ID,'
      '    NAZIV,'
      '    MERKA,'
      '    CENA,'
      '    BRPAK,'
      '    KATALOG,'
      '    BARKOD,'
      '    PROZVODITEL,'
      '    GRUPA,'
      '    KLASIFIKACIJA,'
      '    OBLIK,'
      '    JACINA,'
      '    KOLICINA,'
      '    UNINAZIV,'
      '    UNIFONT,'
      '    VOLUMEN,'
      '    TEZINA,'
      '    MKDART,'
      '    TARIFA,'
      '    AKTIVEN,'
      '    TIP_ARTIKAL,'
      '    OPIS,'
      '    NAZIV2,'
      '    SKU,'
      '    VISINA,'
      '    SIRINA,'
      '    DOLZINA,'
      '    NABAVEN,'
      '    PRODAZBA,'
      '    ZALIHA,'
      '    SERISKI_BROJ,'
      '    GENERIKA,'
      '    JN_CPV,'
      '    JN_TIP_ARTIKAL,'
      '    VID_ARTIKAL_ZAMENA,'
      '    ARTIKAL_ZAMENA,'
      '    DEKLARACIJA,'
      '    URL,'
      '    CUSTOM1,'
      '    CUSTOM2,'
      '    CUSTOM3'
      ')'
      'VALUES('
      '    :ARTVID,'
      '    :ID,'
      '    :NAZIV,'
      '    :MERKA,'
      '    :CENA,'
      '    :BRPAK,'
      '    :KATALOG,'
      '    :BARKOD,'
      '    :PROZVODITEL,'
      '    :GRUPA,'
      '    :KLASIFIKACIJA,'
      '    :OBLIK,'
      '    :JACINA,'
      '    :KOLICINA,'
      '    :UNINAZIV,'
      '    :UNIFONT,'
      '    :VOLUMEN,'
      '    :TEZINA,'
      '    :MKDART,'
      '    :TARIFA,'
      '    :AKTIVEN,'
      '    :TIP_ARTIKAL,'
      '    :OPIS,'
      '    :NAZIV2,'
      '    :SKU,'
      '    :VISINA,'
      '    :SIRINA,'
      '    :DOLZINA,'
      '    :NABAVEN,'
      '    :PRODAZBA,'
      '    :ZALIHA,'
      '    :SERISKI_BROJ,'
      '    :GENERIKA,'
      '    :JN_CPV,'
      '    :JN_TIP_ARTIKAL,'
      '    :VID_ARTIKAL_ZAMENA,'
      '    :ARTIKAL_ZAMENA,'
      '    :DEKLARACIJA,'
      '    :URL,'
      '    :CUSTOM1,'
      '    :CUSTOM2,'
      '    :CUSTOM3'
      ')')
    RefreshSQL.Strings = (
      'select A.ARTVID,'
      '       A.ID,'
      '       A.NAZIV,'
      '       A.MERKA'
      'from MTR_ARTIKAL A'
      'inner join mtr_artvid v on v.id = a.artvid'
      'where(  a.aktiven = 1 and upper(v.opis) like '#39#1040#1052#1041#1040#1051#1040#1046#1040#39
      '     ) and (     A.ARTVID = :OLD_ARTVID'
      '    and A.ID = :OLD_ID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      'select A.ARTVID,'
      '       A.ID,'
      '       A.NAZIV,'
      '       A.MERKA'
      'from MTR_ARTIKAL A'
      'inner join mtr_artvid v on v.id = a.artvid'
      'where a.aktiven = 1 and upper(v.opis) like '#39#1040#1052#1041#1040#1051#1040#1046#1040#39)
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 281
    Top = 92
    poSQLINT64ToBCD = True
    object tblAmbalazaARTVID: TFIBIntegerField
      DisplayLabel = #1042#1080#1076
      FieldName = 'ARTVID'
    end
    object tblAmbalazaID: TFIBIntegerField
      DisplayLabel = #1064#1080#1092#1088#1072
      FieldName = 'ID'
    end
    object tblAmbalazaNAZIV: TFIBStringField
      DisplayLabel = #1053#1072#1079#1080#1074
      FieldName = 'NAZIV'
      Size = 500
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblAmbalazaMERKA: TFIBStringField
      FieldName = 'MERKA'
      Size = 5
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object tblIzvedenaEM: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE MAN_KT_VLEZ'
      'SET '
      '    ID = :ID,'
      '    ID_KT = :ID_KT,'
      '    ID_EKT = :ID_EKT,'
      '    DATUM = :DATUM,'
      '    VID_ARTIKAL = :VID_ARTIKAL,'
      '    ARTIKAL = :ARTIKAL,'
      '    ID_RABOTEN_NALOG = :ID_RABOTEN_NALOG,'
      '    PRIMENI = :PRIMENI,'
      '    GODINA = :GODINA'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    MAN_KT_VLEZ'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO MAN_KT_VLEZ('
      '    ID,'
      '    ID_KT,'
      '    ID_EKT,'
      '    DATUM,'
      '    VID_ARTIKAL,'
      '    ARTIKAL,'
      '    ID_RABOTEN_NALOG,'
      '    PRIMENI,'
      '    GODINA'
      ')'
      'VALUES('
      '    :ID,'
      '    :ID_KT,'
      '    :ID_EKT,'
      '    :DATUM,'
      '    :VID_ARTIKAL,'
      '    :ARTIKAL,'
      '    :ID_RABOTEN_NALOG,'
      '    :PRIMENI,'
      '    :GODINA'
      ')')
    RefreshSQL.Strings = (
      'select'
      '    iem.id,'
      '    iem.izvedena_em,'
      '    m.naziv ||'#39'/'#39'|| cast(iem.koeficient as integer) naziv,'
      '    iem.koeficient'
      'from mtr_izvedena_em iem'
      'inner join mat_merka m on m.id = iem.izvedena_em'
      
        'where(  iem.artvid like :vid_artikal and iem.artsif like :artika' +
        'l'
      '     ) and (     IEM.ID = :OLD_ID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      'select'
      '    iem.id,'
      '    iem.izvedena_em,'
      '    m.naziv ||'#39'/'#39'|| cast(iem.koeficient as integer) naziv,'
      '    iem.koeficient'
      'from mtr_izvedena_em iem'
      'inner join mat_merka m on m.id = iem.izvedena_em'
      'where iem.artvid like :vid_artikal and iem.artsif like :artikal')
    AutoUpdateOptions.GeneratorName = 'GEN__ID'
    AutoUpdateOptions.WhenGetGenID = wgBeforePost
    AutoUpdateOptions.AutoParamsToFields = True
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 440
    Top = 92
    object tblIzvedenaEMIZVEDENA_EM: TFIBStringField
      FieldName = 'IZVEDENA_EM'
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblIzvedenaEMNAZIV: TFIBStringField
      FieldName = 'NAZIV'
      Size = 62
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblIzvedenaEMKOEFICIENT: TFIBBCDField
      FieldName = 'KOEFICIENT'
      Size = 8
    end
    object tblIzvedenaEMID: TFIBIntegerField
      FieldName = 'ID'
    end
  end
  object dsIzvedenaEM: TDataSource
    DataSet = tblIzvedenaEM
    Left = 362
    Top = 92
  end
  object tblIzberiRN: TpFIBDataSet
    RefreshSQL.Strings = (
      'select'
      '   rn.id,'
      '   rn.broj||'#39'/'#39'||rn.godina broj,'
      '   rn.datum_planiran_pocetok datum, '
      '   rn.status,'
      '   rn.zabeleska,'
      '   rn.tip_partner tp_poracka,'
      '   rn.partner p_poracka,'
      '   mp.naziv naziv_partner_poracka,'
      '   rn.source_dokument'
      'from man_raboten_nalog rn'
      
        'left outer join mat_partner mp on mp.id = rn.partner and mp.tip_' +
        'partner = rn.tip_partner'
      'where(  rn.status like :status --and exists'
      '   -- (select rns.linija'
      '   -- from man_raboten_nalog_stavka rns'
      
        '    -- where (rns.id_raboten_nalog = rn.id) and rns.linija <> 0 ' +
        'and rns.status_linija = 1 and rns.linija = :linija)'
      '   and rn.id = :rn'
      '     ) and (     RN.ID = :OLD_ID'
      '     )'
      '    '
      '')
    SelectSQL.Strings = (
      'select'
      '   rn.id,'
      '   rn.broj||'#39'/'#39'||rn.godina broj,'
      '   rn.datum_planiran_pocetok datum, '
      '   rn.status,'
      '   rn.zabeleska,'
      '   rn.tip_partner tp_poracka,'
      '   rn.partner p_poracka,'
      '   mp.naziv naziv_partner_poracka,'
      '   rn.source_dokument'
      'from man_raboten_nalog rn'
      
        'left outer join mat_partner mp on mp.id = rn.partner and mp.tip_' +
        'partner = rn.tip_partner'
      'where rn.status like :status --and exists'
      '   -- (select rns.linija'
      '   -- from man_raboten_nalog_stavka rns'
      
        '    -- where (rns.id_raboten_nalog = rn.id) and rns.linija <> 0 ' +
        'and rns.status_linija = 1 and rns.linija = :linija)'
      '   and rn.id = :rn'
      ''
      'order by rn.datum_planiran_pocetok desc')
    AutoUpdateOptions.GeneratorName = 'GEN__ID'
    AutoUpdateOptions.WhenGetGenID = wgBeforePost
    AutoUpdateOptions.AutoParamsToFields = True
    BeforeOpen = tblIzberiRNBeforeOpen
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 48
    Top = 152
    poAskRecordCount = True
    object tblIzberiRNSTATUS: TFIBIntegerField
      FieldName = 'STATUS'
    end
    object tblIzberiRNNAZIV_PARTNER_PORACKA: TFIBStringField
      DisplayLabel = #1055#1086#1088#1072#1095#1082#1072' - '#1055#1072#1088#1090#1085#1077#1088
      FieldName = 'NAZIV_PARTNER_PORACKA'
      Size = 255
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblIzberiRNZABELESKA: TFIBStringField
      FieldName = 'ZABELESKA'
      Size = 200
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblIzberiRNTP_PORACKA: TFIBIntegerField
      FieldName = 'TP_PORACKA'
    end
    object tblIzberiRNP_PORACKA: TFIBIntegerField
      FieldName = 'P_PORACKA'
    end
    object tblIzberiRNID: TFIBBCDField
      FieldName = 'ID'
      Size = 0
    end
    object tblIzberiRNBROJ: TFIBStringField
      DisplayLabel = #1041#1088#1086#1112' '#1085#1072' '#1056#1053
      FieldName = 'BROJ'
      Size = 112
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblIzberiRNDATUM: TFIBDateTimeField
      FieldName = 'DATUM'
    end
    object tblIzberiRNSOURCE_DOKUMENT: TFIBBCDField
      FieldName = 'SOURCE_DOKUMENT'
      Size = 0
    end
  end
  object dsIzberiRN: TDataSource
    DataSet = tblIzberiRN
    Left = 120
    Top = 152
  end
  object MaxEtiketa: TpFIBStoredProc
    Transaction = dmKon.tProc
    Database = dmKon.fibBaza
    SQL.Strings = (
      'EXECUTE PROCEDURE MAX_MAN_ETIKETA ')
    StoredProcName = 'MAX_MAN_ETIKETA'
    Left = 520
    Top = 120
    qoAutoCommit = True
    qoStartTransaction = True
  end
  object qMax: TpFIBQuery
    Transaction = dmKon.qTransaction
    Database = dmKon.fibBaza
    SQL.Strings = (
      'select coalesce(max(es.br_kutija),0)+1 maks'
      'from man_etiketa_stavka es')
    Left = 560
    Top = 72
    qoAutoCommit = True
    qoStartTransaction = True
  end
  object tblIzberiKamion: TpFIBDataSet
    RefreshSQL.Strings = (
      'select distinct T.ID,'
      '                T.NAZIV,'
      '                T.DATUM,'
      '                T.STATUS,'
      '                P.TIP_PARTNER TP_PORACKA,'
      '                P.PARTNER P_PORACKA,'
      '                MP.NAZIV NAZIV_PARTNER_PORACKA'
      'from MAN_TRAILER T'
      
        'inner join MAN_POR_S_TRAILER PST on PST.ID_TRAILER = T.ID and T.' +
        'STATUS = 0'
      
        'inner join MAN_PORACKA_STAVKA PS on PS.ID = PST.ID_PORACKA_STAVK' +
        'A and PS.STATUS <> 3'
      
        'inner join MAN_PORACKA P on P.ID = PS.ID_PORACKA and PS.STATUS <' +
        '> 3'
      
        'inner join MAT_PARTNER MP on MP.ID = P.PARTNER and MP.TIP_PARTNE' +
        'R = P.TIP_PARTNER'
      'where(  T.STATUS like :STATUS and'
      '      T.ID = :T'
      '     ) and (     T.ID = :OLD_ID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      'select distinct T.ID,'
      '                T.NAZIV,'
      '                T.DATUM,'
      '                T.STATUS,'
      '                P.TIP_PARTNER TP_PORACKA,'
      '                P.PARTNER P_PORACKA,'
      '                MP.NAZIV NAZIV_PARTNER_PORACKA'
      'from MAN_TRAILER T'
      
        'inner join MAN_POR_S_TRAILER PST on PST.ID_TRAILER = T.ID and T.' +
        'STATUS = 0'
      
        'inner join MAN_PORACKA_STAVKA PS on PS.ID = PST.ID_PORACKA_STAVK' +
        'A and PS.STATUS <> 3'
      
        'inner join MAN_PORACKA P on P.ID = PS.ID_PORACKA and PS.STATUS <' +
        '> 3'
      
        'inner join MAT_PARTNER MP on MP.ID = P.PARTNER and MP.TIP_PARTNE' +
        'R = P.TIP_PARTNER'
      'where T.STATUS like :STATUS and'
      '      T.ID = :T'
      'order by T.DATUM desc  ')
    AutoUpdateOptions.GeneratorName = 'GEN__ID'
    AutoUpdateOptions.WhenGetGenID = wgBeforePost
    AutoUpdateOptions.AutoParamsToFields = True
    BeforeOpen = tblIzberiRNBeforeOpen
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 200
    Top = 152
    poAskRecordCount = True
    object tblIzberiKamionID: TFIBIntegerField
      FieldName = 'ID'
    end
    object tblIzberiKamionNAZIV: TFIBStringField
      FieldName = 'NAZIV'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object fbdtfldIzberiKamionDATUM: TFIBDateField
      FieldName = 'DATUM'
    end
    object fbsmlntfldIzberiKamionSTATUS: TFIBSmallIntField
      FieldName = 'STATUS'
    end
    object tblIzberiKamionTP_PORACKA: TFIBIntegerField
      FieldName = 'TP_PORACKA'
    end
    object tblIzberiKamionP_PORACKA: TFIBIntegerField
      FieldName = 'P_PORACKA'
    end
    object tblIzberiKamionNAZIV_PARTNER_PORACKA: TFIBStringField
      FieldName = 'NAZIV_PARTNER_PORACKA'
      Size = 1024
      Transliterate = False
      EmptyStrToNull = True
    end
  end
  object dsIzberiKamion: TDataSource
    DataSet = tblIzberiKamion
    Left = 272
    Top = 152
  end
  object dsIzberiA: TDataSource
    DataSet = tblIzberiA
    Left = 362
    Top = 32
  end
  object tblIzberiA: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE MTR_ARTIKAL'
      'SET '
      '    ARTVID = :ARTVID,'
      '    ID = :ID,'
      '    NAZIV = :NAZIV_ARTIKAL,'
      '    BARKOD = :BARKOD'
      'WHERE'
      '    ARTVID = :OLD_ARTVID'
      '    and ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    MTR_ARTIKAL'
      'WHERE'
      '        ARTVID = :OLD_ARTVID'
      '    and ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO MTR_ARTIKAL('
      '    ARTVID,'
      '    ID,'
      '    NAZIV,'
      '    BARKOD'
      ')'
      'VALUES('
      '    :ARTVID,'
      '    :ID,'
      '    :NAZIV_ARTIKAL,'
      '    :BARKOD'
      ')')
    RefreshSQL.Strings = (
      'select'
      '    pst.id_trailer,'
      '    ma.artvid,'
      '    ma.id,'
      '    ma.naziv,'
      '    ma.merka,'
      '    pst.kolicina,'
      '    rns.seriski_broj,'
      '    rns.zabeleska,'
      '    --rns.kolicina_realna,'
      '    --rns.kolicina_broj,'
      '    rns.vid_ambalaza,'
      '    rns.ambalaza,'
      '    amb.naziv naziv_ambalaza,'
      '    ma.barkod,'
      '  --  rns.kolicina_pakuvanje kolicina_vo_pakuvanje,'
      '  --  rns.id_pakuvanje,'
      '    t.datum datum_pak,'
      '    rns.rok_upotreba,'
      '    cast(t.datum as date)+ 365 datum_do,'
      '    rns.lot,'
      '    rns.id id_rns,'
      '    ma.custom1 sis_paleta,'
      '    ma.custom2 pak_paleta,'
      '    ma.custom3 tez_paleta,'
      '    rn.id id_rn'
      'from man_trailer t'
      
        'inner join man_por_s_trailer pst on pst.id_trailer = t.id and t.' +
        'status = 0'
      
        'inner join man_raboten_nalog_stavka rns on rns.id = pst.id_rn_st' +
        'avka  and rns.status <> 3'
      
        'inner join man_raboten_nalog rn on rn.id = rns.id_raboten_nalog ' +
        'and rn.status <> 3'
      
        'inner join mtr_artikal ma on ma.artvid = rns.vid_artikal and ma.' +
        'id = rns.artikal'
      'left outer join mtr_izvedena_em im on im.id = rns.id_pakuvanje'
      
        'left join mtr_artikal amb on amb.artvid = ma.artvid and amb.id =' +
        ' ma.id'
      'where --rns.status = 1 --and rn.source_dokument is not null'
      '--and '
      't.id = :MAS_ID --and rns.id = :id_rns'
      '--rns.linija = :linija and rns.status_linija = 1')
    SelectSQL.Strings = (
      'select'
      '    pst.id_trailer,'
      '    ma.artvid,'
      '    ma.id,'
      '    ma.naziv,'
      '    ma.merka,'
      '    pst.kolicina,'
      '    rns.seriski_broj,'
      '    rns.zabeleska,'
      '    --rns.kolicina_realna,'
      '    --rns.kolicina_broj,'
      '    rns.vid_ambalaza,'
      '    rns.ambalaza,'
      '    amb.naziv naziv_ambalaza,'
      '    ma.barkod,'
      '  --  rns.kolicina_pakuvanje kolicina_vo_pakuvanje,'
      '  --  rns.id_pakuvanje,'
      '    t.datum datum_pak,'
      '    rns.rok_upotreba,'
      '    cast(t.datum as date)+ 365 datum_do,'
      '    rns.lot,'
      '    rns.id id_rns,'
      '    ma.custom1 sis_paleta,'
      '    ma.custom2 pak_paleta,'
      '    ma.custom3 tez_paleta,'
      '    rn.id id_rn'
      'from man_trailer t'
      
        'inner join man_por_s_trailer pst on pst.id_trailer = t.id and t.' +
        'status = 0'
      
        'inner join man_raboten_nalog_stavka rns on rns.id = pst.id_rn_st' +
        'avka  and rns.status <> 3'
      
        'inner join man_raboten_nalog rn on rn.id = rns.id_raboten_nalog ' +
        'and rn.status <> 3'
      
        'inner join mtr_artikal ma on ma.artvid = rns.vid_artikal and ma.' +
        'id = rns.artikal'
      'left outer join mtr_izvedena_em im on im.id = rns.id_pakuvanje'
      
        'left join mtr_artikal amb on amb.artvid = ma.artvid and amb.id =' +
        ' ma.id'
      'where --rns.status = 1 --and rn.source_dokument is not null'
      '--and '
      't.id = :MAS_ID --and rns.id = :id_rns'
      '--rns.linija = :linija and rns.status_linija = 1')
    AutoUpdateOptions.GeneratorName = 'GEN__ID'
    AutoUpdateOptions.WhenGetGenID = wgBeforePost
    AutoUpdateOptions.AutoParamsToFields = True
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    DataSource = dsIzberiKamion
    Left = 442
    Top = 32
    poAskRecordCount = True
    object tblIzberiAID_TRAILER: TFIBIntegerField
      FieldName = 'ID_TRAILER'
    end
    object tblIzberiAARTVID: TFIBIntegerField
      FieldName = 'ARTVID'
    end
    object tblIzberiAID: TFIBIntegerField
      FieldName = 'ID'
    end
    object tblIzberiANAZIV: TFIBStringField
      FieldName = 'NAZIV'
      Size = 500
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblIzberiAMERKA: TFIBStringField
      FieldName = 'MERKA'
      Size = 5
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblIzberiAKOLICINA: TFIBIntegerField
      FieldName = 'KOLICINA'
    end
    object tblIzberiASERISKI_BROJ: TFIBStringField
      FieldName = 'SERISKI_BROJ'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblIzberiAZABELESKA: TFIBStringField
      FieldName = 'ZABELESKA'
      Size = 500
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblIzberiAVID_AMBALAZA: TFIBIntegerField
      FieldName = 'VID_AMBALAZA'
    end
    object tblIzberiAAMBALAZA: TFIBIntegerField
      FieldName = 'AMBALAZA'
    end
    object tblIzberiANAZIV_AMBALAZA: TFIBStringField
      FieldName = 'NAZIV_AMBALAZA'
      Size = 500
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblIzberiABARKOD: TFIBStringField
      FieldName = 'BARKOD'
      Size = 30
      Transliterate = False
      EmptyStrToNull = True
    end
    object fbdtfldIzberiADATUM_PAK: TFIBDateField
      FieldName = 'DATUM_PAK'
    end
    object tblIzberiAROK_UPOTREBA: TFIBIntegerField
      FieldName = 'ROK_UPOTREBA'
    end
    object fbdtfldIzberiADATUM_DO: TFIBDateField
      FieldName = 'DATUM_DO'
    end
    object tblIzberiALOT: TFIBStringField
      FieldName = 'LOT'
      Size = 30
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblIzberiAID_RNS: TFIBBCDField
      FieldName = 'ID_RNS'
      Size = 0
    end
    object tblIzberiASIS_PALETA: TFIBStringField
      FieldName = 'SIS_PALETA'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblIzberiAPAK_PALETA: TFIBStringField
      FieldName = 'PAK_PALETA'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblIzberiATEZ_PALETA: TFIBStringField
      FieldName = 'TEZ_PALETA'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblIzberiAID_RN: TFIBBCDField
      FieldName = 'ID_RN'
      Size = 0
    end
  end
  object tblEtiketaStavkaK: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE MAN_ETIKETA_STAVKA'
      'SET '
      '    ID_ETIKETA = :ID_ETIKETA,'
      '    VID_ARTIKAL = :VID_ARTIKAL,'
      '    ARTIKAL = :ARTIKAL,'
      '    GTIN = :GTIN,'
      '    SSCC = :SSCC,'
      '    SERISKI_BROJ = :SERISKI_BROJ,'
      '    BARKOD = :BARKOD,'
      '    LOT = :LOT,'
      '    KOLICINA_BROJ = :KOLICINA_BROJ,'
      '    BR_KUTIJA = :BR_KUTIJA,'
      '    ID_POR_S_T = :ID_POR_S_T'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    MAN_ETIKETA_STAVKA'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO MAN_ETIKETA_STAVKA('
      '    ID,'
      '    ID_ETIKETA,'
      '    VID_ARTIKAL,'
      '    ARTIKAL,'
      '    GTIN,'
      '    SSCC,'
      '    SERISKI_BROJ,'
      '    BARKOD,'
      '    LOT,'
      '    KOLICINA_BROJ,'
      '    BR_KUTIJA,'
      '    ID_POR_S_T'
      ')'
      'VALUES('
      '    :ID,'
      '    :ID_ETIKETA,'
      '    :VID_ARTIKAL,'
      '    :ARTIKAL,'
      '    :GTIN,'
      '    :SSCC,'
      '    :SERISKI_BROJ,'
      '    :BARKOD,'
      '    :LOT,'
      '    :KOLICINA_BROJ,'
      '    :BR_KUTIJA,'
      '    :ID_POR_S_T'
      ')')
    RefreshSQL.Strings = (
      'select'
      '  --  me.id ,'
      '    mes.id,'
      '    mes.id_etiketa,'
      '    mes.vid_artikal,'
      '    mes.artikal,'
      '    ma.naziv naziv_artikal,'
      '    mes.gtin,'
      '    mes.sscc,'
      '    mes.seriski_broj,'
      '    mes.barkod,'
      '    mes.lot,'
      '    rns.kolicina,'
      '    rns.seriski_broj,'
      '    rns.zabeleska,'
      '    rns.kolicina_realna,'
      '    rns.vid_ambalaza,'
      '    rns.ambalaza,'
      '    amb.naziv naziv_ambalaza,'
      '    ma.barkod,'
      '    rns.kolicina_pakuvanje kolicina_vo_pakuvanje,'
      '    rns.id_pakuvanje,'
      '    rn.datum_planiran_pocetok datum_pak,'
      '    rns.rok_upotreba,'
      '    cast(rn.datum_planiran_pocetok as date)+ 365 datum_do,'
      '    rns.lot,'
      '    mes.kolicina_broj,'
      '    rns.id id_rns,'
      '    mes.br_kutija ,'
      '    pst.id_trailer,'
      '    pst.kolicina kol_trailer,'
      '    mes.id_por_s_t '
      'from man_etiketa me'
      'inner join man_etiketa_stavka mes on mes.id_etiketa = me.id'
      'inner join man_raboten_nalog rn on me.id_raboten_nalog = rn.id'
      
        'inner join man_raboten_nalog_stavka rns on rns.id_raboten_nalog ' +
        '= rn.id and mes.vid_artikal = rns.vid_artikal and mes.artikal = ' +
        'rns.artikal'
      
        '--inner join MAN_RABOTEN_NALOG_STAVKA RNS on RNS.ID_RABOTEN_NALO' +
        'G = RN.ID and MES.VID_ARTIKAL = RNS.VID_ARTIKAL and MES.ARTIKAL ' +
        '= RNS.ARTIKAL and RNS.ID = MES.ID_RNS'
      
        'inner join MAN_PORACKA_STAVKA PS on PS.ID = RNS.ID_PORACKA_STAVK' +
        'A and ps.status <> 3'
      
        'inner join MAN_PORACKA P on P.ID = PS.ID_PORACKA and p.status <>' +
        ' 3'
      'left join man_por_s_trailer pst on pst.id_poracka_stavka = ps.id'
      ''
      
        'inner join mtr_artikal ma on ma.artvid = rns.vid_artikal and ma.' +
        'id = rns.artikal'
      'left outer join mtr_izvedena_em im on im.id = rns.id_pakuvanje'
      
        'left join mtr_artikal amb on amb.artvid = ma.artvid and amb.id =' +
        ' ma.id'
      'where(  mes.id_etiketa = :MAS_ID'
      '     ) and (     MES.ID = :OLD_ID'
      '     )'
      '    ')
    SelectSQL.Strings = (
      'select'
      '  --  me.id ,'
      '    mes.id,'
      '    mes.id_etiketa,'
      '    mes.vid_artikal,'
      '    mes.artikal,'
      '    ma.naziv naziv_artikal,'
      '    mes.gtin,'
      '    mes.sscc,'
      '    mes.seriski_broj,'
      '    mes.barkod,'
      '    mes.lot,'
      '    rns.kolicina,'
      '    rns.seriski_broj,'
      '    rns.zabeleska,'
      '    rns.kolicina_realna,'
      '    rns.vid_ambalaza,'
      '    rns.ambalaza,'
      '    amb.naziv naziv_ambalaza,'
      '    ma.barkod,'
      '    rns.kolicina_pakuvanje kolicina_vo_pakuvanje,'
      '    rns.id_pakuvanje,'
      '    rn.datum_planiran_pocetok datum_pak,'
      '    rns.rok_upotreba,'
      '    cast(rn.datum_planiran_pocetok as date)+ 365 datum_do,'
      '    rns.lot,'
      '    mes.kolicina_broj,'
      '    rns.id id_rns,'
      '    mes.br_kutija ,'
      '    pst.id_trailer,'
      '    pst.kolicina kol_trailer,'
      '    mes.id_por_s_t '
      'from man_etiketa me'
      'inner join man_etiketa_stavka mes on mes.id_etiketa = me.id'
      'inner join man_raboten_nalog rn on me.id_raboten_nalog = rn.id'
      
        'inner join man_raboten_nalog_stavka rns on rns.id_raboten_nalog ' +
        '= rn.id and mes.vid_artikal = rns.vid_artikal and mes.artikal = ' +
        'rns.artikal'
      
        '--inner join MAN_RABOTEN_NALOG_STAVKA RNS on RNS.ID_RABOTEN_NALO' +
        'G = RN.ID and MES.VID_ARTIKAL = RNS.VID_ARTIKAL and MES.ARTIKAL ' +
        '= RNS.ARTIKAL and RNS.ID = MES.ID_RNS'
      
        'inner join MAN_PORACKA_STAVKA PS on PS.ID = RNS.ID_PORACKA_STAVK' +
        'A and ps.status <> 3'
      
        'inner join MAN_PORACKA P on P.ID = PS.ID_PORACKA and p.status <>' +
        ' 3'
      'left join man_por_s_trailer pst on pst.id_poracka_stavka = ps.id'
      ''
      
        'inner join mtr_artikal ma on ma.artvid = rns.vid_artikal and ma.' +
        'id = rns.artikal'
      'left outer join mtr_izvedena_em im on im.id = rns.id_pakuvanje'
      
        'left join mtr_artikal amb on amb.artvid = ma.artvid and amb.id =' +
        ' ma.id'
      'where mes.id_etiketa = :MAS_ID')
    AutoUpdateOptions.UpdateTableName = 'MAN_ETIKETA_STAVKA'
    AutoUpdateOptions.KeyFields = 'ID'
    AutoUpdateOptions.GeneratorName = 'GEN_MAN_ETIKETA_STAVKA_ID'
    AutoUpdateOptions.WhenGetGenID = wgBeforePost
    AutoUpdateOptions.AutoParamsToFields = True
    BeforeDelete = AllTableBeforeDelete
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    DataSource = dsEtiketaK
    Left = 120
    Top = 272
    object FIBIntegerField1: TFIBIntegerField
      FieldName = 'ID'
    end
    object FIBIntegerField2: TFIBIntegerField
      FieldName = 'ID_ETIKETA'
    end
    object FIBIntegerField3: TFIBIntegerField
      FieldName = 'VID_ARTIKAL'
    end
    object FIBIntegerField4: TFIBIntegerField
      FieldName = 'ARTIKAL'
    end
    object FIBStringField1: TFIBStringField
      FieldName = 'GTIN'
      Size = 30
      Transliterate = False
      EmptyStrToNull = True
    end
    object FIBStringField2: TFIBStringField
      FieldName = 'SSCC'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object FIBIntegerField5: TFIBIntegerField
      FieldName = 'SERISKI_BROJ'
    end
    object FIBStringField3: TFIBStringField
      FieldName = 'BARKOD'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object FIBStringField4: TFIBStringField
      FieldName = 'LOT'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object p1: TFIBFloatField
      FieldName = 'KOLICINA'
    end
    object FIBStringField5: TFIBStringField
      FieldName = 'SERISKI_BROJ1'
      Size = 50
      Transliterate = False
      EmptyStrToNull = True
    end
    object FIBStringField6: TFIBStringField
      FieldName = 'ZABELESKA'
      Size = 500
      Transliterate = False
      EmptyStrToNull = True
    end
    object p2: TFIBFloatField
      FieldName = 'KOLICINA_REALNA'
    end
    object FIBIntegerField6: TFIBIntegerField
      FieldName = 'VID_AMBALAZA'
    end
    object FIBIntegerField7: TFIBIntegerField
      FieldName = 'AMBALAZA'
    end
    object FIBStringField7: TFIBStringField
      FieldName = 'NAZIV_AMBALAZA'
      Size = 500
      Transliterate = False
      EmptyStrToNull = True
    end
    object FIBStringField8: TFIBStringField
      FieldName = 'BARKOD1'
      Size = 30
      Transliterate = False
      EmptyStrToNull = True
    end
    object p3: TFIBFloatField
      FieldName = 'KOLICINA_VO_PAKUVANJE'
    end
    object FIBIntegerField8: TFIBIntegerField
      FieldName = 'ID_PAKUVANJE'
    end
    object fbdtmfld1: TFIBDateTimeField
      FieldName = 'DATUM_PAK'
    end
    object FIBIntegerField9: TFIBIntegerField
      FieldName = 'ROK_UPOTREBA'
    end
    object fbdtfld1: TFIBDateField
      FieldName = 'DATUM_DO'
    end
    object FIBStringField9: TFIBStringField
      FieldName = 'LOT1'
      Size = 30
      Transliterate = False
      EmptyStrToNull = True
    end
    object FIBIntegerField10: TFIBIntegerField
      FieldName = 'KOLICINA_BROJ'
    end
    object FIBBCDField1: TFIBBCDField
      FieldName = 'ID_RNS'
      Size = 0
    end
    object FIBStringField10: TFIBStringField
      FieldName = 'NAZIV_ARTIKAL'
      Size = 500
      Transliterate = False
      EmptyStrToNull = True
    end
    object FIBIntegerField11: TFIBIntegerField
      FieldName = 'BR_KUTIJA'
    end
    object tblEtiketaStavkaKID_TRAILER: TFIBIntegerField
      FieldName = 'ID_TRAILER'
    end
    object tblEtiketaStavkaKKOL_TRAILER: TFIBIntegerField
      FieldName = 'KOL_TRAILER'
    end
    object tblEtiketaStavkaKID_POR_S_T: TFIBIntegerField
      FieldName = 'ID_POR_S_T'
    end
  end
  object tblEtiketaK: TpFIBDataSet
    UpdateSQL.Strings = (
      'UPDATE MAN_ETIKETA'
      'SET '
      '    ID_RABOTEN_NALOG = :ID_RABOTEN_NALOG,'
      '    DATUM = :DATUM,'
      '    TIP_ETIKETA = :TIP_ETIKETA,'
      '    SSCC = :SSCC,'
      '    ID_TRAILER = :ID_TRAILER'
      'WHERE'
      '    ID = :OLD_ID'
      '    ')
    DeleteSQL.Strings = (
      'DELETE FROM'
      '    MAN_ETIKETA'
      'WHERE'
      '        ID = :OLD_ID'
      '    ')
    InsertSQL.Strings = (
      'INSERT INTO MAN_ETIKETA('
      '    ID,'
      '    ID_RABOTEN_NALOG,'
      '    DATUM,'
      '    TIP_ETIKETA,'
      '    SSCC,'
      '    ID_TRAILER'
      ')'
      'VALUES('
      '    :ID,'
      '    :ID_RABOTEN_NALOG,'
      '    :DATUM,'
      '    :TIP_ETIKETA,'
      '    :SSCC,'
      '    :ID_TRAILER'
      ')')
    RefreshSQL.Strings = (
      'select'
      '    me.id,'
      '    me.id_raboten_nalog,'
      '    rn.broj br_rn,'
      '    rn.datum_planiran_pocetok datum_rn,'
      '    rn.godina,'
      '    me.datum,'
      '    me.tip_etiketa,'
      '    me.sscc,'
      '    me.id_trailer'
      'from man_etiketa me'
      'left join man_raboten_nalog rn on rn.id = me.id_raboten_nalog'
      ''
      ' WHERE '
      '        ME.ID = :OLD_ID'
      '    ')
    SelectSQL.Strings = (
      'select'
      '    me.id,'
      '    me.id_raboten_nalog,'
      '    rn.broj br_rn,'
      '    rn.datum_planiran_pocetok datum_rn,'
      '    rn.godina,'
      '    me.datum,'
      '    me.tip_etiketa,'
      '    me.sscc,'
      '    me.id_trailer'
      'from man_etiketa me'
      'left join man_raboten_nalog rn on rn.id = me.id_raboten_nalog'
      'order by me.id desc')
    AutoUpdateOptions.UpdateTableName = 'MAN_ETIKETA'
    AutoUpdateOptions.KeyFields = 'ID'
    AutoUpdateOptions.GeneratorName = 'GEN_MAN_ETIKETA_ID'
    AutoUpdateOptions.WhenGetGenID = wgBeforePost
    AutoUpdateOptions.AutoParamsToFields = True
    BeforeDelete = AllTableBeforeDelete
    Transaction = dmKon.tBaza
    Database = dmKon.fibBaza
    AutoCommit = True
    Left = 120
    Top = 208
    object FIBIntegerField12: TFIBIntegerField
      FieldName = 'ID'
    end
    object FIBBCDField2: TFIBBCDField
      FieldName = 'ID_RABOTEN_NALOG'
      Size = 0
    end
    object FIBStringField11: TFIBStringField
      FieldName = 'BR_RN'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object fbdtmfld2: TFIBDateTimeField
      FieldName = 'DATUM_RN'
    end
    object FIBIntegerField13: TFIBIntegerField
      FieldName = 'GODINA'
    end
    object fbdtmfld3: TFIBDateTimeField
      FieldName = 'DATUM'
    end
    object FIBIntegerField14: TFIBIntegerField
      FieldName = 'TIP_ETIKETA'
    end
    object FIBStringField12: TFIBStringField
      FieldName = 'SSCC'
      Size = 100
      Transliterate = False
      EmptyStrToNull = True
    end
    object tblEtiketaKID_TRAILER: TFIBIntegerField
      FieldName = 'ID_TRAILER'
    end
  end
  object dsEtiketaK: TDataSource
    DataSet = tblEtiketaK
    Left = 48
    Top = 208
  end
  object dsEtiketaStavkaK: TDataSource
    DataSet = tblEtiketaStavkaK
    Left = 48
    Top = 272
  end
end
