object frmCeniPoracki: TfrmCeniPoracki
  Left = 128
  Top = 212
  BorderIcons = [biSystemMenu, biMaximize]
  Caption = #1062#1077#1085#1080' '#1074#1086' '#1055#1086#1088#1072#1095#1082#1080
  ClientHeight = 681
  ClientWidth = 919
  Color = clBtnFace
  Constraints.MinHeight = 500
  Constraints.MinWidth = 610
  Font.Charset = RUSSIAN_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  Position = poScreenCenter
  ShowHint = True
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  TextHeight = 13
  object dxRibbon1: TdxRibbon
    Left = 0
    Top = 0
    Width = 919
    Height = 126
    BarManager = dxBarManager1
    ColorSchemeName = 'Blue'
    Contexts = <>
    TabOrder = 0
    TabStop = False
    OnTabChanging = dxRibbon1TabChanging
    object dxRibbon1Tab1: TdxRibbonTab
      Active = True
      Caption = #1052#1077#1085#1080
      Groups = <
        item
          ToolbarName = 'dxBarManager1Bar1'
        end
        item
          ToolbarName = 'dxBarManager1Bar2'
        end
        item
          ToolbarName = 'dxBarManager1Bar3'
        end>
      Index = 0
    end
    object dxRibbon1Tab2: TdxRibbonTab
      Caption = #1055#1086#1076#1077#1089#1091#1074#1072#1114#1072
      Groups = <
        item
          ToolbarName = 'dxBarManager1Bar4'
        end
        item
          ToolbarName = 'dxBarManager1BarIzgledGrid'
        end>
      Index = 1
    end
  end
  object dPanel: TPanel
    Left = 0
    Top = 126
    Width = 919
    Height = 555
    Align = alClient
    TabOrder = 1
    object cxGrid1: TcxGrid
      Left = 1
      Top = 1
      Width = 917
      Height = 533
      Align = alClient
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = []
      ParentFont = False
      TabOrder = 0
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      object cxGrid1DBTableView1: TcxGridDBTableView
        Navigator.Buttons.CustomButtons = <>
        ScrollbarAnnotations.CustomAnnotations = <>
        DataController.DataSource = dm.dsCenaPoracki
        DataController.Summary.DefaultGroupSummaryItems = <>
        DataController.Summary.FooterSummaryItems = <>
        DataController.Summary.SummaryGroups = <>
        OptionsBehavior.GoToNextCellOnEnter = True
        OptionsBehavior.FocusCellOnCycle = True
        OptionsCustomize.ColumnsQuickCustomization = True
        OptionsView.ColumnAutoWidth = True
        OptionsView.Indicator = True
        object cxGrid1DBTableView1ID: TcxGridDBColumn
          DataBinding.FieldName = 'ID'
          Visible = False
          Options.Editing = False
        end
        object cxGrid1DBTableView1NAZIV_TRAILER: TcxGridDBColumn
          Caption = #1053#1072#1079#1080#1074' '#1082#1072#1084#1080#1086#1085
          DataBinding.FieldName = 'NAZIV_TRAILER'
          Visible = False
          GroupIndex = 0
          Width = 150
        end
        object cxGrid1DBTableView1NAZIV_PARTNER: TcxGridDBColumn
          Caption = #1055#1072#1088#1090#1085#1077#1088
          DataBinding.FieldName = 'NAZIV_PARTNER'
          Options.Editing = False
          Width = 147
        end
        object cxGrid1DBTableView1REF_NO: TcxGridDBColumn
          Caption = #1056#1077#1092'. '#1073#1088
          DataBinding.FieldName = 'REF_NO'
          Options.Editing = False
          Width = 137
        end
        object cxGrid1DBTableView1TIP_PARTNER: TcxGridDBColumn
          Caption = #1058#1080#1087' '#1085#1072' '#1055#1072#1088#1090#1085#1077#1088
          DataBinding.FieldName = 'TIP_PARTNER'
          Visible = False
          Options.Editing = False
          Width = 31
        end
        object cxGrid1DBTableView1PARTNER: TcxGridDBColumn
          Caption = #1064#1080#1092#1088#1072' '#1085#1072' '#1055#1072#1088#1090#1085#1077#1088
          DataBinding.FieldName = 'PARTNER'
          Visible = False
          Options.Editing = False
          Width = 20
        end
        object cxGrid1DBTableView1DATUM: TcxGridDBColumn
          DataBinding.FieldName = 'DATUM'
          Visible = False
          Options.Editing = False
        end
        object cxGrid1DBTableView1NAZIV_STATUS: TcxGridDBColumn
          DataBinding.FieldName = 'NAZIV_STATUS'
          Visible = False
          Options.Editing = False
        end
        object cxGrid1DBTableView1VID_ARTIKAL: TcxGridDBColumn
          Caption = #1042#1080#1076' '#1085#1072' '#1072#1088#1090#1080#1082#1072#1083
          DataBinding.FieldName = 'VID_ARTIKAL'
          Options.Editing = False
          Width = 20
        end
        object cxGrid1DBTableView1ARTIKAL: TcxGridDBColumn
          Caption = #1064#1080#1092#1088#1072' '#1085#1072' '#1072#1088#1090#1080#1082#1072#1083
          DataBinding.FieldName = 'ARTIKAL'
          Options.Editing = False
          Width = 32
        end
        object cxGrid1DBTableView1NAZIV_ARTIKAL: TcxGridDBColumn
          Caption = #1040#1088#1090#1080#1082#1072#1083
          DataBinding.FieldName = 'NAZIV_ARTIKAL'
          Options.Editing = False
          Width = 151
        end
        object cxGrid1DBTableView1CENA: TcxGridDBColumn
          Caption = #1062#1077#1085#1072
          DataBinding.FieldName = 'CENA'
          Styles.Content = dmRes.cxStyle201
          Width = 78
        end
        object cxGrid1DBTableView1VALUTA: TcxGridDBColumn
          Caption = #1042#1072#1083#1091#1090#1072
          DataBinding.FieldName = 'VALUTA'
          PropertiesClassName = 'TcxLookupComboBoxProperties'
          Properties.KeyFieldNames = 'ID'
          Properties.ListColumns = <
            item
              FieldName = 'NAZIV'
            end>
          Properties.ListSource = dmMat.dsValuta
          Styles.Content = dmRes.cxStyle201
          Width = 63
        end
        object cxGrid1DBTableView1KURS: TcxGridDBColumn
          Caption = #1050#1091#1088#1089
          DataBinding.FieldName = 'KURS'
          Styles.Content = dmRes.cxStyle201
          Width = 57
        end
        object cxGrid1DBTableView1TAPACIR_SEDISTE: TcxGridDBColumn
          Caption = #1058#1072#1087#1072#1094#1080#1088' - '#1089#1077#1076#1080#1096#1090#1077
          DataBinding.FieldName = 'TAPACIR_SEDISTE'
          Options.Editing = False
          Width = 40
        end
        object cxGrid1DBTableView1TAPACIR_NAZAD: TcxGridDBColumn
          Caption = #1058#1072#1087#1072#1094#1080#1088' - '#1085#1072#1079#1072#1076
          DataBinding.FieldName = 'TAPACIR_NAZAD'
          Options.Editing = False
          Width = 44
        end
        object cxGrid1DBTableView1BOJA_NOGARKI: TcxGridDBColumn
          Caption = #1041#1086#1112#1072' '#1085#1072' '#1085#1086#1075#1072#1088#1082#1080
          DataBinding.FieldName = 'BOJA_NOGARKI'
          Options.Editing = False
          Width = 45
        end
        object cxGrid1DBTableView1SUNGER: TcxGridDBColumn
          DataBinding.FieldName = 'SUNGER'
          Visible = False
          Options.Editing = False
          Width = 50
        end
        object cxGrid1DBTableView1LABELS: TcxGridDBColumn
          DataBinding.FieldName = 'LABELS'
          Visible = False
          Options.Editing = False
          Width = 50
        end
        object cxGrid1DBTableView1GLIDERS: TcxGridDBColumn
          DataBinding.FieldName = 'GLIDERS'
          Visible = False
          Options.Editing = False
          Width = 50
        end
        object cxGrid1DBTableView1KOLICINA: TcxGridDBColumn
          Caption = #1050#1086#1083#1080#1095#1080#1085#1072
          DataBinding.FieldName = 'KOLICINA'
          Options.Editing = False
          Width = 89
        end
        object cxGrid1DBTableView1ZABELESKA: TcxGridDBColumn
          Caption = #1047#1072#1073#1077#1083#1077#1096#1082#1072
          DataBinding.FieldName = 'ZABELESKA'
          Visible = False
          Options.Editing = False
          Width = 100
        end
        object cxGrid1DBTableView1ID_TRAILER: TcxGridDBColumn
          Caption = #1064#1080#1092#1088#1072' '#1082#1072#1084#1080#1086#1085
          DataBinding.FieldName = 'ID_TRAILER'
          Visible = False
        end
        object cxGrid1DBTableView1CUSTOMER_NAME: TcxGridDBColumn
          DataBinding.FieldName = 'CUSTOMER_NAME'
          Width = 150
        end
        object cxGrid1DBTableView1ANG_NAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'ANG_NAZIV'
          Width = 150
        end
        object cxGrid1DBTableView1POR_VO_TRAILER: TcxGridDBColumn
          Caption = #1053#1072#1079#1080#1074' '#1050#1072#1084#1080#1086#1085
          DataBinding.FieldName = 'POR_VO_TRAILER'
          Visible = False
          Width = 150
        end
      end
      object cxGrid1Level1: TcxGridLevel
        GridView = cxGrid1DBTableView1
      end
    end
    object dxStatusBar1: TdxStatusBar
      Left = 1
      Top = 534
      Width = 917
      Height = 20
      Panels = <
        item
          PanelStyleClassName = 'TdxStatusBarTextPanelStyle'
          Text = 'Esc - '#1048#1079#1083#1077#1079
        end>
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = []
    end
  end
  object cxGridPopupMenu1: TcxGridPopupMenu
    Grid = cxGrid1
    PopupMenus = <>
    Left = 51
    Top = 16
  end
  object PopupMenu1: TPopupMenu
    Left = 234
    Top = 65528
    object N1: TMenuItem
      Caption = #1057#1085#1080#1084#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      ShortCut = 49235
      OnClick = SaveToIniFileExecute
    end
    object Excel1: TMenuItem
      Caption = #1057#1085#1080#1084#1080' '#1075#1086' '#1074#1086' Excel'
      ShortCut = 49221
    end
  end
  object dxBarManager1: TdxBarManager
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Segoe UI'
    Font.Style = []
    Categories.Strings = (
      'Default')
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    ImageOptions.Images = dmRes.cxSmallImages
    ImageOptions.LargeImages = dmRes.cxLargeImages
    PopupMenuLinks = <>
    UseSystemFont = True
    Left = 86
    Top = 16
    PixelsPerInch = 96
    object dxBarManager1Bar1: TdxBar
      CaptionButtons = <>
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 715
      FloatTop = 157
      FloatClientWidth = 51
      FloatClientHeight = 120
      ItemLinks = <
        item
          Visible = True
          ItemName = 'cbGodina'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar2: TdxBar
      Caption = #1040#1082#1094#1080#1080
      CaptionButtons = <>
      DockedLeft = 157
      DockedTop = 0
      FloatLeft = 789
      FloatTop = 120
      FloatClientWidth = 51
      FloatClientHeight = 120
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxbrlrgbtn1'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarLargeButton5'
        end
        item
          Visible = True
          ItemName = 'dxBarSubItem1'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar3: TdxBar
      Caption = #1048#1079#1083#1077#1079
      CaptionButtons = <>
      DockedLeft = 373
      DockedTop = 0
      FloatLeft = 718
      FloatTop = 62
      FloatClientWidth = 51
      FloatClientHeight = 80
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton7'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton8'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar4: TdxBar
      Caption = #1055#1077#1095#1072#1090#1077#1114#1077
      CaptionButtons = <>
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 676
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton12'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton15'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton9'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarLargeButton13'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1BarIzgledGrid: TdxBar
      Caption = #1058#1072#1073#1077#1083#1072
      CaptionButtons = <>
      DockedLeft = 363
      DockedTop = 0
      FloatLeft = 808
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLBtnSnimiIzgled'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarLBtnBrisiIzgled'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarLargeButton2: TdxBarLargeButton
      Action = aAzuriraj
      Category = 0
    end
    object dxBarLargeButton3: TdxBarLargeButton
      Action = aBrisi
      Category = 0
    end
    object dxBarLargeButton4: TdxBarLargeButton
      Action = aSnimiIzgled
      Category = 0
      ScreenTip = tipSnimiIzgled
    end
    object dxBarLargeButton5: TdxBarLargeButton
      Action = aZacuvajExcel
      Category = 0
      ScreenTip = tipZacuvajExcel
    end
    object dxBarLargeButton6: TdxBarLargeButton
      Action = aPecatiTabela
      Caption = #1055#1077#1095#1072#1090#1080' '#1090#1072#1073#1077#1083#1072
      Category = 0
    end
    object dxBarLargeButton7: TdxBarLargeButton
      Action = aHelp
      Category = 0
    end
    object dxBarLargeButton8: TdxBarLargeButton
      Action = aIzlez
      Category = 0
    end
    object dxBarLargeButton1: TdxBarLargeButton
      Action = aNov
      Category = 0
    end
    object dxBarLargeButton9: TdxBarLargeButton
      Action = aSnimiPecatenje
      Category = 0
      ScreenTip = tipSnimiKonf
    end
    object dxBarLargeButton11: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object ribbonFilterReset: TdxBarLargeButton
      Caption = #1056#1077#1089#1077#1090#1080#1088#1072#1112' '#1092#1080#1083#1090#1077#1088
      Category = 0
      Hint = #1056#1077#1089#1077#1090#1080#1088#1072#1112' '#1092#1080#1083#1090#1077#1088
      Visible = ivAlways
    end
    object dxBarLargeButton12: TdxBarLargeButton
      Action = aPodesuvanjePecatenje
      Category = 0
      ScreenTip = tipDizajnReport
    end
    object dxBarLargeButton13: TdxBarLargeButton
      Action = aBrisiPodesuvanjePecatenje
      Category = 0
      ScreenTip = tipBrisiKonf
    end
    object dxBarLargeButton14: TdxBarLargeButton
      Caption = #1048#1079#1073#1088#1080#1096#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      Category = 0
      Visible = ivAlways
      LargeImageIndex = 40
    end
    object dxBarLargeButton15: TdxBarLargeButton
      Action = aPageSetup
      Category = 0
      ScreenTip = tipSetiranjeStrana
    end
    object dxBarLargeButton16: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object dxBarLargeButton17: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object cxBarEditItem1: TcxBarEditItem
      Caption = 'New Item'
      Category = 0
      Hint = 'New Item'
      Visible = ivAlways
      PropertiesClassName = 'TcxCheckBoxProperties'
    end
    object dxBarSubItem1: TdxBarSubItem
      Caption = #1055#1077#1095#1072#1090#1077#1114#1077
      Category = 0
      Visible = ivAlways
      ImageIndex = 30
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton6'
        end>
    end
    object dxBarLBtnSnimiIzgled: TdxBarLargeButton
      Action = aSnimiIzgled
      Category = 0
    end
    object dxBarLBtnBrisiIzgled: TdxBarLargeButton
      Action = aBrisiIzgled
      Category = 0
    end
    object cbGodina: TcxBarEditItem
      Caption = #1043#1086#1076#1080#1085#1072
      Category = 0
      Hint = #1043#1086#1076#1080#1085#1072
      Visible = ivAlways
      OnChange = cbGodinaChange
      PropertiesClassName = 'TcxComboBoxProperties'
      Properties.ImmediateUpdateText = True
      Properties.Items.Strings = (
        '2010'
        '2011'
        '2012'
        '2013'
        '2014'
        '2015'
        '2016')
    end
    object dxBarLargeButton10: TdxBarLargeButton
      Action = aZatvoriRN
      Category = 0
    end
    object dxbrlrgbtn1: TdxBarLargeButton
      Action = aOsveziPodatoci
      Caption = #1054#1089#1074#1077#1078#1080' '#1055#1086#1076#1072#1090#1086#1094#1080
      Category = 0
    end
  end
  object ActionList1: TActionList
    Images = dmRes.cxSmallImages
    Left = 16
    Top = 16
    object aNov: TAction
      Caption = #1053#1086#1074
      ImageIndex = 10
      ShortCut = 116
      OnExecute = aNovExecute
    end
    object aAzuriraj: TAction
      Caption = #1040#1078#1091#1088#1080#1088#1072#1112
      ImageIndex = 12
      ShortCut = 117
      OnExecute = aAzurirajExecute
    end
    object aBrisi: TAction
      Caption = #1041#1088#1080#1096#1080
      ImageIndex = 11
      ShortCut = 119
      OnExecute = aBrisiExecute
    end
    object aSnimiIzgled: TAction
      Caption = #1057#1085#1080#1084#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      ImageIndex = 9
      ShortCut = 24659
      OnExecute = aSnimiIzgledExecute
    end
    object aZacuvajExcel: TAction
      Caption = #1047#1072#1095#1091#1074#1072#1112' '#1074#1086' Excel'
      ImageIndex = 9
      ShortCut = 24645
      OnExecute = aZacuvajExcelExecute
    end
    object aPecatiTabela: TAction
      Caption = #1055#1077#1095#1072#1090#1080
      ImageIndex = 30
      OnExecute = aPecatiTabelaExecute
    end
    object aHelp: TAction
      Caption = #1055#1086#1084#1086#1096
      ImageIndex = 24
    end
    object aIzlez: TAction
      Caption = #1048#1079#1083#1077#1079
      ImageIndex = 25
      OnExecute = aIzlezExecute
    end
    object aZapisi: TAction
      Caption = #1047#1072#1087#1080#1096#1080
      ImageIndex = 7
      ShortCut = 120
      OnExecute = aZapisiExecute
    end
    object aOtkazi: TAction
      Caption = #1054#1090#1082#1072#1078#1080
      ImageIndex = 21
      ShortCut = 27
      OnExecute = aOtkaziExecute
    end
    object aRefresh: TAction
      Caption = 'aRefresh'
      ImageIndex = 18
      OnExecute = aRefreshExecute
    end
    object aSnimiPecatenje: TAction
      Caption = #1057#1085#1080#1084#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      ImageIndex = 39
      OnExecute = aSnimiPecatenjeExecute
    end
    object aPodesuvanjePecatenje: TAction
      Caption = #1044#1080#1079#1072#1112#1085' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090
      ImageIndex = 38
      OnExecute = aPodesuvanjePecatenjeExecute
    end
    object aBrisiPodesuvanjePecatenje: TAction
      Caption = #1041#1088#1080#1096#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      ImageIndex = 37
      OnExecute = aBrisiPodesuvanjePecatenjeExecute
    end
    object aPageSetup: TAction
      Caption = #1057#1077#1090#1080#1088#1072#1114#1077' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072
      ImageIndex = 38
      OnExecute = aPageSetupExecute
    end
    object aFormConfig: TAction
      Caption = 'aFormConfig'
      ShortCut = 24699
      OnExecute = aFormConfigExecute
    end
    object aBrisiIzgled: TAction
      Caption = #1041#1088#1080#1096#1080' '#1075#1086' '#1079#1072#1095#1091#1074#1072#1085#1080#1086#1090' '#1080#1079#1075#1083#1077#1076
      ImageIndex = 56
      OnExecute = aBrisiIzgledExecute
    end
    object aZatvoriRN: TAction
      Caption = #1047#1072#1090#1074#1086#1088#1080' '#1056#1053
      ImageIndex = 85
      OnExecute = aZatvoriRNExecute
    end
    object aOsveziPodatoci: TAction
      Caption = 'aOsveziPodatoci'
      ImageIndex = 18
      ShortCut = 118
      OnExecute = aOsveziPodatociExecute
    end
  end
  object dxComponentPrinter1: TdxComponentPrinter
    CurrentLink = dxComponentPrinter1Link1
    Version = 0
    Left = 156
    PixelsPerInch = 96
    object dxComponentPrinter1Link1: TdxGridReportLink
      DateFormat = 0
      PageNumberFormat = pnfNumeral
      PrinterPage.DMPaper = 9
      PrinterPage.Footer = 6350
      PrinterPage.GrayShading = True
      PrinterPage.Header = 6350
      PrinterPage.Margins.Bottom = 12700
      PrinterPage.Margins.Left = 12700
      PrinterPage.Margins.Right = 12700
      PrinterPage.Margins.Top = 12700
      PrinterPage.Orientation = poLandscape
      PrinterPage.PageFooter.RightTitle.Strings = (
        #1057#1090#1088'.[Page #] '#1086#1076' [Total Pages]')
      PrinterPage.PageSize.X = 210000
      PrinterPage.PageSize.Y = 297000
      PrinterPage._dxMeasurementUnits_ = 0
      PrinterPage._dxLastMU_ = 2
      ReportDocument.CreationDate = 40336.425359629630000000
      TimeFormat = 0
      OptionsFormatting.UseNativeStyles = True
      OptionsOnEveryPage.Caption = False
      OptionsOnEveryPage.FilterBar = False
      OptionsView.Caption = False
      OptionsView.FilterBar = False
      StyleRepository = dmRes.cxStyleRepositoryPrinting
      Styles.StyleSheet = dmRes.dxGridReportLinkStyleSheet
      PixelsPerInch = 96
      BuiltInReportLink = True
    end
  end
  object dxBarScreenTipRepository1: TdxBarScreenTipRepository
    Left = 113
    PixelsPerInch = 96
    object tipSnimiIzgled: TdxBarScreenTip
      Header.Text = #1057#1085#1080#1084#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      Description.Text = #1047#1072#1095#1091#1074#1072#1112' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090' '#1085#1072' '#1092#1086#1088#1084#1072#1090#1072' '#1080' '#1085#1072' '#1090#1072#1073#1077#1083#1072#1090#1072
    end
    object tipZacuvajExcel: TdxBarScreenTip
      Header.Text = #1047#1072#1095#1091#1074#1072#1112' '#1074#1086' Excel'
      Description.Text = #1057#1085#1080#1084#1080' '#1075#1080' '#1087#1086#1076#1072#1090#1086#1094#1080#1090#1077' '#1086#1076' '#1090#1072#1073#1077#1083#1072#1090#1072' '#1074#1086' Excel '#1092#1086#1088#1084#1072#1090
    end
    object tipDizajnReport: TdxBarScreenTip
      Header.Text = #1044#1080#1079#1072#1112#1085' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090
      Description.Text = #1055#1086#1076#1077#1089#1091#1074#1072#1114#1072' '#1085#1072' '#1080#1079#1075#1083#1077#1076#1086#1090' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090' '#1085#1072' '#1090#1072#1073#1077#1083#1072#1090#1072
    end
    object tipSetiranjeStrana: TdxBarScreenTip
      Header.Text = #1057#1077#1090#1080#1088#1072#1114#1077' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072
      Description.Text = #1055#1086#1076#1077#1089#1091#1074#1072#1114#1077' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072' '#1085#1072' '#1082#1086#1112#1072' '#1116#1077' '#1089#1077' '#1087#1077#1095#1072#1090#1080' '#1090#1072#1073#1077#1083#1072#1090#1072
    end
    object tipSnimiKonf: TdxBarScreenTip
      Header.Text = #1057#1085#1080#1084#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      Description.Text = #1047#1072#1095#1091#1074#1072#1112' '#1075#1080' '#1087#1086#1076#1077#1089#1091#1074#1072#1114#1072#1090#1072' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090' '#1080' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072' '#1079#1072' '#1087#1077#1095#1072#1090#1077#1114#1077
    end
    object tipBrisiKonf: TdxBarScreenTip
      Header.Text = #1041#1088#1080#1096#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      Description.Text = #1048#1079#1073#1088#1080#1096#1080' '#1075#1080' '#1087#1086#1076#1077#1089#1091#1074#1072#1114#1072#1090#1072' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090' '#1080' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072' '#1079#1072' '#1087#1077#1095#1072#1090#1077#1114#1077
    end
  end
  object qZatvoriRN: TpFIBUpdateObject
    Transaction = dmKon.qTransaction
    Database = dmKon.fibBaza
    SQL.Strings = (
      'UPDATE MAN_RABOTEN_NALOG SET '
      '    STATUS = 2,'
      '    DATUM_KRAJ = current_timestamp'
      'WHERE'
      '    ID=:ID'
      '')
    OrderInList = 0
    Left = 199
    Top = 65528
    qoAutoCommit = True
    qoStartTransaction = True
  end
end
