inherited frmSosDel: TfrmSosDel
  Caption = #1057#1086#1089#1090#1072#1074#1077#1085' '#1076#1077#1083' '#1085#1072' '#1055#1088#1086#1080#1079#1074#1086#1076
  ClientHeight = 601
  ClientWidth = 842
  ExplicitWidth = 858
  ExplicitHeight = 640
  PixelsPerInch = 96
  TextHeight = 13
  inherited lPanel: TPanel
    Width = 842
    Height = 298
    ExplicitWidth = 842
    ExplicitHeight = 298
    inherited cxGrid1: TcxGrid
      Width = 838
      Height = 294
      Font.Height = -13
      ExplicitWidth = 838
      ExplicitHeight = 294
      inherited cxGrid1DBTableView1: TcxGridDBTableView
        DataController.DataSource = dm.dsArtSD
        object cxGrid1DBTableView1ID: TcxGridDBColumn
          Caption = #1064#1080#1092#1088#1072
          DataBinding.FieldName = 'ID'
          Width = 132
        end
        object cxGrid1DBTableView1NAZIV: TcxGridDBColumn
          Caption = #1053#1072#1079#1080#1074
          DataBinding.FieldName = 'NAZIV'
          Width = 360
        end
        object cxGrid1DBTableView1ARTVID: TcxGridDBColumn
          DataBinding.FieldName = 'ARTVID'
          Visible = False
        end
      end
    end
  end
  inherited dPanel: TPanel
    Top = 424
    Width = 842
    ExplicitTop = 424
    ExplicitWidth = 842
    inherited Label1: TLabel
      Width = 56
      Height = 16
      Font.Height = -13
      ExplicitWidth = 56
      ExplicitHeight = 16
    end
    object Label2: TLabel [1]
      Left = 31
      Top = 53
      Width = 48
      Height = 16
      Caption = #1053#1072#1079#1080#1074' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clRed
      Font.Height = -13
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    inherited Sifra: TcxDBTextEdit
      Left = 99
      Top = 21
      DataBinding.DataField = 'ID'
      DataBinding.DataSource = dm.dsArtSD
      Style.Font.Height = -13
      Style.IsFontAssigned = True
      ExplicitLeft = 99
      ExplicitTop = 21
      ExplicitWidth = 118
      ExplicitHeight = 24
      Width = 118
    end
    inherited OtkaziButton: TcxButton
      Left = 687
      Width = 115
      TabOrder = 3
      Font.Height = -13
      ParentFont = False
      ExplicitLeft = 687
      ExplicitWidth = 115
    end
    inherited ZapisiButton: TcxButton
      Left = 551
      Width = 130
      TabOrder = 2
      Font.Height = -13
      ParentFont = False
      ExplicitLeft = 551
      ExplicitWidth = 130
    end
    object txtNaziv: TcxDBTextEdit
      Left = 99
      Top = 50
      DataBinding.DataField = 'NAZIV'
      DataBinding.DataSource = dm.dsArtSD
      ParentFont = False
      Style.Font.Charset = RUSSIAN_CHARSET
      Style.Font.Color = clWindowText
      Style.Font.Height = -13
      Style.Font.Name = 'Tahoma'
      Style.Font.Style = []
      Style.IsFontAssigned = True
      TabOrder = 1
      OnKeyDown = EnterKakoTab
      Width = 414
    end
  end
  inherited dxRibbon1: TdxRibbon
    Width = 842
    ExplicitWidth = 842
    inherited dxRibbon1Tab1: TdxRibbonTab
      Index = 0
    end
    inherited dxRibbon1Tab2: TdxRibbonTab
      Index = 1
    end
  end
  inherited StatusBar1: TdxRibbonStatusBar
    Top = 578
    Width = 842
    Font.Height = -12
    ExplicitTop = 578
    ExplicitWidth = 842
  end
  inherited dxBarManager1: TdxBarManager
    PixelsPerInch = 96
    inherited dxBarManager1Bar1: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 69
      FloatClientHeight = 167
    end
    inherited dxBarManager1Bar2: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 149
      FloatClientHeight = 188
    end
    inherited dxBarManager1Bar3: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 59
      FloatClientHeight = 108
    end
    inherited dxBarManager1Bar4: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 141
      FloatClientHeight = 221
    end
    inherited dxBarManager1BarTabela: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 174
      FloatClientHeight = 113
    end
  end
  inherited dxComponentPrinter1: TdxComponentPrinter
    inherited dxComponentPrinter1Link1: TdxGridReportLink
      ReportDocument.CreationDate = 44051.441081574080000000
      BuiltInReportLink = True
    end
  end
  inherited dxBarScreenTipRepository1: TdxBarScreenTipRepository
    PixelsPerInch = 96
  end
end
