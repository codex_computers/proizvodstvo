object frmMagacin: TfrmMagacin
  Left = 0
  Top = 0
  BorderIcons = [biSystemMenu]
  Caption = #1054#1076#1073#1077#1088#1080' '#1084#1072#1075#1072#1094#1080#1085
  ClientHeight = 500
  ClientWidth = 533
  Color = 15790320
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  OldCreateOrder = False
  Position = poOwnerFormCenter
  OnClose = FormClose
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 533
    Height = 500
    Align = alClient
    Color = 15790320
    ParentBackground = False
    TabOrder = 0
    object dxTree: TdxDBTreeView
      Left = 1
      Top = 42
      Width = 531
      Height = 457
      ShowNodeHint = True
      HotTrack = True
      RowSelect = True
      DataSource = dmMat.dsReMagacin
      DisplayField = 'ID;NAZIV'
      KeyField = 'ID'
      ListField = 'NAZIV'
      ParentField = 'KOREN'
      RootValue = Null
      SeparatedSt = ' -> '
      RaiseOnError = True
      ReadOnly = True
      DragMode = dmAutomatic
      HideSelection = False
      Indent = 19
      Align = alClient
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clWindowText
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentColor = False
      ParentCtl3D = False
      Ctl3D = True
      Options = [trDBConfirmDelete, trCanDBNavigate, trSmartRecordCopy, trCheckHasChildren]
      SelectedIndex = -1
      TabOrder = 0
      OnDblClick = dxTreeDblClick
      OnKeyDown = dxTreeKeyDown
      ParentFont = False
    end
    object lPanel: TPanel
      Left = 1
      Top = 1
      Width = 531
      Height = 41
      Align = alTop
      Color = 15790320
      ParentBackground = False
      TabOrder = 1
      object lcbMagacin: TcxLookupComboBox
        AlignWithMargins = True
        Left = 9
        Top = 9
        Margins.Left = 6
        Margins.Top = 6
        Margins.Right = 6
        Margins.Bottom = 6
        Properties.DropDownAutoSize = True
        Properties.DropDownSizeable = True
        Properties.ImmediatePost = True
        Properties.KeyFieldNames = 'ID'
        Properties.ListColumns = <
          item
            FieldName = 'ID'
          end
          item
            Width = 400
            FieldName = 'NAZIV'
          end>
        Properties.ListOptions.AnsiSort = True
        Properties.ListOptions.SyncMode = True
        Properties.ListSource = dmMat.dsReMagacin
        TabOrder = 0
        OnKeyDown = EnterKakoTab
        Width = 93
      end
      object lblMagacin: TcxDBLabel
        Left = 192
        Top = 10
        DataBinding.DataField = 'NAZIV'
        DataBinding.DataSource = dmMat.dsReMagacin
        ParentFont = False
        Style.Font.Charset = DEFAULT_CHARSET
        Style.Font.Color = clWindowText
        Style.Font.Height = -11
        Style.Font.Name = 'Tahoma'
        Style.Font.Style = [fsUnderline]
        Style.IsFontAssigned = True
        Transparent = True
        Height = 21
        Width = 329
      end
      object btnOdberi: TcxButton
        Left = 111
        Top = 7
        Width = 75
        Height = 25
        Action = aOdberi
        TabOrder = 1
      end
    end
  end
  object ActionList1: TActionList
    Images = dmRes.cxSmallImages
    Left = 176
    Top = 104
    object aOtkazi: TAction
      Caption = 'aOtkazi'
      ShortCut = 27
      OnExecute = aOtkaziExecute
    end
    object aOdberi: TAction
      Caption = #1054#1076#1073#1077#1088#1080
      ImageIndex = 6
      OnExecute = aOdberiExecute
    end
  end
  object cxEditRepository1: TcxEditRepository
    Left = 184
    Top = 200
  end
end
