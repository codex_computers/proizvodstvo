object frmInternoBaranje: TfrmInternoBaranje
  Tag = 6
  AlignWithMargins = True
  Left = 128
  Top = 201
  BorderIcons = [biSystemMenu, biMaximize]
  Caption = #1048#1085#1090#1077#1088#1085#1086' '#1073#1072#1088#1072#1114#1077
  ClientHeight = 657
  ClientWidth = 772
  Color = clBtnFace
  Constraints.MinHeight = 500
  Constraints.MinWidth = 610
  Font.Charset = RUSSIAN_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  Position = poScreenCenter
  ShowHint = True
  OnClose = FormClose
  OnCreate = FormCreate
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 13
  object dxRibbon1: TdxRibbon
    Left = 0
    Top = 0
    Width = 772
    Height = 126
    BarManager = dxBarManager1
    ColorSchemeName = 'Blue'
    Contexts = <>
    TabOrder = 0
    TabStop = False
    OnTabChanging = dxRibbon1TabChanging
    object dxRibbon1Tab1: TdxRibbonTab
      Active = True
      Caption = #1052#1077#1085#1080
      Groups = <
        item
          ToolbarName = 'dxBarManager1Bar1'
        end
        item
          ToolbarName = 'dxBarManager1Bar6'
        end
        item
          ToolbarName = 'dxBarManager1Bar2'
        end
        item
          ToolbarName = 'dxBarManager1Bar3'
        end>
      Index = 0
    end
    object dxRibbon1Tab2: TdxRibbonTab
      Caption = #1055#1086#1076#1077#1089#1091#1074#1072#1114#1072
      Groups = <
        item
          ToolbarName = 'dxBarManager1Bar4'
        end>
      Index = 1
    end
  end
  object StatusBar1: TdxRibbonStatusBar
    Left = 0
    Top = 634
    Width = 772
    Height = 23
    Panels = <
      item
        PanelStyleClassName = 'TdxStatusBarTextPanelStyle'
        Text = 
          'F5 - '#1053#1086#1074', F6 - '#1040#1078#1091#1088#1080#1088#1072#1112', F7 - '#1054#1089#1074#1077#1078#1080', F8 - '#1041#1088#1080#1096#1080', F9 - '#1047#1072#1087#1080#1096#1080', E' +
          'sc - '#1054#1090#1082#1072#1078#1080' / '#1048#1079#1083#1077#1079
      end>
    Ribbon = dxRibbon1
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clDefault
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
  end
  object PageControl: TcxPageControl
    Left = 0
    Top = 126
    Width = 772
    Height = 508
    Align = alClient
    TabOrder = 2
    Properties.ActivePage = tcInternoBaranje
    ClientRectBottom = 508
    ClientRectRight = 772
    ClientRectTop = 24
    object tcInternoBaranje: TcxTabSheet
      Caption = #1048#1085#1090#1077#1088#1085#1086' '#1073#1072#1088#1072#1114#1077
      ImageIndex = 0
      object lPanel: TPanel
        Left = 0
        Top = 0
        Width = 772
        Height = 376
        Align = alClient
        TabOrder = 0
        object cxGrid1: TcxGrid
          Left = 1
          Top = 1
          Width = 770
          Height = 374
          Align = alClient
          TabOrder = 0
          object cxGrid1DBTableView1: TcxGridDBTableView
            OnFocusedRecordChanged = cxGrid1DBTableView1FocusedRecordChanged
            DataController.DataSource = dm.dsInternoBaranje
            DataController.Options = [dcoAnsiSort, dcoAssignGroupingValues, dcoAssignMasterDetailKeys, dcoSaveExpanding]
            DataController.Summary.DefaultGroupSummaryItems = <>
            DataController.Summary.FooterSummaryItems = <
              item
                Format = '#,##0.00'
                Kind = skSum
                FieldName = 'SUMA'
              end>
            DataController.Summary.SummaryGroups = <>
            FilterRow.InfoText = #1050#1083#1080#1082#1085#1077#1090#1077' '#1086#1074#1076#1077' '#1079#1072' '#1076#1072' '#1076#1077#1092#1080#1085#1080#1088#1072#1090#1077' '#1092#1080#1083#1090#1077#1088
            FilterRow.Visible = True
            OptionsBehavior.IncSearch = True
            OptionsCustomize.ColumnsQuickCustomization = True
            OptionsCustomize.ColumnsQuickCustomizationReordering = qcrEnabled
            OptionsData.Deleting = False
            OptionsData.Editing = False
            OptionsData.Inserting = False
            OptionsView.Footer = True
            OptionsView.GroupFooters = gfAlwaysVisible
            object cxGrid1DBTableView1RE: TcxGridDBColumn
              Caption = #1064#1080#1092#1088#1072' '#1085#1072' '#1083#1086#1082#1072#1094#1080#1112#1072
              DataBinding.FieldName = 'RE'
              Width = 77
            end
            object cxGrid1DBTableView1OD_KADE: TcxGridDBColumn
              Caption = #1051#1086#1082#1072#1094#1080#1112#1072
              DataBinding.FieldName = 'OD_KADE'
              Width = 132
            end
            object cxGrid1DBTableView1BROJ: TcxGridDBColumn
              Caption = #1041#1088#1086#1112
              DataBinding.FieldName = 'BROJ'
            end
            object cxGrid1DBTableView1DATUM: TcxGridDBColumn
              Caption = #1044#1072#1090#1091#1084
              DataBinding.FieldName = 'DATUM'
            end
            object cxGrid1DBTableView1OPIS: TcxGridDBColumn
              Caption = #1054#1087#1080#1089
              DataBinding.FieldName = 'OPIS'
              Width = 165
            end
            object cxGrid1DBTableView1RE2: TcxGridDBColumn
              Caption = #1064#1080#1092#1088#1072' '#1085#1072' '#1076#1077#1089#1090#1080#1085#1072#1094#1080#1112#1072
              DataBinding.FieldName = 'RE2'
              Width = 43
            end
            object cxGrid1DBTableView1KADE: TcxGridDBColumn
              Caption = #1044#1077#1089#1090#1080#1085#1072#1094#1080#1112#1072
              DataBinding.FieldName = 'KADE'
              Width = 205
            end
            object cxGrid1DBTableView1RBN_RE: TcxGridDBColumn
              DataBinding.FieldName = 'RBN_RE'
              Visible = False
            end
            object cxGrid1DBTableView1RBN_BROJ: TcxGridDBColumn
              DataBinding.FieldName = 'RBN_BROJ'
              Visible = False
            end
          end
          object cxGrid1Level1: TcxGridLevel
            GridView = cxGrid1DBTableView1
          end
        end
      end
      object dPanel: TPanel
        Left = 0
        Top = 376
        Width = 772
        Height = 108
        Align = alBottom
        Enabled = False
        TabOrder = 1
        DesignSize = (
          772
          108)
        object Label1: TLabel
          Left = 12
          Top = 11
          Width = 29
          Height = 13
          AutoSize = False
          Caption = #1041#1088#1086#1112
          Font.Charset = RUSSIAN_CHARSET
          Font.Color = clRed
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object Label3: TLabel
          Left = 102
          Top = 11
          Width = 58
          Height = 13
          AutoSize = False
          Caption = #1044#1086#1075#1086#1074#1086#1088
          Font.Charset = RUSSIAN_CHARSET
          Font.Color = clRed
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object Label8: TLabel
          Left = 441
          Top = 11
          Width = 44
          Height = 13
          AutoSize = False
          Caption = #1044#1072#1090#1091#1084
          Font.Charset = RUSSIAN_CHARSET
          Font.Color = clRed
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object Label14: TLabel
          Left = 37
          Top = 53
          Width = 59
          Height = 30
          Alignment = taRightJustify
          AutoSize = False
          Caption = #1056#1077#1086#1085#1089#1082#1080' '#1082#1086#1085#1090#1088#1086#1083#1086#1088
          Font.Charset = RUSSIAN_CHARSET
          Font.Color = clRed
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
          WordWrap = True
        end
        object Sifra: TcxDBTextEdit
          Left = 12
          Top = 26
          BeepOnEnter = False
          DataBinding.DataField = 'BROJ'
          ParentFont = False
          Properties.BeepOnError = True
          Properties.CharCase = ecUpperCase
          TabOrder = 0
          OnEnter = cxDBTextEditAllEnter
          OnExit = cxDBTextEditAllExit
          OnKeyDown = EnterKakoTab
          Width = 84
        end
        object txtBrojDogovor: TcxDBTextEdit
          Left = 102
          Top = 26
          BeepOnEnter = False
          DataBinding.DataField = 'BROJ_DOG'
          ParentFont = False
          Properties.BeepOnError = True
          Properties.CharCase = ecUpperCase
          TabOrder = 1
          OnEnter = cxDBTextEditAllEnter
          OnExit = cxDBTextEditAllExit
          OnKeyDown = EnterKakoTab
          Width = 82
        end
        object Datum: TcxDBDateEdit
          Left = 441
          Top = 26
          DataBinding.DataField = 'DATUM'
          ParentFont = False
          TabOrder = 2
          OnEnter = cxDBTextEditAllEnter
          OnExit = cxDBTextEditAllExit
          OnKeyDown = EnterKakoTab
          Width = 129
        end
        object cxButton2: TcxButton
          Left = 685
          Top = 74
          Width = 75
          Height = 25
          Action = aOtkazi
          Anchors = [akRight, akBottom]
          TabOrder = 7
        end
        object txtTPTeh: TcxDBTextEdit
          Tag = 1
          Left = 102
          Top = 53
          DataBinding.DataField = 'R_TIP_PARTNER'
          ParentFont = False
          TabOrder = 3
          OnEnter = cxDBTextEditAllEnter
          OnExit = cxDBTextEditAllExit
          OnKeyDown = EnterKakoTab
          Width = 59
        end
        object txtTeh: TcxDBTextEdit
          Tag = 1
          Left = 160
          Top = 53
          DataBinding.DataField = 'R_PARTNER'
          ParentFont = False
          TabOrder = 4
          OnEnter = cxDBTextEditAllEnter
          OnExit = cxDBTextEditAllExit
          OnKeyDown = EnterKakoTab
          Width = 61
        end
        object cbTeh: TcxLookupComboBox
          Left = 220
          Top = 53
          Anchors = [akLeft, akTop, akRight, akBottom]
          Properties.DropDownSizeable = True
          Properties.KeyFieldNames = 'TIP_PARTNER;id'
          Properties.ListColumns = <
            item
              FieldName = 'tip_partner'
            end
            item
              FieldName = 'ID'
            end
            item
              FieldName = 'naziv'
            end>
          Properties.ListFieldIndex = 2
          Properties.ListOptions.SyncMode = True
          TabOrder = 5
          OnEnter = cxDBTextEditAllEnter
          OnExit = cxDBTextEditAllExit
          OnKeyDown = EnterKakoTab
          Width = 352
        end
        object ZapisiButton: TcxButton
          Left = 604
          Top = 74
          Width = 75
          Height = 25
          Action = aZapisi
          Anchors = [akRight, akBottom]
          TabOrder = 6
        end
        object txtNazivKoop: TcxDBTextEdit
          Left = 183
          Top = 26
          DataBinding.DataField = 'NAZIV_KOOPERANT'
          Enabled = False
          StyleDisabled.BorderColor = clWindowText
          StyleDisabled.Color = clWindow
          StyleDisabled.TextColor = clWindowText
          TabOrder = 8
          Width = 252
        end
      end
    end
    object tcStavki: TcxTabSheet
      Caption = #1057#1090#1072#1074#1082#1080
      ImageIndex = 1
      object Panel1: TPanel
        Left = 0
        Top = 0
        Width = 772
        Height = 107
        Align = alTop
        Enabled = False
        TabOrder = 0
        DesignSize = (
          772
          107)
        object Label4: TLabel
          Left = 20
          Top = 19
          Width = 58
          Height = 13
          AutoSize = False
          Caption = #1056#1077#1076#1077#1085' '#1073#1088'.'
          Font.Charset = RUSSIAN_CHARSET
          Font.Color = clRed
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object Label9: TLabel
          Left = 90
          Top = 19
          Width = 148
          Height = 13
          AutoSize = False
          Caption = #1052#1072#1090#1077#1088#1080#1112#1072#1083'/'#1059#1089#1083#1091#1075#1072
          Font.Charset = RUSSIAN_CHARSET
          Font.Color = clRed
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object Label10: TLabel
          Left = 563
          Top = 19
          Width = 58
          Height = 13
          AutoSize = False
          Caption = #1050#1086#1083#1080#1095#1080#1085#1072
          Font.Charset = RUSSIAN_CHARSET
          Font.Color = clRed
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = []
          ParentFont = False
        end
        object txtBroj: TcxDBTextEdit
          Tag = 1
          Left = 20
          Top = 34
          BeepOnEnter = False
          DataBinding.DataField = 'REDEN_BROJ'
          ParentFont = False
          Properties.BeepOnError = True
          Properties.CharCase = ecUpperCase
          TabOrder = 0
          OnEnter = cxDBTextEditAllEnter
          OnExit = cxDBTextEditAllExit
          OnKeyDown = EnterKakoTab
          Width = 61
        end
        object txtMaterijal: TcxDBTextEdit
          Tag = 1
          Left = 89
          Top = 34
          BeepOnEnter = False
          DataBinding.DataField = 'ID_USLUGA'
          ParentFont = False
          Properties.BeepOnError = True
          Properties.CharCase = ecUpperCase
          TabOrder = 1
          OnEnter = cxDBTextEditAllEnter
          OnExit = cxDBTextEditAllExit
          OnKeyDown = EnterKakoTab
          Width = 67
        end
        object cbMaterijal: TcxDBLookupComboBox
          Left = 156
          Top = 34
          DataBinding.DataField = 'ID_USLUGA'
          Properties.DropDownSizeable = True
          Properties.KeyFieldNames = 'ID'
          Properties.ListColumns = <
            item
              FieldName = 'id'
            end
            item
              FieldName = 'NAZIV'
            end
            item
              FieldName = 'CENA'
            end>
          Properties.ListFieldIndex = 1
          Properties.ListOptions.SyncMode = True
          TabOrder = 2
          OnEnter = cxDBTextEditAllEnter
          OnExit = cxDBTextEditAllExit
          OnKeyDown = EnterKakoTab
          Width = 394
        end
        object txtKolicina: TcxDBTextEdit
          Tag = 1
          Left = 563
          Top = 34
          BeepOnEnter = False
          DataBinding.DataField = 'KOLICINA'
          ParentFont = False
          Properties.BeepOnError = True
          Properties.CharCase = ecUpperCase
          TabOrder = 3
          OnEnter = cxDBTextEditAllEnter
          OnExit = cxDBTextEditAllExit
          OnKeyDown = EnterKakoTab
          Width = 75
        end
        object ZapisiSButton: TcxButton
          Left = 581
          Top = 76
          Width = 75
          Height = 25
          Action = aZapisi
          Anchors = [akRight, akBottom]
          TabOrder = 4
        end
        object cxButton3: TcxButton
          Left = 662
          Top = 76
          Width = 75
          Height = 25
          Action = aOtkazi
          Anchors = [akRight, akBottom]
          TabOrder = 5
        end
      end
      object Panel2: TPanel
        Left = 0
        Top = 107
        Width = 772
        Height = 377
        Align = alClient
        TabOrder = 1
        object cxGrid2: TcxGrid
          Left = 1
          Top = 1
          Width = 770
          Height = 375
          Align = alClient
          TabOrder = 0
          object cxGrid2DBTableView1: TcxGridDBTableView
            OnFocusedRecordChanged = cxGrid2DBTableView1FocusedRecordChanged
            DataController.DataSource = dm.dsInternoBaranjeStavka
            DataController.Summary.DefaultGroupSummaryItems = <>
            DataController.Summary.FooterSummaryItems = <
              item
                Format = '#,##0.00'
                Kind = skSum
                FieldName = 'IZNOS'
              end>
            DataController.Summary.SummaryGroups = <>
            OptionsBehavior.IncSearch = True
            OptionsData.Deleting = False
            OptionsData.Editing = False
            OptionsData.Inserting = False
            OptionsView.Footer = True
            object cxGrid2DBTableView1RE: TcxGridDBColumn
              Caption = #1064#1092#1088#1072' '#1085#1072' '#1083#1086#1082#1072#1094#1080#1112#1072
              DataBinding.FieldName = 'RE'
            end
            object cxGrid2DBTableView1OD_KADE: TcxGridDBColumn
              Caption = #1051#1086#1082#1072#1094#1080#1112#1072
              DataBinding.FieldName = 'OD_KADE'
            end
            object cxGrid2DBTableView1BROJ: TcxGridDBColumn
              Caption = #1041#1088#1086#1112
              DataBinding.FieldName = 'BROJ'
            end
            object cxGrid2DBTableView1RBR: TcxGridDBColumn
              Caption = #1056#1077#1076#1077#1085' '#1073#1088#1086#1112
              DataBinding.FieldName = 'RBR'
            end
            object cxGrid2DBTableView1ARTVID: TcxGridDBColumn
              Caption = #1042#1080#1076' '#1085#1072' '#1072#1088#1090#1080#1072#1083
              DataBinding.FieldName = 'ARTVID'
            end
            object cxGrid2DBTableView1ARTSIF: TcxGridDBColumn
              Caption = #1064#1080#1092#1088#1072' '#1085#1072' '#1072#1088#1090#1080#1082#1072#1083
              DataBinding.FieldName = 'ARTSIF'
            end
            object cxGrid2DBTableView1NAZIV: TcxGridDBColumn
              Caption = #1040#1088#1090#1080#1082#1072#1083
              DataBinding.FieldName = 'NAZIV'
            end
            object cxGrid2DBTableView1KOLICINA: TcxGridDBColumn
              Caption = #1050#1086#1083#1080#1095#1080#1085#1072
              DataBinding.FieldName = 'KOLICINA'
            end
          end
          object cxGrid2Level1: TcxGridLevel
            GridView = cxGrid2DBTableView1
          end
        end
      end
    end
  end
  object cxGridPopupMenu1: TcxGridPopupMenu
    Grid = cxGrid1
    PopupMenus = <>
    Left = 704
    Top = 48
  end
  object PopupMenu1: TPopupMenu
    Left = 640
    Top = 48
    object N1: TMenuItem
      Caption = #1057#1085#1080#1084#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      ShortCut = 49235
      OnClick = SaveToIniFileExecute
    end
    object Excel1: TMenuItem
      Caption = #1057#1085#1080#1084#1080' '#1075#1086' '#1074#1086' Excel'
      ShortCut = 49221
    end
  end
  object dxBarManager1: TdxBarManager
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Segoe UI'
    Font.Style = []
    Categories.Strings = (
      'Default')
    Categories.ItemsVisibles = (
      2)
    Categories.Visibles = (
      True)
    ImageOptions.LargeImages = dmRes.cxLargeImages
    PopupMenuLinks = <>
    UseSystemFont = True
    Left = 752
    Top = 48
    DockControlHeights = (
      0
      0
      0
      0)
    object dxBarManager1Bar1: TdxBar
      Caption = 'Custom 1'
      CaptionButtons = <>
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 715
      FloatTop = 157
      FloatClientWidth = 51
      FloatClientHeight = 120
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton1'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton2'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton3'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar2: TdxBar
      Caption = #1040#1082#1094#1080#1080
      CaptionButtons = <>
      DockedLeft = 231
      DockedTop = 0
      FloatLeft = 789
      FloatTop = 120
      FloatClientWidth = 51
      FloatClientHeight = 120
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton4'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton5'
        end
        item
          Visible = True
          ItemName = 'dxBarSubItem1'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar3: TdxBar
      Caption = #1048#1079#1083#1077#1079
      CaptionButtons = <>
      DockedLeft = 438
      DockedTop = 0
      FloatLeft = 718
      FloatTop = 62
      FloatClientWidth = 51
      FloatClientHeight = 80
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton7'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton8'
        end>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar4: TdxBar
      Caption = #1055#1077#1095#1072#1090#1077#1114#1077
      CaptionButtons = <>
      DockedLeft = 0
      DockedTop = 0
      FloatLeft = 676
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton12'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton15'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton9'
        end
        item
          Visible = True
          ItemName = 'dxBarLargeButton13'
        end>
      OneOnRow = True
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarManager1Bar6: TdxBar
      Caption = #1047#1072#1096#1090#1080#1090#1072
      CaptionButtons = <>
      DockedLeft = 175
      DockedTop = 0
      FloatLeft = 806
      FloatTop = 8
      FloatClientWidth = 0
      FloatClientHeight = 0
      ItemLinks = <>
      OneOnRow = False
      Row = 0
      UseOwnFont = False
      Visible = True
      WholeRow = False
    end
    object dxBarLargeButton2: TdxBarLargeButton
      Action = aAzuriraj
      Category = 0
    end
    object dxBarLargeButton3: TdxBarLargeButton
      Action = aBrisi
      Category = 0
    end
    object dxBarLargeButton4: TdxBarLargeButton
      Action = aSnimiIzgled
      Category = 0
      ScreenTip = tipSnimiIzgled
    end
    object dxBarLargeButton5: TdxBarLargeButton
      Action = aZacuvajExcel
      Category = 0
      ScreenTip = tipZacuvajExcel
    end
    object dxBarLargeButton6: TdxBarLargeButton
      Action = aPecatiTabela
      Caption = #1055#1077#1095#1072#1090#1080' '#1090#1072#1073#1077#1083#1072
      Category = 0
    end
    object dxBarLargeButton7: TdxBarLargeButton
      Action = aHelp
      Category = 0
    end
    object dxBarLargeButton8: TdxBarLargeButton
      Action = aIzlez
      Category = 0
    end
    object dxBarLargeButton1: TdxBarLargeButton
      Action = aNov
      Category = 0
    end
    object dxBarLargeButton9: TdxBarLargeButton
      Action = aSnimiPecatenje
      Category = 0
      ScreenTip = tipSnimiKonf
    end
    object dxBarLargeButton11: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object ribbonFilterReset: TdxBarLargeButton
      Caption = #1056#1077#1089#1077#1090#1080#1088#1072#1112' '#1092#1080#1083#1090#1077#1088
      Category = 0
      Hint = #1056#1077#1089#1077#1090#1080#1088#1072#1112' '#1092#1080#1083#1090#1077#1088
      Visible = ivAlways
    end
    object dxBarLargeButton12: TdxBarLargeButton
      Action = aPodesuvanjePecatenje
      Category = 0
      ScreenTip = tipDizajnReport
    end
    object dxBarLargeButton13: TdxBarLargeButton
      Action = aBrisiPodesuvanjePecatenje
      Category = 0
      ScreenTip = tipBrisiKonf
    end
    object dxBarLargeButton14: TdxBarLargeButton
      Caption = #1048#1079#1073#1088#1080#1096#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      Category = 0
      Visible = ivAlways
      LargeImageIndex = 40
    end
    object dxBarLargeButton15: TdxBarLargeButton
      Action = aPageSetup
      Category = 0
      ScreenTip = tipSetiranjeStrana
    end
    object dxBarLargeButton16: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object dxBarLargeButton17: TdxBarLargeButton
      Caption = 'New Button'
      Category = 0
      Hint = 'New Button'
      Visible = ivAlways
    end
    object cxBarEditItem1: TcxBarEditItem
      Caption = 'New Item'
      Category = 0
      Hint = 'New Item'
      Visible = ivAlways
      PropertiesClassName = 'TcxCheckBoxProperties'
    end
    object dxBarSubItem1: TdxBarSubItem
      Caption = #1055#1077#1095#1072#1090#1077#1114#1077
      Category = 0
      Visible = ivAlways
      ImageIndex = 30
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxBarLargeButton6'
        end>
    end
    object bbZastitaZadrski: TdxBarButton
      Caption = #1047#1072#1096#1090#1080#1090#1080' '#1079#1072#1076#1088#1096#1082#1080
      Category = 0
      Hint = 
        #1057#1086' '#1087#1088#1086#1084#1077#1085#1072' '#1085#1072' '#1089#1090#1072#1090#1091#1089#1086#1090', '#1089#1080#1090#1077' '#1079#1072#1076#1088#1096#1082#1080' '#1089#1077' '#1079#1072#1096#1090#1080#1090#1077#1085#1080' '#1086#1076' '#1073#1080#1083#1086' '#1082#1072#1082#1074#1080' ' +
        #1087#1088#1086#1084#1077#1085#1080'!'
      Visible = ivAlways
      ImageIndex = 47
      OnClick = aStatusExecute
    end
    object bbVratiZastita: TdxBarButton
      Caption = #1042#1088#1072#1090#1080' '#1079#1072#1096#1090#1080#1090#1077#1085#1080' '#1079#1072#1076#1088#1096#1082#1080
      Category = 0
      Hint = #1057#1086' '#1087#1088#1086#1084#1077#1085#1072' '#1085#1072' '#1089#1090#1072#1090#1091#1089#1086#1090', '#1089#1080#1090#1077' '#1079#1072#1076#1088#1096#1082#1080' '#1089#1077' '#1084#1086#1078#1077' '#1076#1072' '#1089#1077' '#1084#1077#1085#1091#1074#1072#1072#1090'!'
      Visible = ivAlways
      ImageIndex = 48
      OnClick = aVratiStatusExecute
    end
    object bbZakluci: TdxBarButton
      Caption = #1047#1072#1082#1083#1091#1095#1080' '#1079#1072#1076#1088#1096#1082#1080
      Category = 0
      Hint = #1057#1086' '#1079#1072#1082#1083#1091#1095#1091#1074#1072#1114#1077' '#1085#1072' '#1079#1072#1076#1088#1096#1082#1080#1090#1077' '#1077' '#1079#1072#1073#1088#1072#1085#1077#1090#1072' '#1089#1077#1082#1086#1112#1072' '#1087#1088#1086#1084#1077#1085#1072'!'
      Visible = ivAlways
      ImageIndex = 118
      OnClick = aZakluciExecute
    end
  end
  object ActionList1: TActionList
    Left = 664
    Top = 48
    object aNov: TAction
      Caption = #1053#1086#1074
      ImageIndex = 10
      ShortCut = 116
      OnExecute = aNovExecute
    end
    object aAzuriraj: TAction
      Caption = #1040#1078#1091#1088#1080#1088#1072#1112
      ImageIndex = 12
      ShortCut = 117
      OnExecute = aAzurirajExecute
    end
    object aBrisi: TAction
      Caption = #1041#1088#1080#1096#1080
      ImageIndex = 11
      ShortCut = 119
      OnExecute = aBrisiExecute
    end
    object aSnimiIzgled: TAction
      Caption = #1057#1085#1080#1084#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      ImageIndex = 9
      ShortCut = 24659
      OnExecute = aSnimiIzgledExecute
    end
    object aZacuvajExcel: TAction
      Caption = #1047#1072#1095#1091#1074#1072#1112' '#1074#1086' Excel'
      ImageIndex = 9
      ShortCut = 24645
      OnExecute = aZacuvajExcelExecute
    end
    object aPecatiTabela: TAction
      Caption = #1055#1077#1095#1072#1090#1080
      ImageIndex = 30
      OnExecute = aPecatiTabelaExecute
    end
    object aHelp: TAction
      Caption = #1055#1086#1084#1086#1096
      ImageIndex = 24
    end
    object aIzlez: TAction
      Caption = #1048#1079#1083#1077#1079
      ImageIndex = 25
      OnExecute = aIzlezExecute
    end
    object aZapisi: TAction
      Caption = #1047#1072#1087#1080#1096#1080
      ImageIndex = 7
      ShortCut = 120
      OnExecute = aZapisiExecute
    end
    object aOtkazi: TAction
      Caption = #1054#1090#1082#1072#1078#1080
      ImageIndex = 21
      ShortCut = 27
      OnExecute = aOtkaziExecute
    end
    object aRefresh: TAction
      Caption = 'aRefresh'
      ImageIndex = 18
      ShortCut = 118
      OnExecute = aRefreshExecute
    end
    object aSnimiPecatenje: TAction
      Caption = #1057#1085#1080#1084#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      ImageIndex = 39
      OnExecute = aSnimiPecatenjeExecute
    end
    object aPodesuvanjePecatenje: TAction
      Caption = #1044#1080#1079#1072#1112#1085' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090
      ImageIndex = 38
      OnExecute = aPodesuvanjePecatenjeExecute
    end
    object aBrisiPodesuvanjePecatenje: TAction
      Caption = #1041#1088#1080#1096#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      ImageIndex = 37
      OnExecute = aBrisiPodesuvanjePecatenjeExecute
    end
    object aPageSetup: TAction
      Caption = #1057#1077#1090#1080#1088#1072#1114#1077' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072
      ImageIndex = 38
      OnExecute = aPageSetupExecute
    end
    object aFormConfig: TAction
      Caption = 'aFormConfig'
      ShortCut = 24699
      OnExecute = aFormConfigExecute
    end
  end
  object dxComponentPrinter1: TdxComponentPrinter
    CurrentLink = dxComponentPrinter1Link1
    Version = 0
    Left = 544
    Top = 96
    object dxComponentPrinter1Link1: TdxGridReportLink
      Component = cxGrid1
      PrinterPage.DMPaper = 9
      PrinterPage.Footer = 6350
      PrinterPage.GrayShading = True
      PrinterPage.Header = 6350
      PrinterPage.Margins.Bottom = 12700
      PrinterPage.Margins.Left = 12700
      PrinterPage.Margins.Right = 12700
      PrinterPage.Margins.Top = 12700
      PrinterPage.PageFooter.RightTitle.Strings = (
        #1057#1090#1088'.[Page #] '#1086#1076' [Total Pages]')
      PrinterPage.PageSize.X = 210000
      PrinterPage.PageSize.Y = 297000
      PrinterPage._dxMeasurementUnits_ = 0
      PrinterPage._dxLastMU_ = 2
      ReportDocument.CreationDate = 40336.425359629630000000
      OptionsFormatting.UseNativeStyles = True
      OptionsOnEveryPage.Caption = False
      OptionsOnEveryPage.FilterBar = False
      OptionsView.Caption = False
      OptionsView.FilterBar = False
      StyleRepository = dmRes.cxStyleRepositoryPrinting
      Styles.StyleSheet = dmRes.dxGridReportLinkStyleSheet
      BuiltInReportLink = True
    end
  end
  object dxBarScreenTipRepository1: TdxBarScreenTipRepository
    Left = 544
    Top = 48
    object tipSnimiIzgled: TdxBarScreenTip
      Header.Text = #1057#1085#1080#1084#1080' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090
      Description.Text = #1047#1072#1095#1091#1074#1072#1112' '#1075#1086' '#1080#1079#1075#1083#1077#1076#1086#1090' '#1085#1072' '#1092#1086#1088#1084#1072#1090#1072' '#1080' '#1085#1072' '#1090#1072#1073#1077#1083#1072#1090#1072
    end
    object tipZacuvajExcel: TdxBarScreenTip
      Header.Text = #1047#1072#1095#1091#1074#1072#1112' '#1074#1086' Excel'
      Description.Text = #1057#1085#1080#1084#1080' '#1075#1080' '#1087#1086#1076#1072#1090#1086#1094#1080#1090#1077' '#1086#1076' '#1090#1072#1073#1077#1083#1072#1090#1072' '#1074#1086' Excel '#1092#1086#1088#1084#1072#1090
    end
    object tipDizajnReport: TdxBarScreenTip
      Header.Text = #1044#1080#1079#1072#1112#1085' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090
      Description.Text = #1055#1086#1076#1077#1089#1091#1074#1072#1114#1072' '#1085#1072' '#1080#1079#1075#1083#1077#1076#1086#1090' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090' '#1085#1072' '#1090#1072#1073#1077#1083#1072#1090#1072
    end
    object tipSetiranjeStrana: TdxBarScreenTip
      Header.Text = #1057#1077#1090#1080#1088#1072#1114#1077' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072
      Description.Text = #1055#1086#1076#1077#1089#1091#1074#1072#1114#1077' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072' '#1085#1072' '#1082#1086#1112#1072' '#1116#1077' '#1089#1077' '#1087#1077#1095#1072#1090#1080' '#1090#1072#1073#1077#1083#1072#1090#1072
    end
    object tipSnimiKonf: TdxBarScreenTip
      Header.Text = #1057#1085#1080#1084#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      Description.Text = #1047#1072#1095#1091#1074#1072#1112' '#1075#1080' '#1087#1086#1076#1077#1089#1091#1074#1072#1114#1072#1090#1072' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090' '#1080' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072' '#1079#1072' '#1087#1077#1095#1072#1090#1077#1114#1077
    end
    object tipBrisiKonf: TdxBarScreenTip
      Header.Text = #1041#1088#1080#1096#1080' '#1082#1086#1085#1092#1080#1075#1091#1088#1072#1094#1080#1112#1072
      Description.Text = #1048#1079#1073#1088#1080#1096#1080' '#1075#1080' '#1087#1086#1076#1077#1089#1091#1074#1072#1114#1072#1090#1072' '#1085#1072' '#1088#1077#1087#1086#1088#1090#1086#1090' '#1080' '#1085#1072' '#1089#1090#1088#1072#1085#1072#1090#1072' '#1079#1072' '#1087#1077#1095#1072#1090#1077#1114#1077
    end
  end
  object cxGridPopupMenu2: TcxGridPopupMenu
    Grid = cxGrid2
    PopupMenus = <>
    Left = 656
  end
end
