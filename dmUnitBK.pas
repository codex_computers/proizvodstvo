unit dmUnitBK;

interface

uses
  System.SysUtils, System.Classes, Data.DB, FIBDataSet, pFIBDataSet, system.Math, Dialogs, DaNe, Controls,
  FIBQuery, pFIBQuery, pFIBStoredProc;

type
  TdmBK = class(TDataModule)
    dsEtiketa: TDataSource;
    tblEtiketa: TpFIBDataSet;
    dsEtiketaStavka: TDataSource;
    tblEtiketaStavka: TpFIBDataSet;
    tblEtiketaID: TFIBIntegerField;
    tblEtiketaID_RABOTEN_NALOG: TFIBBCDField;
    tblEtiketaBR_RN: TFIBStringField;
    tblEtiketaDATUM_RN: TFIBDateTimeField;
    tblEtiketaGODINA: TFIBIntegerField;
    tblEtiketaDATUM: TFIBDateTimeField;
    tblEtiketaTIP_ETIKETA: TFIBIntegerField;
    tblIzberiArtikal: TpFIBDataSet;
    dsIzberiArtikal: TDataSource;
    tblIzberiArtikalID_RABOTEN_NALOG: TFIBBCDField;
    tblIzberiArtikalSERISKI_BROJ: TFIBStringField;
    tblIzberiArtikalZABELESKA: TFIBStringField;
    tblIzberiArtikalVID_AMBALAZA: TFIBIntegerField;
    tblIzberiArtikalAMBALAZA: TFIBIntegerField;
    tblIzberiArtikalNAZIV_AMBALAZA: TFIBStringField;
    tblEtiketaStavkaID: TFIBIntegerField;
    tblEtiketaStavkaID_ETIKETA: TFIBIntegerField;
    tblEtiketaStavkaVID_ARTIKAL: TFIBIntegerField;
    tblEtiketaStavkaARTIKAL: TFIBIntegerField;
    tblEtiketaStavkaGTIN: TFIBStringField;
    tblEtiketaStavkaSSCC: TFIBStringField;
    tblEtiketaStavkaSERISKI_BROJ: TFIBIntegerField;
    tblEtiketaStavkaBARKOD: TFIBStringField;
    qSetupK: TpFIBQuery;
    tblIzberiArtikalBARKOD: TFIBStringField;
    tblIzberiArtikalID_PAKUVANJE: TFIBIntegerField;
    tblIzberiArtikalARTVID: TFIBIntegerField;
    tblIzberiArtikalID: TFIBIntegerField;
    MaxEtiketaStavka: TpFIBStoredProc;
    tblIzberiArtikalDATUM_PAK: TFIBDateTimeField;
    tblIzberiArtikalROK_UPOTREBA: TFIBIntegerField;
    tblIzberiArtikalDATUM_DO: TFIBDateField;
    dsAmbalaza: TDataSource;
    tblAmbalaza: TpFIBDataSet;
    tblAmbalazaARTVID: TFIBIntegerField;
    tblAmbalazaID: TFIBIntegerField;
    tblAmbalazaNAZIV: TFIBStringField;
    tblAmbalazaMERKA: TFIBStringField;
    tblIzberiArtikalNAZIV: TFIBStringField;
    tblIzvedenaEM: TpFIBDataSet;
    tblIzvedenaEMIZVEDENA_EM: TFIBStringField;
    tblIzvedenaEMNAZIV: TFIBStringField;
    tblIzvedenaEMKOEFICIENT: TFIBBCDField;
    tblIzvedenaEMID: TFIBIntegerField;
    dsIzvedenaEM: TDataSource;
    tblIzberiArtikalLOT: TFIBStringField;
    tblIzberiRN: TpFIBDataSet;
    tblIzberiRNSTATUS: TFIBIntegerField;
    tblIzberiRNNAZIV_PARTNER_PORACKA: TFIBStringField;
    tblIzberiRNZABELESKA: TFIBStringField;
    tblIzberiRNTP_PORACKA: TFIBIntegerField;
    tblIzberiRNP_PORACKA: TFIBIntegerField;
    tblIzberiRNID: TFIBBCDField;
    tblIzberiRNBROJ: TFIBStringField;
    tblIzberiRNDATUM: TFIBDateTimeField;
    dsIzberiRN: TDataSource;
    tblEtiketaStavkaLOT: TFIBStringField;
    tblIzberiArtikalKOLICINA_VO_PAKUVANJE: TFIBFloatField;
    tblIzberiArtikalKOLICINA_BROJ: TFIBIntegerField;
    tblIzberiArtikalKOLICINA: TFIBFloatField;
    tblIzberiArtikalKOLICINA_REALNA: TFIBFloatField;
    tblIzberiArtikalMERKA: TFIBStringField;
    tblEtiketaStavkaKOLICINA: TFIBFloatField;
    tblEtiketaStavkaSERISKI_BROJ1: TFIBStringField;
    tblEtiketaStavkaZABELESKA: TFIBStringField;
    tblEtiketaStavkaKOLICINA_REALNA: TFIBFloatField;
    tblEtiketaStavkaVID_AMBALAZA: TFIBIntegerField;
    tblEtiketaStavkaAMBALAZA: TFIBIntegerField;
    tblEtiketaStavkaNAZIV_AMBALAZA: TFIBStringField;
    tblEtiketaStavkaBARKOD1: TFIBStringField;
    tblEtiketaStavkaKOLICINA_VO_PAKUVANJE: TFIBFloatField;
    tblEtiketaStavkaID_PAKUVANJE: TFIBIntegerField;
    tblEtiketaStavkaDATUM_PAK: TFIBDateTimeField;
    tblEtiketaStavkaROK_UPOTREBA: TFIBIntegerField;
    tblEtiketaStavkaDATUM_DO: TFIBDateField;
    tblEtiketaStavkaLOT1: TFIBStringField;
    tblEtiketaStavkaKOLICINA_BROJ: TFIBIntegerField;
    tblEtiketaStavkaID_RNS: TFIBBCDField;
    tblIzberiArtikalID_RNS: TFIBBCDField;
    tblEtiketaStavkaNAZIV_ARTIKAL: TFIBStringField;
    tblIzberiRNSOURCE_DOKUMENT: TFIBBCDField;
    tblEtiketaSSCC: TFIBStringField;
    MaxEtiketa: TpFIBStoredProc;
    tblIzberiArtikalSIS_PALETA: TFIBStringField;
    tblIzberiArtikalPAK_PALETA: TFIBStringField;
    tblIzberiArtikalTEZ_PALETA: TFIBStringField;
    tblEtiketaStavkaBR_KUTIJA: TFIBIntegerField;
    qMax: TpFIBQuery;
    tblIzberiKamion: TpFIBDataSet;
    dsIzberiKamion: TDataSource;
    tblIzberiKamionID: TFIBIntegerField;
    tblIzberiKamionNAZIV: TFIBStringField;
    fbdtfldIzberiKamionDATUM: TFIBDateField;
    fbsmlntfldIzberiKamionSTATUS: TFIBSmallIntField;
    tblIzberiKamionTP_PORACKA: TFIBIntegerField;
    tblIzberiKamionP_PORACKA: TFIBIntegerField;
    tblIzberiKamionNAZIV_PARTNER_PORACKA: TFIBStringField;
    dsIzberiA: TDataSource;
    tblIzberiA: TpFIBDataSet;
    tblIzberiAID_TRAILER: TFIBIntegerField;
    tblIzberiAARTVID: TFIBIntegerField;
    tblIzberiAID: TFIBIntegerField;
    tblIzberiANAZIV: TFIBStringField;
    tblIzberiAMERKA: TFIBStringField;
    tblIzberiAKOLICINA: TFIBIntegerField;
    tblIzberiASERISKI_BROJ: TFIBStringField;
    tblIzberiAZABELESKA: TFIBStringField;
    tblIzberiAVID_AMBALAZA: TFIBIntegerField;
    tblIzberiAAMBALAZA: TFIBIntegerField;
    tblIzberiANAZIV_AMBALAZA: TFIBStringField;
    tblIzberiABARKOD: TFIBStringField;
    fbdtfldIzberiADATUM_PAK: TFIBDateField;
    tblIzberiAROK_UPOTREBA: TFIBIntegerField;
    fbdtfldIzberiADATUM_DO: TFIBDateField;
    tblIzberiALOT: TFIBStringField;
    tblIzberiAID_RNS: TFIBBCDField;
    tblIzberiASIS_PALETA: TFIBStringField;
    tblIzberiAPAK_PALETA: TFIBStringField;
    tblIzberiATEZ_PALETA: TFIBStringField;
    tblEtiketaStavkaK: TpFIBDataSet;
    FIBIntegerField1: TFIBIntegerField;
    FIBIntegerField2: TFIBIntegerField;
    FIBIntegerField3: TFIBIntegerField;
    FIBIntegerField4: TFIBIntegerField;
    FIBStringField1: TFIBStringField;
    FIBStringField2: TFIBStringField;
    FIBIntegerField5: TFIBIntegerField;
    FIBStringField3: TFIBStringField;
    FIBStringField4: TFIBStringField;
    p1: TFIBFloatField;
    FIBStringField5: TFIBStringField;
    FIBStringField6: TFIBStringField;
    p2: TFIBFloatField;
    FIBIntegerField6: TFIBIntegerField;
    FIBIntegerField7: TFIBIntegerField;
    FIBStringField7: TFIBStringField;
    FIBStringField8: TFIBStringField;
    p3: TFIBFloatField;
    FIBIntegerField8: TFIBIntegerField;
    fbdtmfld1: TFIBDateTimeField;
    FIBIntegerField9: TFIBIntegerField;
    fbdtfld1: TFIBDateField;
    FIBStringField9: TFIBStringField;
    FIBIntegerField10: TFIBIntegerField;
    FIBBCDField1: TFIBBCDField;
    FIBStringField10: TFIBStringField;
    FIBIntegerField11: TFIBIntegerField;
    tblEtiketaK: TpFIBDataSet;
    FIBIntegerField12: TFIBIntegerField;
    FIBBCDField2: TFIBBCDField;
    FIBStringField11: TFIBStringField;
    fbdtmfld2: TFIBDateTimeField;
    FIBIntegerField13: TFIBIntegerField;
    fbdtmfld3: TFIBDateTimeField;
    FIBIntegerField14: TFIBIntegerField;
    FIBStringField12: TFIBStringField;
    dsEtiketaK: TDataSource;
    dsEtiketaStavkaK: TDataSource;
    tblEtiketaStavkaKID_TRAILER: TFIBIntegerField;
    tblEtiketaStavkaKKOL_TRAILER: TFIBIntegerField;
    tblEtiketaKID_TRAILER: TFIBIntegerField;
    tblEtiketaStavkaKID_POR_S_T: TFIBIntegerField;
    tblIzberiAID_RN: TFIBBCDField;
    tblEtiketaStavkaIMENITEL: TFIBIntegerField;
    tblEtiketaStavkaID_SOS_DEL: TFIBIntegerField;
    procedure Kontrolen_broj(barkod : string);
    procedure AllTableBeforeDelete(DataSet: TDataSet);
    procedure IniFile;
    procedure tblIzberiRNBeforeOpen(DataSet: TDataSet);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  dmBK: TdmBK; dolzina_barkod,suma,kb,linija :integer;
  kompaniski_broj:string;
implementation

{%CLASSGROUP 'Vcl.Controls.TControl'}

uses dmKonekcija;

{$R *.dfm}

procedure TdmBK.Kontrolen_broj(barkod : string );
var i,delliv_deset:integer;
begin
  // neparnata pozicija se mnozi so 3, a parnata so 1
     suma := 0;
  //������� �� ������
    dolzina_barkod := Length(barkod);
    if odd(dolzina_barkod) then
    begin
      //od prvata do poslednata cifra
      for I := 1 to dolzina_barkod do
      begin
        if odd(i) then
                suma := suma + StrToInt(Copy(barkod,i,1))*3
        else
                suma := suma + StrToInt(Copy(barkod,i,1))
      end;

    end
    else
    begin
      for I := 1 to dolzina_barkod do
      begin
        if odd(i) then
                suma := suma + StrToInt(Copy(barkod,i,1))
        else
                suma := suma + StrToInt(Copy(barkod,i,1))*3
      end;
    end;
     //Odzemawe na sumata od najbliskiot ednakov ili pogolem broj deliv so deset
     if StrToInt((Copy(IntToStr(suma),length(IntToStr(suma)),1))) = 0 then
         delliv_deset := suma  //strtoint(copy(inttostr(suma),1,length(inttostr(suma))-1)+'0' )
     else
         delliv_deset := StrToInt(Copy(IntToStr(suma),1,length(IntToStr(suma))-2)+IntToStr(StrToInt(Copy(IntToStr(suma),length(IntToStr(suma))-1,1))+1)+'0');

    if suma >= delliv_deset  then
       kb := suma-delliv_deset
    else
       kb := delliv_deset-suma;
end;

procedure TdmBK.tblIzberiRNBeforeOpen(DataSet: TDataSet);
begin
    tblIzberiRN.ParamByName('status').Value := 1;
  //  tblIzberiRN.ParamByName('linija').Value := linija
end;

procedure TdmBK.AllTableBeforeDelete(DataSet: TDataSet);
begin
   frmDaNe := TfrmDaNe.Create(self, '������ �� �����', '���� ��������� ������ �� �� ��������� �������?', 1);
    if (frmDaNe.ShowModal <> mrYes) then
        Abort;
end;

procedure TdmBK.IniFile;
 var
  F: TextFile;
  S: integer;
begin
    AssignFile(F, 'C:\cxCache\Linija.ini');
    Reset(F);
    Readln(F, S);
    linija := S;
    CloseFile(F);
end;

end.
