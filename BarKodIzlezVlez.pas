unit BarKodIzlezVlez;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, cxGraphics, cxControls, cxLookAndFeels,
  cxLookAndFeelPainters, cxContainer, cxEdit, dxSkinsCore, dxSkinBlack,
  dxSkinBlue, dxSkinBlueprint, dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom,
  dxSkinDarkSide, dxSkinDevExpressDarkStyle, dxSkinDevExpressStyle, dxSkinFoggy,
  dxSkinGlassOceans, dxSkinHighContrast, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMetropolis,
  dxSkinMetropolisDark, dxSkinMoneyTwins, dxSkinOffice2007Black,
  dxSkinOffice2007Blue, dxSkinOffice2007Green, dxSkinOffice2007Pink,
  dxSkinOffice2007Silver, dxSkinOffice2010Black, dxSkinOffice2010Blue,
  dxSkinOffice2010Silver, dxSkinOffice2013DarkGray, dxSkinOffice2013LightGray,
  dxSkinOffice2013White, dxSkinPumpkin, dxSkinSeven, dxSkinSevenClassic,
  dxSkinSharp, dxSkinSharpPlus, dxSkinSilver, dxSkinSpringTime, dxSkinStardust,
  dxSkinSummer2008, dxSkinTheAsphaltWorld, dxSkinsDefaultPainters,
  dxSkinValentine, dxSkinVS2010, dxSkinWhiteprint, dxSkinXmas2008Blue,
  cxGroupBox, Vcl.ExtCtrls, cxLabel, cxMaskEdit, cxDropDownEdit, cxLookupEdit,
  cxDBLookupEdit, cxDBLookupComboBox, cxTextEdit, cxDBEdit, Vcl.StdCtrls,
  Vcl.Menus, cxButtons, System.Actions, Vcl.ActnList;

type
  TfrmIzlezVlez = class(TForm)
    Panel1: TPanel;
    cxGroupBox1: TcxGroupBox;
    lbl3: TLabel;
    cxGroupBox2: TcxGroupBox;
    lbl4: TLabel;
    lbl5: TLabel;
    cxlbl2: TcxLabel;
    lbl1: TLabel;
    cxButton1: TcxButton;
    cxButton2: TcxButton;
    cbRE: TcxLookupComboBox;
    cbKT: TcxLookupComboBox;
    cbOperacija: TcxLookupComboBox;
    cbOdKT: TcxLookupComboBox;
    cbSlednaKT: TcxLookupComboBox;
    actnlst1: TActionList;
    aSkeniraj: TAction;
    procedure FormShow(Sender: TObject);
    procedure aSkenirajExecute(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmIzlezVlez: TfrmIzlezVlez;

implementation

{$R *.dfm}

uses dmKonekcija, dmMaticni, dmResources, dmSystem, dmUnit, IzlezStavki;

procedure TfrmIzlezVlez.aSkenirajExecute(Sender: TObject);
begin
  frmIzlezVlez := TfrmIzlezVlez.Create(self);
 // re_kt := cbRE.ed
  frmIzlezVlez.ShowModal;
  frmIzlezVlez.Free;
end;

procedure TfrmIzlezVlez.FormShow(Sender: TObject);
begin
  dm.tblKTOperacija.Close;
  dm.tblKTOperacija.ParamByName('id_kt').AsString := '%';
  dm.tblKTOperacija.Open;
  dm.tblKT.Close;
  dm.tblKT.ParamByName('id_re').AsString := '%';
  dm.tblKT.Open;

end;

end.
