inherited frmOperacija: TfrmOperacija
  Caption = #1054#1087#1077#1088#1072#1094#1080#1112#1072
  ClientHeight = 550
  ClientWidth = 672
  ExplicitWidth = 688
  ExplicitHeight = 589
  PixelsPerInch = 96
  TextHeight = 13
  inherited lPanel: TPanel
    Width = 672
    Height = 266
    ExplicitWidth = 672
    ExplicitHeight = 266
    inherited cxGrid1: TcxGrid
      Width = 668
      Height = 262
      ExplicitWidth = 668
      ExplicitHeight = 262
      inherited cxGrid1DBTableView1: TcxGridDBTableView
        DataController.DataSource = dm.dsOperacija
        object cxGrid1DBTableView1ID: TcxGridDBColumn
          Caption = #1064#1080#1092#1088#1072
          DataBinding.FieldName = 'ID'
          Width = 75
        end
        object cxGrid1DBTableView1NAZIV: TcxGridDBColumn
          DataBinding.FieldName = 'NAZIV'
          Width = 434
        end
        object cxGrid1DBTableView1AKTIVEN: TcxGridDBColumn
          Caption = #1040#1082#1090#1080#1074#1077#1085
          DataBinding.FieldName = 'AKTIVEN'
          Visible = False
        end
        object cxGrid1DBTableView1ID_MAGACIN: TcxGridDBColumn
          Caption = #1064#1080#1092#1088#1072' '#1085#1072' '#1084#1072#1075#1072#1094#1080#1085
          DataBinding.FieldName = 'ID_MAGACIN'
          Visible = False
          Width = 144
        end
        object cxGrid1DBTableView1NAZIV_MAGACIN: TcxGridDBColumn
          Caption = #1052#1072#1075#1072#1094#1080#1085
          DataBinding.FieldName = 'NAZIV_MAGACIN'
          Visible = False
          Width = 291
        end
      end
    end
  end
  inherited dPanel: TPanel
    Top = 392
    Width = 672
    Height = 135
    ExplicitTop = 392
    ExplicitWidth = 672
    ExplicitHeight = 135
    inherited Label1: TLabel
      Visible = False
    end
    object Label2: TLabel [1]
      Left = 30
      Top = 44
      Width = 50
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1053#1072#1079#1080#1074' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clRed
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
    end
    object Label3: TLabel [2]
      Left = 13
      Top = 69
      Width = 67
      Height = 13
      Alignment = taRightJustify
      AutoSize = False
      Caption = #1056#1045' :'
      Font.Charset = RUSSIAN_CHARSET
      Font.Color = clNavy
      Font.Height = -11
      Font.Name = 'Tahoma'
      Font.Style = [fsBold]
      ParentFont = False
      Visible = False
    end
    inherited Sifra: TcxDBTextEdit
      Tag = 0
      TabOrder = 4
      Visible = False
    end
    inherited OtkaziButton: TcxButton
      Left = 581
      Top = 95
      TabOrder = 5
      ExplicitLeft = 581
      ExplicitTop = 95
    end
    inherited ZapisiButton: TcxButton
      Left = 500
      Top = 95
      TabOrder = 3
      ExplicitLeft = 500
      ExplicitTop = 95
    end
    object txtNaziv: TcxDBTextEdit
      Tag = 1
      Left = 86
      Top = 41
      Anchors = [akLeft, akRight, akBottom]
      BeepOnEnter = False
      DataBinding.DataField = 'NAZIV'
      DataBinding.DataSource = dm.dsOperacija
      ParentFont = False
      Properties.BeepOnError = True
      Properties.CharCase = ecUpperCase
      Style.Shadow = False
      TabOrder = 0
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 411
    end
    object txtMagacin: TcxDBTextEdit
      Left = 86
      Top = 66
      BeepOnEnter = False
      DataBinding.DataField = 'ID_MAGACIN'
      DataBinding.DataSource = dm.dsOperacija
      ParentFont = False
      Properties.BeepOnError = True
      Properties.CharCase = ecUpperCase
      Style.Shadow = False
      TabOrder = 1
      Visible = False
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 60
    end
    object cbMagacin: TcxDBLookupComboBox
      Left = 145
      Top = 66
      Anchors = [akRight, akBottom]
      DataBinding.DataField = 'ID_MAGACIN'
      DataBinding.DataSource = dm.dsOperacija
      Properties.KeyFieldNames = 'ID'
      Properties.ListColumns = <
        item
          FieldName = 'ID'
        end
        item
          FieldName = 'naziv'
        end>
      Properties.ListFieldIndex = 1
      Properties.ListSource = dmMat.dsRE
      TabOrder = 2
      Visible = False
      OnEnter = cxDBTextEditAllEnter
      OnExit = cxDBTextEditAllExit
      OnKeyDown = EnterKakoTab
      Width = 314
    end
  end
  inherited dxRibbon1: TdxRibbon
    Width = 672
    ExplicitWidth = 672
    inherited dxRibbon1Tab1: TdxRibbonTab
      Index = 0
    end
    inherited dxRibbon1Tab2: TdxRibbonTab
      Index = 1
    end
  end
  inherited StatusBar1: TdxRibbonStatusBar
    Top = 527
    Width = 672
    ExplicitTop = 527
    ExplicitWidth = 672
  end
  inherited dxBarManager1: TdxBarManager
    Left = 464
    Top = 216
    inherited dxBarManager1Bar1: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 69
      FloatClientHeight = 167
    end
    inherited dxBarManager1Bar2: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 149
      FloatClientHeight = 189
    end
    inherited dxBarManager1Bar3: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 59
      FloatClientHeight = 108
    end
    inherited dxBarManager1Bar4: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 141
      FloatClientHeight = 221
    end
    inherited dxBarManager1BarTabela: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 174
      FloatClientHeight = 113
    end
  end
  inherited dxComponentPrinter1: TdxComponentPrinter
    inherited dxComponentPrinter1Link1: TdxGridReportLink
      ReportDocument.CreationDate = 40998.597937789350000000
      Styles.BandHeader = nil
      Styles.Caption = nil
      Styles.CardCaptionRow = nil
      Styles.CardRowCaption = nil
      Styles.Content = nil
      Styles.ContentEven = nil
      Styles.ContentOdd = nil
      Styles.FilterBar = nil
      Styles.Footer = nil
      Styles.Group = nil
      Styles.Header = nil
      Styles.Preview = nil
      Styles.Selection = nil
      BuiltInReportLink = True
    end
  end
end
