inherited frmGenPlanKam: TfrmGenPlanKam
  Caption = #1043#1077#1085#1077#1088#1072#1083#1077#1085' '#1087#1083#1072#1085' '#1085#1072' '#1050#1040#1052#1048#1054#1053#1048
  ClientHeight = 687
  ClientWidth = 837
  ExplicitWidth = 853
  ExplicitHeight = 726
  PixelsPerInch = 96
  TextHeight = 13
  inherited dxRibbon1: TdxRibbon
    Width = 837
    ExplicitWidth = 837
    inherited dxRibbon1Tab1: TdxRibbonTab
      Index = 0
    end
    inherited dxRibbon1Tab2: TdxRibbonTab
      Index = 1
    end
  end
  inherited StatusBar1: TdxRibbonStatusBar
    Top = 664
    Width = 837
    ExplicitTop = 664
    ExplicitWidth = 837
  end
  inherited lPanel: TPanel
    Width = 837
    Height = 263
    ExplicitWidth = 837
    ExplicitHeight = 263
    inherited cxGrid1: TcxGrid
      Width = 833
      Height = 259
      ExplicitWidth = 833
      ExplicitHeight = 259
      inherited cxGrid1DBTableView1: TcxGridDBTableView
        OnFocusedRecordChanged = cxGrid1DBTableView1FocusedRecordChanged
        DataController.DataSource = dm.dsGenPlanKam
        DataController.Options = [dcoAnsiSort, dcoCaseInsensitive, dcoAssignGroupingValues, dcoAssignMasterDetailKeys, dcoSaveExpanding, dcoGroupsAlwaysExpanded]
        object cxGrid1DBTableView1ID: TcxGridDBColumn
          Caption = #1064#1080#1092#1088#1072
          DataBinding.FieldName = 'ID'
        end
        object cxGrid1DBTableView1NAZIV: TcxGridDBColumn
          Caption = #1053#1072#1079#1080#1074
          DataBinding.FieldName = 'NAZIV'
          Width = 141
        end
        object cxGrid1DBTableView1DATUM: TcxGridDBColumn
          Caption = #1044#1072#1090#1091#1084' '#1085#1072' '#1090#1088#1072#1085#1089#1087#1086#1088#1090
          DataBinding.FieldName = 'DATUM'
          Width = 116
        end
        object cxGrid1DBTableView1MESEC: TcxGridDBColumn
          Caption = #1052#1077#1089#1077#1094
          DataBinding.FieldName = 'MESEC'
        end
        object cxGrid1DBTableView1GODINA: TcxGridDBColumn
          Caption = #1043#1086#1076#1080#1085#1072
          DataBinding.FieldName = 'GODINA'
          Visible = False
          GroupIndex = 0
        end
        object cxGrid1DBTableView1TS_INS: TcxGridDBColumn
          DataBinding.FieldName = 'TS_INS'
          Visible = False
        end
        object cxGrid1DBTableView1TS_UPD: TcxGridDBColumn
          DataBinding.FieldName = 'TS_UPD'
          Visible = False
        end
        object cxGrid1DBTableView1USR_INS: TcxGridDBColumn
          DataBinding.FieldName = 'USR_INS'
          Visible = False
        end
        object cxGrid1DBTableView1USR_UPD: TcxGridDBColumn
          DataBinding.FieldName = 'USR_UPD'
          Visible = False
        end
        object cxGrid1DBTableView1TIP_PARTNER: TcxGridDBColumn
          Caption = #1058#1080#1087' '#1085#1072' '#1087#1072#1088#1090#1085#1077#1088
          DataBinding.FieldName = 'TIP_PARTNER'
          Width = 26
        end
        object cxGrid1DBTableView1PARTNER: TcxGridDBColumn
          Caption = #1064#1080#1092#1088#1072' '#1085#1072' '#1087#1072#1088#1090#1085#1077#1088
          DataBinding.FieldName = 'PARTNER'
          Width = 40
        end
        object cxGrid1DBTableView1NAZIV_PARTNER: TcxGridDBColumn
          Caption = #1055#1072#1088#1090#1085#1077#1088
          DataBinding.FieldName = 'NAZIV_PARTNER'
          Width = 150
        end
        object cxGrid1DBTableView1ZABELESKA: TcxGridDBColumn
          Caption = #1047#1072#1073#1077#1083#1077#1096#1082#1072
          DataBinding.FieldName = 'ZABELESKA'
          Width = 100
        end
        object cxGrid1DBTableView1STATUS: TcxGridDBColumn
          Caption = #1057#1090#1072#1090#1091#1089
          DataBinding.FieldName = 'STATUS'
          PropertiesClassName = 'TcxImageComboBoxProperties'
          Properties.Items = <
            item
              Description = #1054#1090#1074#1086#1088#1077#1085
              ImageIndex = 0
              Value = 0
            end
            item
              Description = #1047#1072#1090#1074#1086#1088#1077#1085
              Value = 1
            end>
        end
      end
    end
  end
  inherited cxPageControl1: TcxPageControl
    Top = 389
    Width = 837
    Height = 275
    OnChange = nil
    ExplicitTop = 389
    ExplicitWidth = 837
    ExplicitHeight = 275
    ClientRectBottom = 275
    ClientRectRight = 837
    inherited Osnovno: TcxTabSheet
      ExplicitTop = 27
      ExplicitWidth = 837
      ExplicitHeight = 248
      inherited dPanel: TPanel
        Width = 837
        Height = 248
        ExplicitWidth = 837
        ExplicitHeight = 248
        inherited Label1: TLabel
          Left = 641
          Top = 106
          Visible = False
          ExplicitLeft = 641
          ExplicitTop = 106
        end
        object lbl1: TLabel [1]
          Left = 90
          Top = 72
          Width = 50
          Height = 13
          Alignment = taRightJustify
          AutoSize = False
          Caption = #1053#1072#1079#1080#1074' :'
          Font.Charset = RUSSIAN_CHARSET
          Font.Color = clRed
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
        end
        object lbl2: TLabel [2]
          Left = 2
          Top = 102
          Width = 138
          Height = 20
          Alignment = taRightJustify
          AutoSize = False
          Caption = #1044#1072#1090#1091#1084' '#1085#1072' '#1090#1088#1072#1085#1089#1087#1086#1088#1090' :'
          Font.Charset = RUSSIAN_CHARSET
          Font.Color = clRed
          Font.Height = -11
          Font.Name = 'Tahoma'
          Font.Style = [fsBold]
          ParentFont = False
          WordWrap = True
        end
        inherited OtkaziButton: TcxButton
          Left = 725
          Top = 208
          TabOrder = 8
          ExplicitLeft = 725
          ExplicitTop = 208
        end
        inherited Sifra: TcxDBTextEdit
          Tag = 0
          Left = 697
          Top = 103
          TabOrder = 9
          Visible = False
          ExplicitLeft = 697
          ExplicitTop = 103
        end
        inherited ZapisiButton: TcxButton
          Left = 644
          Top = 208
          TabOrder = 7
          ExplicitLeft = 644
          ExplicitTop = 208
        end
        object txtNaziv: TcxDBTextEdit
          Tag = 1
          Left = 149
          Top = 69
          BeepOnEnter = False
          DataBinding.DataField = 'NAZIV'
          DataBinding.DataSource = dm.dsGenPlanKam
          ParentFont = False
          Properties.BeepOnError = True
          Properties.CharCase = ecUpperCase
          Style.Font.Charset = DEFAULT_CHARSET
          Style.Font.Color = clWindowText
          Style.Font.Height = -11
          Style.Font.Name = 'Tahoma'
          Style.Font.Style = []
          Style.Shadow = False
          Style.IsFontAssigned = True
          TabOrder = 1
          OnEnter = cxDBTextEditAllEnter
          OnExit = cxDBTextEditAllExit
          OnKeyDown = EnterKakoTab
          Width = 400
        end
        object gbGodMes: TcxGroupBox
          Left = 1
          Top = 1
          Align = alTop
          Caption = #1047#1072' '#1052#1045#1057#1045#1062' '#1080' '#1043#1054#1044#1048#1053#1040
          Style.TextColor = clRed
          Style.TextStyle = [fsBold]
          TabOrder = 0
          Height = 56
          Width = 835
          object txtMesec: TcxTextEdit
            Tag = 1
            Left = 148
            Top = 22
            TabOrder = 0
            OnEnter = cxDBTextEditAllEnter
            OnExit = cxDBTextEditAllExit
            OnKeyDown = EnterKakoTab
            Width = 81
          end
          object txtGodina: TcxTextEdit
            Tag = 1
            Left = 246
            Top = 22
            TabOrder = 1
            OnEnter = cxDBTextEditAllEnter
            OnExit = cxDBTextEditAllExit
            OnKeyDown = EnterKakoTab
            Width = 95
          end
          object cxlbl1: TcxLabel
            Left = 234
            Top = 22
            AutoSize = False
            Caption = '/'
            Height = 21
            Width = 12
          end
        end
        object txtDatum: TcxDBDateEdit
          Tag = 1
          Left = 149
          Top = 99
          DataBinding.DataField = 'DATUM'
          DataBinding.DataSource = dm.dsGenPlanKam
          TabOrder = 2
          OnEnter = cxDBTextEditAllEnter
          OnExit = cxDBTextEditAllExit
          OnKeyDown = EnterKakoTab
          Width = 144
        end
        object cxlbl2: TcxLabel
          Left = 63
          Top = 130
          Caption = #1047#1072' '#1055#1072#1088#1090#1085#1077#1088' :'
          ParentFont = False
          Style.Font.Charset = RUSSIAN_CHARSET
          Style.Font.Color = clWindowText
          Style.Font.Height = -11
          Style.Font.Name = 'Tahoma'
          Style.Font.Style = []
          Style.TextColor = clRed
          Style.TextStyle = [fsBold]
          Style.IsFontAssigned = True
          Properties.Alignment.Horz = taRightJustify
          Properties.WordWrap = True
          Transparent = True
          Width = 77
          AnchorX = 140
        end
        object txtTipPartner: TcxDBTextEdit
          Tag = 1
          Left = 149
          Top = 129
          Hint = #1058#1080#1087' '#1085#1072' '#1055#1072#1088#1090#1085#1077#1088
          DataBinding.DataField = 'TIP_PARTNER'
          DataBinding.DataSource = dm.dsGenPlanKam
          ParentFont = False
          TabOrder = 3
          OnEnter = cxDBTextEditAllEnter
          OnExit = cxDBTextEditAllExit
          OnKeyDown = EnterKakoTab
          Width = 53
        end
        object txtPartner: TcxDBTextEdit
          Tag = 1
          Left = 202
          Top = 129
          Hint = #1064#1080#1092#1088#1072' '#1085#1072' '#1055#1072#1088#1090#1085#1077#1088
          DataBinding.DataField = 'PARTNER'
          DataBinding.DataSource = dm.dsGenPlanKam
          ParentFont = False
          TabOrder = 4
          OnEnter = cxDBTextEditAllEnter
          OnExit = cxDBTextEditAllExit
          OnKeyDown = EnterKakoTab
          Width = 53
        end
        object cbNazivPartner: TcxLookupComboBox
          Tag = 1
          Left = 255
          Top = 129
          Hint = #1053#1072#1079#1080#1074' '#1085#1072' '#1055#1072#1088#1090#1085#1077#1088
          Properties.ClearKey = 46
          Properties.DropDownAutoSize = True
          Properties.DropDownSizeable = True
          Properties.KeyFieldNames = 'tip_partner;id'
          Properties.ListColumns = <
            item
              Width = 40
              FieldName = 'TIP_PARTNER'
            end
            item
              Width = 60
              FieldName = 'id'
            end
            item
              Width = 170
              FieldName = 'naziv'
            end>
          Properties.ListFieldIndex = 2
          Properties.ListOptions.SyncMode = True
          Properties.ListSource = dmMat.dsPartner
          TabOrder = 5
          OnEnter = cxDBTextEditAllEnter
          OnExit = cxDBTextEditAllExit
          OnKeyDown = EnterKakoTab
          Width = 294
        end
        object cxlbl3: TcxLabel
          Left = 65
          Top = 160
          Caption = #1047#1072#1073#1077#1083#1077#1096#1082#1072' :'
          ParentFont = False
          Style.Font.Charset = RUSSIAN_CHARSET
          Style.Font.Color = clWindowText
          Style.Font.Height = -11
          Style.Font.Name = 'Tahoma'
          Style.Font.Style = []
          Style.TextColor = clNavy
          Style.TextStyle = [fsBold]
          Style.IsFontAssigned = True
          Properties.Alignment.Horz = taRightJustify
          Properties.WordWrap = True
          Transparent = True
          Width = 75
          AnchorX = 140
        end
        object txtZabeleska: TcxDBTextEdit
          Left = 149
          Top = 158
          DataBinding.DataField = 'ZABELESKA'
          DataBinding.DataSource = dm.dsGenPlanKam
          TabOrder = 6
          OnEnter = cxDBTextEditAllEnter
          OnExit = cxDBTextEditAllExit
          OnKeyDown = EnterKakoTab
          Width = 400
        end
      end
    end
    inherited Napredno: TcxTabSheet
      ExplicitTop = 27
      ExplicitWidth = 837
      ExplicitHeight = 248
      inherited dPanel2: TPanel
        Width = 837
        Height = 248
        ExplicitWidth = 837
        ExplicitHeight = 248
        inherited Otkazi2: TcxButton
          Top = 200
          ExplicitTop = 200
        end
        inherited Zapisi2: TcxButton
          Top = 200
          ExplicitTop = 200
        end
      end
    end
  end
  inherited cxGridPopupMenu1: TcxGridPopupMenu
    Left = 653
    Top = 16
  end
  inherited PopupMenu1: TPopupMenu
    Left = 793
    Top = 16
  end
  inherited dxBarManager1: TdxBarManager
    Left = 688
    Top = 16
    PixelsPerInch = 96
    inherited dxBarManager1Bar1: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 69
      FloatClientHeight = 162
    end
    inherited dxBarManager1Bar2: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 142
      FloatClientHeight = 243
      ItemLinks = <
        item
          Visible = True
          ItemName = 'dxbrlrgbtn1'
        end
        item
          Visible = True
          ItemName = 'dxbrlrgbtn2'
        end
        item
          Visible = True
          ItemName = 'dxbrlrgbtn3'
        end
        item
          BeginGroup = True
          Visible = True
          ItemName = 'dxBarLargeButton5'
        end
        item
          Visible = True
          ItemName = 'dxBarSubItem1'
        end>
    end
    inherited dxBarManager1Bar3: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      DockedLeft = 533
      FloatClientWidth = 59
      FloatClientHeight = 108
    end
    inherited dxBarManager1Bar4: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 141
      FloatClientHeight = 221
    end
    inherited dxBarManager1BarSnimiIzgled: TdxBar
      DockedDockControl = nil
      DockedDockingStyle = dsNone
      FloatClientWidth = 174
      FloatClientHeight = 113
    end
    object dxbrlrgbtn1: TdxBarLargeButton
      Action = aZatvoriTrailer
      Category = 0
    end
    object dxbrlrgbtn2: TdxBarLargeButton
      Action = aVratiStatusOtvoren
      Category = 0
      SyncImageIndex = False
      ImageIndex = 48
    end
    object dxbrlrgbtn3: TdxBarLargeButton
      Action = aSiteKamioni
      Category = 0
    end
  end
  inherited ActionList1: TActionList
    Left = 618
    Top = 16
    object aZatvoriTrailer: TAction
      Caption = #1047#1072#1090#1074#1086#1088#1080' '#1087#1083#1072#1085
      ImageIndex = 85
      OnExecute = aZatvoriTrailerExecute
    end
    object aVratiStatusOtvoren: TAction
      Caption = #1042#1088#1072#1090#1080' '#1089#1090#1072#1090#1091#1089' '#1054#1058#1042#1054#1056#1045#1053
      ImageIndex = 48
      OnExecute = aVratiStatusOtvorenExecute
    end
    object aSiteKamioni: TAction
      Caption = #1057#1080#1090#1077' '#1050#1072#1084#1080#1086#1085#1080
      ImageIndex = 26
      OnExecute = aSiteKamioniExecute
    end
  end
  inherited dxBarScreenTipRepository1: TdxBarScreenTipRepository
    Left = 723
    Top = 16
    PixelsPerInch = 96
  end
  inherited dxComponentPrinter1: TdxComponentPrinter
    Left = 758
    Top = 16
    inherited dxComponentPrinter1Link1: TdxGridReportLink
      Styles.BandHeader = nil
      Styles.Caption = nil
      Styles.CardCaptionRow = nil
      Styles.CardRowCaption = nil
      Styles.Content = nil
      Styles.ContentEven = nil
      Styles.ContentOdd = nil
      Styles.FilterBar = nil
      Styles.Footer = nil
      Styles.Group = nil
      Styles.Header = nil
      Styles.Preview = nil
      Styles.Selection = nil
      BuiltInReportLink = True
    end
  end
end
