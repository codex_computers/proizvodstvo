unit IzberiPoracki;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, cxStyles, cxCustomData, cxGraphics, cxFilter, cxData, cxEdit,
  DB, cxDBData, cxGridCustomPopupMenu, cxGridPopupMenu,
  ComCtrls, cxContainer, cxTextEdit, cxDBEdit, StdCtrls, cxGridLevel,
  cxClasses, cxControls, cxGridCustomView, cxGridCustomTableView,
  cxGridTableView, cxGridDBTableView, cxGrid, ExtCtrls, Menus, ActnList,
  cxGridExportLink, cxExport, cxLookAndFeelPainters, cxDataStorage,
  cxButtons, dxStatusBar, dxRibbonStatusBar, dxRibbon, dxBar, dxBarExtItems,
  dxPSGlbl, dxPSUtl, dxPSEngn, dxPrnPg, dxBkgnd, dxWrap, dxPrnDev,
  dxPSCompsProvider, dxPSFillPatterns, dxPSEdgePatterns, dxPSCore, dxPScxCommon,
  cxPC, cxMemo, cxLocalization, cxLookAndFeels,
  dxPSPDFExportCore, dxPSPDFExport, cxDrawTextUtils, dxPSPrVwStd, dxPSPrVwAdv,
  dxPSPrVwRibbon, dxPScxEditorProducers, dxPScxExtEditorProducers,
  dxPScxPageControlProducer, dxPgsDlg, cxCheckBox, cxBarEditItem, dxSkinsCore,
  dxSkinBlack, dxSkinBlue, dxSkinCaramel, dxSkinCoffee, dxSkinDarkRoom,
  dxSkinDarkSide, dxSkinFoggy, dxSkinGlassOceans, dxSkiniMaginary, dxSkinLilian,
  dxSkinLiquidSky, dxSkinLondonLiquidSky, dxSkinMcSkin, dxSkinMoneyTwins,
  dxSkinOffice2007Black, dxSkinOffice2007Blue, dxSkinOffice2007Green,
  dxSkinOffice2007Pink, dxSkinOffice2007Silver, dxSkinOffice2010Black,
  dxSkinOffice2010Blue, dxSkinOffice2010Silver, dxSkinPumpkin, dxSkinSeven,
  dxSkinSharp, dxSkinSilver, dxSkinSpringTime, dxSkinStardust, dxSkinSummer2008,
  dxSkinsDefaultPainters, dxSkinValentine, dxSkinXmas2008Blue,
  dxSkinsdxRibbonPainter, dxSkinsdxBarPainter, dxSkinscxPCPainter,
  dxBarSkinnedCustForm, cxDropDownEdit, cxCalendar, cxDBLookupComboBox,
  cxMaskEdit, cxLookupEdit, cxDBLookupEdit, cxDBExtLookupComboBox, cxGroupBox,
  cxRadioGroup, dxRibbonSkins, dxPScxGridLnk, dxPScxGridLayoutViewLnk,
  dxScreenTip, dxSkinBlueprint, dxSkinDevExpressDarkStyle,
  dxSkinDevExpressStyle, dxSkinHighContrast, dxSkinSevenClassic,
  dxSkinSharpPlus, dxSkinTheAsphaltWorld, dxSkinVS2010, dxSkinWhiteprint, dmResources,
  cxSplitter, dxSkinOffice2013White, cxNavigator, System.Actions,
  dxSkinMetropolis, dxSkinMetropolisDark, dxSkinOffice2013DarkGray,
  dxSkinOffice2013LightGray, dxRibbonCustomizationForm,
  dxSkinOffice2016Colorful, dxSkinOffice2016Dark, dxSkinVisualStudio2013Blue,
  dxSkinVisualStudio2013Dark, dxSkinVisualStudio2013Light;

type
//  niza = Array[1..5] of Variant;

  TfrmIzberiPoracki = class(TForm)
    cxGridPopupMenu1: TcxGridPopupMenu;
    PopupMenu1: TPopupMenu;
    N1: TMenuItem;
    Excel1: TMenuItem;
    dxRibbon1Tab1: TdxRibbonTab;
    dxRibbon1: TdxRibbon;
    StatusBar1: TdxRibbonStatusBar;
    dxBarManager1: TdxBarManager;
    dxBarManager1Bar1: TdxBar;
    dxBarManager1Bar2: TdxBar;
    dxBarManager1Bar3: TdxBar;
    dxBarLargeButton2: TdxBarLargeButton;
    dxBarLargeButton3: TdxBarLargeButton;
    dxBarLargeButton4: TdxBarLargeButton;
    dxBarLargeButton5: TdxBarLargeButton;
    dxBarLargeButton6: TdxBarLargeButton;
    dxBarLargeButton7: TdxBarLargeButton;
    dxBarLargeButton8: TdxBarLargeButton;
    dxBarLargeButton1: TdxBarLargeButton;
    ActionList1: TActionList;
    aNov: TAction;
    aAzuriraj: TAction;
    aBrisi: TAction;
    aSnimiIzgled: TAction;
    aZacuvajExcel: TAction;
    aPecatiTabela: TAction;
    aHelp: TAction;
    aIzlez: TAction;
    aZapisi: TAction;
    aOtkazi: TAction;
    aRefresh: TAction;
    dxRibbon1Tab2: TdxRibbonTab;
    dxBarManager1Bar4: TdxBar;
    dxBarLargeButton9: TdxBarLargeButton;
    aSnimiPecatenje: TAction;
    dxComponentPrinter1: TdxComponentPrinter;
    dxComponentPrinter1Link1: TdxGridReportLink;
    dxBarLargeButton11: TdxBarLargeButton;
    ribbonFilterReset: TdxBarLargeButton;
    aPodesuvanjePecatenje: TAction;
    dxBarLargeButton12: TdxBarLargeButton;
    aBrisiPodesuvanjePecatenje: TAction;
    dxBarLargeButton13: TdxBarLargeButton;
    dxBarLargeButton14: TdxBarLargeButton;
    aPageSetup: TAction;
    dxBarLargeButton15: TdxBarLargeButton;
    dxBarLargeButton16: TdxBarLargeButton;
    dxBarLargeButton17: TdxBarLargeButton;
    dxBarSubItem1: TdxBarSubItem;
    aFormConfig: TAction;
    dxBarScreenTipRepository1: TdxBarScreenTipRepository;
    tipSnimiIzgled: TdxBarScreenTip;
    tipZacuvajExcel: TdxBarScreenTip;
    tipDizajnReport: TdxBarScreenTip;
    tipSetiranjeStrana: TdxBarScreenTip;
    tipSnimiKonf: TdxBarScreenTip;
    tipBrisiKonf: TdxBarScreenTip;
    dxBarManager1BarIzgledGrid: TdxBar;
    dxBarLBtnSnimiIzgled: TdxBarLargeButton;
    dxBarLBtnBrisiIzgled: TdxBarLargeButton;
    aBrisiIzgled: TAction;
    aSpustiSoberi: TAction;
    dxBarLargeButton10: TdxBarLargeButton;
    lPanel: TPanel;
    cxGrid1DBTableView1: TcxGridDBTableView;
    cxGrid1Level1: TcxGridLevel;
    cxGrid1: TcxGrid;
    cxGrid1DBTableView1ID: TcxGridDBColumn;
    cxGrid1DBTableView1PORACKA: TcxGridDBColumn;
    cxGrid1DBTableView1DATUM: TcxGridDBColumn;
    cxGrid1DBTableView1DATUM_OD_ISPORAKA: TcxGridDBColumn;
    cxGrid1DBTableView1DATUM_DO_ISPORAKA: TcxGridDBColumn;
    cxGrid1DBTableView2: TcxGridDBTableView;
    cxGrid1DBTableView2DBColumn: TcxGridDBColumn;
    cxGrid1DBTableView2DBColumn1: TcxGridDBColumn;
    cxGrid1DBTableView2ID_PORACKA: TcxGridDBColumn;
    cxGrid1DBTableView2DBColumn2: TcxGridDBColumn;
    cxGrid1DBTableView2DBColumn3: TcxGridDBColumn;
    cxGrid1DBTableView2DBColumn4: TcxGridDBColumn;
    cxGrid1DBTableView2DBColumn5: TcxGridDBColumn;
    cxGrid1DBTableView2DBColumn6: TcxGridDBColumn;
    cxGrid1DBTableView2DBColumn7: TcxGridDBColumn;
    cxGrid1DBTableView2DBColumn8: TcxGridDBColumn;
    cxGrid1DBTableView2DBColumn9: TcxGridDBColumn;
    cxGrid1DBTableView2LABELS: TcxGridDBColumn;
    cxGrid1DBTableView2GLIDERS: TcxGridDBColumn;
    cxGrid1DBTableView2DBColumn10: TcxGridDBColumn;
    cxGrid1DBTableView2DBColumn11: TcxGridDBColumn;
    cxGrid1DBTableView2DBColumn12: TcxGridDBColumn;
    cxGrid1DBTableView2DBColumn13: TcxGridDBColumn;
    cxGrid1DBTableView2STATUS: TcxGridDBColumn;
    cxGrid1DBTableView1Izberi: TcxGridDBColumn;
    cxGrid1DBTableView2Column1: TcxGridDBColumn;
    Panel1: TPanel;
    dPanel: TPanel;
    cxSplitter1: TcxSplitter;
    cxGrid2DBTableView1: TcxGridDBTableView;
    cxGrid2Level1: TcxGridLevel;
    cxGrid2: TcxGrid;
    cxGrid2DBTableView1DBColumn: TcxGridDBColumn;
    cxGrid2DBTableView1DBColumn1: TcxGridDBColumn;
    cxGrid2DBTableView1DBColumn2: TcxGridDBColumn;
    cxGrid2DBTableView1DBColumn3: TcxGridDBColumn;
    cxGrid2DBTableView1DBColumn4: TcxGridDBColumn;
    cxGrid2DBTableView1DBColumn5: TcxGridDBColumn;
    cxGrid2DBTableView1DBColumn6: TcxGridDBColumn;
    cxGrid2DBTableView1DBColumn7: TcxGridDBColumn;
    cxGrid2DBTableView1DBColumn8: TcxGridDBColumn;
    cxGrid2DBTableView1LABELS: TcxGridDBColumn;
    cxGrid2DBTableView1GLIDERS: TcxGridDBColumn;
    cxGrid2DBTableView1DBColumn9: TcxGridDBColumn;
    cxGrid2DBTableView1SERISKI_BROJ: TcxGridDBColumn;
    cxGrid2DBTableView1DBColumn10: TcxGridDBColumn;
    cxGrid2DBTableView1DBColumn11: TcxGridDBColumn;
    cxGrid2DBTableView1DBColumn12: TcxGridDBColumn;
    cxGrid2DBTableView1STATUS: TcxGridDBColumn;
    cxGrid2DBTableView1DBColumn13: TcxGridDBColumn;
    cxGrid2DBTableView1ID_PORACKA: TcxGridDBColumn;
    cxGrid3DBTableView1: TcxGridDBTableView;
    cxGrid3Level1: TcxGridLevel;
    cxGrid3: TcxGrid;
    cxSplitter2: TcxSplitter;
    cxGrid3DBTableView1ID: TcxGridDBColumn;
    cxGrid3DBTableView1ID_PORACKA_STAVKA: TcxGridDBColumn;
    cxGrid3DBTableView1VID_SUROVINA: TcxGridDBColumn;
    cxGrid3DBTableView1SUROVINA: TcxGridDBColumn;
    cxGrid3DBTableView1NAZIV_ELEMENT: TcxGridDBColumn;
    cxGrid3DBTableView1MERKA: TcxGridDBColumn;
    cxGrid3DBTableView1NAZIV_MERKA: TcxGridDBColumn;
    cxGrid3DBTableView1KOLICINA: TcxGridDBColumn;
    cxGrid3DBTableView1ZABELESKA: TcxGridDBColumn;
    aKreirajRN: TAction;
    aSiteStavki: TAction;
    cbSite: TcxBarEditItem;
    aDodadiVoRN: TAction;
    dxBarLargeButton18: TdxBarLargeButton;
    cxGrid2DBTableView1IZBERI: TcxGridDBColumn;

    procedure cxDBTextEditAllEnter(Sender: TObject);
    procedure cxDBTextEditAllExit(Sender: TObject);
    procedure EnterKakoTab(Sender: TObject; var Key: Word;
      Shift: TShiftState);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure FormShow(Sender: TObject);
    //procedure PrvPosledenTab(panel:TPanel;var posledna,prva:TWinControl);
    procedure SaveToIniFileExecute(Sender: TObject);
    procedure cxGrid1DBTableView1KeyPress(Sender: TObject; var Key: Char);
    procedure aZapisiExecute(Sender: TObject);
    procedure aOtkaziExecute(Sender: TObject);
    procedure aNovExecute(Sender: TObject);
    procedure aAzurirajExecute(Sender: TObject);
    procedure aBrisiExecute(Sender: TObject);
    procedure aZacuvajExcelExecute(Sender: TObject);
    procedure aRefreshExecute(Sender: TObject);
    procedure aSnimiIzgledExecute(Sender: TObject);
    procedure aIzlezExecute(Sender: TObject);
    procedure aPecatiTabelaExecute(Sender: TObject);
    procedure aSnimiPecatenjeExecute(Sender: TObject);
    procedure zacuvajPrintVoIni(ime:AnsiString);
    procedure procitajPrintOdIni(ime:AnsiString);
    procedure aPodesuvanjePecatenjeExecute(Sender: TObject);
    procedure aBrisiPodesuvanjePecatenjeExecute(Sender: TObject);
    procedure aPageSetupExecute(Sender: TObject);
    procedure aFormConfigExecute(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure PartnerPropertiesChange(Sender: TObject);
    procedure aBrisiIzgledExecute(Sender: TObject);
    procedure aSpustiSoberiExecute(Sender: TObject);
    procedure aKreirajRNExecute(Sender: TObject);
    procedure aSteStavkiExecute(Sender: TObject);
    procedure site(kako:Boolean; cxgrid : TcxGridDBTableView);
    procedure cbSitePropertiesChange(Sender: TObject);
    procedure aDodadiVoRNExecute(Sender: TObject);
    procedure cxGrid1DBTableView1IzberiPropertiesEditValueChanged(
      Sender: TObject);
    //procedure cbSiteChange(Sender: TObject);
  private
    { Private declarations }

    _sifra_kluc :array[1..10] of string;  // �������� ���������� �� ������ �� ������� �� ������������� ���

  protected
    list : TList;
    inserting, sobrano : boolean;
    prva, posledna :TWinControl;
    procedure prefrli;

  public
    { Public declarations }
    constructor Create(Owner : TComponent; insert : boolean = true);reintroduce; overload;

    procedure SetSifra(br: integer; s : string);
    function GetSifra(br: integer) : string;
    var godina : string;
    //������� �� ���������� ������������ _sifra_kluc
    property sifra_kluc[br :integer]: string read GetSifra write SetSifra;
  end;

var
  frmIzberiPoracki: TfrmIzberiPoracki;
  rData : TRepositoryData;

implementation

uses DaNe, dmKonekcija, Utils, FormConfig, NurkoRepository, dmMaticni, dmSystem,
  dmUnit, NovRN;

{$R *.dfm}
//------------------------------------------------------------------------------

constructor TfrmIzberiPoracki.Create(Owner : TComponent; insert : boolean = true);
begin
inherited Create(Owner);
//	�������� ���� ������� � �������� �� ����������� ���������
//	insert = true -> ����� ���� ����� ���������� �� ������� �� Insert Mode
//  insert = false -> ����� ���� ����� ������� �� ����� �� ������. �� ������� ����� ����� �� �� �������� F5
  inserting := insert;
end;

//	����� �� �����������. �� ��������� ������� �� ������, �� �������� ������� �� ���� ��������,
//	�� ������� ������� �� ������ ������ � �� ������� �� Insert Mode
procedure TfrmIzberiPoracki.aNovExecute(Sender: TObject);
begin
//  if(cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) then
//  begin
//    dPanel.Enabled:=True;
//    lPanel.Enabled:=False;
//    prva.SetFocus;
//    cxGrid1DBTableView1.DataController.DataSet.Insert;
//  end
//  else ShowMessage('������ �������� �� ��� �������� �� �� ��������� �������/r/n � ����� ������� �� ������� ��� �����!');
end;

//	����� �� ���������. �� ��������� ������� �� ������, �� �������� ������� �� ���� ��������,
//	�� ������� ������� �� ������ ������ � �� ������� �� Edit Mode
procedure TfrmIzberiPoracki.aAzurirajExecute(Sender: TObject);
begin
//  if(cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) then
//  begin
//    dPanel.Enabled:=True;
//    lPanel.Enabled:=False;
//    prva.SetFocus;
//    cxGrid1DBTableView1.DataController.DataSet.Edit;
//  end
//  else ShowMessage('������ �������� �� ��� �������� �� �� ��������� �������/r/n � ����� ������� �� ������� ��� �����!');
end;

//	����� �� ������ �� ������������� �����
procedure TfrmIzberiPoracki.aBrisiExecute(Sender: TObject);
begin
//  if ((cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) and
//     (cxGrid1DBTableView1.DataController.RecordCount <> 0)) then
//    cxGrid1DBTableView1.DataController.DataSet.Delete();
end;

procedure TfrmIzberiPoracki.aBrisiIzgledExecute(Sender: TObject);
begin
  brisiGridVoBaza(Name,cxGrid1DBTableView1);
  //brisiPivotVoBaza(Name,cxDBPivotGrid1); �� pivot ��� ���
  BrisiFormaIzgled(self);
end;

//	����� �� ���������� �� ����������
procedure TfrmIzberiPoracki.aRefreshExecute(Sender: TObject);
begin
  cxGrid1DBTableView1.DataController.Filter.Root.Clear; //���������� �� ��������
  cxGrid1DBTableView1.DataController.DataSet.Refresh;
end;

//	����� �� ����� �� ������� �� ������� �� ������� �����
procedure TfrmIzberiPoracki.aIzlezExecute(Sender: TObject);
begin
  Close;
end;

procedure TfrmIzberiPoracki.aKreirajRNExecute(Sender: TObject);
var i,j : Integer;
begin
   // �� �� ������ ���� ����� �� ��

frmDaNe := TfrmDaNe.Create(self, '�������', '���� �������� ������� �� �������� �� ��?', 1);
  if (frmDaNe.ShowModal = mrYes) then
     begin
           // dm.tblRabotenNalog.Insert;
           //dm.tblRabotenNalogbroj.Value := dmMat.zemiMax(dm.pMaxRN,null,null,,null,null,)
           //dm.tblRabotenNalogBROJ.Value := dmMat.zemiMax(dm.pMaxRN,'godina',Null,Null,godina,Null,Null,'MAKS');
         //  dm.tblRabotenNalogBROJ.Value := ' ';

           // dm.tblRabotenNalogID_RE.AsInteger := dmKon.firma_id;
           // dm.tblRabotenNalogID_FIRMA.AsInteger := dmKon.firma_id;
           // dm.tblRabotenNalogGODINA.AsString := godina;
           // dm.tblRabotenNalogDATUM_PLANIRAN_POCETOK.Value := Now;
           // dm.tblRabotenNalog.Post;


            dm.tblRabotenNalog.Insert;
            frmNovRN := TfrmNovRN.Create(self);
            godina_rn := strtoint(godina);
            frmNovRN.qMaxBroj.ParamByName('godina').Value := godina;
            frmNovRN.qMaxBroj.ParamByName('id_re').Value := dmKon.UserRE;
            frmNovRN.qMaxBroj.ExecQuery;
            frmNovRN.txtBroj.Text := frmNovRN.qMaxBroj.FldByName['broj'].Value;
            frmNovRN.ShowModal;
         //   if frmNovRN.btnZapisi.ModalResult = mrOk then
          //      dm.tblRabotenNalogBROJ.Value := frmNovRN.txtBroj.Text+'/'+frmNovRN.txtNaziv.Text ;
           // else

            frmNovRN.Free;






          with cxGrid1DBTableView1.DataController do
             //begin
              for I := 0 to RecordCount - 1 do
              begin
                if (DisplayTexts[i,cxGrid1DBTableView1Izberi.Index]= '�����') then
                begin
                   // vk_selectirani := vk_selectirani+1;
                  //  if (momentalno>0) and (Abs(StrToFloat(Values[i,cxGrid2DBTableView1RAZLIKA.Index]))>0) then
                   //with cxGrid2DBTableView1.DataController do
                   // for j := 0 to RecordCount - 1 do

                   // begin
                         dm.pPorackaRN.Close;
                         dm.pPorackaRN.ParamByName('id_firma').AsInteger := dmKon.firma_id;
                         dm.pPorackaRN.ParamByName('godina').AsString := godina;
                         dm.pPorackaRN.ParamByName('id_poracka').AsString := (Values[i,cxGrid1DBTableView1ID.Index]);    // cxGrid1DBTableView1ID.EditValue;
                         dm.pPorackaRN.ParamByName('id_rn').asinteger := dm.tblRabotenNalogID.Value;
                      //   dm.pPorackaRN.ParamByName('id_ps').AsString := (Values[j,cxGrid2DBTableView1DBColumn13.Index]);    // cxGrid1DBTableView1ID.EditValue;
                         //uste eden parametar za poracka stavka, koj ke treba da se dodade vo procedurata

                         dm.pPorackaRN.ExecProc;
                    //end;

                end;
              end;
              ShowMessage('������� � �� �� ��������� �������!');


//              dm.tblRNStavka.Open;
//              dm.pRNNormativ.Open;
//              dm.tblRNSurovini.Open;
//              dm.tblArtikalRN.Close;
//              dm.tblArtikalRN.Open;
//
//              dm.tblIzberiRN.Close;
//              dm.tblIzberiRN.ParamByName('status').AsString := '1';
//              dm.tblIzberiRN.ParamByName('godina').AsString := godina;
//              dm.tblIzberiRN.ParamByName('id_re').AsString := '%';
//              dm.tblIzberiRN.Open;

              dm.tblIzberiPoracki.CloseOpen(true);
              dm.tblIzberiPStavki.open;
              dm.tblIzberiPElement.Open;
     end
     else
       abort;

//   dm.pPorackaRN.Close;
//   dm.pPorackaRN.ParamByName('id_firma').AsString := dmKon.firma;
//   dm.pPorackaRN.ParamByName('godina').AsString := godina;
//   dm.pPorackaRN.ParamByName('id_poracka').
//   dm.pPorackaRN.ParamByName('id_rn').asinteger := dm.tblRabotenNalogID.Value;
//   dm.pPorackaRN.ExecProc;


   //dm.tblRabotenNalog.AutoUpdateOptions.WhenGetGenID(2);

   //����� �� ��������� �� �� ������ ��, �� �� �� ������ ��������� �������
end;

//	����� �� ������� �� �������� �� ������ �� ���� (Utils.pas)
procedure TfrmIzberiPoracki.aSnimiIzgledExecute(Sender: TObject);
begin
  zacuvajGridVoBaza(Name,cxGrid1DBTableView1);
  ZacuvajFormaIzgled(self);
end;

//	����� �� ������� �� ������ �� Excel ������ (Utils.pas)
procedure TfrmIzberiPoracki.aZacuvajExcelExecute(Sender: TObject);
begin
  zacuvajVoExcel(cxGrid1, Caption);
end;

//  ��������� �� ������ �� ������� �� ���������� �� ����� �� Enter
procedure TfrmIzberiPoracki.EnterKakoTab(Sender: TObject; var Key: Word;
  Shift: TShiftState);
var
  kom : TWinControl;
begin
    kom := Sender as TWinControl;
    case Key of
        VK_DOWN:
        begin
          PostMessage(Handle,WM_NextDlgCtl,0,0);
          key:=0;
        end;
        VK_UP:
        begin
          PostMessage(Handle,WM_NextDlgCtl,1,0);
          key:=0;
        end;
        VK_RETURN:
        begin
          PostMessage(Handle,WM_NextDlgCtl,0,0);
        end;
        VK_INSERT:
        begin
          //  �� ����� ��������
//          if (kom = cxExtLookupComboBox1)  then
//          begin
//            frmNurkoRepository := TfrmNurkoRepository.Create(nil,Name,rData);
//            frmNurkoRepository.kontrola_naziv := kom.Name;
//            frmNurkoRepository.ShowModal;
//
//            if (frmNurkoRepository.ModalResult = mrOk) then
//              cxExtLookupComboBox1.EditValue := frmNurkoRepository.cxGrid1Level1.GridView.DataController.GetRecordId(frmNurkoRepository.cxGrid1Level1.GridView.DataController.FocusedRecordIndex);
//
//            frmNurkoRepository.Free;
//       	 end;
	end;
    end;
end;

//	������� �� ��� �� ������ ��� �������� �� �����
procedure TfrmIzberiPoracki.cxDBTextEditAllEnter(Sender: TObject);
begin
    TEdit(Sender).Color:=clSkyBlue;
end;

//	������� �� ��� �� ������ ��� ����� �� �����
procedure TfrmIzberiPoracki.cxDBTextEditAllExit(Sender: TObject);
begin
    TEdit(Sender).Color:=clWhite;
end;

procedure TfrmIzberiPoracki.cxGrid1DBTableView1IzberiPropertiesEditValueChanged(
  Sender: TObject);
  var i:integer;
begin
 // if cbSite.EditValue then
 // if cxGrid1DBTableView1Izberi. then
  if  cxGrid1DBTableView1.Columns[0].EditValue  then


  begin
  //  site(true, cxGrid2DBTableView1);
    //cxGrid1DBTableView1IzberiPropertiesChange(Sender);
   // dm.tblIzberiPStavki.Close;
  //  dm.tblizb
      //with cxGrid2DBTableView1.DataController do
      //  for I := 0 to FilteredRecordCount - 1 do
        dm.tblIzberiPStavki.First;
        while not dm.tblIzberiPStavki.Eof do
        begin
             //Values[FilteredRecordIndex[i] ,  cxGrid2DBTableView1IZBERI.Index]:= 1
             dm.tblIzberiPStavki.edit;
             dm.tblIzberiPStavkiIZBERI.Value := 1;
             dm.tblIzberiPStavki.Post;

             dm.tblIzberiPStavki.Next;
        end;
  end
  else
  begin
    //site(false, cxGrid2DBTableView1);
    //cxTxtVkZadolzuvanje.Text := '';
//    with cxGrid2DBTableView1.DataController do
 //       for I := 0 to FilteredRecordCount - 1 do
       // begin
 //         Values[FilteredRecordIndex[i] ,  cxGrid2DBTableView1IZBERI.Index]:= 0;
            dm.tblIzberiPStavki.First;
        while not dm.tblIzberiPStavki.Eof do
        begin
             //Values[FilteredRecordIndex[i] ,  cxGrid2DBTableView1IZBERI.Index]:= 1
             dm.tblIzberiPStavki.edit;
             dm.tblIzberiPStavkiIZBERI.Value := 0;
             dm.tblIzberiPStavki.Post;

             dm.tblIzberiPStavki.Next;
        end;
  end;

end;

//  ��������� �� ������ �� ��������� �� ���������� ���� �� ��������
//  ��������� ��: br - ����� ��� �� ������ (�� 0 �� 9) �� ��������� �� ����� �� ����� �� ���������� ����
//  s - ��������� �� ������ �� ������ (���������� �� string �� ����� �� integer �� IntToStr ���������)
procedure TfrmIzberiPoracki.SetSifra(br: integer; s : string);
begin
  _sifra_kluc[br] := s;
end;

//  ������� �� ������ �� ��������� �� ���������� ���� �� ��������
//  ��������� ��: br - ����� ��� �� ������ (�� 0 �� 9) �� ��������� �� ����� �� ����� �� ���������� ����
function TfrmIzberiPoracki.GetSifra(br: integer) : string;
begin
  Result := _sifra_kluc[br];
end;

procedure TfrmIzberiPoracki.PartnerPropertiesChange(Sender: TObject);
begin

end;

//  �� ������� �� ������� ��� ����������� �� Master-�� �� �������� �� ��������� �� ������ ����� SetSifra
procedure TfrmIzberiPoracki.prefrli;
begin
end;

procedure TfrmIzberiPoracki.FormClose(Sender: TObject; var Action: TCloseAction);
begin
    //	��������� ��� ��������� �� �������
//    if (cxGrid1DBTableView1.DataController.DataSource.State = dsEdit) or (cxGrid1DBTableView1.DataController.DataSource.State = dsInsert) then
//    begin
//        frmDaNe := TfrmDaNe.Create(self, '���������� ��������', '���������� �� �� ��������. ���� ������ �� �� �������?', 1);
//        if (frmDaNe.ShowModal <> mrYes) then
//        begin
//            cxGrid1DBTableView1.DataController.DataSet.Cancel;
//            Action := caFree;
//        end
//        else
//          if (Validacija(dPanel) = false) then
//          begin
//            cxGrid1DBTableView1.DataController.DataSet.Post;
            Action := caFree;
//          end
//          else
//   Action := caNone;
//    end;
end;
procedure TfrmIzberiPoracki.FormCreate(Sender: TObject);
begin
  ProcitajFormaIzgled(self);
  if (dmRes.use_skin = true) then dxRibbon1.ColorSchemeName := dmRes.skin_name;

  rData := TRepositoryData.Create();  // kreirame instance od klasata TRepositoryData sto se naogja vo dmRes
end;

//------------------------------------------------------------------------------

procedure TfrmIzberiPoracki.FormShow(Sender: TObject);
begin
  //  ������� �� ������������ ������������/���������� �� ���������� �� ���� �����
    SpremiForma(self);
  //	�� ��������� �� ������� ������ ��� ���������� � ���� � ��� �������� �� �������
//    PrvPosledenTab(dPanel,posledna,prva);
    dxBarManager1Bar1.Caption := Caption;
    dxComponentPrinter1Link1.ReportTitle.Text := Caption;

  //	������� �� ������������ �� ������ �� ������
    procitajGridOdBaza(Name,cxGrid1DBTableView1,false,false);
  //	������� �� ������������ �� ��������� �� ������
    procitajPrintOdBaza(Name,cxGrid1DBTableView1.Name, dxComponentPrinter1Link1);
    sobrano := true;

    dm.tblIzberiPoracki.CloseOpen(True);
    dm.tblIzberiPStavki.Open;
    dm.tblIzberiPElement.Open;

    cxSplitter2.CloseSplitter;

//  �������� �� ����� �� ������ �� cxExtLookupComboBox ��� cxDBExtLookupComboBox ��������
    //cxExtLookupComboBox1.RepositoryItem := dmRes.InitRepository(23, 'cxExtLookupComboBox1', Name, rData);
  	//procitajGridOdBaza(Name + '_' + IntToStr(rData.nurkoRepository[rData.br_repository]), rData.gridRepository[rData.br_repository] ,false,false);
end;
//------------------------------------------------------------------------------

procedure TfrmIzberiPoracki.SaveToIniFileExecute(Sender: TObject);
begin
    cxGrid1DBTableView1.StoreToIniFile(Name,True,[]);
end;
//------------------------------------------------------------------------------

procedure TfrmIzberiPoracki.cxGrid1DBTableView1KeyPress(Sender: TObject;
  var Key: Char);
begin
  if(Ord(Key) <> VK_RETURN) then cxSortiraj(cxGrid1DBTableView1);
end;

//  ����� �� �����
procedure TfrmIzberiPoracki.aZapisiExecute(Sender: TObject);
var
  st: TDataSetState;
begin
//  ZapisiButton.SetFocus;

//  st := cxGrid1DBTableView1.DataController.DataSet.State;
//  if st in [dsEdit,dsInsert] then
//  begin
//    if (Validacija(dPanel) = false) then
//    begin
//      if ((st = dsInsert) and inserting) then
//      begin
//        cxGrid1DBTableView1.DataController.DataSet.Post;
//        aNov.Execute;
//      end;
//
//      if ((st = dsInsert) and (not inserting)) then
//      begin
//        cxGrid1DBTableView1.DataController.DataSet.Post;
//        dPanel.Enabled:=false;
//        lPanel.Enabled:=true;
//        cxGrid1.SetFocus;
//      end;
//
//      if (st = dsEdit) then
//      begin
//        cxGrid1DBTableView1.DataController.DataSet.Post;
//        dPanel.Enabled:=false;
//        lPanel.Enabled:=true;
//        cxGrid1.SetFocus;
//      end;
//    end;
//  end;
end;


procedure TfrmIzberiPoracki.cbSitePropertiesChange(Sender: TObject);
begin
   aSiteStavki.Execute;
end;

procedure TfrmIzberiPoracki.aSteStavkiExecute(Sender: TObject);
begin
  if cbSite.EditValue then
  begin
    site(true, cxGrid1DBTableView1);

    //cxGrid1DBTableView1IzberiPropertiesChange(Sender);
  end
  else
  begin
    site(false, cxGrid1DBTableView1);
    //cxTxtVkZadolzuvanje.Text := '';
  end;
end;

//	����� �� ���������� �� �������
procedure TfrmIzberiPoracki.aOtkaziExecute(Sender: TObject);
begin
  if (cxGrid1DBTableView1.DataController.DataSource.State = dsBrowse) then
  begin
      ModalResult := mrCancel;
      Close();
  end
//  else
//  begin
//      cxGrid1DBTableView1.DataController.DataSet.Cancel;
//      RestoreControls(dPanel);
//      dPanel.Enabled := false;
//      lPanel.Enabled := true;
//      cxGrid1.SetFocus;
//  end;
end;

//----------------------------------------------------------------------------------
// ��������� �� ��������, ���������� �� ��������� � �������� �� Print ��������
//----------------------------------------------------------------------------------

//	����� �� �������� �� �������� �� �������
procedure TfrmIzberiPoracki.aPageSetupExecute(Sender: TObject);
begin
  dxComponentPrinter1Link1.PageSetup;
end;

//	����� �� ������� �� ������
procedure TfrmIzberiPoracki.aPecatiTabelaExecute(Sender: TObject);
begin
  dxComponentPrinter1Link1.ReportTitle.Text := Caption;

  dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Clear;
  dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Add(dmkon.firma_naziv);
  dxComponentPrinter1Link1.PrinterPage.PageHeader.RightTitle.Add(DateTimeToStr(Now));

  dxComponentPrinter1Link1.PrinterPage.PageHeader.LeftTitle.Clear;
  //dxComponentPrinter1Link1.PrinterPage.PageHeader.LeftTitle.Add('������� : ' + TipSifra.Text + '-' + IdSifra.Text + ' ' + Partner.Text);

  dxComponentPrinter1.Preview(true, dxComponentPrinter1Link1);
end;

//	����� �� ���������� �� ��������� �� �������
procedure TfrmIzberiPoracki.aPodesuvanjePecatenjeExecute(Sender: TObject);
begin
  dxComponentPrinter1Link1.DesignReport();
end;

procedure TfrmIzberiPoracki.aSnimiPecatenjeExecute(Sender: TObject);
begin
  zacuvajPrintVoBaza(Name,cxGrid1DBTableView1.Name,dxComponentPrinter1Link1);
end;

//	����� �� �������� ��� �������� �� �������� �� ������ ���� ��� ��������� �� ����� ������
procedure TfrmIzberiPoracki.aSpustiSoberiExecute(Sender: TObject);
begin
  if (sobrano = true) then
  begin
    cxGrid1DBTableView1.ViewData.Expand(false);
    sobrano := false;
  end
  else
  begin
    cxGrid1DBTableView1.ViewData.Collapse(false);
    sobrano := true;
  end;
end;

//	����� �� ������ �� ����������� �������� �� ������� (Utils.pas)
procedure TfrmIzberiPoracki.aBrisiPodesuvanjePecatenjeExecute(Sender: TObject);
begin
  brisiPrintOdBaza(Name,cxGrid1DBTableView1.Name, dxComponentPrinter1Link1);
end;

procedure TfrmIzberiPoracki.aDodadiVoRNExecute(Sender: TObject);
var i : Integer;
begin
//proverka kakov e RN (negoviot status)
//informacija vo koj RN kje vleze sakanata poracka
 if dm.tblRabotenNalogSTATUS.Value <> 2 then
 begin
   frmDaNe := TfrmDaNe.Create(self, '�������', '���� �� �������� ������� �� �� '+dm.tblRabotenNalogBROJ.AsString+'/'+dm.tblRabotenNalogGODINA.AsString+ '?', 1);
   if (frmDaNe.ShowModal = mrYes) then
      begin
          with cxGrid1DBTableView1.DataController do
             //begin
              for I := 0 to RecordCount - 1 do
              begin
                if (DisplayTexts[i,cxGrid1DBTableView1Izberi.Index]= '�����') then
                begin
                   // vk_selectirani := vk_selectirani+1;
                  //  if (momentalno>0) and (Abs(StrToFloat(Values[i,cxGrid2DBTableView1RAZLIKA.Index]))>0) then
                    begin
                         dm.pPorackaRN.Close;
                         dm.pPorackaRN.ParamByName('id_firma').AsInteger := dmKon.firma_id;
                         dm.pPorackaRN.ParamByName('godina').AsString := godina;
                         dm.pPorackaRN.ParamByName('id_poracka').AsString := (Values[i,cxGrid1DBTableView1ID.Index]);    // cxGrid1DBTableView1ID.EditValue;
                         dm.pPorackaRN.ParamByName('id_rn').asinteger := dm.tblRabotenNalogID.Value;
                         dm.pPorackaRN.ExecProc;
                    end;

                end;
              end;
              dm.tblIzberiPoracki.CloseOpen(true);
              dm.tblIzberiPStavki.open;
              dm.tblIzberiPElement.Open;

      end;
 end;

end;

//  ����� �� ����� �� ����� �� ������������� �� �������� � ��������������� ��
//  �������(������ ���������� ��������)
procedure TfrmIzberiPoracki.aFormConfigExecute(Sender: TObject);
begin
  frmFormConfig := TfrmFormConfig.Create(Application);
  frmFormConfig.formPtr := Addr(Self);
  frmFormConfig.ShowModal;
  frmFormConfig.Free;
end;

//----------------------------------------------------------------------------------
// ���� -> ��������� �� ��������, ���������� �� ��������� � �������� �� Print ��������
//----------------------------------------------------------------------------------

// ��������� �� ���������� �� ��������� � �������� �� Print �������� �� .ini ��������
procedure TfrmIzberiPoracki.zacuvajPrintVoIni(ime:AnsiString);
var
  AStream: TFileStream;
begin
  AStream := TFileStream.Create(dmKon.RabotenDir + ime + 'Print.ini', fmCreate);
  try
    AStream.WriteComponent(dxComponentPrinter1Link1);
  finally
    AStream.Free;
  end;

end;

// ��������� �� ������ �� ��������� � �������� �� Print �������� �������� �� .ini ��������
procedure TfrmIzberiPoracki.procitajPrintOdIni(ime:AnsiString);
var
  AStream: TFileStream;
begin
  AStream := TFileStream.Create(dmKon.RabotenDir + ime + 'Print.ini', fmOpenRead);
  try
    dxComponentPrinter1Link1.RestoreDefaults;
    AStream.ReadComponent(dxComponentPrinter1Link1);
  finally
    AStream.Free;
  end;
end;

procedure TfrmIzberiPoracki.site(kako:Boolean; cxgrid : TcxGridDBTableView);   //procedura so koa gi stikliram - otstikliram site
var    suma, I,j, izb:INTEGER;
begin
        with cxGrid.DataController do
        for I := 0 to FilteredRecordCount - 1 do
        begin
          Values[FilteredRecordIndex[i] ,  cxGrid.Columns[0].Index]:=  kako;

//        if kako  then  izb := 1 else izb := 0;
//
//        with cxGrid2DBTableView1.DataController do
//        for j := 0 to FilteredRecordCount - 1 do
//        begin
//          Values[FilteredRecordIndex[j] ,  cxGrid2DBTableView1.Columns[0].Index]:=  izb;

//        if kako  then  izb := 1 else izb := 0;
//
//
//
//        dm.tblIzberiPStavki.First;
//        while not dm.tblIzberiPStavki.Eof do
//        begin
//             //Values[FilteredRecordIndex[i] ,  cxGrid2DBTableView1IZBERI.Index]:= 1
//             dm.tblIzberiPStavki.edit;
//             dm.tblIzberiPStavkiIZBERI.Value := izb;
//             dm.tblIzberiPStavki.Post;
//
//             dm.tblIzberiPStavki.Next;
       // end;
         end;


end;

end.
